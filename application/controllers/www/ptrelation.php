<?php
class Ptrelation extends WWW_Controller{
	public $menu1='ptrelation';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/ptrelation_model','ptrelation');
	}

	public function index(){
		$this->ptrelation->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->ptrelation->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->ptrelation->attributeLabels();
		$data['listLayout']=$this->ptrelation->listLayout();
		$this->render('www/ptrelation/list',$data);
	}
	public function add(){
		$this->ptrelation->db->query('SET NAMES UTF8');
		$data["labels"]=$this->ptrelation->attributeLabels();
		
		
		
		
		
		if(!empty($_POST)){
		if(isset($_GET['type_id'])){
           $_POST['ptrelation']['type_id']=$_GET['type_id'];
           $this->ptrelation->add($_POST['ptrelation']);
		    success('www/ptrelation?type_id='.$_GET['type_id'],'创建成功');
		}else{
		  $this->ptrelation->add($_POST['ptrelation']);
		  success('www/ptrelation','创建成功');
		}
			
		}
		$this->render('www/ptrelation/add',$data);
	}
	public function update(){
		$this->ptrelation->db->query('SET NAMES UTF8');
		$data["labels"]=$this->ptrelation->attributeLabels();
		$data['id_aData']=$this->ptrelation->id_aGetInfo($_GET['ptrelation_id']);
		$data['listLayout']=$this->ptrelation->listLayout();
		
		
		
		
		
		if(!empty($_POST)){
		    if(isset($_GET['type_id'])){
		      $_POST['ptrelation']['type_id']=$_GET['type_id'];
		      $this->ptrelation->update($_POST['ptrelation'],$_GET['ptrelation_id']);
			    success('www/ptrelation?type_id='.$_GET['type_id'],'更新成功');
		    }else{
              	$this->ptrelation->update($_POST['ptrelation'],$_GET['ptrelation_id']);
			    success('www/ptrelation','更新成功');
		    }
		}
		$this->render('www/ptrelation/update',$data);
	}

}
?>