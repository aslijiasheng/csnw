<?php
class Applyopen extends WWW_Controller{
	public function __construct(){
		parent::__construct();
		require_once('application/libraries/snoopy.php');
		$this->db->query('SET NAMES UTF8');
		$this->load->model('www/applyopen_model','applyopen');
		$this->applyopen->db->query('SET NAMES UTF8');
		$this->load->model('www/order_d_model','order_d');
		$this->order_d->db->query('SET NAMES UTF8');
		$this->load->model('www/system_log_model','system_log');
		$this->system_log->db->query('SET NAMES UTF8');
		$this->load->model('www/api_model','api');
		$this->load->model('www/order_model','order');
		$this->load->model('www/message_model','message');
		$this->api->db->query('SET NAMEES UTF8');
	}
	
	//cus lee 2014-05-25 通过订单明细ID同步订单到crm
	public function order_crm($order_d_id){
		//首先查询出所对应的订单相关信息
		$sql = "select order_d_order_id from tc_order_d where order_d_id = '".$order_d_id."'";
		$res = $this->db->query($sql)->result_array();
		$order_id = $res[0]['order_d_order_id'];
		//获取订单信息
		$order_data = $this->order->id_aGetInfo($order_id);
		//这里需要判断几个条件
		//这里要判断下订单的创建人，只有合同同步账号过来的订单才做同步
		if($order_data['order_create_user_id']!=1000019){
			return "该订单不是从合同过来的订单，不能同步到CRM";
		}
		//判断这个订单是否同步过CRM
		if($order_data['order_is_crm']==1002){
			return "该订单已经同步过CRM,不能再同步了";
		}
		//如果有产品没有开通则不同步
		foreach($order_data['detailed']['order_d_arr'] as $k=>$v){
			if($v['order_d_product_basic_style']!=1003){
				return "该订单存在未开通信息，只有全部开通后才能同步给CRM";
			}
		}
		//return "达标！开始同步";
		//p($res);
		
		$this->load->model('www/api_model','api');
		$this->api->order_crm_add($order_id);
		return "达标！开始同步";
	}

	//线下开通页面
	public function ajax_belowline_open(){
		//echo 123;
		$data['order_d_id'] = $_GET['id'];
		//首先查询这个订单明细已有开通信息了？如果已有则报错，不能申请
		$isThere = $this->applyopen->id_isThere($data['order_d_id']);
		if(!$isThere){
			echo "还没有申请开通！";exit;
		}
		//首先查询出关于这个订单明细对应的产品开通数据
		$data['params'] = $this->order_d_id_params_info($data['order_d_id']);
		//查询applyopen里属性
		$applyopen_data = $this->applyopen->id_aGetInfo($data['order_d_id']);
		//p($applyopen_data);
		$data['applyopen']['open']=json_decode($applyopen_data['applyopen_open_info'],true);
		$data['applyopen']['return']=json_decode($applyopen_data['applyopen_return_info'],true);
		$this->load->view('www/applyopen/ajax_belowline_open',$data);
	}

	//申请开通页面
	public function ajax_apply_open(){
		$data['order_d_id'] = $_GET['id'];
		//首先查询出关于这个订单明细对应的产品开通数据
		$data['params'] = $this->order_d_id_params_info($data['order_d_id']);
		//p($data);exit;
		$this->load->view('www/applyopen/ajax_apply_open',$data);
	}

	//确认开通页面
	public function ajax_confirm_open(){
		$data['order_d_id'] = $_GET['id'];
		//首先查询这个订单明细已有开通信息了？如果已有则报错，不能申请
		$isThere = $this->applyopen->id_isThere($data['order_d_id']);
		if(!$isThere){
			echo "还没有申请开通！";exit;
		}
		//首先查询出关于这个订单明细对应的产品开通数据
		$data['params'] = $this->order_d_id_params_info($data['order_d_id']);
		//查询applyopen里属性
		$applyopen_data = $this->applyopen->id_aGetInfo($data['order_d_id']);
		//解析出里面的开通参数
		$data['open_info'] = json_decode($applyopen_data['applyopen_open_info'],true);
		//查询出这条的备注信息
		$where = array(
			'system_log_module'=>'order_d',
			'system_log_module_id'=>$applyopen_data['applyopen_order_d_id'],
			'system_log_operation'=>'申请开通',
		);
		$system_log = $this->system_log->GetInfo($where);
		//p($system_log);
		$data['note'] = $system_log[0]['system_log_note'];
		//p($data);
		$this->load->view('www/applyopen/ajax_confirm_open',$data);
	}

	//查看参数
	public function ajax_see_params(){
		$data['order_d_id'] = $_GET['id'];
		//首先查询出关于这个订单明细对应的产品开通数据
		$data['params'] = $this->order_d_id_params_info($data['order_d_id']);
		//查询applyopen里属性
		$applyopen_data = $this->applyopen->id_aGetInfo($data['order_d_id']);
		$data['applyopen']['open']=json_decode($applyopen_data['applyopen_open_info'],true);
		$data['applyopen']['return']=json_decode($applyopen_data['applyopen_return_info'],true);
		$this->order_crm($data['order_d_id']);//cus lee 2014-05-25 同步crm订单 
		$this->load->view('www/applyopen/ajax_see_params',$data);
	}


	//通过明细ID获取者个产品的开通参数
	public function order_d_id_params_info($id){
		//$sql = "select * from tc_product_oparam where product_oparam_product_basic_id = (select product_basic_id from tc_product_basic where product_basic_code = (select order_d_product_basic_code from tc_order_d where order_d_id = '".$id."'))";
		
		$sql = "
select 
po.product_oparam_id,
po.product_oparam_product_basic_id,
po.product_oparam_name,
po.product_oparam_content,
po.product_oparam_type,
po.product_oparam_atime,
po.product_oparam_utime 
from tc_product_oparam po
left join tc_product_basic pb on po.product_oparam_product_basic_id = pb.product_basic_id
left join tc_order_d od on pb.product_basic_code = od.order_d_product_basic_code
where order_d_id = '".$id."'
		";
		//p($sql);
		$res = $this->db->query($sql)->result_array();
		foreach ($res as $k=>$v){
			if($v['product_oparam_type']==1001){ //表示开通参数
				$res['open'][]=json_decode($v['product_oparam_content'],true);
			}
			if($v['product_oparam_type']==1002){ //表示返回参数
				$res['return'][]=json_decode($v['product_oparam_content'],true);
			}
		}
		return $res;
	}


	//ajax调用的修改(专门给线下使用)
	public function ajax_update_post(){
		if($_POST){
			$order_d_id = $_POST['order_d_id'];
			//p($_POST);
			$params_r = $_POST['params']['return'];
			$params_r['note'] = $_POST['note'];
			$note = $_POST['note'];
			//写入开通参数和返回参数信息
			$data=array(
				'applyopen_return_info'=>json_encode($params_r),
			);

			//cus lee 2014-06-20 开通参数、返回参数回写 start
			if(isset($params_r['actual_startdate']) and isset($params_r['actual_enddate'])){
				$open_startdate = $params_r['actual_startdate'];//开通开始时间
				$open_enddate = $params_r['actual_enddate'];//开通结束时间
				$upsql = "update tc_order_d set order_d_open_startdate='".$open_startdate."',order_d_open_enddate='".$open_enddate."' where order_d_id = '".$order_d_id."'";
				$this->db->query($upsql);
			}
			//cus lee 2014-06-20 开通参数、返回参数回写 end

			//p($data);exit;
			$this->applyopen->update($order_d_id,$data);
			//修改订单明细的开通状态
			$order_d_data = array(
				 "order_d_product_basic_style"=>1003,
			);
			$this->order_d->update($order_d_data,$order_d_id);
			//把备注单独拿出来写日志
			$log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s'),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'线下开通',
				'system_log_note'=>$note,
				'system_log_module'=>'order_d',
				'system_log_module_id'=>$order_d_id
			);
			$this->system_log->add($log_data);
			$applyopen_id = $this->db->select('applyopen_id')->from('tc_applyopen')->where('applyopen_order_d_id',$order_d_id)->get()->row_array();
			$applyopen_id = $applyopen_id['applyopen_id'];
			$message_data = array(
					'message_status'=>1002
				);
	            $param['message_type'] = 1009;
	            $param['message_module_id'] = $applyopen_id;
				$this->message->update($message_data,$param);
			$this->order_crm($order_d_id);//cus lee 2014-05-25 同步crm订单
			echo 1;
		}else{
			echo '失败';
		}
	}

	//ajax调用的新增
	public function ajax_add_post(){
		if($_POST){
			//首先查询这个订单明细已有开通信息了？如果已有则报错，不能申请
			$isThere = $this->applyopen->id_isThere($_POST['order_d_id']);
			if($isThere){
				//p($_POST['order_d_id']);
				//p($isThere);
				//echo "已有开通信息，不能再新建";exit;
				//如果已经存在则直接做修改，免得出现错误的情况！
				$params = $_POST['params'];
				$note = $params['note'];
				$applyopen_open_info = $params;
				//去掉里面的备注
				unset($applyopen_open_info['note']);
				$applyopen_data['applyopen_open_info'] = json_encode($applyopen_open_info);//开通参数
				//p($applyopen_data['applyopen_open_info']);
				$applyopen_data['applyopen_create_user'] = $this->session->userdata('user_id');//创建人
				$applyopen_data['applyopen_create_time'] = date('Y-m-d H:i:s');//创建时间
				$applyopen_data['applyopen_order_d_id'] = $_POST['order_d_id'];//所属订单明细ID
				//p($applyopen_open_info);exit;
				$data=array(
					'applyopen_open_info'=>$applyopen_data['applyopen_open_info'],
				);
				$this->applyopen->update($applyopen_data['applyopen_order_d_id'],$data);
			}else{
				//p($_POST['params']);//exit;
				$params = $_POST['params'];
				$note = $params['note'];
				$applyopen_open_info = $params;
				//去掉里面的备注
				//unset($applyopen_open_info['note']);
				$applyopen_data['applyopen_open_info'] = json_encode($applyopen_open_info);//开通参数
				$applyopen_data['applyopen_create_user'] = $this->session->userdata('user_id');//创建人
				$applyopen_data['applyopen_create_time'] = date('Y-m-d H:i:s');//创建时间
				$applyopen_data['applyopen_order_d_id'] = $_POST['order_d_id'];//所属订单明细ID
				//p($applyopen_data);die;
				$this->applyopen->add($applyopen_data);
			}
			//修改订单明细的开通状态
			$applyopen_id = $this->db->insert_id();
			$order_d_data = array(
				 "order_d_product_basic_style"=>1002,
			);
			$this->order_d->update($order_d_data,$applyopen_data['applyopen_order_d_id']);
			//把备注单独拿出来写日志
			$log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s'),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'申请开通',
				'system_log_note'=>$note,
				'system_log_module'=>'order_d',
				'system_log_module_id'=>$applyopen_data['applyopen_order_d_id']
			);
			$this->system_log->add($log_data);
			$order_id = $this->db->select('order_d_order_id')->from('tc_order_d')->where('order_d_id',$applyopen_data['applyopen_order_d_id'])->get()->row_array();
			$order_data = $this->order->id_aGetInfo($order_id['order_d_order_id']);

			//p($order_data);
			$message_data = array(
				'message_owner'=>$order_data['order_nreview'],
				'message_module'=>'applyopen',
				'message_module_id'=>$applyopen_id,
				'message_type'=>1009,
				'message_department'=>$order_data['order_nreview_arr']['user_department'],
				'message_url'=>'www/applyopen/ajax_confirm_open'
			);
				$this->message->add($message_data);
			echo 1;
		}else{
			echo '失败';
		}
	}

	//ajax调用的确认开通(这里得打接口了)
	public function ajax_confirm_post(){
		if($_POST){
			//p($_POST);exit;
			$params = $_POST['params'];
			$params['order_d_id']=$_POST['order_d_id'];
			//p($params);

			if($params['issue_type']=='taoex'){
				$res = $this->taoex_open($params);
				//p($res);
				if($res['res']==false){
					//p($params);
					echo '原因:'.$res['msg'];
				}
				if($res['res']==true){
					//p($res['msg']);
					echo 1;exit;
				}
			}
			//OA的开通方法
			if ($params['issue_type']=='oaekd' or $params['issue_type']=='oabqkz' or $params['issue_type']=='oadrp' or $params['issue_type']=='oawxd'){
				$res = $this->oa_open($params);
				if($res['res']=='succ'){
					echo 1;
				}else{
					//echo 1;
					p($res);
				}
			}
			if ($params['issue_type']=='sms'){
				$res = $this->sms_open($params);
				if($res['res']==1){
					echo 1;
				}else{
					p($res);
				}
			}
			if ($params['issue_type']=='saas'){
				$res = $this->saas_open($params);
				if($res['res']==1){
					echo 1;
				}else{
					p($res);
				}
			}
			$this->order_crm($params['order_d_id']);//cus lee 2014-05-25 同步crm订单 
		}else{
			echo '失败：没有参数';
		}
	}





	/*---------------------下面是各开通接口------------------------*/
	//cus lee 2013-12-9 oa自动开通方法 start
	public function oa_open($params_arr){

		//echo "<pre>";print_r($params_arr);echo "</pre>";exit;
		$url = 'http://oa.eqimingxing.com/index.php/openapi/openapp/router'; //正式环境
		//$url = 'http://smb.vip.ishopex.cn/cmop/index.php/openapi/openapp/router'; //测试环境
		$token = 'qw1re2c4e9ac3673de3bc12b1cytuwed';

$sql = "SELECT
o.order_number,
od.order_d_product_basic_primecost,
od.order_d_product_basic_disc,
a.account_account_shopexid,
a.account_email
FROM tc_order_d od
left join tc_order o on o.order_id = od.order_d_order_id
left join tc_account a on a.account_id = o.order_account
where od.order_d_id = '".$params_arr['order_d_id']."'";
//p($sql);exit;
$res = $this->db->query($sql)->result_array();
//p($res);exit;
$order_number = $res[0]["order_number"]; //订单号
$fee = $res[0]["order_d_product_basic_primecost"]; //订单原价
$total_pay_fee = $res[0]["order_d_product_basic_disc"]; //订单实付
$shopex_id = $res[0]["account_account_shopexid"]; //客户的商业ID
$email = $res[0]["account_email"]; //客户的邮箱

//如果没有开通邮箱，则直接用开通邮箱作为用户账号
if ($email==""){
	$email = $this->api->lee_getCustomerEmail($shopex_id);
}

		if ($params_arr['issue_type']=='oaekd'){ //易开店产品
			$params['method'] = 'open.ekd';//方法名    是    string
			$params['domain'] = $params_arr['ymqz'];//域名前缀    是  string
			$params['shopex_id'] = $shopex_id;//shopex_id   是 string
			$params['shopex_email'] = $email;//shopex_email    是  string
			$params['version'] = $params_arr['version']; //1 标准版 2 企业版 3 旗舰版       版本    是   int
			$params['order_id'] = $order_number;//订单号  是  string
			$params['begin_time'] = $params_arr['start_date'];//应用开始时间 是   string
			$params['end_time'] = $params_arr['end_date'];//应用结束时间 是   string
		}
		if ($params_arr['issue_type']=='oadrp'){ //DRP
			$params['method'] = 'open.drp';//方法名    是    string
			$params['domain'] = $params_arr['ymqz'];//域名前缀    是   string
			$params['shopex_id'] = $shopex_id;//shopex_id   是 string
			$params['shopex_email'] = $email;//shopex_email    是  string
			$params['version'] = $params_arr['version']; //1 基础版 2 标准版 3 企业版 4 旗舰版 5 快速启动版  版本    是  int
			$params['order_id'] = $order_number;//订单号  是  string
			$params['begin_time'] = $params_arr['start_date'];//应用开始时间 是  string
			$params['end_time'] = $params_arr['end_date'];//应用结束时间 是  string
			if ($params_arr['is_erp_enable']==1){
				$params['is_erp_enable'] = $params_arr['is_erp_enable']; //0 | 1   控制是否同时开通ERP 只有在版本是企业和旗舰版时有效,其他版本不开通ERP   否  int 这个不填了
			}
			//$params['mobile'] = '13288888888';//手机号码 否  string 这个不填了
		}
		if ($params_arr['issue_type']=='oabqkz'){ //版权控制
			$params['method'] = 'control.copyright';//方法名    是    string
			$params['domain'] = $params_arr['sjym'];//三级域名地址    是   string
			$params['clear_version'] = $params_arr['clear_version'];//0 | 1   是否清楚版权   是  int
		}
		//cus lee 微信道的开通参数 2013-12-23 start
		if ($params_arr['issue_type']=='oawxd'){ //微信道
			$params['method'] = 'open.wxd';//方法名    是    string
			$params['domain'] = $params_arr['ymqz'];//域名前缀    是  string
			$params['shopex_id'] = $entid;//shopex_id   是 string
			if ($sere[0]["email"]==""){
				$params['shopex_email'] = kernel::single('center_api_prism_sync')->lee_getCustomerEmail($entid);//shopex_email    是  string
			}else{
				$params['shopex_email'] = $sere[0]["email"];
			}
			$params['version'] = '1'; //1 标准版目前只有这个版本 版本    是   int
			$params['order_id'] = $order_no;//订单号  是  string
			$params['begin_time'] = $params_arr['start_date'];//应用开始时间 是   string
			$params['end_time'] = $params_arr['end_date'];//应用结束时间 是   string
		}
		//cus lee 微信道的开通参数 2013-12-23 end
			
		//p($params);exit;
		$params['sign'] = $this->gen_oa_sign($params,$token);


		$snoopy = new snoopy();
		$snoopy->submit($url,$params);
		$res = $snoopy->results;
		//echo $res;
		$json_arr = json_decode($res,true);
		//echo "<pre>";print_r($json_arr);echo "</pre>";

		//做个记录
		$toppath = $_SERVER["DOCUMENT_ROOT"] . "lee_text/applyopen_" . date('Y-m-d', time()) . "_oa.txt";
		$Ts = fopen($toppath, "a+");
		fputs($Ts, json_encode($params)."\r\n".$res."\r\n");
		fclose($Ts);

		if($json_arr['res']=='succ'){
			$params_all['open']=$params_arr;
			$params_all['return']=$params_arr;
			if ($params_arr['start_date']==""){
				$params_all['return']['start_date']=date("Y-m-d H:i:s",time());
			}
			//echo "开通成功";
			if (isset($json_arr['info']['domain'])){
				$params_all['return']['domain']=$json_arr['info']['domain'];
			}

			//实际开通日期
			if (isset($params['begin_time'])){
				$params_all['return']['actual_startdate']=$params['begin_time']; //添加实际开通日期到返回参数中
			}
			//实际结束日期
			if (isset($params['end_time'])){
				$params_all['return']['actual_enddate']=$params['end_time']; //添加实际结束日期到返回参数中
			}
			//exit;
			//p($json_arr);exit;
			$this->open_is_ok($params_all,$params_arr['order_d_id']);


			//cus lee 判断出是否同时开通ERP为是的话！同时开通套件里的ERP 2013-12-17 start
			if($params_arr['issue_type']=='oadrp' and $params_arr['is_erp_enable']==1 and ($params_arr['version']==3 or $params_arr['version']==4)){
				//当开通oadrp是企业版、旗舰版并且是否同时开通ERP为是
				//将开通erp的状态改成已开通
$rel_sesql = "
select od.order_d_id from tc_order_d od
left join tc_product_basic pb on pb.product_basic_code =  od.order_d_product_basic_code
left join tc_product_oparam po on po.product_oparam_product_basic_id = pb.product_basic_id and po.product_oparam_name = '工单类型'
where od.order_d_order_id = (select order_d_order_id 
from tc_order_d
where order_d_id='".$params_arr['order_d_id']."')
and po.product_oparam_content like '%oatj%'
";
				$rel_data = $this->db->query($rel_sesql)->result_array();
				//$rel_order_d_id = $rel_data[0]['order_d_id'];
				//echo $rel_sesql;exit;
				$rel_order_d_id = $rel_data[0]['order_d_id'];
				//这里的开通参数不能动的！所以等于空
				$rel_params_all['open']="";
				$rel_params_all['return']=$params_all['return'];
				$this->open_is_ok($rel_params_all,$rel_order_d_id);
			}
			
			//cus lee 判断出是否同时开通ERP为是的话！同时开通套件里的ERP 2013-12-17 end

			$return_arr= array(
				"res"=>true,
				"msg"=>"开通成功",
			);
			return $return_arr;

		}
		if ($json_arr['res']='fail'){
			$msg=$json_arr["msg"]."<pre>".print_r($params,true).print_r($json_arr,true)."</pre>";
			//echo "<pre>";print_r($params);print_r($json_arr);echo "</pre>";exit;
			$return_arr= array(
				"res"=>false,
				"msg"=>$msg,
			);
			return $return_arr;
		}
	}

	//签名方法
	public function gen_oa_sign($params,$token){
		return strtoupper(md5(strtoupper(md5($this->assemble_oa($params))).$token));
	}

	public function assemble_oa($params)
	{
		if(!is_array($params))  return null;
		ksort($params, SORT_STRING);
		$sign = '';
		foreach($params AS $key=>$val){
			if(is_null($val))   continue;
			if(is_bool($val))   $val = ($val) ? 1 : 0;
			$sign .= $key . (is_array($val) ? assemble_oa($val) : $val);
		}
		return $sign;
	}
	//cus lee 2013-12-9 oa自动开通方法 end

	//cus lee 2013-12-9 sms短信自动开通方法 start
	public function sms_open($params_arr){
		//echo "<pre>";print_r($params_arr);exit;


		$url = 'http://webapi.sms.shopex.cn'; //正式环境
		$certi_app = 'sms.revenue_order'; //api名称
		$params = array();
		//系统级应用参数
		$params['certi_app'] = $certi_app;//必填，API名称

$sql = "SELECT
s.salespay_pay_date,
o.order_number,
od.order_d_product_basic_primecost,
od.order_d_product_basic_disc,
od.order_d_product_basic_code,
a.account_account_shopexid,
a.account_email
FROM tc_order_d od
left join tc_order o on o.order_id = od.order_d_order_id
left join tc_account a on a.account_id = o.order_account
left join tc_salespay s on s.salespay_order_id = o.order_id
where od.order_d_id = '".$params_arr['order_d_id']."'";

		$res = $this->db->query($sql)->result_array();
		$order_number = $res[0]["order_number"]; //订单号
		$salespay_pay_date = strtotime($res[0]["salespay_pay_date"]); //订单支付时间(时间戳)
		$product_basic_primecost = $res[0]["order_d_product_basic_primecost"]; //产品成本
		$product_basic_disc = $res[0]["order_d_product_basic_disc"]; //产品成交价
		$shopexid = $res[0]["account_account_shopexid"]; //客户的商业ID
		$product_basic_code = $res[0]["order_d_product_basic_code"]; //产品编号


		//接口参数
		$params['revenue_orderno'] = $order_number;// / 必填，营收平台订单号
		//$params['entid'] = 10000;// 测试默认给10000  必填，shopexid
		$params['entid'] = $shopexid;// 正式环境传正常的商业ID  必填，shopexid
		$params['sms_num'] = $params_arr['sm_amount'];//  /必填，短信条数
		$params['effect_starttime'] = strtotime($params_arr['start_date']);// /必填，短信开始时间（时间戳）
		$params['effect_endtime'] = strtotime($params_arr['end_date']);//  /必填，短信过期时间（时间戳）
		$params['order_paytime'] = $salespay_pay_date;//  /必填，订单支付时间
		$params['order_trade_no'] = 0; //  /必填，交易号
		$params['order_amount'] = $product_basic_disc;//   /必填，订单金额（精确到小数点后2位） //这里就写产品的成交价吧
		$params['order_cpcode'] = $product_basic_code;//   /必填，productid或者营收平台的编码均可
		$params['order_cost'] = $product_basic_primecost;//       /必填，订单成本 //这里就写产品的成本价
		//echo "<pre>";print_r($params);exit;

		$snoopy = new snoopy();
		$snoopy->submit($url,$params);
		$res = $snoopy->results;
		//echo $res;
		$json_arr = json_decode($res,true);
		//做个记录
		$toppath = $_SERVER["DOCUMENT_ROOT"] . "lee_text/applyopen_" . date('Y-m-d', time()) . "_sms.txt";
		$Ts = fopen($toppath, "a+");
		fputs($Ts, json_encode($params)."\r\n".$res."\r\n");
		fclose($Ts);

		//echo "<pre>";print_r($json_arr);echo "</pre>";exit;
		if($json_arr['res']=='succ'){
			$params_all['open']=$params_arr;
			$params_all['return']=$params_arr;
			//实际开通日期
			if (isset($params['effect_starttime'])){
				$params_all['return']['actual_startdate']=$params_arr['start_date']; //添加实际开通日期到返回参数中
			}
			//实际结束日期
			if (isset($params['effect_endtime'])){
				$params_all['return']['actual_enddate']=$params_arr['end_date']; //添加实际结束日期到返回参数中
			}
			$this->open_is_ok($params_all,$params_arr['order_d_id']);
			$return_arr= array(
				"res"=>true,
				"msg"=>"开通成功",
			);
			return $return_arr;
		}
		if($json_arr['res']=='fail'){
			//echo $json_arr['info'];
			$return_arr= array(
				"res"=>false,
				"msg"=>$json_arr["msg"].":".$json_arr["info"],
			);
			return $return_arr;
		}
		//exit;

	}
	//cus lee 2013-12-9 sms短信自动开通方法 end

	//cus lee 2013-12-8 saas自动开通方法 start
	public function saas_open($params_arr){
		//echo "<pre>";print_r($params_arr);exit;
		$url_home = "http://open.sradar.cn/index.php/Competence/"; //正式环境
		//$url_home = "http://open.229.cn/index.php/Competence/"; //测试环境
		//根据开通天数计算出开始时间和结束时间
		$actual_startdate = time(); //开始时间
		$actual_enddate = time()+$params_arr['time_limit']*24*60*60; //结束时间
		//echo "开始时间：".$actual_startdate."-结束时间：".$actual_enddate;exit;
$sql = "SELECT
u.user_name,
d.department_name,
od.order_d_product_basic_primecost,
od.order_d_product_basic_disc,
a.account_account_shopexid,
a.account_email
FROM tc_order_d od
left join tc_order o on o.order_id = od.order_d_order_id
left join tc_account a on a.account_id = o.order_account
left join tc_user u on u.user_id = o.order_owner
left join tc_department d on d.department_id = o.order_department
where od.order_d_id = '".$params_arr['order_d_id']."'";
		//p($sql);exit;
		$res = $this->db->query($sql)->result_array();
		//p($res);exit;
		$fee = $res[0]["order_d_product_basic_primecost"]; //订单原价
		$total_pay_fee = $res[0]["order_d_product_basic_disc"]; //订单实付
		$shopex_id = $res[0]["account_account_shopexid"]; //客户的商业ID
		$email = $res[0]["account_account_shopexid"]; //客户的商业ID
		$user_name = $res[0]["user_name"]; //订单的销售
		$department_name = $res[0]["department_name"]; //订单的部门
		$business_source = $department_name."|".$user_name;
		//exit;
		//print_r($sd_info);exit;

		$url = $url_home.'index?shopex_id='.$shopex_id.'&product_id='.$params_arr['saas_no'].'&business_source='.$business_source.'&time_limit='.$params_arr['time_limit'].'&username=admin&password=admin123';
		if (isset($params_arr["sub_acc_num"])){
			$url=$url."&sub_acc_num=".$params_arr["sub_acc_num"];
		}
		//exit;
		$snoopy = new snoopy();
		$snoopy->submit($url,"");
		$res = $snoopy->results;
		//echo $res;
		$json_arr = json_decode($res,true);
		//做个记录
		$toppath = $_SERVER["DOCUMENT_ROOT"] . "lee_text/applyopen_" . date('Y-m-d', time()) . "_saas.txt";
		$Ts = fopen($toppath, "a+");
		fputs($Ts, $url."\r\n".$res."\r\n");
		fclose($Ts);
		if ($json_arr["statusCode"] == 1){ //接口成功
			$params_all['open']=$params_arr;
			$params_all['return']=$params_arr;
			//实际开通日期
			if (isset($actual_startdate)){
				$params_all['return']['actual_startdate']=date('Y-m-d',$actual_startdate); //添加实际开通日期到返回参数中
			}
			//实际结束日期
			if (isset($actual_enddate)){
				$params_all['return']['actual_enddate']=date('Y-m-d',$actual_enddate); //添加实际结束日期到返回参数中
			}
			$this->open_is_ok($params_all,$params_arr['order_d_id']);
			$return_arr= array(
				"res"=>true,
				"msg"=>"开通成功",
			);
			return $return_arr;
		}else{
			$return_arr= array(
				"res"=>false,
				"msg"=>$json_arr["statusCode"].":".$json_arr["msg"],
			);
			//echo "<pre>";print_r($params_arr);exit;
			//echo $return_arr['msg'];exit;
			return $return_arr;
		}
	}
	//cus lee 2013-12-8 saas自动开通方法 end

	//cus lee 2013-12-8 taoex自动开通方法 start
	public function taoex_open($params_arr){
		//echo "<pre>";print_r($params_arr);echo "<pre>";exit;
		$url = 'http://api.saas.taoex.com/api.php';	//线上正式环境
		//$url = 'http://api.sandbox.saas.taoex.com/api.php'; //沙箱环境
		$app_key = 'pbi';
		$app_secret = '55A810D1EC55422FBB307538040D6850';
		//p($params_arr);
$sql = "SELECT
od.order_d_product_basic_primecost,
od.order_d_product_basic_disc,
a.account_account_shopexid,
a.account_email
FROM tc_order_d od
left join tc_order o on o.order_id = od.order_d_order_id
left join tc_account a on a.account_id = o.order_account
where od.order_d_id = '".$params_arr['order_d_id']."'";
		//p($sql);exit;
		$res = $this->db->query($sql)->result_array();
		//p($res);exit;
		$fee = $res[0]["order_d_product_basic_primecost"]; //订单原价
		$total_pay_fee = $res[0]["order_d_product_basic_disc"]; //订单实付
		$shopex_id = $res[0]["account_account_shopexid"]; //客户的商业ID
		$email = $res[0]["account_email"]; //客户的邮箱
		//echo $shopex_id;
		//echo $this->api->lee_getCustomerEmail($shopex_id);
		//exit;


		$params = array();
		//系统级应用参数
		$params['app_key'] = $app_key;
		$params['method'] = 'order.add';
		$params['format'] = 'json';
		//接口参数
		$params['tenant_type'] = 0; //租户类型	0: 全网租户 / 1: 淘宝租户	是

		//如果填写了开通邮箱，则直接用开通邮箱作为用户账号
		if ($params_arr['open_email']=="" or !isset($params_arr['open_email'])){
			//cus lee 2013-12-25 这里不用账号了修改成获取邮箱，因为如果账号是电话会报错 start
			//用户账号得用shopexid调用接口来查询
			if ($email==""){
				//这里需要通过用户中心接口在查询对应的email
				$params['username'] = $this->api->lee_getCustomerEmail($shopex_id);
			}else{
				$params['username'] = $email;
			}
			//cus lee 2013-12-25 这里不用账号了修改成获取邮箱，因为如果账号是电话会报错 end
			$params_arr['open_email'] = $params['username'];
		}else{
			$params['username'] = $params_arr['open_email'];
		}

		//$params['username'] = 'nick@gmail.com'; //	租户标识 全网租户为 email
		$params['source_type'] = 0; //应用开通来源类型	0:外部开通 / 1:淘宝箱开通	是
		$params['service_code'] = $params_arr['service_code']; //应用服务代码
		if (isset($params_arr['version_code'])){
			$params['version_code'] = $params_arr['version_code']; //应用版本代码
		}
		$params['host_name'] = $params_arr['host_name']; //应用租户主机名
		$params['order_cycle_start'] = $params_arr['start_date']; //订购服务起始日期
		$params['order_cycle_end'] = $params_arr['end_date']; //订购服务失效日期
		$params['fee'] = $fee; //	订单原价		否
		//$params['prom_fee'] = 0; //	订单优惠		否
		$params['total_pay_fee'] = $total_pay_fee; //	订单实付		否
		$params['memo'] = $params_arr['note']; //	备注文字		否
		//$params['memo'] = '营收测试'; //	备注文字		否
		//$params['is_rds'] = 0; //	是否为聚石塔	0/1	否
		//$params['resource'] = 0; //	聚石塔资源	ins_xxxxxxxx	否
		
		

		ksort($params);
		$query = '';
		foreach($params as $k=>$v){
			if ($k != 'sign') {
				$query .= $k.$v;
			}
		}
		$sign = strtoupper(md5($query.$app_secret));
		$params['sign']=$sign;
		//p($params);exit;

		$snoopy = new snoopy();
		$snoopy->submit($url,$params);
		$res = $snoopy->results;
		$json_arr = json_decode($res,true);
		//p($json_arr);//exit;
		//做个记录
		$toppath = $_SERVER["DOCUMENT_ROOT"] . "lee_text/applyopen_" . date('Y-m-d', time()) . "_taoex.txt";
		$Ts = fopen($toppath, "a+");
		fputs($Ts, json_encode($params)."\r\n".$res."\r\n");
		fclose($Ts);
		//echo $json_arr['error_response']['code'];exit;
		if (isset($json_arr['order_add_response']['success'])){
			if($json_arr['order_add_response']['success']==true){
				$params_all['open']=$params_arr;
				$params_all['return']=$json_arr['order_add_response']['data'];
				//开通成功
				//成功后需要的处理
				//添加返回参数
				if (isset($json_arr['order_add_response']['data']['server_name'])){
					$params_all['return']['domain']=$json_arr['order_add_response']['data']['server_name']; //添加一个站点地址到返回参数中
				}
				if (isset($json_arr['order_add_response']['data']['init_passwd'])){
					$params_all['return']['password_id']=$json_arr['order_add_response']['data']['init_passwd']; //添加用户密码到返回参数中
				}
				if (isset($json_arr['order_add_response']['data']['order_type'])){
					$params_all['return']['order_type']=$json_arr['order_add_response']['data']['order_type']; //添加订单类型到返回参数中
				}
				//实际开通日期
				if (isset($json_arr['order_add_response']['data']['order_cycle_start'])){
					$params_all['return']['actual_startdate']=$json_arr['order_add_response']['data']['order_cycle_start']; //添加实际开通日期到返回参数中
				}
				//实际结束日期
				if (isset($json_arr['order_add_response']['data']['order_cycle_end'])){
					$params_all['return']['actual_enddate']=$json_arr['order_add_response']['data']['order_cycle_end']; //添加实际结束日期到返回参数中
				}
				//用户名
				if (isset($json_arr['order_add_response']['data']['server_name'])){
					$params_all['return']['user']=$json_arr['order_add_response']['data']['server_name']; //添加用户名到返回参数中
				}

				$this->open_is_ok($params_all,$params_arr['order_d_id']);

				$return_arr= array(
					"res"=>true,
					"msg"=>$json_arr,
				);
				return $return_arr;
			}
		}else{
			//报错
			//echo "<pre>";print_r($params);print_r($json_arr);echo "</pre>";exit;
			$return_arr= array(
				"res"=>false,
				"msg"=>$json_arr['error_response']['code'],
			);
			//p($return_arr);exit;
			return $return_arr;
		}
    }
	//cus lee 2013-12-8 taoex自动开通方法 end

	//cus lee 2013-12-8 开通后执行的通用步骤 start
	public function open_is_ok($params_arr,$order_d_id){
		//p($params_arr);exit;
		//cus lee 2014-06-20 开通参数、返回参数回写 start
		$open_startdate = $params_arr['return']['actual_startdate'];//开通开始时间
		$open_enddate = $params_arr['return']['actual_enddate'];//开通结束时间
		$upsql = "update tc_order_d set order_d_open_startdate='".$open_startdate."',order_d_open_enddate='".$open_enddate."' where order_d_id = '".$order_d_id."'";
		$this->db->query($upsql);
		//cus lee 2014-06-20 开通参数、返回参数回写 end
		
		//获取这个产品明细对应的参数信息
		$params_info = $this->order_d_id_params_info($order_d_id);
		//p($params_info['open']);
		//循环开通参数。然后把值全部写进去
		if($params_arr['open']!=""){
			$note = $params_arr['open']['note'];
			foreach($params_info['open'] as $k=>$v){
				if(isset($params_arr['open'][$v['params_code']])){
					$params_o[$v['params_code']]=$params_arr['open'][$v['params_code']];
				}
			}
			$data['applyopen_open_info']=json_encode($params_o);
		}else{
			$note = "";
		}
		//p($params_info['return']);
		//exit;
		//p($params_arr);
		if($params_arr['return']!=""){
			$params_r="";
			foreach($params_info['return'] as $k=>$v){
				//首先判断这个是否默认填写了
				if(isset($params_arr['return'][$v['params_code']])){
					$params_r[$v['params_code']]=$params_arr['return'][$v['params_code']];
				}
			}
			//{"actual_startdate":"2014-11-24","actual_enddate":"2080-11-24","domain":"\u65e0","note":""}
			$data['applyopen_return_info']=json_encode($params_r);
		}
		//p($params_o);
		//p($params_r);
		//写入开通参数和返回参数信息
		/*
		$data=array(
			'applyopen_open_info'=>json_encode($params_o),
			'applyopen_return_info'=>json_encode($params_r),
		);
		*/
		$this->applyopen->update($order_d_id,$data);

		//接下来把订单明细的状态改成已开通
		$order_d_data = array(
			 "order_d_product_basic_style"=>1003,
		);
		$this->order_d->update($order_d_data,$order_d_id);

		//把备注单独拿出来写日志
		$log_data = array(
			'system_log_addtime'=>date('Y-m-d H:i:s'),
			'system_log_user_id'=>$this->session->userdata('user_id'),
			'system_log_operation'=>'确认开通',
			'system_log_note'=>$note,
			'system_log_module'=>'order_d',
			'system_log_module_id'=>$order_d_id,
		);
		$this->system_log->add($log_data);
	}
	//cus lee 2013-12-8 开通后执行的通用步骤 end


	//cus lee 2014-06-20 历史数据初始化，处理所有订单明细，将返回参数总的开始时间，结束时间 回写到明细中 start
	//applyopen/init_open_date
	public function init_open_date(){
		//首先查询出所有的已开通的订单明细以及对应的参数
		$sql = "
select 
od.order_d_id,
o.order_number,
o.order_id,
ao.applyopen_create_time,
ao.applyopen_open_info,
ao.applyopen_return_info
from tc_order_d od
left join tc_applyopen ao on od.order_d_id = ao.applyopen_order_d_id
left join tc_order o on od.order_d_order_id = o.order_id
where od.order_d_product_basic_style = 1003
		";
		$res = $this->db->query($sql)->result_array();
		foreach ($res as $v){
			$order_d_id = $v['order_d_id']; //订单明细ID
			$order_number = $v['order_number']; //订单编号
			$applyopen_create_time = $v['applyopen_create_time'];//开通创建时间
			if($v['applyopen_open_info']!=""){
				$applyopen_open_info = json_decode($v['applyopen_open_info'],true);//开通参数
			}else{
				$applyopen_open_info = "";
			}
			if($v['applyopen_return_info']!=""){
				$applyopen_return_info = json_decode($v['applyopen_return_info'],true);//返回数
			}else{
				$applyopen_return_info = "";
			}
			//p($applyopen_open_info);
			//p($applyopen_return_info);
			//exit;
			
			//这里是把返回参数的时间补齐
			if(!isset($applyopen_return_info['actual_enddate'])){
				p("订单编号".$order_number);
				//当这个产品为sms开通类型的
				if(isset($applyopen_open_info['issue_type'])){
					if($applyopen_open_info['issue_type'] == "sms"){
						//添加
						echo "开通参数";
						p($applyopen_open_info);
						//echo "返回参数";
						//p($applyopen_return_info);
						//把开通参数的时间补齐到返回参数中
						if(isset($applyopen_open_info['start_date']) and isset($applyopen_open_info['end_date'])){
							$applyopen_return_info['actual_startdate'] = $applyopen_open_info['start_date'];
							$applyopen_return_info['actual_enddate'] = $applyopen_open_info['end_date'];
							//修改参数
							echo "修改后的返回参数";
							p($applyopen_return_info);
						}

					}
					if($applyopen_open_info['issue_type'] == "saas"){
						//添加
						echo "开通参数";
						p($applyopen_open_info);
						//echo "返回参数";
						//p($applyopen_return_info);
						//把开通参数的时间补齐到返回参数中
						$actual_startdate = date("Y-m-d",strtotime($applyopen_create_time)); //开始时间
						$actual_enddate = date("Y-m-d",strtotime($applyopen_create_time)+$applyopen_open_info['time_limit']*24*60*60); //结束时间
						$applyopen_return_info['actual_startdate'] = $actual_startdate;
						$applyopen_return_info['actual_enddate'] = $actual_enddate;
						//修改参数
						echo "修改后的返回参数";
						p($applyopen_return_info);
					}
					if ($applyopen_open_info['issue_type']=='oaekd' or $applyopen_open_info['issue_type']=='oabqkz' or $applyopen_open_info['issue_type']=='oadrp' or $applyopen_open_info['issue_type']=='oawxd'){
						//添加
						echo "开通参数";
						p($applyopen_open_info);
						//echo "返回参数";
						//p($applyopen_return_info);
						//把开通参数的时间补齐到返回参数中
						if(isset($applyopen_open_info['start_date']) and isset($applyopen_open_info['end_date'])){
							$applyopen_return_info['actual_startdate'] = $applyopen_open_info['start_date'];
							$applyopen_return_info['actual_enddate'] = $applyopen_open_info['end_date'];
							//修改参数
							echo "修改后的返回参数";
							p($applyopen_return_info);
						}
					}
					if($applyopen_open_info['issue_type'] == "taoex"){
						//添加
						echo "开通参数";
						p($applyopen_open_info);
						//echo "返回参数";
						//p($applyopen_return_info);
						//如果开通参数里有开始时间结束时间，把开通参数的时间补齐到返回参数中
						if(isset($applyopen_open_info['start_date']) and isset($applyopen_open_info['end_date'])){
							$applyopen_return_info['actual_startdate'] = $applyopen_open_info['start_date'];
							$applyopen_return_info['actual_enddate'] = $applyopen_open_info['end_date'];
							//修改参数
							echo "修改后的返回参数";
							p($applyopen_return_info);
						}
					}
					
					//最后将内容回写到返回参数
					//$upsql = "update tc_applyopen set applyopen_return_info = '".json_encode($applyopen_return_info)."' where applyopen_order_d_id = '".$order_d_id."'";
					//$this->db->query($upsql);
					//p('返回参数时间添加完毕');
					
				}
			}
			
			/*
			//这里回写订单明细里的时间
			//p("订单编号".$order_number);
			if(isset($applyopen_return_info['actual_startdate'])){
				$upsql = "update tc_order_d set order_d_open_startdate = '".$applyopen_return_info['actual_startdate']."' where order_d_id = '".$order_d_id."'";
				$this->db->query($upsql);

			}
			if(isset($applyopen_return_info['actual_enddate'])){
				$upsql = "update tc_order_d set order_d_open_enddate = '".$applyopen_return_info['actual_enddate']."' where order_d_id = '".$order_d_id."'";
				$this->db->query($upsql);
			}
			echo "初始化完毕";
			*/
		}
	}
	//cus lee 2014-06-20 历史数据初始化，处理所有订单明细，将返回参数总的开始时间，结束时间 回写到明细中 end

}
?>
