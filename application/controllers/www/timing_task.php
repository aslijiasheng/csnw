<?php

class Timing_task extends WWW_Controller {

	public $menu1 = 'timing_task';

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('PRC');
		$this->output->enable_profiler(false);
		$this->db->query('set names utf8');
		$this->load->model('www/timing_task_model', 'timing_task');
	}

	public function exec() {
		set_time_limit(0);
		include_once("lib/snoopy.inc.php");

		while (1) {
		
		
			$tasks = $this->db->get('tc_timing_task')->result_array();
			$save['timing_task_do_time'] = date('Y-m-d H:i:s', time());
			foreach ($tasks as $k => $v) {
				if ($v['timing_task_status'] == '1001') {
					$snoopy = new snoopy();
					$snoopy->submit($v['timing_task_url'].'?task='.$v['timing_task_id']);
				}
			}

			error_log(print_r(array('Date' => date('Y-m-d H:i:s')), true) . "\n", 3, './logs/exec' . date("Ymd_h") . "log.log");
			// Sleep for 30 seconds
			sleep(30);
		}
	}

	public function index() {
		$this->timing_task->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['totalNumber'] = $this->timing_task->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"] = $this->timing_task->attributeLabels();
		$data['listLayout'] = $this->timing_task->listLayout();
		$this->render('www/timing_task/list', $data);
	}

	public function add() {
		$this->timing_task->db->query('SET NAMES UTF8');
		$data["labels"] = $this->timing_task->attributeLabels();

		$this->load->model('admin/enum_model', 'enum');
		$data["timing_task_status_enum"] = $this->enum->getlist(607);

		if (!empty($_POST)) {
			if (isset($_GET['type_id'])) {
				$_POST['timing_task']['type_id'] = $_GET['type_id'];
				$this->timing_task->add($_POST['timing_task']);
				success('www/timing_task?type_id=' . $_GET['type_id'], '创建成功');
			} else {
				$this->timing_task->add($_POST['timing_task']);
				success('www/timing_task', '创建成功');
			}
		}
		$this->render('www/timing_task/add', $data);
	}

	public function update() {
		$this->timing_task->db->query('SET NAMES UTF8');
		$data["labels"] = $this->timing_task->attributeLabels();
		$data['id_aData'] = $this->timing_task->id_aGetInfo($_GET['timing_task_id']);
		$data['listLayout'] = $this->timing_task->listLayout();

		$this->load->model('admin/enum_model', 'enum');
		$data["timing_task_status_enum"] = $this->enum->getlist(607);

		if (!empty($_POST)) {
			if (isset($_GET['type_id'])) {
				$_POST['timing_task']['type_id'] = $_GET['type_id'];
				$this->timing_task->update($_POST['timing_task'], $_GET['timing_task_id']);
				success('www/timing_task?type_id=' . $_GET['type_id'], '更新成功');
			} else {
				$this->timing_task->update($_POST['timing_task'], $_GET['timing_task_id']);
				success('www/timing_task', '更新成功');
			}
		}
		$this->render('www/timing_task/update', $data);
	}

	public function ajax_select() {
		if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
			$select_arr = json_decode($_POST["select_json"], true);
			$like["timing_task_" . $select_arr['attr']] = $select_arr['value'];
		} else {
			//$select_arr = "";
			$like = "";
		}
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$where = "";
		if (isset($_GET['type_id'])) {
			if ($_GET['type_id'] != "" and $_GET['type_id'] != 0) {
				$where['type_id'] = $_GET['type_id'];
			}
		}

		$data['listData'] = $this->timing_task->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
		$data["labels"] = $this->timing_task->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取

		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['totalNumber'] = $this->timing_task->countGetInfo($where, $like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/timing_task/ajax_select', $data);
	}

	public function ajax_select_quote() {
		if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
			$select_arr = json_decode($_POST["select_json"], true);
			$like["timing_task_" . $select_arr['attr']] = $select_arr['value'];
		} else {
			//$select_arr = "";
			$like = "";
		}
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$where = "";

		$data['listData'] = $this->timing_task->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
		$data["labels"] = $this->timing_task->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取

		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['totalNumber'] = $this->timing_task->countGetInfo($where, $like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/timing_task/ajax_select_quote', $data);
	}

	public function ajax_list() {
		$this->timing_task->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['listData'] = $this->timing_task->listGetInfo('', $_GET['page'], $_GET['perNumber']);
		$data['totalNumber'] = $this->timing_task->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"] = $this->timing_task->attributeLabels();
		$data['listLayout'] = $this->timing_task->listLayout();
		$this->output->enable_profiler(false);
		$this->load->view('www/timing_task/ajax_list', $data);
	}

	public function del() {
		$this->timing_task->del($_GET['timing_task_id']);
		success('www/timing_task', '删除成功');
	}

	// 状态
	public function do_status() {
		if ($_GET['timing_task_id']) {
			$task = $this->timing_task->id_aGetInfo($_GET['timing_task_id']);
			$data['timing_task_status'] = $task['timing_task_status'] == '1001' ? '1002' : '1001';
			$this->timing_task->update($data, $_GET['timing_task_id']);
			header("Location:" . site_url('www/timing_task'));
		}
	}

}

?>