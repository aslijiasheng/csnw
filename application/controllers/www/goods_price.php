<?php
class Goods_price extends WWW_Controller{
	public $menu1='goods_price';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/goods_price_model','goods_price');
	}

	public function index(){
		$this->goods_price->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->goods_price->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->goods_price->attributeLabels();
		$data['listLayout']=$this->goods_price->listLayout();
		$this->render('www/goods_price/list',$data);
	}
	public function add(){
		$this->goods_price->db->query('SET NAMES UTF8');
		$data["labels"]=$this->goods_price->attributeLabels();
		
		
		
		
		
		
		$this->load->model('admin/enum_model','enum');$data["goods_price_visable_enum"]=$this->enum->getlist(414);
		
		
		
		
		
		
		$this->load->model('admin/enum_model','enum');$data["goods_price_cycle_unit_enum"]=$this->enum->getlist(417);
		
		
		
		
		
		if(!empty($_POST)){
		if(isset($_GET['type_id'])){
           $_POST['goods_price']['type_id']=$_GET['type_id'];
           $this->goods_price->add($_POST['goods_price']);
		    success('www/goods_price?type_id='.$_GET['type_id'],'创建成功');
		}else{
		  $this->goods_price->add($_POST['goods_price']);
		  success('www/goods_price','创建成功');
		}
			
		}
		$this->render('www/goods_price/add',$data);
	}
	public function update(){
		$this->goods_price->db->query('SET NAMES UTF8');
		$data["labels"]=$this->goods_price->attributeLabels();
		$data['id_aData']=$this->goods_price->id_aGetInfo($_GET['goods_price_id']);
		$data['listLayout']=$this->goods_price->listLayout();
		
		
		
		
		
		
		$this->load->model('admin/enum_model','enum');
		$data["goods_price_visable_enum"]=$this->enum->getlist(414);
		
		
		
		
		
		
		$this->load->model('admin/enum_model','enum');
		$data["goods_price_cycle_unit_enum"]=$this->enum->getlist(417);
		
		
		
		
		
		if(!empty($_POST)){
		    if(isset($_GET['type_id'])){
		      $_POST['goods_price']['type_id']=$_GET['type_id'];
		      $this->goods_price->update($_POST['goods_price'],$_GET['goods_price_id']);
			    success('www/goods_price?type_id='.$_GET['type_id'],'更新成功');
		    }else{
              	$this->goods_price->update($_POST['goods_price'],$_GET['goods_price_id']);
			    success('www/goods_price','更新成功');
		    }
		}
		$this->render('www/goods_price/update',$data);
	}

}
?>