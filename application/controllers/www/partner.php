<?php

class Partner extends WWW_Controller {

	public $menu1 = 'partner';

	public function __construct() {
		parent::__construct();
		$this->load->model('www/partner_model', 'partner');
	}

	public function index() {
		$this->partner->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['totalNumber'] = $this->partner->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"] = $this->partner->attributeLabels();
		$data['listLayout'] = $this->partner->listLayout();
		$this->render('www/partner/list', $data);
	}

	public function ajax_select() {
		if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
			$select_arr = json_decode($_POST["select_json"], true);
			$like["partner_" . $select_arr['attr']] = $select_arr['value'];
		} else {
			//$select_arr = "";
			$like = "";
		}
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$where = "";
		//if (isset($_GET['type_id'])){
		//if($_GET['type_id']!="" and $_GET['type_id']!=0){
		//$where['type_id']=$_GET['type_id'];
		//}
		//}

		$data['listData'] = $this->partner->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
		$data["labels"] = $this->partner->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['totalNumber'] = $this->partner->countGetInfo($where, $like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/partner/ajax_select', $data);
	}

	public function add() {
		$this->partner->db->query('SET NAMES UTF8');
		$data["labels"] = $this->partner->attributeLabels();
		$this->load->model('admin/enum_model', 'enum');
		$data["partner_partner_level_enum"] = $this->enum->getlist(102);
		if (!empty($_POST)) {
			if (isset($_GET['type_id'])) {
				$_POST['partner']['type_id'] = $_GET['type_id'];
				$this->partner->add($_POST['partner']);
				success('www/partner?type_id=' . $_GET['type_id'], '创建成功');
			} else {
				$this->partner->add($_POST['partner']);
				success('www/partner', '创建成功');
			}
		}
		$this->render('www/partner/add', $data);
	}

	public function update() {
		$this->partner->db->query('SET NAMES UTF8');
		$data["labels"] = $this->partner->attributeLabels();
		$data['id_aData'] = $this->partner->id_aGetInfo($_GET['partner_id']);
		$data['listLayout'] = $this->partner->listLayout();
		$this->load->model('admin/enum_model', 'enum');
		$data["partner_partner_level_enum"] = $this->enum->getlist(102);
		if (!empty($_POST)) {
			if (isset($_GET['type_id'])) {
				$_POST['partner']['type_id'] = $_GET['type_id'];
				$this->partner->update($_POST['partner'], $_GET['partner_id']);
				success('www/partner?type_id=' . $_GET['type_id'], '更新成功');
			} else {
				$this->partner->update($_POST['partner'], $_GET['partner_id']);
				success('www/partner', '更新成功');
			}
		}
		$this->render('www/partner/update', $data);
	}

	public function del() {
		$this->partner->del($_GET['partner_id']);
		success('www/partner', '删除成功');
	}

	public function view() {
		$this->partner->db->query('SET NAMES UTF8');
		$data["labels"] = $this->partner->attributeLabels();
		$data['listLayout'] = $this->partner->id_aGetInfo($_GET['partner_id']);
		$this->render('www/partner/view', $data);
	}

	public function ajax_list() {
		$this->user->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['listData'] = $this->partner->listGetInfo('', $_GET['page'], $_GET['perNumber']);
		$data['totalNumber'] = $this->partner->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"] = $this->partner->attributeLabels();
		$data['listLayout'] = $this->partner->listLayout();
		$this->output->enable_profiler(false);
		$this->load->view('www/partner/ajax_list', $data);
	}

	public function ajax_select_quote() {
//		p($_POST);exit;
		if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
			$select_arr = json_decode($_POST["select_json"], true);
			$like["partner_" . $select_arr['attr']] = $select_arr['value'];
		} else {
			//$select_arr = "";
			$like = "";
		}
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$where = "";

		$data['listData'] = $this->partner->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
		$data["labels"] = $this->partner->attributeLabels();

//		$data['listData'] = $this->partner->listGetInfo($rel_role_id, $like);
//		$data['totalNumber'] = $this->partner->countGetInfo; //数据总数

//		if (isset($_POST['rel_role_id'])) {
//			//有post表示有查询条件
//			$rel_role_id = $_POST['rel_role_id'];
//			$data['listData'] = $this->partner->roleGetInfo($rel_role_id, $like);
//			$data['totalNumber'] = $this->partner->countRole($rel_role_id, $like); //数据总数
//		} else {
//			$data['listData'] = $this->partner->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
//			$data['totalNumber'] = $this->partner->countGetInfo($where, $like); //数据总数
//			//p($data['listData']);
//		}
//		die('1');

//		$data["labels"] = $this->partner->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['totalNumber'] = $this->partner->countGetInfo($where, $like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/partner/ajax_select_quote', $data);

//		if (!isset($_GET['perNumber'])) {
//			$_GET['perNumber'] = 10;
//		}
//		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
//		//p($data);
//		$this->output->enable_profiler(false);
//		$this->load->view('www/partner/ajax_select_quote', $data);
	}

}

?>