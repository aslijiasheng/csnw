<?php

class Bgrelation extends WWW_Controller {

	public $menu1 = 'bgrelation';

	public function __construct() {
		parent::__construct();
		$this->load->model('www/bgrelation_model', 'bgrelation');
	}

	public function index() {
		$this->bgrelation->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['totalNumber'] = $this->bgrelation->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"] = $this->bgrelation->attributeLabels();
		$data['listLayout'] = $this->bgrelation->listLayout();
		$this->render('www/bgrelation/list', $data);
	}

	public function add() {
		$this->bgrelation->db->query('SET NAMES UTF8');
		$data["labels"] = $this->bgrelation->attributeLabels();

		if (!empty($_POST)) {
			if (isset($_GET['type_id'])) {
				$_POST['bgrelation']['type_id'] = $_GET['type_id'];
				$this->bgrelation->add($_POST['bgrelation']);
				success('www/bgrelation?type_id=' . $_GET['type_id'], '创建成功');
			} else {
				$this->bgrelation->add($_POST['bgrelation']);
				success('www/bgrelation', '创建成功');
			}
		}
		$this->render('www/bgrelation/add', $data);
	}

	public function update() {
		$this->bgrelation->db->query('SET NAMES UTF8');
		$data["labels"] = $this->bgrelation->attributeLabels();
		$data['id_aData'] = $this->bgrelation->id_aGetInfo($_GET['bgrelation_id']);
		$data['listLayout'] = $this->bgrelation->listLayout();

		if (!empty($_POST)) {
			if (isset($_GET['type_id'])) {
				$_POST['bgrelation']['type_id'] = $_GET['type_id'];
				$this->bgrelation->update($_POST['bgrelation'], $_GET['bgrelation_id']);
				success('www/bgrelation?type_id=' . $_GET['type_id'], '更新成功');
			} else {
				$this->bgrelation->update($_POST['bgrelation'], $_GET['bgrelation_id']);
				success('www/bgrelation', '更新成功');
			}
		}
		$this->render('www/bgrelation/update', $data);
	}

}

?>