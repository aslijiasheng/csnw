<?php
class Order_record extends WWW_Controller{
	public $menu1='order_record';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/order_record_model','order_record');
	}

	public function index(){
		$this->order_record->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->order_record->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->order_record->attributeLabels();
		$data['listLayout']=$this->order_record->listLayout();
		$this->render('www/order_record/list',$data);
	}
	public function add(){
		$this->order_record->db->query('SET NAMES UTF8');
		$data["labels"]=$this->order_record->attributeLabels();
		
		
		
		
		
		
		$this->load->model('admin/enum_model','enum');$data["order_record_operation_enum"]=$this->enum->getlist(463);
		
		
		
		
		
		if(!empty($_POST)){
		if(isset($_GET['type_id'])){
           $_POST['order_record']['type_id']=$_GET['type_id'];
           $this->order_record->add($_POST['order_record']);
		    success('www/order_record?type_id='.$_GET['type_id'],'创建成功');
		}else{
		  $this->order_record->add($_POST['order_record']);
		  success('www/order_record','创建成功');
		}
			
		}
		$this->render('www/order_record/add',$data);
	}
	public function update(){
		$this->order_record->db->query('SET NAMES UTF8');
		$data["labels"]=$this->order_record->attributeLabels();
		$data['id_aData']=$this->order_record->id_aGetInfo($_GET['order_record_id']);
		$data['listLayout']=$this->order_record->listLayout();
		
		
		
		
		
		
		$this->load->model('admin/enum_model','enum');
		$data["order_record_operation_enum"]=$this->enum->getlist(463);
		
		
		
		
		
		if(!empty($_POST)){
		    if(isset($_GET['type_id'])){
		      $_POST['order_record']['type_id']=$_GET['type_id'];
		      $this->order_record->update($_POST['order_record'],$_GET['order_record_id']);
			    success('www/order_record?type_id='.$_GET['type_id'],'更新成功');
		    }else{
              	$this->order_record->update($_POST['order_record'],$_GET['order_record_id']);
			    success('www/order_record','更新成功');
		    }
		}
		$this->render('www/order_record/update',$data);
	}
    public function ajax_list(){
       $this->order_record->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['listData']=$this->order_record->listGetInfo('',$_GET['page'],$_GET['perNumber']);
		$data['totalNumber'] = $this->order_record->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->order_record->attributeLabels();
		$data['listLayout']=$this->order_record->listLayout();
		$this->output->enable_profiler(false);
		$this->load->view('www/order_record/ajax_list',$data);
    }
	public function ajax_select(){
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["order_record_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		if (isset($_GET['type_id'])){ 
			if($_GET['type_id']!="" and $_GET['type_id']!=0){
				$where['type_id']=$_GET['type_id']; 
			}
		}
		
		$data['listData']=$this->order_record->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);
		$data["labels"]=$this->order_record->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取
		
		

		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->order_record->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/order_record/ajax_select',$data);
	}
	public function ajax_select_quote(){
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["order_record_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		
		$data['listData']=$this->order_record->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);
		$data["labels"]=$this->order_record->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取
		
		

		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->order_record->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/order_record/ajax_select_quote',$data);
	}

}
?>