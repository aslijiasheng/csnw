<?php

if ( !defined( 'BASEPATH' ) )
    exit( 'No direct script access allowed' );

class Cancel extends WWW_Controller {

    public $menu1 = 'cancel';

    public function __construct() {
        parent::__construct();
        $this->load->model( 'www/cancel_model', 'cancel' );
        $this->load->model('www/message_model','message');
        $this->load->model('www/order_model','order');
        $this->load->model('www/system_log_model','system_log');
        $this->db->query( 'SET NAMES UTF8' );
    }

    //订单作废
    public function order_cancel() {
        if ( isset( $_GET['order_id'] ) ) {
            $this->load->model( 'www/order_model', 'order' );
			$this->load->model( 'www/invoice_model', 'invoice' );
            $data['cancel'] = $_GET['cancel'];
            $data['order_id'] = $_GET['order_id'];
			
			$where = array( 
							"invoice_order_id"    => $data['order_id'],
							"invoice_status !="   => "1007",
							);
			$count = $this->invoice->countGetInfo( $where );
			if ( $count>0 ) {
				echo $count;
				die;
			}
            $data["labels"] = $this->order->attributeLabels();
            $data["order_data"]=$this->order->id_aGetInfo( $data['order_id'] );
            //明细
            $where=array(
                'order_d_order_id'=>$data['order_id'],
            );
            $this->db->select()->from('tc_order_d')->where($where)->order_by('order_d_id')->group_by('order_d_goods_code');
            $res = $this->db->get()->result_array();
            foreach($res as $k=>$v){
                $data['listData'][$k] = $v;
                $data['listData'][$k]['order_d_order_primecost'] = 0;
                $data['listData'][$k]['order_d_order_disc'] = 0;
                $where=array(
                    'order_d_order_id'=>$_GET['order_id'],
                    'order_d_goods_code'=>$v['order_d_goods_code']
                );
                $res_p = $this->order_d->GetInfo($where);
                $data['listData'][$k]['product_arr'] = $res_p;
                foreach($res_p as $vv){
                    $data['listData'][$k]['order_d_order_primecost'] += $vv['order_d_product_basic_primecost']; // 原价
                    $data['listData'][$k]['order_d_order_disc'] += $vv['order_d_product_basic_disc']; // 折后价
                }
            }

            $this->load->view( 'www/cancel/order_cancel', $data );
        } else {
            echo "不能没有ID和Node";
        }
    }

    //销售作废流程操作
    public function ajax_order_cancel() {
        //1001默认订单 1002作废订单 1003主管审核 1004财务审核
        if ( $_POST['cancel'] ) {
            $pCancel = $_POST['cancel'];

            if ( empty( $pCancel['remark'] ) ) { //备注不能为空
                die( 'remark error' );
            }

            switch ( $pCancel['operation'] ) {
            case 'cancel': //订单作废
                $res_c = $this->cancel->isCancel( $pCancel['order_id'] );//该订单是否有 收退款、返点 记录
                if ( $res_c ) {
                    die( 'order error' );
                } else {
                    $this->cancel->orderCancel( $pCancel['order_id'], $pCancel['remark'], $state = 1003 ); //订单立即作废，待财务确认
                    $log_data = array(
                        'system_log_addtime'=>date('Y-m-d H:i:s',time()),
                        'system_log_user_id'=>$this->session->userdata('user_id'),
                        'system_log_operation' => '订单申请作废',
                        'system_log_module'=>'Order',
                        'system_log_module_id'=>$pCancel['order_id'],
                    );
                    $order_data = $this->order->id_aGetInfo($pCancel['order_id']);
                    $message_data = array(
                        'message_owner'=>$order_data['order_review'],
                        'message_module'=>'order',
                        'message_module_id'=>$pCancel['order_id'],
                        'message_type'=>1014,
                        'message_department'=>$order_data['order_review_arr']['user_department'],
                        'message_url'=>'www/cancel/cancel_check'
                    );
                    $this->message->add($message_data);
                    $mess_re = $this->db->select()->from('tc_message')->where(array('message_module_id'=>$pCancel['order_id'],'message_type'=>1016))->get()->result_array();

                    if(!empty($mess_re)){
                        $message_data = array(
                            'message_status'=>1002
                        );
                         $param['message_type'] = 1016;
                        $param['message_module_id'] = $pCancel['order_id'];
                        $this->message->update($message_data,$param);
                    }

                    die( '1' );
                }
                break;
            case 'change': //订单变更
                // if ( $this->cancel->isCancel( $pCancel['order_id'] ) ) die( 'order error' );
                $this->load->model( 'www/order_model', 'order' );
                if ( empty( $_POST['new_cancel']['cancel_order_number'] ) ) die( 'none order' );
                $res_order = $this->order->whereGetInfo( array( 'order_number' => $_POST['new_cancel']['cancel_order_number'] ) ); //检查订单编号
                if ( !$res_order ) {
                    die( 'none order' );
                }else {
                    //变更订单存入数据库 order_cancen_number，待财务确认后，订单变更
                    $res = $this->cancel->saveOrderModify( $_POST );

                    if ( $res ) {
                        $this->cancel->orderCancel( $_POST['cancel']['order_id'], $_POST['cancel']['remark'], $state = 1005 );
                        $log_data = array(
                            'system_log_addtime'=>date('Y-m-d H:i:s',time()),
                            'system_log_user_id'=>$this->session->userdata('user_id'),
                            'system_log_operation' => '订单申请变更',
                            'system_log_module'=>'Order',
                            'system_log_module_id'=>$pCancel['order_id'],
                        );
                        $order_data = $this->order->id_aGetInfo($pCancel['order_id']);
                        $message_data = array(
                            'message_owner'=>$order_data['order_review'],
                            'message_module'=>'order',
                            'message_module_id'=>$pCancel['order_id'],
                            'message_type'=>1018,
                            'message_department'=>$order_data['order_review_arr']['user_department'],
                            'message_url'=>'www/cancel/cancel_check'
                        );
                        $this->message->add($message_data);
                        $mess_re = $this->db->select()->from('tc_message')->where(array('message_module_id'=>$pCancel['order_id'],'message_type'=>1017))->get()->result_array();

                        if(!empty($mess_re)){
                            $message_data = array(
                                'message_status'=>1002
                            );
                            $param['message_type'] = 1017;
                            $param['message_module_id'] = $pCancel['order_id'];
                        $this->message->update($message_data,$param);
                    }
                        die( '1' );
                    } else
                        die( 'error' );
                }
                break;
            default:
                die( 'error' );
                break;
            }
        } else {
            die( 'error' );
        }
    }

    public function cancel_check() {
        if ( isset( $_GET['order_id'] ) ) {
            $data['record'] = array(); //备注记录表
            $data['order_id'] = $_GET['order_id'];
            $data['cancel'] = $_GET['cancel'];

            $this->load->model( 'www/order_model', 'order' );
            $data["labels"] = $this->order->attributeLabels();
            $data["order_data"]=$this->order->id_aGetInfo( $data['order_id'] );

            $this->load->model( 'www/order_record_model', 'record' );
            $where = array( 'cancel_order_id' => $data['order_id'] );
            $res_record = $this->record->GetInfo( $where );
            if ( $res_record )
                $data['record'] = $res_record;

            //明细
            $where=array(
                'order_d_order_id'=>$data['order_id'],
            );
            $this->db->select()->from('tc_order_d')->where($where)->order_by('order_d_id')->group_by('order_d_goods_code');
            $res = $this->db->get()->result_array();
            foreach($res as $k=>$v){
                $data['listData'][$k] = $v;
                $data['listData'][$k]['order_d_order_primecost'] = 0;
                $data['listData'][$k]['order_d_order_disc'] = 0;
                $where=array(
                    'order_d_order_id'=>$_GET['order_id'],
                    'order_d_goods_code'=>$v['order_d_goods_code']
                );
                $res_p = $this->order_d->GetInfo($where);
                $data['listData'][$k]['product_arr'] = $res_p;
                foreach($res_p as $vv){
                    $data['listData'][$k]['order_d_order_primecost'] += $vv['order_d_product_basic_primecost']; // 原价
                    $data['listData'][$k]['order_d_order_disc'] += $vv['order_d_product_basic_disc']; // 折后价
                }
            }

            $this->load->view( 'www/cancel/order_cancel_check', $data );
        } else {
            echo "不能没有ID和Node";
        }
    }
	
	/***
	*	主管审核作废确认
	*/
    public function ajax_cancel_check() {
		//p($_POST);die();
        if ( $_POST['cancel'] ) {
            $pCancel = $_POST['cancel'];
            if ( empty( $pCancel['remark'] ) ) {
                die( 'remark error' );
            }

            if ( isset( $pCancel['refuse'] ) ) {
                 $this->cancel->clearOrderModify($_POST);
                $this->cancel->orderCancel( $pCancel['order_id'], $pCancel['remark'], $state = 1001 );
                switch ( $pCancel['operation'] ) {
                 case 'cancel':
                 $log_data = array(
                        'system_log_addtime'=>date('Y-m-d H:i:s',time()),
                        'system_log_user_id'=>$this->session->userdata('user_id'),
                        'system_log_operation' => '订单审核作废',
                        'system_log_module'=>'Order',
                        'system_log_module_id'=>$pCancel['order_id'],
						'system_log_note' => '订单作废拒绝',
                    );
                    $order_data = $this->order->id_aGetInfo($pCancel['order_id']);
                    $message_data = array(
                        'message_status'=>1002
                    );
                    $param['message_type'] = 1014;
                    $param['message_module_id'] = $pCancel['order_id'];
                    //p($param);die;
                    $this->message->update($message_data,$param);
                    $message_data = array(
                        'message_owner'=>$order_data['order_typein'],
                        'message_module'=>'order',
                        'message_module_id'=>$pCancel['order_id'],
                        'message_type'=>1016,
                        //'message_department'=>$order_data['order_owner_arr']['user_department'],
                        'message_url'=>'www/cancel/order_cancel'
                    );
                    $this->system_log->add($log_data);
                    $this->message->add($message_data);
                die( '1' );
                break;
                case 'change':
                $log_data = array(
                        'system_log_addtime'=>date('Y-m-d H:i:s',time()),
                        'system_log_user_id'=>$this->session->userdata('user_id'),
                        'system_log_operation' => '订单审核变更',
                        'system_log_module'=>'Order',
                        'system_log_module_id'=>$pCancel['order_id'],
						'system_log_note' => '订单作废拒绝',
                    );
                    $order_data = $this->order->id_aGetInfo($pCancel['order_id']);
                    $message_data = array(
                        'message_status'=>1002
                    );
                    $param['message_type'] = 1018;
                    $param['message_module_id'] = $pCancel['order_id'];
                    $this->message->update($message_data,$param);
                    $message_data = array(
                        'message_owner'=>$order_data['order_typein'],
                        'message_module'=>'order',
                        'message_module_id'=>$pCancel['order_id'],
                        'message_type'=>1017,
                        //'message_department'=>$order_data['order_owner_arr']['user_department'],
                        'message_url'=>'www/cancel/order_cancel'
                    );
                    $this->system_log->add($log_data);
                    $this->message->add($message_data);
                    die('1');
                    break;
                default:
					die( '作废流程有问题' );
					break;
              }

            }

            switch ( $pCancel['operation'] ) {
				case 'cancel':
					$this->cancel->orderCancel( $pCancel['order_id'], $pCancel['remark'], $state = 1004 );
					 $log_data = array(
							'system_log_addtime'=>date('Y-m-d H:i:s',time()),
							'system_log_user_id'=>$this->session->userdata('user_id'),
							'system_log_operation' => '订单审核作废',
							'system_log_module'=>'Order',
							'system_log_module_id'=>$pCancel['order_id'],
							'system_log_note' => '订单作废通过',
						);
						$order_data = $this->order->id_aGetInfo($pCancel['order_id']);
						$message_data = array(
							'message_status'=>1002
						);
						$param['message_type'] = 1014;
						$param['message_module_id'] = $pCancel['order_id'];
						$this->message->update($message_data,$param);
						$message_data = array(
							'message_owner'=>$order_data['order_finance'],
							'message_module'=>'order',
							'message_module_id'=>$pCancel['order_id'],
							'message_type'=>1015,
							'message_department'=>$order_data['order_finance_arr']['user_department'],
							'message_url'=>'www/cancel/cancel_affirm'
						);
						$this->system_log->add($log_data);
						$this->message->add($message_data);
					die( '1' );
					break;
				case 'change':
					$this->cancel->orderCancel( $pCancel['order_id'], $pCancel['remark'], $state = 1006 );
					 $log_data = array(
							'system_log_addtime'=>date('Y-m-d H:i:s',time()),
							'system_log_user_id'=>$this->session->userdata('user_id'),
							'system_log_operation' => '订单审核变更',
							'system_log_module'=>'Order',
							'system_log_module_id'=>$pCancel['order_id'],
							'system_log_note' => '订单作废通过',
						);
						$order_data = $this->order->id_aGetInfo($pCancel['order_id']);
						$message_data = array(
							'message_status'=>1002
						);
						$param['message_type'] = 1018;
						$param['message_module_id'] = $pCancel['order_id'];
						$this->message->update($message_data,$param);
						$message_data = array(
							'message_owner'=>$order_data['order_finance'],
							'message_module'=>'order',
							'message_module_id'=>$pCancel['order_id'],
							'message_type'=>1019,
							'message_department'=>$order_data['order_finance_arr']['user_department'],
							'message_url'=>'www/cancel/cancel_affirm'
						);
						$this->system_log->add($log_data);
						$this->message->add($message_data);
					die( '1' );
					break;
				default:
					die( '作废流程有问题' );
					break;
            }
        }else die( 'error' );
    }

    //订单确认作废
    public function cancel_affirm() {
        if ( isset( $_GET['order_id'] ) ) {
            $data['record'] = array(); //备注记录表
            $data['order_id'] = $_GET['order_id'];
            $data['cancel'] = $_GET['cancel'];

            $this->load->model( 'www/order_model', 'order' );
            $data["labels"] = $this->order->attributeLabels();
            $data["order_data"]=$this->order->id_aGetInfo( $data['order_id'] );

            $this->load->model( 'www/order_record_model', 'record' );
            $where = array( 'cancel_order_id' => $data['order_id'] );
            $res_record = $this->record->GetInfo( $where );
            if ( $res_record )
                $data['record'] = $res_record;

            //明细
            $where=array(
                'order_d_order_id'=>$data['order_id'],
            );
            $this->db->select()->from('tc_order_d')->where($where)->order_by('order_d_id')->group_by('order_d_goods_code');
            $res = $this->db->get()->result_array();
            foreach($res as $k=>$v){
                $data['listData'][$k] = $v;
                $data['listData'][$k]['order_d_order_primecost'] = 0;
                $data['listData'][$k]['order_d_order_disc'] = 0;
                $where=array(
                    'order_d_order_id'=>$_GET['order_id'],
                    'order_d_goods_code'=>$v['order_d_goods_code']
                );
                $res_p = $this->order_d->GetInfo($where);
                $data['listData'][$k]['product_arr'] = $res_p;
                foreach($res_p as $vv){
                    $data['listData'][$k]['order_d_order_primecost'] += $vv['order_d_product_basic_primecost']; // 原价
                    $data['listData'][$k]['order_d_order_disc'] += $vv['order_d_product_basic_disc']; // 折后价
                }
            }

            $this->load->view( 'www/cancel/order_cancel_affirm', $data );
        } else {
            echo "不能没有ID和Node";
        }
    }

    public function ajax_cancel_affirm() {
        if ( $_POST['cancel'] ) {
            $pCancel = $_POST['cancel'];
			


            if ( empty( $pCancel['remark'] ) ) {
                die( 'remark error' );
            }
			
			
            if ( isset( $pCancel['refuse'] ) ) {
                $this->cancel->clearOrderModify($_POST);
                $this->cancel->orderCancel( $pCancel['order_id'], $pCancel['remark'], $state = 1001 );
                switch ( $pCancel['operation'] ) {
                     case 'cancel':
                    $log_data = array(
                        'system_log_addtime'=>date('Y-m-d H:i:s',time()),
                        'system_log_user_id'=>$this->session->userdata('user_id'),
                        'system_log_operation' => '订单确认作废',
                        'system_log_module'=>'Order',
                        'system_log_module_id'=>$pCancel['order_id'],
						'system_log_note' => '订单作废拒绝',
                    );
                    $order_data = $this->order->id_aGetInfo($pCancel['order_id']);
                    $message_data = array(
                        'message_status'=>1002
                    );
                    $param['message_type'] = 1015;
                    $param['message_module_id'] = $pCancel['order_id'];
                    $this->message->update($message_data,$param);
                    $message_data = array(
                        'message_owner'=>$order_data['order_review'],
                        'message_module'=>'order',
                        'message_module_id'=>$pCancel['order_id'],
                        'message_type'=>1014,
                        'message_department'=>$order_data['order_review_arr']['user_department'],
                        'message_url'=>'www/cancel/cancel_check'
                    );
                    $this->system_log->add($log_data);
                    $this->message->add($message_data);
                    die( '1' );
                    break;
                    case 'change':
                    $log_data = array(
                        'system_log_addtime'=>date('Y-m-d H:i:s',time()),
                        'system_log_user_id'=>$this->session->userdata('user_id'),
                        'system_log_operation' => '订单确认变更',
                        'system_log_module'=>'Order',
                        'system_log_module_id'=>$pCancel['order_id'],
						'system_log_note' => '订单作废拒绝',
                    );
                    $order_data = $this->order->id_aGetInfo($pCancel['order_id']);
                    $message_data = array(
                        'message_status'=>1002
                    );
                    $param['message_type'] = 1019;
                    $param['message_module_id'] = $pCancel['order_id'];
                    $this->message->update($message_data,$param);
                    $message_data = array(
                        'message_owner'=>$order_data['order_review'],
                        'message_module'=>'order',
                        'message_module_id'=>$pCancel['order_id'],
                        'message_type'=>1018,
                        'message_department'=>$order_data['order_review_arr']['user_department'],
                        'message_url'=>'www/cancel/cancel_check'
                    );
                    $this->system_log->add($log_data);
                    $this->message->add($message_data);
                    die('1');
                    break;
                 default:
                    die( '作废流程有问题' );
                    break;
                }
                die( '1' );
            }

            switch ( $pCancel['operation'] ) {
            case 'cancel':
				//新增需求 加入订单作废并新增出账红冲 start
				$this->cancel->orderRedCancel($pCancel['order_id'], $state = 1002);
				//新增需求 加入订单作废并新增出账红冲 end
                $this->cancel->orderCancel( $pCancel['order_id'], $pCancel['remark'], $state = 1002 );
                 $log_data = array(
                        'system_log_addtime'=>date('Y-m-d H:i:s',time()),
                        'system_log_user_id'=>$this->session->userdata('user_id'),
                        'system_log_operation' => '订单确认作废',
                        'system_log_module'=>'Order',
                        'system_log_module_id'=>$pCancel['order_id'],
						'system_log_note' => '订单作废成功',
                    );
                    $order_data = $this->order->id_aGetInfo($pCancel['order_id']);
                    $message_data = array(
                        'message_status'=>1002
                    );
                    $param['message_type'] = 1015;
                    $param['message_module_id'] = $pCancel['order_id'];
                    $this->message->update($message_data,$param);

                    $this->system_log->add($log_data);
                die( '1' );
            case 'change':
				//p($_POST); 
                $res = $this->cancel->updateOrderModify( $_POST ); //更变操作
                $this->cancel->orderCancel( $pCancel['order_id'], $pCancel['remark'], $state = 1002 );
				//p("ddd");die();
                 $log_data = array(
                        'system_log_addtime'=>date('Y-m-d H:i:s',time()),
                        'system_log_user_id'=>$this->session->userdata('user_id'),
                        'system_log_operation' => '订单确认变更',
                        'system_log_module'=>'Order',
                        'system_log_module_id'=>$pCancel['order_id'],
						'system_log_note' => '订单作废成功',
                    );
					
                    $order_data = $this->order->id_aGetInfo($pCancel['order_id']);
                    $message_data = array(
                        'message_status'=>1002
                    );
                    $param['message_type'] = 1019;
                    $param['message_module_id'] = $pCancel['order_id'];
                    $this->message->update($message_data,$param);

                    $this->system_log->add($log_data);
                die( '1' );
                break;
            default:
                die( '作废流程有问题' );
                break;
            }
        }else die( 'error' );
    }

}

?>
