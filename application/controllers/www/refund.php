<?php
class Refund extends WWW_Controller{
	public $menu1='refund';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/refund_model','refund');
		$this->load->model('www/system_log_model','system_log');
		$this->load->model('www/message_model','message');
		$this->load->model('www/message_remind_model','message_remind');
		$this->load->model('www/order_model','order');
		$this->db->query('SET NAMES UTF8');
	}

	public function detailed_list_view(){
		if(isset($_GET['order_id'])){
			$where=array(
				'refund_order_id'=>$_GET['order_id']
			);
			$data['listData']=$this->refund->GetInfo($where);
			$data["labels"]=$this->refund->attributeLabels();
			$data['listLayout']=$this->refund->listLayout();
			$this->output->enable_profiler(false);
			$this->load->view('www/refund/detailed_list_view',$data);
		}else{
			echo "不能没有ID";
		}
	}

	//ajax新增
	public function ajax_add(){
		if(isset($_GET['order_id'])){

			$data["order_data"]=$this->order->id_aGetInfo($_GET['order_id']);
			$order_id = $data["order_data"]['order_id'];
			//需要的一些数据
			//已确认到帐金额
			$where = array(
				'salespay_order_id'=>$order_id,
				'salespay_status'=>1002,
			);
			$this->load->model('www/salespay_model','salespay');
			$data["ydz_amount"]=$this->salespay->SumGetInfo('salespay_pay_amount',$where); //已到帐
			//已确认退款
			$where = array(
				'refund_order_id'=>$order_id,
				'refund_status'=>1003,
			);
			$data["ytk_amount"]=$this->refund->SumGetInfo('refund_amount',$where); //已退款
			//未确认退款
			$where = array(
				'refund_order_id'=>$order_id,
				'refund_status !='=>1003,
			);
			$data["wtk_amount"]=$this->refund->SumGetInfo('refund_amount',$where); //已退款

			//可退款金额 已确认到帐金额-已确认退款-未确认退款=可退款金额
			$data["ktk_amount"]=$data["ydz_amount"]-$data["ytk_amount"]-$data["wtk_amount"];
			$this->load->model('admin/enum_model','enum');$data["refund_pay_method_enum"]=$this->enum->getlist(453);
			//p($data);
			$this->load->view('www/refund/ajax_add',$data);
		}else{
			echo "不能没有ID";
		}
	}

	//ajax查看页面
	public function ajax_view(){
		if(isset($_GET['refund_id'])){
			$data["refund_data"]=$this->refund->id_aGetInfo($_GET['refund_id']);
			$order_id = $data["refund_data"]['refund_order_id_arr']['order_id'];
			$data["labels"]=$this->refund->attributeLabels();

			//解析付款信息
			if(isset($data['refund_data']['refund_pay_info'])){
				$data['refund_data']["refund_pay_info_arr"]=json_decode($data['refund_data']['refund_pay_info'],true);
			}

			//解析退款商品信息
			if(isset($data['refund_data']['refund_goods_info'])){
				$data['refund_data']["refund_goods_info_arr"]=json_decode($data['refund_data']['refund_goods_info'],true);
			}

			//已确认到帐金额
			$where = array(
				'salespay_order_id'=>$order_id,
				'salespay_status'=>1002,
			);
			$this->load->model('www/salespay_model','salespay');
			$data["ydz_amount"]=$this->salespay->SumGetInfo('salespay_pay_amount',$where); //已到帐
			//已确认退款
			$where = array(
				'refund_order_id'=>$order_id,
				'refund_status'=>1003,
				'refund_id !='=>$data["refund_data"]['refund_id'],
			);
			$data["ytk_amount"]=$this->refund->SumGetInfo('refund_amount',$where); //已退款
			//未确认退款
			$where = array(
				'refund_order_id'=>$order_id,
				'refund_status !='=>1003,
				'refund_id !='=>$data["refund_data"]['refund_id'],
			);
			$data["wtk_amount"]=$this->refund->SumGetInfo('refund_amount',$where); //已退款
			//可退款金额 已确认到帐金额-已确认退款-未确认退款=可退款金额
			$data["ktk_amount"]=$data["ydz_amount"]-$data["ytk_amount"]-$data["wtk_amount"];
			$this->load->model('admin/enum_model','enum');$data["refund_pay_method_enum"]=$this->enum->getlist(453);
			//p($data);
			$this->load->view('www/refund/ajax_view',$data);
		}else{
			echo "不能没有ID";
		}
	}

	//ajax编辑
	public function ajax_edit(){
		if(isset($_GET['refund_id'])){
			$data["refund_data"]=$this->refund->id_aGetInfo($_GET['refund_id']);
			$order_id = $data["refund_data"]['refund_order_id_arr']['order_id'];
			$data["labels"]=$this->refund->attributeLabels();

			//解析付款信息
			if(isset($data['refund_data']['refund_pay_info'])){
				$data['refund_data']["refund_pay_info_arr"]=json_decode($data['refund_data']['refund_pay_info'],true);
			}

			//解析退款商品信息
			if(isset($data['refund_data']['refund_goods_info'])){
				$data['refund_data']["refund_goods_info_arr"]=json_decode($data['refund_data']['refund_goods_info'],true);
			}

			//已确认到帐金额
			$where = array(
				'salespay_order_id'=>$order_id,
				'salespay_status'=>1002,
			);
			$this->load->model('www/salespay_model','salespay');
			$data["ydz_amount"]=$this->salespay->SumGetInfo('salespay_pay_amount',$where); //已到帐
			//已确认退款
			$where = array(
				'refund_order_id'=>$order_id,
				'refund_status'=>1003,
				'refund_id !='=>$data["refund_data"]['refund_id'],
			);
			$data["ytk_amount"]=$this->refund->SumGetInfo('refund_amount',$where); //已退款
			//未确认退款
			$where = array(
				'refund_order_id'=>$order_id,
				'refund_status !='=>1003,
				'refund_id !='=>$data["refund_data"]['refund_id'],
			);
			$data["wtk_amount"]=$this->refund->SumGetInfo('refund_amount',$where); //已退款
			//可退款金额 已确认到帐金额-已确认退款-未确认退款=可退款金额
			$data["ktk_amount"]=$data["ydz_amount"]-$data["ytk_amount"]-$data["wtk_amount"];
			$this->load->model('admin/enum_model','enum');$data["refund_pay_method_enum"]=$this->enum->getlist(453);
			//p($data);
			$this->load->view('www/refund/ajax_edit',$data);
		}else{
			echo "不能没有ID";
		}
	}

	//ajax审批
	public function ajax_examine_refund(){
		if(isset($_GET['refund_id'])){
			$data["refund_data"]=$this->refund->id_aGetInfo($_GET['refund_id']);
			$order_id = $data["refund_data"]['refund_order_id_arr']['order_id'];
			$data["order_data"]=$this->order->id_aGetInfo($order_id);
			$data["labels"]=$this->refund->attributeLabels();

			//解析付款信息
			if(isset($data['refund_data']['refund_pay_info'])){
				$data['refund_data']["refund_pay_info_arr"]=json_decode($data['refund_data']['refund_pay_info'],true);
			}
			//p($data['refund_data']["refund_pay_info_arr"]);
			//解析退款商品信息
			if(isset($data['refund_data']['refund_goods_info'])){
				$data['refund_data']["refund_goods_info_arr"]=json_decode($data['refund_data']['refund_goods_info'],true);
			}

			//已确认到帐金额
			$where = array(
				'salespay_order_id'=>$order_id,
				'salespay_status'=>1002,
			);
			$this->load->model('www/salespay_model','salespay');
			$data["ydz_amount"]=$this->salespay->SumGetInfo('salespay_pay_amount',$where); //已到帐
			//已确认退款
			$where = array(
				'refund_order_id'=>$order_id,
				'refund_status'=>1003,
				'refund_id !='=>$data["refund_data"]['refund_id'],
			);
			$data["ytk_amount"]=$this->refund->SumGetInfo('refund_amount',$where); //已退款
			//未确认退款
			$where = array(
				'refund_order_id'=>$order_id,
				'refund_status !='=>1003,
				'refund_id !='=>$data["refund_data"]['refund_id'],
			);
			$data["wtk_amount"]=$this->refund->SumGetInfo('refund_amount',$where); //已退款
			//可退款金额 已确认到帐金额-已确认退款-未确认退款=可退款金额
			$data["ktk_amount"]=$data["ydz_amount"]-$data["ytk_amount"]-$data["wtk_amount"];
			$this->load->view('www/refund/ajax_examine_refund',$data);
		}else{
			echo "不能没有ID";
		}
	}

	//ajax审批
	public function ajax_confirm_refund(){
		if(isset($_GET['refund_id'])){
			$data["refund_data"]=$this->refund->id_aGetInfo($_GET['refund_id']);
			//p($data);exit;
			$order_id = $data["refund_data"]['refund_order_id_arr']['order_id'];
			$data["order_data"]=$this->order->id_aGetInfo($order_id);
			$data["labels"]=$this->refund->attributeLabels();

			//解析付款信息
			if(isset($data['refund_data']['refund_pay_info'])){
				$data['refund_data']["refund_pay_info_arr"]=json_decode($data['refund_data']['refund_pay_info'],true);
			}

			//解析退款商品信息
			if(isset($data['refund_data']['refund_goods_info'])){
				$data['refund_data']["refund_goods_info_arr"]=json_decode($data['refund_data']['refund_goods_info'],true);
			}

			//已确认到帐金额
			$where = array(
				'salespay_order_id'=>$order_id,
				'salespay_status'=>1002,
			);
			$this->load->model('www/salespay_model','salespay');
			$data["ydz_amount"]=$this->salespay->SumGetInfo('salespay_pay_amount',$where); //已到帐
			//已确认退款
			$where = array(
				'refund_order_id'=>$order_id,
				'refund_status'=>1003,
				'refund_id !='=>$data["refund_data"]['refund_id'],
			);
			$data["ytk_amount"]=$this->refund->SumGetInfo('refund_amount',$where); //已退款
			//未确认退款
			$where = array(
				'refund_order_id'=>$order_id,
				'refund_status !='=>1003,
				'refund_id !='=>$data["refund_data"]['refund_id'],
			);
			$data["wtk_amount"]=$this->refund->SumGetInfo('refund_amount',$where); //已退款
			//可退款金额 已确认到帐金额-已确认退款-未确认退款=可退款金额
			$data["ktk_amount"]=$data["ydz_amount"]-$data["ytk_amount"]-$data["wtk_amount"];
			$this->load->model('admin/enum_model','enum');$data["refund_pay_account_enum"]=$this->enum->getlist(540);
			$this->load->view('www/refund/ajax_confirm_refund',$data);
		}else{
			echo "不能没有ID";
		}
	}


	//ajax调用的新增
	public function ajax_add_post(){
		if($_POST){
			//p($_POST);die;
			$refund_data = $_POST['refund'];
			$refund_data['refund_pay_info'] = $_POST['payinfo'];

			$refund_data['refund_pay_info']['department'] = $_POST['order_finance_department'];
			$refund_data['refund_pay_info'] = json_encode($refund_data['refund_pay_info']);
			$refund_data['refund_goods_info'] = json_encode($_POST['goods_info']);
			//p($refund_data);exit;
			if(!isset($refund_data['refund_status'])){
				$refund_data['refund_status'] = '1001'; //退款状态创建后默认给【未审批】
			}
			$this->refund->add($refund_data);
			$refund_id = $this->db->insert_id();
			$log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'审批退款',
				'system_log_module'=>'Order',
				'system_log_module_id'=>$refund_data['refund_order_id'],
				'system_log_note'=>$refund_data['refund_reason']
				);
			$this->system_log->add($log_data);
			$this->load->model('www/user_model','user');
			$manage_info = $this->user->id_aGetInfo($refund_data['refund_manage']);
			$message_data = array(
					'message_owner'=>$refund_data['refund_manage'],
					'message_module'=>'refund',
					'message_module_id'=>$refund_id,
					'message_type'=>1007,
					'message_department'=>$manage_info['user_department'],
					'message_url'=>'www/refund/ajax_examine_refund'
			);
			$this->message->add($message_data);
			// $mess_remind_data = array(
			// 	'message_remind_role_id' =>3,
			// 	'message_remind_status'=>1001
			// );
			// $this->message_remind->update($mess_remind_data,$mess_remind_data['message_remind_role_id']);
			echo 1;
		}else{
			echo '失败';
		}
	}

	//ajax调用的编辑
	public function ajax_update_post(){
		if($_POST){
			$refund_data = $_POST['refund'];
			if(isset($_POST['payinfo'])){
				$refund_data['refund_pay_info'] = json_encode($_POST['payinfo']);
			}
			if(isset($_POST['goods_info'])){
				$refund_data['refund_goods_info'] = json_encode($_POST['goods_info']);
			}
			if(!isset($refund_data['refund_status'])){
				$refund_data['refund_status'] = 1001;
			}
			//p($refund_data);exit;
//cus lee 2014-04-23  调用渠道接口 start
			$refund_sql_data=$this->refund->id_aGetInfo($refund_data['refund_id']);
			//p($refund_sql_data['refund_order_id_arr']);exit;
			if($refund_sql_data['refund_order_id_arr']['order_source']==1003){ //当来源为【渠道】的订单
				//调用打渠道的接口
				$params['action'] = 'SalesOrderReturnMoneyApply'; //预存款和销售订单的审核
				$params['id']= $refund_sql_data['refund_number']; //传给营收的工单编码
				//p($params['id']);exit;
				//p($salespay_data['salespay_status']);exit;
				if($refund_data['refund_status'] == 1004){ //当选择确认款项的时候
					$params['result']= 'ok'; //审核成功
					$params['remark']= 'XXXXXXX'; //审核备注
					$params['time']= date("Y-m-d H:i:s"); //审核时间 (用当前时间)
					$res = $this->qudao_api($params);
					if($res['res']=='fail'){
						echo $res['msg'];exit;
					}else{
						//p($res);
					}
				}else if($refund_data['refund_status'] == 1005){ //当选择款项不成功的时候
					$params['result']= 'failure'; //审核失败
					$params['remark']= 'XXXXXXX'; //审核备注
					$params['time']= date("Y-m-d H:i:s"); //审核时间 (用当前时间)
					$res = $this->qudao_api($params);
					if($res['res']=='fail'){
						echo $res['msg'];exit;
					}else{
						//p($res);
					}
				}
			}
//cus lee 2014-04-23  调用渠道接口 end
			$this->refund->update($refund_data,$refund_data['refund_id']);
			$log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),

				'system_log_module'=>'Order',
				'system_log_module_id'=>$refund_data['refund_order_id'],

				);
			if($refund_data['refund_status'] == 1001){
				$this->load->model('www/user_model','user');
				$manage_info = $this->user->id_aGetInfo($refund_data['refund_manage']);
				$log_data['system_log_operation'] = '编辑退款信息';
				$log_data['system_log_note'] = $refund_data['refund_reason'];
				$message_data1 = array(
					'message_status'=>1002
				);
	            $param['message_type'] = 1013;
	            $param['message_module_id'] = $refund_data['refund_id'];
				$this->message->update($message_data1,$param);
				$message_data = array(
					'message_owner'=>$refund_data['refund_manage'],
					'message_module'=>'refund',
					'message_module_id'=>$refund_data['refund_id'],
					'message_type'=>1007,
					'message_department'=>$manage_info['user_department'],
					'message_url'=>'www/refund/ajax_examine_refund'
				);
				$this->message->add($message_data);
			// 	$mess_remind_data = array(
			// 	'message_remind_role_id' =>3,
			// 	'message_remind_status'=>1001
			// );
			// $this->message_remind->update($mess_remind_data,$mess_remind_data['message_remind_role_id']);
			}else if($refund_data['refund_status'] == 1002){
				$order_data = $this->order->id_aGetInfo($refund_data['refund_order_id']);
				$log_data['system_log_operation'] = '退款审核通过';
				$message_data = array(
					'message_owner'=>$order_data['order_finance'],
					'message_module'=>'refund',
					'message_module_id'=>$refund_data['refund_id'],
					'message_type'=>1008,
					'message_department'=>$order_data['order_finance_arr']['user_department'],
					'message_url'=>'www/refund/ajax_confirm_refund'
				);
				$this->message->add($message_data);
				$message_data = array(
					'message_status'=>1002
				);
	            $param['message_type'] = 1007;
	            $param['message_module_id'] = $refund_data['refund_id'];
				$this->message->update($message_data,$param);
			}else if($refund_data['refund_status'] == 1003){
				$log_data['system_log_operation'] = '退款审核不通过';
				$message_data = array(
					'message_status'=>'1002'
				);
	            $param['message_type'] = 1007;
	            $param['message_module_id'] = $refund_data['refund_id'];
				$this->message->update($message_data,$param);
				$order_data = $this->order->id_aGetInfo($refund_data['refund_order_id']);
	 		   $message_data = array(
						'message_owner'=>$order_data['order_typein'],
						'message_module'=>'refund',
						'message_module_id'=>$refund_data['refund_id'],
						'message_type'=>1013,
						//'message_department'=>$order_data['order_owner_arr']['user_department'],
						'message_url'=>'www/refund/ajax_edit'
				);
				$this->message->add($message_data);
			}else if($refund_data['refund_status'] == 1004){
				$log_data['system_log_operation'] = '退款成功';
				$message_data = array(
					'message_status'=>'1002'
				);
	            $param['message_type'] = 1008;
	            $param['message_module_id'] = $refund_data['refund_id'];
				$this->message->update($message_data,$param);
				$this->load->model('www/books_model','books');
				$this->books->update(array('books_state'=>1002),$refund_data['refund_book_id']);
			}else if($refund_data['refund_status'] == 1005){
				$log_data['system_log_operation'] = '执行退款失败';
				$message_data1 = array(
					'message_status'=>'1002'
				);
	            $param['message_type'] = 1008;
	            $param['message_module_id'] = $refund_data['refund_id'];
				$this->message->update($message_data1,$param);
				$order_data = $this->order->id_aGetInfo($refund_data['refund_order_id']);
	 		   $message_data = array(
						'message_owner'=>$order_data['order_typein'],
						'message_module'=>'refund',
						'message_module_id'=>$refund_data['refund_id'],
						'message_type'=>1013,
						//'message_department'=>$order_data['order_owner_arr']['user_department'],
						'message_url'=>'www/refund/ajax_edit'
				);
				$this->message->add($message_data);
			}
			$this->system_log->add($log_data);
			echo 1;
		}else{
			echo '失败??';
		}
	}

	//ajax调用的删除
	public function ajax_del_post(){
		if($_POST){
			$refund_id = $_POST['refund_id'];
			$this->refund->del($refund_id);
			$log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'删除返点',
				'system_log_module'=>'Order',
				'system_log_module_id'=>$_POST['order_id'],
				);

			$this->system_log->add($log_data);
			echo 1;
		}else{
			echo '失败';
		}
	}

	public function qudao_api($params){
		require_once('application/libraries/snoopy.php');
		$snoopy = new Snoopy();
		$url="http://192.168.35.28:9014/api/review";
		//同步渠道
		$snoopy->submit($url,$params);
		$res = $snoopy->results;
		//print_r($res);
		return json_decode($res,true);
	}
}
?>