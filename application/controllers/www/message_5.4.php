<?php
class Message extends WWW_Controller{
	public $menu1='message';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/message_model','message');
		$this->db->query('SET NAMES UTF8');

	}

	public function index(){
		$this->message->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		 $data = $this->get_data_auth('message');
		$data['role_id_arr'] = json_encode($this->session->userdata('role_id_arr'));
		$data['totalNumber'] = $this->message->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->message->attributeLabels();
		$data['listLayout']=$this->message->listLayout();
		$this->render('www/message/list',$data);
	}
	public function ajax_select(){
		//p($_GET);
		$tem = explode('?',$_GET['module']);
		$module = $tem[0];
		$new_where=$_POST;
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
					$select_arr = json_decode($_POST["select_json"],true);
					//p($select_arr['attr'].'-'.$select_arr['value']);
					$new_where['select_arr']=array(
						$select_arr['attr']=>$select_arr['value'],
					);
				}else{
					$new_where['select_arr']="";
				}


				//把条件组合起来主要是where、get_attr和select_arr
				//循环出get_attr
				$where_all=$new_where['where'];
				if($new_where['where_rel']==""){
					$rel_all = "";
				}else{
					$rel_all = "(".$new_where['where_rel'].")";
				}

				if(isset($new_where['get_attr']) && $new_where['get_attr']!=""){
					$i=0;
					$ga_rel="";
					foreach ($new_where['get_attr'] as $key=>$value){
						$i++;

							$where_all['ga'.$i]=array(
								'attr'=>$key,
								'value'=>$value,
								'action'=>'=',
							);

						if($ga_rel==""){
							$ga_rel='ga'.$i;
						}else{
							$ga_rel=$ga_rel.' and ga'.$i;
						}
					}
					if($rel_all!==""){
						$rel_all=$rel_all." and (".$ga_rel.")";
					}else{
						$rel_all="(".$ga_rel.")";
					}

				}

				if ($new_where['select_arr']!=""){
					$i=0;
					$sa_rel="";
					foreach ($new_where['select_arr'] as $key=>$value){
						$i++;
						$where_all['sa'.$i]=array(
							'attr'=>$key,
							'value'=>$value,
							'action'=>'like',
						);
						if($sa_rel==""){
							$sa_rel='sa'.$i;
						}else{
							$sa_rel=$sa_rel.' and sa'.$i;
						}
					}
					if($rel_all!=""){
						$rel_all=$rel_all." and (".$sa_rel.")";
					}else{
						$rel_all="(".$sa_rel.")";
					}

				}
				$new_where_ok = array(
					"where_all"=>$where_all,
					"rel_all"=>$rel_all,
					"page"=>$new_where['page'],
					"perNumber"=>$new_where['perNumber'],
					"obj"=>$new_where['obj'],
				);
				$this->load->model('www/api_model','api');
				$listData = $this->api->select($new_where_ok);
			//p($listData['info']);
				if($module!='applyopen'){
						if($module!='order'){
							foreach ($listData['info'] as $k=>$v){
								$this->load->model('www/'.$v['message_module'].'_model',$v['message_module']);
								$module_data = $this->$v['message_module']->id_aGetInfo($v['message_module_id']);
								//p($module_data);die;
								$listData['info'][$k]['order_number'] = $module_data[$v['message_module'].'_order_id_arr']['order_number'];
								$listData['info'][$k]['order_amount'] = $module_data[$v['message_module'].'_order_id_arr']['order_amount'];
								$salespay_info = $this->db->get_where('tc_salespay',array('salespay_order_id'=>$module_data[$v['message_module'].'_order_id_arr']['order_id'],'salespay_status'=>1002))->result_array();
								$listData['info'][$k]['order_ydz_amount']=0;
								if(!empty($salespay_info)){

									foreach ($salespay_info as $key => $value) {
										$listData['info'][$k]['order_ydz_amount']+=$value['salespay_pay_amount'];
									}
								}

								$listData['info'][$k]['order_account'] = $module_data[$v['message_module'].'_order_id_arr']['order_account_arr']['account_name'];
								$listData['info'][$k]['order_owner'] = $module_data[$v['message_module'].'_order_id_arr']['order_owner_arr']['user_name'];

							}
						}else{
							foreach ($listData['info'] as $k=>$v){
								$this->load->model('www/'.$v['message_module'].'_model',$v['message_module']);
								$module_data = $this->$v['message_module']->id_aGetInfo($v['message_module_id']);
								$listData['info'][$k]['order_number'] = $module_data['order_number'];
								$listData['info'][$k]['order_amount'] = $module_data['order_amount'];
								$salespay_info = $this->db->get_where('tc_salespay',array('salespay_order_id'=>$module_data['order_id'],'salespay_status'=>1002))->result_array();
								$listData['info'][$k]['order_ydz_amount']=0;
								if(!empty($salespay_info)){

									foreach ($salespay_info as $key => $value) {
										$listData['info'][$k]['order_ydz_amount']+=$value['salespay_pay_amount'];
									}
								}

								$listData['info'][$k]['order_account'] = $module_data['order_account_arr']['account_name'];
								$listData['info'][$k]['order_owner'] = $module_data['order_owner']['user_name'];

							}
						}

				}else{
					foreach($listData['info'] as $k=>$v){
						$t = $this->db->select('applyopen_order_d_id')->from('tc_applyopen')->where('applyopen_id',$v['message_module_id'])->get()->row_array();
						$order_d_id_arr[] = $t['applyopen_order_d_id'];
					}
					foreach($order_d_id_arr as $k=>$v){
						$t = $this->db->select()->from('tc_order_d')->where('order_d_id',$v)->order_by('order_d_id')->group_by('order_d_goods_code')->get()->result_array();
						$data['apply_data'][] = $t[0];
					}
					// foreach ($data['apply_data'] as $key => $value) {
					// 	# code...
					// }
					//p($data['apply_data']);
				}
				//p($listData);
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["message_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		if (isset($_GET['type_id'])){
			if($_GET['type_id']!="" and $_GET['type_id']!=0){
				$where['type_id']=$_GET['type_id'];
			}
		}

		$data['listData']=$listData['info'];
		$data['sel_data']=$listData['sel_data'];
		//p($data['listData']);
		if(!empty($data['listData'])){
			foreach ($data['listData'] as $key => $value) {

				$data['listData'][$key]['type_name'] = $this->message->action_name($value['message_type']);
			}
		}

		//p($data['listData']);
		$data["labels"]=$this->message->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取



		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->message->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/message/ajax_select',$data);
	}

	//
	public function new_ajax_select(){
		//p($_POST);//exit;

		$this->load->model('www/api_model','api');
		$listData = $this->api->select($_POST);
		if(!isset($_GET['module'])){
			foreach ($listData['info'] as $k=>$v){
				$this->load->model('www/'.$v['message_module'].'_model',$v['message_module']);
				$module_data = $this->$v['message_module']->id_aGetInfo($v['message_module_id']);
				$listData['info'][$k]['order_number'] = $module_data[$v['message_module'].'_order_id_arr']['order_number'];
				$listData['info'][$k]['order_amount'] = $module_data[$v['message_module'].'_order_id_arr']['order_amount'];
				$salespay_info = $this->db->get_where('tc_salespay',array('salespay_order_id'=>$module_data[$v['message_module'].'_order_id_arr']['order_id'],'salespay_status'=>1002))->result_array();
				$listData['info'][$k]['order_ydz_amount']=0;
				if(!empty($salespay_info)){

					foreach ($salespay_info as $key => $value) {
						$listData['info'][$k]['order_ydz_amount']+=$value['salespay_pay_amount'];
					}
				}

				$listData['info'][$k]['order_account'] = $module_data[$v['message_module'].'_order_id_arr']['order_account_arr']['account_name'];
				$listData['info'][$k]['order_owner'] = $module_data[$v['message_module'].'_order_id_arr']['order_owner']['user_name'];
			}
		}else if($_GET['module']=='order'){
			foreach ($listData['info'] as $k=>$v){
				$this->load->model('www/order_model','order');
				$module_data = $this->order->id_aGetInfo($v['message_module_id']);
				$listData['info'][$k]['order_number'] = $module_data['order_number'];
				$listData['info'][$k]['order_amount'] = $module_data['order_amount'];
				$salespay_info = $this->db->get_where('tc_salespay',array('salespay_order_id'=>$module_data['order_id'],'salespay_status'=>1002))->result_array();
				$listData['info'][$k]['order_ydz_amount']=0;
				if(!empty($salespay_info)){

					foreach ($salespay_info as $key => $value) {
						$listData['info'][$k]['order_ydz_amount']+=$value['salespay_pay_amount'];
					}
				}

				$listData['info'][$k]['order_account'] = $module_data['order_account_arr']['account_name'];
				$listData['info'][$k]['order_owner'] = $module_data['order_owner']['user_name'];
			}
		}

		$data["labels"]=$this->message->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取


		if(isset($_GET['module']) && $_GET['module'] == 'applyopen'){
			foreach($listData['info'] as $k=>$v){
						$t = $this->db->select('applyopen_order_d_id')->from('tc_applyopen')->where('applyopen_id',$v['message_module_id'])->get()->row_array();
						$order_d_id_arr[] = $t['applyopen_order_d_id'];
					}
					foreach($order_d_id_arr as $k=>$v){
						$t = $this->db->select()->from('tc_order_d')->where('order_d_id',$v)->order_by('order_d_id')->group_by('order_d_goods_code')->get()->result_array();
						$data['apply_data'][] = $t[0];
					}
		}
		$data['listData']=$listData['info'];
		if(!empty($data['listData'])){
			foreach ($data['listData'] as $key => $value) {

				$data['listData'][$key]['type_name'] = $this->message->action_name($value['message_type']);
			}
		}
		$data['totalNumber'] = $listData['count']; //数据总数
		$data['sel_data']=$listData['sel_data'];
		$data['perNumber'] = $_POST['perNumber']; //每页显示的记录数
		$data['page'] = $_POST['page'];//当前页数
		//p($data);exit;
		//根据类型的不同调用不同的列表页

		$this->load->view('www/message/ajax_select',$data);

	}



}
?>