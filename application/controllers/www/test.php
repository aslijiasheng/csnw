<?php

class Test extends CI_Controller {

	public function index() {
		echo "test";
		$this->load->helper('message');
		message('操作成功', 'www/test/f1');
	}

	public function f1() {
		echo "f1";
	}

	public function api() {
		include("lib/snoopy.inc.php");
		$url = "http://localhost/gii1234/index.php/www/api";
		$snoopy = new Snoopy();

		$snoopy->agent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)";
		//$snoopy->rawheaders["X_FORWARDED_FOR"] = '111.111.1.1';
		$snoopy->referer = 'http://localhost/gii1234/index.php/www/index'; //伪装来源页地址 http_referer
		// $snoopy->referer = "222.222.2.2";
		// $snoopy->remote_addr = '111.11.1.1';
//         $snoopy->proxy_host = "my.proxy.host";
// $snoopy->proxy_port = "8080";
		// $params['loginname'] = 'licheng';
		// $params['loginpass'] = '1111';
		// $params['action'] = 'add';
		// $params['object'] = 'order';
		// $data = array(
		//   'agreement_no' => '122212014010201', //合同编号
		//   'number' => '122212014010201', //合同编号
		//   'agreement_name' => 'taoex订单010201', //合同名称
		//   'create_user_id' => '1',
		//   'create_time' => '1395554323',
		//   'type' => '6', //订单类型.类型名称
		//   'owner.gonghao' => 's001', //s1307,s1492 //所属销售.
		//   //'department'=> 1, //所属部门（可不填写，默认等于所属销售的所属部门）
		//   'finance.gonghao' => 's001', //财务 s0134阎静
		//   'finance.name' => '阎静', //财务 s0134阎静
		//   'review.gonghao' => 's001', //s0399,s1090 //主管
		//   //'create_time' => time(), //创建时间(这个系统自动生成)
		//   'amount' => 30, //订单总金额
		//   'enum12312.order_department' => '是', //订单总金额
		//   'account.account_shopexid' => '110110160890', //客户.商业ID
		//   'agent.partner_id' => '1231231231213', //关联代理商.商业ID
		//   //'rebate_amount' => '100', //返点返利成本
		//   'detailed' => array(
		//     'product_items' => json_encode(array(
		//       array('goods_code' => 'goods_0331', 'goods_amount' => 10, 'price_code' => 'gp00000000393'),
		//       array('goods_code' => 'goods_0124', 'goods_amount' => 20, 'price_code' => 'gp00000000147'),
		//     ))
		//   )
		// );
		// $json = json_encode($data);
		// $params['json'] = $json;
		// $snoopy->submit($url, $params);
		// $res = $snoopy->results;
		// print_r($res);
		// $snoopy->submit($url, $params);
		// $res = $snoopy->results;

		$submit_url = "http://localhost/gii1234/index.php/www/index/login";
		// $submit_vars["loginmode"] = "normal";
		// $submit_vars["styleid"] = "1";
		// $submit_vars["cookietime"] = "315360000";
		// $submit_vars["loginfield"] = "username";
		$submit_vars["user[user_login_name]"] = "licheng"; //你的用户名
		$submit_vars["user[user_password]"] = "1";  //你的密码
		$snoopy->submit($submit_url, $submit_vars);
		print $snoopy->results;
	}

	//模拟通过接口改变客户信息
	public function account() {
		p(log_message('level', 'message'));
	}

	public function add() {
		include("lib/snoopy.inc.php");
		$this->load->library('session');
		$this->load->model('www/user_model', 'user');

		$params = array();
		$snoopy = new snoopy();
		$url = 'http://localhost/framework/index.php/api';

		$user_id = $this->session->userdata('user_id');
		$res_user = $this->db->select('user_login_name, user_password')->get_where('tc_user', array('user_id' => $user_id))->row_array();
		$user_auth = $this->user->user_auth($user_id);

		// $params['app_type'] = 'post';
		// $params['format'] = 'json';
		// $params['version'] = '1.0';
		// $params['call_times'] = 'time';
		// $params['return'] = 'has';



		$params['user'] = $res_user['user_login_name'];
		$params['pwd'] = $res_user['user_password'];
		// $params['user'] = 'ww';
		// $params['pwd'] = '1';
		$params['action'] = 'add';
		$params['object'] = 'order';
//		$params['data'] = '{"amount":9999,"agent.shopex_id":661401560283,"create_time":"2014-03-31 13:57:56","detailed":{"salespay":[{"pay_info":{"remit_account":"6222xxxxxxxx","remit_bank.name":"农业银行","remit_person":"某某某"},"pay_date":"2014-04-20 15:34:05","pay_method":"汇款","pay_amount":110000,"pay_note":"xxx"}]},"deposit_paytype.name":"保证金","type.name":"预收款订单","finance.gonghao":"s00001","review.name":"s00001","number":"sa109310123221"}';
//		$params['data'] = '{"amount":9999,"agent.shopex_id":"11121212","create_time":"2014-03-31 13:57:56","type.name":"预收款订单","finance.gonghao":"s0001","review.gonghao":"s0001","number":"sa109310123112211"}';
		$params['data'] = '{"type.name": "销售订单","create_time": "2014-04-21  11:15:10","agreement_no": "SDM2013121300140","amount": "2000.00","owner.gonghao": "s0329","finance.gonghao": "s1631","review.gonghao": "s1482","detailed": {"order_d": [{"goods.code": "goods_0071","goods_amount": "2000.00","goods_price.code": "gp00000000071"}]}}';

//		$params['data'] = json_encode(array(
		// "number": "sa109310123221", // 订单id
		//          "type.name": "预存款订单", // 订单类型
		//          "create_time": "2014-03-31 13:57:56", // 创建时间
		//          "deposit_paytype.name": "保证金", // 款项类型（保证金/货款）
		//          "amount": 9999, // 金额
		//          "agent.shopex_id": "661401560283", // 经销商shopexid
		//          "review.name": "s00001", // 渠道经理工号
		//          "finance.gonghao": "s00001", //财务工号
		//          "detailed": {
		//
				// 'agreement_name' => 'xxx合同', //合同名称
		// 'create_time' => '2014-03-31 13:57:56', //创建时间
		// 'agreement_no' => 'iowkskldfjakljfl', //合同编号
		// 'owner.name' => '宋雪', //所属销售
		// 'account.account_shopexid' => '661401560283', //所属客户
		// 'department.name' => '直销业务中心', //所属部门
		// 'agent.name' => '1231', //所属代理商
		// 'finance.name' => '宋雪', //财务人员
		// 'if_renew.name' => '是',
		// 'review.name' => '宋雪', //业务主管
		// 'nreview.name' => '宋雪', //内审人员
		// 'amount' => 9999.000, //订单金额
		// 'PassID' => '661401560283', //SopexID
		// // 				'order_d' => array(
		// 		array(
		// 			'goods.code' => 'group_0044',
		// 			'goods_amount' => 123000,
		// 			'goods_price.code' => 'gp00000000393'
		// 		),
		// 		array(
		// 			'goods.code' => 'goods_0016',
		// 			'goods_amount' => 123000,
		// 			'goods_price.code' => 'gp00000000392'
		// 		)
		// 	),
//			'number' => 'yc00333',
//			'type.name' => '预收款订单',
//			'create_time' => "2014-03-31 13:57:56",
//			"deposit_paytype.name" => '保证金',
//			"amount" => 9999, // 金额
//			"agent.shopex_id" => "11121212",
//			"review.gonghao" => "s0028", // 渠道经理工号
//			"finance.gonghao" => "s0028", //财务工号
//			'detailed' => array(
//				'salespay' => array(
//					array(
//						"pay_amount" => 110000, // 本次入账金额
//						"pay_date" => "2014-04-20 15:34:05", // 入账日期
//						"pay_note" => "xxx", // 备注
//						'pay_method.name' => '汇款', // 支付方式（汇款/支付宝/支票/现金）
//						"money_type.name" => '入款项',
//						'pay_info' => array(
//							"remit_bank.name" => "农业银行", // 汇款账户
//							"remit_person" => "某某某", // 汇款人姓名
//							"remit_account" => "6222xxxxxxxx" // 汇款人银行账号
//						)
//					)
//				)
//			)
//		));
		// $order_params = json_encode($order_params);
		// $params = array_merge($params, $order_params);
		// $params = json_encode($params);
//		$url = 'http://192.168.35.18/index.php/api';
		$snoopy->submit($url, $params);
		$res = $snoopy->results;
		p($res);
		exit;


		if (in_array($params['object'] . '_' . $params['action'], $user_auth['activity_auth_arr'])) {
			$snoopy->submit($url, $params);
			$res = $snoopy->results;
			p($res);
			exit;
//			p(json_decode($res, true));
		}
	}

	public function add1() {//预存款订单
		include("lib/snoopy.inc.php");
		$this->load->library('session');
		$this->load->model('www/user_model', 'user');

		$params = array();
		$snoopy = new snoopy();
		$url = 'http://127.0.0.1/leegii/index.php/api';

		$user_id = $this->session->userdata('user_id');
		$res_user = $this->db->select('user_login_name, user_password')->get_where('tc_user', array('user_id' => $user_id))->row_array();
		$user_auth = $this->user->user_auth($user_id);

		$params['action'] = 'add'; //操作行动
		$params['object'] = 'order'; //对象
		$params['user'] = 'licheng'; //账号
		$params['pwd'] = '1'; //密码
		$params['data'] = array(
			'number' => 'licheng-test-0140', // 订单编号
			'type.name' => "销售订单", // 订单类型
			'create_user_id.gonghao' => "s1619", // 创建人
			'create_time' => "2014-03-31 13:57:56", // 创建时间
			'amount' => '1000', // 金额
			'rebate_amount' => '1000', //可返点总金额
			'account.account_shopexid' => '661311477178', // 所属客户.shopexid
			'agent.shopex_id' => '11121212', // 所属经销商.shopexid
			'owner.gonghao' => "s1619", //渠道经理（所属销售）.工号
			'review.gonghao' => "s1619", //业务主管.工号
			'finance.gonghao' => 's1619', //财务人员.工号
			'nreview.gonghao' => 's1619', //内审人员.工号
			'department.name' => '网络直销部-上海', //所属部门.名称
			'agreement_name' => '测试合同名称001', //合同名称
			'agreement_no' => 'ht-test-001', //合同编号
			'detailed' => array(
				'order_d' => array(
					array(
						'goods.code' => 'goods_0141', // 商品编号
						'goods_amount' => 123000, // 折后价
						'goods_price.code' => 'gp00000000393' // 商品原价格编号
					),
					array('goods.code' => 'group_0030', 'goods_amount' => 123000, 'goods_price.code' => 'gp00000000392')
				),
			),
		);
		$params['data'] = json_encode($params['data']);
//		$url = 'http://192.168.35.18/index.php/api';
		$snoopy->submit($url, $params);
		$res = $snoopy->results;
		p($res);
	}

	public function add22() {//预存款订单
		include("lib/snoopy.inc.php");
		$this->load->library('session');
		$this->load->model('www/user_model', 'user');

		$params = array();
		$snoopy = new snoopy();
		$url = 'http://localhost/framework/index.php/api';

		$user_id = $this->session->userdata('user_id');
		$res_user = $this->db->select('user_login_name, user_password')->get_where('tc_user', array('user_id' => $user_id))->row_array();
		$user_auth = $this->user->user_auth($user_id);

		$params['user'] = $res_user['user_login_name'];
		$params['pwd'] = $res_user['user_password'];
		// $params['user'] = 'ww';
		// $params['pwd'] = '1';
		$order_params = array(
			'action' => 'add',
			'object' => 'invoice',
			'data' => array(
				'order_id' => '1',
				'title' => '票据抬头',
				'type.name' => '增值税发票',
			)
		);

		$params = array_merge($params, $order_params);
		$params = json_encode($params);

		$snoopy->submit($url, $params);
		$res = $snoopy->results;
		p($res);
		exit;
	}

	public function curl_post() {
		$email = '@' . dirname(__FILE__) . '/email.txt'; //注意一定要在文件路径前加@
		$url = 'http://pro.shopex.cn/index.php/openapi/api/post';
		$params = array(
			'starttime' => 1395352800,
			'endtime' => 1395460800,
			'file' => $email,
			'sign' => '5baf628bc8b7e08fe8148f5a29578b3c'
		);
		$scheme = parse_url($url, PHP_URL_SCHEME);
		var_dump($scheme, $params);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0;)');
		curl_setopt($ch, CURLOPT_POST, count($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		//要注意https
		if ($scheme == 'https') {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		}
		$content = curl_exec($ch);
		$response = curl_getinfo($ch);
		curl_close($ch);
		var_dump($content, $response);
	}

	public function crm() {
		$data = '110110635108';
		//$lee_url = '192.168.0.18:8088';
		$lee_url = '192.168.35.14'; //CRM升级负载均衡换IP
		$lee_orgcode = 'ShopEx'; //单位名称
		$wsURL = "http://" . $lee_url . "/webservice/service.php?orgcode=" . $lee_orgcode . "&class=WS_System&wsdl";
		$params = array("admin", "hql444h3l");
		$header = null;
		$client = new SoapClient($wsURL);
		$session = $client->__soapCall("login", $params, array(), $header);
		$wsURL = "http://" . $lee_url . "/webservice/service.php?orgcode=" . $lee_orgcode . "&class=WS_EAI&wsdl";
		$queryXML = '<Interface type="query" model="object" value ="Account"><Condition><Express field="Account.customerID" operator="=" value="' . $data . ' "/></Condition></Interface>';
		$params = array($queryXML);
		$header = new SoapHeader("http://" . $_SERVER['HTTP_HOST'], "PHPSESSID", $session);
		$client = new SoapClient($wsURL);
		$resultXML = $client->__soapCall("process", $params, array(), $header);
		$resultObj = simplexml_load_string($resultXML);
		$resultArr = get_object_vars($resultObj->Row); //转换成数组
		p($resultArr);
		exit;
	}

}

?>