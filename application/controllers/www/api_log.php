<?php
class Api_log extends WWW_Controller{
	public $menu1='api_log';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/api_log_model','api_log');
	}

	public function index(){
		$this->api_log->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->api_log->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->api_log->attributeLabels();
		$data['listLayout']=$this->api_log->listLayout();
		$this->render('www/api_log/list',$data);
	}
	public function ajax_select(){
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["api_log_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		if (isset($_GET['type_id'])){ 
			if($_GET['type_id']!="" and $_GET['type_id']!=0){
				$where['type_id']=$_GET['type_id']; 
			}
		}
		
		$data['listData']=$this->api_log->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);
		$data["labels"]=$this->api_log->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取
		
		

		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->api_log->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/api_log/ajax_select',$data);
	}
	public function add(){
		$this->api_log->db->query('SET NAMES UTF8');
		$data["labels"]=$this->api_log->attributeLabels();
		
		
		
		
		
		
		
		
		$this->load->model('admin/enum_model','enum');$data["api_log_status_enum"]=$this->enum->getlist(624);
		
		
		
		
		
		if(!empty($_POST)){
		if(isset($_GET['type_id'])){
           $_POST['api_log']['type_id']=$_GET['type_id'];
           $this->api_log->add($_POST['api_log']);
		    success('www/api_log?type_id='.$_GET['type_id'],'创建成功');
		}else{
		  $this->api_log->add($_POST['api_log']);
		  success('www/api_log','创建成功');
		}
			
		}
		$this->render('www/api_log/add',$data);
	}
	public function update(){
		$this->api_log->db->query('SET NAMES UTF8');
		$data["labels"]=$this->api_log->attributeLabels();
		$data['id_aData']=$this->api_log->id_aGetInfo($_GET['api_log_id']);
		$data['listLayout']=$this->api_log->listLayout();
		
		
		
		
		
		
		
		
		$this->load->model('admin/enum_model','enum');
		$data["api_log_status_enum"]=$this->enum->getlist(624);
		
		
		
		
		
		if(!empty($_POST)){
		    if(isset($_GET['type_id'])){
		      $_POST['api_log']['type_id']=$_GET['type_id'];
		      $this->api_log->update($_POST['api_log'],$_GET['api_log_id']);
			    success('www/api_log?type_id='.$_GET['type_id'],'更新成功');
		    }else{
              	$this->api_log->update($_POST['api_log'],$_GET['api_log_id']);
			    success('www/api_log','更新成功');
		    }
		}
		$this->render('www/api_log/update',$data);
	}
	public function view(){
		$this->api_log->db->query('SET NAMES UTF8');
        $data["labels"]=$this->api_log->attributeLabels();
		$data['listLayout']=$this->api_log->id_aGetInfo($_GET['api_log_id']);
		$this->render('www/api_log/view',$data);
	}
    public function ajax_list(){
       $this->api_log->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['listData']=$this->api_log->listGetInfo('',$_GET['page'],$_GET['perNumber']);
		$data['totalNumber'] = $this->api_log->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->api_log->attributeLabels();
		$data['listLayout']=$this->api_log->listLayout();
		$this->output->enable_profiler(false);
		$this->load->view('www/api_log/ajax_list',$data);
    }
	public function ajax_select_quote(){
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["api_log_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		
		$data['listData']=$this->api_log->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);
		$data["labels"]=$this->api_log->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取
		
		

		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->api_log->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/api_log/ajax_select_quote',$data);
	}

}
?>
