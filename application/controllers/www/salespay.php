<?php

class Salespay extends WWW_Controller {

	public $menu1 = 'salespay';

	public function __construct() {
		parent::__construct();
		$this->load->model('www/salespay_model', 'salespay');
		$this->salespay->db->query('SET NAMES UTF8');
		$this->load->model('www/system_log_model', 'system_log');
		$this->system_log->db->query('SET NAMES UTF8');
		$this->load->model('www/message_model', 'message');
		$this->message->db->query('SET NAMES UTF8');
		$this->load->model('www/order_model', 'order');
		$this->order->db->query('SET NAMES UTF8');
		//$this->output->enable_profiler(TRUE);
	}

	public function index() {
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data = $this->get_data_auth('sale_order');
		$data['totalNumber'] = $this->salespay->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"] = $this->salespay->attributeLabels();
		$data['listLayout'] = $this->salespay->listLayout();
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
		if (in_array('salespay_list', $user_auth['activity_auth_arr'])) {
			$this->render('www/salespay/list', $data);
		} else {
			$this->load->view('www/layouts/operation_403');
		}
	}

	public function add() {
		$this->salespay->db->query('SET NAMES UTF8');
		$data["labels"] = $this->salespay->attributeLabels();
		$this->load->model('admin/enum_model', 'enum');
		$data["salespay_pay_method_enum"] = $this->enum->getlist(144);
		$this->load->model('admin/enum_model', 'enum');
		$data["salespay_status_enum"] = $this->enum->getlist(146);
		if (!empty($_POST)) {
			if (isset($_GET['type_id'])) {
				$_POST['salespay']['type_id'] = $_GET['type_id'];
				$this->salespay->add($_POST['salespay']);
				success('www/salespay?type_id=' . $_GET['type_id'], '创建成功');
			} else {
				$this->salespay->add($_POST['salespay']);
				success('www/salespay', '创建成功');
			}
		}
		$this->render('www/salespay/add', $data);
	}

	public function update() {
		$data["labels"] = $this->salespay->attributeLabels();
		$data['id_aData'] = $this->salespay->id_aGetInfo($_GET['salespay_id']);
		$data['listLayout'] = $this->salespay->listLayout();

		$this->load->model('admin/enum_model', 'enum');
		$data["salespay_pay_method_enum"] = $this->enum->getlist(144);

		$this->load->model('admin/enum_model', 'enum');
		$data["salespay_status_enum"] = $this->enum->getlist(146);

		if (!empty($_POST)) {
			if (isset($_GET['type_id'])) {
				$_POST['salespay']['type_id'] = $_GET['type_id'];
				$this->salespay->update($_POST['salespay'], $_GET['salespay_id']);
				success('www/salespay?type_id=' . $_GET['type_id'], '更新成功');
			} else {
				$this->salespay->update($_POST['salespay'], $_GET['salespay_id']);
				success('www/salespay', '更新成功');
			}
		}
		$this->render('www/salespay/update', $data);
	}

	//老查询接口格式改成新格式
	public function where_format($where) {
		//这里可以判断下是否已经有[where_all]属性！如果已有！证明本来就是新格式，不用转化
		if (isset($where['where_all'])) {
			return $where;
		} else {
			//首先处理select_json
			$new_where = $where;
			if (isset($where["select_json"]) and $where["select_json"] != "") {
				$select_arr = json_decode($where["select_json"], true);
				$new_where['select_arr'] = array(
					$select_arr['attr'] => $select_arr['value'],
				);
			} else {
				$new_where['select_arr'] = "";
			}

			//把条件组合起来主要是where、get_attr和select_arr
			//循环出get_attr
			$where_all = $new_where['where'];
			if (isset($new_where['where_rel']) and $new_where['where_rel'] != "") {
				$rel_all = "(" . $new_where['where_rel'] . ")";
			} else {
				$rel_all = "";
			}

			if (isset($new_where['get_attr']) and $new_where['get_attr'] != "") {
				$i = 0;
				$ga_rel = "";
				foreach ($new_where['get_attr'] as $key => $value) {
					$i++;
					$where_all['ga' . $i] = array(
						'attr' => $key,
						'value' => $value,
						'action' => '=',
					);
					if ($ga_rel == "") {
						$ga_rel = 'ga' . $i;
					} else {
						$ga_rel = $ga_rel . ' and ga' . $i;
					}
				}
				if ($rel_all !== "") {
					$rel_all = $rel_all . " and (" . $ga_rel . ")";
				} else {
					$rel_all = "(" . $ga_rel . ")";
				}
			}

			if ($new_where['select_arr'] != "") {
				$i = 0;
				$sa_rel = "";
				foreach ($new_where['select_arr'] as $key => $value) {
					$i++;
					$where_all['sa' . $i] = array(
						'attr' => $key,
						'value' => $value,
						'action' => 'like',
					);
					if ($sa_rel == "") {
						$sa_rel = 'sa' . $i;
					} else {
						$sa_rel = $sa_rel . ' and sa' . $i;
					}
				}
				if ($rel_all != "") {
					$rel_all = $rel_all . " and (" . $sa_rel . ")";
				} else {
					$rel_all = "(" . $sa_rel . ")";
				}
			}
			$new_where_ok = array(
				"where_all" => $where_all,
				"rel_all" => $rel_all,
				"page" => $new_where['page'],
				"perNumber" => $new_where['perNumber'],
				"obj" => $new_where['obj'],
			);
			//p($new_where_ok );exit;
			return $new_where_ok;
		}
	}

	//用新接口查询
	public function ajax_select() {
		$post = $_POST;
		$post['obj'] = 'salespay';
		$where = $this->where_format($post);
		//p($where);
		//exit;
		$this->load->model('www/api_model', 'api');
		$listData = $this->api->select($where);
		//p($listData);
		$data["labels"] = $this->salespay->attributeLabels();
		$data['listData'] = $listData['info'];
		$data['totalNumber'] = $listData['count']; //数据总数
		$data['sel_data'] = $listData['sel_data'];
		//p($data['sel_data']);
		$data['perNumber'] = $_POST['perNumber']; //每页显示的记录数
		$data['page'] = $_POST['page']; //当前页数
		$this->load->view('www/salespay/ajax_select', $data);
	}

	public function ajax_select_quote() {
		if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
			$select_arr = json_decode($_POST["select_json"], true);
			$like["salespay_" . $select_arr['attr']] = $select_arr['value'];
		} else {
			//$select_arr = "";
			$like = "";
		}
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$where = "";

		$data['listData'] = $this->salespay->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
		$data["labels"] = $this->salespay->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取



		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['totalNumber'] = $this->salespay->countGetInfo($where, $like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/salespay/ajax_select_quote', $data);
	}

	public function ajax_list() {
		$this->salespay->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['listData'] = $this->salespay->listGetInfo('', $_GET['page'], $_GET['perNumber']);
		$data['totalNumber'] = $this->salespay->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"] = $this->salespay->attributeLabels();
		$data['listLayout'] = $this->salespay->listLayout();
		$this->output->enable_profiler(false);
		$this->load->view('www/salespay/ajax_list', $data);
	}

	//入账记录 明细上的列表查询
	public function detailed_list_view_into() {
		$this->salespay->db->query('SET NAMES UTF8');
		if (isset($_GET['order_id'])) {
			//p($_GET['order_id']);exit;
			$where = array(
				'salespay_order_id' => $_GET['order_id'],
				'salespay_money_type' => '1001',
			);

			$data['orderData'] = $this->order->id_aGetInfo($_GET['order_id']); 
			//获取订单的相关数据
			//p($data['orderData']);
			
			$data['listData'] = $this->salespay->GetInfo($where);
			$data["labels"] = $this->salespay->attributeLabels();
			
			$data['listLayout'] = $this->salespay->listLayout();
			//p($data['listData']);
			$this->output->enable_profiler(false);
			$this->load->view('www/salespay/detailed_list_view_into', $data);
		} else {
			echo "不能没有ID";
		}
	}

	//出账记录 明细上的列表查询
	public function detailed_list_view_out() {
		$this->salespay->db->query('SET NAMES UTF8');
		if (isset($_GET['order_id'])) {
			$where = array(
				'salespay_order_id' => $_GET['order_id'],
				'salespay_money_type' => '1002',
			);
			// $data['orderData'] = $this->order->id_aGetInfo($_GET['order_id']); //获取订单的相关数据
			//p($data['orderData']);
			$data['listData'] = $this->salespay->GetInfo($where);
			$data["labels"] = $this->salespay->attributeLabels();
			$data['listLayout'] = $this->salespay->listLayout();
			//p($data['listData']);
			$this->output->enable_profiler(false);
			$this->load->view('www/salespay/detailed_list_view_out', $data);
		} else {
			echo "不能没有ID";
		}
	}

	//ajax新增 入款项
	public function ajax_add_into() {
		if (isset($_GET['order_id'])) {
			$data["order_data"] = $this->order->id_aGetInfo($_GET['order_id']);
			$order_id = $data["order_data"]['order_id'];
			//已确认到帐
			$where = array(
				'salespay_order_id'   => $order_id,
				'salespay_status'     => 1002,
				'salespay_money_type' => 1001,
			);
			$data["ydz_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //已到帐
			//未确认到账
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status !=' => 1002,
				'salespay_money_type' => 1001,
			);
			$data["wdz_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //未到账
			//p($this->db->last_query());
			//可到帐金额 订单总金额-已到帐-未到账=可到帐
			$data["kdz_amount"] = $data["order_data"]['order_amount'] - $data["ydz_amount"] - $data["wdz_amount"];
			$data["labels"] = $this->salespay->attributeLabels();
			$this->load->model('admin/enum_model', 'enum');
			$data["salespay_pay_method_enum"] = $this->enum->getlist(144); //支付方式
			$this->load->model('admin/enum_model', 'enum');
			$data["salespay_status_enum"] = $this->enum->getlist(146); //到帐状态
			$this->load->model('admin/enum_model', 'enum');
			$data["my_bank_account_enum"] = $this->enum->getlist(363); //商派银行账号
			$this->load->model('admin/enum_model', 'enum');
			$data["my_alipay_account_enum"] = $this->enum->getlist(364); //商派支付宝账号
			$this->load->model('admin/enum_model', 'enum');
			$data["money_type_enum"] = $this->enum->getlist(545); //款项类型
			$this->load->model('admin/enum_model', 'enum');
			$data["account_bank_type_enum"] = $this->enum->getlist(560); //客户账号类型
			$this->load->model('admin/enum_model', 'enum');
			$data["salespay_subsidiary_enum"] = $this->enum->getlist(629); //所属子公司 2014/08/25
			$this->load->view('www/salespay/ajax_add_into', $data);
		} else {
			echo "不能没有ID";
		}
	}

	//ajax新增 出款项
	public function ajax_add_out() {
		//p($_GET);
		if (isset($_GET['order_id']) and isset($_GET['money_type_name'])) {
			$data['money_type_name'] = $_GET['money_type_name'];
			$data["order_data"] = $this->order->id_aGetInfo($_GET['order_id']);
			$order_id = $data["order_data"]['order_id'];
			//已确认到帐
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status' => 1002,
			);
			$data["ydz_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //已到帐

			//提货销售订单退款，可退款金额等于订单总金额
			if($data['order_data']['type_id'] == 13){
				$data["ydz_amount"] = $data['order_data']['order_amount'];
			}

			//未确认到账
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status !=' => 1002,
			);
			$data["wdz_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //未到账
			//可到帐金额 订单总金额-已到帐-未到账=可到帐
			$data["kdz_amount"] = $data["order_data"]['order_amount'] - $data["ydz_amount"] - $data["wdz_amount"];

			//已确认返点
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status' => 1002,
				'salespay_money_type_name' => 1002,
			);
			$data["yfd_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //已返点
			//未确认返点
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status !=' => 1002,
				'salespay_money_type_name' => 1002,
			);
			$data["wfd_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //未返点
			//可返点金额 订单返点总金额-已返点-未返点=可返点
			$data["kfd_amount"] = $data["order_data"]['order_rebate_amount'] - $data["yfd_amount"] - $data["wfd_amount"];

			//已确认退款
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status' => 1002,
				'salespay_money_type_name' => 1003,
			);
			$data["ytk_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //已退款
			//未确认退款
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status !=' => 1002,
				'salespay_money_type_name' => 1003,
			);
			$data["wtk_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //未退款
			//可退款金额 订单已确认到帐-已退款-未退款=可退款
			$data["ktk_amount"] = $data["ydz_amount"] - $data["ytk_amount"] - $data["wtk_amount"];

			$data["labels"] = $this->salespay->attributeLabels();
			$this->load->model('admin/enum_model', 'enum');
			$data["salespay_pay_method_enum"] = $this->enum->getlist(144); //支付方式
			$this->load->model('admin/enum_model', 'enum');
			$data["salespay_status_enum"] = $this->enum->getlist(146); //到帐状态
			$this->load->model('admin/enum_model', 'enum');
			$data["my_bank_account_enum"] = $this->enum->getlist(363); //商派银行账号
			$this->load->model('admin/enum_model', 'enum');
			$data["my_alipay_account_enum"] = $this->enum->getlist(364); //商派支付宝账号
			$this->load->model('admin/enum_model', 'enum');
			$data["money_type_enum"] = $this->enum->getlist(545); //款项类型
			$this->load->model('admin/enum_model', 'enum');
			$data["account_bank_type_enum"] = $this->enum->getlist(560); //客户账号类型

			if ($data['money_type_name'] == 1002) {//返点款项
				$this->load->view('www/salespay/ajax_add_out_rebate', $data);
			}
			if ($data['money_type_name'] == 1003) {//退款项
				$this->load->view('www/salespay/ajax_add_out_refund', $data);
			}
		} else {
			echo "参数不正确";
		}
	}

	//ajax编辑
	public function ajax_edit() {
		//echo 111;exit;
		$this->salespay->db->query('SET NAMES UTF8');
		if (isset($_GET['salespay_id'])) {
			$data["salespay_data"] = $this->salespay->id_aGetInfo($_GET['salespay_id']);
			$order_id = $data["salespay_data"]['salespay_order_id_arr']['order_id'];
			$data["order_data"] = $this->order->id_aGetInfo($order_id);
			//已确认到帐
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status' => 1002,
				'salespay_id !=' => $data["salespay_data"]['salespay_id'],
			);
			$data["ydz_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //已到帐
			//未确认到账
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status !=' => 1002,
				'salespay_id !=' => $data["salespay_data"]['salespay_id'],
			);
			$data["wdz_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //未到账
			//可到帐金额 订单总金额-已到帐-未到账=可到帐
			$data["kdz_amount"] = $data["salespay_data"]['salespay_order_id_arr']['order_amount'] - $data["ydz_amount"] - $data["wdz_amount"];


			//已确认返点
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status' => 1002,
				'salespay_money_type_name' => 1002,
				'salespay_id !=' => $data["salespay_data"]['salespay_id'],
			);
			$data["yfd_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //已返点
			//未确认返点
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status !=' => 1002,
				'salespay_money_type_name' => 1002,
				'salespay_id !=' => $data["salespay_data"]['salespay_id'],
			);
			$data["wfd_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //未返点
			//可返点金额 订单返点总金额-已返点-未返点=可返点
			$data["kfd_amount"] = $data["order_data"]['order_rebate_amount'] - $data["yfd_amount"] - $data["wfd_amount"];

			//已确认退款
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status' => 1002,
				'salespay_money_type_name' => 1003,
				'salespay_id !=' => $data["salespay_data"]['salespay_id'],
			);
			$data["ytk_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //已退款
			//未确认退款
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status !=' => 1002,
				'salespay_money_type_name' => 1003,
			);
			$data["wtk_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //未退款
			//可退款金额 订单已确认到帐-已退款-未退款=可退款
			$data["ktk_amount"] = $data["ydz_amount"] - $data["ytk_amount"] - $data["wtk_amount"];


			$data["labels"] = $this->salespay->attributeLabels();
			$this->load->model('admin/enum_model', 'enum');
			$data["salespay_pay_method_enum"] = $this->enum->getlist(144); //支付方式
			$this->load->model('admin/enum_model', 'enum');
			$data["salespay_status_enum"] = $this->enum->getlist(146); //到帐状态
			$this->load->model('admin/enum_model', 'enum');
			$data["my_bank_account_enum"] = $this->enum->getlist(363); //商派银行账号
			$this->load->model('admin/enum_model', 'enum');
			$data["my_alipay_account_enum"] = $this->enum->getlist(364); //商派支付宝账号
			$this->load->model('admin/enum_model', 'enum');
			$data["money_type_enum"] = $this->enum->getlist(545); //款项类型
			$this->load->model('admin/enum_model', 'enum');
			$data["account_bank_type_enum"] = $this->enum->getlist(560); //客户账号类型
			$this->load->model('admin/enum_model', 'enum');
			$data["salespay_subsidiary_enum"] = $this->enum->getlist(629); //所属子公司 2014/08/25
			if ($data['salespay_data']['salespay_money_type'] == 1002) {
				if ($data['salespay_data']['salespay_money_type_name'] == '1002') {
					//这里代表返点款项
					
					$this->load->view('www/salespay/ajax_edit_out_rebate', $data);
				}
				if ($data['salespay_data']['salespay_money_type_name'] == '1003') {
					$data['salespay_data']['product_json'] = json_decode($data['salespay_data']['salespay_refund_product_json'], true);
					//这里代表退款项
					
					$this->load->view('www/salespay/ajax_edit_out_refund', $data);
				}
			} else {
				$this->load->view('www/salespay/ajax_edit_into', $data);
			}
		} else {
			echo "不能没有ID";
		}
	}

	//ajax查看
	public function ajax_view() {
		$this->salespay->db->query('SET NAMES UTF8');
		if (isset($_GET['salespay_id'])) {
			$data["salespay_data"] = $this->salespay->id_aGetInfo($_GET['salespay_id']);

			$order_id = $data["salespay_data"]['salespay_order_id_arr']['order_id'];
			//已确认到帐
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status' => 1001,
				'salespay_id !=' => $data["salespay_data"]['salespay_id'],
			);
			$data["ydz_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //已到帐
			//未确认到账
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status !=' => 1001,
				'salespay_id !=' => $data["salespay_data"]['salespay_id'],
			);
			$data["wdz_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //未到账
			//可到帐金额 订单总金额-已到帐-未到账=可到帐
			$data["kdz_amount"] = $data["salespay_data"]['salespay_order_id_arr']['order_amount'] - $data["ydz_amount"] - $data["wdz_amount"];

			$data["labels"] = $this->salespay->attributeLabels();

			//p($data['salespay_data']['salespay_my_bank_account_arr']['enum_name']);//exit;
			if ($data['salespay_data']['salespay_money_type'] == 1002) {
				if ($data['salespay_data']['salespay_money_type_name'] == '1002') {
					//这里代表返点款项
					$this->load->view('www/salespay/ajax_view_out_rebate', $data);
				}
				if ($data['salespay_data']['salespay_money_type_name'] == '1003') {
					//这里代表退款项
					$this->load->view('www/salespay/ajax_view_out_refund', $data);
				}
			} else {
				$this->load->view('www/salespay/ajax_view_into', $data);
			}
		} else {
			echo "不能没有ID";
		}
	}

	//在写一个确认到帐页面
	public function ajax_confirm_salespay() {
		if (isset($_GET['salespay_id'])) {
			$data["salespay_data"] = $this->salespay->id_aGetInfo($_GET['salespay_id']);
			$order_id = $data["salespay_data"]['salespay_order_id_arr']['order_id'];
			//已确认到帐
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status' => 1001,
			);
			$data["ydz_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //已到帐
			//未确认到账
			$where = array(
				'salespay_order_id' => $order_id,
				'salespay_status' => "",
			);
			$data["wdz_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //未到账
			//可到帐金额 订单总金额-已到帐-未到账=可到帐
			$data["kdz_amount"] = $data["salespay_data"]['salespay_order_id_arr']['order_amount'] - $data["ydz_amount"] - $data["wdz_amount"];
			$data["labels"] = $this->salespay->attributeLabels();
			if ($data['salespay_data']['salespay_money_type'] == 1002) {
				//这里代表出账款项
				if ($data['salespay_data']['salespay_money_type_name'] == '1002') {
					//这里代表返点款项
					$this->load->view('www/salespay/ajax_confirm_salespay_out_rebate', $data);
				}
				if ($data['salespay_data']['salespay_money_type_name'] == '1003') {
					//这里代表退款项
					$this->load->view('www/salespay/ajax_confirm_salespay_out_refund', $data);
				}
			} else {
				//这里代表入账款项
				$this->load->view('www/salespay/ajax_confirm_salespay_into', $data);
			}
		} else {
			echo "不能没有ID";
		}
	}

	//ajax调用的新增
	public function ajax_add_post() {
		if ($_POST) {
			$salespay_data = $_POST['salespay'];

			if (!isset($salespay_data['salespay_status'])) {
				$salespay_data['salespay_status'] = '1001';
			}

			if (!isset($salespay_data['salespay_money_type'])) {
				$salespay_data['salespay_money_type'] = '1001';
			}

			if (!isset($salespay_data['salespay_money_type_name'])) {
				$salespay_data['salespay_money_type_name'] = '1001';
			}

			if ($salespay_data['salespay_money_type'] == '1003') {
				//如果是退款项，需要记录产品信息
				$salespay_data['salespay_goods_info_json'] = json_encode($_POST['goods_info']);
				//循环整理成文本（用于给财务显示,同时接口打过来直接打文本就好！）
				//p($_POST['goods_info']);exit;
				$salespay_data['salespay_goods_info_txt'] = "";
				foreach ($_POST['goods_info'] as $goods_k => $goods_v) {
					$salespay_data['salespay_goods_info_txt'] = $salespay_data['salespay_goods_info_txt'] . '商品名称：' . $goods_v['goods_name'] . " 折后价格：" . $goods_v['basic_disc'] . " 已使用天数：" . $goods_v['days'] . " 已使用费用：" . $goods_v['cost'] . " 退款金额：" . $goods_v['amount'] . "\n\r";
				}
			}
			//p($salespay_data['salespay_goods_info_txt']);exit;
			$order_data = $this->order->id_aGetInfo($salespay_data['salespay_order_id']);
			// 申请入款项金额判断
			if (!$this->salespay->checkPayAmount($salespay_data, $order_data)) {
				die('amount');
			}

			//修正 入账/出账 保存和日志
			switch ($salespay_data['salespay_money_type_name']) {
				case '1001':
					//日志
					$system_log_operation = '新增入款项';
					$message_owner = $order_data['order_finance'];
					$message_module = 'salespay';
					$message_type = '1001'; //确认到账
					$message_url = 'www/salespay/ajax_confirm_salespay';
					break;
				case '1002':
					//日志
					$system_log_operation = '新增返点款项';
					$message_owner = $salespay_data['salespay_reviewer'];
					$message_module = 'rebate';
					$message_type = '1020'; //出账审核
					$message_url = 'www/salespay/ajax_check_salespay';
					break;
				case '1003':
					if (isset($_POST['goods_info'])) {
						$salespay_data['salespay_refund_product_json'] = json_encode($_POST['goods_info']);
					}
					//日志
					$system_log_operation = '新增退款项';
					$message_owner = $salespay_data['salespay_reviewer'];
					$message_module = 'refund';
					$message_type = '1020'; //出账审核
					$message_url = 'www/salespay/ajax_check_salespay';
					break;
			}
			
			$this->salespay->add($salespay_data); //添加 入账/出账
			
			$salespay_id = $this->db->insert_id(); //获取 salespay_id

			$log_data = array(
				'system_log_addtime' => date('Y-m-d H:i:s', time()),
				'system_log_user_id' => $this->session->userdata('user_id'),
				'system_log_operation' => $system_log_operation,
				'system_log_note' => $salespay_data['salespay_pay_note'],
				'system_log_module' => 'Order',
				'system_log_module_id' => $salespay_data['salespay_order_id']
			);
			$message_data = array(
				'message_owner' => $message_owner,
				'message_module' => $message_module,
				'message_module_id' => $salespay_id,
				'message_type' => $message_type,
				'message_department' => $order_data['order_finance_arr']['user_department'],
				'message_url' => $message_url
			);
			$this->system_log->add($log_data);
			$this->message->add($message_data);
			echo 1;
		} else {
			echo '失败';
		}
	}

	//ajax调用的编辑
	public function ajax_update_post() {
		if ($_POST) {
			if (!$this->session->userdata('user_id'))
				die(', 请重新登陆'); //2014-6-17 add
			$salespay_data = $_POST['salespay'];
			if (isset($_POST['payinfo'])) {
				$salespay_data['salespay_pay_info'] = json_encode($_POST['payinfo']);
			}
			if (!isset($salespay_data['salespay_status'])) {
				$salespay_data['salespay_status'] = 1001;
			}
			//这里添加个回传渠道接口状态接口
			//cus lee 2014-04-23 同步渠道 start
			//首先要判断出是不是渠道打过来的订单
			$salespay_sql_data = $this->salespay->id_aGetInfo($salespay_data['salespay_id']);

			//保存时候出入款项处理
			switch ($salespay_sql_data['salespay_money_type_name']) {
				case '1001': //入账
					$message_module = 'salespay';
					break;
				case '1002': //返点
					$salespay_data['salespay_money_type_name'] = 1002;
					$message_module = 'rebate';
					break;
				case '1003': //退款
					if (!empty($_POST['goods_info'])) {
						$salespay_data['salespay_refund_product_json'] = json_encode($_POST['goods_info']);
						$salespay_data['salespay_money_type_name'] = 1003;
						//计算退款总金额
						$all_amount = $goods_cost = $goods_amount = 0;
						foreach ($_POST['goods_info'] as $v) {
							$all_amount += $v['basic_disc'];
							$goods_cost += $v['cost'];
							$goods_amount += $v['amount'];
						}
						if (($goods_cost + $goods_amount) > $all_amount)
							die('，请检查退款金额');
						$salespay_data['salespay_pay_amount'] = $goods_amount;
					}
					$message_module = 'refund';
					break;
			}

			if ($salespay_sql_data['salespay_order_id_arr']['order_source'] == 1003) { 	//当来源为【渠道】的订单
				//调用打渠道的接口
				$params['action'] = 'ForeDepositApply'; //预存款和销售订单的审核
				//p($salespay_sql_data['salespay_order_id_arr']['type_id']);//exit;
				if ($salespay_sql_data['salespay_order_id_arr']['type_id'] == 8) {//返点订单打这个
					$params['action'] = 'SalesReturnMoneyOrderApply';
				}
				// 新增 保证金方法
				$order_id  = $salespay_data['salespay_order_id'];
				$order_sql = "select * from tc_order where order_id = '$order_id'";
				$order_data = $this->db->query($order_sql)->result_array();
				$order_data = $order_data[0];
				// 新增 保证金方法
				if($order_data['order_deposit_paytype'] == "1001" && ($salespay_sql_data['salespay_order_id_arr']['type_id'] == "10")){
					$params['action'] = 'ForeDepositReturnMoneyApply';
				}
				$params['id'] = $salespay_sql_data['salespay_number']; //传给营收的工单编码
				//p($params['id']);exit;
				//p($salespay_data['salespay_status']);exit;
				if ($salespay_data['salespay_status'] == 1002) { //当选择确认款项的时候
					$salespay_data['salespay_create_time'] = date("Y-m-d H:i:s");
					$params['result'] = 'ok'; //审核成功
					$params['remark'] = 'XXXXXXX'; //审核备注
					$params['time'] = date("Y-m-d H:i:s"); //审核时间 (用当前时间)
					$res = $this->qudao_api($params);
					if ($res['res'] == 'fail') {
//						echo $res['msg'];exit;
					} else {
						//p($res);
					}
				} else if ($salespay_data['salespay_status'] == 1003) { //当选择款项不成功的时候
					$params['result'] = 'failure'; //审核失败
					$params['remark'] = 'XXXXXXX'; //审核备注
					$params['time'] = date("Y-m-d H:i:s"); //审核时间 (用当前时间)
					$res = $this->qudao_api($params);
					if ($res['res'] == 'fail') {
						//echo $res['msg'];exit;
					} else {
						//p($res);
						//p($params);
					}
				}
			}
			//cus lee 2014-04-23 同步渠$道 end
			$order_data = $this->order->id_aGetInfo($salespay_data['salespay_order_id']);
			$this->salespay->update($salespay_data, $salespay_data['salespay_id']);
			if ($salespay_data['salespay_status'] == 1002) { //已确认到帐
				$log_data = array(
					'system_log_addtime' => date('Y-m-d H:i:s', time()),
					'system_log_user_id' => $this->session->userdata('user_id'),
					'system_log_operation' => '确认到账',
					'system_log_module' => 'Order',
					'system_log_module_id' => $salespay_data['salespay_order_id']
				);
				$message_data = array(
					'message_status' => 1002
				);
				$where1 = array('message_module_id' => $salespay_data['salespay_id']);
				$this->message->update($message_data, $where1);
				$this->load->model('www/books_model', 'books');
				//日记账 确认到账时间
				$res_order = $this->db->select('order_number,order_department,order_owner,order_finance')->get_where('tc_order', array('order_id' => $salespay_data['salespay_order_id']))->row_array();
				$books_bdate = isset($salespay_data['salespay_sp_real_date']) ? $salespay_data['salespay_sp_real_date'] : date('Y-m-d H:i:s', time());
				$this->books->update(array('books_state' => 1002, 'books_department_id' => $res_order['order_department'], 'books_order_no' => $res_order['order_number'], 'books_bdate' => $salespay_data['salespay_sp_real_date'], 'books_attn_id' => $res_order['order_owner'], 'books_accounting' => $res_order['order_finance']), $salespay_data['salespay_book_id']);

				// 线上订单，改变订单类型
				if ($order_data['type_id'] == 7) {
					$save_order = array(
						'type_id' => 6
					);
					$this->order->update($save_order, $salespay_data['salespay_order_id']);
				}
			} else if ($salespay_data['salespay_status'] == 1001) { //未确认到帐
				$log_data = array(
					'system_log_addtime' => date('Y-m-d H:i:s', time()),
					'system_log_user_id' => $this->session->userdata('user_id'),
					'system_log_operation' => '编辑入款项',
					'system_log_module' => 'Order',
					'system_log_module_id' => $salespay_data['salespay_order_id'],
					'system_log_note' => $salespay_data['salespay_pay_note']
				);
				$message1_data = array(
					'message_status' => 1002
				);

				$where1 = array('message_module_id' => $salespay_data['salespay_id'], 'message_type' => 1010);
				$this->message->update($message1_data, $where1);

				//编辑款项，如果是入款项保存财务，否则保存业务主管
				if ($message_module == 'salespay') {
					$message_owner = $order_data['order_finance'];
					$message_url = 'www/salespay/ajax_confirm_salespay';
					$message_type = 1001;
				} else {
					$message_owner = $salespay_data['salespay_reviewer']; //指定销售主管
					$message_url = 'www/salespay/ajax_check_salespay';
					$message_type = 1020;
				}
				$message_data = array(
					'message_owner' => $message_owner,
					'message_module' => $message_module,
					'message_module_id' => $salespay_data['salespay_id'],
					'message_type' => $message_type,
					'message_department' => $order_data['order_finance_arr']['user_department'],
					'message_url' => $message_url
				);
				$this->message->add($message_data);
				$this->salespay->update($salespay_data, $salespay_data['salespay_id']);
			} else {
				$log_data = array(
					'system_log_addtime' => date('Y-m-d H:i:s', time()),
					'system_log_user_id' => $this->session->userdata('user_id'),
					'system_log_operation' => '到账不成功',
					'system_log_module' => 'Order',
					'system_log_module_id' => $salespay_data['salespay_order_id']
				);
				$message1_data = array(
					'message_status' => 1002
				);
				$where1 = array('message_module_id' => $salespay_data['salespay_id'], 'message_type' => 1001);
				$this->message->update($message1_data, $where1);

				$message_data = array(
					'message_owner' => $order_data['order_typein'],
					'message_module' => $message_module,
					'message_module_id' => $salespay_data['salespay_id'],
					'message_type' => 1010,
					//'message_department'=>$order_data['order_typein_arr']['user_department'],
					'message_url' => 'www/salespay/ajax_edit'
				);
				$this->message->add($message_data);
			}

			$this->system_log->add($log_data);
			echo 1;
		} else {
			echo '失败??';
		}
	}

	//ajax调用的删除
	public function ajax_del_post() {
		if ($_POST) {
			//p($_POST);
			$salespay_id = $_POST['salespay_id'];
			$this->salespay->del($salespay_id);
			$log_data = array(
				'system_log_addtime' => date('Y-m-d H:i:s', time()),
				'system_log_user_id' => $this->session->userdata('user_id'),
				'system_log_operation' => '删除入款项',
				'system_log_module' => 'Order',
				'system_log_module_id' => $_POST['order_id']
			);
			//p($log_data);
			$this->system_log->add($log_data);
			echo 1;
		} else {
			echo '失败??';
		}
	}
	//渠道
	public function qudao_api($params) {
		include_once(FCPATH."/Classes/lib/snoopy.inc.php");
		$snoopy = new Snoopy();
		//$url="http://192.168.0.96:9014/api/review";  //正式环境
		$url = "http://192.168.35.28:9014/api/review";  //测试环境
		//同步渠道
		$snoopy->submit($url, $params);
		$res = $snoopy->results;
		//print_r($res);
		return json_decode($res, true);
	}

	//审核出账
	public function ajax_check_salespay() {
		if (isset($_GET['salespay_id'])) {
			$data["salespay_data"] = $this->salespay->id_aGetInfo($_GET['salespay_id']);
			$order_id = $data["salespay_data"]['salespay_order_id_arr']['order_id'];
			$where = array(// 已确认到帐
				'salespay_order_id' => $order_id,
				'salespay_status' => 1001,
			);
			$data["ydz_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); // 已到帐
			$where = array(// 未确认到账
				'salespay_order_id' => $order_id,
				'salespay_status' => "",
			);
			$data["wdz_amount"] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); // 未到账
			// 可到帐金额 订单总金额-已到帐-未到账=可到帐
			$data["kdz_amount"] = $data["salespay_data"]['salespay_order_id_arr']['order_amount'] - $data["ydz_amount"] - $data["wdz_amount"];
			$data["labels"] = $this->salespay->attributeLabels();
			if ($data['salespay_data']['salespay_money_type'] == 1002) {
				//这里代表出账款项
				if ($data['salespay_data']['salespay_money_type_name'] == '1002') {
					//这里代表返点款项
					$this->load->view('www/salespay/ajax_check_salespay_out_rebate', $data);
				}
				if ($data['salespay_data']['salespay_money_type_name'] == '1003') {
					//这里代表退款项
					$this->load->view('www/salespay/ajax_check_salespay_out_refund', $data);
				}
			} else {
				die('data error');
			}
		} else {
			die('不能没有ID');
		}
	}

	public function ajax_check_post_update() {
		if ($_POST) {
			if (!$this->session->userdata('user_id'))
				die('请重新登陆'); //2014-6-17 add
			$salespay_post = $_POST['salespay'];
//			p($salespay_post);exit;
			$this->salespay->update($salespay_post, $salespay_post['salespay_id']);
			//更新待办
			$message_data = array('message_status' => 1002);
			$where1 = array('message_module_id' => $salespay_post['salespay_id'], 'message_owner' => $this->session->userdata('user_id'));
			$this->message->update($message_data, $where1);

			//审核通过，财务增加待办
			$order_data = $this->order->id_aGetInfo($salespay_post['salespay_order_id']);
			$salespay_data = $this->salespay->id_aGetInfo($salespay_post['salespay_id']);
			$message_module = $salespay_data['salespay_money_type_name'] == '1002' ? 'rebate' : 'refund';
			if ($salespay_data['salespay_status'] == 1005) { //通过审核
				$message_data = array(
					'message_owner' => $order_data['order_finance'],
					'message_module' => $message_module,
					'message_module_id' => $salespay_data['salespay_id'],
					'message_type' => 1001,
					'message_department' => $order_data['order_finance_arr']['user_department'],
					'message_url' => 'www/salespay/ajax_confirm_salespay'
				);
			} else {
				/*
				* 新增拒绝 功能 传送给渠道接口 20140925 start
				* 调用打渠道的接口
				*/
				$params['action']   =  'ForeDepositApply'; //预存款和销售订单的审核
				if ($salespay_data['salespay_order_id_arr']['type_id'] == 8) {//返点订单打这个
					$params['action'] = 'SalesReturnMoneyOrderApply';
				}
				$params['id'] 		=   $salespay_data['salespay_number']; //传给营收的工单编码
				$params['result'] 	=   'failure'; //审核失败
				$params['remark'] 	=   'XXXXXXX'; //审核备注
				$params['time'] 	=   date("Y-m-d H:i:s"); //审核时间 (用当前时间)
				$res 				=   $this->qudao_api($params);
				//新增拒绝 功能 传送给渠道接口 20140925 end
				$message_data = array(
					'message_owner' => $order_data['order_typein'],
					'message_module' => $message_module,
					'message_module_id' => $salespay_data['salespay_id'],
					'message_type' => 1010,
					'message_department' => $order_data['order_finance_arr']['user_department'],
					'message_url' => 'www/salespay/ajax_edit'
				);
			}
			$this->message->add($message_data);
			die('1');
		} else {
			die('none post');
		}
	}

}

?>