<?php 
   class Menu_Auth extends WWW_Controller{
   	 public $menu1='auth';
   	 public function __construct(){
		parent::__construct();
		$this->load->model('www/menu_auth_model','auth_menu');
		
	 }
	 public function index(){
      $auth_menu_data = $this->auth_menu->check($_GET['role_id']);
      $data['menu_auth_check']=json_decode($auth_menu_data[0]['role_menu_auth']);
      //p($data['menu_auth_check']);
	   $this->load->model('admin/menu_model', 'menu');
   	   $data['labels'] = $this->menu->attributeLabels();
       $this->menu->db->query('SET NAMES UTF8');
       $data['list'] = $this->menu->menulist();
      $this->render('www/menu_auth/list.php',$data);
    }
    public function add(){
    	if(!empty($_POST)){
    		$data['role_menu_auth']=json_encode($_POST);
    	 $tem=array();
          foreach ($_POST as $key => $value) {
            if($key!='sub'){
               $tem[]=$key;
  
            }
          }
          $data['role_menu_auth']=json_encode($tem);

      }else{
      	$data['role_menu_auth']=null;
      }
    	          $this->auth_menu->update($this->input->get('role_id'),$data);
          success('www/role','操作成功');
    	//p($data['role_menu_auth']);
    }
}