<?php

class Goods extends WWW_Controller {

    public $menu1 = 'goods';

    public function __construct() {
        parent::__construct();
        $this->load->model('www/goods_model', 'goods');
    }

    public function index() {
        $this->goods->db->query('SET NAMES UTF8');
         $user_auth = $this->user->user_auth($this->session->userdata('user_id'));
        if (!in_array('goods_list', $user_auth['activity_auth_arr'])) {
            $this->load->view('www/layouts/operation_403');
        } else {
             if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data['totalNumber'] = $this->goods->countGetInfo(); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        $data["labels"] = $this->goods->attributeLabels();
        $data['listLayout'] = $this->goods->listLayout();
        $this->render('www/goods/list', $data);
        }

    }

    public function add() {
        $this->goods->db->query('SET NAMES UTF8');
        $data["labels"] = $this->goods->attributeLabels();

        $this->load->model('admin/enum_model', 'enum');
        $data["goods_type_enum"] = $this->enum->getlist(402);
        $data["goods_visable_enum"] = $this->enum->getlist(404);
        $data["goods_is_trail_enum"] = $this->enum->getlist(405);
        $data["goods_is_sale_enum"] = $this->enum->getlist(406);
        $data["goods_sale_type_enum"] = $this->enum->getlist(407);
        $data["goods_check_pay_enum"] = $this->enum->getlist(409);

        if (!empty($_POST)) {
            if (isset($_GET['type_id'])) {
                $_POST['goods']['type_id'] = $_GET['type_id'];
                $this->goods->add($_POST['goods']);
                success('www/goods?type_id=' . $_GET['type_id'], '创建成功');
            } else {
                $this->goods->add($_POST['goods']);
                success('www/goods', '创建成功');
            }
        }
        $this->render('www/goods/add', $data);
    }

    public function update() {
        $this->goods->db->query('SET NAMES UTF8');
        $data["labels"] = $this->goods->attributeLabels();
        $data['id_aData'] = $this->goods->id_aGetInfo($_GET['goods_id']);
        $data['listLayout'] = $this->goods->listLayout();

        $this->load->model('admin/enum_model', 'enum');
        $data["goods_type_enum"] = $this->enum->getlist(402);
        $data["goods_visable_enum"] = $this->enum->getlist(404);
        $data["goods_is_trail_enum"] = $this->enum->getlist(405);
        $data["goods_is_sale_enum"] = $this->enum->getlist(406);
        $data["goods_sale_type_enum"] = $this->enum->getlist(407);
        $data["goods_check_pay_enum"] = $this->enum->getlist(409);

        if (!empty($_POST)) {
            if (isset($_GET['type_id'])) {
                $_POST['goods']['type_id'] = $_GET['type_id'];
                $this->goods->update($_POST['goods'], $_GET['goods_id']);
                success('www/goods?type_id=' . $_GET['type_id'], '更新成功');
            } else {
                $this->goods->update($_POST['goods'], $_GET['goods_id']);
                success('www/goods', '更新成功');
            }
        }
        $this->render('www/goods/update', $data);
    }

    public function ajax_list() {
        $this->goods->db->query('SET NAMES UTF8');
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data['listData'] = $this->goods->listGetInfo('', $_GET['page'], $_GET['perNumber']);
        $data['totalNumber'] = $this->goods->countGetInfo(); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        $data["labels"] = $this->goods->attributeLabels();
        $data['listLayout'] = $this->goods->listLayout();
        $this->output->enable_profiler(false);
        $this->load->view('www/goods/ajax_list', $data);
    }

    public function ajax_select() {
        if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
            $select_arr = json_decode($_POST["select_json"], true);
            $like["goods_" . $select_arr['attr']] = $select_arr['value'];
        } else {
            //$select_arr = "";
            $like = "";
        }
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $where = "";
        if (isset($_GET['type_id'])) {
            if ($_GET['type_id'] != "" and $_GET['type_id'] != 0) {
                $where['type_id'] = $_GET['type_id'];
            }
        }

        $data['listData'] = $this->goods->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
        $data["labels"] = $this->goods->attributeLabels();
        //如果包含对象类型，添加一个数组，用于视图获取

        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data['totalNumber'] = $this->goods->countGetInfo($where, $like); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        //p($data);
        $this->output->enable_profiler(false);
        $this->load->view('www/goods/ajax_select', $data);
    }

    public function ajax_select_quote() {
        if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
            $select_arr = json_decode($_POST["select_json"], true);
            $like["goods_" . $select_arr['attr']] = $select_arr['value'];
        } else {
            //$select_arr = "";
            $like = "";
        }
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $where = "";

        $data['listData'] = $this->goods->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
        $data["labels"] = $this->goods->attributeLabels();
        //如果包含对象类型，添加一个数组，用于视图获取

        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data['totalNumber'] = $this->goods->countGetInfo($where, $like); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        //p($data);
        $this->output->enable_profiler(false);
        $this->load->view('www/goods/ajax_select_quote', $data);
    }

    public function api($starttime = '') {
        date_default_timezone_set('Asia/Shanghai');
        if (empty($starttime)) {
            $starttime = 1356969600;
        } elseif ($starttime > time()) {
            die('finish');
        }

        $endtime = $starttime + 108000;
        $res = $this->goods->sync($starttime, $endtime);
        $url = 'www/goods/api/' . $endtime;
        if ($res) {
            if ($res['line']) {
                echo $starttime . '<->' . $endtime . ' ' . $res['line'] . ' 出错';
                exit;
            }
            usleep(500);
            redirect($url, 'refresh');
        } else {
            // 没有返回内容，继续抓取数据
            echo date('Y-m-d h:m', $starttime) . ' <-> ' . date('Y-m-d h:m', $endtime) . ' 没有数据 ' . $starttime;
            usleep(500);
            redirect($url, 'refresh');
        }
    }

    public function view() {
        $this->goods->db->query('SET NAMES UTF8');
        $data['products'] = $this->goods->getGoodsProduct($_GET['goods_id']);
        $this->load->model('www/goods_price_model', 'gp');
        $data['goods_price'] = $this->gp->GetInfo(array('goods_price_goods_id' => $_GET['goods_id']));
        $data["labels"] = $this->goods->attributeLabels();
        $data['listLayout'] = $this->goods->id_aGetInfo($_GET['goods_id']);
        $this->render('www/goods/view', $data);
    }

}

?>