<?php

class Books extends WWW_Controller {

    public $menu1 = 'books';
    private $cellArray = array(
        1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E',
        6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J',
        11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O',
        16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T',
        21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y',
        26 => 'Z',
        27 => 'AA', 28 => 'AB', 29 => 'AC', 30 => 'AD', 31 => 'AE',
        32 => 'AF', 33 => 'AG', 34 => 'AH', 35 => 'AI', 36 => 'AJ',
        37 => 'AK', 38 => 'AL', 39 => 'AM', 40 => 'AN', 41 => 'AO',
        42 => 'AP', 43 => 'AQ', 44 => 'AR', 45 => 'AS', 46 => 'AT',
        47 => 'AU', 48 => 'AV', 49 => 'AW', 50 => 'AX', 51 => 'AY',
        52 => 'AZ', 53 => 'BA', 54 => 'BB', 55 => 'BC', 56 => 'BD', 57 => 'BE',
        58 => 'BF', 59 => 'BG', 60 => 'BH', 61 => 'BI', 62 => 'BJ', 63 => 'BK', 64 => 'BL');

    public function __construct() {
        parent::__construct();
        $this->load->model('www/books_model', 'books');
    }

    public function index() {
        $this->books->db->query('SET NAMES UTF8');
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data = $this->get_data_auth('sale_order');
        $data['totalNumber'] = $this->books->countGetInfo(); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        $data["labels"] = $this->books->attributeLabels();
        $data['listLayout'] = $this->books->listLayout();

        $data['seniorquery_attr'] = Array
            (
            Array
                (
                'name' => 'subsidiary',
                'label' => '所属子公司',
                'attrtype' => '14',
                'sub_attr' => Array
                    (
                    'enum' => Array
                        (
                        0 => Array
                            (
                            'enum_name' => '商派网络',
                            'enum_key' => 1001
                        ),
                        1 => Array
                            (
                            'enum_name' => '商派软件',
                            'enum_key' => 1002
                        ),
                        2 => Array
                            (
                            'enum_name' => '酷美',
                            'enum_key' => 1003
                        ),
						3 => Array
                            (
                            'enum_name' => '有量',
                            'enum_key' => 1004
                        )
                    )
                )
            ),
            Array
                (
                'name' => 'bank_account',
                'label' => '账户',
                'attrtype' => '15',
                'sub_attr' => Array
                    (
                    'enum' => Array
                        (
                        0 => Array
                            (
                            'enum_name' => '工行基本户',
                            'enum_key' => 1001
                        ),
                        1 => Array
                            (
                            'enum_name' => '现金',
                            'enum_key' => 1002
                        ),
                        2 => Array
                            (
                            'enum_name' => '招行（现）',
                            'enum_key' => 1003
                        ),
                        3 => Array
                            (
                            'enum_name' => '工行（现）',
                            'enum_key' => 1004
                        ),
                        4 => Array
                            (
                            'enum_name' => '建行（现）',
                            'enum_key' => 1005
                        ),
                        5 => Array
                            (
                            'enum_name' => '农行（现）',
                            'enum_key' => 1006
                        ),
                        6 => Array
                            (
                            'enum_name' => '中行（现）',
                            'enum_key' => 1007
                        ),
                        7 => Array
                            (
                            'enum_name' => 'Paypal（贝宝）',
                            'enum_key' => 1008
                        ),
                        8 => Array
                            (
                            'enum_name' => '支付宝（payment@shopex.cn）',
                            'enum_key' => 1009
                        ),
                        9 => Array
                            (
                            'enum_name' => '支付宝（alipay_f@shopex.cn）',
                            'enum_key' => 1010
                        ),
                        10 => Array
                            (
                            'enum_name' => '支付宝（wanggou@shopex.cn）',
                            'enum_key' => 1011
                        ),
                        11 => Array
                            (
                            'enum_name' => '支付宝（tp_taobao@shopex.cn）',
                            'enum_key' => 1012
                        ),
                        12 => Array
                            (
                            'enum_name' => '支付宝（shangpai@shopex.cn）',
                            'enum_key' => 1013
                        ),
                        13 => Array
                            (
                            'enum_name' => '支付宝（weishangye@shopex.cn）',
                            'enum_key' => 1014
                        ),
                        14 => Array
                            (
                            'enum_name' => '支付宝（b2btaobao@shopex.cn）',
                            'enum_key' => 1015
                        ),
                        15 => Array
                            (
                            'enum_name' => '支付宝（sms_alipay@shopex.cn）',
                            'enum_key' => 1016
                        ),
                        16 => Array
                            (
                            'enum_name' => '支付宝（wdwd_alipay@shopex.cn）',
                            'enum_key' => 1017
                        ),
                        17 => Array
                            (
                            'enum_name' => '支付宝（iwancu@shopex.cn）',
                            'enum_key' => 1018
                        ),
                        18 => Array
                            (
                            'enum_name' => '中国银行（基本户）',
                            'enum_key' => 1019
                        ),
                        19 => Array
                            (
                            'enum_name' => '上海银行（一般户）',
                            'enum_key' => 1020
                        ),
                        20 => Array
                            (
                            'enum_name' => '交通银行（一般户）',
                            'enum_key' => 1021
                        ),
                        21 => array(
                            "enum_name" => "支付宝（alipay_ec@shopex.cn）",
                            "enum_key" => 1022
                        ),
                        22 => array(
                            "enum_name" => "招行（一般户）",
                            "enum_key" => 1023
                        ),
                        23 => array(
                            "enum_name" => "交通银行（基本户）商派软件",
                            "enum_key" => 1024
                        ),
                        24 => array(
                            "enum_name" => "支付宝（software@shopex.cn）商派软件",
                            "enum_key" => 1025
                        ),
                        25 => array(
                            "enum_name" => "SMS@shopex.cn（商派软件）",
                            "enum_key" => 1026
                        ),
                        26 => array(
                            "enum_name" => "招行延安东路支行（商派软件）",
                            "enum_key" => 1027
                        ),
						27 => array(
							"enum_name" => "交行基本户（有量）",
							"enum_key" => 1028
						),
						28 => array(
							"enum_name" => "支付宝（sms@shopex.cn）",
							"enum_key" => 1029
						),
						29 => array(
							"enum_name" => "财付通（商派网络）",
							"enum_key" => 1030
						),
						30 => array(
							"enum_name" => "百付宝（商派网络）",
							"enum_key" => 1031
						),
						31 => array(
							"enum_name" => "财付通（商派软件）",
							"enum_key" => 1032
						)
                    )
                )
            ),
            Array
                (
                'name' => 'adate',
                'label' => '实际到账/出账日期',
                'attrtype' => '17'
            ),
            Array
                (
                'name' => 'bdate',
                'label' => '确认到账/出账日期',
                'attrtype' => '17'
            ),
            Array
                (
                'name' => 'trade_no',
                'label' => '交易号',
                'attrtype' => '8'
            ),
            Array
                (
                'name' => 'attn_id',
                'label' => '经办人',
                'attrtype' => '19',
                'sub_attr' => Array
                    (
                    'url' => site_url('www/user/ajax_list'),
                    'title' => '人员',
                    'quote_name' => 'user'
                )
            ),
            Array
                (
                'name' => 'department_id',
                'label' => '部门',
                'attrtype' => '19',
                'sub_attr' => Array
                    (
                    'url' => site_url('www/department/ajax_list'),
                    'title' => '部门',
                    'quote_name' => 'department'
                )
            ),
            Array
                (
                'name' => 'customer_info',
                'label' => '客户信息',
                'attrtype' => '8'
            ),
            Array
                (
                'name' => 'sale_no',
                'label' => '销售记录号',
                'attrtype' => '8'
            ),
            Array
                (
                'name' => 'order_no',
                'label' => '订单号',
                'attrtype' => '8'
            ),
            Array
                (
                'name' => 'Note',
                'label' => '摘要',
                'attrtype' => '9'
            ),
            Array
                (
                'name' => 'debit_amount',
                'label' => '借方发生额',
                'attrtype' => '7'
            ),
            Array
                (
                'name' => 'Credit_amount',
                'label' => '贷方发生额',
                'attrtype' => '7'
            ),
            Array
                (
                'name' => 'cashier',
                'label' => '出纳',
                'attrtype' => '19',
                'sub_attr' => Array
                    (
                    'url' => site_url('www/user/ajax_list'),
                    'title' => '人员',
                    'quote_name' => 'user'
                )
            ),
            Array
                (
                'name' => 'accounting',
                'label' => '会计',
                'attrtype' => '19',
                'sub_attr' => Array
                    (
                    'url' => site_url('www/user/ajax_list'),
                    'title' => '人员',
                    'quote_name' => 'user'
                )
            ),
            Array
                (
                'name' => 'sales_no',
                'label' => '销售号',
                'attrtype' => '8'
            ),
            Array
                (
                'name' => 'number',
                'label' => '日记账编号',
                'attrtype' => '8'
            )
        );

        $user_auth = $this->user->user_auth($this->session->userdata('user_id'));
        if (in_array('books_list', $user_auth['activity_auth_arr'])) {
            $this->render('www/books/list', $data);
        } else {
            $this->load->view('www/layouts/operation_403');
        }
    }

    public function view() {
        if (!isset($_SERVER['HTTP_REFERER'])) {
            $this->load->view('www/layouts/illegal');
        } else {
//首先通过ID获取
            if (isset($_GET)) {
                $data["labels"] = $this->books->attributeLabels();
                $data['data'] = $this->books->id_aGetInfo($_GET['books_id']);
                $this->render('www/books/view', $data);
            } else {
                echo "参数错误";
                exit;
            }
        }
    }

    //老查询接口格式改成新格式
    public function where_format($where) {
        //这里可以判断下是否已经有[where_all]属性！如果已有！证明本来就是新格式，不用转化
        if (isset($where['where_all'])) {
            return $where;
        } else {
            //首先处理select_json
            $new_where = $where;
            if (!isset($where['page'])) {
                $new_where['page'] = 1;
            }
            if (isset($where["select_json"]) and $where["select_json"] != "") {
                $select_arr = json_decode($where["select_json"], true);
                $new_where['select_arr'] = array(
                    $select_arr['attr'] => $select_arr['value'],
                );
            } else {
                $new_where['select_arr'] = "";
            }
            //把条件组合起来主要是where、get_attr和select_arr
            //首先判断是否是高级查询！如果是高级查询则先处理高级查询
            if (isset($where["seniorquery"]) and $where["seniorquery"] != "") {
                //高级查询处理
                //高级查询rel里的数字的需要处理啊一下的！因为可能和别的有冲突
                foreach ($where["seniorquery"]['where'] as $sw_k => $sw_v) {
                    $new_where["seniorquery"]['where']['sq' . $sw_k] = $sw_v;
                    unset($new_where['seniorquery']['where'][$sw_k]); //去掉原值
                    $new_where["seniorquery"]['rel'] = str_replace($sw_k, 'sq' . $sw_k, $new_where["seniorquery"]['rel']);
                }
                //p($where["seniorquery"]['where']);exit;
                //把条件组合起来主要是where、get_attr和select_arr
                //循环出get_attr
                $where_all = $new_where["seniorquery"]['where'];
                if (isset($new_where["seniorquery"]['rel']) and $new_where["seniorquery"]['rel'] != "") {
                    $rel_all = "(" . $new_where["seniorquery"]['rel'] . ")";
                } else {
                    $rel_all = "";
                }
            } else {
                //不是高级查询
                $where_all = isset($new_where['where']) ? $new_where['where'] : '';
                if (isset($new_where['where_rel']) and $new_where['where_rel'] != "") {
                    $rel_all = "(" . $new_where['where_rel'] . ")";
                } else {
                    $rel_all = "";
                }
            }

            //循环出get_attr
            if (isset($new_where['get_attr']) and $new_where['get_attr'] != "") {
                $i = 0;
                $ga_rel = "";
                foreach ($new_where['get_attr'] as $key => $value) {
                    $i++;
                    $where_all['ga' . $i] = array(
                        'attr' => $key,
                        'value' => $value,
                        'action' => '=',
                    );
                    if ($ga_rel == "") {
                        $ga_rel = 'ga' . $i;
                    } else {
                        $ga_rel = $ga_rel . ' and ga' . $i;
                    }
                }
                if ($rel_all !== "") {
                    $rel_all = $rel_all . " and (" . $ga_rel . ")";
                } else {
                    $rel_all = "(" . $ga_rel . ")";
                }
            }

            if ($new_where['select_arr'] != "") {
                $i = 0;
                $sa_rel = "";
                foreach ($new_where['select_arr'] as $key => $value) {
                    $i++;
                    $where_all['sa' . $i] = array(
                        'attr' => $key,
                        'value' => $value,
                        'action' => 'like',
                    );
                    if ($sa_rel == "") {
                        $sa_rel = 'sa' . $i;
                    } else {
                        $sa_rel = $sa_rel . ' and sa' . $i;
                    }
                }
                if ($rel_all != "") {
                    $rel_all = $rel_all . " and (" . $sa_rel . ")";
                } else {
                    $rel_all = "(" . $sa_rel . ")";
                }
            }
            $new_where['perNumber'] = isset($new_where['perNumber']) ? $new_where['perNumber'] : '';
            $new_where_ok = array(
                "where_all" => $where_all,
                "rel_all" => $rel_all,
                "page" => $new_where['page'],
                "perNumber" => $new_where['perNumber'],
                "obj" => $new_where['obj'],
            );
//			p($new_where_ok); exit;
            return $new_where_ok;
        }
    }

    public function add() {
        $this->books->db->query('SET NAMES UTF8');
        $data["labels"] = $this->books->attributeLabels();

        $this->load->model('admin/enum_model', 'enum');

        $data["books_company_enum"] = $this->enum->getlist(114);
        //增加所属子公司 2014/08/25
        $data["books_subsidiary_enum"] = $this->enum->getlist(628);

        $data["books_bank_account_enum"] = $this->enum->getlist(115);

        $data["books_invoice_track_enum"] = $this->enum->getlist(133);

        $data["books_state_enum"] = $this->enum->getlist(136);

        if (!empty($_POST)) {
            if (isset($_GET['type_id'])) {
                $_POST['books']['type_id'] = $_GET['type_id'];
                $this->books->add($_POST['books']);
                success('www/books?type_id=' . $_GET['type_id'], '创建成功');
            } else {
                $this->books->add($_POST['books']);
                success('www/books', '创建成功');
            }
        }
        $this->render('www/books/add', $data);
    }

    public function update() {
        $this->books->db->query('SET NAMES UTF8');
        $data["labels"] = $this->books->attributeLabels();
        $data['id_aData'] = $this->books->id_aGetInfo($_GET['books_id']);
        $data['listLayout'] = $this->books->listLayout();

        $this->load->model('admin/enum_model', 'enum');
        $data["books_company_enum"] = $this->enum->getlist(114);
		
		//增加所属子公司 2014/08/25
        $data["books_subsidiary_enum"] = $this->enum->getlist(628);

        $this->load->model('admin/enum_model', 'enum');
        $data["books_bank_account_enum"] = $this->enum->getlist(115);

        $this->load->model('admin/enum_model', 'enum');
        $data["books_invoice_track_enum"] = $this->enum->getlist(133);

        $this->load->model('admin/enum_model', 'enum');
        $data["books_state_enum"] = $this->enum->getlist(136);

        if (!empty($_POST)) {
            if (isset($_GET['type_id'])) {
//				$_POST['books']['type_id'] = $_GET['type_id'];
                $res = $this->books->update($_POST['books'], $_GET['books_id']);
                success('www/books?type_id=' . $_GET['type_id'], '更新成功');
            } else {
                $_POST['books'] = array_filter($_POST['books']);
                $this->books->update($_POST['books'], $_GET['books_id']);
                success('www/books', '更新成功');
            }
        }
        $this->render('www/books/update', $data);
    }

    public function ajax_select() {
        $where = $_POST;
        if (isset($where['seniorquery'])) {
            //循环1次，去掉所有attr为0的
            foreach ($where['seniorquery']['where'] as $k => $v) {
                if ($v['attr'] == '0') {
                    unset($where['seniorquery']['where'][$k]);
                }
            }
        }
        $where['obj'] = 'books';
        $select_data = $this->where_format($where); //转化成api的格式查询
        $this->load->model('www/api_model', 'api');
        $listData = $this->api->select($select_data);
//		p($listData);
        if (isset($listData['res'])) {
            echo "查询失败！" . $listData['msg'];
            exit;
        }

        $data["labels"] = $this->books->attributeLabels();
        $data['listData'] = $listData['info'];
        $data['totalNumber'] = $listData['count']; //数据总数
//		p($data['totalNumber']);exit;
        $data['sel_data'] = $listData['sel_data'];
        if (isset($_POST['page'])) {
            $data['page'] = $_POST['page'];
        } else {
            $data['page'] = 1; //默认第一页
        }

        $data['user_auth'] = $this->user->user_auth($this->session->userdata('user_id'));
        $this->load->view('www/books/ajax_select', $data);
    }

    public function ajax_select_quote() {
        $where = $_POST;
        //p($where);
        if (isset($where['seniorquery'])) {
            //循环1次，去掉所有attr为0的
            foreach ($where['seniorquery']['where'] as $k => $v) {
                if ($v['attr'] == '0') {
                    unset($where['seniorquery']['where'][$k]);
                }
            }
        }
        $where['obj'] = 'books';
        //这里单独去掉get传值，因为JS会获取order上面的传值
        if (isset($where['get_attr'])) {
            unset($where['get_attr']);
        }
        $select_data = $this->where_format($where); //转化成api的格式查询
        //添加一个条件
        $select_data['where_all']['lee1'] = array(
            'attr' => 'order_no',
            'action' => 'NULL'
        );

        if ($select_data['rel_all'] != "") {
            $select_data['rel_all'] = $select_data['rel_all'] . 'and (lee1)';
        } else {
            $select_data['rel_all'] = '(lee1)';
        }
        $this->load->model('www/api_model', 'api');
        //p($select_data);
        $listData = $this->api->select($select_data);
        //exit;
        //p($listData);
        if (isset($listData['res'])) {
            echo "查询失败！" . $listData['msg'];
            exit;
        }

        $data["labels"] = $this->books->attributeLabels();
        $data['listData'] = $listData['info'];
        $data['totalNumber'] = $listData['count']; //数据总数
        $data['sel_data'] = $listData['sel_data'];
        //p($data['sel_data']);
        if (isset($_POST['page'])) {
            $data['page'] = $_POST['page'];
        } else {
            $data['page'] = 1; //默认第一页
        }
        $this->load->view('www/books/ajax_select_quote', $data);
    }

    //旧版，暂时用作参考，正式的好了就可以删除掉了
    public function new_ajax_select_quote() {
        //p($_POST);
        //p($_GET);
        if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
            $select_arr = json_decode($_POST["select_json"], true);
            $like["books_" . $select_arr['attr']] = $select_arr['value'];
        } else {
//$select_arr = "";
            $like = "";
        }
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $where['books_state'] = null;
        $data['listData'] = $this->books->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
        $data["labels"] = $this->books->attributeLabels();
//如果包含对象类型，添加一个数组，用于视图获取

        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data['totalNumber'] = $this->books->countGetInfo($where, $like); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
//p($data);
        $this->output->enable_profiler(false);
        $this->load->view('www/books/ajax_select_quote', $data);
    }

    public function ajax_list() {
        $this->books->db->query('SET NAMES UTF8');
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        //增加日记账判断 判断如果日记账已经关联订单不予显示
        $where = array(
            "books_order_no !=" => "",
        );
        //增加日记账判断 判断如果日记账已经关联订单不予显示
        $data['listData'] = $this->books->listGetInfo($where, $_GET['page'], $_GET['perNumber']);
        $data['totalNumber'] = $this->books->countGetInfo(); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        $data["labels"] = $this->books->attributeLabels();
        $data['listLayout'] = $this->books->listLayout();

        //直接查询该对象的类型来获取相关数据 start----------------------
        /*
          $this->load->model( "admin/attribute_model", 'attr' );
          $this->attr->db->query( "set names utf8" );
          $obj_attr = $this->attr->check( 27 );
          foreach ( $obj_attr as $k => $v ) {
          $data['seniorquery_attr'][$k] = array(
          'name' => $v['attr_name'],
          'label' => $v['attr_label'],
          'attrtype' => $v['attr_type'],
          );
          //引用类型得多加几个参数
          if ( $v['attr_type'] == 19 ) {
          $data['seniorquery_attr'][$k]['sub_attr'] = array(
          'url' => site_url( 'www/' . $v['attr_quote_id_arr']['obj_name'] . '/ajax_list' ),
          'title' => $v['attr_quote_id_arr']['obj_label'],
          'quote_name' => $v['attr_quote_id_arr']['obj_name'],
          );
          }
          //枚举类型得多加几个参数
          if ( $v['attr_type'] == 14 or $v['attr_type'] == 15 ) {
          //查询出关联枚举的内容
          $this->load->model( 'admin/enum_model', 'enum' );
          $enum_arr = $this->enum->getlist( $v['attr_id'] );
          //循环去掉一些没必要的属性
          foreach ( $enum_arr as $k2 => $v2 ) {
          unset( $enum_arr[$k2]['enum_id'] );
          unset( $enum_arr[$k2]['attr_id'] );
          unset( $enum_arr[$k2]['disp_order'] );
          unset( $enum_arr[$k2]['is_default'] );
          unset( $enum_arr[$k2]['system_flag'] );
          }
          $data['seniorquery_attr'][$k]['sub_attr'] = array(
          'enum' => $enum_arr,
          );
          }
          }
          //p($data['seniorquery_attr']);
          p($this->arrayeval($data['seniorquery_attr']));
          //eval("\$arr = ".$str.'; ');
         */
        //直接查询该对象的类型来获取相关数据 end----------------------
        //手动写高级查询字段

        $data['seniorquery_attr'] = Array
            (
            Array
                (
                'name' => 'company',
                'label' => '所属子公司',
                'attrtype' => '14',
                'sub_attr' => Array
                    (
                    'enum' => Array
                        (
                        0 => Array
                            (
                            'enum_name' => '商派网络',
                            'enum_key' => 1001
                        ),
                        1 => Array
                            (
                            'enum_name' => '商派软件',
                            'enum_key' => 1002
                        ),
                        2 => Array(
                            'enum_name' => '酷美',
                            'enum_key' => 1003
                        )
                    )
                )
            ),
            Array
                (
                'name' => 'bank_account',
                'label' => '账户',
                'attrtype' => '15',
                'sub_attr' => Array
                    (
                    'enum' => Array
                        (
                        0 => Array
                            (
                            'enum_name' => '工行基本户',
                            'enum_key' => 1001
                        ),
                        1 => Array
                            (
                            'enum_name' => '现金',
                            'enum_key' => 1002
                        ),
                        2 => Array
                            (
                            'enum_name' => '招行（现）',
                            'enum_key' => 1003
                        ),
                        3 => Array
                            (
                            'enum_name' => '工行（现）',
                            'enum_key' => 1004
                        ),
                        4 => Array
                            (
                            'enum_name' => '建行（现）',
                            'enum_key' => 1005
                        ),
                        5 => Array
                            (
                            'enum_name' => '农行（现）',
                            'enum_key' => 1006
                        ),
                        6 => Array
                            (
                            'enum_name' => '中行（现）',
                            'enum_key' => 1007
                        ),
                        7 => Array
                            (
                            'enum_name' => 'Paypal（贝宝）',
                            'enum_key' => 1008
                        ),
                        8 => Array
                            (
                            'enum_name' => '支付宝（payment@shopex.cn）',
                            'enum_key' => 1009
                        ),
                        9 => Array
                            (
                            'enum_name' => '支付宝（alipay_f@shopex.cn）',
                            'enum_key' => 1010
                        ),
                        10 => Array
                            (
                            'enum_name' => '支付宝（wanggou@shopex.cn）',
                            'enum_key' => 1011
                        ),
                        11 => Array
                            (
                            'enum_name' => '支付宝（tp_taobao@shopex.cn）',
                            'enum_key' => 1012
                        ),
                        12 => Array
                            (
                            'enum_name' => '支付宝（shangpai@shopex.cn）',
                            'enum_key' => 1013
                        ),
                        13 => Array
                            (
                            'enum_name' => '支付宝（weishangye@shopex.cn）',
                            'enum_key' => 1014
                        ),
                        14 => Array
                            (
                            'enum_name' => '支付宝（b2btaobao@shopex.cn）',
                            'enum_key' => 1015
                        ),
                        15 => Array
                            (
                            'enum_name' => '支付宝（sms_alipay@shopex.cn）',
                            'enum_key' => 1016
                        ),
                        16 => Array
                            (
                            'enum_name' => '支付宝（wdwd_alipay@shopex.cn）',
                            'enum_key' => 1017
                        ),
                        17 => Array
                            (
                            'enum_name' => '支付宝（iwancu@shopex.cn）',
                            'enum_key' => 1018
                        ),
                        18 => Array
                            (
                            'enum_name' => '中国银行（基本户）',
                            'enum_key' => 1019
                        ),
                        19 => Array
                            (
                            'enum_name' => '上海银行（一般户）',
                            'enum_key' => 1020
                        ),
                        20 => Array
                            (
                            'enum_name' => '交通银行（一般户）',
                            'enum_key' => 1021
                        ),
                        21 => array(
                            "enum_name" => "支付宝（alipay_ec@shopex.cn）",
                            "enum_key" => 1022
                        ),
                        22 => array(
                            "enum_name" => "招行（一般户）",
                            "enum_key" => 1023
                        ),
                        23 => array(
                            "enum_name" => "交通银行（基本户）商派软件",
                            "enum_key" => 1024
                        ),
                        24 => array(
                            "enum_name" => "支付宝（software@shopex.cn）商派软件",
                            "enum_key" => 1025
                        ),
                        25 => array(
                            "enum_name" => "SMS@shopex.cn（商派软件）",
                            "enum_key" => 1026
                        ),
                        26 => array(
                            "enum_name" => "招行延安东路支行（商派软件）",
                            "enum_key" => 1027
                        )
                    )
                )
            ),
            Array
                (
                'name' => 'adate',
                'label' => '实际到账/出账日期',
                'attrtype' => '17'
            ),
            Array
                (
                'name' => 'bdate',
                'label' => '确认到账/出账日期',
                'attrtype' => '17'
            ),
            Array
                (
                'name' => 'trade_no',
                'label' => '交易号',
                'attrtype' => '8'
            ),
            Array
                (
                'name' => 'attn_id',
                'label' => '经办人',
                'attrtype' => '19',
                'sub_attr' => Array
                    (
                    'url' => site_url('www/user/ajax_list'),
                    'title' => '人员',
                    'quote_name' => 'user'
                )
            ),
            Array
                (
                'name' => 'department_id',
                'label' => '部门',
                'attrtype' => '19',
                'sub_attr' => Array
                    (
                    'url' => site_url('www/department/ajax_list'),
                    'title' => '部门',
                    'quote_name' => 'department'
                )
            ),
            Array
                (
                'name' => 'customer_info',
                'label' => '客户信息',
                'attrtype' => '8'
            ),
            Array
                (
                'name' => 'sale_no',
                'label' => '销售记录号',
                'attrtype' => '8'
            ),
            Array
                (
                'name' => 'order_no',
                'label' => '订单号',
                'attrtype' => '8'
            ),
            Array
                (
                'name' => 'Note',
                'label' => '摘要',
                'attrtype' => '9'
            ),
            Array
                (
                'name' => 'debit_amount',
                'label' => '借方发生额',
                'attrtype' => '7'
            ),
            Array
                (
                'name' => 'Credit_amount',
                'label' => '贷方发生额',
                'attrtype' => '7'
            ),
            Array
                (
                'name' => 'cashier',
                'label' => '出纳',
                'attrtype' => '19',
                'sub_attr' => Array
                    (
                    'url' => site_url('www/user/ajax_list'),
                    'title' => '人员',
                    'quote_name' => 'user'
                )
            ),
            Array
                (
                'name' => 'accounting',
                'label' => '会计',
                'attrtype' => '19',
                'sub_attr' => Array
                    (
                    'url' => site_url('www/user/ajax_list'),
                    'title' => '人员',
                    'quote_name' => 'user'
                )
            ),
            Array
                (
                'name' => 'sales_no',
                'label' => '销售号',
                'attrtype' => '8'
            ),
            Array
                (
                'name' => 'number',
                'label' => '日记账编号',
                'attrtype' => '8'
            )
        );

        $this->output->enable_profiler(false);
        $this->load->view('www/books/ajax_list', $data);
    }

    //数组转换成字串
    public function arrayeval($array, $level = 0) {
        $space = '';
        for ($i = 0; $i <= $level; $i++) {
            $space .= "\t";
        }
        $evaluate = "Array\n$space(\n";
        $comma = $space;
        foreach ($array as $key => $val) {
            $key = is_string($key) ? '\'' . addcslashes($key, '\'\\') . '\'' : $key;
            $val = !is_array($val) && (!preg_match("/^\-?\d+$/", $val) || strlen($val) > 12) ? '\'' . addcslashes($val, '\'\\') . '\'' : $val;
            if (is_array($val)) {
                $evaluate .= "$comma$key => " . $this->arrayeval($val, $level + 1);
            } else {
                if (is_numeric($val)) {
                    $evaluate .= "$comma$key => '" . $val . "'";
                } else {
                    $evaluate .= "$comma$key => " . $val;
                }
            }
            $comma = ",\n$space";
        }
        $evaluate .= "\n$space)";
        return $evaluate;
    }

    public function del() {
        if (!isset($_POST['books_id'])) {
            die('没有订单id');
        } elseif ($this->books->checkOrder($_POST['books_id'])) {
            die('ok');
        } else {
            die('确认到账，部门');
        }
    }

    //统计
    public function statistical() {
        $r_enum = $this->db->select('attr_id, enum_name, enum_key')->from('dd_enum')->where_in('attr_id', array('628', '115'))->get()->result_array();
        foreach ($r_enum as $k => $v) {
            if ($v['attr_id'] == 628) {
                $data['books_company'][] = $v;
            } else {
                $data['books_bank_account'][] = $v;
            }
        }
        $this->load->view('www/books/statistical', $data);
    }

    public function ajax_statistical() {
        if ($_POST) {
            $result = $this->books->getAmount(array_filter($_POST));
            print_r($result);
            exit;
        }
    }

    public function export() {
        $where = $_POST['sel_json'];
        $where = json_decode($where, true);

        $where['page'] = 1;
        $where['perNumber'] = $this->db->count_all('tc_books');
        $this->load->model('www/api_model', 'api');
        $result = $this->api->select($where, 'books');
        //时区修正S
        $defTimeZone = date_default_timezone_get();
        date_default_timezone_set('UTC');
        foreach ($result as $k => $v) {
            //$result[$k]['books_company'] = isset($v['books_company_arr']['enum_name']) ? $v['books_company_arr']['enum_name'] : '';
            //更正所属子公司信息 2014/08/28
            $result[$k]['books_subsidiary'] = isset($v['books_subsidiary_arr']['enum_name']) ? $v['books_subsidiary_arr']['enum_name'] : '';
            $result[$k]['books_bank_account'] = isset($v['books_bank_account_arr']['enum_name']) ? $v['books_bank_account_arr']['enum_name'] : '';
            $result[$k]['books_accounting'] = isset($v['books_accounting_arr']['user_name']) ? $v['books_accounting_arr']['user_name'] : '';
            $result[$k]['books_department_id'] = isset($v['books_department_id_arr']['department_name']) ? $v['books_department_id_arr']['department_name'] : '';
            $result[$k]['books_attn_id'] = isset($v['books_attn_id_arr']['user_name']) ? $v['books_attn_id_arr']['user_name'] : '';
            $result[$k]['books_adate'] = isset($v['books_adate']) ? strtotime($v['books_adate']) : '';
            $result[$k]['books_bdate'] = isset($v['books_bdate']) ? strtotime($v['books_bdate']) : '';
        }
        date_default_timezone_set($defTimeZone); //时区修正E
        // 用PHPexcel生产excel文件
        require_once 'Classes/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $attr_data = $this->books->attributeLabels();
		
        unset($attr_data['books_state']);
        unset($attr_data['books_invoice_track']);
        unset($attr_data['books_createtime']);
        unset($attr_data['books_updatetime']);
        unset($attr_data['books_sales_no']);
        //释放公司字段  2014/08/28
        unset($attr_data['books_company']);

        // 标题
        $i = 0;
        foreach ($attr_data as $k => $v) {
            $i++;
            $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[$i] . '1', $v);
        }
		$objPHPExcel->getActiveSheet()->getStyle('E')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        // 下面类容 循环
        for ($i = 1; $i <= count($result); $i++) {
            $ii = 0;
            foreach ($attr_data as $k => $v) {
                $ii++;
                switch ($k) {
                    //格式化输出时间
                    case 'books_adate';
                    case 'books_bdate';
                        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[$ii] . ($i + 1), PHPExcel_Shared_Date::PHPToExcel($result[$i - 1][$k]));
                        $objPHPExcel->getActiveSheet()->getStyle($this->cellArray[$ii] . ($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
                        break;
					case 'books_trade_no':
						$objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[$ii] . ($i + 1), "  " . $result[$i - 1][$k]);
						break;
                    default;
                        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[$ii] . ($i + 1), $result[$i - 1][$k]);
                        break;
                }
            }
        }

        // Set document security
        $objPHPExcel->getSecurity()->setLockWindows(true);
        $objPHPExcel->getSecurity()->setLockStructure(true);
        $objPHPExcel->getSecurity()->setWorkbookPassword("PHPExcel");

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $lee_excel_url = "upload/execl_11_" . date('YmdHis') . ".xls";
        $objWriter->save($lee_excel_url);
        echo base_url($lee_excel_url);
    }

    //日记账取消关联
    public function press() {
        if (!isset($_POST['books_id']))
            die('没有订单id');

        $this->load->model('www/salespay_model', 'salespay');
        $this->load->model('www/system_log_model', 'system_log');

        // 日记账订单编号
        $books = $this->books->id_aGetInfo($_POST['books_id']);
        if (empty($books['books_order_no']))
            die('日记账没有关联的订单');

        // 出入账
        $res_s = $this->salespay->GetInfo($where = array('salespay_book_id' => $books['books_id']));
        if (empty($res_s))
            die('【出/入账信息】没有关联日记账');
        if (count($res_s) > 1)
            die('日记账关联了多个【出/入账信息】请核对');

        // 更新
        $system_log_note = '原始数据：日记账' . print_r($books, true) . '原始数据：出入账' . print_r($res_s, true);
        // books_order_no(订单编号) books_department_id(部门ID) books_trade_no(交易号)
        $books_data = array('books_order_no' => '', 'books_department_id' => '', 'books_trade_no' => '');
        $this->books->update($books_data, $books['books_id']);
        // salespay_book_id(日记账ID) salespay_status(出入账状态) salespay_sp_date(确认到账日期)
        $salespay_data = array('salespay_book_id' => '', 'salespay_status' => '1001', 'salespay_sp_date' => '', 'salespay_pay_date' => '');
        $this->salespay->update($salespay_data, $res_s[0]['salespay_id']);
        $log_data = array(
            'system_log_addtime' => date('Y-m-d H:i:s', time()),
            'system_log_user_id' => $this->session->userdata('user_id'),
            'system_log_ip' => $this->get_client_ip(),
            'system_log_param' => '修改数据：日记账' . print_r($books_data, true) . '修改数据：出入账' . print_r($salespay_data, true),
            'system_log_operation' => '修改客户信息',
            'system_log_module' => 'Books',
            'system_log_module_id' => $books['books_id'],
            'system_log_note' => $system_log_note
        );
        $this->system_log->add($log_data);
        die('ok');
    }

}

?>