<?php
class Rebate extends WWW_Controller{
	public $menu1='rebate';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/rebate_model','rebate');
		$this->load->model('www/system_log_model','system_log');
		$this->load->model('www/message_model','message');
		$this->load->model('www/message_remind_model','message_remind');
		$this->load->model('www/order_model','order');
		$this->db->query('SET NAMES UTF8');
	}

	//作为明细的列表页面
	public function detailed_list_view(){
		if(isset($_GET['order_id'])){
			$where=array(
				'rebate_order_id'=>$_GET['order_id']
			);
			$data['listData']=$this->rebate->GetInfo($where);
			$data["labels"]=$this->rebate->attributeLabels();
			$data['listLayout']=$this->rebate->listLayout();
			$this->output->enable_profiler(false);
			$this->load->view('www/rebate/detailed_list_view',$data);
		}else{
			echo "不能没有ID";
		}
    }

	//ajax新增
	public function ajax_add(){
		if(isset($_GET['order_id'])){
			$this->load->model('www/order_model','order');
			$data["order_data"]=$this->order->id_aGetInfo($_GET['order_id']);
			$order_id = $data["order_data"]['order_id'];
			//已确认返点
			$where = array(
				'rebate_order_id'=>$order_id,
				'rebate_status'=>1002,
			);
			$data["yfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where); //已返点
			//未确认返点
			$where = array(
				'rebate_order_id'=>$order_id,
				'rebate_status !='=>1002,
			);
			$data["wfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where); //未返点

			$data["labels"]=$this->rebate->attributeLabels();
			//p($data);
			//可返点金额 订单返点总金额-已返点-未返点=可返点
			$data["kfd_amount"]=$data["order_data"]['order_rebate_amount']-$data["yfd_amount"]-$data["wfd_amount"];
			$this->load->model('admin/enum_model','enum');$data["rebate_pay_method_enum"]=$this->enum->getlist(435);
			$this->load->view('www/rebate/ajax_add',$data);
		}else{
			$where = array(
				'rebate_order_id'=>1,
				'rebate_status'=>1001,
			);
			$data["yfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where);
			echo $data["yfd_amount"];
			echo "不能没有ID";
		}
	}


	//ajax查看
	public function ajax_view(){
		if(isset($_GET['rebate_id'])){
			$data["rebate_data"]=$this->rebate->id_aGetInfo($_GET['rebate_id']);
			$order_id = $data["rebate_data"]['rebate_order_id_arr']['order_id'];
			//已确认返点
			$where = array(
				'rebate_order_id'=>$order_id,
				'rebate_status'=>1002,
			);
			$data["yfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where); //已返点
			//未确认返点
			$where = array(
				'rebate_order_id'=>$order_id,
				'rebate_status'=>1001,
			);
			$data["wfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where); //未返点
			//可返点金额 订单返点总金额-已返点-未返点=可返点
			$data["kfd_amount"]=$data["rebate_data"]['rebate_order_id_arr']['order_rebate_amount']-$data["yfd_amount"]-$data["wfd_amount"];
			//解析入账信息
			if(isset($data['rebate_data']['rebate_pay_info'])){
				$data['rebate_data']["rebate_pay_info_arr"]=json_decode($data['rebate_data']['rebate_pay_info'],true);
			}
			$data["labels"]=$this->rebate->attributeLabels();
			//p($data);//exit;
			$this->load->view('www/rebate/ajax_view',$data);
		}else{
			echo "不能没有ID";
		}
	}

	//ajax编辑
	public function ajax_edit(){
		if(isset($_GET['rebate_id'])){
			$data["rebate_data"]=$this->rebate->id_aGetInfo($_GET['rebate_id']);
			$order_id = $data["rebate_data"]['rebate_order_id_arr']['order_id'];
			//已确认返点
			$where = array(
				'rebate_order_id'=>$order_id,
				'rebate_status'=>1002,
			);
			$data["yfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where); //已返点
			//未确认返点
			$where = array(
				'rebate_order_id'=>$order_id,
				'rebate_status'=>1001,
			);
			$data["wfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where); //未返点
			//可返点金额 订单返点总金额-已返点-未返点=可返点
			$data["kfd_amount"]=$data["rebate_data"]['rebate_order_id_arr']['order_rebate_amount']-$data["yfd_amount"]-$data["wfd_amount"];
			//解析入账信息
			if(isset($data['rebate_data']['rebate_pay_info'])){
				$data['rebate_data']["rebate_pay_info_arr"]=json_decode($data['rebate_data']['rebate_pay_info'],true);
			}
			$data["labels"]=$this->rebate->attributeLabels();
			$this->load->model('admin/enum_model','enum');$data["rebate_pay_method_enum"]=$this->enum->getlist(435);
			//p($data['rebate_data']);
			$this->load->view('www/rebate/ajax_edit',$data);
		}else{
			echo "不能没有ID";
		}
	}

	//ajax审批
	public function ajax_examine_rebate(){
		if(isset($_GET['rebate_id'])){
			$data["rebate_data"]=$this->rebate->id_aGetInfo($_GET['rebate_id']);
			$order_id = $data["rebate_data"]['rebate_order_id_arr']['order_id'];
			//已确认返点
			$where = array(
				'rebate_order_id'=>$order_id,
				'rebate_status'=>1002,
			);
			$data["yfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where); //已返点
			//未确认返点
			$where = array(
				'rebate_order_id'=>$order_id,
				'rebate_status'=>1001,
			);
			$data["wfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where); //未返点
			//可返点金额 订单返点总金额-已返点-未返点=可返点
			$data["kfd_amount"]=$data["rebate_data"]['rebate_order_id_arr']['order_rebate_amount']-$data["yfd_amount"]-$data["wfd_amount"];
			//解析入账信息
			if(isset($data['rebate_data']['rebate_pay_info'])){
				$data['rebate_data']["rebate_pay_info_arr"]=json_decode($data['rebate_data']['rebate_pay_info'],true);
			}
			$data["labels"]=$this->rebate->attributeLabels();
			//p($data);//exit;
			$this->load->view('www/rebate/ajax_examine_rebate',$data);
		}else{
			echo "不能没有ID";
		}
	}

	//ajax确认
	public function ajax_confirm_rebate(){
		if(isset($_GET['rebate_id'])){
			$data["rebate_data"]=$this->rebate->id_aGetInfo($_GET['rebate_id']);
			$order_id = $data["rebate_data"]['rebate_order_id_arr']['order_id'];
			//已确认返点
			$where = array(
				'rebate_order_id'=>$order_id,
				'rebate_status'=>1002,
			);
			$data["yfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where); //已返点
			//未确认返点
			$where = array(
				'rebate_order_id'=>$order_id,
				'rebate_status'=>1001,
			);
			$data["wfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where); //未返点
			//可返点金额 订单返点总金额-已返点-未返点=可返点
			$data["kfd_amount"]=$data["rebate_data"]['rebate_order_id_arr']['order_rebate_amount']-$data["yfd_amount"]-$data["wfd_amount"];
			//解析入账信息
			if(isset($data['rebate_data']['rebate_pay_info'])){
				$data['rebate_data']["rebate_pay_info_arr"]=json_decode($data['rebate_data']['rebate_pay_info'],true);
			}
			$data["labels"]=$this->rebate->attributeLabels();
			$this->load->model('admin/enum_model','enum');$data["rebate_pay_account_enum"]=$this->enum->getlist(540);
			//p($data["rebate_data"]);//exit;
			$this->load->view('www/rebate/ajax_confirm_rebate',$data);
		}else{
			echo "不能没有ID";
		}
	}


	//ajax调用的新增
	public function ajax_add_post(){
		if($_POST){
			//p($_POST);die;
			$rebate_data = $_POST['rebate'];
			$rebate_data['rebate_pay_info'] = $_POST['payinfo'];
			$rebate_data['rebate_pay_info']['reviewer'] = $_POST['order_finance'];
			$rebate_data['rebate_pay_info']['department'] = $_POST['order_finance_department'];
			$rebate_data['rebate_pay_info'] = json_encode($rebate_data['rebate_pay_info']);
			if(!isset($rebate_data['rebate_status'])){
				$rebate_data['rebate_status'] = '1001'; //返点状态创建后默认给【未审批】
			}
			//p($rebate_data);
			//echo $rebate_data['rebate_order_id'];
			$this->rebate->add($rebate_data);
			$rebate_id = $this->db->insert_id();
			$log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'申请返点',
				'system_log_module'=>'Order',
				'system_log_module_id'=>$rebate_data['rebate_order_id'],
				'system_log_note'=>$rebate_data['rebate_note']
				);
			$this->load->model('www/user_model','user');
			$reviewer_info = $this->user->id_aGetInfo($_POST['rebate']['rebate_reviewer']);
			$this->system_log->add($log_data);
			$message_data = array(
					'message_owner'=>$_POST['rebate']['rebate_reviewer'],
					'message_module'=>'rebate',
					'message_module_id'=>$rebate_id,
					'message_type'=>1005,
					'message_department'=>$reviewer_info['user_department'],
					'message_url'=>'www/rebate/ajax_examine_rebate'
			);
			$this->message->add($message_data);
			$mess_remind_data = array(
				'message_remind_role_id' =>3,
				'message_remind_status'=>1001
			);
			$this->message_remind->update($mess_remind_data,$mess_remind_data['message_remind_role_id']);
			echo 1;
		}else{
			echo '失败';
		}
	}

	//ajax调用的编辑
	public function ajax_update_post(){
		if($_POST){
			$rebate_data = $_POST['rebate'];
			if(isset($_POST['payinfo'])){
				$rebate_data['rebate_pay_info'] = $_POST['payinfo'];
				$rebate_data['rebate_pay_info']['reviewer'] = $_POST['order_finance'];
				$rebate_data['rebate_pay_info']['department'] = $_POST['order_finance_department'];
				$rebate_data['rebate_pay_info'] = json_encode($rebate_data['rebate_pay_info']);
			}
			 if(!isset($rebate_data['rebate_status'])){
			 	$rebate_data['rebate_status'] = 1001;
			 }
			 //p($rebate_data);
			$this->rebate->update($rebate_data,$rebate_data['rebate_id']);

			$log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),

				'system_log_module'=>'Order',
				'system_log_module_id'=>$rebate_data['rebate_order_id'],

				);
			if($rebate_data['rebate_status'] == 1001){
				$log_data['system_log_operation'] = '编辑返点信息';
				$log_data['system_log_note'] = $rebate_data['rebate_note'];
				$message_data1 = array(
					'message_status'=>1002
				);
	            $param['message_type'] = 1012;
	            $param['message_module_id'] = $rebate_data['rebate_id'];
				$this->message->update($message_data1,$param);
				$this->load->model('www/user_model','user');
				$reviewer_info = $this->user->id_aGetInfo($_POST['rebate']['rebate_reviewer']);
				$message_data = array(
					'message_owner'=>$_POST['rebate']['rebate_reviewer'],
					'message_module'=>'rebate',
					'message_module_id'=>$rebate_data['rebate_id'],
					'message_type'=>1005,
					'message_department'=>$reviewer_info['user_department'],
					'message_url'=>'www/rebate/ajax_examine_rebate'
				);
				$this->message->add($message_data);
				$mess_remind_data = array(
					'message_remind_role_id' =>3,
					'message_remind_status'=>1001
				);
				$this->message_remind->update($mess_remind_data,$mess_remind_data['message_remind_role_id']);
			}else if($rebate_data['rebate_status'] == 1002){
				$log_data['system_log_operation'] = '返点审核通过';
				$log_data['system_log_note'] = $rebate_data['rebate_examine_note'];
				// $this->load->model('www/user_model','user');
				// $reviewer_info = $this->user->id_aGetInfo($_POST['rebate']['rebate_reviewer']);
				$message_data = array(
					'message_owner'=>$_POST['order_finance'],
					'message_module'=>'rebate',
					'message_module_id'=>$rebate_data['rebate_id'],
					'message_type'=>1006,
					'message_department'=>$_POST['order_finance_department'],
					'message_url'=>'www/rebate/ajax_confirm_rebate'
				);
				$this->message->add($message_data);
				$message_data = array(
					'message_status'=>1002
				);
	            $param['message_type'] = 1005;
	            $param['message_module_id'] = $rebate_data['rebate_id'];
				$this->message->update($message_data,$param);
			}else if($rebate_data['rebate_status'] == 1003){
				$log_data['system_log_operation'] = '返点审核不通过';
				$log_data['system_log_note'] = $rebate_data['rebate_examine_note'];
				$message_data1 = array(
					'message_status'=>'1002'
				);
	            $param['message_type'] = 1005;
	            $param['message_module_id'] = $rebate_data['rebate_id'];
				$this->message->update($message_data1,$param);
				$order_data = $this->order->id_aGetInfo($rebate_data['rebate_order_id']);
	 		   $message_data = array(
						'message_owner'=>$order_data['order_typein'],
						'message_module'=>'rebate',
						'message_module_id'=>$rebate_data['rebate_id'],
						'message_type'=>1012,
						//'message_department'=>$order_data['order_owner_arr']['user_department'],
						'message_url'=>'www/rebate/ajax_edit'
				);
				$this->message->add($message_data);
			}else if($rebate_data['rebate_status'] == 1004){
				$log_data['system_log_operation'] = '返点成功';
				$message_data = array(
					'message_status'=>'1002'
				);
	            $param['message_type'] = 1006;
	            $param['message_module_id'] = $rebate_data['rebate_id'];
				$this->message->update($message_data,$param);
				$this->load->model('www/books_model','books');
				$this->books->update(array('books_state'=>1002),$rebate_data['rebate_book_id']);
			}else if($rebate_data['rebate_status'] == 1005){
				$log_data['system_log_operation'] = '执行返点失败';
				$message_data1 = array(
					'message_status'=>'1002'
				);
	            $param['message_type'] = 1006;
	            $param['message_module_id'] = $rebate_data['rebate_id'];
				$this->message->update($message_data1,$param);
				$order_data = $this->order->id_aGetInfo($rebate_data['rebate_order_id']);
	 		   $message_data = array(
						'message_owner'=>$order_data['order_typein'],
						'message_module'=>'rebate',
						'message_module_id'=>$rebate_data['rebate_id'],
						'message_type'=>1012,
						//'message_department'=>$order_data['order_owner_arr']['user_department'],
						'message_url'=>'www/rebate/ajax_edit'
				);
				$this->message->add($message_data);
			}
			$this->system_log->add($log_data);
			echo 1;
		}else{
			echo '失败??';
		}
	}

	//ajax调用的删除
	public function ajax_del_post(){
		if($_POST){
			//p($_POST);die;
			$rebate_id = $_POST['rebate_id'];
			$this->rebate->del($rebate_id);
			$log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'删除返点',
				'system_log_module'=>'Order',
				'system_log_module_id'=>$_POST['order_id'],
				);

			$this->system_log->add($log_data);
			echo 1;
		}else{
			echo '失败??';
		}
	}

	public function ajax_rebateOrder(){
		if(isset($_GET['order_id'])){
			$this->load->model('www/order_model', 'order');
			$data['data'] = $this->order->id_aGetInfo($_GET['order_id']);
//			$data["rebate_data"]=$this->rebate->id_aGetInfo($_GET['rebate_id']);
//			$order_id = $data["rebate_data"]['rebate_order_id_arr']['order_id'];
//			//已确认返点
//			$where = array(
//				'rebate_order_id'=>$order_id,
//				'rebate_status'=>1002,
//			);
//			$data["yfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where); //已返点
//			//未确认返点
//			$where = array(
//				'rebate_order_id'=>$order_id,
//				'rebate_status'=>1001,
//			);
//			$data["wfd_amount"]=$this->rebate->SumGetInfo('rebate_amount',$where); //未返点
//			//可返点金额 订单返点总金额-已返点-未返点=可返点
//			$data["kfd_amount"]=$data["rebate_data"]['rebate_order_id_arr']['order_rebate_amount']-$data["yfd_amount"]-$data["wfd_amount"];
//			//解析入账信息
//			if(isset($data['rebate_data']['rebate_pay_info'])){
//				$data['rebate_data']["rebate_pay_info_arr"]=json_decode($data['rebate_data']['rebate_pay_info'],true);
//			}
//			$data["labels"]=$this->rebate->attributeLabels();
			//p($data);//exit;
			$this->load->view('www/rebate/ajax_rebateOrder',$data);
		}else{
			echo "不能没有ID";
		}
	}

	public function ajax_confirm_rebateOrder(){
		if ($_POST) {
			if (empty($_POST['order_number'])) die('order_number is null');
			if (empty($_POST['order_id'])) die('order_id is null');
			if (empty($_POST['books_id'])) die('books_id is null');
			$this->load->model('www/books_model', 'books');
			$con = array('books_order_no'=>$_POST['order_number']);
			$this->books->update($con, $_POST['books_id']);
			$this->rebate->update(array('order_rebates_state'=>'1002'), $_POST['order_id']);
			die('1');
//			$res_books = $this->books->GetInfo($con);
//			if ($res_books) die('订单已关联');
//			$res = $this->books->update($con, $_POST['books_id']);
//			if ($res) die('1');
//				else die('关联失败');
		}else die('error');
	}

	public function ajax_depositOrder(){
		if(isset($_GET['order_id'])){
			$this->load->model('www/order_model', 'order');
			$data['data'] = $this->order->id_aGetInfo($_GET['order_id']);
			$this->load->view('www/rebate/ajax_depositOrder',$data);
		}else{
			echo "不能没有ID";
		}
	}

	public function ajax_confirm_depositOrder(){
		if ($_POST) {
			if (empty($_POST['order_number'])) die('order_number is null');
			if (empty($_POST['order_id'])) die('order_id is null');
			if (empty($_POST['books_id'])) die('books_id is null');
			$this->load->model('www/books_model', 'books');
			$con = array('books_order_no'=>$_POST['order_number']);
			$this->books->update($con, $_POST['books_id']);
			$this->rebate->update(array('order_rebates_state'=>'1002'), $_POST['order_id']);
			die('1');
//			$res_books = $this->books->GetInfo($con);
//			if ($res_books) die('订单已关联');
//			$res = $this->books->update($con, $_POST['books_id']);
//			if ($res) die('1');
//				else die('关联失败');
		}else die('error');
	}
}
?>