<?php

class Message extends WWW_Controller {

	public $menu1 = 'message';

	public function __construct() {
		parent::__construct();
		$this->load->model('www/message_model', 'message');
		$this->load->model('www/invoice_model','invoice');
		$this->db->query('SET NAMES UTF8');
	}

	public function index() {
		$this->message->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$where['w1'] = array(
			'attr' => 'owner',
			'value' => $this->session->userdata('user_id'),
			'action' => '=',
		);
		$where['w2'] = array(
			'attr' => 'status',
			'value' => 1001,
			'action' => '=',
		);
		$where['w3'] = array(
			'attr' => 'module_id',
			'value' => 0,
			'action' => 'NOT_EQUAL',
		);
		$where_rel = "w1 and w2 and w3";
		$data['where'] = json_encode($where);
		$data['where_rel'] = $where_rel;
		$data['role_id_arr'] = json_encode($this->session->userdata('role_id_arr'));
		$data['totalNumber'] = $this->message->countGetInfo(); //数据总数

		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"] = $this->message->attributeLabels();
		$data['listLayout'] = $this->message->listLayout();
		
		//p($data);exit;
		$this->render('www/message/list', $data);
	}

	public function ajax_select() {
		$user_id = $this->session->userdata('user_id');
		$tem = explode('?', $_GET['module']);
		$module = $tem[0];
		$new_where = $_POST;

		//p($new_where);exit;
		if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
			$select_arr = json_decode($_POST["select_json"], true);
			//p($select_arr['attr'].'-'.$select_arr['value']);
			$new_where['select_arr'] = array(
				$select_arr['attr'] => $select_arr['value'],
			);
		} else {
			$new_where['select_arr'] = "";
		}

		//把条件组合起来主要是where、get_attr和select_arr
		//循环出get_attr
		$where_all = $new_where['where'];
		if ($new_where['where_rel'] == "") {
			$rel_all = "";
		} else {
			$rel_all = "(" . $new_where['where_rel'] . ")";
		}

		if (isset($new_where['get_attr']) && $new_where['get_attr'] != "") {
			$i = 0;
			$ga_rel = "";
			foreach ($new_where['get_attr'] as $key => $value) {
				$i++;

				$where_all['ga' . $i] = array(
					'attr' => $key,
					'value' => $value,
					'action' => '=',
				);

				if ($ga_rel == "") {
					$ga_rel = 'ga' . $i;
				} else {
					$ga_rel = $ga_rel . ' and ga' . $i;
				}
			}
			if ($rel_all !== "") {
				$rel_all = $rel_all . " and (" . $ga_rel . ")";
			} else {
				$rel_all = "(" . $ga_rel . ")";
			}
		}

		if ($new_where['select_arr'] != "") {
			$i = 0;
			$sa_rel = "";
			foreach ($new_where['select_arr'] as $key => $value) {
				$i++;
				$where_all['sa' . $i] = array(
					'attr' => $key,
					'value' => $value,
					'action' => 'like',
				);
				if ($sa_rel == "") {
					$sa_rel = 'sa' . $i;
				} else {
					$sa_rel = $sa_rel . ' and sa' . $i;
				}
			}
			if ($rel_all != "") {
				$rel_all = $rel_all . " and (" . $sa_rel . ")";
			} else {
				$rel_all = "(" . $sa_rel . ")";
			}
		}
		//p($rel_all);die;
		$new_where_ok = array(
			"where_all" => $where_all,
			"rel_all" => $rel_all,
			"page" => $new_where['page'],
			"perNumber" => $new_where['perNumber'],
			"obj" => $new_where['obj'],
		);
		$this->load->model('www/api_model', 'api');
		$listData = $this->api->select($new_where_ok);
		// 针对不同待办处理
		switch ($module) {
			case 'salespay':
			case 'refund':
			case 'rebate':
				$listData = $this->salespayMessage($listData);
				break;
			case 'applyopen';
				$res = $this->applyopenMessage($listData);
				$data['apply_data'] = $res['apply_data'];
				$order_d_id_arr = $res['order_d_data'];
				break;
			case 'pennyOrder':
				$listData = $this->pennyOrderMessage($listData);
				break;
			case 'cancel_check'://审核订单作废
				$listData = $this->message->GetInfo( array("message_owner"=>$user_id,"message_type"=>array("1018", "1014"),"message_status"=>1001) );
				break;
			case 'cancel_affirm'://确认订单作废
				$listData = $this->message->GetInfo( array("message_owner"=>$user_id,"message_type"=>array("1019", "1015"),"message_status"=>1001) );
				//echo $this->db->last_query();
				break;
			case 'order':
			default:
				$listData = $this->defaultMessage($listData);
				break;
		}
		
		if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
			$select_arr = json_decode($_POST["select_json"], true);
			$like["message_" . $select_arr['attr']] = $select_arr['value'];
		} else {
			//$select_arr = "";
			$like = "";
		}
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$where = "";
		if (isset($_GET['type_id'])) {
			if ($_GET['type_id'] != "" and $_GET['type_id'] != 0) {
				$where['type_id'] = $_GET['type_id'];
			}
		}

		//$data['listData'] = $listData;	
		if ($module=="cancel_check" || $module=="cancel_affirm") {
			$data['listData'] = $listData;
		} else {
			$data['listData'] = $listData['info'];
		}
		$data['sel_data'] = $listData['sel_data'];
		//cus ljs 2014-07-24 查询开票金额 strat
		//$data['module'] = $module;
		foreach($data['listData'] as $key => $value)
		{
			if($value['message_module'] == 'invoice')
			{
				$where=array(
					'invoice_id'=>$value['message_module_id']
				);
				$invoice_data=$this->invoice->GetInfo($where);
				p($invoice_data);
				$data['listData'][$key]['invoice_amount']=$invoice_data[0]['invoice_amount'];
			}
		}
		//cus ljs 2014-07-24 查询开票金额 end
		if (!empty($data['listData'])) {
			foreach ($data['listData'] as $key => $value) {

				$data['listData'][$key]['type_name'] = $this->message->action_name($value['message_type']);
			}
		}
		$data["labels"] = $this->message->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['totalNumber'] = $listData['count']; //数据总数
		$data['page'] = 1; //默认第一页
		$this->output->enable_profiler(false);
		switch( $module )
		{
			case "pennyOrder":
				$this->load->view('www/message/ajax_penny_select', $data);
				break;
			case "invoice":
				$this->load->view('www/message/ajax_select_invoice', $data);
				break;
			case "cancel_check":
				$this->load->view('www/message/ajax_select_check', $data);
				break;
			case "cancel_affirm":
				$this->load->view('www/message/ajax_select_affirm', $data);
				break;
			default:
				
				$this->load->view('www/message/ajax_select', $data);
				break;
		}
		/*
		if ($module == 'pennyOrder') {
			$this->load->view('www/message/ajax_penny_select', $data);
		} elseif($module == "invoice"){
			$this->load->view('www/message/ajax_select_invoice', $data);
		}else{
			$this->load->view('www/message/ajax_select', $data);
		}*/
	}

	//
	public function new_ajax_select() {
		//p($_POST);//exit;
		$this->load->model('www/api_model', 'api');
		$listData = $this->api->select($_POST);

		$module = isset($_GET['module']) ? $_GET['module'] : '';
		switch ($module) {
			case 'order':
				foreach ($listData['info'] as $k => $v) {
					$this->load->model('www/order_model', 'order');
					$module_data = $this->order->id_aGetInfo($v['message_module_id']);
					$listData['info'][$k]['order_number'] = $module_data['order_number'];
					$listData['info'][$k]['order_amount'] = $module_data['order_amount'];
					$salespay_info = $this->db->get_where('tc_salespay', array('salespay_order_id' => $module_data['order_id'], 'salespay_status' => 1002))->result_array();
					$listData['info'][$k]['order_ydz_amount'] = 0;
					if (!empty($salespay_info)) {

						foreach ($salespay_info as $key => $value) {
							$listData['info'][$k]['order_ydz_amount']+=$value['salespay_pay_amount'];
						}
					}
					$account = $this->db->select('account_name')->from('tc_account')->where('account_id', $module_data['order_account'])->get()->row_array();
					$own = $this->db->select('user_name')->from('tc_user')->where('user_id', $module_data['order_owner'])->get()->row_array();
					$listData['info'][$k]['order_account'] = $account['account_name'];
					$listData['info'][$k]['order_owner'] = $own['user_name'];
				}
				break;
			default:
				foreach ($listData['info'] as $k => $v) {
					$this->load->model('www/salespay_model', 'salespay');
					$module_data = $this->salespay->id_aGetInfo($v['message_module_id']);
					$listData['info'][$k]['order_number'] = $module_data['salespay_order_id_arr']['order_number'];
					$listData['info'][$k]['order_amount'] = $module_data['salespay_order_id_arr']['order_amount'];
					$salespay_info = $this->db->get_where('tc_salespay', array('salespay_order_id' => $module_data['salespay_order_id_arr']['order_id'], 'salespay_status' => 1002))->result_array();
					$listData['info'][$k]['order_ydz_amount'] = 0;
					if (!empty($salespay_info)) {

						foreach ($salespay_info as $key => $value) {
							$listData['info'][$k]['order_ydz_amount']+=$value['salespay_pay_amount'];
						}
					}
					$own = $this->db->select('user_name')->from('tc_user')->where('user_id', $module_data['salespay_order_id_arr']['order_owner'])->get()->row_array();

					$account = $this->db->select('account_name')->from('tc_account')->where('account_id', $module_data['salespay_order_id_arr']['order_account'])->get()->row_array();
					$listData['info'][$k]['order_account'] = $account['account_name'];
					$listData['info'][$k]['order_owner'] = $own['user_name'];
				}
				break;
		}

		$data["labels"] = $this->message->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取

		if (isset($_GET['module']) && $_GET['module'] == 'applyopen') {
			foreach ($listData['info'] as $k => $v) {
				$t = $this->db->select('applyopen_order_d_id')->from('tc_applyopen')->where('applyopen_id', $v['message_module_id'])->get()->row_array();
				$order_d_id_arr[] = $t['applyopen_order_d_id'];
			}
			foreach ($order_d_id_arr as $k => $v) {
				$t = $this->db->select()->from('tc_order_d')->where('order_d_id', $v)->order_by('order_d_id')->group_by('order_d_goods_code')->get()->result_array();
				$data['apply_data'][] = $t[0];
			}
		}
		$data['listData'] = $listData['info'];
		if (!empty($data['listData'])) {
			foreach ($data['listData'] as $key => $value) {

				$data['listData'][$key]['type_name'] = $this->message->action_name($value['message_type']);
			}
		}
		$data['totalNumber'] = $listData['count']; //数据总数
		$data['sel_data'] = $listData['sel_data'];
		$data['perNumber'] = $_POST['perNumber']; //每页显示的记录数
		$data['page'] = $_POST['page']; //当前页数
		//p($data);exit;
		//根据类型的不同调用不同的列表页

		$this->load->view('www/message/ajax_select', $data);
	}

	// 待查账
	public function audit() {
		$this->load->model('www/api_model', 'api');
		$data_arr = array('6' => 'sale_order', '7' => 'online_order', '8' => 'rebates_order', '9' => 'penny_order', '10' => 'deposit_order', '13' => 'delivery_order');
		$whereSql = '';
		foreach ($data_arr as $k => $v) {
			$data_auth = $this->get_data_auth($v);
			if (!empty($data_auth) && $data_auth['where'] != '\'\'') {
				$where = (json_decode($data_auth['where'], true));
				foreach ($where as $k2 => $v2) {
					$w_sql = '';
					if ($v2['attr'] == 'department.id') {
						$department_id = str_pad($v2['value'], 4, "0", STR_PAD_LEFT);
						$department_id_arr = $this->db->select('department_id')->like('department_treepath', $department_id, 'both')->get('tc_department')->result_array();
						if ($department_id_arr) {
							$d_arr = array();
							foreach ($department_id_arr as $dv) {
								$d_arr[] = $dv['department_id'];
							}
							$department_ids = implode(',', $d_arr);
						}
						$w_sql .= ' AND order_department IN (' . $department_ids . ')';
					} else {
						$attr = explode('.', $v2['attr']);
						$w_sql .= ' AND order_' . $attr[0] . '=' . $v2['value'];
					}
				}
				$w_sql = ltrim($w_sql, ' AND');
				$whereSql .= '(' . $w_sql . ') AND ';
			} elseif (!empty($data_auth) && $data_auth['where'] == '\'\'') {
//				$w_sql = 'type_id = ' . $k;
//				$whereSql .= $w_sql . ' AND ';
			} else {
				$w_sql = 'type_id != ' . $k;
				$whereSql .= $w_sql . ' AND ';
			}
		}
		$whereSql = empty($whereSql) ? '' : $whereSql;
		$sql = 'SELECT * FROM tc_order WHERE 1 AND ' . $whereSql . ' order_id IN ( SELECT salespay_order_id FROM `tc_salespay` WHERE salespay_money_type = 1001 AND  salespay_status = 1001 ) ORDER BY order_id DESC';
		$result = $this->db->query($sql)->result_array();
		$objDetail = $this->api->objDetail('order');
		$data['orders'] = $this->api->parse($result, 'order', $objDetail);
		$this->render('www/message/audit', $data);
	}

	/**
	 * 入款项（入账、出账）
	 * @param type $listData
	 * @return type array
	 */
	private function salespayMessage($listData) {
		if (is_array($listData) && !empty($listData)) {
			$this->load->model('www/order_model', 'order');
			$this->load->model('www/salespay_model', 'salespay');
			foreach ($listData['info'] as $k => $v) {				
				$module_data = $this->salespay->id_aGetInfo($v['message_module_id']);
				$listData['info'][$k]['order_number'] = $module_data['salespay_order_id_arr']['order_number'];
				$listData['info'][$k]['order_amount'] = $module_data['salespay_order_id_arr']['order_amount'];
				//新增所属子公司 start
				$temp_data = $this->order->id_aGetInfo($module_data['salespay_order_id_arr']['order_id']);
				$listData['info'][$k]['order_subsidiary'] = $temp_data['order_subsidiary_arr']['enum_name'];
				//新增所属子公司 end
				$salespay_info = $this->db->get_where('tc_salespay', array('salespay_order_id' => $module_data['salespay_order_id_arr']['order_id'], 'salespay_status' => 1002))->result_array();
				$listData['info'][$k]['order_ydz_amount'] = 0;
				if (!empty($salespay_info)) {
					foreach ($salespay_info as $key => $value) {
						if ($value['salespay_pay_amount'] == "1001") {
							$listData['info'][$k]['order_ydz_amount']+=$value['salespay_pay_amount'];
						} else if ($value['salespay_pay_amount'] == "1003") {
							$listData['info'][$k]['order_ydz_amount']-=$value['salespay_pay_amount'];
						} else if ($value['salespay_pay_amount'] == "1004") {
							$listData['info'][$k]['order_ydz_amount']+=$value['salespay_pay_amount'];
						} else if ($value['salespay_pay_amount'] == "1005") {
							$listData['info'][$k]['order_ydz_amount']-=$value['salespay_pay_amount'];
						}
					}
				}

				$own = $this->db->select('user_name')->from('tc_user')->where('user_id', $module_data['salespay_order_id_arr']['order_owner'])->get()->row_array();
				$account = $this->db->select('account_name')->from('tc_account')->where('account_id', $module_data['salespay_order_id_arr']['order_account'])->get()->row_array();
				$listData['info'][$k]['order_owner'] = isset($own) ? $own['user_name'] : '';
				$listData['info'][$k]['order_account'] = isset($account) ? $account['account_name'] : '';
			}
			return $listData;
		}
	}

	/**
	 * 开通
	 * @param type $listData
	 * @param type $order_d_id_arr
	 * @return type array
	 */
	private function applyopenMessage($listData, $order_d_id_arr) {
		foreach ($listData['info'] as $k => $v) {
			$t = $this->db->select('applyopen_order_d_id')->from('tc_applyopen')->where('applyopen_id', $v['message_module_id'])->get()->row_array();
			$order_d_id_arr[] = $t['applyopen_order_d_id'];
		}
		foreach ($order_d_id_arr as $k => $v) {
			$t = $this->db->select()->from('tc_order_d')->where('order_d_id', $v)->order_by('order_d_id')->group_by('order_d_goods_code')->get()->result_array();
			$data['apply_data'][] = $t[0];
		}
		$result['order_d_data'] = $order_d_id_arr;
		$result['apply_data'] = $data['apply_data'];
		return $result;
	}

	private function pennyOrderMessage($listData) {
		if (is_array($listData) && !empty($listData)) {
			foreach ($listData['info'] as $k => $v) {
				$this->load->model('www/order_model', 'order');
				$module_data = $this->order->id_aGetInfo($v['message_module_id']);

				$listData['info'][$k]['order_subsidiary'] = $module_data['order_subsidiary_arr']['enum_name'];
				$listData['info'][$k]['order_number'] = $module_data['order_number'];
				$listData['info'][$k]['order_transfer_name'] = $module_data['order_transfer_name'];
				$listData['info'][$k]['order_change_out'] = $module_data['order_change_out_arr']['user_name'];
				$listData['info'][$k]['order_change_into'] = $module_data['order_change_into_arr']['user_name'];
				$listData['info'][$k]['order_change_into_money'] = $module_data['order_change_into_money'];
			}
			return $listData;
		}
	}

	private function defaultMessage($listData) {
		if (is_array($listData) && !empty($listData)) {
			$this->load->model('www/order_model', 'order');
			foreach ($listData['info'] as $k => $v) {
				$this->load->model('www/' . $v['message_module'] . '_model', $v['message_module']);
				$module_data = $this->$v['message_module']->id_aGetInfo($v['message_module_id']);
				$listData['info'][$k]['order_number'] = $module_data[$v['message_module'] . '_order_id_arr']['order_number'];
				$listData['info'][$k]['order_amount'] = $module_data[$v['message_module'] . '_order_id_arr']['order_amount'];
				$salespay_info = $this->db->get_where('tc_salespay', array('salespay_order_id' => $module_data[$v['message_module'] . '_order_id_arr']['order_id'], 'salespay_status' => 1002))->result_array();
				$listData['info'][$k]['order_ydz_amount'] = 0;
				if (!empty($salespay_info)) {
					foreach ($salespay_info as $key => $value) {
						//$listData['info'][$k]['order_ydz_amount']+=$value['salespay_pay_amount'];
						if ($value['salespay_money_type_name'] == "1001") {
							$listData['info'][$k]['order_ydz_amount']+=$value['salespay_pay_amount'];
						} else if ($value['salespay_money_type_name'] == "1003") {
							$listData['info'][$k]['order_ydz_amount']-=$value['salespay_pay_amount'];
						} else if ($value['salespay_money_type_name'] == "1004") {
							$listData['info'][$k]['order_ydz_amount']+=$value['salespay_pay_amount'];
						} else if ($value['salespay_money_type_name'] == "1005") {
							$listData['info'][$k]['order_ydz_amount']-=$value['salespay_pay_amount'];
						}
					}
				}
				$listData['info'][$k]['order_ydz_amount'] = number_format($listData['info'][$k]['order_ydz_amount'], 2, '.', '');
				$listData['info'][$k]['order_amount'] = number_format($listData['info'][$k]['order_amount'], 2, '.', '');		
				$tmp_data = $this->order->id_aGetInfo($module_data[$v['message_module'] . '_order_id_arr']['order_id']);
				
				$listData['info'][$k]['order_subsidiary'] = $tmp_data['order_subsidiary_arr']['enum_name'];

				$own = $this->db->select('user_name')->from('tc_user')->where('user_id', $module_data[$v['message_module'] . '_order_id_arr']['order_owner'])->get()->row_array();
				$account = $this->db->select('account_name')->from('tc_account')->where('account_id', $module_data[$v['message_module'] . '_order_id_arr']['order_account'])->get()->row_array();
				$listData['info'][$k]['order_owner'] = isset($own) ? $own['user_name'] : '';
				$listData['info'][$k]['order_account'] = isset($account) ? $account['account_name'] : '';
			}
			return $listData;
		}
	}

}

?>