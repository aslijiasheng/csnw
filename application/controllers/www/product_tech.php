<?php

class Product_tech extends WWW_Controller {

    public $menu1 = 'product_tech';

    public function __construct() {
        parent::__construct();
        $this->load->model('www/product_tech_model', 'product_tech');
    }

    public function index() {
        $user_auth = $this->user->user_auth($this->session->userdata('user_id'));
        if (!in_array('product_tech_list', $user_auth['activity_auth_arr'])) {
            $this->load->view('www/layouts/operation_403');
        } else {
            $this->product_tech->db->query('SET NAMES UTF8');
            if (!isset($_GET['page'])) {
                $_GET['page'] = 1;
            }
            if (!isset($_GET['perNumber'])) {
                $_GET['perNumber'] = 10;
            }
            $data['totalNumber'] = $this->product_tech->countGetInfo(); //数据总数
            $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
            $data["labels"] = $this->product_tech->attributeLabels();
            $data['listLayout'] = $this->product_tech->listLayout();
            $this->render('www/product_tech/list', $data);
        }
    }

    public function add() {
        $this->product_tech->db->query('SET NAMES UTF8');
        $data["labels"] = $this->product_tech->attributeLabels();

        if (!empty($_POST)) {
            if (isset($_GET['type_id'])) {
                $_POST['product_tech']['type_id'] = $_GET['type_id'];
                $this->product_tech->add($_POST['product_tech']);
                success('www/product_tech?type_id=' . $_GET['type_id'], '创建成功');
            } else {
                $this->product_tech->add($_POST['product_tech']);
                success('www/product_tech', '创建成功');
            }
        }
        $this->render('www/product_tech/add', $data);
    }

    public function update() {
        $this->product_tech->db->query('SET NAMES UTF8');
        $data["labels"] = $this->product_tech->attributeLabels();
        $data['id_aData'] = $this->product_tech->id_aGetInfo($_GET['product_tech_id']);
        $data['listLayout'] = $this->product_tech->listLayout();

        if (!empty($_POST)) {
            if (isset($_GET['type_id'])) {
                $_POST['product_tech']['type_id'] = $_GET['type_id'];
                $this->product_tech->update($_POST['product_tech'], $_GET['product_tech_id']);
                success('www/product_tech?type_id=' . $_GET['type_id'], '更新成功');
            } else {
                $this->product_tech->update($_POST['product_tech'], $_GET['product_tech_id']);
                success('www/product_tech', '更新成功');
            }
        }
        $this->render('www/product_tech/update', $data);
    }

    public function ajax_list() {
        $this->product_tech->db->query('SET NAMES UTF8');
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data['listData'] = $this->product_tech->listGetInfo('', $_GET['page'], $_GET['perNumber']);
        $data['totalNumber'] = $this->product_tech->countGetInfo(); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        $data["labels"] = $this->product_tech->attributeLabels();
        $data['listLayout'] = $this->product_tech->listLayout();
        $this->output->enable_profiler(false);
        $this->load->view('www/product_tech/ajax_list', $data);
    }

    public function ajax_select() {
        if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
            $select_arr = json_decode($_POST["select_json"], true);
            $like["product_tech_" . $select_arr['attr']] = $select_arr['value'];
        } else {
            //$select_arr = "";
            $like = "";
        }
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $where = "";
        if (isset($_GET['type_id'])) {
            if ($_GET['type_id'] != "" and $_GET['type_id'] != 0) {
                $where['type_id'] = $_GET['type_id'];
            }
        }

        $data['listData'] = $this->product_tech->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
        $data["labels"] = $this->product_tech->attributeLabels();
        //如果包含对象类型，添加一个数组，用于视图获取

        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data['totalNumber'] = $this->product_tech->countGetInfo($where, $like); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        //p($data);
        $this->output->enable_profiler(false);
        $this->load->view('www/product_tech/ajax_select', $data);
    }

    public function ajax_select_quote() {
        if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
            $select_arr = json_decode($_POST["select_json"], true);
            $like["product_tech_" . $select_arr['attr']] = $select_arr['value'];
        } else {
            //$select_arr = "";
            $like = "";
        }
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $where = "";

        $data['listData'] = $this->product_tech->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
        $data["labels"] = $this->product_tech->attributeLabels();
        //如果包含对象类型，添加一个数组，用于视图获取

        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data['totalNumber'] = $this->product_tech->countGetInfo($where, $like); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        //p($data);
        $this->output->enable_profiler(false);
        $this->load->view('www/product_tech/ajax_select_quote', $data);
    }

}

?>