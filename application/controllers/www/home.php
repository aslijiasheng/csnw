<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Home extends WWW_Controller {

	public $menu1 = 'home';

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->model('www/api_model', 'api');
		$this->load->model('www/message_model', 'message');
		$this->message->db->query('SET NAMES UTF8');

		$data = $this->get_data_auth('message');
		// p(json_decode($data));
		// if(empty($data)){
		// 	$data['count'] =0;
		// }else{
		// 	$data['where'] = object_array(json_decode($data['where']));
		// 	p($data['where_rel']);
		// $where_all=$data['where'];
		// 		if($data['where_rel']==""){
		// 			$rel_all = "";
		// 		}else{
		// 			$rel_all = "(".$data['where_rel'].")";
		// 		}
		$new_where_ok = array(
			"page" => 1,
			"perNumber" => 5,
			"obj" => 'message',
		);

		$user_id = $this->session->userdata('user_id');
		$new_where_ok['where_all']['w3'] = array('attr' => 'status', 'value' => 1001, 'action' => '=');
		$new_where_ok['where_all']['w1'] = array('attr' => 'owner', 'value' => $user_id, 'action' => '=');
		$new_where_ok['where_all']['w5'] = array('attr' => 'module_id', 'value' => 0, 'action' => 'NOT_EQUAL');
		$new_where_ok['rel_all'] = 'w1 and w3 and w4 and w5';

		//开票
		$new_where_ok['where_all']['w4'] = array('attr' => 'module', 'value' => 'invoice', 'action' => '=');
		$listData = $this->api->select($new_where_ok);
		$data['invoice_count'] = $listData['count'];

		//入账
		$new_where_ok['where_all']['w4'] = array('attr' => 'module', 'value' => 'salespay', 'action' => '=');
		$listData = $this->api->select($new_where_ok);
		$data['salespay_count'] = $listData['count'];

		//返点
		$new_where_ok['where_all']['w4'] = array('attr' => 'module', 'value' => 'rebate', 'action' => '=');
		$listData = $this->api->select($new_where_ok);
		$data['rebate_count'] = $listData['count'];

		//退款
		$new_where_ok['where_all']['w4'] = array('attr' => 'module', 'value' => 'refund', 'action' => '=');
		$listData = $this->api->select($new_where_ok);
		$data['refund_count'] = $listData['count'];

		//开通待办
		$new_where_ok['where_all']['w4'] = array('attr' => 'module', 'value' => 'applyopen', 'action' => '=');
		$listData = $this->api->select($new_where_ok);
		$data['applyopen_count'] = $listData['count'];

		//作废订单
		$new_where_ok['where_all']['w4'] = array('attr' => 'module', 'value' => 'order', 'action' => '=');
		$listData = $this->api->select($new_where_ok);
		
		//审核订单作废及变更作废
		$where_in = array("1018", "1014");
		$where = array( "message_type"=>$where_in,"message_owner"=>$user_id,"message_status"=>"1001");
		$data['cancel_check_count'] = $this->message->getCount( $where );
		//PC::debug($this->db->last_query());
		
		//确认订单作废及变更作废
		$where_in = array("1019", "1015");
		$where = array( "message_type"=>$where_in,"message_owner"=>$user_id,"message_status"=>"1001");
		$data['cancel_affirm_count'] = $this->message->getCount( $where );
		
		//$data['cancel_count'] = $listData['count'];

		//待查账
		$data_arr = array('6' => 'sale_order', '7' => 'online_order', '8' => 'rebates_order', '9' => 'penny_order', '10' => 'deposit_order', '13' => 'delivery_order');
		$whereSql = '';
		foreach ($data_arr as $k => $v) {
			$data_auth = $this->get_data_auth($v);
			if (!empty($data_auth) && $data_auth['where'] != '\'\'') {
				$where = (json_decode($data_auth['where'], true));
				foreach ($where as $k2 => $v2) {
					$w_sql = '';
					if ($v2['attr'] == 'department.id') {
						$department_id = str_pad($v2['value'], 4, "0", STR_PAD_LEFT);
						$department_id_arr = $this->db->select('department_id')->like('department_treepath', $department_id, 'both')->get('tc_department')->result_array();
						if ($department_id_arr) {
							$d_arr = array();
							foreach ($department_id_arr as $dv) {
								$d_arr[] = $dv['department_id'];
							}
							$department_ids = implode(',', $d_arr);
						}
						$w_sql .= ' AND order_department IN (' . $department_ids . ')';
					} else {
						$attr = explode('.', $v2['attr']);
						$w_sql .= ' AND order_' . $attr[0] . '=' . $v2['value'];
					}
				}
				$w_sql = ltrim($w_sql, ' AND');
				$whereSql .= '(' . $w_sql . ') AND ';
			} elseif (!empty($data_auth) && $data_auth['where'] == '\'\'') {
//				$w_sql = 'type_id = ' . $k;
//				$whereSql .= $w_sql . ' AND ';
			} else {
				$w_sql = 'type_id != ' . $k;
				$whereSql .= $w_sql . ' AND ';
			}
		}
		$whereSql = empty($whereSql) ? '' : $whereSql;
		/*
		2014-09-16 待查账造成慢查询 取消功能
		$sql = 'SELECT * FROM tc_order WHERE 1 AND ' . $whereSql . ' order_id IN ( SELECT salespay_order_id FROM `tc_salespay` WHERE salespay_money_type = 1001 AND salespay_status = 1001 ) ORDER BY order_id DESC';
		$r_order = $this->db->query($sql)->result_array();
		$data['audit_count'] = count($r_order);
		*/

		//内划订单
		$new_where_ok['where_all']['w4'] = array('attr' => 'module', 'value' => 'pennyOrder', 'action' => '=');
		$listData = $this->api->select($new_where_ok);
		$data['pennyOrder_count'] = $listData['count'];

//		if ($user_id) {
//			$sql = 'SELECT salespay_id FROM tc_salespay WHERE 1 AND salespay_status=1001 AND salespay_id IN (SELECT message_module_id FROM tc_message WHERE message_module="salespay" AND message_owner=' . $user_id . ' AND message_status=1001)';
//			$res_salespay = $this->db->query($sql)->result_array();
//			$where = '';
//			if ($res_salespay) {
//				foreach ($res_salespay as $v) {
//					$where .= $v['salespay_id'] . ',';
//				}
//				$where = rtrim($where, ',');
//			}
//			$new_where_ok['where_all']['w4'] = array('attr' => 'module', 'value' => 'salespay', 'action' => '=');
//			$new_where_ok['where_all']['w6'] = array('attr' => 'module_id', 'value' => $where, 'action' => 'INC');
//			$new_where_ok['rel_all'] = 'w1 and w3 and w4 and w5 and w6';
//			$listData = $this->api->select($new_where_ok);
//			p($listData);exit;
//			$data['audit_count'] = $listData['count'];
//		}
		$this->render('www/home', $data);
	}

}

?>