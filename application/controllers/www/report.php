<?php

class Report extends WWW_Controller {

    public $menu1 = 'xssrmx';

    public function __construct() {
        parent::__construct();
        $this->db->query('SET NAMES UTF8');
        $this->load->model('admin/type_model', 'type');
        $this->load->model('admin/enum_model', 'enum');
    }

    private $cellArray = array(
        1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E',
        6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J',
        11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O',
        16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T',
        21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y',
        26 => 'Z',
        27 => 'AA', 28 => 'AB', 29 => 'AC', 30 => 'AD', 31 => 'AE',
        32 => 'AF', 33 => 'AG', 34 => 'AH', 35 => 'AI', 36 => 'AJ',
        37 => 'AK', 38 => 'AL', 39 => 'AM', 40 => 'AN', 41 => 'AO',
        42 => 'AP', 43 => 'AQ', 44 => 'AR', 45 => 'AS', 46 => 'AT',
        47 => 'AU', 48 => 'AV', 49 => 'AW', 50 => 'AX', 51 => 'AY',
        52 => 'AZ', 53 => 'BA', 54 => 'BB', 55 => 'BC', 56 => 'BD', 57 => 'BE',
        58 => 'BF', 59 => 'BG', 60 => 'BH', 61 => 'BI', 62 => 'BJ', 63 => 'BK', 64 => 'BL');

    public function index() {
        $this->xssrmx();
    }

    public function xssrmx($return = false) {
        // 权限
        $data_auth = $this->get_data_auth('report');
        $where_data_auth = $this->discon($data_auth);

        //销售收入明细
        $this->menu1 = 'xssrmx';
        //默认值
        $data['start_date'] = date('Y-m' . "-01");
        $data['end_date'] = date('Y-m-d');
        if (isset($_POST['start_date'])) {
            $data['start_date'] = $_POST['start_date'];
        }
        if (isset($_POST['end_date'])) {
            $data['end_date'] = $_POST['end_date'];
        }
        //组合查询条件
        $where = $where_data_auth . "s.salespay_sp_date >= '" . $data['start_date'] . " 00:00:00' and s.salespay_sp_date < '" . $data['end_date'] . " 23:59:59' and (o.order_deposit_paytype!='1001' or o.order_deposit_paytype is null) and (s.salespay_money_type_name!='1002' or s.salespay_money_type_name is null)";
        if (isset($_POST['department'])) {
            if ($_POST['department'] != "" and $_POST['new_department'] != "") {
                //这里要做成隶属于的关系！所以需要查询出所有这个部门的子集
                $sql_d = "select department_id from tc_department where department_treepath like concat((select department_treepath from tc_department where department_id = '" . $_POST['department'] . "'),'%')";
                $d_data = $this->db->query($sql_d)->result_array();
                //p($d_data);exit;
                foreach ($d_data as $d_k => $d_v) {
                    $d_data[$d_k] = $d_v['department_id'];
                }
                $d_data_str = implode(",", $d_data);
                $where = $where . " and o.order_department in(" . $d_data_str . ")";
            }
        }
        if (isset($_POST['user'])) {
            if ($_POST['user'] != "" and $_POST['new_user'] != "") {
                $where = $where . " and o.order_owner = '" . $_POST['user'] . "'";
            }
        }
        $sql = "
select
o.order_id as order_id,
o.order_number as order_number,
o.type_id as order_type,
a.account_name as account_name,
a.type_id as account_tyoe,
p.partner_name as partner_name,
d.department_name as department_name,
ou.user_name as owner_name,
s.salespay_subsidiary as salespay_subsidiary,
s.salespay_pay_amount as salespay_pay_amount,
s.salespay_sp_date as salespay_sp_date,
s.salespay_money_type as salespay_money_type,
s.salespay_money_type_name as salespay_money_type_name,
s.salespay_pay_method as salespay_pay_method,
s.salespay_status as salespay_status,
s.salespay_my_bank_account as salespay_my_bank_account
from tc_salespay s
left join tc_order o on s.salespay_order_id = o.order_id
left join tc_account a on a.account_id = o.order_account
left join tc_department d on d.department_id = o.order_department
left join tc_user ou on o.order_owner = ou.user_id
left join tc_partner p on p.partner_id = o.order_agent
where s.salespay_status=1002 and " . $where . "
order by s.salespay_sp_date
		";
        /*
          o.order_number as 订单编号,
          o.type_id as 订单类型,
          a.account_name as 客户名称,
          a.type_id as 客户类型,
          p.partner_name as 经销商名称,
          d.department_name as 所属部门,
          ou.user_name as 所属销售,
          s.salespay_pay_amount as 款项金额,
          s.salespay_sp_date as 确认到帐时间,
          s.salespay_money_type as 款项类型,
          s.salespay_pay_method as 支付方式,
          s.salespay_status as 到账状态,
          s.salespay_my_bank_account as 商派银行账号
         */
        $data['data'] = $this->db->query($sql)->result_array();
        //p($data['data'][0]);
        //订单类型的数组,用于获取客户类型的中文标签
        $data['order_type_arr'] = $this->type->obj_id_GetInfo(20);
        //客户类型的数组,用于获取客户类型的中文标签
        $data['account_type_arr'] = $this->type->obj_id_GetInfo(1);
        //获取【所属子公司】的中文标签
        $data['salespay_subsidiary_attr'] = $this->enum->attr_id_GetInfo(629);
        //获取【款项类型】的中文标签
        $data['money_type_arr'] = $this->enum->attr_id_GetInfo(545);
        //获取【款项类型名称】的中文标签
        $data['money_type_name_arr'] = $this->enum->attr_id_GetInfo(564);
        //获取【支付方式】的中文标签
        $data['pay_method_arr'] = $this->enum->attr_id_GetInfo(144);
        //获取【商派银行账号】的中文标签
        $data['my_bank_account_arr'] = $this->enum->attr_id_GetInfo(363);
        //p($data['pay_method_arr']);
        //p($data['my_bank_account_arr']);
        //循环数组，计算合计以及查询产品信息等
        foreach ($data['data'] as $k => $v) {
            //获取产品明细里的产品名称
            $sql2 = "select order_d_goods_name from tc_order_d where order_d_order_id = '" . $v['order_id'] . "'";
            $order_d_data = $this->db->query($sql2)->result_array();
            $order_d_data2 = array();
            foreach ($order_d_data as $k2 => $v2) {
                if (!in_array($v2['order_d_goods_name'], $order_d_data2)) {
                    $order_d_data2[] = $v2['order_d_goods_name'];
                }
            }
            $data['data'][$k]['goods_name'] = implode(',', $order_d_data2);

            //获取订单开票金额
            $sql3 = "select sum(invoice_amount) as sum_invoice_amount from tc_invoice where invoice_order_id = '" . $v['order_id'] . "' and invoice_status='1002'";
            $res3 = $this->db->query($sql3)->result_array();
            $data['data'][$k]['sum_invoice_amount'] = $res3[0]['sum_invoice_amount'];


            //判断红冲 2014/08/28
            if ($v['salespay_money_type_name'] == "1005") {
                $v['salespay_pay_amount'] = "-" . $v['salespay_pay_amount'];
            }

            //将各种支付方式分类
            if ($data['data'][$k]['salespay_money_type'] == 1001) {
                $data['data'][$k]['salespay_pay_amount'] = $data['data'][$k]['salespay_pay_amount'];
            }

            if ($data['data'][$k]['salespay_money_type'] == 1002) {
                $data['data'][$k]['salespay_pay_amount'] = 0 - $data['data'][$k]['salespay_pay_amount'];
            }

            if (isset($v['salespay_pay_method'])) {
                //归类支付宝
                if ($data['pay_method_arr'][$v['salespay_pay_method']] == '支付宝') {
                    //if ($data['data'][$k]['salespay_money_type'] == 1001) {
                    $data['data'][$k]['zfb_amount'] = $data['data'][$k]['salespay_pay_amount'];
                    //}
                    //if ($data['data'][$k]['salespay_money_type'] == 1002) {
                    //	p($data['data'][$k]['salespay_pay_amount']);exit;
                    //	$data['data'][$k]['zfb_amount'] = 0 - $data['data'][$k]['salespay_pay_amount'];
                    //}
                } else {
                    $data['data'][$k]['zfb_amount'] = 0;
                }
                //归类现金
                if ($data['pay_method_arr'][$v['salespay_pay_method']] == '现金') {
                    //if ($data['data'][$k]['salespay_money_type'] == 1001) {
                    $data['data'][$k]['xj_amount'] = $data['data'][$k]['salespay_pay_amount'];
                    //}
                    //if ($data['data'][$k]['salespay_money_type'] == 1002) {
                    //$data['data'][$k]['xj_amount'] = 0 - $data['data'][$k]['salespay_pay_amount'];
                    //}
                } else {
                    $data['data'][$k]['xj_amount'] = 0;
                }
                //支付类型为汇款，各种银行账号分别归类
                if ($data['pay_method_arr'][$v['salespay_pay_method']] == '汇款') {
					
                    foreach ($data['my_bank_account_arr'] as $smba_k => $smba_v) {
                        if ($data['data'][$k]['salespay_my_bank_account'] == $smba_k) {
                            //	if ($data['data'][$k]['salespay_money_type'] == 1001) {
                            $data['data'][$k]['hk_amount'][$smba_k] = $data['data'][$k]['salespay_pay_amount'];
                            //	}
                            //	if ($data['data'][$k]['salespay_money_type'] == 1002) {
                            //		$data['data'][$k]['hk_amount'][$smba_k] = 0 - $data['data'][$k]['salespay_pay_amount'];
                            //	}
                        } else {
                            $data['data'][$k]['hk_amount'][$smba_k] = 0;
                        }
                    }
					
					//退款项
					if ($data['data'][$k]['salespay_money_type_name'] == "1003") {
						
					}
					
                } else {
                    foreach ($data['my_bank_account_arr'] as $smba_k => $smba_v) {
                        $data['data'][$k]['hk_amount'][$smba_k] = 0;
                    }
                }
            } else {
                $data['data'][$k]['zfb_amount'] = 0;
                $data['data'][$k]['xj_amount'] = 0;
                foreach ($data['my_bank_account_arr'] as $smba_k => $smba_v) {
                    $data['data'][$k]['hk_amount'][$smba_k] = 0;
                }
            }

            //汇总支付宝
            if (isset($data['hz']['zfb_amount'])) {
                $data['hz']['zfb_amount'] = $data['data'][$k]['zfb_amount'] + $data['hz']['zfb_amount'];
            } else {
                $data['hz']['zfb_amount'] = $data['data'][$k]['zfb_amount'];
            }
            //汇总现金
            if (isset($data['hz']['xj_amount'])) {
                $data['hz']['xj_amount'] = $data['data'][$k]['xj_amount'] + $data['hz']['xj_amount'];
            } else {
                $data['hz']['xj_amount'] = $data['data'][$k]['xj_amount'];
            }

            //汇总所有银行的
            foreach ($data['my_bank_account_arr'] as $smba_k => $smba_v) {
                if (isset($data['hz']['hk_amount'][$smba_k])) {
                    //if('aaa'=>)
                    $data['hz']['hk_amount'][$smba_k] = $data['data'][$k]['hk_amount'][$smba_k] + $data['hz']['hk_amount'][$smba_k];
                } else {
                    $data['hz']['hk_amount'][$smba_k] = $data['data'][$k]['hk_amount'][$smba_k];
                }
            }

            //合计已开票金额
            if (isset($data['hz']['sum_invoice_amount'])) {
                $data['hz']['sum_invoice_amount'] = $data['data'][$k]['sum_invoice_amount'] + $data['hz']['sum_invoice_amount'];
            } else {
                $data['hz']['sum_invoice_amount'] = $data['data'][$k]['sum_invoice_amount'];
            }

            //合计总金额
            if (isset($data['hz']['salespay_pay_amount'])) {
                $data['hz']['salespay_pay_amount'] = $data['data'][$k]['salespay_pay_amount'] + $data['hz']['salespay_pay_amount'];
            } else {
                $data['hz']['salespay_pay_amount'] = $data['data'][$k]['salespay_pay_amount'];
            }

            //合计入账信息总金额
            if ($data['data'][$k]['salespay_money_type'] == 1001) {
                if (isset($data['hz']['rz_amount'])) {
                    $data['hz']['rz_amount'] = $data['data'][$k]['salespay_pay_amount'] + $data['hz']['rz_amount'];
                } else {
                    $data['hz']['rz_amount'] = $data['data'][$k]['salespay_pay_amount'];
                }
            }

            //合计返点款项金额
            if ($data['data'][$k]['salespay_money_type_name'] == 1002) {
                if (isset($data['hz']['fd_amount'])) {
                    $data['hz']['fd_amount'] = $data['data'][$k]['salespay_pay_amount'] + $data['hz']['fd_amount'];
                } else {
                    $data['hz']['fd_amount'] = $data['data'][$k]['salespay_pay_amount'];
                }
            }

            //合计返点款项金额
            if ($data['data'][$k]['salespay_money_type_name'] == 1003) {
                if (isset($data['hz']['tk_amount'])) {
                    $data['hz']['tk_amount'] = $data['data'][$k]['salespay_pay_amount'] + $data['hz']['tk_amount'];
                } else {
                    $data['hz']['tk_amount'] = $data['data'][$k]['salespay_pay_amount'];
                }
            }
        }

        $data['user_auth'] = $this->user->user_auth($this->session->userdata('user_id'));
        if ($return) {
            return $data;
        }
		//p($data);
		//die;
        $this->render('www/report/xssrmx', $data);
    }

    public function xssrjsmx($return = false) {
        // 权限
        $data_auth = $this->get_data_auth('report');
        $where_data_auth = $this->discon($data_auth);

        //销售收入明细
        $this->menu1 = 'xssrjsmx';
        //默认值
        $data['start_date'] = date('Y-m' . "-01");
        $data['end_date'] = date('Y-m-d');
        if (isset($_POST['start_date'])) {
            $data['start_date'] = $_POST['start_date'];
        }
        if (isset($_POST['end_date'])) {
            $data['end_date'] = $_POST['end_date'];
        }
        //组合查询条件
        //$where = $where_data_auth . "s.salespay_sp_date > '" . $data['start_date'] . " 00:00:00' and s.salespay_sp_date < '" . $data['end_date'] . " 23:59:59'";
        $where = $where_data_auth . "s.salespay_sp_date >= '" . $data['start_date'] . " 00:00:00' and s.salespay_sp_date < '" . $data['end_date'] . " 23:59:59' and (o.order_deposit_paytype!='1001' or o.order_deposit_paytype is null) and (s.salespay_money_type_name!='1002' or s.salespay_money_type_name is null)";
        if (isset($_POST['department'])) {
            if ($_POST['department'] != "" and $_POST['new_department'] != "") {
                //这里要做成隶属于的关系！所以需要查询出所有这个部门的子集
                $sql_d = "select department_id from tc_department where department_treepath like concat((select department_treepath from tc_department where department_id = '" . $_POST['department'] . "'),'%')";
                $d_data = $this->db->query($sql_d)->result_array();
                //p($d_data);exit;
                foreach ($d_data as $d_k => $d_v) {
                    $d_data[$d_k] = $d_v['department_id'];
                }
                $d_data_str = implode(",", $d_data);
                $where = $where . " and o.order_department in(" . $d_data_str . ")";
            }
        }
        if (isset($_POST['user'])) {
            if ($_POST['user'] != "" and $_POST['new_user'] != "") {
                $where = $where . " and o.order_owner = '" . $_POST['user'] . "'";
            }
        }
        $sql = "
select
o.order_id as order_id,
o.order_number as order_number,
o.type_id as order_type,
a.account_name as account_name,
a.type_id as account_tyoe,
p.partner_name as partner_name,
d.department_name as department_name,
ou.user_name as owner_name,
s.salespay_subsidiary as salespay_subsidiary,
s.salespay_pay_amount as salespay_pay_amount,
s.salespay_sp_date as salespay_sp_date,
s.salespay_money_type as salespay_money_type,
s.salespay_money_type_name as salespay_money_type_name,
s.salespay_pay_method as salespay_pay_method,
s.salespay_status as salespay_status,
s.salespay_my_bank_account as salespay_my_bank_account
from tc_salespay s
left join tc_order o on s.salespay_order_id = o.order_id
left join tc_account a on a.account_id = o.order_account
left join tc_department d on d.department_id = o.order_department
left join tc_user ou on o.order_owner = ou.user_id
left join tc_partner p on p.partner_id = o.order_agent
where s.salespay_status=1002 and " . $where . "
order by s.salespay_sp_date
		";
        /*
          o.order_number as 订单编号,
          o.type_id as 订单类型,
          a.account_name as 客户名称,
          a.type_id as 客户类型,
          p.partner_name as 经销商名称,
          d.department_name as 所属部门,
          ou.user_name as 所属销售,
          s.salespay_pay_amount as 款项金额,
          s.salespay_sp_date as 确认到帐时间,
          s.salespay_money_type as 款项类型,
          s.salespay_pay_method as 支付方式,
          s.salespay_status as 到账状态,
          s.salespay_my_bank_account as 商派银行账号
         */
        $data['data'] = $this->db->query($sql)->result_array();
        //p($data['data'][0]);
        //订单类型的数组,用于获取客户类型的中文标签
        $data['order_type_arr'] = $this->type->obj_id_GetInfo(20);
        //客户类型的数组,用于获取客户类型的中文标签
        $data['account_type_arr'] = $this->type->obj_id_GetInfo(1);
        //获取【所属子公司】的中文标签
        $data['salespay_subsidiary_attr'] = $this->enum->attr_id_GetInfo(629);
        //获取【款项类型】的中文标签
        $data['money_type_arr'] = $this->enum->attr_id_GetInfo(545);
        //获取【款项类型名称】的中文标签
        $data['money_type_name_arr'] = $this->enum->attr_id_GetInfo(564);
        //获取【支付方式】的中文标签
        $data['pay_method_arr'] = $this->enum->attr_id_GetInfo(144);
        //获取【商派银行账号】的中文标签
        $data['my_bank_account_arr'] = $this->enum->attr_id_GetInfo(363);
        //p($data['pay_method_arr']);
        //p($data['my_bank_account_arr']);
        //循环数组，计算合计以及查询产品信息等

        foreach ($data['data'] as $k => $v) {
            //获取产品明细里的产品名称
            $sql2 = "select order_d_goods_name from tc_order_d where order_d_order_id = '" . $v['order_id'] . "'";
            $order_d_data = $this->db->query($sql2)->result_array();
            $order_d_data2 = array();
            foreach ($order_d_data as $k2 => $v2) {
                if (!in_array($v2['order_d_goods_name'], $order_d_data2)) {
                    $order_d_data2[] = $v2['order_d_goods_name'];
                }
            }
            $data['data'][$k]['goods_name'] = implode(',', $order_d_data2);

            //获取订单开票金额
            $sql3 = "select sum(invoice_amount) as sum_invoice_amount from tc_invoice where invoice_order_id = '" . $v['order_id'] . "' and invoice_do_status='1001'";
            $res3 = $this->db->query($sql3)->result_array();
            $data['data'][$k]['sum_invoice_amount'] = $res3[0]['sum_invoice_amount'];

            //判断红冲 2014/08/28
            if ($v['salespay_money_type_name'] == "1005") {
                $v['salespay_pay_amount'] = "-" . $v['salespay_pay_amount'];
            }

            //将各种支付方式分类
            if ($data['data'][$k]['salespay_money_type'] == 1001) {
                $data['data'][$k]['salespay_pay_amount'] = $data['data'][$k]['salespay_pay_amount'];
            }

            if ($data['data'][$k]['salespay_money_type'] == 1002) {
                $data['data'][$k]['salespay_pay_amount'] = 0 - $data['data'][$k]['salespay_pay_amount'];
            }

            if (isset($v['salespay_pay_method'])) {
                //归类支付宝
                if ($data['pay_method_arr'][$v['salespay_pay_method']] == '支付宝') {
                    //if ($data['data'][$k]['salespay_money_type'] == 1001) {
                    $data['data'][$k]['zfb_amount'] = $data['data'][$k]['salespay_pay_amount'];
                    // }
                    // if ($data['data'][$k]['salespay_money_type'] == 1002) {
                    // 	$data['data'][$k]['zfb_amount'] = 0 - $data['data'][$k]['salespay_pay_amount'];
                    // }
                } else {
                    $data['data'][$k]['zfb_amount'] = 0;
                }
                //归类现金
                if ($data['pay_method_arr'][$v['salespay_pay_method']] == '现金') {
                    //if ($data['data'][$k]['salespay_money_type'] == 1001) {
                    $data['data'][$k]['xj_amount'] = $data['data'][$k]['salespay_pay_amount'];
                    // }
                    // if ($data['data'][$k]['salespay_money_type'] == 1002) {
                    // 	$data['data'][$k]['xj_amount'] = 0 - $data['data'][$k]['salespay_pay_amount'];
                    // }
                } else {
                    $data['data'][$k]['xj_amount'] = 0;
                }
                //支付类型为汇款，各种银行账号分别归类
                if ($data['pay_method_arr'][$v['salespay_pay_method']] == '汇款') {
                    foreach ($data['my_bank_account_arr'] as $smba_k => $smba_v) {
                        if ($data['data'][$k]['salespay_my_bank_account'] == $smba_k) {
                            //if ($data['data'][$k]['salespay_money_type'] == 1001) {
                            $data['data'][$k]['hk_amount'][$smba_k] = $data['data'][$k]['salespay_pay_amount'];
                            // }
                            // if ($data['data'][$k]['salespay_money_type'] == 1002) {
                            // 	$data['data'][$k]['hk_amount'][$smba_k] = 0 - $data['data'][$k]['salespay_pay_amount'];
                            // }
                        } else {
                            $data['data'][$k]['hk_amount'][$smba_k] = 0;
                        }
                    }
                } else {
                    foreach ($data['my_bank_account_arr'] as $smba_k => $smba_v) {
                        $data['data'][$k]['hk_amount'][$smba_k] = 0;
                    }
                }
            } else {
                $data['data'][$k]['zfb_amount'] = 0;
                $data['data'][$k]['xj_amount'] = 0;
                foreach ($data['my_bank_account_arr'] as $smba_k => $smba_v) {
                    $data['data'][$k]['hk_amount'][$smba_k] = 0;
                }
            }

            //汇总支付宝
            if (isset($data['hz']['zfb_amount'])) {
                $data['hz']['zfb_amount'] = $data['data'][$k]['zfb_amount'] + $data['hz']['zfb_amount'];
            } else {
                $data['hz']['zfb_amount'] = $data['data'][$k]['zfb_amount'];
            }

            //汇总现金
            if (isset($data['hz']['xj_amount'])) {
                $data['hz']['xj_amount'] = $data['data'][$k]['xj_amount'] + $data['hz']['xj_amount'];
            } else {
                $data['hz']['xj_amount'] = $data['data'][$k]['xj_amount'];
            }

            //汇总所有银行的
            foreach ($data['my_bank_account_arr'] as $smba_k => $smba_v) {
                if (isset($data['hz']['hk_amount'][$smba_k])) {
                    //if('aaa'=>)
                    $data['hz']['hk_amount'][$smba_k] = $data['data'][$k]['hk_amount'][$smba_k] + $data['hz']['hk_amount'][$smba_k];
                } else {
                    $data['hz']['hk_amount'][$smba_k] = $data['data'][$k]['hk_amount'][$smba_k];
                }
            }

            //合计已开票金额
            if (isset($data['hz']['sum_invoice_amount'])) {
                $data['hz']['sum_invoice_amount'] = $data['data'][$k]['sum_invoice_amount'] + $data['hz']['sum_invoice_amount'];
            } else {
                $data['hz']['sum_invoice_amount'] = $data['data'][$k]['sum_invoice_amount'];
            }

            //合计总金额
            if (isset($data['hz']['salespay_pay_amount'])) {
                $data['hz']['salespay_pay_amount'] = $data['data'][$k]['salespay_pay_amount'] + $data['hz']['salespay_pay_amount'];
            } else {
                $data['hz']['salespay_pay_amount'] = $data['data'][$k]['salespay_pay_amount'];
            }

            //合计入账信息总金额
            if ($data['data'][$k]['salespay_money_type'] == 1001) {
                if (isset($data['hz']['rz_amount'])) {
                    $data['hz']['rz_amount'] = $data['data'][$k]['salespay_pay_amount'] + $data['hz']['rz_amount'];
                } else {
                    $data['hz']['rz_amount'] = $data['data'][$k]['salespay_pay_amount'];
                }
            }

            //合计返点款项金额
            if ($data['data'][$k]['salespay_money_type_name'] == 1002) {
                if (isset($data['hz']['fd_amount'])) {
                    $data['hz']['fd_amount'] = $data['data'][$k]['salespay_pay_amount'] + $data['hz']['fd_amount'];
                } else {
                    $data['hz']['fd_amount'] = $data['data'][$k]['salespay_pay_amount'];
                }
            }

            //合计返点款项金额
            if ($data['data'][$k]['salespay_money_type_name'] == 1003) {
                if (isset($data['hz']['tk_amount'])) {
                    $data['hz']['tk_amount'] = $data['data'][$k]['salespay_pay_amount'] + $data['hz']['tk_amount'];
                } else {
                    $data['hz']['tk_amount'] = $data['data'][$k]['salespay_pay_amount'];
                }
            }
        }

        // 权限控制
        $data['user_auth'] = $this->user->user_auth($this->session->userdata('user_id'));


        if ($return)
            return $data;

        $this->render('www/report/xssrjsmx', $data);
    }

    // 销售收入明细 导出
    public function xssrmxDownload() {
        // 权限控制
        $data['user_auth'] = $this->user->user_auth($this->session->userdata('user_id'));
        if (!in_array('xssrmxDownload', $data['user_auth']['activity_auth_arr'])) {
            die('没有权限');
        }

        //返回搜索结果
        $res = $this->xssrmx($return = true);

        //处理结果
        $data['listdata'] = $res['data'];
        //订单类型的数组,用于获取客户类型的中文标签
        $order_type_arr = $this->type->obj_id_GetInfo(20);
        //客户类型的数组,用于获取客户类型的中文标签
        $account_type_arr = $this->type->obj_id_GetInfo(1);
        //获取【所属子公司】的中文标签
        $salespay_subsidiary_arr = $this->enum->attr_id_GetInfo(629);
        //获取【款项类型】的中文标签
        $money_type_arr = $this->enum->attr_id_GetInfo(545);
        //获取【款项类型名称】的中文标签
        $money_type_name_arr = $this->enum->attr_id_GetInfo(564);
        //获取【支付方式】的中文标签
        $pay_method_arr = $this->enum->attr_id_GetInfo(144);
        //获取【商派银行账号】的中文标签
        $my_bank_account_arr = $this->enum->attr_id_GetInfo(363);

        $total = array(
            'alipay' => 0, 'cash' => 0, 'ICBC' => 0, 'BCM' => 0, 'SPDB' => 0, 'ICBC_PERSON' => 0, 'BOC_PERSON' => 0, 'CCB_PERSON' => 0, 'ABC_PERSON' => 0, 'CMB_PERSON' => 0, 'Paypal' => 0, 'BCM_BASIC' => 0, 'SMS_BASIC' => 0, 'CMB_BASIC' => 0, 'Tenpay' => 0, 'all' => 0, 'salespay_pay_amount' => 0, 'sum_invoice_amount' => 0
        );
        //PC::xssrmxDownload($data);
        foreach ($data['listdata'] as $k => $v) {
            $data['listdata'][$k]['salespay_sp_date'] = date('Y-m-d', strtotime($v['salespay_sp_date']));
            //日期
            if (isset($v['order_type']) && !empty($v['order_type']))
                $data['listdata'][$k]['order_type'] = $order_type_arr[$v['order_type']]; //订单类型
            if (isset($v['account_tyoe']) && !empty($v['account_tyoe']))
                $data['listdata'][$k]['account_tyoe'] = $account_type_arr[$v['account_tyoe']]; //客户类型
            if (isset($v['salespay_subsidiary']) && !empty($v['salespay_subsidiary']))
                $data['listdata'][$k]['salespay_subsidiary'] = $salespay_subsidiary_arr[$v['salespay_subsidiary']]; //所属子公司
//更新金额
            if (isset($v['salespay_money_type_name']) && !empty($v['salespay_money_type_name'])) {
                $data['listdata'][$k]['salespay_money_type_name'] = $money_type_name_arr[$v['salespay_money_type_name']]; //款项类型名称
            }
            if (isset($v['salespay_pay_method'])) { // 支付宝
                $data['listdata'][$k]['alipay'] = $data['listdata'][$k]['cash'] = '';
                $data['listdata'][$k]['ICBC'] = $data['listdata'][$k]['BCM'] = $data['listdata'][$k]['SPDB'] = $data['listdata'][$k]['ICBC_PERSON'] = $data['listdata'][$k]['BOC_PERSON'] = $data['listdata'][$k]['CCB_PERSON'] = $data['listdata'][$k]['ABC_PERSON'] = $data['listdata'][$k]['CMB_PERSON'] = $data['listdata'][$k]['Paypal'] = '';
                switch ($pay_method_arr[$v['salespay_pay_method']]) {
                    case '支付宝':
                        $data['listdata'][$k]['alipay'] = $v['salespay_pay_amount'];
                        $total['alipay'] += $data['listdata'][$k]['alipay']; // 统计支付宝
                        break;
                    case '现金':
                        if ($v['salespay_money_type'] == 1001)
                            $data['listdata'][$k]['cash'] = $v['salespay_pay_amount'];
                        elseif ($v['salespay_money_type'] == 1002)
                            $data['listdata'][$k]['cash'] = $v['salespay_pay_amount'];
                        $total['cash'] += $data['listdata'][$k]['cash']; // 统计现金
                        break;
                    case '汇款':
                        foreach ($my_bank_account_arr as $k2 => $v2) {
                            switch ($k2) {
                                case '1001':
                                    $data['listdata'][$k]['ICBC'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['ICBC'] += $data['listdata'][$k]['ICBC'];
                                    break;
                                case '1002':
                                    $data['listdata'][$k]['BCM'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['BCM'] += $data['listdata'][$k]['BCM'];
                                    break;
                                case '1003':
                                    $data['listdata'][$k]['SPDB'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['SPDB'] += $data['listdata'][$k]['SPDB'];
                                    break;
                                case '1004':
                                    $data['listdata'][$k]['ICBC_PERSON'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['ICBC_PERSON'] += $data['listdata'][$k]['ICBC_PERSON'];
                                    break;
                                case '1005':
                                    $data['listdata'][$k]['BOC_PERSON'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['BOC_PERSON'] += $data['listdata'][$k]['BOC_PERSON'];
                                    break;
                                case '1006':
                                    $data['listdata'][$k]['CCB_PERSON'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['CCB_PERSON'] += $data['listdata'][$k]['CCB_PERSON'];
                                    break;
                                case '1007':
                                    $data['listdata'][$k]['ABC_PERSON'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['ABC_PERSON'] += $data['listdata'][$k]['ABC_PERSON'];
                                    break;
                                case '1008':
                                    $data['listdata'][$k]['CMB_PERSON'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['CMB_PERSON'] += $data['listdata'][$k]['CMB_PERSON'];
                                    break;
                                case '1009':
                                    $data['listdata'][$k]['Paypal'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['Paypal'] += $data['listdata'][$k]['Paypal'];
                                    break;
                                case '1010':
                                    $data['listdata'][$k]['BCM_BASIC'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['BCM_BASIC'] += $data['listdata'][$k]['BCM_BASIC'];
                                    break;
                                case '1011':
                                    $data['listdata'][$k]['SMS_BASIC'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['SMS_BASIC'] += $data['listdata'][$k]['SMS_BASIC'];
                                    break;
                                case '1012':
                                    $data['listdata'][$k]['CMB_BASIC'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['CMB_BASIC'] += $data['listdata'][$k]['CMB_BASIC'];
                                    break;
								case '1013':
									$data['listdata'][$k]['Tenpay'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['Tenpay'] += $data['listdata'][$k]['Tenpay'];
                                    break;
                            }
                        }
                        break;
                    default:
                        break;
                }
                $data['listdata'][$k]['total'] = $data['listdata'][$k]['BCM_BASIC'] + $data['listdata'][$k]['SMS_BASIC'] + $data['listdata'][$k]['CMB_BASIC'] + $data['listdata'][$k]['alipay'] + $data['listdata'][$k]['cash'] + $data['listdata'][$k]['ICBC'] + $data['listdata'][$k]['BCM'] + $data['listdata'][$k]['SPDB'] + $data['listdata'][$k]['ICBC_PERSON'] + $data['listdata'][$k]['BOC_PERSON'] + $data['listdata'][$k]['CCB_PERSON'] + $data['listdata'][$k]['ABC_PERSON'] + $data['listdata'][$k]['CMB_PERSON'] + $data['listdata'][$k]['Paypal']+ $data['listdata'][$k]['Tenpay'];
                $total['salespay_pay_amount'] += $v['salespay_pay_amount'];
                $total['sum_invoice_amount'] += $v['sum_invoice_amount'];
                $total['all'] += $data['listdata'][$k]['total'];
            }
        }

        // 用PHPexcel生产excel文件
        require_once 'Classes/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $attr_data = array(
            'salespay_sp_date' => '日期',
            'order_type' => '订单类型',
            'order_number' => '订单号',
            'salespay_subsidiary' => '所属子公司',
            'account_tyoe' => '客户类型',
            'account_name' => '客户名称',
            'partner_name' => '经销商名称',
            'goods_name' => '购买商品',
            'owner_name' => '所属销售',
            'department_name' => '所属部门',
            'salespay_money_type_name' => '款项类型名称',
            'salespay_pay_amount' => '款项金额',
            'sum_invoice_amount' => '已开票金额',
            'alipay' => '支付宝',
            'cash' => '现金',
            'ICBC' => '工商银行（基本户）',
            'BCM' => '交通银行（一般户）',
            'SPDB' => '上海银行（一般户）',
            'ICBC_PERSON' => '工行（个人）',
            'BOC_PERSON' => '中行（个人）',
            'CCB_PERSON' => '建行（个人）',
            'ABC_PERSON' => '农行（个人）',
            'CMB_PERSON' => '招行（个人）',
            'Paypal' => 'Paypal（贝宝）',
            'BCM_BASIC' => '交通银行（基本户）商派软件 ',
            'SMS_BASIC' => 'SMS@shopex.cn（商派软件）',
            'CMB_BASIC' => '招行延安东路支行（商派软件）',
			'Tenpay' => '财付通（商派软件）',
            'total' => '合计'
        );

        // 标题
        $i = 0;
        foreach ($attr_data as $k => $v) {
            $i++;
            $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[$i] . '1', $v);
        }


        // 下面类容 循环
        for ($i = 1; $i <= count($data['listdata']); $i++) {
            $ii = 0;
            foreach ($attr_data as $k => $v) {
                $ii++;
                $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[$ii] . ($i + 1), $data['listdata'][$i - 1][$k]);
            }
        }

        // 统计
        $i = count($data['listdata']);
        $j = 12;
        //PC::total($total);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[1] . ($i + 2), '统计');
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[$j] . ($i + 2), $total['salespay_pay_amount']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['sum_invoice_amount']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['alipay']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['cash']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['ICBC']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['BCM']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['SPDB']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['ICBC_PERSON']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['BOC_PERSON']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['CCB_PERSON']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['ABC_PERSON']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['CMB_PERSON']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['Paypal']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['BCM_BASIC']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['SMS_BASIC']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['CMB_BASIC']);
		$objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['Tenpay']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['all']);

        // Set document security
        $objPHPExcel->getSecurity()->setLockWindows(true);
        $objPHPExcel->getSecurity()->setLockStructure(true);
        $objPHPExcel->getSecurity()->setWorkbookPassword("PHPExcel");

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $lee_excel_url = "upload/execl_11_" . date('YmdHis') . ".xls";
        $objWriter->save($lee_excel_url);
        echo base_url($lee_excel_url);
    }

    // 销售收入结算明细 导出
    public function xssrjsmxDownload() {

        // 权限控制
        $data['user_auth'] = $this->user->user_auth($this->session->userdata('user_id'));
        if (!in_array('xssrjsmxDownload', $data['user_auth']['activity_auth_arr'])) {
            die('没有权限');
        }

        //返回搜索结果
        $res = $this->xssrjsmx($return = true);

        //处理结果
        $data['listdata'] = $res['data'];
        //订单类型的数组,用于获取客户类型的中文标签
        $order_type_arr = $this->type->obj_id_GetInfo(20);
        //客户类型的数组,用于获取客户类型的中文标签
        $account_type_arr = $this->type->obj_id_GetInfo(1);
        //获取【所属子公司】的中文标签
        $salespay_subsidiary_arr = $this->enum->attr_id_GetInfo(629);
        //获取【款项类型】的中文标签
        $money_type_arr = $this->enum->attr_id_GetInfo(545);
        //获取【款项类型名称】的中文标签
        $money_type_name_arr = $this->enum->attr_id_GetInfo(564);
        //获取【支付方式】的中文标签
        $pay_method_arr = $this->enum->attr_id_GetInfo(144);
        //获取【商派银行账号】的中文标签
        $my_bank_account_arr = $this->enum->attr_id_GetInfo(363);

        $total = array(
            'alipay' => 0, 'cash' => 0, 'ICBC' => 0, 'BCM' => 0, 'SPDB' => 0, 'ICBC_PERSON' => 0, 'BOC_PERSON' => 0, 'CCB_PERSON' => 0, 'ABC_PERSON' => 0, 'CMB_PERSON' => 0, 'Paypal' => 0, 'all' => 0, 'salespay_pay_amount' => 0, 'sum_invoice_amount' => 0
        );

        foreach ($data['listdata'] as $k => $v) {
            $data['listdata'][$k]['salespay_sp_date'] = date('Y-m-d', strtotime($v['salespay_sp_date'])); //日期
            if (isset($v['order_type']) && !empty($v['order_type']))
                $data['listdata'][$k]['order_type'] = $order_type_arr[$v['order_type']]; //订单类型
            if (isset($v['account_tyoe']) && !empty($v['account_tyoe']))
                $data['listdata'][$k]['account_tyoe'] = $account_type_arr[$v['account_tyoe']]; //客户类型
            if (isset($v['salespay_subsidiary']) && !empty($v['salespay_subsidiary']))
                $data['listdata'][$k]['salespay_subsidiary'] = $salespay_subsidiary_arr[$v['salespay_subsidiary']]; //所属子公司
            if (isset($v['salespay_money_type_name']) && !empty($v['salespay_money_type_name']))
                $data['listdata'][$k]['salespay_money_type_name'] = $money_type_name_arr[$v['salespay_money_type_name']]; //款项类型名称
            if (isset($v['salespay_pay_method'])) { // 支付宝
                $data['listdata'][$k]['alipay'] = $data['listdata'][$k]['cash'] = '';
                $data['listdata'][$k]['ICBC'] = $data['listdata'][$k]['BCM'] = $data['listdata'][$k]['SPDB'] = $data['listdata'][$k]['ICBC_PERSON'] = $data['listdata'][$k]['BOC_PERSON'] = $data['listdata'][$k]['CCB_PERSON'] = $data['listdata'][$k]['ABC_PERSON'] = $data['listdata'][$k]['CMB_PERSON'] = $data['listdata'][$k]['Paypal'] = '';
                switch ($pay_method_arr[$v['salespay_pay_method']]) {
                    case '支付宝':
                        $data['listdata'][$k]['alipay'] = $v['salespay_pay_amount'];
                        $total['alipay'] += $data['listdata'][$k]['alipay']; // 统计支付宝
                        break;
                    case '现金':
                        if ($v['salespay_money_type'] == 1001)
                            $data['listdata'][$k]['cash'] = $v['salespay_pay_amount'];
                        elseif ($v['salespay_money_type'] == 1002)
                            $data['listdata'][$k]['cash'] = $v['salespay_pay_amount'];
                        $total['cash'] += $data['listdata'][$k]['cash']; // 统计现金
                        break;
                    case '汇款':
                        foreach ($my_bank_account_arr as $k2 => $v2) {
                            switch ($k2) {
                                case '1001':
                                    $data['listdata'][$k]['ICBC'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['ICBC'] += $data['listdata'][$k]['ICBC'];
                                    break;
                                case '1002':
                                    $data['listdata'][$k]['BCM'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['BCM'] += $data['listdata'][$k]['BCM'];
                                    break;
                                case '1003':
                                    $data['listdata'][$k]['SPDB'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['SPDB'] += $data['listdata'][$k]['SPDB'];
                                    break;
                                case '1004':
                                    $data['listdata'][$k]['ICBC_PERSON'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['ICBC_PERSON'] += $data['listdata'][$k]['ICBC_PERSON'];
                                    break;
                                case '1005':
                                    $data['listdata'][$k]['BOC_PERSON'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['BOC_PERSON'] += $data['listdata'][$k]['BOC_PERSON'];
                                    break;
                                case '1006':
                                    $data['listdata'][$k]['CCB_PERSON'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['CCB_PERSON'] += $data['listdata'][$k]['CCB_PERSON'];
                                    break;
                                case '1007':
                                    $data['listdata'][$k]['ABC_PERSON'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['ABC_PERSON'] += $data['listdata'][$k]['ABC_PERSON'];
                                    break;
                                case '1008':
                                    $data['listdata'][$k]['CMB_PERSON'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['CMB_PERSON'] += $data['listdata'][$k]['CMB_PERSON'];
                                    break;
                                case '1009':
                                    $data['listdata'][$k]['Paypal'] = empty($v['hk_amount'][$k2]) ? '' : $v['hk_amount'][$k2];
                                    $total['Paypal'] += $data['listdata'][$k]['Paypal'];
                                    break;
                            }
                        }
                        break;
                    default:
                        break;
                }
                $data['listdata'][$k]['total'] = $data['listdata'][$k]['alipay'] + $data['listdata'][$k]['cash'] + $data['listdata'][$k]['ICBC'] + $data['listdata'][$k]['BCM'] + $data['listdata'][$k]['SPDB'] + $data['listdata'][$k]['ICBC_PERSON'] + $data['listdata'][$k]['BOC_PERSON'] + $data['listdata'][$k]['CCB_PERSON'] + $data['listdata'][$k]['ABC_PERSON'] + $data['listdata'][$k]['CMB_PERSON'] + $data['listdata'][$k]['Paypal'];
                $total['salespay_pay_amount'] += $v['salespay_pay_amount'];
                $total['sum_invoice_amount'] += $v['sum_invoice_amount'];
                $total['all'] += $data['listdata'][$k]['total'];
            }
        }

        // 用PHPexcel生产excel文件
        require_once 'Classes/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $attr_data = array(
            'salespay_sp_date' => '日期',
            'order_type' => '订单类型',
            'order_number' => '订单号',
            'salespay_subsidiary' => '所属子公司',
            'account_tyoe' => '客户类型',
            'account_name' => '客户名称',
            'partner_name' => '经销商名称',
            'goods_name' => '购买商品',
            'owner_name' => '所属销售',
            'department_name' => '所属部门',
            'salespay_money_type_name' => '款项类型名称',
            'salespay_pay_amount' => '款项金额',
//			'sum_invoice_amount' => '已开票金额',
            'alipay' => '支付宝',
            'cash' => '现金',
            'ICBC' => '工商银行（基本户）',
            'BCM' => '交通银行（一般户）',
            'SPDB' => '上海银行（一般户）',
            'ICBC_PERSON' => '工行（个人）',
            'BOC_PERSON' => '中行（个人）',
            'CCB_PERSON' => '建行（个人）',
            'ABC_PERSON' => '农行（个人）',
            'CMB_PERSON' => '招行（个人）',
            'Paypal' => 'Paypal（贝宝）',
            'total' => '合计'
        );

        // 标题
        $i = 0;
        foreach ($attr_data as $k => $v) {
            $i++;
            $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[$i] . '1', $v);
        }


        // 下面类容 循环
        for ($i = 1; $i <= count($data['listdata']); $i++) {
            $ii = 0;
            foreach ($attr_data as $k => $v) {
                $ii++;
                $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[$ii] . ($i + 1), $data['listdata'][$i - 1][$k]);
            }
        }

        // 统计
        $i = count($data['listdata']);
        $j = 11;
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[1] . ($i + 2), '统计');
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[$j] . ($i + 2), $total['salespay_pay_amount']);
//		$objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['sum_invoice_amount']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['alipay']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['cash']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['ICBC']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['BCM']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['SPDB']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['ICBC_PERSON']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['BOC_PERSON']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['CCB_PERSON']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['ABC_PERSON']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['CMB_PERSON']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['Paypal']);
        $objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[++$j] . ($i + 2), $total['all']);

        // Set document security
        $objPHPExcel->getSecurity()->setLockWindows(true);
        $objPHPExcel->getSecurity()->setLockStructure(true);
        $objPHPExcel->getSecurity()->setWorkbookPassword("PHPExcel");

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $lee_excel_url = "upload/execl_11_" . date('YmdHis') . ".xls";
        $objWriter->save($lee_excel_url);
        echo base_url($lee_excel_url);
    }

    private function discon($data_auth) {
        if (array_diff(array('where_rel', 'where'), array_keys($data_auth)) or $data_auth['where'] == '\'\'')
            return '';

        $where_arr = json_decode($data_auth['where'], true);
        $returnWhere = '';
        foreach ($where_arr as $k => $v) {
            switch ($v['action']) {
                case 'BELONGTO';
                    $department_id = $this->db->select('department_id')->like('department_treepath', $v['value'], 'both')->get('tc_department')->result_array();
                    if ($department_id) {
                        $d_arr = array();
                        foreach ($department_id as $dv) {
                            $d_arr[] = $dv['department_id'];
                        }
                        $d_arr = trim(implode(',', $d_arr), ',');
                        $$k = 'd.department_id IN(' . $d_arr . ')';
                    }
                    break;
                default:
                    $v['attr'] = explode('.', $v['attr']);
                    $$k = 'o.order_' . $v['attr'][0] . ' = \'' . $v['value'] . '\'';
                    break;
            }
            $data_auth['where_rel'] = str_replace($k, $$k, $data_auth['where_rel']);
        }
        return '(' . $data_auth['where_rel'] . ') AND ';
    }

}

?>