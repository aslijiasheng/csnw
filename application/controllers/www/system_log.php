<?php
class System_log extends WWW_Controller{
	public $menu1='system_log';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/system_log_model','system_log');
		$this->system_log->db->query('SET NAMES UTF8');
	}

	public function index(){
		$this->system_log->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->system_log->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->system_log->attributeLabels();
		$data['listLayout']=$this->system_log->listLayout();
		$this->render('www/system_log/list',$data);
	}
	public function ajax_select(){
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["system_log_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		if (isset($_GET['type_id'])){
			if($_GET['type_id']!="" and $_GET['type_id']!=0){
				$where['type_id']=$_GET['type_id'];
			}
		}

		$data['listData']=$this->system_log->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);
		$data["labels"]=$this->system_log->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取



		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->system_log->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/system_log/ajax_select',$data);
	}
    public function del(){
       $this->system_log->del($_GET['system_log_id']);
	   success('www/system_log','删除成功');
    }
	public function add(){

		$data["labels"]=$this->system_log->attributeLabels();
		if(!empty($_POST)){
		if(isset($_GET['type_id'])){
           $_POST['system_log']['type_id']=$_GET['type_id'];
           $this->system_log->add($_POST['system_log']);
		    success('www/system_log?type_id='.$_GET['type_id'],'创建成功');
		}else{
		  $this->system_log->add($_POST['system_log']);
		  success('www/system_log','创建成功');
		}

		}
		$this->render('www/system_log/add',$data);
	}
	public function view(){
		$this->system_log->db->query('SET NAMES UTF8');
        $data["labels"]=$this->system_log->attributeLabels();
		$data['listLayout']=$this->system_log->id_aGetInfo($_GET['system_log_id']);
		$this->render('www/system_log/view',$data);
	}

    //订单日志显示
	public function detailed_list_view(){
		if(isset($_GET['order_id'])){
			//p($_GET['order_id']);exit;
			$where=array(
				'system_log_module_id'=>$_GET['order_id'],
				'system_log_module'=>$_GET['module']
			);
			//p($where);
			$data['listData']=$this->system_log->GetInfo($where);
			$data["labels"]=$this->system_log->attributeLabels();
			$data['listLayout']=$this->system_log->listLayout();
			//p($data['listData']);die;
			$this->output->enable_profiler(false);
			$this->load->view('www/system_log/detailed_list_view',$data);
		}else{
			echo "不能没有ID";
		}
    }


}
?>