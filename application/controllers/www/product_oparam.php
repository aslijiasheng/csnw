<?php
class Product_oparam extends WWW_Controller{
	public $menu1='product_oparam';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/product_oparam_model','product_oparam');
	}

	public function index(){
		$this->product_oparam->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->product_oparam->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->product_oparam->attributeLabels();
		$data['listLayout']=$this->product_oparam->listLayout();
		$this->render('www/product_oparam/list',$data);
	}
	public function add(){
		$this->product_oparam->db->query('SET NAMES UTF8');
		$data["labels"]=$this->product_oparam->attributeLabels();
		
		
		
		
		
		
		
		
		$this->load->model('admin/enum_model','enum');$data["product_oparam_type_enum"]=$this->enum->getlist(391);
		
		
		
		
		
		if(!empty($_POST)){
		if(isset($_GET['type_id'])){
           $_POST['product_oparam']['type_id']=$_GET['type_id'];
           $this->product_oparam->add($_POST['product_oparam']);
		    success('www/product_oparam?type_id='.$_GET['type_id'],'创建成功');
		}else{
		  $this->product_oparam->add($_POST['product_oparam']);
		  success('www/product_oparam','创建成功');
		}
			
		}
		$this->render('www/product_oparam/add',$data);
	}
	public function update(){
		$this->product_oparam->db->query('SET NAMES UTF8');
		$data["labels"]=$this->product_oparam->attributeLabels();
		$data['id_aData']=$this->product_oparam->id_aGetInfo($_GET['product_oparam_id']);
		$data['listLayout']=$this->product_oparam->listLayout();
		
		
		
		
		
		
		
		
		$this->load->model('admin/enum_model','enum');
		$data["product_oparam_type_enum"]=$this->enum->getlist(391);
		
		
		
		
		
		if(!empty($_POST)){
		    if(isset($_GET['type_id'])){
		      $_POST['product_oparam']['type_id']=$_GET['type_id'];
		      $this->product_oparam->update($_POST['product_oparam'],$_GET['product_oparam_id']);
			    success('www/product_oparam?type_id='.$_GET['type_id'],'更新成功');
		    }else{
              	$this->product_oparam->update($_POST['product_oparam'],$_GET['product_oparam_id']);
			    success('www/product_oparam','更新成功');
		    }
		}
		$this->render('www/product_oparam/update',$data);
	}

}
?>