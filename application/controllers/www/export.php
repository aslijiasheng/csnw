<?php

//这是专门给导入写的控制器
class Export extends WWW_Controller {

	private $cellArray = array(
		1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E',
		6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J',
		11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O',
		16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T',
		21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y',
		26 => 'Z',
		27 => 'AA', 28 => 'AB', 29 => 'AC', 30 => 'AD', 31 => 'AE',
		32 => 'AF', 33 => 'AG', 34 => 'AH', 35 => 'AI', 36 => 'AJ',
		37 => 'AK', 38 => 'AL', 39 => 'AM', 40 => 'AN', 41 => 'AO',
		42 => 'AP', 43 => 'AQ', 44 => 'AR', 45 => 'AS', 46 => 'AT',
		47 => 'AU', 48 => 'AV', 49 => 'AW', 50 => 'AX', 51 => 'AY',
		52 => 'AZ', 53 => 'BA', 54 => 'BB', 55 => 'BC', 56 => 'BD', 57 => 'BE',
		58 => 'BF', 59 => 'BG', 60 => 'BH', 61 => 'BI', 62 => 'BJ', 63 => 'BK', 64 => 'BL');

	public function __construct() {
		parent::__construct();
	}

	public function execlExport() {
		if (isset($_POST['sel_json'])) {
			$sel_data = json_decode($_POST['sel_json'], true);
			//p($listData);
			//p($sel_data['obj']);
			if ($sel_data['obj'] == "order") {
				$data['attr_data'] = array(
					'order_number' => '订单编号',
					'order_create_time' => '订单创建时间',
					'order_create_user_id' => '订单创建者id',
					'order_department' => '所属部门',
					'order_owner' => '所属销售',
					'order_account' => '所属客户',
					'order_agent' => '所属代理商',
					'order_agreement_no' => '合同编号',
					'order_amount' => '订单金额',
					'order_finance' => '财务人员',
					'order_agreement_name' => '合同名称',
					'order_review' => '业务主管',
					'order_nreview' => '内审人员',
					'order_if_renew' => '是否续费订单',
					'order_rebate_amount' => '可返点总金额',
					'order_is_cancel' => '订单是否作废',
					'order_relation_order_id' => '关联销售订单',
					'order_transfer_name' => '款项名称',
					'order_change_out' => '转出方',
					'order_change_into' => '转入方',
					'order_out_examine' => '转出方主管审核',
					'order_change_into_money' => '转入金额',
					'order_transfer_state' => '内划状态',
					'order_transfer_date' => '确认划款日期',
					//'order_PassID' => 'PassID',
					//'order_customer_type' => '客户类型',
					//'order_rmb_type' => '返款类型',
					//'order_rmb_amount' => '申请支出金额',
					//'order_rmb_handlingcharge' => '手续费承担方',
					//'order_rmb_pay_method' => '打款方式',
					//'order_rmb_bank' => '汇款银行',
					//'order_state' => '订单状态',
					//'order_account_type' => '账户类型',
					//'order_account_name' => '账户名称',
					//'order_account_number' => '银行账号',
					//'order_bank_name' => '开户银行名称',
					//'order_bank_country' => '开户银行所在城市',
					//'order_payment_number' => '支付宝账号',
					//'order_rmb_reason' => '返款原因',
					'order_rmb_person' => '选择审核人员',
					'order_rmb_user' => '经手人',
					'order_rmb_finance_id' => '选择审核财务',
					'order_prepay_company' => '公司名称',
					'order_sale_source' => '销售项目',
					'order_transfer_reason' => '划款原因',
					'order_rebates_type' => '返点类型',
					'order_rebates_state' => '返点订单状态',
					'order_rebates_remark' => '返点备注',
					'order_typein' => '录入人'
				);
			}
			$this->load->view('www/export/execlExport', $data);
		} else {
			echo "参数不对";
		}
	}

	public function execlDownload() {
//		error_log(print_r(array('Date'=>date('Y-m-d H:i:s'),$_POST),true)."\n",3,'./yuancheng'.date("Ymd_h")."log.log");
//		p($_POST);exit;
//		$_POST['sel_json'] = '{"where_all":{"w1":{"attr":"owner.id","value":"1883","action":"="},"w2":{"attr":"department.id","value":"0","action":"="},"ga1":{"attr":"type_id","value":"6","action":"="},"sa1":{"attr":"number","value":"so201405190019","action":"like"}},"rel_all":"(w1 or w2) and (ga1) and (sa1)","page":"1","perNumber":"20","obj":"order"}';
//		$_POST['form'] = '1';
		if (isset($_POST['sel_json']) and isset($_POST['form'])) {
			//先处理选中的attr
			$attr_data = array();
			foreach ($_POST['form'] as $k => $v) {
				$attr_data[$v['name']] = $v['value'];
			}

			$sel_data = json_decode($_POST['sel_json'], true);
			//根据查询条件获取相关信息
			$sel_data['page'] = 1;
			$sel_data['perNumber'] = 1000;
			$this->load->model('www/api_model', 'api');
			$listData = $this->api->select($sel_data);
			if (isset($listData['res'])) {
				echo "查询失败！" . $listData['msg'];
				exit;
			}
			$data['listdata'] = $listData['info'];

			$obj_order = $this->api->objDetail('order');

			foreach ($data['listdata'] as $k => $v) {
				foreach ($obj_order['obj_attr'] as $k2 => $v2) {
					if (isset($v[$obj_order['obj'] . '_' . $k2])) {
//						die($v2);
						switch ($v2) {
							case 'enum':
								$data['listdata'][$k][$obj_order['obj'] . '_' . $k2] = $v[$obj_order['obj'] . '_' . $k2 . '_arr']['enum_name'];
								break;
							case 'integer':
								break;
							default :
								$data['listdata'][$k][$obj_order['obj'] . '_' . $k2] = $v[$obj_order['obj'] . '_' . $k2 . '_arr'][$v2 . '_name'];
								break;
						}
					}
				}
			}

//用PHPexcel生产excel文件
			require_once 'Classes/PHPExcel.php';
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
//标题
			$i = 0;
			foreach ($attr_data as $k => $v) {
				$i++;
				$objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[$i] . '1', $v);
			}


//下面类容 循环

			for ($i = 1; $i <= count($data['listdata']); $i++) {
				$ii = 0;
				foreach ($attr_data as $k => $v) {
					$ii++;
					$objPHPExcel->getActiveSheet()->setCellValue($this->cellArray[$ii] . ($i + 1), $data['listdata'][$i - 1][$k]);
				}
			}

// Set document security
			$objPHPExcel->getSecurity()->setLockWindows(true);
			$objPHPExcel->getSecurity()->setLockStructure(true);
			$objPHPExcel->getSecurity()->setWorkbookPassword("PHPExcel");

// Save Excel 2007 file
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$lee_excel_url = "upload/execl_" . $sel_data['obj'] . "_" . date('YmdHis') . ".xls";
			$objWriter->save($lee_excel_url);
			echo base_url($lee_excel_url);
		}
	}

}

?>