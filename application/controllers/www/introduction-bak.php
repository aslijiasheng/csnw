<?php

//这是专门给导入写的控制器
class introduction extends WWW_Controller {

	public $menu1 = 'introduction';

	public function __construct() {
		parent::__construct();
		$this->load->model('www/api_model', 'api');
		$this->db->query('SET NAMES UTF8');
	}

	public function get_extension($file) {
		return substr(strrchr($file, '.'), 1);
	}

	//用于转化时间类型格式
	public function GetData($val) {
		$jd = GregorianToJD(1, 1, 1970);
		$gregorian = JDToGregorian($jd + intval($val) - 25569);
		return $gregorian; /*		 * 显示格式为 “月/日/年” */
	}

	//转化数组的编码格式
	public function array_iconv($in_charset, $out_charset, $arr) {
		return eval('return ' . iconv($in_charset, $out_charset, var_export($arr, true) . ';'));
	}

	//转化数组的格式，比如前面加*号的 去掉*
	public function array_replace_format($arr) {
		//去掉所有的*
		$json = json_encode($arr);
		$json = str_replace('"*', '"', $json);
		//exit;
		$arr = json_decode($json);
		return $arr;
	}

	//几个导入先做成死的吧！整理一个数组用来设置入款项的字段名
	public function obj_attr($obj_name) {
		switch ($obj_name) {
			case 'books':
				return array(
					'company.name' => '公司',
					'bank_account.name' => '账户',
					'adate' => '实际到账/出账日期',
					// 'bdate' => '确认到账/出账日期',
					'trade_no' => '交易号',
					'attn_id.name' => '经办人-名称',
					'memo' => '备注',
					'customer_info' => '客户信息',
					'sale_no' => '销售记录号',
					//'invoice_track.name' => '发票追踪',
					'rate' => '汇率',
					'rmb' => '折合RMB',
					'state' => '状态',
					'Note' => '摘要',
					'debit_amount' => '借方发生额',
					'Credit_amount' => '贷方发生额',
					'cashier.name' => '出纳',
					'accounting.name' => '会计',
					'sales_no' => '销售号',
					'other_memo' => '其他备注',
				);
				break;
			case 'order_sale':
				return array(
					'order' => array(
						'account.account_shopexid' => '客户商业ID',
						'account.name' => '客户名称',
						'amount' => '金额',
						'owner.gonghao' => '所属销售工号',
						'review.gonghao' => "业务主管工号",
					),
					'order_d' => array(
						'goods.code' => '商品编号',
						'goods_amount' => '金额',
						'goods_price.code' => '价格表编号',
					),
					'salespay' => array(
						'sp_date' => '确认到帐日期',
						'sp_real_date' => '实际到帐日期',
						'pay_note' => '入账备注',
						'pay_amount' => '金额',
						'pay_method.name' => '支付方式',
						'my_alipay_account.name' => '商派支付宝账号',
						'account_alipay_account' => '客户支付宝账号',
						'my_bank_account.name' => '商派银行账户类型',
						'account_bank_account' => '客户银行账号',
						'alipay_order' => '交易号',
						'book_id.number' => '日记账编号',
					),
				);
				break;
			case 'order_online':
				return array(
					'order' => array(
						'number' => '订单编号',
						'account.account_shopexid' => '客户商业ID',
						'account.name' => '客户名称',
						'amount' => '金额',
						'owner.gonghao' => '所属销售工号',
						'review.gonghao' => "业务主管工号",
					),
					'order_d' => array(
						'goods.code' => '商品编号',
						'goods_amount' => '金额',
						'goods_price.code' => '价格表编号',
					),
					'salespay' => array(
						'sp_real_date' => '实际到帐日期',
						'pay_note' => '备注',
						'pay_amount' => '金额',
						'pay_method.name' => '支付方式',
						'my_alipay_account.name' => '商派支付宝账号',
						'account_alipay_account' => '客户支付宝账号',
						'alipay_order' => '交易号',
					),
				);
				break;
			default:
				echo "没有定义这个对象的导入属性";
		}
	}

	// 修正 getcsv
	public function fgetcsv_reg(& $handle, $length = null, $d = ',', $e = '"') {
		$d = preg_quote($d);
		$e = preg_quote($e);
		$_line = "";
		$eof = false;
		while ($eof != true) {
			$_line .= (empty($length) ? fgets($handle) : fgets($handle, $length));
			$itemcnt = preg_match_all('/' . $e . '/', $_line, $dummy);
			if ($itemcnt % 2 == 0)
				$eof = true;
		}
		$_csv_line = preg_replace('/(?: |[ ])?$/', $d, trim($_line));
		$_csv_pattern = '/(' . $e . '[^' . $e . ']*(?:' . $e . $e . '[^' . $e . ']*)*' . $e . '|[^' . $d . ']*)' . $d . '/';
		preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
		$_csv_data = $_csv_matches[1];
		for ($_csv_i = 0; $_csv_i < count($_csv_data); $_csv_i++) {
			$_csv_data[$_csv_i] = preg_replace('/^' . $e . '(.*)' . $e . '$/s', '$1', $_csv_data[$_csv_i]);
			$_csv_data[$_csv_i] = str_replace($e . $e, $e, $_csv_data[$_csv_i]);
		}
		return empty($_line) ? false : $_csv_data;
	}

	//对应字段属性页面
	public function execlFile() {
		$data = "";
		//p($_GET);exit;
		if (isset($_GET['obj'])) {
			$data['obj'] = $this->obj_attr($_GET['obj']);
			$extension = $this->get_extension($_FILES["file"]["name"]); //获取后缀名
			switch ($extension) {
				case 'csv': //文件格式为csv直接读取
//					setlocale( LC_ALL, array ( 'zh_CN.gbk' , 'zh_CN.gb2312' , 'zh_CN.gb18030' ) ) ;
					$file = fopen($_FILES["file"]["tmp_name"], 'rb');
//					$file=iconv('utf-8','gb2312',$file);
					while ($execldata = $this->fgetcsv_reg($file)) { //每次读取CSV里面的一行内容
						echo $execldata;
						$execldata_all[] = $execldata;
					}
					$data['execl_data'] = $this->array_iconv('gbk', 'utf-8', $execldata_all);
					break;
				case 'xlsx':
				case 'xls': //这两种格式用PHPExcel来读取
					//echo "xls和xlsx格式暂时不支持，请转成csv格式导入";
					//exit;
					require_once 'Classes/PHPExcel.php';
					require_once('classes/PHPExcel/IOFactory.php');
					//$reader = PHPExcel_IOFactory::createReader('Excel2007');
					$filePath = $_FILES["file"]["tmp_name"];
					//$filePath = 'E:\wamp\www\leegii\upload\111.xlsx';
					$PHPExcel = new PHPExcel();
					//echo 111;
					/*					 * 默认用excel2007读取excel，若格式不对，则用之前的版本进行读取 */
					$PHPReader = new PHPExcel_Reader_Excel5();
					//为了可以读取所有版本Excel文件
					if (!$PHPReader->canRead($filePath)) {
						$PHPReader = new PHPExcel_Reader_Excel5();
						if (!$PHPReader->canRead($filePath)) {
							echo 'Excel文件格式不对！暂时只支持xls和csv格式';
							return;
						}
					}
					//echo 222;
					$PHPExcel = $PHPReader->load($filePath);
					/*					 * 读取excel文件中的第一个工作表 */
					$currentSheet = $PHPExcel->getSheet(0);
					/*					 * 取得最大的列号 */
					$allColumn = $currentSheet->getHighestColumn();
					/*					 * 取得一共有多少行 */
					$allRow = $currentSheet->getHighestRow();
					/*					 * 从第二行开始输出，因为excel表中第一行为列名 */
					/*
					  for($currentRow = 1;$currentRow <= $allRow;$currentRow++){
					  //从第A列开始输出
					  for($currentColumn= 'A';$currentColumn<= $allColumn; $currentColumn++){
					  $val = $currentSheet->getCellByColumnAndRow(ord($currentColumn) - 65,$currentRow)->getValue();//ord()将字符转为十进制数
					  $arr[$currentRow][$currentColumn]=$val;
					  }
					  }
					  p($arr);
					 */
					$all = array();
					for ($currentRow = 1; $currentRow <= $allRow; $currentRow++) {

						$flag = 0;
						$col = array();
						for ($currentColumn = 'A'; $currentColumn <= $allColumn; $currentColumn++) {

							$address = $currentColumn . $currentRow;

							$string = $currentSheet->getCell($address)->getValue();
							$col[$flag] = $string;
							$flag++;
						}
						//p($col);
						$all[] = $col;
					}
					//p($all);
					$data['execl_data'] = $all;
					break;
				default:
					echo "不是execl的正常格式";
			}
			//p($data);
			if ($_GET['obj'] == 'order_sale') {
				$this->render('www/introduction/execlFile_order_sale', $data);
			} elseif ($_GET['obj'] == 'order_online') {
				$this->render('www/introduction/execlFile_order_online', $data);
			} else {
				$this->render('www/introduction/execlFile', $data);
			}
		} else {
			echo "参数错误";
		}
	}

	//确认导入
	public function confirm_introduction() {
		//p($_POST);
		//解析成数组
		$file_arr = $_POST['file'];
		$json_arr = json_decode($_POST['execl_data']);
		//循环的之前去掉第一条数据，因为第一条数据是属性名
		array_shift($json_arr);
		//p($json_arr);
		//p($_POST['file']);
		$r_teue = 0;
		$r_false = 0;
		foreach ($json_arr as $jk => $jv) {
			foreach ($file_arr as $fk => $fv) {
				if ($fv != "") {
					$params[$fk] = $jv[$fv];
				}
			}
			///p($params);exit;
			//调用接口导入
			
			$result = $this->api->api_add($params, 'books', '');
			//p($result);
			$result = json_decode($result, true);
			//p($result);
			if ($result['res'] == 'succ') {
				$r_teue++;
			} else {
				$r_false++;
				//p($params);
				//p($result);
			}
			//p($params);
			//p($result);
			//exit;
		}
		//exit;
		echo "成功了" . $r_teue . "条，失败了" . $r_false . "条！";
	}

	//认账导入得单独处理
	public function confirm_introduction_order_sale() {
		$this->load->model('www/books_model', 'books');
		//p($_POST);
		//解析成数组
		$file_arr = $_POST['file'];
		$json_arr = json_decode($_POST['execl_data'], true);
		//循环的之前去掉第一条数据，因为第一条数据是属性名
		array_shift($json_arr);
//		p($json_arr);
		//p($_POST['file']);
		//exit;
		$r_teue = 0;
		$r_false = 0;
		$r_error = "失败信息：\n";
		//这里需要将主表、产品明细、入款项的内容分开
		foreach ($json_arr as $jk => $jv) {
			//先循环主表
			$params = array();
			foreach ($file_arr['order'] as $ok => $ov) {
				if ($ov != "") {
					//echo $jv[$ov]."|".$ok."/n";
					$params[$ok] = $jv[$ov];
				}
				$params['create_time'] = date('Y-m-d H:i:s', time()); //创建时间
			}
			//再循环产品明细
			foreach ($file_arr['order_d'] as $odk => $odv) {
				if ($odv != "") {
					$params['detailed']['order_d'][0][$odk] = $jv[$odv];
				}
			}
			//再循环入款项
			foreach ($file_arr['salespay'] as $sk => $sv) {
				if ($sv != "") {
					$params['detailed']['salespay'][0][$sk] = $jv[$sv];
				}
			}
			//额外添加一些参数
			$params['type.name'] = "销售订单";
			//p($this->session->userdata);exit;
			$params['create_user_id'] = $this->session->userdata['user_id'];
			$params['typein'] = $this->session->userdata['user_id'];
			$params['finance'] = $this->session->userdata['user_id'];
			$params['detailed']['salespay'][0]['money_type.name'] = '入账记录'; //款项类型 (入账记录/出账记录)
			$params['detailed']['salespay'][0]['status.name'] = '未确认到帐'; //到帐状态
			$params['detailed']['salespay'][0]['create_time'] = date('Y-m-d H:i:s', time()); //创建时间
			//认账导入不需要同步crm，如果没有客户信息直接在数据库增加记录 2014-06-27
			if (!empty($params['account.name'])) {
				$this->load->model('www/account_model', 'account');
				$where = array('account_name' => $params['account.name']);
				$res_a = $this->account->GetInfo($where);
				if (!$res_a) { //没有客户直接创建客户
					$data_account = array(
						'account_name' => $params['account.name'],
						'account_create_time' => date("Y-m-d H:i:s", time()),
						'type_id' => 1 //个人客户
					);
					$res_a_id = $this->account->add($data_account);
					$this->account->update($data = array('account_pbi_id' => $res_a_id), $res_a_id);
				}
//				elseif (count($res_a) > 1) { //检查是否存在多个客户名称
//					$more_account[$jk] = $params['account.name'];
//				}
			}
			//调用接口导入
			//这里的数组都需要转换一下格式
			//检查日记账编号是否存在，是否已经关联订单
			if ($this->books->checkBooksNumber($params['detailed']['salespay'][0]['book_id.number'])) {
//				if (!isset($more_account[$jk])) {
					$params_data = $this->array_replace_format($params);
					$this->load->model('www/api_model', 'api');
					$result = $this->api->api_add(json_encode($params_data), 'order');
					$result = json_decode($result, true);
					if ($result['res'] == 'succ') {
						//订单创建成功后关联入账和日记账
						$this->channelSaleOrder($result['info']['order_id'], $params);
						$r_teue++;
					} else {
						$r_false++;
						$r_error .= $params['account.name'] . "数据库添加失败\n";
					}
//				} else {
//					$r_false++;
//					$r_error .= '客户名称 ' . $params['account.name'] . " 重复\n";
//				}
			} else {
				$r_false++;
				$r_error .= '日记账编号 ' . $params['detailed']['salespay'][0]['book_id.number'] . " 已经存在关联\n";
			}
		}
		echo "成功了" . $r_teue . "条，失败了" . $r_false . "条！\n";
		if ($r_false > 0)
			echo $r_error;
	}

	//认账导入得单独处理
	public function confirm_introduction_order_online() {
		$this->load->model('www/books_model', 'books');
		//解析成数组
		$file_arr = $_POST['file'];
		$json_arr = json_decode($_POST['execl_data'], true);
		//循环的之前去掉第一条数据，因为第一条数据是属性名
		array_shift($json_arr);
		$r_teue = 0;
		$r_false = 0;
		$r_error = '';
		//这里需要将主表、产品明细、入款项的内容分开
		//p($json_arr);die();
		foreach ($json_arr as $jk => $jv) {
			//先循环主表
			$params = array();
			foreach ($file_arr['order'] as $ok => $ov) {
				if ($ov != "") {
					//echo $jv[$ov]."|".$ok."/n";
					$params[$ok] = $jv[$ov];
				}
				$params['create_time'] = date('Y-m-d H:i:s', time()); //创建时间
			}
			//p($params);
			//再循环产品明细
			foreach ($file_arr['order_d'] as $odk => $odv) {
				if ($odv != "") {
					$params['detailed']['order_d'][0][$odk] = $jv[$odv];
				}
			}
			//再循环入款项
			foreach ($file_arr['salespay'] as $sk => $sv) {
				if ($sv != "") {
					$params['detailed']['salespay'][0][$sk] = $jv[$sv];
				}
				// 入账/出账时间
				if ($sk == 'sp_real_date')
					$params['detailed']['salespay'][0]['pay_date'] = $jv[$sv];
			}
			//额外添加一些参数
			$params['type.name'] = "线上订单";
			//p($this->session->userdata);exit;
			$params['create_user_id'] = $this->session->userdata['user_id'];
			$params['typein'] = $this->session->userdata['user_id'];
			$params['finance'] = $this->session->userdata['user_id'];
			$params['detailed']['salespay'][0]['money_type.name'] = '入账记录'; //款项类型 (入账记录/出账记录)
			$params['detailed']['salespay'][0]['status.name'] = '未确认到帐'; //到帐状态
			$params['detailed']['salespay'][0]['create_time'] = date('Y-m-d H:i:s', time()); //创建时间
			//订单导入不需要同步crm，如果没有客户信息直接在数据库增加记录 2014-07-03
			if (!empty($params['account.name'])) {
				$this->load->model('www/account_model', 'account');
				$where = array('account_name' => $params['account.name']);
				$res_a = $this->account->GetInfo($where);
				if (!$res_a) { //没有客户直接创建客户
					$data_account = array(
						'account_name' => $params['account.name'],
						'account_create_time' => date("Y-m-d H:i:s", time()),
						'type_id' => 1 //个人客户
					);
					$res_a_id = $this->account->add($data_account);
					$this->account->update($data = array('account_pbi_id' => $res_a_id), $res_a_id);
				}
//				elseif (count($res_a) > 1) { //检查是否存在多个客户名称
//					$more_account[$jk] = $params['account.name'];
//				}
			}

//			if (!isset($more_account[$jk])) {
				$params_data = $this->array_replace_format($params);
				
				//require_once(FCPATHp . '/Classes/src/PhpConsole/__autoload.php');
				//PhpConsole\Helper::register();
				//PC::debug($params);

				$this->load->model('www/api_model', 'api');
				$result = $this->api->api_add(json_encode($params_data), 'order');
				$result = json_decode($result, true);
				if ($result['res'] == 'succ') {
					//订单创建成功后关联入账和日记账
					$r_teue++;
				} else {
					$r_false++;
					$r_error .= $params['account.name'] . "数据库添加失败\n";
				}
//			} else {
//				$r_false++;
//				$r_error .= '客户名称 ' . $params['account.name'] . " 重复\n";
//			}
		}
		echo "成功了" . $r_teue . "条，失败了" . $r_false . "条！\n";
		if ($r_false > 0)
			echo $r_error;
	}

//批量开票
	public function BulkBilling() {
		//定义一下状态
		$state = array(
			1001 => '未开票',
			1002 => '已开票',
			1003 => '未找到',
		);
		$data['execl_data'] = $this->execl_analytical($_FILES);
		//循环execl里的内容，查询出这些订单是否存在
		$flag = 1;
		$invoice_num = 0;
		$invoice_amount = 0; //总金额
		$order_info = array();

		$this->load->model('www/order_model', 'order');
		$this->order->db->query('SET NAMES UTF8');
		foreach ($data['execl_data'] as $k => $v) {
			$data['execl_data'][$k]['number'] = $v[0];
			$count = $this->order->countGetInfo(array('order_number' => $v[0]));
			if ($count == 0) {
				$flag = 0;
				$data['execl_data'][$k]['state'] = 1003;
			} else {
				//如果找到有这个订单，再判断订单是否已开票
				$sql = "select count(*) count from tc_invoice where invoice_order_id = (select order_id from `tc_order` where order_number='" . $v[0] . "')";
				$row = $this->db->query($sql)->row_array();
				if ($row['count'] > 0) {
					$data['execl_data'][$k]['state'] = 1002;
					$flag = 0;
				} else {
					$order_amount = $this->db->select('order_amount,order_id')->from('tc_order')->where('order_number', $data['execl_data'][$k]['number'])->get()->row_array();
					//$data['execl_data'][$k]['order_amount'] = $order_amount['order_amount'];
					$order_info[$k]['order_id'] = $order_amount['order_id'];

					$order_info[$k]['order_amount'] = $order_amount['order_amount'];
					$data['execl_data'][$k]['state'] = 1001;
					$invoice_num ++;
					$invoice_amount += $order_amount['order_amount'];
				}
			}

			$data['execl_data'][$k]['state_name'] = $state[$data['execl_data'][$k]['state']];
		}
		$data['flag'] = $flag;
		$data['invoice_num'] = $invoice_num;
		$data['invoice_amount'] = $invoice_amount;
		$data['order_info'] = json_encode($order_info);
		if (!empty($_POST)) {
			$data1['content'] = $_POST['invoice'];
			$order_info = json_decode($_POST['order_info']);

			$data1['content'] = json_encode($data1['content']);
			$data1['type'] = $_POST['invoice']['invoice_type'];
			$data1['status'] = 1002;



			if ($_POST['invoice']['invoice_type'] == '1001') {
				if ($_POST['invoice_type_1']['invoice_customer_type'] == '1002') {
					foreach ($_POST['customer_1'] as $key => $value) {
						$data1[$key] = $value;
					}
				} else {
					foreach ($_POST['customer_2'] as $key => $value) {
						$data1[$key] = $value;
					}
				}
			} else {
				$data1['title'] = $_POST['invoice_type_2']['invoice_title'];
			}


			$data1['order_id'] = $_POST['invoice']['invoice_order_id'];
			if (isset($_POST['invoice_type_1'])) {
				$data1['customer_type'] = $_POST['invoice_type_1']['invoice_customer_type'];
			}
			$data1['do_time'] = $_POST['invoice']['invoice_do_time'];
			$data1['addtime'] = date("Y-m-d H:i:s", time());
			unset($data1['execl_data']);
			unset($data1['flag']);
			unset($data1['num']);

			//细节处理
			$data1['title'] = $data1['invoice_title'];
			$data1['taxpayer'] = $data1['invoice_taxpayer'];
			$data1['code'] = $data1['invoice_code'];
			$data1['invoice_no'] = $data1['invoice_invoice_no'];
			$data1['bank'] = $data1['invoice_bank'];
			$data1['tel'] = $data1['invoice_tel'];
			$data1['address'] = $data1['invoice_address'];
			$data1['memo'] = $data1['invoice_memo'];
			unset($data1['invoice_title']);
			unset($data1['invoice_taxpayer']);
			unset($data1['invoice_code']);
			unset($data1['invoice_invoice_no']);
			unset($data1['invoice_bank']);
			unset($data1['invoice_tel']);
			unset($data1['invoice_address']);
			unset($data1['invoice_memo']);

			foreach ($order_info as $key => $value) {
				$data1['order_id'] = $value->order_id;
				$data1['amount'] = $value->order_amount;
				$data1['kp_type'] = 1003;
				//p($data1);die;
				$this->load->model('www/invoice_model', 'invoice');
				$this->invoice->api_add($data1);
			}
			// $log_data = array(
			// 	'system_log_addtime'=>date('Y-m-d H:i:s',time()),
			// 	'system_log_user_id'=>$this->session->userdata('user_id'),
			// 	'system_log_module'=>'Order',
			// 	'system_log_module_id'=>$value->order_id
			// );
			// $log_data['system_log_operation'] = '财务批量开票';
			// $this->system_log->add($log_data);
			// $this->output->enable_profiler(TRUE);
			redirect('www/order/?type_id=6');
		}
		$this->render('www/introduction/BulkBilling', $data);
	}

	//这里专门写execl解析成数组的方法
	public function execl_analytical($files) {
		$extension = $this->get_extension($files["file"]["name"]); //获取后缀名
		switch ($extension) {
			case 'csv': //文件格式为csv直接读取
				$file = fopen($files["file"]["tmp_name"], 'r');
				while ($execldata = fgetcsv($file)) { //每次读取CSV里面的一行内容
					$execldata_all[] = $execldata;
				}
				$execl_data = $this->array_iconv('gbk', 'utf-8', $execldata_all);
				break;
			case 'xlsx':
			case 'xls': //这两种格式用PHPExcel来读取
				echo "xls和xlsx格式暂时不支持，请转成csv格式导入";
				exit;
				break;
			default:
				echo "不是execl的正常格式";
		}
		return $execl_data;
	}

	//认账导入成功，执行操作
	private function channelSaleOrder($order_id = '', $channel_data) {
		if ($order_id) {
			$this->load->model('www/system_log_model', 'system_log');
			$this->load->model('www/order_model', 'order');
			$this->load->model('www/books_model', 'books');
			$this->load->model('www/salespay_model', 'salespay');
			$this->load->model('www/message_model', 'message');

			$res_o = $this->order->id_aGetInfo($order_id);
			$res_s = $this->salespay->GetInfo(array('salespay_order_id' => $order_id));

			//salespay操作
			$time = $channel_data['detailed']['salespay'][0]['sp_date'];
			$salespay_data = array(
				'salespay_pay_date' => $time,
				'salespay_sp_real_date' => $time,
				'salespay_status' => 1002
			);
			$this->salespay->update($salespay_data, $res_s['0']['salespay_id']);

			//books操作
			$books_data = array(
				'books_department_id' => $res_o['order_department'],
				'books_order_no' => $res_o['order_number'],
				'books_trade_no' => $res_s['0']['salespay_alipay_order'],
				'books_bdate' => $time
			);
			$this->books->update($books_data, $res_s['0']['salespay_book_id']);

			//增加日志
			$log_data = array(
				'system_log_addtime' => date('Y-m-d H:i:s', time()),
				'system_log_user_id' => $this->session->userdata('user_id'),
				'system_log_operation' => '执行认账导入',
				'system_log_note' => '认账导入关联日记账',
				'system_log_module' => 'Order',
				'system_log_module_id' => $order_id
			);
			$this->system_log->add($log_data);

			//删除待办，出入账待办
			$res_m = $this->message->GetInfo($where = array('message_module_id' => $res_s[0]['salespay_id'], 'message_owner' => $this->session->userdata('user_id')));
			$this->message->del($res_m[0]['message_id']);
		}
	}

}

?>