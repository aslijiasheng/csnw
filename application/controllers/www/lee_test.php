<?php
//这是专门给导入写的控制器
class lee_test extends WWW_Controller{
	public $menu1='lee_ui';

	public function __construct(){
		parent::__construct();
		$this->db->query('SET NAMES UTF8');
		include_once("lib/snoopy.inc.php");
	}
	
	//初始化所有订单到CRM
	public function lee_crm_initialization(){
		//查询出所有没有同步过的订单
		$sql = "SELECT order_id FROM tc_order where order_is_crm is null or order_is_crm = '1001'";
		$res = $this->db->query($sql)->result_array();
		foreach($res as $v){
			echo $v['order_id'];
			$this->load->model('www/api_model','api');
			p($this->api->order_crm_add($v['order_id']));
		}
	}

	public function cs01(){
		$this->render('www/test/1');
	}

	public function lee_ui(){
		$this->render('www/lee_test/lee_ui');
	}

	//cus lee 2014-06-27 U8凭证定时任务
	public function u8_timing(){
		//首先获取当前时间的前一天
		$start_date = date('Y-m-d',time()-24*60*60)." 00:00:00";
		p($start_date);
		
	}

	public function lee_api_getCustomerEmail(){
		$shopex_id = '661401560283';
		require_once('application/libraries/snoopy.php');
		require_once('application/libraries/oauth.php');
		$config = array(
			'prism' => array(
				'key' => 'AQAFY1',
				'secret' => '3JD5ZICVHKMAT5EOAY6A',
				'site' => 'https://openapi.ishopex.cn',
				'oauth' => 'https://oauth.ishopex.cn',
			),
		);
		//关于新用户中心调用查看的接口方法
		//测试环境可能有时间差！所以通过网络获取
		$snoopy = new Snoopy();
		$snoopy->submit("http://openapi.ishopex.cn/api/platform/timestamp", "");
		$time = $snoopy->results;
		//$time = time();
		$url = 'api/usercenter/passport/getinfo';
		//echo date("Y-m-d H:i:s",$time);
		//$data["uid"] = '441111082500';
		$data["eid"] = $shopex_id;
		//$data["login_name"] = "564277291@qq.com22";
		$prism = new oauth2($config['prism']);
		$r = $prism->request()->post($url, $data, $time);
		$res = $r->parsed();
		//echo 111;
		print_r($res);exit;
		if ($res["status"] == "success") {
			//成功
			return $res["data"]['email'];
		} elseif ($res["status"] == "error") {
			//echo "<pre>";print_r($res);exit;
			switch ($res["code"]) {
				case "0011":
					return "没有匹配到账号";
					break;
				case "0001":
					return "参数不能为空";
					break;
				default:
					return $res["code"];
			}
		}
		//return $res;
	}
	
	//支付方式与科目编号对照表
	/*
	 * $pay_method 支付方式
	 * $my_bank_account 商派银行账号
	 * $my_alipay_account 商派支付宝账号
	 */
	public function get_account_code($id){
		//汇款		1001
		//支付宝 	1002
		//支票		1003
		//现金		1004
		switch ($id){
			//汇款
			case '1001_1001':
			  return '100201';
			  break;  
			case '1001_1001': //汇款/工商银行（基本户） 	1001
			  return '100201';
			  break;
			case '1001_1002': //汇款/交通银行（一般户） 	1002
			  return '100207';
			  break;
			case '1001_1003': //汇款/上海银行（一般户） 	1003
			  return '100209';
			  break;
			case '1001_1004': //汇款/工行（个人） 	1004
			  return '100190';
			  break;
			case '1001_1005': //汇款/中行（个人） 	1005
			  return '100194';
			  break;
			case '1001_1006': //汇款/建行（个人） 	1006
			  return '100192';
			  break;
			case '1001_1007': //汇款/农行（个人） 	1007
			  return '100193';
			  break;
			case '1001_1008': //汇款/招行（个人） 	1008
			  return '100191';
			  break;
			case '1001_1009': //汇款/Paypal（贝宝） 	1009
			  return '100199';
			  break;
			//支付宝
			case '1002_1001': //payment@shopex.cn 	1001
			  return '100102';
			  break;
			case '1002_1002': //alipay_f@shopex.cn 	1002
			  return '100103';
			  break;
			case '1002_1003': //alipay_ec@shopex.cn 	1003
			  return '100112';
			  break;
			case '1002_1004': //wanggou@shopex.cn 	1004
			  return '100104';
			  break;
			case '1002_1005': //tp_taobao@shopex.cn 	1005
			  return '100105';
			  break;
			case '1002_1006': //shangpai@shopex.cn 	1006
			  return '100106';
			  break;
			case '1002_1007': //weishangye@shopex.cn 	1007
			  return '100107';
			  break;
			case '1002_1008': //b2btaobao@shopex.cn 	1008
			  return '100108';
			  break;
			case '1002_1009': //sms_alipay@shopex.cn 	1009
			  return '100109';
			  break;
			case '1002_1010': //wdwd_alipay@shopex.cn 	1010
			  return '100110';
			  break;
			//支票
			case '1003':
			  return '100201';
			  break;
			//现金
			case '1004':
			  return '100101';
			  break;
			default:
			  return null;
		}
	}

	
	public $get_account_code = array(
		//汇款		1001
		//支付宝 	1002
		//支票		1003
		//现金		1004
		//汇款
		'1001_1001'=>'100201', //汇款/工商银行（基本户） 	1001
		'1001_1002'=>'100207', //汇款/交通银行（一般户） 	1002
		'1001_1003'=>'100209', //汇款/上海银行（一般户） 	1003
		'1001_1004'=>'100190', //汇款/工行（个人） 	1004
		'1001_1005'=>'100194', //汇款/中行（个人） 	1005
		'1001_1006'=>'100192', //汇款/建行（个人） 	1006
		'1001_1007'=>'100193', //汇款/农行（个人） 	1007
		'1001_1008'=>'100191', //汇款/招行（个人） 	1008
		'1001_1009'=>'100199', //汇款/Paypal（贝宝） 	1009
		//支付宝
		'1002_1001'=>'100102', //payment@shopex.cn 	1001
		'1002_1002'=>'100103', //alipay_f@shopex.cn 	1002
		'1002_1003'=>'100112', //alipay_ec@shopex.cn 	1003
		'1002_1004'=>'100104', //wanggou@shopex.cn 	1004
		'1002_1005'=>'100105', //tp_taobao@shopex.cn 	1005
		'1002_1006'=>'100106', //shangpai@shopex.cn 	1006
		'1002_1007'=>'100107', //weishangye@shopex.cn 	1007
		'1002_1008'=>'100108', //b2btaobao@shopex.cn 	1008
		'1002_1009'=>'100109', //sms_alipay@shopex.cn 	1009
		'1002_1010'=>'100110', //wdwd_alipay@shopex.cn 	1010
		//支票
		'1003'=>'100201',
		//现金
		'1004'=>'100101',
	);
	

	public function lee_u8(){
		echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8" />';
		if(isset($_GET['date'])){
			$start_time=$_GET['date'];
			$this->u8_pz($start_time);
		}else{
			echo "没有时间参数";
		}
		
	}

	//根据技术产品编号同步U8库存档案
	public function u8_inventory_code(){
		if(!isset($_GET['code'])){
			echo "不能没有技术产品ID";exit;
		}else{
			$product_tech_code = $_GET['code'];
		}
		$sql = "select product_tech_id,product_tech_name from tc_product_tech where product_tech_code = '".$product_tech_code."'";
		$pt_data = $this->db->query($sql)->result_array();
		$pt_data = $pt_data[0];
		//p($pt_data);
		$u8_inventory_data['code'] = '1'.sprintf("%05d",$pt_data['product_tech_id']);
		$u8_inventory_data['name'] = $pt_data['product_tech_name'];
		//p($u8_inventory_data);
		$res_inventory = $this->u8_inventory($u8_inventory_data);
		$item_arr = (array)$res_inventory['item'];
		p($item_arr);
		if($item_arr['@attributes']['dsc']=="ok"){
			p("技术产品同步成功");
		}else{
			p($product_tech_code."编号，未知问题1：".$item_arr['@attributes']['dsc']);
			//exit;
		}
	}

	//根据订单编号单独导入
	public function u8_pz_number(){
		if(!isset($_GET['date'])){
			echo "没有开始时间是不行的";exit;
		}else{
			$start_time = date("Y-m-d 00:00:00",strtotime($_GET['date']));
			$end_time = date("Y-m-d 23:59:59",strtotime($_GET['date'])); //结束时间
		}
		if(!isset($_GET['number'])){
			echo "不能没有订单编号";exit;
		}else{
			$order_number = $_GET['number'];
		}

		$sql = "
select 
o.order_id,
o.order_number,
o.order_account,
o.order_department,
o.order_agent,
o.type_id,
o.order_source,
o.order_deposit_paytype,
(select sum(i.invoice_amount) from tc_invoice i where i.invoice_order_id = o.order_id and i.invoice_do_time >= '".$start_time."' and i.invoice_do_time <= '".$end_time."' and i.invoice_status=1002) now_invoice_amount,
(select sum(i.invoice_amount) from tc_invoice i where i.invoice_order_id = o.order_id and i.invoice_do_time < '".$start_time."' and i.invoice_status=1002) before_invoice_amount,
(select sum(s.salespay_pay_amount) from tc_salespay s where s.salespay_order_id = o.order_id and s.salespay_sp_real_date >= '".$start_time."' and s.salespay_sp_real_date <= '".$end_time."' and s.salespay_status=1002 and s.salespay_money_type = 1001) now_salespay_amount,
(select sum(s.salespay_pay_amount) from tc_salespay s where s.salespay_order_id = o.order_id and s.salespay_sp_real_date < '".$start_time."' and s.salespay_status=1002 and s.salespay_money_type = 1001) before_salespay_amount
from tc_order o 
where order_number = '".$order_number."'";
		//echo $sql;
		$order_data = $this->db->query($sql)->result_array();
		//p($order_data);exit;
		//循环$order_data，生成xml
		$date = strtotime($start_time);
		foreach ($order_data as $ok=>$ov){
			$order_number = $ov['order_number'];
			$order_type_id = $ov['type_id']; //所属部门ID 
			$order_department = $ov['order_department']; //所属部门ID 
			$order_account = $ov['order_account']; //所属客户ID
			$order_agent = $ov['order_agent']; //代理商ID
			$order_source = $ov['order_source']; //订单来源
			$order_deposit_paytype = $ov['order_deposit_paytype']; //款项类型
			$now_invoice_amount = $ov['now_invoice_amount']; //当日开票金额
			$before_invoice_amount = $ov['before_invoice_amount']; //过去开票金额
			$now_salespay_amount = $ov['now_salespay_amount']; //当日入款金额
			$before_salespay_amount = $ov['before_salespay_amount']; //过去入款金额
			//p($ov);exit;
			//还需要什么？

			//这里的部门需要获取到他对应的一级部门的信息
			//$sql_dept = "select department_name,department_code,department_is_u8 from tc_department where department_treepath = (select left(department_treepath,9) from tc_department where department_id='".$order_department."')"; //不在获取一级部门，根据当前部门来获取
			if($order_department!=0 and $order_department!=""){
			$sql_dept = "select department_u8_code,department_name from tc_department where department_id='".$order_department."'";
			//p($sql_dept);//exit;
			$dept_data = $this->db->query($sql_dept)->result_array();
			$dept_data = $dept_data[0];
			//p($dept_data);exit;
			$department_name = $dept_data['department_name']; //部门名称
			$department_code = $dept_data['department_u8_code']; //部门编号
			if($department_code==""){
				echo "部门【".$department_name."】对应u8编码不能为空！";exit;
			}
			//$department_is_u8 = $dept_data['department_is_u8']; //是否同步U8
			//p($dept_data);
			/*
			if($department_is_u8!=1002){
				$res_dept = $this->u8_dept($department_name,$department_code); //同步U8
				//p($res_dept);
				$item_arr = (array)$res_dept['item'];
				if($item_arr['@attributes']['dsc']=="ok"){
					//表示同步完成
					$upsql_dept = "update tc_department set department_is_u8='1002' where department_id='".$order_department."'";
					$this->db->query($upsql_dept);
					//echo "提交完毕";
				}
			}
			*/
			}else{
				echo $order_number."部门为空";exit;
			}

			
			//查询出客户信息，用于同步U8客户信息
			if(isset($order_account)){
				$sql_account = "select account_id,account_name,type_id,account_account_shopexid,account_is_u8,account_pbi_id,account_name_u8 from tc_account where account_id = '".$order_account."'";
				$account_data = $this->db->query($sql_account)->result_array();
				
				$account_data = $account_data[0];
				$account_id = $account_data['account_id']; //客户ID
				$account_name = $account_data['account_name']; //客户名称
				$type_id = $account_data['type_id']; //类型ID
				$account_account_shopexid = $account_data['account_account_shopexid']; //商业ID
				$account_is_u8 = $account_data['account_is_u8']; //是否同步U8
				$account_pbi_id = $account_data['account_pbi_id']; //老营收ID
				$account_name_u8 = $account_data['account_name_u8']; //客户名称（U8）
				//组成一个用于同步的客户信息
				if($type_id==1){
					$u8_account_data['sort_code']="02";//客户分类为个人
					//如果是个人 客户名称改成 名称+ID
					if($account_account_shopexid==""){
						$u8_account_data['u8_name']=$account_name."_n".$account_id;
					}else{
						$u8_account_data['u8_name']=$account_name."_".$account_account_shopexid;
					}
				}
				if($type_id==2){
					$u8_account_data['sort_code']="01";//客户分类为企业
					$u8_account_data['u8_name']=$account_name;
				}
				//如果老营收ID为空,则使用shopexid作为U8的客户编码，否则用营收的ID
				if($account_pbi_id==""){
					$u8_account_data['u8_code']=$account_account_shopexid;
				}else{
					$u8_account_data['u8_code']=$account_pbi_id;
				}
				//如果没有类型，这种是专门导入的！所以直接用account_id来做
				if($type_id==""){
					$u8_account_data['sort_code']="02";//客户分类为个人
					//如果是个人 客户名称改成 名称+ID
					$u8_account_data['u8_name']=$account_name."_".$account_id;
					$u8_account_data['u8_code']=$account_id;
				}
				//p($u8_account_data);
				if($account_is_u8!=1002){
					//当发现是否同步ID不为是的时候
					$res_account = $this->u8_account($u8_account_data);
					//p($res_account);
					$item_arr = (array)$res_account['item'];
					if($item_arr['@attributes']['dsc']=="ok"){
						p("客户同步成功");
					}else
					if($item_arr['@attributes']['dsc']=="客户编码已经存在，不可重复！"){
						p("客户编码已存在");
						$upsql_account = "update tc_account set account_is_u8='1002' where account_id = '".$order_account."'";
						$this->db->query($upsql_account);
					}else
					if($item_arr['@attributes']['dsc']=="客户简称已经存在，不可重复！"){
						p($order_number."订单".$account_name.'客户简称已经存在，不可重复！无法同步！怎么办');
						$order_data[$ok]['account_fail']="1";
						//exit;
					}else{
						//p($sql_account);
						//p($account_pbi_id);
						//p($account_account_shopexid);
						//p($u8_account_data);
						p($order_number."订单，未知问题2：".$item_arr['@attributes']['dsc']);
						$order_data[$ok]['account_fail']="1";
						//exit;
					}
				}
			}else{
				//该订单没有客户信息的时候 看看有没有经销商信息,将经销商信息作为客户信息提交
				//查询出这个代理商的相关信息
				if(isset($order_agent)){
					$sql_agent = "select partner_id,partner_name,partner_shopex_id,partner_is_u8,partner_pbi_id from tc_partner where partner_id = '".$order_agent."'";
					//p($sql_agent);
					$agent_data = $this->db->query($sql_agent)->result_array();
					$agent_data = $agent_data[0];
					$account_id = $agent_data['partner_id']; //经销商ID
					$partner_name = $agent_data['partner_name']; //经销商名称
					$partner_shopex_id = $agent_data['partner_shopex_id']; //经销商商业ID
					$partner_is_u8 = $agent_data['partner_is_u8']; //是否同步U8
					$partner_pbi_id = $agent_data['partner_pbi_id']; //老营收ID

					//组成一个用于同步的客户信息
					$u8_account_data['sort_code']="03";//经销商
					$u8_account_data['u8_name']=$partner_name;
					//如果老营收ID为空,则使用shopexid作为U8的客户编码，否则用营收的ID
					if($partner_pbi_id==""){
						$u8_account_data['u8_code']=$partner_shopex_id;
					}else{
						$u8_account_data['u8_code']=$partner_pbi_id;
					}
					//p($u8_account_data);
					if($partner_is_u8!=1002){
						//当发现是否同步ID不为是的时候
						$res_account = $this->u8_account($u8_account_data);
						//p($res_account);
						$item_arr = (array)$res_account['item'];
						if($item_arr['@attributes']['dsc']=="ok"){
							p("同步成功");
							$upsql_partner = "update tc_partner set partner_is_u8='1002' where partner_id = '".$order_agent."'";
							$this->db->query($upsql_partner);
						}else
						if($item_arr['@attributes']['dsc']=="客户编码已经存在，不可重复！"){
							p("客户编码已存在");
							$upsql_partner = "update tc_partner set partner_is_u8='1002' where partner_id = '".$order_agent."'";
							$this->db->query($upsql_partner);
						}else
						if($item_arr['@attributes']['dsc']=="客户简称已经存在，不可重复！"){
							p($agent_data);
							p($order_number."订单".$partner_name.'代理商简称已经存在，不可重复！无法同步！怎么办');
							$order_data[$ok]['account_fail']="1";
							//exit;
						}else{
							p("未知问题3：".$item_arr['@attributes']['dsc']);
							$order_data[$ok]['account_fail']="1";
						}
					}
				}else{
					echo "该订单既没有客户又没有代理商，怎么处理？？";exit;
				}
			}
			//exit;

			//只有在销售订单和提货销售订单下 才获取产品信息
			if($order_type_id==13 or $order_type_id==6){
				//echo 111;exit;
				//按照订单找到最高金额产品，
				$sql_pt = "select product_tech_u8_code,product_tech_id,product_tech_code,product_tech_name from tc_product_tech where product_tech_id = (select ptrelation_product_tech_id from tc_ptrelation where ptrelation_product_basic_id = (select product_basic_id from tc_product_basic where product_basic_code = (SELECT order_d_product_basic_code FROM `tc_order_d` WHERE order_d_order_id = '".$ov['order_id']."' order by order_d_product_basic_disc DESC LIMIT 1)))";
				//p($sql_pt);
				$product_tech_data = $this->db->query($sql_pt)->result_array();
				//p($product_tech_data);//exit;
				$product_tech_u8_code = "";
				if(isset($product_tech_data[0]['product_tech_u8_code'])){
					$product_tech_u8_code =  $product_tech_data[0]['product_tech_u8_code']; //技术产品（U8编号）
				}
				if($product_tech_u8_code == ""){
					$u8_inventory_data['code'] = '1'.sprintf("%05d",$product_tech_data[0]['product_tech_id']);
					$u8_inventory_data['name'] = $product_tech_data[0]['product_tech_name'];
					//p($u8_inventory_data);
					$res_inventory = $this->u8_inventory($u8_inventory_data);
					$item_arr = (array)$res_inventory['item'];
					//p($item_arr);
					if($item_arr['@attributes']['dsc']=="ok"){
						$upsql_pt = "update tc_product_tech set product_tech_u8_code = '".$u8_inventory_data['code']."' where product_tech_id = '".$product_tech_data[0]['product_tech_id']."'";
						$this->db->query($upsql_pt);
						p("技术产品同步成功");
					}else{
						p($product_tech_code."编号，未知问题4：".$item_arr['@attributes']['dsc']);
						//exit;
						$order_data[$ok]['product_fail']="1";
					}
					//p($order_number."订单,没有找到技术产品对应的u8_code");
					//$order_data[$ok]['product_fail']="1";
				}
			}

			//如果有当日开票的，需要知道当日开票信息
			if($now_invoice_amount>0){
				//好像不需要查询开票信息
				$sql_ni = "select * from tc_invoice i where i.invoice_order_id = '".$ov['order_id']."' and i.invoice_do_time >= '".$start_time."' and i.invoice_do_time <= '".$end_time."' and i.invoice_status=1002";
				$now_invoice_data = $this->db->query($sql_ni)->result_array();
				//p($now_invoice_data);
				if(isset($now_invoice_data[0]['invoice_invoice_no'])){
					$invoice_invoice_no = $now_invoice_data[0]['invoice_invoice_no'];
				}else{
					p($order_number."没有票据号码");
					exit;
				}
				//$invoice_content_arr =  json_decode($now_invoice_data[0]['invoice_content'],true);
				//p($invoice_content_arr);
				
			}
			
			//如果有当日入款的，需要知道当日入款信息
			if($now_salespay_amount>0){
				$sql_ns = "select salespay_pay_method,salespay_my_bank_account,salespay_my_alipay_account from tc_salespay s where s.salespay_order_id = '".$ov['order_id']."' and s.salespay_sp_real_date >= '".$start_time."' and s.salespay_sp_real_date <= '".$end_time."' and s.salespay_status=1002 and s.salespay_money_type = 1001";
				$now_salespay_data = $this->db->query($sql_ns)->result_array();
				$ns_data['salespay_pay_method']=$now_salespay_data[0]['salespay_pay_method'];//支付方式
				$ns_data['salespay_my_bank_account']=$now_salespay_data[0]['salespay_my_bank_account'];//商派银行账号
				$ns_data['salespay_my_alipay_account']=$now_salespay_data[0]['salespay_my_alipay_account'];//商派支付宝账号
				//根据支付方式来归类一下科目编号
				if($ns_data['salespay_pay_method']==1001){
					//支付方式为汇款
					$order_data[$ok]['now_salespay_account_code'] = $this->get_account_code["1001_".$ns_data['salespay_my_bank_account']];
				}else if($ns_data['salespay_pay_method']==1002){
					//p($order_number);
					//p($ns_data);
					//支付方式为支付宝
					$order_data[$ok]['now_salespay_account_code'] = $this->get_account_code["1002_".$ns_data['salespay_my_alipay_account']];
				}else{
					//其他支付方式
					$order_data[$ok]['now_salespay_account_code'] = $this->get_account_code[$ns_data['salespay_pay_method']];
				}
				//echo $order_data[$ok]['now_salespay_account_code'];
			}

			//如果之前有开票，需要知道之前开票的开票信息
			//如果之前有入款的，需要知道之前入款的信息
			if($before_salespay_amount>0){
				$sql_ns = "select salespay_pay_method,salespay_my_bank_account,salespay_my_alipay_account from tc_salespay s where s.salespay_order_id = '".$ov['order_id']."' and s.salespay_sp_real_date < '".$start_time."' and s.salespay_status=1002 and s.salespay_money_type = 1001";
				$now_salespay_data = $this->db->query($sql_ns)->result_array();
				$ns_data['salespay_pay_method']=$now_salespay_data[0]['salespay_pay_method'];//支付方式
				$ns_data['salespay_my_bank_account']=$now_salespay_data[0]['salespay_my_bank_account'];//商派银行账号
				$ns_data['salespay_my_alipay_account']=$now_salespay_data[0]['salespay_my_alipay_account'];//商派支付宝账号
				//根据支付方式来归类一下科目编号
				if($ns_data['salespay_pay_method']==1001){
					//支付方式为汇款
					$order_data[$ok]['before_salespay_account_code'] = $this->get_account_code["1001_".$ns_data['salespay_my_bank_account']];
				}else if($ns_data['salespay_pay_method']==1002){
					//p($ns_data);exit;
					//支付方式为支付宝
					$order_data[$ok]['before_salespay_account_code'] = $this->get_account_code["1002_".$ns_data['salespay_my_alipay_account']];
				}else{
					//其他支付方式
					$order_data[$ok]['before_salespay_account_code'] = $this->get_account_code[$ns_data['salespay_pay_method']];
				}
			}

//首先根据订单类型来判断什么凭证

//=============预收款订单 对应 收到储值款============
//其他应付款-保证金 224101
//其他应付款-云生意储值 224104
//其他应付款-云猫储值 224105
//预收账款/客户款 220302
if($order_type_id==10){ //订单类型=预收款订单
	//echo $order_data[$ok]['now_salespay_account_code'];exit;

	$the_amount=$now_salespay_amount; //本次金额为【当日收款】

	//根据不同的订单来源给予不同的科目类型
	if($order_source=='1001' and $order_deposit_paytype=='1002'){
		//云猫的预收款
		$pz_account_code='224105';
		$abstract = $ov['order_number'].",收到储值款";
	}else
	if($order_source=='1002' and $order_deposit_paytype=='1002'){
		//云生意的预收款
		$pz_account_code='224104';
		$abstract = $ov['order_number'].",收到储值款";
	}else
	if($order_source=='1003' and $order_deposit_paytype=='1003'){
		//渠道过来的货款
		$pz_account_code='220302';
		$abstract = $ov['order_number'].",收到储值款";
	}else
	if($order_deposit_paytype=='1001'){
		//保证金
		$pz_account_code='224101';
		$abstract = $ov['order_number'].",保证金收款生成";
	}else{
		echo "订单来源，未知枚举错误!";
	}
/*********款项类型order_deposit_paytype********
保证金 	1001
预收款 	1002
货款 	1003
退预收款 	1004
退保证金 	1005
*/
//p($order_data[$ok]);
	$pz_i=1;//每个凭证明细序号
	//借：银行存款/现金
	$order_data[$ok]['pz_item'][$pz_i]=array(); //借：银行存款/现金
	$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
	$order_data[$ok]['pz_item'][$pz_i]['account_code']=$order_data[$ok]['now_salespay_account_code'];//科目_编号
	$order_data[$ok]['pz_item'][$pz_i]['abstract']=$abstract; //摘要
	$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
	$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
	$order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号 银行存款/现金没有辅助项
	$order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 银行存款/现金没有辅助项
	$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
	$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 银行存款/现金没有辅助项
	$pz_i++;
	//贷：预收账款-客户款(220302)
	$order_data[$ok]['pz_item'][$pz_i]=array(); //贷：预收账款
	$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
	$order_data[$ok]['pz_item'][$pz_i]['account_code']=$pz_account_code;//科目_编号
	$order_data[$ok]['pz_item'][$pz_i]['abstract']=$abstract; //摘要
	$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
	$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$the_amount; //贷方金额
	$order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
	if($pz_account_code=='224104' or $pz_account_code=='224105'){
		$order_data[$ok]['pz_item'][$pz_i]['dept_id'] = '';
	}
	$order_data[$ok]['pz_item'][$pz_i]['cust_id']=$u8_account_data['u8_code']; //客户编码(这里用商业ID还是用什么？)
	$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
	$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品
	$pz_i++;
	//=========保证金收款生成=========
}


//==============提货销售订单 对应 开票================
if($order_type_id==13){ //订单类型=提货销售订单
	$the_amount=$now_invoice_amount; //本次金额为【当日收款】

	//根据不同的订单来源给予不同的科目类型
	if($order_source=='1001'){
		//云猫的预收款
		$pz_account_code='224105';
		$abstract = $ov['order_number'].",之前到款当日开票";
		$department_code = ''; //云猫不挂部门核算，所以不打部门
	}else
	if($order_source=='1002'){
		//云生意的预收款
		$pz_account_code='224104';
		$abstract = $ov['order_number'].",之前到款当日开票";
		$department_code = ''; //云生意不挂部门核算，所以不打部门
	}else
	if($order_source=='1003'){
		//渠道过来的货款
		$pz_account_code='220302';
		$abstract = $ov['order_number'].",".$invoice_invoice_no.",之前到款当日开票";
	}else{
		echo "提货销售订单，未知情况错误!";exit;
	}
	
	//贷：主营业务收入（6001）
	//主营业务收入=金额*0.94 //计算了税率的
	$zyywsr_amount = round($the_amount/1.06,2);
	$order_data[$ok]['pz_item'][$pz_i]=array(); //贷：主营业务收入
	$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
	$order_data[$ok]['pz_item'][$pz_i]['account_code']='6001';//科目_编号
	$order_data[$ok]['pz_item'][$pz_i]['abstract']=$abstract; //摘要
	$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
	$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$zyywsr_amount; //贷方金额
	$order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
	$order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 主营业务收入不挂客户（为空）
	$order_data[$ok]['pz_item'][$pz_i]['item_id']='1'; //项目编号
	$order_data[$ok]['pz_item'][$pz_i]['self_define10']=$product_tech_u8_code; //产品编号 ???
	$pz_i++;
	//贷：应交税费-应交增值税-销项税额(22210101)
	//销项税额=金额*0.06 //计算了税率的
	$xxsg_amount = round(($the_amount/1.06)*0.06,2); //小数点后2位数（四舍五入）
	$order_data[$ok]['pz_item'][$pz_i]=array(); //贷：应交税费-应交增值税-销项税额
	$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
	$order_data[$ok]['pz_item'][$pz_i]['account_code']='22210101';//科目_编号
	$order_data[$ok]['pz_item'][$pz_i]['abstract']=$abstract; //摘要
	$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
	$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$xxsg_amount; //贷方金额
	$order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号
	$order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码(这里用商业ID还是用什么？)
	$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
	$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品
	$pz_i++;
	//借：预收账款-客户款（220302）
	$order_data[$ok]['pz_item'][$pz_i]=array(); //借：预收账款-客户款
	$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
	$order_data[$ok]['pz_item'][$pz_i]['account_code']=$pz_account_code;//科目_编号
	$order_data[$ok]['pz_item'][$pz_i]['abstract']=$abstract; //摘要
	$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
	$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
	$order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
	if($pz_account_code=='224104' or $pz_account_code=='224105'){
		$order_data[$ok]['pz_item'][$pz_i]['dept_id'] = '';
	}
	$order_data[$ok]['pz_item'][$pz_i]['cust_id']=$u8_account_data['u8_code']; //客户编码(这里用商业ID还是用什么？)
	$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
	$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品
	$pz_i++;
}

//===================销售订单 开始分类 start =======================
if($order_type_id==6){ //订单类型=销售订单
	if(isset($now_invoice_amount)){
		$now_invoice_amount_js = $now_invoice_amount; //计算当日剩余开票
	}else{
		$now_invoice_amount_js = 0; //计算当日剩余金额
	}
	if(isset($now_salespay_amount)){
		$now_salespay_amount_js = $now_salespay_amount; //计算当日剩余金额
	}else{
		$now_salespay_amount_js = 0; //计算当日剩余金额
	}
	//--------------先抵扣以前的预收账款 start-----------------
	//当有【剩余开票】和【当日收款】，则做【之前开票当日收款】
	$pz_i=1;//每个凭证明细序号
	if($before_invoice_amount>0 and $now_salespay_amount>0){
		if ($before_invoice_amount>=$now_salespay_amount){
			//当【剩余开票】大于等于【当日收款】
			//金额使用$now_salespay_amount
			$the_amount=$now_salespay_amount; //本次金额为【当日收款】
			$now_salespay_amount_js=$now_salespay_amount_js-$now_salespay_amount;//计算当日剩余开票
		}else if($before_invoice_amount<$now_salespay_amount){
			//当【剩余开票】小于【当日收款】
			//金额用$before_invoice_amount
			$the_amount=$before_invoice_amount; //本次金额为【剩余开票】
			$now_salespay_amount_js=$now_salespay_amount_js-$before_invoice_amount;//计算当日剩余开票
		}else{
			//p();
			echo $before_invoice_amount;
			echo "--";
			echo $now_salespay_amount;
			echo "未知情况11";exit;
		}
		//借：银行存款/现金
		$order_data[$ok]['pz_item'][$pz_i]=array(); //借：银行存款/现金
		//$entry_id=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']=$order_data[$ok]['now_salespay_account_code'];//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",之前开票当日收款"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号 银行存款/现金没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 银行存款/现金没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 银行存款/现金没有辅助项
		$pz_i++;
		//贷：应收账款（1122）
		$order_data[$ok]['pz_item'][$pz_i]=array(); //贷：应收账款
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']='1122';//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",之前开票当日收款"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$the_amount; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=$u8_account_data['u8_code']; //客户编码(这里用商业ID还是用什么？) ???
		$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品 ???
		$pz_i++;
	}
	//当【剩余入款】和【当日开票】都大于0，则做【之前入款当日开票】
	if($before_salespay_amount>0 and $now_invoice_amount>0){
		if ($before_salespay_amount>=$now_invoice_amount){
			//当【剩余收款】大于等于【当日开票】
			//金额使用$now_invoice_amount
			$the_amount=$now_invoice_amount; //本次金额为【当日收款】
			$now_invoice_amount_js=$now_invoice_amount_js-$now_invoice_amount;//计算当日剩余开票
		}else if($before_invoice_amount<$now_salespay_amount){
			//当【剩余开票】小于【当日收款】
			//金额用$before_salespay_amount
			$the_amount=$before_salespay_amount; //本次金额为【剩余开票】
			$now_invoice_amount_js=$now_invoice_amount_js-$before_salespay_amount;//计算当日剩余开票
		}else{
			echo "未知情况12";exit;
		}
		//借：预收账款-客户款（220302）
		$order_data[$ok]['pz_item'][$pz_i]=array(); //借：预收账款-客户款
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']='220302';//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",之前入款当日开票"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=$u8_account_data['u8_code']; //客户编码(这里用商业ID还是用什么？)
		$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品
		$pz_i++;
		//贷：主营业务收入（6001）
		//主营业务收入=金额*0.94 //计算了税率的
		$zyywsr_amount = round($the_amount/1.06,2);
		$order_data[$ok]['pz_item'][$pz_i]=array(); //贷：主营业务收入
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']='6001';//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",".$invoice_invoice_no.",之前入款当日开票"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$zyywsr_amount; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 主营业务收入不挂客户（为空）
		$order_data[$ok]['pz_item'][$pz_i]['item_id']='1'; //项目编号
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=$product_tech_u8_code; //产品编号 ???
		$pz_i++;
		//贷：应交税费-应交增值税-销项税额(22210101)
		//销项税额=金额*0.06 //计算了税率的
		$xxsg_amount = round(($the_amount/1.06)*0.06,2); //小数点后2位数（四舍五入）
		$order_data[$ok]['pz_item'][$pz_i]=array(); //贷：应交税费-应交增值税-销项税额
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']='22210101';//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",".$invoice_invoice_no.",之前入款当日开票"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$xxsg_amount; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码(这里用商业ID还是用什么？)
		$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品
		$pz_i++;
	}
	//--------------先抵扣以前的预收账款 end-----------------


	//过去的金额都抵扣完毕，开始根据当日的款项信息来分类了
	//当【当日剩余开票】大于0 和【当日剩余汇款】大于0，则做【收款且开票】
	//echo $now_invoice_amount_js."-".$now_salespay_amount_js;
	if($now_invoice_amount_js>0 and $now_salespay_amount_js>0){
		if ($now_invoice_amount_js>=$now_salespay_amount_js){
			//当【剩余开票】大于等于【当日收款】
			//金额使用$now_salespay_amount
			$the_amount=$now_salespay_amount_js; //本次金额为【当日收款】
			$now_invoice_amount_js=$now_invoice_amount_js-$now_salespay_amount_js; //计算当日剩余开票
			$now_salespay_amount_js=0; //计算当日剩余入款
		}else if($now_invoice_amount_js<$now_salespay_amount_js){
			//当【剩余开票】小于【当日收款】
			//金额用$before_invoice_amount
			$the_amount=$now_invoice_amount_js; //本次金额为【剩余开票】
			$now_invoice_amount_js=0; //计算当日剩余开票
			$now_salespay_amount_js=$now_salespay_amout_js-$now_invoice_amount_js; //计算当日剩余入款
		}else{
			echo "未知情况13";exit;
		}

		//借：银行存款/现金
		$order_data[$ok]['pz_item'][$pz_i]=array(); //借：银行存款/现金
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']=$order_data[$ok]['now_salespay_account_code'];//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",收款且开票"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号 银行存款/现金没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 银行存款/现金没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 银行存款/现金没有辅助项
		$pz_i++;
		//贷：主营业务收入（6001）
		//主营业务收入=金额*0.94 //计算了税率的
		$zyywsr_amount = round($the_amount/1.06,2);
		$order_data[$ok]['pz_item'][$pz_i]=array(); //贷：主营业务收入
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']='6001';//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",".$invoice_invoice_no.",收款且开票"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$zyywsr_amount; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 主营业务收入不挂客户（为空）
		$order_data[$ok]['pz_item'][$pz_i]['item_id']='1'; //项目编号
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=$product_tech_u8_code; //产品编号 ???
		$pz_i++;
		//贷：应交税费-应交增值税-销项税额(22210101)
		//销项税额=金额*0.06 //计算了税率的
		$xxsg_amount = round(($the_amount/1.06)*0.06,2); //小数点后2位数（四舍五入）
		$order_data[$ok]['pz_item'][$pz_i]=array(); //贷：应交税费-应交增值税-销项税额
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']='22210101';//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",".$invoice_invoice_no.",收款且开票"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$xxsg_amount; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号 销项税额没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 销项税额没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 销项税额没有辅助项
		$pz_i++;
	}

	//-----------最后处理已收款未开票和已开票未收款------------
	if($now_invoice_amount_js>0 and $now_salespay_amount_js==0){
		//当【当日剩余开票】大于0 和【当日剩余收款】等于0，则做【开票未收款】
		$the_amount=$now_invoice_amount_js;//本次金额为【当日剩余开票】
		//借：应收账款（1122）
		$order_data[$ok]['pz_item'][$pz_i]=array(); //借：应收账款
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']='1122';//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",开票未收款"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=$u8_account_data['u8_code']; //客户编码(这里用商业ID还是用什么？)
		$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 应收账款不用挂产品
		$pz_i++;
		//贷：主营业务收入（6001）
		//主营业务收入=金额*0.94 //计算了税率的
		$zyywsr_amount = round($the_amount/1.06,2);
		$order_data[$ok]['pz_item'][$pz_i]=array(); //贷：主营业务收入
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']='6001';//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",".$invoice_invoice_no.",开票未收款"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$zyywsr_amount; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 主营业务收入不挂客户（为空）
		$order_data[$ok]['pz_item'][$pz_i]['item_id']='1'; //项目编号
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=$product_tech_u8_code; //产品编号 ???
		$pz_i++;
		//贷：应交税费-应交增值税-销项税额(22210101)
		//销项税额=金额*0.06 //计算了税率的
		$xxsg_amount = round(($the_amount/1.06)*0.06,2); //小数点后2位数（四舍五入）
		$order_data[$ok]['pz_item'][$pz_i]=array(); //贷：应交税费-应交增值税-销项税额
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']='22210101';//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",".$invoice_invoice_no.",开票未收款"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$xxsg_amount; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码
		$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号
		$pz_i++;
	}else if($now_invoice_amount_js==0 and $now_salespay_amount_js>0){
		//当【当日剩余开票】等于0 和【当日剩余收款】大于0，则做【收款未开票】
		$the_amount=$now_salespay_amount_js;//本次金额为【当日剩余收款】
		//借：银行存款/现金
		$order_data[$ok]['pz_item'][$pz_i]=array(); //借：银行存款/现金
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']=$order_data[$ok]['now_salespay_account_code'];//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",收款未开票"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码
		$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号
		$pz_i++;
		//贷：预收账款-客户(220302)
		$order_data[$ok]['pz_item'][$pz_i]=array(); //借：银行存款/现金
		$order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
		$order_data[$ok]['pz_item'][$pz_i]['account_code']='220302';//科目_编号
		$order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",收款未开票"; //摘要
		$order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
		$order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$the_amount; //贷方金额
		$order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
		$order_data[$ok]['pz_item'][$pz_i]['cust_id']=$u8_account_data['u8_code']; //客户编码(这里用商业ID还是用什么？)
		$order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
		$order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品
		$pz_i++;


	}else if($now_invoice_amount_js==0 and $now_salespay_amount_js==0){
		//都等于0则表示 前面 都抵扣完毕不需要再处理了
	}else{
		echo $now_invoice_amount_js."--".$now_salespay_amount_js;
		echo "出现未知情况，证明前面有什么地方算错了！";
		p($ov);
	}
}
//=================== 开始分类 end =======================
		}
		//p($order_data);exit;
		$pz_count = count($order_data);
		$pz_succ=0; //表示成功的凭证
		$pz_fail=0; //表示失败的凭证
		$pz_pbg=0; //表示同步过的凭证
		//循环订单最后转化成xml凭证
		$date = strtotime($start_time);
		$voucher_id = 2014001; //凭证ID号
		//p($order_data);//exit;
		foreach($order_data as $item){
			if(isset($item['account_fail'])){
				//一旦知道客户同步失败，跳出本次循环，不再同步该这订单的凭证，记失败1次
				p("客户同步失败");
				$pz_fail++;
				continue;
			}
			if(isset($item['product_fail'])){
				//一旦知道产品同步失败，跳出本次循环，不再同步该这订单的凭证，记失败1次
				p("产品同步失败");
				$pz_fail++;
				continue;
			}
			//p($item);exit;
			$xml = $this->createXml($item,$date,$voucher_id);
			//同步凭证到U8
			//$queryXML = '<ufinterface roottag="voucher" receiver="u8" sender="001" proc="add" renewproofno="y" codeexchanged="N" exportneedexch="N" >';
			
			$queryXML = $xml;
			
			//p($queryXML);exit;
			//同步前首先要判断一下这个日期是否同步过
			$sql_count = "select count(*) lee_count from tc_syn_u8_voucher where syn_u8_voucher_order='".$item['order_id']."
			' and syn_u8_voucher_syn_date='".$start_time."'";
			$count_data = $this->db->query($sql_count)->result_array();
			if($count_data[0]['lee_count']==0){
				$res_pz = $this->u8_eai_gl($queryXML);
				//p($res_pz);

				if($res_pz['xml']==""){
					p($item['order_number']."同步成功");
					$pz_succ++; //成功+1
					//同步凭证完毕后，写入同步U8凭证表
					$insql_pz = "insert into tc_syn_u8_voucher (syn_u8_voucher_order,syn_u8_voucher_syn_date,syn_u8_voucher_query_xml) values ('".$item['order_id']."','".$start_time."','".addslashes($queryXML)."')";
					//p($insql_pz);
					$this->db->query($insql_pz);
				}else{
					p($item['order_number']."失败".$res_pz['xml']);
					$pz_fail++;
				}
			}else{
				//p("该时间同步过这个订单的凭证");
				//$pz_fail++;
				$pz_pbg++;
			}
			
			//exit;
		}
		//exit;
		echo $pz_count."个订单同步完毕，成功".$pz_succ."条，失败".$pz_fail."条，同步过的订单".$pz_pbg."条";
	}

    /**
     * U8批量导凭证
     * @param $start_time 开始时间
     */
    public function u8_pz($start_time){
		if(!isset($start_time)){
			echo "没有开始时间是不行的";exit;
		}
		//开始时间，结束时间
		//$start_time = "2014-05-05 00:00:00"; //开始时间
		$end_time = date("Y-m-d 23:59:59",strtotime($start_time)); //结束时间
		//p($start_time."--".$end_time);exit;
		$data = "";
		//首先是根据时间导入，当天导入前天的凭证。
		//根据订单去搜索，条件是开票【开票时间】和入款项【实际到帐日期（用于U8）】
		//搜索出这个时间段的所有已确认款项的入款项信息
		$sql = "select salespay_order_id from tc_salespay where salespay_sp_real_date >= '".$start_time."' and salespay_sp_real_date <= '".$end_time."' and salespay_status=1002";
		$salespay_data = $this->db->query($sql)->result_array();
		//p($sql);
		//搜索出这个时间段的所有已开票的开票信息
		$sql = "select invoice_order_id from tc_invoice where invoice_do_time >= '".$start_time."' and invoice_do_time <= '".$end_time."' and invoice_status=1002";
        //p($sql);
        $invoice_data = $this->db->query($sql)->result_array();
		//根据订单汇总一下！
		//循环入款项
		foreach ($salespay_data as $sk=>$sv){
			$order_id_arr[]=$sv['salespay_order_id'];
		}
		//循环开票
		foreach ($invoice_data as $ik=>$iv){
			$order_id_arr[]=$iv['invoice_order_id'];
		}
		//去重
		if(!isset($order_id_arr)){
			echo "没有搜索到任何达标的订单。";exit;
		}
        //同步前首先要判断一下这个日期是否同步过
        $sql_syn_u8 = "select syn_u8_voucher_order from tc_syn_u8_voucher where syn_u8_voucher_syn_date='".$start_time."'";
        $syn_u8_data = $this->db->query($sql_syn_u8)->result_array();
        foreach ($syn_u8_data as $suk=>$suv){
            $syn_u8_arr[]=$suv['syn_u8_voucher_order'];
        }
        //p($order_id_arr);
        //p($syn_u8_arr);
        $order_id_arr = array_diff($order_id_arr,$syn_u8_arr);
        //p($order_id_arr);
        //exit;
        $order_id_arr = array_unique($order_id_arr);
        sort($order_id_arr); //排序
        //$ys = ceil(count($order_id_arr)/100);
        //for($i=1;$i<=$ys;$i++){
            //$start_on = ($i-1)*100;
            //$new_order_id_arr = array_slice($order_id_arr,$start_on,100);
            //p($new_order_id_arr);

            //$order_id_in = implode(',',$new_order_id_arr);
        //p($order_id_arr);
        //exit;
        $order_id_in = implode(',',$order_id_arr);
            //p($order_id_in);exit;
            //查询出所有需要查询的订单
            /*
            $sql = "
    select
    o.order_id,
    o.order_number,
    o.order_account,
    o.order_department,
    o.order_agent,
    o.type_id,
    o.order_source,
    o.order_deposit_paytype,
    (select sum(i.invoice_amount) from tc_invoice i where i.invoice_order_id = o.order_id and i.invoice_do_time >= '".$start_time."' and i.invoice_do_time <= '".$end_time."' and i.invoice_status=1002) now_invoice_amount,
    (select sum(i.invoice_amount) from tc_invoice i where i.invoice_order_id = o.order_id and i.invoice_do_time < '".$start_time."' and i.invoice_status=1002) before_invoice_amount,
    (select sum(s.salespay_pay_amount) from tc_salespay s where s.salespay_order_id = o.order_id and s.salespay_sp_real_date >= '".$start_time."' and s.salespay_sp_real_date <= '".$end_time."' and s.salespay_status=1002 and s.salespay_money_type = 1001) now_salespay_amount,
    (select sum(s.salespay_pay_amount) from tc_salespay s where s.salespay_order_id = o.order_id and s.salespay_sp_real_date < '".$start_time."' and s.salespay_status=1002 and s.salespay_money_type = 1001) before_salespay_amount
    from tc_order o
    where order_id in(".$order_id_in.")";
            */
            $sql = "
    select
    o.order_id,
    o.order_number,
    o.order_account,
    o.order_department,
    o.order_agent,
    o.type_id,
    o.order_source,
    o.order_deposit_paytype
    from tc_order o
    where order_id in(".$order_id_in.")";
            //echo $sql;exit;
            $order_data = $this->db->query($sql)->result_array();
            //p($order_data);
            //循环$order_data，生成xml
            $date = strtotime($start_time);
            foreach ($order_data as $ok=>$ov){
                $order_id = $ov['order_id'];
                $order_number = $ov['order_number'];
                $order_type_id = $ov['type_id']; //所属部门ID
                $order_department = $ov['order_department']; //所属部门ID
                $order_account = $ov['order_account']; //所属客户ID
                $order_agent = $ov['order_agent']; //代理商ID
                $order_source = $ov['order_source']; //订单来源
                $order_deposit_paytype = $ov['order_deposit_paytype']; //款项类型

                $sql_sum1 = "select sum(i.invoice_amount) now_invoice_amount from tc_invoice i where i.invoice_order_id = '".$order_id."' and i.invoice_do_time >= '".$start_time."' and i.invoice_do_time <= '".$end_time."' and i.invoice_status=1002";
                $sum1_data = $this->db->query($sql_sum1)->result_array();
                //p($sum1_data);exit;
                $now_invoice_amount = $sum1_data[0]['now_invoice_amount']; //当日开票金额

                $sql_sum2 = "select sum(i.invoice_amount) before_invoice_amount from tc_invoice i where i.invoice_order_id = '".$order_id."' and i.invoice_do_time < '".$start_time."' and i.invoice_status=1002";
                $sum2_data = $this->db->query($sql_sum2)->result_array();
                $before_invoice_amount = $sum2_data[0]['before_invoice_amount']; //过去开票金额

                $sql_sum3 = "select sum(s.salespay_pay_amount) now_salespay_amount from tc_salespay s where s.salespay_order_id = '".$order_id."' and s.salespay_sp_real_date >= '".$start_time."' and s.salespay_sp_real_date <= '".$end_time."' and s.salespay_status=1002 and s.salespay_money_type = 1001";
                $sum3_data = $this->db->query($sql_sum3)->result_array();
                $now_salespay_amount = $sum3_data[0]['now_salespay_amount']; //当日入款金额

                $sql_sum4 = "select sum(s.salespay_pay_amount) before_salespay_amount from tc_salespay s where s.salespay_order_id = '".$order_id."' and s.salespay_sp_real_date < '".$start_time."' and s.salespay_status=1002 and s.salespay_money_type = 1001";
                $sum4_data = $this->db->query($sql_sum4)->result_array();
                $before_salespay_amount = $sum4_data[0]['before_salespay_amount']; //过去入款金额


                //还需要什么？

                //这里的部门需要获取到他对应的一级部门的信息
                //$sql_dept = "select department_name,department_code,department_is_u8 from tc_department where department_treepath = (select left(department_treepath,9) from tc_department where department_id='".$order_department."')"; //不在获取一级部门，根据当前部门来获取
                if($order_department!=0 and $order_department!=""){
                $sql_dept = "select department_u8_code,department_name from tc_department where department_id='".$order_department."'";
                //p($sql_dept);//exit;
                $dept_data = $this->db->query($sql_dept)->result_array();
                $dept_data = $dept_data[0];
                //p($dept_data);exit;
                $department_name = $dept_data['department_name']; //部门名称
                $department_code = $dept_data['department_u8_code']; //部门编号
                if($department_code==""){
                    echo "部门【".$department_name."】对应u8编码不能为空！";exit;
                }
                //$department_is_u8 = $dept_data['department_is_u8']; //是否同步U8
                //p($dept_data);
                /*
                if($department_is_u8!=1002){
                    $res_dept = $this->u8_dept($department_name,$department_code); //同步U8
                    //p($res_dept);
                    $item_arr = (array)$res_dept['item'];
                    if($item_arr['@attributes']['dsc']=="ok"){
                        //表示同步完成
                        $upsql_dept = "update tc_department set department_is_u8='1002' where department_id='".$order_department."'";
                        $this->db->query($upsql_dept);
                        //echo "提交完毕";
                    }
                }
                */
                }else{
                    echo $order_number."部门为空";exit;
                }


                //查询出客户信息，用于同步U8客户信息
                if(isset($order_account)){
                    $sql_account = "select account_id,account_name,type_id,account_account_shopexid,account_is_u8,account_pbi_id,account_name_u8 from tc_account where account_id = '".$order_account."'";
                    $account_data = $this->db->query($sql_account)->result_array();

                    $account_data = $account_data[0];
                    $account_id = $account_data['account_id']; //客户ID
                    $account_name = $account_data['account_name']; //客户名称
                    $type_id = $account_data['type_id']; //类型ID
                    $account_account_shopexid = $account_data['account_account_shopexid']; //商业ID
                    $account_is_u8 = $account_data['account_is_u8']; //是否同步U8
                    $account_pbi_id = $account_data['account_pbi_id']; //老营收ID
                    $account_name_u8 = $account_data['account_name_u8']; //客户名称（U8）
                    //组成一个用于同步的客户信息
                    if($type_id==1){
                        $u8_account_data['sort_code']="02";//客户分类为个人
                        //如果是个人 客户名称改成 名称+ID
                        if($account_account_shopexid==""){
                            $u8_account_data['u8_name']=$account_name."_n".$account_id;
                        }else{
                            $u8_account_data['u8_name']=$account_name."_".$account_account_shopexid;
                        }
                    }
                    if($type_id==2){
                        $u8_account_data['sort_code']="01";//客户分类为企业
                        $u8_account_data['u8_name']=$account_name;
                    }
                    //如果老营收ID为空,则使用shopexid作为U8的客户编码，否则用营收的ID
                    if($account_pbi_id==""){
                        $u8_account_data['u8_code']=$account_account_shopexid;
                    }else{
                        $u8_account_data['u8_code']=$account_pbi_id;
                    }
                    //如果没有类型，这种是专门导入的！所以直接用account_id来做
                    if($type_id==""){
                        $u8_account_data['sort_code']="02";//客户分类为个人
                        //如果是个人 客户名称改成 名称+ID
                        $u8_account_data['u8_name']=$account_name."_".$account_id;
                        $u8_account_data['u8_code']=$account_id;
                    }
                    //p($u8_account_data);
                    if($account_is_u8!=1002){
                        //当发现是否同步ID不为是的时候
                        $res_account = $this->u8_account($u8_account_data);
                        //p($res_account);
                        $item_arr = (array)$res_account['item'];
                        if($item_arr['@attributes']['dsc']=="ok"){
                            p("客户同步成功");
                        }else
                        if($item_arr['@attributes']['dsc']=="客户编码已经存在，不可重复！"){
                            p("客户编码已存在");
                            $upsql_account = "update tc_account set account_is_u8='1002' where account_id = '".$order_account."'";
                            $this->db->query($upsql_account);
                        }else
                        if($item_arr['@attributes']['dsc']=="客户简称已经存在，不可重复！"){
                            p($order_number."订单".$account_name.'客户简称已经存在，不可重复！无法同步！怎么办');
                            $order_data[$ok]['account_fail']="1";
                            //exit;
                        }else{
                            //p($sql_account);
                            //p($account_pbi_id);
                            //p($account_account_shopexid);
                            //p($u8_account_data);
                            p($order_number."订单，未知问题5：".$item_arr['@attributes']['dsc']);
                            $order_data[$ok]['account_fail']="1";
                            //exit;
                        }
                    }
                }else{
                    //该订单没有客户信息的时候 看看有没有经销商信息,将经销商信息作为客户信息提交
                    //查询出这个代理商的相关信息
                    if(isset($order_agent)){
                        $sql_agent = "select partner_id,partner_name,partner_shopex_id,partner_is_u8,partner_pbi_id from tc_partner where partner_id = '".$order_agent."'";
                        //p($sql_agent);
                        $agent_data = $this->db->query($sql_agent)->result_array();
                        $agent_data = $agent_data[0];
                        $account_id = $agent_data['partner_id']; //经销商ID
                        $partner_name = $agent_data['partner_name']; //经销商名称
                        $partner_shopex_id = $agent_data['partner_shopex_id']; //经销商商业ID
                        $partner_is_u8 = $agent_data['partner_is_u8']; //是否同步U8
                        $partner_pbi_id = $agent_data['partner_pbi_id']; //老营收ID

                        //组成一个用于同步的客户信息
                        $u8_account_data['sort_code']="03";//经销商
                        $u8_account_data['u8_name']=$partner_name;
                        //如果老营收ID为空,则使用shopexid作为U8的客户编码，否则用营收的ID
                        if($partner_pbi_id==""){
                            $u8_account_data['u8_code']=$partner_shopex_id;
                        }else{
                            $u8_account_data['u8_code']=$partner_pbi_id;
                        }
                        //p($u8_account_data);
                        if($partner_is_u8!=1002){
                            //当发现是否同步ID不为是的时候
                            $res_account = $this->u8_account($u8_account_data);
                            //p($res_account);
                            $item_arr = (array)$res_account['item'];
                            if($item_arr['@attributes']['dsc']=="ok"){
                                p("同步成功");
                                $upsql_partner = "update tc_partner set partner_is_u8='1002' where partner_id = '".$order_agent."'";
                                $this->db->query($upsql_partner);
                            }else
                            if($item_arr['@attributes']['dsc']=="客户编码已经存在，不可重复！"){
                                p("客户编码已存在");
                                $upsql_partner = "update tc_partner set partner_is_u8='1002' where partner_id = '".$order_agent."'";
                                $this->db->query($upsql_partner);
                            }else
                            if($item_arr['@attributes']['dsc']=="客户简称已经存在，不可重复！"){
                                p($agent_data);
                                p($order_number."订单".$partner_name.'代理商简称已经存在，不可重复！无法同步！怎么办');
                                $order_data[$ok]['account_fail']="1";
                                //exit;
                            }else{
                                p("未知问题6：".$item_arr['@attributes']['dsc']);
                                $order_data[$ok]['account_fail']="1";
                            }
                        }
                    }else{
                        p($order_number."该订单既没有客户又没有代理商，怎么处理？？");//exit;
                        $order_data[$ok]['account_fail']="1";
                    }
                }
                //exit;

                //只有在销售订单下 才获取产品信息
                if($order_type_id==13 or $order_type_id==6){
                    //echo 111;exit;
                    //按照订单找到最高金额产品，
                    $sql_pt = "select product_tech_u8_code,product_tech_id,product_tech_code,product_tech_name from tc_product_tech where product_tech_id = (select ptrelation_product_tech_id from tc_ptrelation where ptrelation_product_basic_id = (select product_basic_id from tc_product_basic where product_basic_code = (SELECT order_d_product_basic_code FROM `tc_order_d` WHERE order_d_order_id = '".$ov['order_id']."' order by order_d_product_basic_disc DESC LIMIT 1)))";
                    //p($sql_pt);
                    $product_tech_data = $this->db->query($sql_pt)->result_array();
                    //p($product_tech_data);//exit;
                    $product_tech_u8_code = "";
                    if(isset($product_tech_data[0]['product_tech_u8_code'])){
                        $product_tech_u8_code =  $product_tech_data[0]['product_tech_u8_code']; //技术产品（U8编号）
                    }
                    if($product_tech_u8_code == ""){
                        if(isset($product_tech_data[0])){
                            $u8_inventory_data['code'] = '1'.sprintf("%05d",$product_tech_data[0]['product_tech_id']);
                            $u8_inventory_data['name'] = $product_tech_data[0]['product_tech_name'];
                            //p($u8_inventory_data);
                            $res_inventory = $this->u8_inventory($u8_inventory_data);
                            $item_arr = (array)$res_inventory['item'];
                            //p($item_arr);
                            if($item_arr['@attributes']['dsc']=="ok"){
                                $upsql_pt = "update tc_product_tech set product_tech_u8_code = '".$u8_inventory_data['code']."' where product_tech_id = '".$product_tech_data[0]['product_tech_id']."'";
                                $this->db->query($upsql_pt);
                                p("技术产品同步成功");
                            }else{
                                p($product_tech_code."编号，未知问题7：".$item_arr['@attributes']['dsc']);
                                //exit;
                                $order_data[$ok]['product_fail']="1";
                            }
                        }else{
                            p($order_number."编号，该订单是否没有明细！");
                            //exit;
                            $order_data[$ok]['product_fail']="1";
                        }
                        //p($order_number."订单,没有找到技术产品对应的u8_code");
                        //$order_data[$ok]['product_fail']="1";
                    }
                }


                //如果有当日开票的，需要知道当日开票信息
                if($now_invoice_amount>0){
                    //好像不需要查询开票信息
                    $sql_ni = "select * from tc_invoice i where i.invoice_order_id = '".$ov['order_id']."' and i.invoice_do_time >= '".$start_time."' and i.invoice_do_time <= '".$end_time."' and i.invoice_status=1002";
                    $now_invoice_data = $this->db->query($sql_ni)->result_array();
                    //p($now_invoice_data);
                    if(isset($now_invoice_data[0]['invoice_invoice_no'])){
                        $invoice_invoice_no = $now_invoice_data[0]['invoice_invoice_no'];
                    }else{
                        p($order_number."没有票据号码");
                        exit;
                    }
                    //$invoice_content_arr =  json_decode($now_invoice_data[0]['invoice_content'],true);
                    //p($invoice_content_arr);

                }

                //如果有当日入款的，需要知道当日入款信息
                if($now_salespay_amount>0){
                    $sql_ns = "select salespay_pay_method,salespay_my_bank_account,salespay_my_alipay_account from tc_salespay s where s.salespay_order_id = '".$ov['order_id']."' and s.salespay_sp_real_date >= '".$start_time."' and s.salespay_sp_real_date <= '".$end_time."' and s.salespay_status=1002 and s.salespay_money_type = 1001";
                    $now_salespay_data = $this->db->query($sql_ns)->result_array();
                    $ns_data['salespay_pay_method']=$now_salespay_data[0]['salespay_pay_method'];//支付方式
                    $ns_data['salespay_my_bank_account']=$now_salespay_data[0]['salespay_my_bank_account'];//商派银行账号
                    $ns_data['salespay_my_alipay_account']=$now_salespay_data[0]['salespay_my_alipay_account'];//商派支付宝账号
                    //根据支付方式来归类一下科目编号
                    if($ns_data['salespay_pay_method']==1001){
                        if($this->get_account_code("1001_".$ns_data['salespay_my_bank_account']) == null){
                            p($order_number."没有汇款帐号对应的U8信息");
                            $order_data[$ok]['now_salespay_account_code'] = "";
                            $order_data[$ok]['account_code_fail']="1";
                        }else{
                            //支付方式为汇款
                            $order_data[$ok]['now_salespay_account_code'] = $this->get_account_code("1001_".$ns_data['salespay_my_bank_account']);
                        }

                    }else if($ns_data['salespay_pay_method']==1002){
                        //p($order_number);
                        //p($ns_data);
                        if($ns_data['salespay_my_alipay_account']=="" or $ns_data['salespay_my_alipay_account']==0){
                            p($order_number."没有支付宝入款账号信息");
                            $order_data[$ok]['account_code_fail']="1";
                        }else{
                            //支付方式为支付宝
                            if($this->get_account_code("1002_".$ns_data['salespay_my_alipay_account']) == null){
                                p($order_number."没有支付宝帐号对应的U8信息");
                                $order_data[$ok]['now_salespay_account_code'] = "";
                                $order_data[$ok]['account_code_fail']="1";
                            }else{
                                //支付方式为支付宝
                                $order_data[$ok]['now_salespay_account_code'] = $this->get_account_code("1002_".$ns_data['salespay_my_alipay_account']);
                            }
                        }
                    }else{
                        //其他支付方式
                        $order_data[$ok]['now_salespay_account_code'] = $this->get_account_code[$ns_data['salespay_pay_method']];
                    }
                }

                //如果之前有开票，需要知道之前开票的开票信息
                //如果之前有入款的，需要知道之前入款的信息
                if($before_salespay_amount>0){
                    $sql_ns = "select salespay_pay_method,salespay_my_bank_account,salespay_my_alipay_account from tc_salespay s where s.salespay_order_id = '".$ov['order_id']."' and s.salespay_sp_real_date < '".$start_time."' and s.salespay_status=1002 and s.salespay_money_type = 1001";
                    $now_salespay_data = $this->db->query($sql_ns)->result_array();
                    $ns_data['salespay_pay_method']=$now_salespay_data[0]['salespay_pay_method'];//支付方式
                    $ns_data['salespay_my_bank_account']=$now_salespay_data[0]['salespay_my_bank_account'];//商派银行账号
                    $ns_data['salespay_my_alipay_account']=$now_salespay_data[0]['salespay_my_alipay_account'];//商派支付宝账号
                    //根据支付方式来归类一下科目编号
                    if($ns_data['salespay_pay_method']==1001){
                        //支付方式为汇款
                        //$order_data[$ok]['before_salespay_account_code'] = $this->get_account_code["1001_".$ns_data['salespay_my_bank_account']];
                        if($this->get_account_code("1001_".$ns_data['salespay_my_bank_account']) == null){
                            p($order_number."没有汇款帐号对应的U8信息");
                            $order_data[$ok]['now_salespay_account_code'] = "";
                            $order_data[$ok]['account_code_fail']="1";
                        }else{
                            //支付方式为汇款
                            $order_data[$ok]['now_salespay_account_code'] = $this->get_account_code("1001_".$ns_data['salespay_my_bank_account']);
                        }
                    }else if($ns_data['salespay_pay_method']==1002){
                        //支付方式为支付宝
                        //$order_data[$ok]['before_salespay_account_code'] = $this->get_account_code["1002_".$ns_data['salespay_my_alipay_account']];
                        if($this->get_account_code("1002_".$ns_data['salespay_my_alipay_account']) == null){
                            p($order_number."没有支付宝帐号对应的U8信息");
                            $order_data[$ok]['now_salespay_account_code'] = "";
                            $order_data[$ok]['account_code_fail']="1";
                        }else{
                            //支付方式为支付宝
                            $order_data[$ok]['now_salespay_account_code'] = $this->get_account_code("1002_".$ns_data['salespay_my_alipay_account']);
                        }
                    }else{
                        //其他支付方式
                        $order_data[$ok]['before_salespay_account_code'] = $this->get_account_code[$ns_data['salespay_pay_method']];
                    }
                }

    //首先根据订单类型来判断什么凭证

    //=============预收款订单 对应 收到储值款============
    //其他应付款-保证金 224101
    //其他应付款-云生意储值 224104
    //其他应付款-云猫储值 224105
    //预收账款/客户款 220302
    if($order_type_id==10){ //订单类型=预收款订单
        $the_amount=$now_salespay_amount; //本次金额为【当日收款】

        //根据不同的订单来源给予不同的科目类型
        if($order_source=='1001' and $order_deposit_paytype=='1002'){
            //云猫的预收款
            $pz_account_code='224105';
            $abstract = $ov['order_number'].",收到储值款";
            $u8_account_data['u8_code'] = '541583'; //云猫默认客户都是云猫
            $department_code = '010561'; //云猫 默认挂（公共）
        }else
        if($order_source=='1002' and $order_deposit_paytype=='1002'){
            //云生意的预收款
            $pz_account_code='224104';
            $abstract = $ov['order_number'].",收到储值款";
            $u8_account_data['u8_code'] = '541581'; //云生意默认客户都是云生意
            $department_code = '010561'; //云生意 默认挂（公共）
        }else
        if($order_source=='1003' and $order_deposit_paytype=='1003'){
            //渠道过来的货款
            $pz_account_code='220302';
            $abstract = $ov['order_number'].",收到储值款";
        }else
        if($order_deposit_paytype=='1001'){
            //保证金
            $pz_account_code='224101';
            $abstract = $ov['order_number'].",保证金收款生成";
        }else{
            echo "订单来源，未知枚举错误!";
        }
    /*********款项类型order_deposit_paytype********
    保证金 	1001
    预收款 	1002
    货款 	1003
    退预收款 	1004
    退保证金 	1005
    */
    //p($order_data[$ok]);
        $pz_i=1;//每个凭证明细序号
        //借：银行存款/现金
        $order_data[$ok]['pz_item'][$pz_i]=array(); //借：银行存款/现金
        $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
        $order_data[$ok]['pz_item'][$pz_i]['account_code']=$order_data[$ok]['now_salespay_account_code'];//科目_编号
        $order_data[$ok]['pz_item'][$pz_i]['abstract']=$abstract; //摘要
        $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
        $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
        $order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号 银行存款/现金没有辅助项
        $order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 银行存款/现金没有辅助项
        $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
        $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 银行存款/现金没有辅助项
        $pz_i++;
        //贷：预收账款-客户款(220302)
        $order_data[$ok]['pz_item'][$pz_i]=array(); //贷：预收账款
        $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
        $order_data[$ok]['pz_item'][$pz_i]['account_code']=$pz_account_code;//科目_编号
        $order_data[$ok]['pz_item'][$pz_i]['abstract']=$abstract; //摘要
        $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
        $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$the_amount; //贷方金额
        $order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
        if($pz_account_code=='224104' or $pz_account_code=='224105'){
            $order_data[$ok]['pz_item'][$pz_i]['dept_id'] = '';
        }
        $order_data[$ok]['pz_item'][$pz_i]['cust_id']=$u8_account_data['u8_code']; //客户编码(这里用商业ID还是用什么？)
        $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
        $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品
        $pz_i++;
        //=========保证金收款生成=========
    }


    //==============提货销售订单 对应 开票================
    if($order_type_id==13){ //订单类型=提货销售订单
        $the_amount=$now_invoice_amount; //本次金额为【当日收款】

        //根据不同的订单来源给予不同的科目类型
        if($order_source=='1001'){
            //云猫的货款
            $pz_account_code='224105';
            $abstract = $ov['order_number'].",之前到款当日开票";
            $department_code = '010561'; //云猫  默认挂（公共）
            $u8_account_data['u8_code'] = '541583'; //云猫默认客户都是云猫
        }else
        if($order_source=='1002'){
            //云生意的货款
            $pz_account_code='224104';
            $abstract = $ov['order_number'].",之前到款当日开票";
            $department_code = '010561'; //云生意 默认挂（公共）
            $u8_account_data['u8_code'] = '541581'; //云生意默认客户都是云生意
        }else
        if($order_source=='1003'){
            //渠道过来的货款
            $pz_account_code='220302';
            $abstract = $ov['order_number'].",".$invoice_invoice_no.",之前到款当日开票";
        }else{
            p($order_number);
            echo 1;
            p($order_source);
            echo 2;
            p($order_deposit_paytype);
            echo "提货销售订单，未知情况错误!";exit;
        }
        $pz_i=1;
        //贷：主营业务收入（6001）
        //主营业务收入=金额*0.94 //计算了税率的
        $zyywsr_amount = round($the_amount/1.06,2);
        $order_data[$ok]['pz_item'][$pz_i]=array(); //贷：主营业务收入
        $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
        $order_data[$ok]['pz_item'][$pz_i]['account_code']='6001';//科目_编号
        $order_data[$ok]['pz_item'][$pz_i]['abstract']=$abstract; //摘要
        $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
        $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$zyywsr_amount; //贷方金额
        $order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
        $order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 主营业务收入不挂客户（为空）
        $order_data[$ok]['pz_item'][$pz_i]['item_id']='1'; //项目编号
        $order_data[$ok]['pz_item'][$pz_i]['self_define10']=$product_tech_u8_code; //产品编号 ???
        $pz_i++;
        //贷：应交税费-应交增值税-销项税额(22210101)
        //销项税额=金额*0.06 //计算了税率的
        $xxsg_amount = round(($the_amount/1.06)*0.06,2); //小数点后2位数（四舍五入）
        $order_data[$ok]['pz_item'][$pz_i]=array(); //贷：应交税费-应交增值税-销项税额
        $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
        $order_data[$ok]['pz_item'][$pz_i]['account_code']='22210101';//科目_编号
        $order_data[$ok]['pz_item'][$pz_i]['abstract']=$abstract; //摘要
        $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
        $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$xxsg_amount; //贷方金额
        $order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号
        $order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码(这里用商业ID还是用什么？)
        $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
        $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品
        $pz_i++;
        //借：预收账款-客户款（220302）
        $order_data[$ok]['pz_item'][$pz_i]=array(); //借：预收账款-客户款
        $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
        $order_data[$ok]['pz_item'][$pz_i]['account_code']=$pz_account_code;//科目_编号
        $order_data[$ok]['pz_item'][$pz_i]['abstract']=$abstract; //摘要
        $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
        $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
        $order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
        if($pz_account_code=='224104' or $pz_account_code=='224105'){
            $order_data[$ok]['pz_item'][$pz_i]['dept_id'] = '';
        }
        $order_data[$ok]['pz_item'][$pz_i]['cust_id']=$u8_account_data['u8_code']; //客户编码(这里用商业ID还是用什么？)
        $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
        $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品
        $pz_i++;
    }

    //===================销售订单 开始分类 start =======================
    if($order_type_id==6){ //订单类型=销售订单
        if(isset($now_invoice_amount)){
            $now_invoice_amount_js = $now_invoice_amount; //计算当日剩余开票
        }else{
            $now_invoice_amount_js = 0; //计算当日剩余金额
        }
        if(isset($now_salespay_amount)){
            $now_salespay_amount_js = $now_salespay_amount; //计算当日剩余金额
        }else{
            $now_salespay_amount_js = 0; //计算当日剩余金额
        }
        //--------------先抵扣以前的预收账款 start-----------------
        //当有【剩余开票】和【当日收款】，则做【之前开票当日收款】
        $pz_i=1;//每个凭证明细序号
        if($before_invoice_amount>0 and $now_salespay_amount>0){
            if ($before_invoice_amount>=$now_salespay_amount){
                //当【剩余开票】大于等于【当日收款】
                //金额使用$now_salespay_amount
                $the_amount=$now_salespay_amount; //本次金额为【当日收款】
                $now_salespay_amount_js=$now_salespay_amount_js-$now_salespay_amount;//计算当日剩余开票
            }else if($before_invoice_amount<$now_salespay_amount){
                //当【剩余开票】小于【当日收款】
                //金额用$before_invoice_amount
                $the_amount=$before_invoice_amount; //本次金额为【剩余开票】
                $now_salespay_amount_js=$now_salespay_amount_js-$before_invoice_amount;//计算当日剩余开票
            }else{
                echo "未知情况14";exit;
            }
            //借：银行存款/现金
            $order_data[$ok]['pz_item'][$pz_i]=array(); //借：银行存款/现金
            //$entry_id=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']=$order_data[$ok]['now_salespay_account_code'];//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",之前开票当日收款"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号 银行存款/现金没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 银行存款/现金没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 银行存款/现金没有辅助项
            $pz_i++;
            //贷：应收账款（1122）
            $order_data[$ok]['pz_item'][$pz_i]=array(); //贷：应收账款
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']='1122';//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",之前开票当日收款"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$the_amount; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=$u8_account_data['u8_code']; //客户编码(这里用商业ID还是用什么？) ???
            $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品 ???
            $pz_i++;
        }
        //当【剩余入款】和【当日开票】都大于0，则做【之前入款当日开票】
        if($before_salespay_amount>0 and $now_invoice_amount>0){
            if ($before_salespay_amount>=$now_invoice_amount){
                //当【剩余收款】大于等于【当日开票】
                //金额使用$now_invoice_amount
                $the_amount=$now_invoice_amount; //本次金额为【当日收款】
                $now_invoice_amount_js=$now_invoice_amount_js-$now_invoice_amount;//计算当日剩余开票
            //}else if($before_invoice_amount<$now_salespay_amount){
            }else if($before_salespay_amount<$now_invoice_amount){
                //当【剩余收款】小于【当日开票】
                //当【剩余开票】小于【当日收款】
                //金额用$before_salespay_amount
                $the_amount=$before_salespay_amount; //本次金额为【剩余开票】
                $now_invoice_amount_js=$now_invoice_amount_js-$before_salespay_amount;//计算当日剩余开票
            }else{
                p($before_salespay_amount);
                p($now_invoice_amount);
                echo $ov['order_number']."未知情况15";exit;
            }
            //借：预收账款-客户款（220302）
            $order_data[$ok]['pz_item'][$pz_i]=array(); //借：预收账款-客户款
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']='220302';//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",之前入款当日开票"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=$u8_account_data['u8_code']; //客户编码(这里用商业ID还是用什么？)
            $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品
            $pz_i++;
            //贷：主营业务收入（6001）
            //主营业务收入=金额*0.94 //计算了税率的
            $zyywsr_amount = round($the_amount/1.06,2);
            $order_data[$ok]['pz_item'][$pz_i]=array(); //贷：主营业务收入
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']='6001';//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",".$invoice_invoice_no.",之前入款当日开票"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$zyywsr_amount; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 主营业务收入不挂客户（为空）
            $order_data[$ok]['pz_item'][$pz_i]['item_id']='1'; //项目编号
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=$product_tech_u8_code; //产品编号 ???
            $pz_i++;
            //贷：应交税费-应交增值税-销项税额(22210101)
            //销项税额=金额*0.06 //计算了税率的
            $xxsg_amount = round(($the_amount/1.06)*0.06,2); //小数点后2位数（四舍五入）
            $order_data[$ok]['pz_item'][$pz_i]=array(); //贷：应交税费-应交增值税-销项税额
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']='22210101';//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",".$invoice_invoice_no.",之前入款当日开票"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$xxsg_amount; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码(这里用商业ID还是用什么？)
            $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品
            $pz_i++;
        }
        //--------------先抵扣以前的预收账款 end-----------------


        //过去的金额都抵扣完毕，开始根据当日的款项信息来分类了
        //当【当日剩余开票】大于0 和【当日剩余汇款】大于0，则做【收款且开票】
        //echo $now_invoice_amount_js."-".$now_salespay_amount_js;
        if($now_invoice_amount_js>0 and $now_salespay_amount_js>0){
            if ($now_invoice_amount_js>=$now_salespay_amount_js){
                //当【剩余开票】大于等于【当日收款】
                //金额使用$now_salespay_amount
                $the_amount=$now_salespay_amount_js; //本次金额为【当日收款】
                $now_invoice_amount_js=$now_invoice_amount_js-$now_salespay_amount_js; //计算当日剩余开票
                $now_salespay_amount_js=0; //计算当日剩余入款
            }else if($now_invoice_amount_js<$now_salespay_amount_js){
                //当【剩余开票】小于【当日收款】
                //金额用$before_invoice_amount
                $the_amount=$now_invoice_amount_js; //本次金额为【剩余开票】
                $now_invoice_amount_js=0; //计算当日剩余开票
                $now_salespay_amount_js=$now_salespay_amout_js-$now_invoice_amount_js; //计算当日剩余入款
            }else{
                echo "未知情况16";exit;
            }

            //借：银行存款/现金
            $order_data[$ok]['pz_item'][$pz_i]=array(); //借：银行存款/现金
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']=$order_data[$ok]['now_salespay_account_code'];//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",收款且开票"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号 银行存款/现金没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 银行存款/现金没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 银行存款/现金没有辅助项
            $pz_i++;
            //贷：主营业务收入（6001）
            //主营业务收入=金额*0.94 //计算了税率的
            $zyywsr_amount = round($the_amount/1.06,2);
            $order_data[$ok]['pz_item'][$pz_i]=array(); //贷：主营业务收入
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']='6001';//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",".$invoice_invoice_no.",收款且开票"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$zyywsr_amount; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 主营业务收入不挂客户（为空）
            $order_data[$ok]['pz_item'][$pz_i]['item_id']='1'; //项目编号
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=$product_tech_u8_code; //产品编号 ???
            $pz_i++;
            //贷：应交税费-应交增值税-销项税额(22210101)
            //销项税额=金额*0.06 //计算了税率的
            $xxsg_amount = round(($the_amount/1.06)*0.06,2); //小数点后2位数（四舍五入）
            $order_data[$ok]['pz_item'][$pz_i]=array(); //贷：应交税费-应交增值税-销项税额
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']='22210101';//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",".$invoice_invoice_no.",收款且开票"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$xxsg_amount; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号 销项税额没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 销项税额没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 销项税额没有辅助项
            $pz_i++;
        }

        //-----------最后处理已收款未开票和已开票未收款------------
        if($now_invoice_amount_js>0 and $now_salespay_amount_js==0){
            //当【当日剩余开票】大于0 和【当日剩余收款】等于0，则做【开票未收款】
            $the_amount=$now_invoice_amount_js;//本次金额为【当日剩余开票】
            //借：应收账款（1122）
            $order_data[$ok]['pz_item'][$pz_i]=array(); //借：应收账款
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']='1122';//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",开票未收款"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=$u8_account_data['u8_code']; //客户编码(这里用商业ID还是用什么？)
            $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 应收账款不用挂产品
            $pz_i++;
            //贷：主营业务收入（6001）
            //主营业务收入=金额*0.94 //计算了税率的
            $zyywsr_amount = round($the_amount/1.06,2);
            $order_data[$ok]['pz_item'][$pz_i]=array(); //贷：主营业务收入
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']='6001';//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",".$invoice_invoice_no.",开票未收款"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$zyywsr_amount; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码 主营业务收入不挂客户（为空）
            $order_data[$ok]['pz_item'][$pz_i]['item_id']='1'; //项目编号
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=$product_tech_u8_code; //产品编号 ???
            $pz_i++;
            //贷：应交税费-应交增值税-销项税额(22210101)
            //销项税额=金额*0.06 //计算了税率的
            $xxsg_amount = round(($the_amount/1.06)*0.06,2); //小数点后2位数（四舍五入）
            $order_data[$ok]['pz_item'][$pz_i]=array(); //贷：应交税费-应交增值税-销项税额
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']='22210101';//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",".$invoice_invoice_no.",开票未收款"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$xxsg_amount; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码
            $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号
            $pz_i++;
        }else if($now_invoice_amount_js==0 and $now_salespay_amount_js>0){
            //当【当日剩余开票】等于0 和【当日剩余收款】大于0，则做【收款未开票】
            $the_amount=$now_salespay_amount_js;//本次金额为【当日剩余收款】
            //借：银行存款/现金
            $order_data[$ok]['pz_item'][$pz_i]=array(); //借：银行存款/现金
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']=$order_data[$ok]['now_salespay_account_code'];//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",收款未开票"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=$the_amount; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=0; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=''; //部门编号
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=''; //客户编码
            $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号
            $pz_i++;
            //贷：预收账款-客户(220302)
            $order_data[$ok]['pz_item'][$pz_i]=array(); //借：银行存款/现金
            $order_data[$ok]['pz_item'][$pz_i]['entry_id']=$pz_i; //序号
            $order_data[$ok]['pz_item'][$pz_i]['account_code']='220302';//科目_编号
            $order_data[$ok]['pz_item'][$pz_i]['abstract']=$ov['order_number'].",收款未开票"; //摘要
            $order_data[$ok]['pz_item'][$pz_i]['natural_debit_currency']=0; //借方金额
            $order_data[$ok]['pz_item'][$pz_i]['natural_credit_currency']=$the_amount; //贷方金额
            $order_data[$ok]['pz_item'][$pz_i]['dept_id']=$department_code; //部门编号
            $order_data[$ok]['pz_item'][$pz_i]['cust_id']=$u8_account_data['u8_code']; //客户编码(这里用商业ID还是用什么？)
            $order_data[$ok]['pz_item'][$pz_i]['item_id']=''; //项目编号（其实就是技术产品） 销项税额没有辅助项
            $order_data[$ok]['pz_item'][$pz_i]['self_define10']=''; //产品编号 预收账款不用挂产品
            $pz_i++;


        }else if($now_invoice_amount_js==0 and $now_salespay_amount_js==0){
            //都等于0则表示 前面 都抵扣完毕不需要再处理了
        }else{
            echo $now_invoice_amount_js."--".$now_salespay_amount_js;
            echo "出现未知情况，证明前面有什么地方算错了！";
            p($ov);
        }
    }
    //=================== 开始分类 end =======================
            }
            //p($order_data);
            $pz_count = count($order_data);
            $pz_succ=0; //表示成功的凭证
            $pz_fail=0; //表示失败的凭证
            $pz_pbg=0; //表示同步过的凭证
            //循环订单最后转化成xml凭证
            $date = strtotime($start_time);
            $voucher_id = 2014001; //凭证ID号
            //p($order_data);//exit;
            foreach($order_data as $item){
                if(isset($item['account_code_fail'])){
                    //一旦知道客户同步失败，跳出本次循环，不再同步该这订单的凭证，记失败1次
                    p("科目编码同步失败");
                    $pz_fail++;
                    continue;
                }

                if(isset($item['account_fail'])){
                    //一旦知道客户同步失败，跳出本次循环，不再同步该这订单的凭证，记失败1次
                    p("客户同步失败");
                    $pz_fail++;
                    continue;
                }
                if(isset($item['product_fail'])){
                    //一旦知道产品同步失败，跳出本次循环，不再同步该这订单的凭证，记失败1次
                    p("产品同步失败");
                    $pz_fail++;
                    continue;
                }
                //p($item);exit;
                $xml = $this->createXml($item,$date,$voucher_id);
                //同步凭证到U8
                //$queryXML = '<ufinterface roottag="voucher" receiver="u8" sender="001" proc="add" renewproofno="y" codeexchanged="N" exportneedexch="N" >';

                $queryXML = $xml;

                //p($queryXML);
                //exit;
                //同步前首先要判断一下这个日期是否同步过
                $sql_count = "select count(*) lee_count from tc_syn_u8_voucher where syn_u8_voucher_order='".$item['order_id']."
                ' and syn_u8_voucher_syn_date='".$start_time."'";
                $count_data = $this->db->query($sql_count)->result_array();
                if($count_data[0]['lee_count']==0){
                    $res_pz = $this->u8_eai_gl($queryXML);
                    //p($res_pz);

                    if($res_pz['xml']==""){
                        p($item['order_number']."同步成功");
                        $pz_succ++; //成功+1
                        //同步凭证完毕后，写入同步U8凭证表
                        $insql_pz = "insert into tc_syn_u8_voucher (syn_u8_voucher_order,syn_u8_voucher_syn_date,syn_u8_voucher_query_xml) values ('".$item['order_id']."','".$start_time."','".addslashes($queryXML)."')";
                        //p($insql_pz);
                        $this->db->query($insql_pz);
                    }else{
                        p($item['order_number']."失败".$res_pz['xml']);
                        $pz_fail++;
                    }

                    //这里开始判断同步凭证是否成功
                    //首先要看报错内容
                    /*
                    $item_arr = (array)$res_pz['item'];
                    //echo 111;
                    //p($item_arr);
                    if(isset($item_arr[1])){
                        //这样代表有多个返回结果，一般都是报错信息，循环输出
                        foreach($item_arr as $ia_v){
                            $ia_v_arr = (array)$ia_v;
                            p($ia_v_arr['@attributes']['dsc']);
                        }
                        $pz_fail++;
                    }else{
                        //这种表示只有一个返回结果
                        //p($item_arr);
                        $u8_dsc = $item_arr['@attributes']['dsc'];
                        if($u8_dsc == '凭证已成功保存'){
                            $pz_succ++; //成功+1
                            //同步凭证完毕后，写入同步U8凭证表
                            $insql_pz = "insert into tc_syn_u8_voucher (syn_u8_voucher_order,syn_u8_voucher_syn_date,syn_u8_voucher_query_xml) values ('".$item['order_id']."','".$start_time."','".addslashes($queryXML)."')";
                            //p($insql_pz);
                            $this->db->query($insql_pz);
                        }
                    }
                    */
                }else{
                    //p("该时间同步过这个订单的凭证");
                    //$pz_fail++;
                    $pz_pbg++;
                }

                //exit;
            }
            //exit;
            echo $pz_count."个订单同步完毕，成功".$pz_succ."条，失败".$pz_fail."条，同步过的订单".$pz_pbg."条";

        //}
	}


	//同步部门
	public function u8_dept($department_name,$department_code){
		$xml = '
			<department>
				<code>'.$department_code.'</code>
				<endflag>1</endflag>
				<name>'.$department_name.'</name>
				<rank>1</rank>
				<manager/>
				<prop/>
				<phone/>
				<address/>
				<remark/>
				<creditline/>
				<creditgrade/>
				<creditdate/>
				<ddepbegindate/>
				<ddependdate/>
				<vauthorizedoc/>
				<vauthorizeunit/>
				<cdepfax/>
				<cdeppostcode/>
				<cdepemail/>
				<cdeptype/>
			</department>';
		$resxml_arr = $this->u8_eai($xml);
		return $resxml_arr;
		//p($resxml_arr);
	}


	//添加产品的xml(存货档案)
	public function u8_inventory($u8_inventory_data){
		//p($u8_inventory_data);
		//exit;
		$xml = '
<ufinterface sender="001" receiver="u8" roottag="inventory" proc="add" codeexchanged="N" exportneedexch="N"> 
  <inventory>
    <header>
      <code>'.$u8_inventory_data['code'].'</code>
      <name>'.$u8_inventory_data['name'].'</name>
      <InvAddCode/>
      <specs/>
      <sort_code>01</sort_code>
      <main_supplier/>
      <main_measure>1</main_measure>
      <switch_item/>
      <inv_position/>
      <sale_flag>1</sale_flag>
      <purchase_flag>0</purchase_flag>
      <selfmake_flag>0</selfmake_flag>
      <prod_consu_flag>0</prod_consu_flag>
      <in_making_flag>0</in_making_flag>
      <tax_serv_flag>0</tax_serv_flag>
      <suit_flag>0</suit_flag>
      <tax_rate>0</tax_rate>
      <unit_weight/>
      <unit_volume>0</unit_volume>
      <pro_sale_price/>
      <ref_cost/>
      <ref_sale_price/>
      <bottom_sale_price/>
      <new_cost/>
      <advance_period/>
      <ecnomic_batch/>
      <safe_stock/>
      <top_stock/>
      <bottom_stock/>
      <backlog/>
      <ABC_type/>
      <qlty_guarantee_flag>0</qlty_guarantee_flag>
      <batch_flag>0</batch_flag>
      <entrust_flag>0</entrust_flag>
      <backlog_flag>0</backlog_flag>
      <start_date>2012-01-01 00:00:00</start_date>
      <end_date/>
      <free_item1>0</free_item1>
      <free_item2>0</free_item2>
      <self_define1/>
      <self_define2/>
      <self_define3/>
      <discount_flag>0</discount_flag>
      <top_source_price/>
      <quality/>
      <retailprice/>
      <price1/>
      <price2/>
      <price3/>
      <CreatePerson/>
      <ModifyPerson/>
      <ModifyDate/>
      <subscribe_point/>
      <avgquantity/>
      <pricetype/>
      <bfixunit>0</bfixunit>
      <outline/>
      <inline/>
      <overdate/>
      <warndays/>
      <expense_rate/>
      <btrack>0</btrack>
      <bserial>0</bserial>
      <bbarcode>0</bbarcode>
      <barcode/>
      <auth_class/>
      <self_define4/>
      <self_define5/>
      <self_define6/>
      <self_define7/>
      <self_define8/>
      <self_define9/>
      <self_define10/>
      <self_define11/>
      <self_define12/>
      <self_define13/>
      <self_define14/>
      <self_define15/>
      <self_define16/>
      <free_item3>0</free_item3>
      <free_item4>0</free_item4>
      <free_item5>0</free_item5>
      <free_item6>0</free_item6>
      <free_item7>0</free_item7>
      <free_item8>0</free_item8>
      <free_item9>0</free_item9>
      <free_item10>0</free_item10>
      <unitgroup_type>0</unitgroup_type>
      <unitgroup_code>1</unitgroup_code>
      <puunit_code/>
      <saunit_code/>
      <stunit_code/>
      <caunit_code/>
      <unitgroup_name>套</unitgroup_name>
      <ccomunitname>套</ccomunitname>
      <puunit_name/>
      <saunit_name/>
      <stunit_name/>
      <caunit_name/>
      <puunit_ichangrate/>
      <saunit_ichangrate/>
      <stunit_ichangrate/>
      <caunit_ichangrate/>
      <check_frequency/>
      <frequency/>
      <check_day/>
      <lastcheck_date/>
      <wastage/>
      <solitude>0</solitude>
      <enterprise/>
      <address/>
      <file/>
      <brand/>
      <checkout_no/>
      <licence/>
      <specialties>0</specialties>
      <defwarehouse/>
      <salerate/>
      <advanceDate/>
      <currencyName/>
      <ProduceAddress/>
      <produceNation/>
      <RegisterNo/>
      <EnterNo/>
      <PackingType/>
      <EnglishName/>
      <PropertyCheck>0</PropertyCheck>
      <PreparationType/>
      <Commodity/>
      <RecipeBatch>0</RecipeBatch>
      <NotPatentName/>
      <cAssComunitCode/>
      <ROPMethod/>
      <SubscribePoint/>
      <BatchRule/>
      <AssureProvideDays/>
      <VagQuantity/>
      <TestStyle/>
      <DTMethod/>
      <DTRate/>
      <DTNum/>
      <DTUnit/>
      <DTStyle/>
      <QTMethod/>
      <bPlanInv>0</bPlanInv>
      <bProxyForeign>0</bProxyForeign>
      <bATOModel>0</bATOModel>
      <bCheckItem>0</bCheckItem>
      <bPTOModel>0</bPTOModel>
      <bequipment>0</bequipment>
      <cProductUnit/>
      <fOrderUpLimit/>
      <cMassUnit>0</cMassUnit>
      <fRetailPrice/>
      <cInvDepCode/>
      <iAlterAdvance/>
      <fAlterBaseNum/>
      <cPlanMethod>R</cPlanMethod>
      <bMPS>0</bMPS>
      <bROP>0</bROP>
      <bRePlan>0</bRePlan>
      <cSRPolicy>PE</cSRPolicy>
      <bBillUnite>0</bBillUnite>
      <iSupplyDay/>
      <fSupplyMulti/>
      <fMinSupply/>
      <bCutMantissa>0</bCutMantissa>
      <cInvPersonCode/>
      <iInvTfId/>
      <cEngineerFigNo/>
      <bInTotalCost>0</bInTotalCost>
      <iSupplyType>0</iSupplyType>
      <bConfigFree1>0</bConfigFree1>
      <bConfigFree2>0</bConfigFree2>
      <bConfigFree3>0</bConfigFree3>
      <bConfigFree4>0</bConfigFree4>
      <bConfigFree5>0</bConfigFree5>
      <bConfigFree6>0</bConfigFree6>
      <bConfigFree7>0</bConfigFree7>
      <bConfigFree8>0</bConfigFree8>
      <bConfigFree9>0</bConfigFree9>
      <bConfigFree10>0</bConfigFree10>
      <iDTLevel/>
      <cDTAQL/>
      <bOutInvDT>0</bOutInvDT>
      <bPeriodDT>0</bPeriodDT>
      <cDTPeriod/>
      <bBackInvDT>0</bBackInvDT>
      <iEndDTStyle/>
      <bDTWarnInv/>
      <fBackTaxRate/>
      <cCIQCode/>
      <cWGroupCode/>
      <cWUnit/>
      <fGrossW/>
      <cVGroupCode/>
      <cVUnit/>
      <fLength/>
      <fWidth/>
      <fHeight/>
      <cpurpersoncode/>
      <iBigMonth/>
      <iBigDay/>
      <iSmallMonth/>
      <iSmallDay/>
      <cshopunit/>
      <bimportmedicine>0</bimportmedicine>
      <bfirstbusimedicine>0</bfirstbusimedicine>
      <bforeexpland>0</bforeexpland>
      <cinvplancode/>
      <fconvertrate>1</fconvertrate>
      <dreplacedate/>
      <binvmodel>0</binvmodel>
      <iimptaxrate/>
      <iexptaxrate/>
      <bexpsale>1</bexpsale>
      <idrawbatch/>
      <bcheckbsatp>0</bcheckbsatp>
      <cinvprojectcode/>
      <itestrule/>
      <crulecode/>
      <bcheckfree1>0</bcheckfree1>
      <bcheckfree2>0</bcheckfree2>
      <bcheckfree3>0</bcheckfree3>
      <bcheckfree4>0</bcheckfree4>
      <bcheckfree5>0</bcheckfree5>
      <bcheckfree6>0</bcheckfree6>
      <bcheckfree7>0</bcheckfree7>
      <bcheckfree8>0</bcheckfree8>
      <bcheckfree9>0</bcheckfree9>
      <bcheckfree10>0</bcheckfree10>
      <bbommain>0</bbommain>
      <bbomsub>0</bbomsub>
      <bproductbill>0</bproductbill>
      <icheckatp>0</icheckatp>
      <iinvatpid/>
      <iplantfday/>
      <ioverlapday/>
      <fmaxsupply/>
      <bpiece>0</bpiece>
      <bsrvitem>0</bsrvitem>
      <bsrvfittings>0</bsrvfittings>
      <fminsplit/>
      <bspecialorder>0</bspecialorder>
      <btracksalebill>0</btracksalebill>
      <fbuyexcess/>
      <isurenesstype>1</isurenesstype>
      <idatetype/>
      <idatesum/>
      <idynamicsurenesstype/>
      <ibestrowsum/>
      <ipercentumsum/>
      <binbyprocheck>1</binbyprocheck>
      <irequiretrackstyle>0</irequiretrackstyle>
      <ibomexpandunittype>1</ibomexpandunittype>
      <iexpiratdatecalcu>0</iexpiratdatecalcu>
      <bpurpricefree1>0</bpurpricefree1>
      <bpurpricefree2>0</bpurpricefree2>
      <bpurpricefree3>0</bpurpricefree3>
      <bpurpricefree4>0</bpurpricefree4>
      <bpurpricefree5>0</bpurpricefree5>
      <bpurpricefree6>0</bpurpricefree6>
      <bpurpricefree7>0</bpurpricefree7>
      <bpurpricefree8>0</bpurpricefree8>
      <bpurpricefree9>0</bpurpricefree9>
      <bpurpricefree10>0</bpurpricefree10>
      <bompricefree1>0</bompricefree1>
      <bompricefree2>0</bompricefree2>
      <bompricefree3>0</bompricefree3>
      <bompricefree4>0</bompricefree4>
      <bompricefree5>0</bompricefree5>
      <bompricefree6>0</bompricefree6>
      <bompricefree7>0</bompricefree7>
      <bompricefree8>0</bompricefree8>
      <bompricefree9>0</bompricefree9>
      <bompricefree10>0</bompricefree10>
      <bsalepricefree1>0</bsalepricefree1>
      <bsalepricefree2>0</bsalepricefree2>
      <bsalepricefree3>0</bsalepricefree3>
      <bsalepricefree4>0</bsalepricefree4>
      <bsalepricefree5>0</bsalepricefree5>
      <bsalepricefree6>0</bsalepricefree6>
      <bsalepricefree7>0</bsalepricefree7>
      <bsalepricefree8>0</bsalepricefree8>
      <bsalepricefree9>0</bsalepricefree9>
      <bsalepricefree10>0</bsalepricefree10>
      <finvoutuplimit/>
      <bbondedinv>0</bbondedinv>
      <bbatchcreate>0</bbatchcreate>
      <bbatchproperty1>0</bbatchproperty1>
      <bbatchproperty2>0</bbatchproperty2>
      <bbatchproperty3>0</bbatchproperty3>
      <bbatchproperty4>0</bbatchproperty4>
      <bbatchproperty5>0</bbatchproperty5>
      <bbatchproperty6>0</bbatchproperty6>
      <bbatchproperty7>0</bbatchproperty7>
      <bbatchproperty8>0</bbatchproperty8>
      <bbatchproperty9>0</bbatchproperty9>
      <bbatchproperty10>0</bbatchproperty10>
      <bcontrolfreerange1>0</bcontrolfreerange1>
      <bcontrolfreerange2>0</bcontrolfreerange2>
      <bcontrolfreerange3>0</bcontrolfreerange3>
      <bcontrolfreerange4>0</bcontrolfreerange4>
      <bcontrolfreerange5>0</bcontrolfreerange5>
      <bcontrolfreerange6>0</bcontrolfreerange6>
      <bcontrolfreerange7>0</bcontrolfreerange7>
      <bcontrolfreerange8>0</bcontrolfreerange8>
      <bcontrolfreerange9>0</bcontrolfreerange9>
      <bcontrolfreerange10>0</bcontrolfreerange10>
      <finvciqexch>1</finvciqexch>
      <iwarrantyperiod/>
      <iwarrantyunit>0</iwarrantyunit>
      <binvkeypart>1</binvkeypart>
      <iacceptearlydays>999</iacceptearlydays>
      <fcurllaborcost/>
      <fcurlvarmanucost/>
      <fcurlfixmanucost/>
      <fcurlomcost/>
      <fnextllaborcost/>
      <fnextlvarmanucost/>
      <fnextlfixmanucost/>
      <fnextlomcost/>
      <dinvcreatedatetime>'.date('Y-m-d H:i:s').'</dinvcreatedatetime>
      <bpuquota>0</bpuquota>
      <binvrohs>0</binvrohs>
      <fprjmatlimit/>
      <bprjmat>0</bprjmat>
      <binvasset>0</binvasset>
      <bsrvproduct>0</bsrvproduct>
      <iacceptdelaydays>0</iacceptdelaydays>
      <cInvMnemCode/>
      <iPlanCheckDay>0</iPlanCheckDay>
      <iMaterialsCycle/>
      <idrawtype>0</idrawtype>
      <bSCkeyProjections>0</bSCkeyProjections>
    </header>
    <body/>
  </inventory> 
</ufinterface>
		';
		$resxml_arr = $this->u8_eai($xml);
		//p($resxml_arr);
		return $resxml_arr;
	}

	//同步客户信息
	public function u8_account($u8_account_data){
		$xml = '
<ufinterface sender="001" receiver="u8" roottag="customer" proc="add" codeexchanged="N" exportneedexch="N"> 
  <customer> 
    <code>'.$u8_account_data['u8_code'].'</code>  
    <name>'.$u8_account_data['u8_name'].'</name>  
    <abbrname>'.$u8_account_data['u8_name'].'</abbrname>  
    <sort_code>'.$u8_account_data['sort_code'].'</sort_code>  
    <domain_code/>  
    <industry/>  
    <address/>  
    <postcode/>  
    <tax_reg_code/>  
    <bank_open/>  
    <bank_acc_number/>  
    <seed_date>'.date('Y-m-d H:i:s').'</seed_date>  
    <legal_man/>
    <email/>  
    <contact/>
    <phone/>  
    <fax/>  
    <bp/>  
    <mobile/>  
    <spec_operator/>  
    <discount_rate>0</discount_rate>  
    <credit_rank/>  
    <credit_amount>0</credit_amount>  
    <credit_deadline>0</credit_deadline>  
    <pay_condition/>  
    <devliver_site/>
    <deliver_mode/>  
    <head_corp_code>1001</head_corp_code>
    <deli_warehouse/>  
    <super_dept/>  
    <ar_rest>0</ar_rest>  
    <last_tr_date/>  
    <last_tr_amount>0</last_tr_amount>
    <last_rec_date/>  
    <last_rec_amount>0</last_rec_amount>
    <end_date/>
    <tr_frequency>0</tr_frequency>
    <self_define1/>
    <self_define2/>
    <self_define3/>
    <pricegrade>-1</pricegrade>  
    <CreatePerson>阎静</CreatePerson>  
    <ModifyPerson>阎静</ModifyPerson>  
    <ModifyDate>'.date('Y-m-d H:i:s').'</ModifyDate>  
    <auth_class/>  
    <self_define4/>  
    <self_define5/>  
    <self_define6/>  
    <self_define7/>  
    <self_define8/>  
    <self_define9/>  
    <self_define10/>  
    <self_define11/>  
    <self_define12/>  
    <self_define13/>  
    <self_define14/>  
    <self_define15/>  
    <self_define16/>  
    <InvoiceCompany>1001</InvoiceCompany>  
    <Credit>0</Credit>  
    <CreditByHead>0</CreditByHead>  
    <CreditDate>0</CreditDate>  
    <LicenceDate>0</LicenceDate>  
    <LicenceSDate/>  
    <LicenceEDate/>  
    <LicenceADays/>  
    <BusinessDate>0</BusinessDate>
    <BusinessSDate/>
    <BusinessEDate/>
    <BusinessADays/>
    <Proxy>0</Proxy>
    <ProxySDate/>
    <ProxyEDate/>  
    <ProxyADays/>  
    <Memo/>  
    <bLimitSale>0</bLimitSale>  
    <cCusCountryCode/>  
    <cCusEnName/>  
    <cCusEnAddr1/>  
    <cCusEnAddr2/>  
    <cCusEnAddr3/>  
    <cCusEnAddr4/>  
    <cCusPortCode/>  
    <cPrimaryVen/>  
    <fCommisionRate/>  
    <fInsueRate/>  
    <bHomeBranch>0</bHomeBranch>  
    <cBranchAddr/>  
    <cBranchPhone/>
    <cBranchPerson/>
    <cCusTradeCCode/>
    <CustomerKCode/>
    <bCusState>0</bCusState>
    <ccusbankcode/>
    <cRelVendor/>
    <ccusexch_name>人民币</ccusexch_name>
    <bshop>0</bshop>
    <Crm_Contact_cMobilePhone/>
    <Crm_Contact_cOfficePhone/>
    <bcusdomestic>1</bcusdomestic>
    <bcusoverseas>0</bcusoverseas>
    <ccuscreditcompany>1001</ccuscreditcompany>
    <ccussaprotocol/>
    <ccusexprotocol/>
    <ccusotherprotocol/>
    <fcusdiscountrate/>
    <ccussscode/>
    <ccuscmprotocol/>
    <dcuscreatedatetime>'.date('Y-m-d H:i:s').'</dcuscreatedatetime>  
    <cCusMnemCode/>  
    <fAdvancePaymentRatio/>  
    <bServiceAttribute>0</bServiceAttribute>  
    <bOnGPinStore>0</bOnGPinStore>  
    <bRequestSign>0</bRequestSign>  
    <sa_invoicecustomersall>
      <sa_invoicecustomers>
        <ccuscode>1001</ccuscode>
        <cinvoicecompany>1001</cinvoicecompany>
        <autoid>45380</autoid>
        <bdefault>True</bdefault>
      </sa_invoicecustomers>
    </sa_invoicecustomersall> 
  </customer>
</ufinterface>
		';
		$resxml_arr = $this->u8_eai($xml);
		return $resxml_arr;
	}
	
	//凭证创建xml
	public function createXml($item,$date,$voucher_id){
		//$voucher_id = '1';
		//$sql = 'SELECT max(id) as maxid FROM sdb_manager_mcheckout';
		//$row = $this->db->select($sql);
		//$voucher_id = $row['0']['maxid'] + 1;		

		$xml = "<voucher>
					<voucher_head>
						<company></company>
						<voucher_type>记</voucher_type>
						<fiscal_year>".date('Y',$date)."</fiscal_year>
						<accounting_period>".date('n',$date)."</accounting_period>
						<voucher_id></voucher_id>
						<attachment_number></attachment_number>
						<date>".date('Y-n-j',$date)."</date>
						<auditdate></auditdate>
						<enter>UAP</enter>
						<cashier></cashier>
						<signature></signature>
						<checker></checker>
						<posting_date></posting_date>
						<posting_person></posting_person>
						<voucher_making_system></voucher_making_system>
						<memo1></memo1>
						<memo2></memo2>
						<reserve1></reserve1>
						<reserve2></reserve2>
						<revokeflag></revokeflag>
					</voucher_head><voucher_body>";
		
		$i = 0;
		$xml_body = "";
		
		if(isset($item['pz_item'])){
			foreach($item['pz_item'] as $k=>$v){
				//p($v);
				$xml_body .= "
					<entry>
						<entry_id>".$v['entry_id']."</entry_id>
						<account_code>".$v['account_code']."</account_code>
						<abstract>".$v['abstract']."</abstract>
						<settlement></settlement>
						<document_id></document_id>
						<document_date></document_date>
						<currency></currency>
						<unit_price></unit_price>
						<exchange_rate1></exchange_rate1>
						<exchange_rate2></exchange_rate2>
						<debit_quantity></debit_quantity>
						<primary_debit_amount></primary_debit_amount>
						<secondary_debit_amount></secondary_debit_amount>
						<natural_debit_currency>".$v['natural_debit_currency']."</natural_debit_currency>
						<credit_quantity></credit_quantity>
						<primary_debit_amount></primary_debit_amount>
						<secondary_credit_amount></secondary_credit_amount>
						<natural_credit_currency>".$v['natural_credit_currency']."</natural_credit_currency>
						<bill_type></bill_type>
						<bill_id></bill_id>
						<bill_date></bill_date>
						<auxiliary_accounting>
							<item name='dept_id'>".$v['dept_id']."</item>
							<item name='personnel_id'></item>
							<item name='cust_id'>".$v['cust_id']."</item>
							<item name='supplier_id'></item>
							<item name='item_id'>".$v['item_id']."</item>
							<item name='item_class'></item>
							<item name='operator'></item>
							<item name='self_define1'></item>
							<item name='self_define2'></item>
							<item name='self_define3'></item>
							<item name='self_define4'></item>
							<item name='self_define5'></item>
							<item name='self_define6'></item>
							<item name='self_define7'></item>
							<item name='self_define8'></item>
							<item name='self_define9'></item>
							<item name='self_define10'>".$v['self_define10']."</item>
							<item name='self_define11'></item>
							<item name='self_define12'></item>
							<item name='self_define13'></item>
							<item name='self_define14'></item>
							<item name='self_define15'></item>
							<item name='self_define16'></item>
						</auxiliary_accounting>
						<detail>
							<cash_flow_statement></cash_flow_statement>
							<code_remark_statement></code_remark_statement>
						</detail>
					</entry>
				";
			}
		}


		if(isset($xml_body)){
			$xml .= $xml_body."</voucher_body></voucher>";
		}else{
			$xml = "";
		}
		//p($xml);
		return $xml;
	}


	//正常的eai接口
	public function u8_eai($xml){
		$snoopy = new Snoopy();
		$url = "http://192.168.35.28:9014/api/u8";
		$queryXML = '<?xml version="1.0" encoding="utf-8"?>';
		$queryXML .= $xml;
		//echo $queryXML;
		//exit;
		$params = array();
		//$url = "http://192.168.0.17/U8EAI/import_gl.asp";
		//$url = "http://192.168.0.102/U8EAI/import_gl.asp";
		//$params['url']='http://192.168.0.102/U8EAI/import.asp';//U8测试环境eai接口地址
		$params['url']='http://192.168.0.17/U8EAI/import.asp';//U8正式环境eai接口地址
		$params['xml']=$queryXML;
		$snoopy->submit($url,$params);
		$res = $snoopy->results;
		//p($res);
		//exit;
		$res_arr = json_decode($res,true);
		//p($res_arr);exit;
		if(!isset($res_arr)){
			error_log("\r\n".'-----------------query '.date('Y-m-d H:i:s').'--------------'."\r\n".$queryXML."\r\n", 3, dirname(__FILE__) . '/../../../' . 'logs/lee/U8_' . date("Y-m-d_H")."_timeout.txt");
			echo '接口同步失败，可能连接服务器超时,请检查eai接口';
			exit;
		};
		$resxml = $res_arr['xml'];
		//xml转化成数组
		$resxml_arr = (array)simplexml_load_string($resxml);
		//做下log日志
		error_log("\r\n".'-----------------query '.date('Y-m-d H:i:s').'--------------'."\r\n".$queryXML."\r\n".'-----------------result '.date('Y-m-d H:i:s').'--------------'."\r\n".$resxml, 3, dirname(__FILE__) . '/../../../' . 'logs/lee/U8_' . date("Y-m-d_H") . ".txt");
		//p($xml_arr);
		//print_r($xml_arr['item']->attributes);
		//$item_arr = (array)$resxml_arr['item'];
		return $resxml_arr;
		//echo $item_arr['@attributes']['dsc'];
	}

	//gl开发的eat接口
	public function u8_eai_gl($xml){
		$snoopy = new Snoopy();
		$url = "http://192.168.35.28:9014/api/u8";
		$queryXML = '<?xml version="1.0" encoding="utf-8"?>';
		$queryXML .= '<ufinterface roottag="voucher" billtype="gl" docid="81554591" receiver="u8" sender="111" proc="query" codeexchanged="N" renewproofno="n" exportneedexch="N" timestamp="0x00000000003FBD41" lastquerydate="2012-02-06 12:00:29">';
		$queryXML .= $xml;
		$queryXML .= '</ufinterface>';
		//echo $queryXML;
		//exit;
		$params = array();
		//$url = "http://192.168.0.17/U8EAI/import_gl.asp";
		//$url = "http://192.168.0.102/U8EAI/import_gl.asp";
		//$params['url']='http://192.168.0.102/U8EAI/import_gl.asp';//U8测试环境eai接口地址
		$params['url']='http://192.168.0.17/U8EAI/import_gl.asp';//U8正式环境eai接口地址
		$params['xml']=$queryXML;
		$snoopy->submit($url,$params);
		$res = $snoopy->results;
		$res_arr = json_decode($res,true);
		//做下log日志
		error_log("\r\n".'-----------------query '.date('Y-m-d H:i:s').'--------------'."\r\n".$queryXML."\r\n".'-----------------result '.date('Y-m-d H:i:s').'--------------'."\r\n".$res, 3, dirname(__FILE__) . '/../../../' . 'logs/lee/U8_' . date("Y-m-d_H") . ".txt");
		return $res_arr;
		
	}


	//修复时间戳的开通
	public function lee_shijiancuo(){
		//首先查询出这些有错误的订单明细编号
		$sql = "
SELECT order_d_id FROM tc_order_d WHERE order_d_order_id IN(
SELECT order_id FROM `tc_order` WHERE `order_number` IN (
'so201407300005'
)
) 
AND order_d_open_enddate = '0000-00-00 00:00:00'
		";
		$data = $this->db->query($sql)->result_array();
		//p($data);
		$js = 0;
		foreach($data as $v){
			$order_d_id = $v["order_d_id"];
		
			$sql2 = "select applyopen_return_info from tc_applyopen where applyopen_order_d_id = '".$order_d_id ."'";
			$data2 = $this->db->query($sql2)->result_array();
			$applyopen_return_info = $data2[0]['applyopen_return_info'];
			$return_arr = json_decode($applyopen_return_info,true);
			//p($return_arr);
			$startdate = date("Y-m-d H:i:s",$return_arr['actual_startdate']);
			$enddate = date("Y-m-d H:i:s",$return_arr['actual_enddate']);
			//p($startdate."-".$enddate);
			$return_arr_ok = $return_arr;
			$return_arr_ok['actual_startdate'] = $startdate;
			$return_arr_ok['actual_enddate'] = $enddate;
			$return_jion_ok=json_encode($return_arr_ok);
			//p($return_jion_ok);
			//修改返回参数的时间
			$upsql = "update tc_applyopen set applyopen_return_info = '".$return_jion_ok."' where applyopen_order_d_id = '".$order_d_id ."'";
			$this->db->query($upsql);
			//p($upsql);
			//修改订单明细的时间
			$upsql2 = "update tc_order_d set order_d_open_startdate = '".$startdate."',order_d_open_enddate = '".$enddate."' where order_d_id = '".$order_d_id ."'";
			$this->db->query($upsql2);
			//p($upsql2);
			//exit;
			$js++;
		}
		echo "修改".$js."条订单成功！";
	}

}




?>