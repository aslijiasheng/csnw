<?php
class Role extends WWW_Controller{
	public $menu1='role';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/role_model','role');
	}

	public function index(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('role_list', $user_auth['activity_auth_arr'])){
            $this->render('www/layouts/404');
		}else{
			$this->role->db->query('SET NAMES UTF8');
			if (!isset($_GET['page'])){
				$_GET['page']=1;
			}
			if (!isset($_GET['perNumber'])){
				$_GET['perNumber']=10;
			}
			$data['totalNumber'] = $this->role->countGetInfo(); //数据总数
			$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
			$data["labels"]=$this->role->attributeLabels();
			$data['listLayout']=$this->role->listLayout();
			$this->render('www/role/list',$data);
		}
	}
	public function ajax_select(){
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["role_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		if (isset($_GET['type_id'])){ 
			if($_GET['type_id']!="" and $_GET['type_id']!=0){
				$where['type_id']=$_GET['type_id']; 
			}
		}
		
		$data['listData']=$this->role->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);
		$data["labels"]=$this->role->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取
		
		

		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->role->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/role/ajax_select',$data);
	}
	public function add(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('role_add', $user_auth['activity_auth_arr'])){
            $this->render('www/layouts/404');
		}else{
			$this->role->db->query('SET NAMES UTF8');
			$data["labels"]=$this->role->attributeLabels();
			if(!empty($_POST)){
			if(isset($_GET['type_id'])){
	           $_POST['role']['type_id']=$_GET['type_id'];
	           $this->role->add($_POST['role']);
			    success('www/role?type_id='.$_GET['type_id'],'创建成功');
			}else{
			  $this->role->add($_POST['role']);
			  success('www/role','创建成功');
			}
				
			}
			$this->render('www/role/add',$data);
		}
		
	}
	public function view(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('role_view', $user_auth['activity_auth_arr'])){
            $this->render('www/layouts/404');
		}else{
			$this->role->db->query('SET NAMES UTF8');
	        $data["labels"]=$this->role->attributeLabels();
			$data['listLayout']=$this->role->id_aGetInfo($_GET['role_id']);
			$this->render('www/role/view',$data);
		}
		
	}
	public function update(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('role_view', $user_auth['activity_auth_arr'])){
            $this->render('www/layouts/404');
		}else{

			$this->role->db->query('SET NAMES UTF8');
			$data["labels"]=$this->role->attributeLabels();
			$data['id_aData']=$this->role->id_aGetInfo($_GET['role_id']);
			$data['listLayout']=$this->role->listLayout();
			if(!empty($_POST)){

			   
			    	//p($_POST);die;
	              	$this->role->update($_POST['role'],$_GET['role_id']);
				    success('www/role','更新成功');
			   
			}
			$this->render('www/role/update',$data);
		}
		
	}
	//分配角色
	public function assign(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('role_assign', $user_auth['activity_auth_arr'])){
            $this->render('www/layouts/404');
		}else{
			$data['choose_role_arr']=$this->db->where('user_id',$this->input->get('user_id'))->get('tc_role_user')->result_array();
			if(!empty($data['choose_role_arr'])){
				foreach($data['choose_role_arr'] as $role_arr){
					$data['role_arr'][]=$role_arr['role_id'];
				}
			}
			$this->load->model('www/user_model','user');
			$data['user_info']=$this->user->id_aGetInfo($_GET['user_id']);
			$data['listData']=$this->role->GetInfo();
			$this->render('www/role/assign',$data);
		}
	}
	public function doassign(){
		$arr['listData']=$this->db->get_where('tc_role_user',array('user_id'=>$this->input->get('user_id')))->result_array();
		if(!empty($arr['listData'])){
			$this->db->where('user_id',$this->input->get('user_id'))->delete('tc_role_user');
		}
		   if(!empty($_POST['role'])){
		   	   $data['user_id']=$_GET['user_id'];
			foreach ($_POST['role'] as $role_id) {
				$data['role_id']=$role_id;
				$this->db->insert('tc_role_user',$data);
			}	
		   }
		  
		$this->output->enable_profiler(TRUE);
		success('www/user/view?user_id='.$_GET['user_id'],'操作成功');
	}

	public function del(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('role_del', $user_auth['activity_auth_arr'])){
            $this->render('www/layouts/404');
		}else{
			$this->role->del($_GET['role_id']);
			success('www/role','删除成功');
		}
	}

	
}
?>