<?php
class User extends WWW_Controller{
	public $menu1='user';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/user_model','user');
	}

	public function add(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('user_add', $user_auth['activity_auth_arr'])){
            $this->load->view('www/layouts/operation_403');
		}else{
			$this->user->db->query('SET NAMES UTF8');
			$data["labels"]=$this->user->attributeLabels();
			$this->load->model('admin/enum_model','enum');$data["user_sex_enum"]=$this->enum->getlist(64);
			$this->load->model('admin/enum_model','enum');$data["user_duty_name_enum"]=$this->enum->getlist(69);
			$this->load->model('admin/enum_model','enum');$data["user_status_enum"]=$this->enum->getlist(70);
			if(!empty($_POST)){
			if(isset($_GET['type_id'])){
	           $_POST['user']['type_id']=$_GET['type_id'];
	           $this->user->add($_POST['user']);
			    success('www/user?type_id='.$_GET['type_id'],'创建成功');
			}else{
			  $this->user->add($_POST['user']);
			  success('www/user','创建成功');
			}

			}
			$this->render('www/user/add',$data);
		}

	}
	public function index(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('user_list', $user_auth['activity_auth_arr'])){
            $this->load->view('www/layouts/403');
		}else{
			$this->user->db->query('SET NAMES UTF8');
			if (!isset($_GET['page'])){
				$_GET['page']=1;
			}
			if (!isset($_GET['perNumber'])){
				$_GET['perNumber']=10;
			}
			$data['totalNumber'] = $this->user->countGetInfo(); //数据总数
			$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
			$data["labels"]=$this->user->attributeLabels();
			$data['listLayout']=$this->user->listLayout();
			$this->render('www/user/list',$data);
		}
	}
    public function del(){
    	$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('user_del', $user_auth['activity_auth_arr'])){
            $this->load->view('www/layouts/403');
		}else{
			$this->user->del($_GET['user_id']);
	   		success('www/user','删除成功');
		}
    }
	public function update(){

		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		//p($user_auth['activity_auth_arr']);
		if(!in_array('user_edit', $user_auth['activity_auth_arr'])){
            $this->load->view('www/layouts/403');
		}else{
			$this->user->db->query('SET NAMES UTF8');
			$data["labels"]=$this->user->attributeLabels();
			$data['id_aData']=$this->user->id_aGetInfo($_GET['user_id']);
			$data['listLayout']=$this->user->listLayout();
			$this->load->model('admin/enum_model','enum');
			$data["user_sex_enum"]=$this->enum->getlist(64);
			$this->load->model('admin/enum_model','enum');
			$data["user_duty_name_enum"]=$this->enum->getlist(69);
			$this->load->model('admin/enum_model','enum');
			$data["user_status_enum"]=$this->enum->getlist(70);
			if(!empty($_POST)){
				//p($_POST);die;
	              	$this->user->update($_POST['user'],$_GET['user_id']);
				    success('www/user','更新成功');
			}
			$this->render('www/user/update',$data);
		}

	}
	public function view(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('user_view', $user_auth['activity_auth_arr'])){
            $this->load->view('www/layouts/403');
		}else{
			$this->user->db->query('SET NAMES UTF8');
	        $data["labels"]=$this->user->attributeLabels();
			$data['listLayout']=$this->user->id_aGetInfo($_GET['user_id']);
			//$user_auth = $this->user->user_auth($_GET['user_id']);
			$this->render('www/user/view',$data);
		}
	}
	public function ajax_select(){
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["user_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		$data['listData']=$this->user->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);
		$data["labels"]=$this->user->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取



		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->user->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/user/ajax_select',$data);
	}
    public function ajax_list(){
       $this->user->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['listData']=$this->user->listGetInfo('',$_GET['page'],$_GET['perNumber']);
		$data['totalNumber'] = $this->user->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->user->attributeLabels();
		$data['listLayout']=$this->user->listLayout();
		$this->output->enable_profiler(false);
		$this->load->view('www/user/ajax_list',$data);
    }
	public function ajax_select_quote(){
		//p($_POST);
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["user_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		if(isset($_POST['rel_role_id'])){
			//有post表示有查询条件
			$rel_role_id = $_POST['rel_role_id'];
			$data['listData']=$this->user->roleGetInfo($rel_role_id,$like);
			$data['totalNumber'] = $this->user->countRole($rel_role_id,$like); //数据总数

		}else{
			$data['listData']=$this->user->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);
			$data['totalNumber'] = $this->user->countGetInfo($where,$like); //数据总数
			//p($data['listData']);
		}

		$data["labels"]=$this->user->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/user/ajax_select_quote',$data);
	}

	//配置user的额外信息
	public function add_other_info(){
		$data = $_GET;


		$this->render('www/user/add_other_info',$data);
	}
	public function do_add(){
		$imgname = $_FILES["thumb"]['name']; //获取上传的文件名称
		$filetype = pathinfo($imgname, PATHINFO_EXTENSION);//获取后缀
		$file_name = $this->fileupload();
		$file_name = 'upload/'.$file_name.'.'.$filetype;
		$arr = $this->db->get_where('tc_user_info',array('user_id'=>$_POST['user_id']))->row_array();
		if(!empty($arr)){
			$data['pic_path'] = $file_name;
			$this->db->where('user_id',$_POST['user_id'])->update('tc_user_info',$data);
		}else{
			$data['user_id'] = $_POST['user_id'];
			$data['pic_path'] = $file_name;
			$this->db->insert('tc_user_info',$data);
		}
		success('www/user','操作成功');
	}

}
?>