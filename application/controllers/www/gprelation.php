<?php
class Gprelation extends WWW_Controller{
	public $menu1='gprelation';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/gprelation_model','gprelation');
	}

	public function index(){
		$this->gprelation->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->gprelation->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->gprelation->attributeLabels();
		$data['listLayout']=$this->gprelation->listLayout();
		$this->render('www/gprelation/list',$data);
	}
	public function add(){
		$this->gprelation->db->query('SET NAMES UTF8');
		$data["labels"]=$this->gprelation->attributeLabels();
		
		
		
		
		
		
		
		
		
		if(!empty($_POST)){
		if(isset($_GET['type_id'])){
           $_POST['gprelation']['type_id']=$_GET['type_id'];
           $this->gprelation->add($_POST['gprelation']);
		    success('www/gprelation?type_id='.$_GET['type_id'],'创建成功');
		}else{
		  $this->gprelation->add($_POST['gprelation']);
		  success('www/gprelation','创建成功');
		}
			
		}
		$this->render('www/gprelation/add',$data);
	}
	public function update(){
		$this->gprelation->db->query('SET NAMES UTF8');
		$data["labels"]=$this->gprelation->attributeLabels();
		$data['id_aData']=$this->gprelation->id_aGetInfo($_GET['gprelation_id']);
		$data['listLayout']=$this->gprelation->listLayout();
		
		
		
		
		
		
		
		
		
		if(!empty($_POST)){
		    if(isset($_GET['type_id'])){
		      $_POST['gprelation']['type_id']=$_GET['type_id'];
		      $this->gprelation->update($_POST['gprelation'],$_GET['gprelation_id']);
			    success('www/gprelation?type_id='.$_GET['type_id'],'更新成功');
		    }else{
              	$this->gprelation->update($_POST['gprelation'],$_GET['gprelation_id']);
			    success('www/gprelation','更新成功');
		    }
		}
		$this->render('www/gprelation/update',$data);
	}

}
?>