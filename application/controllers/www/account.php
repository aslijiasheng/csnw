<?php

class Account extends WWW_Controller {

	public $menu1 = 'account';

	public function __construct() {
		parent::__construct();
		$this->load->model('www/account_model', 'account');
	}

	public function index() {
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
		if (!in_array('account_list', $user_auth['activity_auth_arr'])) {
			$this->load->view('www/layouts/operation_403');
			return;
		} else {
			$this->account->db->query('SET NAMES UTF8');
			if (!isset($_GET['page'])) {
				$_GET['page'] = 1;
			}
			if (!isset($_GET['perNumber'])) {
				$_GET['perNumber'] = 10;
			}
			//如果包含类型则生成一个关于类型的数组
			//$data['type_arr'];
			$data['type_arr']['1']["type_id"] = '1';
			$data['type_arr']['1']["type_name"] = '个人客户';
			if (isset($_GET['type_id'])) {
				if ($_GET['type_id'] == 1) {
					$this->menu1 = 'personal_account';
				}
			}
			$data['type_arr']['2']["type_id"] = '2';
			$data['type_arr']['2']["type_name"] = '企业客户';
			if (isset($_GET['type_id'])) {
				if ($_GET['type_id'] == 2) {
					$this->menu1 = 'entreprise_account';
				}
			}
			//专门给高级查询设计一个可供查询的数组
			$data['seniorquery_attr'] = array();
			//直接查询该对象的类型来获取相关数据
			$this->load->model("admin/attribute_model", 'attr');
			$this->attr->db->query("set names utf8");
			$obj_attr = $this->attr->check(1);
			foreach ($obj_attr as $k => $v) {
				$data['seniorquery_attr'][$k] = array(
					'name' => $v['attr_name'],
					'label' => $v['attr_label'],
					'attrtype' => $v['attr_type'],
				);
				//引用类型得多加几个参数
				if ($v['attr_type'] == 19) {
					$data['seniorquery_attr'][$k]['sub_attr'] = array(
						'url' => site_url('www/' . $v['attr_quote_id_arr']['obj_name'] . '/ajax_list'),
						'title' => $v['attr_quote_id_arr']['obj_label'],
						'quote_name' => $v['attr_quote_id_arr']['obj_name'],
					);
				}
				//枚举类型得多加几个参数
				if ($v['attr_type'] == 14 or $v['attr_type'] == 15) {
					//查询出关联枚举的内容
					$this->load->model('admin/enum_model', 'enum');
					$enum_arr = $this->enum->getlist($v['attr_id']);
					//循环去掉一些没必要的属性
					foreach ($enum_arr as $k2 => $v2) {
						unset($enum_arr[$k2]['enum_id']);
						unset($enum_arr[$k2]['attr_id']);
						unset($enum_arr[$k2]['disp_order']);
						unset($enum_arr[$k2]['is_default']);
						unset($enum_arr[$k2]['system_flag']);
					}
					$data['seniorquery_attr'][$k]['sub_attr'] = array(
						'enum' => $enum_arr,
					);
				}
			}
			//用于测试添加几条查询条件
			$data['seniorquery_attr'][] = array(
				'name' => 'int',
				'label' => '数值类型',
				'attrtype' => '5',
			);
			//p($data['seniorquery_attr']);exit;
			$data['totalNumber'] = $this->account->countGetInfo(); //数据总数
			$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
			$data["labels"] = $this->account->attributeLabels();
			$data['listLayout'] = $this->account->listLayout();
			$this->render('www/account/list', $data);
		}
	}

	//老查询接口格式改成新格式
	public function where_format($where) {
		//这里可以判断下是否已经有[where_all]属性！如果已有！证明本来就是新格式，不用转化
		if (isset($where['where_all'])) {
			return $where;
		} else {
			//首先处理select_json
			$new_where = $where;
			if (!isset($where['page'])) {
				$new_where['page'] = 1;
			}
			if (isset($where["select_json"]) and $where["select_json"] != "") {
				$select_arr = json_decode($where["select_json"], true);
				$new_where['select_arr'] = array(
					$select_arr['attr'] => $select_arr['value'],
				);
			} else {
				$new_where['select_arr'] = "";
			}

			//把条件组合起来主要是where、get_attr和select_arr
			//首先判断是否是高级查询！如果是高级查询则先处理高级查询
			if (isset($where["seniorquery"]) and $where["seniorquery"] != "") {
				//高级查询处理
				//高级查询rel里的数字的需要处理啊一下的！因为可能和别的有冲突
				foreach ($where["seniorquery"]['where'] as $sw_k => $sw_v) {
					$new_where["seniorquery"]['where']['sq' . $sw_k] = $sw_v;
					unset($new_where['seniorquery']['where'][$sw_k]); //去掉原值
					$new_where["seniorquery"]['rel'] = str_replace($sw_k, 'sq' . $sw_k, $new_where["seniorquery"]['rel']);
				}
				//p($where["seniorquery"]['where']);exit;
				//把条件组合起来主要是where、get_attr和select_arr
				//循环出get_attr
				$where_all = $new_where["seniorquery"]['where'];
				if (isset($new_where["seniorquery"]['rel']) and $new_where["seniorquery"]['rel'] != "") {
					$rel_all = "(" . $new_where["seniorquery"]['rel'] . ")";
				} else {
					$rel_all = "";
				}
			} else {
				//不是高级查询
				$where_all = $new_where['where'];
				if (isset($new_where['where_rel']) and $new_where['where_rel'] != "") {
					$rel_all = "(" . $new_where['where_rel'] . ")";
				} else {
					$rel_all = "";
				}
			}
			//p($where_all);
			//循环出get_attr
			if (isset($new_where['get_attr']) and $new_where['get_attr'] != "") {
				$i = 0;
				$ga_rel = "";
				foreach ($new_where['get_attr'] as $key => $value) {
					$i++;
					$where_all['ga' . $i] = array(
						'attr' => $key,
						'value' => $value,
						'action' => '=',
					);
					if ($ga_rel == "") {
						$ga_rel = 'ga' . $i;
					} else {
						$ga_rel = $ga_rel . ' and ga' . $i;
					}
				}
				if ($rel_all !== "") {
					$rel_all = $rel_all . " and (" . $ga_rel . ")";
				} else {
					$rel_all = "(" . $ga_rel . ")";
				}
			}

			if ($new_where['select_arr'] != "") {
				$i = 0;
				$sa_rel = "";
				foreach ($new_where['select_arr'] as $key => $value) {
					$i++;
					$where_all['sa' . $i] = array(
						'attr' => $key,
						'value' => $value,
						'action' => 'like',
					);
					if ($sa_rel == "") {
						$sa_rel = 'sa' . $i;
					} else {
						$sa_rel = $sa_rel . ' and sa' . $i;
					}
				}
				if ($rel_all != "") {
					$rel_all = $rel_all . " and (" . $sa_rel . ")";
				} else {
					$rel_all = "(" . $sa_rel . ")";
				}
			}
			$new_where_ok = array(
				"where_all" => $where_all,
				"rel_all" => $rel_all,
				"page" => $new_where['page'],
				"perNumber" => $new_where['perNumber'],
				"obj" => $new_where['obj'],
			);
			//p($new_where_ok );//exit;
			return $new_where_ok;
		}
	}

	public function ajax_select() {
		$where = $_POST;
		if (isset($where['seniorquery'])) {
			//循环1次，去掉所有attr为0的
			foreach ($where['seniorquery']['where'] as $k => $v) {
				if ($v['attr'] == '0') {
					unset($where['seniorquery']['where'][$k]);
				}
			}
		}
		$where['obj'] = 'account';
		$select_data = $this->where_format($where); //转化成api的格式查询
		$this->load->model('www/api_model', 'api');
		$listData = $this->api->select($select_data);
		//p($listData);
		if (isset($listData['res'])) {
			echo "查询失败！" . $listData['msg'];
			exit;
		}
		$data["labels"] = $this->account->attributeLabels();
		$data['listData'] = $listData['info'];
		$data['totalNumber'] = $listData['count']; //数据总数
		$data['sel_data'] = $listData['sel_data'];
		$data['page'] = 1; //默认第一页
		//如果包含对象类型，添加一个数组，用于视图获取
		$data['typelistlayout']['1'][] = 'account_name';
		$data['typelistlayout']['1'][] = 'account_account_shopexid';
		$data['typelistlayout']['1'][] = 'account_telephone';
		$data['typelistlayout']['1'][] = 'account_department';
		$data['typelistlayout']['2'][] = 'account_name';
		$data['typelistlayout']['2'][] = 'account_account_shopexid';
		$data['typelistlayout']['2'][] = 'account_telephone';
		$data['typelistlayout']['2'][] = 'account_email';
		$data['typelistlayout']['2'][] = 'account_department';
		$this->load->view('www/account/ajax_select', $data);
	}

	public function ajax_select2() {
		//p($this->session->userdata('activity_auth_arr'));
		p($_POST);

		if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
			$select_arr = json_decode($_POST["select_json"], true);
			$like["account_" . $select_arr['attr']] = $select_arr['value'];
		} else {
			//$select_arr = "";
			$like = "";
		}
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$where = "";
		if (isset($_GET['type_id'])) {
			if ($_GET['type_id'] != "" and $_GET['type_id'] != 0) {
				$where['type_id'] = $_GET['type_id'];
			}
		}

		$data['listData'] = $this->account->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
		$data["labels"] = $this->account->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取
		$data['typelistlayout']['1'][] = 'account_name';
		$data['typelistlayout']['1'][] = 'account_account_shopexid';
		$data['typelistlayout']['1'][] = 'account_telephone';
		$data['typelistlayout']['1'][] = 'account_department';
		$data['typelistlayout']['2'][] = 'account_name';
		$data['typelistlayout']['2'][] = 'account_account_shopexid';
		$data['typelistlayout']['2'][] = 'account_telephone';
		$data['typelistlayout']['2'][] = 'account_email';
		$data['typelistlayout']['2'][] = 'account_department';
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['totalNumber'] = $this->account->countGetInfo($where, $like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);

		$this->load->view('www/account/ajax_select', $data);
	}

	public function add() {
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
		if (!in_array('account_add', $user_auth['activity_auth_arr'])) {
			$this->render('www/layouts/403');
		} else {
			$this->account->db->query('SET NAMES UTF8');
			$data["labels"] = $this->account->attributeLabels();

			if (!empty($_POST)) {
				if (isset($_GET['type_id'])) {
					$_POST['account']['type_id'] = $_GET['type_id'];
					$this->account->add($_POST['account']);
					success('www/account?type_id=' . $_GET['type_id'], '创建成功');
				} else {
					$this->account->add($_POST['account']);
					success('www/account', '创建成功');
				}
			}
			$this->render('www/account/add', $data);
		}
	}

	public function update() {
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
		if (!in_array('account_update', $user_auth['activity_auth_arr'])) {
			$this->render('www/layouts/403');
		} else {
			$this->account->db->query('SET NAMES UTF8');
			$data["labels"] = $this->account->attributeLabels();
			$data['id_aData'] = $this->account->id_aGetInfo($_GET['account_id']);
			$data['listLayout'] = $this->account->listLayout();

			if (!empty($_POST)) {
				$this->load->model('www/system_log_model', 'system_log');
				$system_log_note = '原始数据：' . print_r($this->account->id_aGetInfo($_GET['account_id']), true);
				if (isset($_GET['type_id'])) {
					$_POST['account']['type_id'] = $_GET['type_id'];
					$this->account->update($_POST['account'], $_GET['account_id']);
					// 添加修改日志
					$log_data = array(
						'system_log_addtime' => date('Y-m-d H:i:s', time()),
						'system_log_user_id' => $this->session->userdata('user_id'),
						'system_log_ip' => $this->get_client_ip(),
						'system_log_param' => '修改数据：' . print_r($_POST['account'], true),
						'system_log_operation' => '修改客户信息',
						'system_log_module' => 'Account',
						'system_log_module_id' => $_GET['account_id'],
						'system_log_note' => $system_log_note
					);
					$this->system_log->add($log_data);
					success('www/account?type_id=' . $_GET['type_id'], '更新成功');
				} else {
					$system_log_note = '原始数据：' . print_r($this->account->id_aGetInfo($_GET['account_id']), true);
					$this->account->update($_POST['account'], $_GET['account_id']);
					// 添加修改日志
					$log_data = array(
						'system_log_addtime' => date('Y-m-d H:i:s', time()),
						'system_log_user_id' => $this->session->userdata('user_id'),
						'system_log_ip' => $this->get_client_ip(),
						'system_log_param' => '修改数据：' . print_r($_POST['account'], true),
						'system_log_operation' => '修改客户信息',
						'system_log_module' => 'Account',
						'system_log_module_id' => $_GET['account_id'],
						'system_log_note' => $system_log_note
					);
					$this->system_log->add($log_data);
					success('www/account', '更新成功');
				}
			}
			$this->render('www/account/update', $data);
		}
	}

	public function view() {
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
		$this->account->db->query('SET NAMES UTF8');
		if (!in_array('account_view', $user_auth['activity_auth_arr'])) {
			$this->render('www/layouts/403');
		} else {
			$data["labels"] = $this->account->attributeLabels();
			$data['listLayout'] = $this->account->id_aGetInfo($_GET['account_id']);
			//关联订单
			$data['relOrder'] = $this->account->getRelOrder($_GET['account_id']);
//			p($data['relOrder']);
//			exit;
			$this->render('www/account/view', $data);
		}
	}

	public function del() {
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
		if (!in_array('account_del', $user['activity_auth_arr'])) {
			$this->render('www/layouts/403');
		} else {
			$this->account->del($_GET['account_id']);
			success('www/account', '删除成功');
		}
	}

	public function ajax_list() {
		$this->account->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['listData'] = $this->account->listGetInfo('', $_GET['page'], $_GET['perNumber']);
		$data['totalNumber'] = $this->account->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"] = $this->account->attributeLabels();
		$data['listLayout'] = $this->account->listLayout();
		$this->output->enable_profiler(false);
		$this->load->view('www/account/ajax_list', $data);
	}

	public function ajax_select_quote() {
		if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
			$select_arr = json_decode($_POST["select_json"], true);
			$like["account_" . $select_arr['attr']] = $select_arr['value'];
		} else {
			//$select_arr = "";
			$like = "";
		}
		if (!isset($_GET['page'])) {
			$_GET['page'] = 1;
		}
		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$where = "";

		$data['listData'] = $this->account->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
		$data["labels"] = $this->account->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取




		$data['typelistlayout']['1'][] = 'account_name';

		$data['typelistlayout']['1'][] = 'account_account_shopexid';

		$data['typelistlayout']['1'][] = 'account_telephone';

		$data['typelistlayout']['1'][] = 'account_department';



		$data['typelistlayout']['2'][] = 'account_name';

		$data['typelistlayout']['2'][] = 'account_account_shopexid';

		$data['typelistlayout']['2'][] = 'account_telephone';

		$data['typelistlayout']['2'][] = 'account_email';

		$data['typelistlayout']['2'][] = 'account_department';




		if (!isset($_GET['perNumber'])) {
			$_GET['perNumber'] = 10;
		}
		$data['totalNumber'] = $this->account->countGetInfo($where, $like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/account/ajax_select_quote', $data);
	}

	public function ajax_seniorquery() {
		$data['seniorquery_attr'] = array(
			array(
				'name' => 'name',
				'label' => '名称',
				'attrtype' => 1,
			),
			array(
				'name' => 'leader',
				'label' => '负责人',
				'attrtype' => 1,
			),
		);
		$this->load->view('www/account/ajax_seniorquery', $data);
	}

}

?>