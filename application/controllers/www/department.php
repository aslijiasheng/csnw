<?php
class Department extends WWW_Controller{
	public $menu1='department';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/department_model','department');
	}
	public function index(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('department_list', $user_auth['activity_auth_arr'])){
            $this->load->view('www/layouts/operation_403');
		}else{
			$this->department->db->query('SET NAMES UTF8');
			if (!isset($_GET['page'])){
				$_GET['page']=1;
			}
			if (!isset($_GET['perNumber'])){
				$_GET['perNumber']=10;
			}
			$data['totalNumber'] = $this->department->countGetInfo(); //数据总数
			$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
			$data["labels"]=$this->department->attributeLabels();
			$data['listLayout']=$this->department->listLayout();
			$this->render('www/department/list',$data);
		}
	}
	public function add(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('department_add', $user_auth['activity_auth_arr'])){
            $this->load->view('www/layouts/403');
		}else{
            $this->department->db->query('SET NAMES UTF8');
			$data["labels"]=$this->department->attributeLabels();
			if(!empty($_POST)){
			  $insert_id=$this->department->add($_POST['department']);
			  //echo $insert_id;die;
			  if(!empty($_POST['department']['department_uid'])){

			  	$u_department=$this->db->get_where('tc_department',array('department_id'=>$_POST['department']['department_uid']))->result_array();
			  	//p($u_department);
			  	$u_department_path=$u_department[0]['department_treepath'];
			  	$len=explode(',',$u_department_path);
			  	$len=count($len);
			  	$where['department_treelevel']=$len;
			  	if($insert_id<10){
			  	  $u_department_path.=",000".$insert_id;
			  	}else if($insert_id<100){
			  	  $u_department_path.=",00".$insert_id;
			  	}else{
			  	   $u_department_path.=",0".$insert_id;
			  	}
			  	$where['department_treepath']=$u_department_path;
			  	$this->db->where('department_id',$insert_id)->update('tc_department',$where);
			  }else{
			  	$where['department_treelevel']=1;
			  	if($insert_id<10){
			  		$where['department_treepath']="0000,000".$insert_id;
			  	}else if($insert_id<100){
			  		$where['department_treepath']="0000,00".$insert_id;
			  	}else{
			  		$where['department_treepath']="0000,0".$insert_id;
			  	}

			  	$this->db->where('department_id',$insert_id)->update('tc_department',$where);
			  }
			  success('www/department','创建成功');
			}
			$this->render('www/department/add',$data);
		}

	}
	public function update(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('department_edit', $user_auth['activity_auth_arr'])){
            $this->load->view('www/layouts/403');
		}else{
			$this->department->db->query('SET NAMES UTF8');
			$data["labels"]=$this->department->attributeLabels();
			$data['id_aData']=$this->department->id_aGetInfo($_GET['department_id']);
			$data['listLayout']=$this->department->listLayout();

			if(!empty($_POST)){
	              	$this->department->update($_POST['department'],$_GET['department_id']);
				    success('www/department','更新成功');
			}
			$this->render('www/department/update',$data);
		}
	}
	public function view(){
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('department_view', $user_auth['activity_auth_arr'])){
            $this->load->view('www/layouts/403');
		}else{
			$this->department->db->query('SET NAMES UTF8');
	        $data["labels"]=$this->department->attributeLabels();
			$data['listLayout']=$this->department->id_aGetInfo($_GET['department_id']);
			$this->render('www/department/view',$data);
		}
	}
    public function del(){
       $user_auth = $this->user->user_auth($this->session->userdata('user_id'));//判断访问权限
		if(!in_array('department_del', $user_auth['activity_auth_arr'])){
            $this->load->view('www/layouts/403');
		}else{
			 $this->department->del($_GET['department_id']);
	   		success('www/department','删除成功');
		}
    }
	public function ajax_select(){
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["department_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		if (isset($_GET['type_id'])){
			if($_GET['type_id']!="" and $_GET['type_id']!=0){
				$where['type_id']=$_GET['type_id'];
			}
		}
		$data['listData']=$this->department->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);

		$data["labels"]=$this->department->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取



		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->department->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/department/ajax_select',$data);
	}
    public function ajax_list(){
       $this->department->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['listData']=$this->department->listGetInfo('',$_GET['page'],$_GET['perNumber']);
		$data['totalNumber'] = $this->department->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->department->attributeLabels();
		$data['listLayout']=$this->department->listLayout();
		$this->output->enable_profiler(false);
		$this->load->view('www/department/ajax_list',$data);
    }
	
	public function ajax_select_quote(){
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["department_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";

		$data['listData']=$this->department->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);
		for ($i=0;$i<count($data['listData']);$i++) {
			$tem=array();
			for($j=$i+1;$j<count($data['listData']);$j++){
				if(strcmp($data['listData'][$j]['department_treepath'],$data['listData'][$i]['department_treepath'])<0){
                   $tem=$data['listData'][$j];
                   $data['listData'][$j]=$data['listData'][$i];
                   $data['listData'][$i]=$tem;
				}
			}
		}
		$data["labels"]=$this->department->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取



		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->department->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/department/ajax_select_quote',$data);
	}

}
?>