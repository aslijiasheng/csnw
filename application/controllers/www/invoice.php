<?php
class Invoice extends WWW_Controller{
	public $menu1='invoice';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/invoice_model','invoice');
		$this->invoice->db->query('SET NAMES UTF8');
		$this->load->model('www/system_log_model','system_log');
		$this->system_log->db->query('SET NAMES UTF8');
		$this->load->model('www/message_model','message');
		$this->message->db->query('SET NAMES UTF8');
		$this->load->model('www/message_remind_model','message_remind');
		$this->load->model('www/order_model','order');
		$this->db->query('SET NAMES UTF8');
	}

	public function index(){
		$this->invoice->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data = $this->get_data_auth('sale_order');
		$data['totalNumber'] = $this->invoice->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->invoice->attributeLabels();
		$data['listLayout']=$this->invoice->listLayout();
		$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
		if(in_array('invoice_list',$user_auth['activity_auth_arr'])){
			$this->render('www/invoice/list',$data);
		}else{
			$this->load->view('www/layouts/operation_403');
		}
	}
	public function add(){
		$this->invoice->db->query('SET NAMES UTF8');
		$data["labels"]=$this->invoice->attributeLabels();
		$this->load->model('admin/enum_model','enum');
		$data["invoice_type_enum"]=$this->enum->getlist(155);
		$this->load->model('admin/enum_model','enum');
		$data["invoice_customer_type_enum"]=$this->enum->getlist(157);

		$this->load->model('admin/enum_model','enum');
		$data["invoice_content_class_enum"]=$this->enum->getlist(428);


		$this->load->model('admin/enum_model','enum');
		$data["invoice_other_content_enum"]=$this->enum->getlist(429);
		
		//新增所属子公司 2014/08/25
		$this->load->model('admin/enum_model','enum');
		$data["invoice_subsidiary_enum"]=$this->enum->getlist(630);
		//新增所属子公司 2014/08/25
		$this->load->model('www/order_model','order');
		$data["order_data"]=$this->order->id_aGetInfo($_GET['order_id']);
		//p($data["order_data"]);
		if(!empty($_POST)){
		if(isset($_GET['type_id'])){
           $_POST['invoice']['type_id']=$_GET['type_id'];
           $this->invoice->add($_POST['invoice']);
		    success('www/invoice?type_id='.$_GET['type_id'],'创建成功');
		}else{
		  $this->invoice->add($_POST['invoice']);
		  success('www/invoice','创建成功');
		}

		}
		$this->load->view('www/invoice/add',$data);
	}


	//老查询接口格式改成新格式
	public function where_format($where){
		//这里可以判断下是否已经有[where_all]属性！如果已有！证明本来就是新格式，不用转化
		if(isset($where['where_all'])){
			return $where;
		}else{
			//首先处理select_json
			$new_where=$where;
			if (isset($where["select_json"]) and $where["select_json"]!=""){
				$select_arr = json_decode($where["select_json"],true);
				$new_where['select_arr']=array(
					$select_arr['attr']=>$select_arr['value'],
				);
			}else{
				$new_where['select_arr']="";
			}

			//把条件组合起来主要是where、get_attr和select_arr
			//循环出get_attr
			$where_all=$new_where['where'];
			if(isset($new_where['where_rel']) and $new_where['where_rel']!=""){
				$rel_all = "(".$new_where['where_rel'].")";
			}else{
				$rel_all = "";
			}

			if(isset($new_where['get_attr']) and $new_where['get_attr']!=""){
				$i=0;
				$ga_rel="";
				foreach ($new_where['get_attr'] as $key=>$value){
					$i++;
					$where_all['ga'.$i]=array(
						'attr'=>$key,
						'value'=>$value,
						'action'=>'=',
					);
					if($ga_rel==""){
						$ga_rel='ga'.$i;
					}else{
						$ga_rel=$ga_rel.' and ga'.$i;
					}
				}
				if($rel_all!==""){
					$rel_all=$rel_all." and (".$ga_rel.")";
				}else{
					$rel_all="(".$ga_rel.")";
				}

			}

			if ($new_where['select_arr']!=""){
				$i=0;
				$sa_rel="";
				foreach ($new_where['select_arr'] as $key=>$value){
					$i++;
					$where_all['sa'.$i]=array(
						'attr'=>$key,
						'value'=>$value,
						'action'=>'like',
					);
					if($sa_rel==""){
						$sa_rel='sa'.$i;
					}else{
						$sa_rel=$sa_rel.' and sa'.$i;
					}
				}
				if($rel_all!=""){
					$rel_all=$rel_all." and (".$sa_rel.")";
				}else{
					$rel_all="(".$sa_rel.")";
				}

			}
			$new_where_ok = array(
				"where_all"=>$where_all,
				"rel_all"=>$rel_all,
				"page"=>$new_where['page'],
				"perNumber"=>$new_where['perNumber'],
				"obj"=>$new_where['obj'],
			);
			return $new_where_ok;
		}
	}

	//用新接口查询
	public function ajax_select(){
		$post = $_POST;
		$post['obj'] = 'invoice';
		$where = $this->where_format($post);
		//p($where);
		//exit;
		$this->load->model('www/api_model','api');
		$listData = $this->api->select($where);
		//p($listData);
		$data["labels"]=$this->invoice->attributeLabels();
		$data['listData']=$listData['info'];
		$data['totalNumber'] = $listData['count']; //数据总数
		$data['sel_data']=$listData['sel_data'];
		$data['perNumber'] = $_POST['perNumber']; //每页显示的记录数
		$data['page'] = $_POST['page'];//当前页数
		$this->load->view('www/invoice/ajax_select',$data);
	}

	/*
	public function ajax_select(){
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["invoice_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		if (isset($_GET['type_id'])){
			if($_GET['type_id']!="" and $_GET['type_id']!=0){
				$where['type_id']=$_GET['type_id'];
			}
		}

		$data['listData']=$this->invoice->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);
		$data["labels"]=$this->invoice->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取




			$data['typelistlayout']['11'][]='invoice_memo';

			$data['typelistlayout']['11'][]='invoice_addtime';

			$data['typelistlayout']['11'][]='invoice_untime';

			$data['typelistlayout']['11'][]='invoice_amount';

			$data['typelistlayout']['11'][]='invoice_product_id';

			$data['typelistlayout']['11'][]='invoice_invoice_value';

			$data['typelistlayout']['11'][]='invoice_content';

			$data['typelistlayout']['11'][]='invoice_bank';

			$data['typelistlayout']['11'][]='invoice_account';

			$data['typelistlayout']['11'][]='invoice_tel';

			$data['typelistlayout']['11'][]='invoice_address';

			$data['typelistlayout']['11'][]='invoice_title';

			$data['typelistlayout']['11'][]='invoice_customer_type';

			$data['typelistlayout']['11'][]='invoice_invoice_no';

			$data['typelistlayout']['11'][]='invoice_type';

			$data['typelistlayout']['11'][]='invoice_sale_id';

			$data['typelistlayout']['11'][]='invoice_order_id';



			$data['typelistlayout']['12'][]='invoice_memo';

			$data['typelistlayout']['12'][]='invoice_addtime';

			$data['typelistlayout']['12'][]='invoice_untime';

			$data['typelistlayout']['12'][]='invoice_amount';

			$data['typelistlayout']['12'][]='invoice_product_id';

			$data['typelistlayout']['12'][]='invoice_invoice_value';

			$data['typelistlayout']['12'][]='invoice_content';

			$data['typelistlayout']['12'][]='invoice_bank';

			$data['typelistlayout']['12'][]='invoice_account';

			$data['typelistlayout']['12'][]='invoice_tel';

			$data['typelistlayout']['12'][]='invoice_address';

			$data['typelistlayout']['12'][]='invoice_title';

			$data['typelistlayout']['12'][]='invoice_customer_type';

			$data['typelistlayout']['12'][]='invoice_invoice_no';

			$data['typelistlayout']['12'][]='invoice_type';

			$data['typelistlayout']['12'][]='invoice_sale_id';

			$data['typelistlayout']['12'][]='invoice_order_id';
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->invoice->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/invoice/ajax_select',$data);
	}
	*/
	public function view(){
		$this->invoice->db->query('SET NAMES UTF8');
        $data["labels"]=$this->invoice->attributeLabels();
		$data['listLayout']=$this->invoice->id_aGetInfo($_GET['invoice_id']);
		$this->render('www/invoice/view',$data);
	}
	public function update(){
		$this->invoice->db->query('SET NAMES UTF8');
		$data["labels"]=$this->invoice->attributeLabels();
		$this->load->model('admin/enum_model','enum');$data["invoice_type_enum"]=$this->enum->getlist(155);
		$this->load->model('admin/enum_model','enum');$data["invoice_customer_type_enum"]=$this->enum->getlist(157);

		$this->load->model('admin/enum_model','enum');$data["invoice_content_class_enum"]=$this->enum->getlist(428);


		$this->load->model('admin/enum_model','enum');$data["invoice_other_content_enum"]=$this->enum->getlist(429);

		if(!empty($_POST)){
		if(isset($_GET['type_id'])){
           $_POST['invoice']['type_id']=$_GET['type_id'];
           $this->invoice->add($_POST['invoice']);
		    success('www/invoice?type_id='.$_GET['type_id'],'创建成功');
		}else{
		  $this->invoice->add($_POST['invoice']);
		  success('www/invoice','创建成功');
		}

		}
		$this->load->view('www/invoice/update',$data);
	}
    public function del(){
       $this->invoice->del($_GET['invoice_id']);
	   success('www/invoice','删除成功');
    }
   //ajax 新增
    public function ajax_add(){
    	//p($_GET);die;
    	$this->invoice->db->query('SET NAMES UTF8');
    	if(isset($_GET['order_id'])){
    		$this->load->model('www/order_model','order');
			$data["order_data"]=$this->order->id_aGetInfo($_GET['order_id']);
			//p($data); [order_amount] => 791050.000
    		$data['max_invoice_amount'] = 0;//入款项金额或订单金额
			if($_GET['type']==1001 or $_GET['type']==1003 ){
    			//$this->load->model('www/salespay_model',"salespay");
    			 $arr["salespay_data"] = $this->db->get_where('tc_salespay',array("salespay_order_id"=>$_GET['order_id'],"salespay_status"=>1002))->result_array();
				//p($arr["salespay_data"]);
    			 foreach ($arr["salespay_data"] as $key => $value) {
    			 	$data['max_invoice_amount'] += floatval($value['salespay_pay_amount']);
    			 }
				// p($data['max_invoice_amount']);
    			 if($data['max_invoice_amount']==0){
    			 	$data['max_invoice_amount']=$data['order_data']['order_amount'];
    			 }
				// p($data['max_invoice_amount']);//[order_amount] => 791050.000
    		}else{
               $data['max_invoice_amount'] = $data['order_data']['order_amount'];
    		}
            //echo "入款项:".$data['max_invoice_amount']."<br>";

			$where=array('invoice_order_id'=>$_GET['order_id']);
			$data['invoice_data'] = $this->invoice->GetInfo($where);
			//p($this->db->last_query()); SELECT * FROM (`tc_invoice`) WHERE `invoice_order_id` =  '6880'
			$data['invoice_value'] = 0;//已开票金额
			$data['odd_invoice_value'] = 0;//剩余开票金额
			
			 if(!empty($data['invoice_data'])){
			 	//p($data['invoice_data']);
			 	foreach ($data['invoice_data'] as $key => $value) {
			 		//echo $value['invoice_uninvoice_status'];
			 		 if($value['invoice_amount']!='' && $value['invoice_uninvoice_status']!=1003 && $value['invoice_uninvoice_status']!=1004 && $value['invoice_status']!=1007){
			 			$data['invoice_value'] += $value['invoice_amount'];
			 		}else if($value['invoice_amount']!='' && $value['invoice_uninvoice_status'] == 1004){
			 			//p(111);
			 			$data['invoice_value'] -= $value['invoice_amount'];
			 		}else{
                          continue;
			 		 }
			 	}
			 	//p($data['invoice_value']);
			 }
			 // echo '乙开票金额：'.$data['invoice_value']."<br>";
			  $data['odd_invoice_value'] = $data['max_invoice_amount']-$data['invoice_value'];
			 // echo '可开票金额:'.$data['odd_invoice_value']."<br>";

			$this->load->model('www/account_model','account');
			$data['account_data'] = $this->account->id_aGetInfo($data["order_data"]['order_account']);

			// $data['order_d_data'] = $this->db->get_where('tc_order_d',array('order_d_order_id'=>$data['order_data']['order_id']))->result_array();
			//p($data['order_d_data']);
			$this->load->model('admin/enum_model','enum');
			$data["invoice_content_class_enum"]=$this->enum->getlist(428);
			$this->load->model('admin/enum_model','enum');
			$data["invoice_other_content_enum"]=$this->enum->getlist(429);
			//新增所属子公司 2014/08/25
			$this->load->model('admin/enum_model','enum');
			$data["invoice_subsidiary_enum"]=$this->enum->getlist(630);
			//新增所属子公司 2014/08/25
			if($_GET['type']==1001 or $_GET['type']==1003){
				$this->load->view('www/invoice/ajax_add',$data);
			}else{
				$this->load->view('www/invoice/ajax_add_2',$data);
			}

		}
	}
    public function ajax_add_post(){
		if($_POST){
			//p($_POST);die;
			$data['invoice_amount'] =0;

			$data['invoice_content'] = $_POST['invoice'];
			if(isset($_POST['invoice']['invoice_reviewer'])){
				$data['invoice_reviewer'] = $_POST['invoice']['invoice_reviewer'];
				//$data['invoice_content']['invoice_reviewer'] = $_POST['new_invoice']['invoice_reviewer'];
			}else{
				$data['invoice_reviewer'] = $_POST['order_finance'];
			}
			$data['invoice_content']['department'] = $_POST['order_finance_department'];
			//p($data['invoice_content']);
			if(isset($_POST['service_select'])){
              $data['invoice_content']['service_select'] = $_POST['service_select'];
            }
            if(isset($_POST['net_service_select'])){
            	$data['invoice_content']['net_service_select'] = $_POST['net_service_select'];
            }

			$data['invoice_content'] = json_encode($data['invoice_content']);
			if($_POST['invoice']['invoice_content_class']=='1001'){
				//p($_POST['invoice']['invoice_price']);
				// foreach($_POST['invoice']['goods_name'] as $key=>$value){
				// 	$data['invoice_content'][$value] = $_POST['invoice']['invoice_price'][$key];
				// }

				//

                // p($data['invoice_content']);
				//$data['invoice_content'] = $_POST['invoice'][]
			  if(isset($_POST['invoice']['invoice_price'])){
					foreach ($_POST['invoice']['invoice_price'] as $key => $value) {
					  	$data['invoice_amount'] += floatval($value);
				    }
			  }
			 // p($data['invoice_amount']);
			}else{
			    foreach ($_POST['invoice']['other_price'] as $key => $value) {

				  	$data['invoice_amount'] += floatval($value);

			    }

		    }
		    // $data['invoice_content']['invoice_content_class'] = $_POST['invoice']['invoice_content_class'];
		    // $data['invoice_content']['invoice_value'] = floatval($_POST['invoice_value']);
		    // $data['invoice_content']['odd_invoice_value']= floatval($_POST['odd_invoice_value']);
		    //p($data['invoice_content']);
		    if(isset($_POST['order_type'])){
				$_POST['invoice']['invoice_type']='1002';
			}
			$w1 = $data['invoice_amount'] * 1000;
			$w2 = $_POST['invoice']['odd_invoice_value'] * 1000;
			if($w1>$w2){ echo 2; }else{
				foreach ($_POST['invoice'] as $key => $value) {
				if(!in_array($key,array("invoice_value","service_charge","net_service_charge","odd_invoice_value","invoice_other_content","invoice_content_class","other_price","goods_disc","invoice_price","goods_name","invoice_reviewer"))){
					$data[$key] = $value;
				}
			}


			if($_POST['invoice']['invoice_type']=='1001'){
				if($_POST['invoice_type_1']['invoice_customer_type']=='1002'){
                   foreach($_POST['customer_1'] as $key=>$value){
                   	$data[$key] = $value;
                   }
				}else{
                   foreach($_POST['customer_2'] as $key=>$value){
                   	$data[$key] = $value;
                   }
				}
			}else{

				foreach($_POST['invoice_type_2'] as $key=>$value){
               		$data[$key] = $value;
                }

			}

			$data['invoice_order_id'] = $_POST['invoice']['invoice_order_id'];
			$data['invoice_kp_type'] = $_GET['type'];
			if($_GET['type'] == 1003) {$data['invoice_status'] = 1002;$data['invoice_do_time']=date('Y-m-d',time());}else if($_GET['type'] == 1001){$data['invoice_status'] = 1001;}else{$data['invoice_status'] = 1004;}
			if(isset($_POST['invoice_type_1'])){
			  $data['invoice_customer_type'] = $_POST['invoice_type_1']['invoice_customer_type'];
			}
			//$data['invoice_content'] = json_encode($data['invoice_content']);
			//p($data);
			//$data['invoice_amount'] = $_POST['invoice'][1]['id_ammount'];
			// $salespay_data = $_POST['salespay'];
			// $salespay_data['salespay_pay_info'] = json_encode($_POST['payinfo']);

			$data['invoice_addtime'] = date("Y-m-d H:i:s",time());
			if(!$this->invoice->checkAmount($data)){
				die('开票金额不得大于可开票金额，也有可能是精度问题');
			}
			$this->invoice->add($data);
			$invoice_id = $this->db->insert_id();
			$log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_module'=>'Order',
				'system_log_module_id'=>$data['invoice_order_id']
				);
			if(isset($data['invoice_memo'])) $log_data['system_log_note'] = $data['invoice_memo'];
			if($data['invoice_kp_type']==1001){
				$log_data['system_log_operation'] = '申请开票';
				$message_data = array(
					'message_owner'=>$_POST['order_finance'],
					'message_module'=>'invoice',
					'message_module_id'=>$invoice_id,
					'message_type'=>1002,
					'message_url'=>'www/invoice/confirm_invoice',
					'message_department'=>$_POST['order_finance_department']
				);

			}else if($data['invoice_kp_type']==1002){
				$log_data['system_log_operation'] = '申请预开票';
				//p($_POST['invoice']['invoice_reviewer']);die;
				$this->load->model('www/user_model','user');
				$reviewer_info = $this->user->id_aGetInfo($_POST['invoice']['invoice_reviewer']);
				//p($reviewer_info);die;
				$message_data = array(
					'message_owner'=>$_POST['invoice']['invoice_reviewer'],
					'message_module'=>'invoice',
					'message_module_id'=>$invoice_id,
					'message_type'=>1004,
					'message_department'=>$reviewer_info['user_department'],
					'message_url'=>'www/invoice/verify_invoice'
				);
			}/*else{
				$log_data['system_log_operation'] = '财务自主开票';
				$message_data = array(
					'message_owner'=>$_POST['order_finance'],
					'message_module'=>'invoice',
					'message_module_id'=>$invoice_id,
					'message_type'=>1002,
					'message_department'=>$_POST['order_finance_department'],
					'message_url'=>'www/invoice/confirm_invoice'
				);
			}*/
			$this->system_log->add($log_data);
			$this->message->add($message_data);
				//添加提醒信息
			$mess_remind_data = array(
				'message_remind_role_id' =>3,
				'message_remind_status'=>1001
			);
			$this->message_remind->update($mess_remind_data,$mess_remind_data['message_remind_role_id']);
			// $this->output->enable_profiler(TRUE);
			echo 1;
			}

		}else{
			echo '失败';
		}
	}

	public function ajax_edit(){
		if(isset($_GET['invoice_id'])){
			$data["labels"]=$this->invoice->attributeLabels();

			$where=array(
				'invoice_id'=>$_GET['invoice_id']
			);
			$data['listData']=$this->invoice->GetInfo($where);

			$data['listData'] = $data['listData'][0];
			//p($data['listData']);
			//p(get_object_vars(json_decode($data['listData']['invoice_content'])));
			$data['listData']['invoice_content'] = object_array(json_decode($data['listData']['invoice_content']));
			//p($data['listData']['invoice_content']);
			$this->load->model('www/order_model','order');
			$data["order_data"]=$this->order->id_aGetInfo($data['listData']['invoice_order_id']);
			$this->load->model('admin/enum_model','enum');$data["invoice_content_class_enum"]=$this->enum->getlist(428);
			$this->load->model('admin/enum_model','enum');$data["invoice_other_content_enum"]=$this->enum->getlist(429);
			$this->load->model('admin/enum_model','enum');$data["invoice_type_enum"]=$this->enum->getlist(155);
			$this->load->model('admin/enum_model','enum');$data["invoice_customer_type_enum"]=$this->enum->getlist(157);
			//所属子公司
			$this->load->model('admin/enum_model','enum');$data["invoice_subsidiary_enum"]=$this->enum->getlist(630);
			// $data['order_d_data'] = $this->db->get_where('tc_order_d',array('order_d_order_id'=>$data['order_data']['order_id']))->result_array();
			//p($data['order_data']);
			$this->load->view('www/invoice/ajax_edit',$data);
		}else{
			echo "不能没有ID";
		}
	}


      public function detailed_list_view(){
		$this->invoice->db->query('SET NAMES UTF8');
		if(isset($_GET['order_id'])){
			//p($_GET['order_id']);exit;
			$where=array(
				'invoice_order_id'=>$_GET['order_id']
			);
			$data['listData']=$this->invoice->GetInfo($where);
			$data["labels"]=$this->invoice->attributeLabels();
			$data['listLayout']=$this->invoice->listLayout();
			//p($data['listData']);
			$this->output->enable_profiler(false);
			$this->load->view('www/invoice/detailed_list_view',$data);
		}else{
			echo "不能没有ID";
		}
    }

    //ajax调用的编辑
	public function ajax_update_post(){
		if($_POST){
			$invoice_data = $_POST['invoice'];
			//p($_POST);die;
			//查当前票据的状态信息
			$inv_info = $this->db->select('invoice_do_status,invoice_process_status')->from('tc_invoice')->where('invoice_id',$_POST['invoice']['invoice_id'])->get()->row_array();

			$data['invoice_amount'] =0;

			$data['invoice_content'] = $_POST['invoice'];
			if(isset($_POST['invoice']['invoice_reviewer'])){
				$data['invoice_content']['invoice_reviewer_id'] = $_POST['invoice']['invoice_reviewer'];
				$data['invoice_content']['invoice_reviewer'] = $_POST['new_invoice']['invoice_reviewer'];
			}else{
				$data['invoice_content']['invoice_reviewer_id'] = $_POST['order_finance'];
			}
			$data['invoice_content']['department'] = $_POST['order_finance_department'];
			if(isset($_POST['service_select'])){
              $data['invoice_content']['service_select'] = $_POST['service_select'];
            }
            if(isset($_POST['net_service_select'])){
            	$data['invoice_content']['net_service_select'] = $_POST['net_service_select'];
            }

			$data['invoice_content'] = json_encode($data['invoice_content']);
			if($_POST['invoice']['invoice_content_class']=='1001'){

			  if(isset($_POST['invoice']['invoice_price'])){
					foreach ($_POST['invoice']['invoice_price'] as $key => $value) {
					  	$data['invoice_amount'] += floatval($value);
				    }
			  }
			 // p($data['invoice_amount']);
			}else{
			    foreach ($_POST['invoice']['other_price'] as $key => $value) {

				  	$data['invoice_amount'] += floatval($value);

			    }

		    }


			if($data['invoice_amount']>$_POST['invoice']['odd_invoice_value']){ echo 2; }else{
				foreach ($_POST['invoice'] as $key => $value) {
				if(!in_array($key,array("invoice_value","service_charge","net_service_charge","odd_invoice_value","invoice_other_content","invoice_content_class","other_price","goods_disc","invoice_price","goods_name","invoice_reviewer"))){
					$data[$key] = $value;
				}
			}
			if($_POST['invoice']['invoice_type']=='1001'){
				if($_POST['invoice_type_1']['invoice_customer_type']=='1002'){
                   foreach($_POST['customer_1'] as $key=>$value){
                   	$data[$key] = $value;
                   }
				}else{
                   foreach($_POST['customer_2'] as $key=>$value){
                   	$data[$key] = $value;
                   }
				}
                $data['invoice_customer_type'] = $_POST['invoice_type_1']['invoice_customer_type'];
			}else{
					$data['invoice_amount'] = $_POST['invoice_type_2']['invoice_amount'];
					$data['invoice_title'] = $_POST['invoice_type_2']['invoice_title'];
					foreach($_POST['customer_2'] as $key=>$value){
                   	  if($key !='invoice_title'){
                   	  	$data[$key] = null;
                   	  }
                   }

				}
			$data['invoice_order_id'] = $_POST['invoice']['invoice_order_id'];
			//$data['invoice_kp_type'] = $_GET['type'];
			$tem = $this->db->select('invoice_kp_type')->from('tc_invoice')->where('invoice_id',$data['invoice_id'])->get()->row_array();
			$data['invoice_kp_type'] = $tem['invoice_kp_type'];
			if($data['invoice_kp_type']==1001){
				$data['invoice_status'] = 1001;
			}else{
				$data['invoice_status'] = 1004;
			}
			//p($data);die;
			$this->invoice->update($data,$_POST['invoice']['invoice_id']);
			//p($data);die;
			$log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'编辑票据',
				'system_log_module'=>'Order',
				'system_log_module_id'=>$data['invoice_order_id']
				);

			if(isset($data['invoice_memo'])) $log_data['system_log_note'] = $data['invoice_memo'];

			$this->system_log->add($log_data);
			if($data['invoice_status']==1004){
				$message_data = array(
					'message_owner'=>$_POST['order_finance'],
					'message_module'=>'invoice',
					'message_module_id'=>$_POST['invoice']['invoice_id'],
					'message_type'=>1004,
					'message_department'=>$_POST['order_finance_department'],
					'message_url'=>'www/invoice/verify_invoice'
				);
				$this->message->add($message_data);
			}else{
				$message_data = array(
					'message_owner'=>$_POST['order_finance'],
					'message_module'=>'invoice',
					'message_module_id'=>$_POST['invoice']['invoice_id'],
					'message_type'=>1002,
					'message_department'=>$_POST['order_finance_department'],
					'message_url'=>'www/invoice/confirm_invoice'
				);
				$this->message->add($message_data);
			}

			 $message_data1 = array(
				'message_status'=>'1002'
				);
           $param['message_type'] = 1011;
           $param['message_module_id'] = $_POST['invoice']['invoice_id'];
		   $this->message->update($message_data1,$param);
			$mess_remind_data = array(
				'message_remind_role_id' =>3,
				'message_remind_status'=>1001
			);
			$this->message_remind->update($mess_remind_data,$mess_remind_data['message_remind_role_id']);
			echo 1;
			}
		}else{
			echo '失败??';
		}
	}

	//ajax调用的删除
	public function ajax_del_post(){
		if($_POST){
			//p($_POST);
			$invoice_id = $_POST['invoice_id'];
			$this->invoice->del($invoice_id);
			$log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'删除票据',
				'system_log_module'=>'Order',
				'system_log_module_id'=>$_POST['order_id']
				);
			$this->system_log->add($log_data);
			echo 1;
		}else{
			echo '失败??';
		}
	}

    public function ajax_view(){
      if(isset($_GET['invoice_id'])){
      	$where=array(
				'invoice_id'=>$_GET['invoice_id']
			);
		$data['listData']=$this->invoice->GetInfo($where);
		$data['listData'] = $data['listData'][0];
		//p(json_decode($data['listData']['invoice_content']));
		$data['listData']['invoice_content'] = object_array(json_decode($data['listData']['invoice_content']));
		//p($data['listData']['invoice_content'] );
		$data["labels"]=$this->invoice->attributeLabels();
		$this->load->model('www/order_model','order');
		$data["order_data"]=$this->order->id_aGetInfo($data['listData']['invoice_order_id']);

		$data['order_d_data'] = $this->db->get_where('tc_order_d',array('order_d_order_id'=>$data['order_data']['order_id']))->result_array();

		//$data["order_data"]=$this->order->id_aGetInfo($data['listData']['invoice_order_id']);
		//$data['listLayout']=$this->invoice->listLayout();
		// p($data['listData']);
		// p($data['labels']);
      	$this->load->view('www/invoice/ajax_view',$data);
      }
    }

    //执行开票
    public function confirm_invoice(){
		if(isset($_GET['invoice_id'])){
	      	$where=array(
					'invoice_id'=>$_GET['invoice_id']
				);
			$data['listData']=$this->invoice->GetInfo($where);
			$data['listData'] = $data['listData'][0];
			//p($data['listData']);
			$data['listData']['invoice_content'] = object_array(json_decode($data['listData']['invoice_content']));
			$this->load->model('www/order_model','order');
			$data["order_data"]=$this->order->id_aGetInfo($data['listData']['invoice_order_id']);
			p($data);
	    	$this->load->view('www/invoice/confirm_invoice',$data);
       }
    }
    public function update_confirm_invoice(){
    	if(isset($_POST)){
    		//p($_POST);die;
          foreach($_POST['invoice'] as $key=>$value){
          	if($key!='invoice_id' && $key!='invoice_order_id'){
          		$data[$key] = $value;
          	}
          }
         // p($data);die;
          $data['invoice_status'] = 1002;

//cus lee 2014-04-23 渠道接口 start
			$invoice_data=$this->invoice->id_aGetInfo($_POST['invoice']['invoice_id']);

			//echo $invoice_data['invoice_order_id_arr']['order_source'];exit;;
			if($invoice_data['invoice_order_id_arr']['order_source']==1003){ //当来源为【渠道】的订单
				//调用打渠道的接口
				$params['action'] = 'ForeDepositTicketApply'; //预存款的票据审核
				$params['id']= $invoice_data['invoice_number']; //传给营收的渠道编码
				$params['result']= 'ok'; //审核失败
				$params['remark']= 'XXXXXXX'; //审核备注
				$params['time']= date("Y-m-d H:i:s"); //审核时间 (用当前时间)
				//p($params);
				$res = $this->qudao_api($params);
				//p($res);
				if($res['res']=='fail'){
					//echo $res['msg'];exit;
				}else{
					//p($res);
				}
			}
//cus lee 2014-04-23 渠道接口 end

          $this->invoice->update($data,$_POST['invoice']['invoice_id']);
		  $error_log=trim($this->invoice->db->_error_number())=="0"?"":"-error_no:".$this->invoice->db->_error_number()."-erro_rs:".$this->invoice->db->_error_message();
           $log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'执行开票',
				'system_log_note'=>$data['invoice_do_note'].$error_log,
				'system_log_module'=>'Order',
				'system_log_module_id'=>$_POST['invoice']['invoice_order_id']
				);
           $message_data = array(
				'message_status'=>'1002'
				);
           $param['message_type'] = 1002;
           $param['message_module_id'] = $_POST['invoice']['invoice_id'];
           //p($log_data);die;
           //echo $_POST['invoice']['invoice_id'];
          // p($message_data);
           $this->message->update($message_data,$param);
			$this->system_log->add($log_data);
           echo 1;
    	}else{
    		echo '失败';
    	}
    }
    //退回执行开票
    public function return_do_invoice(){
       if(isset($_POST)){
       //	p($_POST);die;
       	 foreach($_POST['invoice'] as $key=>$value){
          	if($key!='invoice_id'){
          		$data[$key] = $value;
          	}
          }
       	 $data['invoice_status'] = 1003;
//cus lee 2014-04-23 渠道接口 start
			$invoice_data=$this->invoice->id_aGetInfo($_POST['invoice']['invoice_id']);
			//echo $invoice_data['invoice_order_id_arr']['order_source'];exit;
			if($invoice_data['invoice_order_id_arr']['order_source']==1003){ //当来源为【渠道】的订单
				//调用打渠道的接口
				$params['action'] = 'ForeDepositTicketApply'; //预存款的票据审核
				$params['id']= $invoice_data['invoice_number']; //传给营收的渠道编码
				$params['result']= 'failure'; //审核失败
				$params['remark']= 'XXXXXXX'; //审核备注
				$params['time']= date("Y-m-d H:i:s"); //审核时间 (用当前时间)
				//p($params);
				$res = $this->qudao_api($params);
				//p($res);
				if($res['res']=='fail'){
					//echo $res['msg'];exit;
				}else{
					//p($res);
				}
			}
//cus lee 2014-04-23 渠道接口 end
            $this->invoice->update($data,$_POST['invoice']['invoice_id']);
            $order_data = $this->order->id_aGetInfo($_POST['invoice']['invoice_order_id']);
            //p($order_data);
            $log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'退回开票申请',
				'system_log_note'=>$data['invoice_do_note'],
				'system_log_module'=>'Order',
				'system_log_module_id'=>$_POST['invoice']['invoice_order_id']
				);
           //p($log_data);die;
			$this->system_log->add($log_data);
			$message_data1 = array(
				'message_status'=>'1002'
				);
           $param['message_type'] = 1002;
           $param['message_module_id'] = $_POST['invoice']['invoice_id'];
  			$this->message->update($message_data1,$param);
  			$message_data = array(
					'message_owner'=>$order_data['order_typein'],
					'message_module'=>'invoice',
					'message_module_id'=>$_POST['invoice']['invoice_id'],
					'message_type'=>1011,
					//'message_department'=>$order_data['order_owner_arr']['user_department'],
					'message_url'=>'www/invoice/ajax_edit'
			);
			$this->message->add($message_data);

            echo 1;
       }else{
       	echo "失败";
       }
    }
    //审核预开票申请
    public function verify_invoice(){
      if(isset($_GET['invoice_id'])){
	      	$where=array(
					'invoice_id'=>$_GET['invoice_id']
				);
			$data['listData']=$this->invoice->GetInfo($where);
			$data['listData'] = $data['listData'][0];
			$this->load->model('www/order_model','order');
			$data["order_data"]=$this->order->id_aGetInfo($data['listData']['invoice_order_id']);
			$data['listData']['invoice_content'] = object_array(json_decode($data['listData']['invoice_content']));
			//p($data['listData']);
	    	$this->load->view('www/invoice/verify_invoice',$data);
       }
    }

     public function update_verify_invoice(){
     	if(isset($_POST)){
     		//p($_POST);die;
          foreach($_POST['invoice'] as $key=>$value){
          	if($key!='invoice_id' && $key!='invoice_order_id'){
          		$data[$key] = $value;
          	}
          }
          $data['invoice_status'] = 1005;
           $this->invoice->update($data,$_POST['invoice']['invoice_id']);
            $log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'审核预开票申请',
				'system_log_note'=>$data['invoice_process_note'],
				'system_log_module'=>'Order',
				'system_log_module_id'=>$_POST['invoice']['invoice_order_id']
				);
           //p($log_data);die;
			$this->system_log->add($log_data);

            $message_data1 = array(
				'message_status'=>'1002'
				);
           $param['message_type'] = 1004;
           $param['message_module_id'] = $_POST['invoice']['invoice_id'];
  			$this->message->update($message_data1,$param);
  			$order_data = $this->order->id_aGetInfo($_POST['invoice']['invoice_order_id']);
            $message_data2 = array(
					'message_owner'=>$order_data['order_finance'],
					'message_module'=>'invoice',
					'message_module_id'=>$_POST['invoice']['invoice_id'],
					'message_type'=>1002,
					'message_department'=>$order_data['order_finance_arr']['user_department'],
					'message_url'=>'www/invoice/confirm_invoice'
			);
			$this->message->add($message_data2);

           echo 1;
    	}else{
    		echo '失败';
    	}
     }

     //驳回审批
      public function return_verify_invoice(){
       if(isset($_POST)){
       	 foreach($_POST['invoice'] as $key=>$value){
          	if($key!='invoice_id'){
          		$data[$key] = $value;
          	}
          }
       	  $data['invoice_status'] = 1003;
       	  //p($data);
            $this->invoice->update($data,$_POST['invoice']['invoice_id']);
            $log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'预开票审核不通过',
				'system_log_note'=>$data['invoice_process_note'],
				'system_log_module'=>'Order',
				'system_log_module_id'=>$_POST['invoice']['invoice_order_id']
				);
           //p($log_data);die;
			$this->system_log->add($log_data);
			$message_data1 = array(
				'message_status'=>'1002'
				);
           $param['message_type'] = 1004;
           $param['message_module_id'] = $_POST['invoice']['invoice_id'];
 		   $this->message->update($message_data1,$param);
 		   $order_data = $this->order->id_aGetInfo($_POST['invoice']['invoice_order_id']);
 		   $message_data = array(
					'message_owner'=>$order_data['order_typein'],
					'message_module'=>'invoice',
					'message_module_id'=>$_POST['invoice']['invoice_id'],
					'message_type'=>1011,
					//'message_department'=>$order_data['order_owner_arr']['user_department'],
					'message_url'=>'www/invoice/ajax_edit'
			);
			$this->message->add($message_data);
            echo 1;
       }else{
       	echo "失败";
       }
    }

    //申请作废
    public function apply_invalidate_invoice(){
       if(isset($_GET['invoice_id'])){
	      	$where=array(
					'invoice_id'=>$_GET['invoice_id']
				);
			$data['listData']=$this->invoice->GetInfo($where);
			$data['listData'] = $data['listData'][0];
			$data['listData']['invoice_content'] = object_array(json_decode($data['listData']['invoice_content']));
	    	$this->load->view('www/invoice/apply_invalidate_invoice',$data);
       }
    }

    public function do_apply_invalidate_invoice(){
       if(isset($_POST)){
       	//p($_POST);die;
          foreach($_POST['invoice'] as $key=>$value){
          	if($key!='invoice_id'){
          		$data[$key] = $value;
          	}
          }
          $data['invoice_apply_untime'] = date("Y-m-d H:i:s",time());
         $data['invoice_status'] = 1006;
           $this->invoice->update($data,$_POST['invoice']['invoice_id']);
           $log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'申请废票',
				'system_log_note'=>$data['invoice_apply_uninvoice_note'],
				'system_log_module'=>'Order',
				'system_log_module_id'=>$_POST['invoice']['invoice_order_id']
				);
           //增加待办信息
           $message_data = array(
				'message_owner'=>$_POST['order_finance'],
				'message_module'=>'invoice',
				'message_module_id'=>$_POST['invoice']['invoice_id'],
				'message_type'=>1003,
				'message_url'=>'www/invoice/invalidate_invoice'
				);
           //p($log_data);die;
			$this->system_log->add($log_data);
			$this->message->add($message_data);
			$mess_remind_data = array(
				'message_remind_role_id' =>3,
				'message_remind_status'=>1001
			);
			$this->message_remind->update($mess_remind_data,$mess_remind_data['message_remind_role_id']);
           echo 1;
    	}else{
    		echo '失败';
    	}
    }

    //执行废票
     public function invalidate_invoice(){
       if(isset($_GET['invoice_id'])){
	      	$where=array(
					'invoice_id'=>$_GET['invoice_id']
				);
			$data['listData']=$this->invoice->GetInfo($where);
			$data['listData'] = $data['listData'][0];
			$this->load->model('www/order_model','order');
			$data["order_data"]=$this->order->id_aGetInfo($data['listData']['invoice_order_id']);
			$do_time_month = explode('-',$data['listData']['invoice_do_time']);
            $do_time_month = intval($do_time_month[1]);
            $month = date('m',time());
            if($month-$do_time_month<1){
            	$data['invalidate_type'] = 0;//标示当月作废
            }else{
            	$data['invalidate_type'] = 1;//标示非当月作废
            }
			$data['listData']['invoice_content'] = object_array(json_decode($data['listData']['invoice_content']));
	    	$this->load->view('www/invoice/invalidate_invoice',$data);
       }
    }

    public function do_invalidate_invoice(){
       if(isset($_POST)){

       	   $where=array(
					'invoice_id'=>$_POST['invoice']['invoice_id']
				);
			$arr['listData']=$this->invoice->GetInfo($where);
			//p($arr['listData']);
			$do_time_month = explode('-',$arr['listData'][0]['invoice_do_time']);
            $do_time_month = intval($do_time_month[1]);
            //echo "开票月份：".$do_time_month;
			$month = date('m',time());
			//echo "作废月：".$month."<br>";die;
			//echo $arr['listData'][0]['invoice_do_time'];die;

			if($month-$do_time_month<1){
				//echo "本月作废";die;
				foreach($_POST['invoice'] as $key=>$value){
					if($key!='invoice_id' && $key!='invoice_invoice_no' && $key!='invoice_code'){
						$data[$key] = $value;
					}
				}

				$data['invoice_do_time'] = $arr['listData'][0]['invoice_do_time'];
				$data['invoice_status'] = 1007;
				$data['invoice_untime'] = date('Y-m-d',time());
				//p($data);die;
				$this->invoice->update($data,$_POST['invoice']['invoice_id']);
				//p($_POST);

			}else{
				//跨月作废

				 foreach($_POST['invoice'] as $key=>$value){
		          	if($key!='invoice_id'){
		          		$data[$key] = $value;
		          	          		}}

		          foreach($arr['listData'][0] as $key=>$value){
		          	if(!in_array($key, array('invoice_do_time','invoice_invoice_no','invoice_code','invoice_uninvoice_note','invoice_uninvoice_time','invoice_uninvoice_status','invoice_type_arr','invoice_customer_type_arr','invoice_uninvoice_status_arr','invoice_process_status_arr','invoice_do_status_arr','invoice_id','invoice_order_id_arr','invoice_status_arr'))){
		               $data[$key] = $value;
		            }
		          }
		         // p($_POST);die;
		          $data['invoice_uninvoice_status'] = 1004;
		          $data['invoice_status'] = 1008;
		          $data['invoice_amount'] = $data['invoice_amount'];
		          $data['invoice_do_time'] = $_POST['invoice']['invoice_do_time'];
		          $data['invoice_untime'] = date("Y-m-d H:i:s",time());
		          //p($data);die;
		          $this->invoice->update(array('invoice_status'=>1007),$_POST['invoice']['invoice_id']);
		           $this->invoice->add($data);
			}
			//p($data);
			//echo 'asdd:'.$data['invoice_uninvoice_note'];
			$log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'执行废票',
				'system_log_note'=>$data['invoice_uninvoice_note'],
				'system_log_module'=>'Order',
				'system_log_module_id'=>$_POST['invoice']['invoice_order_id']
				);


           //p($log_data);die;
			$this->system_log->add($log_data);
			$message_data = array(
				'message_status'=>'1002'
				);
			$param['message_type'] = 1003;
			$param['message_module_id'] = $_POST['invoice']['invoice_id'];
			$this->message->update($message_data,$param);
           echo 1;
    	}else{
    		echo '失败';
    	}
    }
//驳回废票申请
    public function return_invalidate_invoice(){
    	 if(isset($_POST)){
    	 	//p($_POST);die;
       	 // foreach($_POST['invoice'] as $key=>$value){
         //  	if(!in_array(needle, array('invoice_id','invoice_do'))$key!='invoice_id' and $key!='invoice_do_time'){
         //  		$data[$key] = $value;
         //  	}
         //  }
       	   $data['invoice_status'] = 1009;
           $this->invoice->update($data,$_POST['invoice']['invoice_id']);
           $log_data = array(
				'system_log_addtime'=>date('Y-m-d H:i:s',time()),
				'system_log_user_id'=>$this->session->userdata('user_id'),
				'system_log_operation'=>'驳回废票申请',
				'system_log_note'=>$_POST['invoice']['invoice_uninvoice_note'],
				'system_log_module'=>'Order',
				'system_log_module_id'=>$_POST['invoice']['invoice_order_id']
				);
           //p($log_data);die;
			$this->system_log->add($log_data);
			$message_data = array(
				'message_status'=>'1002'
				);
			$param['message_type'] = 1003;
			$param['message_module_id'] = $_POST['invoice']['invoice_id'];
			$this->message->update($message_data,$param);
            echo 1;
       }else{
       	echo "失败";
       }
    }

    public function new_ajax_select(){
		//p($_POST);//exit;
		$this->load->model('www/api_model','api');
		$listData = $this->api->select($_POST);
		$data["labels"]=$this->invoice->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取

		//$data['type_arr']=$this->type_arr();
		// if(isset($_GET['type_id'])){

		// 	$this->menu1=$data['type_arr'][$_GET['type_id']]["type_label"];
		// }
		$data['listData']=$listData['info'];
		$data['totalNumber'] = $listData['count']; //数据总数
		$data['sel_data']=$listData['sel_data'];
		$data['perNumber'] = $_POST['perNumber']; //每页显示的记录数
		$data['page'] = $_POST['page'];//当前页数
		//p($data);exit;
		//根据类型的不同调用不同的列表页

			$this->load->view('www/invoice/ajax_select',$data);

	}

	public function qudao_api($params){
		require_once('application/libraries/snoopy.php');
		$snoopy = new Snoopy();
		//$url="http://192.168.0.96:9014/api/review"; //正式环境
		$url="http://192.168.35.28:9014/api/review";  //测试环境
		//同步渠道
		$snoopy->submit($url,$params);
		$res = $snoopy->results;
		//print_r($res);
		return json_decode($res,true);
	}
}
?>