<?php

class Activity_Auth extends WWW_Controller {

	public $menu1 = 'activity_auth';

	public function __construct() {
		parent::__construct();
		$this->load->model('www/activity_auth_model', 'activity_auth');
	}

	public function index() {
		$this->load->model('admin/object_model', 'obj');
		$data['listData'] = $this->obj->objlist();
		//p($data['listData']);
		$activity_auth = $this->activity_auth->check($this->input->get('role_id'));

		if ($activity_auth[0]['role_activity_auth'] != null) {
			//echo "11";die;
			$data['activity_auth_checked'] = json_decode($activity_auth[0]['role_activity_auth']);
		}
		$this->render('www/activity_auth/list', $data);
	}

	public function add() {
		$this->output->enable_profiler(TRUE);
		if (!empty($_POST)) {
			foreach ($_POST as $key => $value) {
				$tem[] = $key;
			}
			// for($i=0;$i<count($_POST);$i++){
			//  $arr[$tem[$i]]=array();
			//   foreach ($_POST[$tem[$i]] as $key => $value) {
			//     $arr[$tem[$i]][]=$key;
			//   }
			// }
			$data['role_activity_auth'] = json_encode($tem);
		} else {
			$data['role_activity_auth'] = null;
		}
		$this->activity_auth->add($data, $this->input->get('role_id'));
		success('www/role', '操作成功');
	}

}

?>