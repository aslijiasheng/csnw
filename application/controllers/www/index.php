<?php
  class Index extends WWW_Controller{
  	public function __construct(){
		parent::__construct();
		$this->load->model('www/user_model','user');
		$this->user->db->query('set names utf8');
	}
	public function index(){
		// lee_jump('http://192.168.35.28:8200/Logon.aspx?Action=Login&AppCode=%u0070%u006F%u0072%u0074%u0061%u006C&CurrentUrl=%u0068%u0074%u0074%u0070%u003A%u002F%u002F%u0031%u0039%u0032%u002E%u0031%u0036%u0038%u002E%u0033%u0035%u002E%u0032%u0038%u003A%u0038%u0030%u0031%u0033%u002F&AppAttribute=&hasAppAttribute=True&ErrorMsg=','请通过个人门户登陆');
		$this->login();
	}
	public function login(){
		 $this->load->helper('form');
		 $error='';
		if(!empty($_POST['user'])){
			//p($_POST['user']);
			//echo 111;exit;
		  $this->load->library('form_validation');
		  $this->form_validation->set_rules('user[user_login_name]','登陆名','required');
		  $this->form_validation->set_rules('user[user_password]','密码','required');
		  if($this->form_validation->run() !=false){
		  	  $info=$this->user->db->get_where('tc_user',array('user_login_name'=>$_POST['user']['user_login_name']))->result_array();

		      if(!empty($info)){
		      	 $role_info = $this->db->get_where('tc_role_user',array('user_id'=>$info[0]['user_id']))->result_array();
		  	     $data['id_aData']=$this->user->id_aGetInfo($info[0]['user_id']);
		  	    // p($role_info)
		  	     if(!empty($role_info)){
		  	     	foreach($role_info as $role_arr){
		  	     		$roles[]=$role_arr['role_id'];
		  	     	}
		  	     }

		  	    if($data['id_aData']['user_password']!=$_POST['user']['user_password']){
		  	    	$error="密码错误";
		  	    }else{
		  	    	 $this->session->set_userdata('role_id_arr',$roles);
		  	    	 $this->session->set_userdata('department_id',$data['id_aData']['user_department']);
                     $this->session->set_userdata('user_id',$data['id_aData']['user_id']);
                     $this->session->set_userdata('user_name',$info[0]['user_name']);
                    // p($_SERVER);
                     $this->load->model('www/system_log_model','system_log');
                     $log_data['system_log_user_id'] = $data['id_aData']['user_id'];
                     $log_data['system_log_ip'] = $_SERVER["REMOTE_ADDR"];
                     $log_data['system_log_login_time'] = date('Y-m-d H:i:s',time());
                     $log_data['system_log_addtime'] = date('Y-m-d H:i:s',time());
                     $log_data['system_log_operation'] = '登录';
                     $this->system_log->add($log_data);
                     //$this->output->enable_profiler(TRUE);
		  		     success('www/home','登陆成功');
		  	    }
		      }else{
		      	$error="用户名不存在";
		  	    $this->session->set_userdata('error_message', '登陆名不存在');
		      }
		  }else{
		  	$this->session->set_userdata('error_message', null);
		  }

		}
         $data['error']=$error;
        // p($data);die;
		$this->load->view('www/index/login',$data);
	}

	public function out(){
		$this->session->sess_destroy();
		redirect("www/");
	}
  }
 ?>