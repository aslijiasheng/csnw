<?php
class Order_adjunct extends WWW_Controller{
	public $menu1='order_adjunct';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/order_adjunct_model','order_adjunct');
	}

	public function index(){
		$this->order_adjunct->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->order_adjunct->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->order_adjunct->attributeLabels();
		$data['listLayout']=$this->order_adjunct->listLayout();
		$this->render('www/order_adjunct/list',$data);
	}
	public function add(){
		$this->order_adjunct->db->query('SET NAMES UTF8');
		$data["labels"]=$this->order_adjunct->attributeLabels();
		
		
		
		
		
		
		
		if(!empty($_POST)){
		if(isset($_GET['type_id'])){
           $_POST['order_adjunct']['type_id']=$_GET['type_id'];
           $this->order_adjunct->add($_POST['order_adjunct']);
		    success('www/order_adjunct?type_id='.$_GET['type_id'],'创建成功');
		}else{
		  $this->order_adjunct->add($_POST['order_adjunct']);
		  success('www/order_adjunct','创建成功');
		}
			
		}
		$this->render('www/order_adjunct/add',$data);
	}
	public function update(){
		$this->order_adjunct->db->query('SET NAMES UTF8');
		$data["labels"]=$this->order_adjunct->attributeLabels();
		$data['id_aData']=$this->order_adjunct->id_aGetInfo($_GET['order_adjunct_id']);
		$data['listLayout']=$this->order_adjunct->listLayout();
		
		
		
		
		
		
		
		if(!empty($_POST)){
		    if(isset($_GET['type_id'])){
		      $_POST['order_adjunct']['type_id']=$_GET['type_id'];
		      $this->order_adjunct->update($_POST['order_adjunct'],$_GET['order_adjunct_id']);
			    success('www/order_adjunct?type_id='.$_GET['type_id'],'更新成功');
		    }else{
              	$this->order_adjunct->update($_POST['order_adjunct'],$_GET['order_adjunct_id']);
			    success('www/order_adjunct','更新成功');
		    }
		}
		$this->render('www/order_adjunct/update',$data);
	}
	public function ajax_select(){
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["order_adjunct_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		if (isset($_GET['type_id'])){ 
			if($_GET['type_id']!="" and $_GET['type_id']!=0){
				$where['type_id']=$_GET['type_id']; 
			}
		}
		
		$data['listData']=$this->order_adjunct->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);
		$data["labels"]=$this->order_adjunct->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取
		
		

		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->order_adjunct->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/order_adjunct/ajax_select',$data);
	}
	public function ajax_select_quote(){
		if (isset($_POST["select_json"]) and $_POST["select_json"]!=""){
			$select_arr = json_decode($_POST["select_json"],true);
			$like["order_adjunct_".$select_arr['attr']]=$select_arr['value'];
		}else{
			//$select_arr = "";
			$like="";
		}
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$where="";
		
		$data['listData']=$this->order_adjunct->listGetInfo($where,$_GET['page'],$_GET['perNumber'],$like);
		$data["labels"]=$this->order_adjunct->attributeLabels();
		//如果包含对象类型，添加一个数组，用于视图获取
		
		

		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['totalNumber'] = $this->order_adjunct->countGetInfo($where,$like); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		//p($data);
		$this->output->enable_profiler(false);
		$this->load->view('www/order_adjunct/ajax_select_quote',$data);
	}
    public function ajax_list(){
       $this->order_adjunct->db->query('SET NAMES UTF8');
		if (!isset($_GET['page'])){
			$_GET['page']=1;
		}
		if (!isset($_GET['perNumber'])){
			$_GET['perNumber']=10;
		}
		$data['listData']=$this->order_adjunct->listGetInfo('',$_GET['page'],$_GET['perNumber']);
		$data['totalNumber'] = $this->order_adjunct->countGetInfo(); //数据总数
		$data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
		$data["labels"]=$this->order_adjunct->attributeLabels();
		$data['listLayout']=$this->order_adjunct->listLayout();
		$this->output->enable_profiler(false);
		$this->load->view('www/order_adjunct/ajax_list',$data);
    }
    public function del(){
       $this->order_adjunct->del($_GET['order_adjunct_id']);
	   success('www/order_adjunct','删除成功');
    }

}
?>