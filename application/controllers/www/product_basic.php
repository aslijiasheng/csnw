<?php

class Product_basic extends WWW_Controller {

    public $menu1 = 'product_basic';

    public function __construct() {
        parent::__construct();
        $this->load->model('www/product_basic_model', 'product_basic');
    }

    public function index() {
        $this->product_basic->db->query('SET NAMES UTF8');
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data['totalNumber'] = $this->product_basic->countGetInfo(); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        $data["labels"] = $this->product_basic->attributeLabels();
        $data['listLayout'] = $this->product_basic->listLayout();
        $this->render('www/product_basic/list', $data);
    }

    public function add() {
        $this->product_basic->db->query('SET NAMES UTF8');
        $data["labels"] = $this->product_basic->attributeLabels();

        $this->load->model('admin/enum_model', 'enum');
        $data["product_basic_category_enum"] = $this->enum->getlist(370);
        $data["product_basic_sub_category_enum"] = $this->enum->getlist(371);
        $data["product_basic_sales_type_enum"] = $this->enum->getlist(374);
        $data["product_basic_charging_type_enum"] = $this->enum->getlist(375);
        $data["product_basic_calc_unit_enum"] = $this->enum->getlist(376);
        $data["product_basic_service_unit_enum"] = $this->enum->getlist(377);
        $data["product_basic_can_renewal_enum"] = $this->enum->getlist(378);
        $data["product_basic_has_expired_enum"] = $this->enum->getlist(379);
        $data["product_basic_need_open_enum"] = $this->enum->getlist(380);
        $data["product_basic_need_host_enum"] = $this->enum->getlist(381);

        if (!empty($_POST)) {
            if (isset($_GET['type_id'])) {
                $_POST['product_basic']['type_id'] = $_GET['type_id'];
                $this->product_basic->add($_POST['product_basic']);
                success('www/product_basic?type_id=' . $_GET['type_id'], '创建成功');
            } else {
                $this->product_basic->add($_POST['product_basic']);
                success('www/product_basic', '创建成功');
            }
        }
        $this->render('www/product_basic/add', $data);
    }

    public function update() {
        $this->product_basic->db->query('SET NAMES UTF8');
        $data["labels"] = $this->product_basic->attributeLabels();
        $data['id_aData'] = $this->product_basic->id_aGetInfo($_GET['product_basic_id']);
        $data['listLayout'] = $this->product_basic->listLayout();

        $this->load->model('admin/enum_model', 'enum');
        $data["product_basic_category_enum"] = $this->enum->getlist(370);
        $data["product_basic_sub_category_enum"] = $this->enum->getlist(371);
        $data["product_basic_sales_type_enum"] = $this->enum->getlist(374);
        $data["product_basic_charging_type_enum"] = $this->enum->getlist(375);
        $data["product_basic_calc_unit_enum"] = $this->enum->getlist(376);
        $data["product_basic_service_unit_enum"] = $this->enum->getlist(377);
        $data["product_basic_can_renewal_enum"] = $this->enum->getlist(378);
        $data["product_basic_has_expired_enum"] = $this->enum->getlist(379);
        $data["product_basic_need_open_enum"] = $this->enum->getlist(380);
        $data["product_basic_need_host_enum"] = $this->enum->getlist(381);

        if (!empty($_POST)) {
            if (isset($_GET['type_id'])) {
                $_POST['product_basic']['type_id'] = $_GET['type_id'];
                $this->product_basic->update($_POST['product_basic'], $_GET['product_basic_id']);
                success('www/product_basic?type_id=' . $_GET['type_id'], '更新成功');
            } else {
                $this->product_basic->update($_POST['product_basic'], $_GET['product_basic_id']);
                success('www/product_basic', '更新成功');
            }
        }
        $this->render('www/product_basic/update', $data);
    }

    public function ajax_list() {
        $this->product_basic->db->query('SET NAMES UTF8');
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data['listData'] = $this->product_basic->listGetInfo('', $_GET['page'], $_GET['perNumber']);
        $data['totalNumber'] = $this->product_basic->countGetInfo(); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        $data["labels"] = $this->product_basic->attributeLabels();
        $data['listLayout'] = $this->product_basic->listLayout();
        $this->output->enable_profiler(false);
        $this->load->view('www/product_basic/ajax_list', $data);
    }

    public function ajax_select() {
        if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
            $select_arr = json_decode($_POST["select_json"], true);
            $like["product_basic_" . $select_arr['attr']] = $select_arr['value'];
        } else {
            //$select_arr = "";
            $like = "";
        }
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $where = "";
        if (isset($_GET['type_id'])) {
            if ($_GET['type_id'] != "" and $_GET['type_id'] != 0) {
                $where['type_id'] = $_GET['type_id'];
            }
        }

        $data['listData'] = $this->product_basic->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
        $data["labels"] = $this->product_basic->attributeLabels();
        //如果包含对象类型，添加一个数组，用于视图获取

        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data['totalNumber'] = $this->product_basic->countGetInfo($where, $like); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        //p($data);
        $this->output->enable_profiler(false);
        $this->load->view('www/product_basic/ajax_select', $data);
    }

    public function ajax_select_quote() {
        if (isset($_POST["select_json"]) and $_POST["select_json"] != "") {
            $select_arr = json_decode($_POST["select_json"], true);
            $like["product_basic_" . $select_arr['attr']] = $select_arr['value'];
        } else {
            //$select_arr = "";
            $like = "";
        }
        if (!isset($_GET['page'])) {
            $_GET['page'] = 1;
        }
        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $where = "";

        $data['listData'] = $this->product_basic->listGetInfo($where, $_GET['page'], $_GET['perNumber'], $like);
        $data["labels"] = $this->product_basic->attributeLabels();
        //如果包含对象类型，添加一个数组，用于视图获取

        if (!isset($_GET['perNumber'])) {
            $_GET['perNumber'] = 10;
        }
        $data['totalNumber'] = $this->product_basic->countGetInfo($where, $like); //数据总数
        $data['perNumber'] = $_GET['perNumber']; //每页显示的记录数
        //p($data);
        $this->output->enable_profiler(false);
        $this->load->view('www/product_basic/ajax_select_quote', $data);
    }

    public function api($starttime = '') {
        date_default_timezone_set('Asia/Shanghai');
        if (empty($starttime)) {
            $starttime = 1356969600;
        } elseif ($starttime > 1395216699) {
            die('finish');
        }

        $endtime = $starttime + 108000;
        $res = $this->product_basic->sync($starttime, $endtime);
        $url = 'www/product_basic/api/' . $endtime;
        if ($res) {
            if ($res['line']) {
                echo $starttime . '<->' . $endtime . ' ' . $res['line'] . ' 出错';
                exit;
            }
            usleep(500);
            redirect($url, 'refresh');
        } else {
            // 没有返回内容，继续抓取数据
            echo date('Y-m-d h:m', $starttime) . ' <-> ' . date('Y-m-d h:m', $endtime) . ' 没有数据 ' . $starttime;
            usleep(500);
            redirect($url, 'refresh');
        }
    }

}

?>