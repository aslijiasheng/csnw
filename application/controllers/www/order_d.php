<?php
class Order_d extends WWW_Controller{
	public $menu1='order_d';

	public function __construct(){
		parent::__construct();
		$this->load->model('www/order_d_model','order_d');
		$this->order_d->db->query('SET NAMES UTF8');
		$this->load->model('www/salespay_model','salespay');
		$this->salespay->db->query('SET NAMES UTF8');
		$this->load->model('www/order_model','order');
		$this->order->db->query('SET NAMES UTF8');
	}

    public function detailed_list_view(){
		if(isset($_GET['order_id'])){
			//p($_GET['order_id']);exit;
			$order_id = $_GET['order_id'];
			$where=array(
				'order_d_order_id'=>$_GET['order_id'],
			);
			// $data['listData']=$this->order_d->GetInfo($where);

			$this->db->select()->from('tc_order_d')->where($where)->order_by('order_d_id')->group_by('order_d_list');
			$res = $this->db->get()->result_array();
			//p($this->db->last_query());
			$cycle_unit = array('1001'=>'年', '1002'=>'月', '1003'=>'天');
			// $r_enum = $this->db->select('enum_name, enum_key')->get_where('dd_enum', array('attr_id'=>'417'))->result_array();
			foreach($res as $k=>$v){
				$data['listData'][$k] = $v;
				$data['listData'][$k]['order_d_order_primecost'] = 0;
				$data['listData'][$k]['order_d_order_disc'] = 0;
				$where=array(
					'order_d_order_id'=>$_GET['order_id'],
					'order_d_list'=>$v['order_d_list'],
					'order_d_goods_code'=>$v['order_d_goods_code']
				);
				$res_p = $this->order_d->GetInfo($where);
				$data['listData'][$k]['product_arr'] = $res_p;
				foreach($res_p as $kk=>$vv){
					$data['listData'][$k]['order_d_order_primecost'] += $vv['order_d_product_basic_primecost']; // 原价
					$data['listData'][$k]['order_d_order_disc'] += $vv['order_d_product_basic_disc']; // 折后价
					// 周期，捆绑商品需要根据关系表查
					$r_cycle = $this->order_d->getCycle($vv);
					if($r_cycle){
						if($r_cycle['goods_price_cycle_days'] == 0){
							$data['listData'][$k]['product_arr'][$kk]['cycle'] = $r_cycle['goods_price_effective_days'].' '.'天';
						}else{
							$data['listData'][$k]['product_arr'][$kk]['cycle'] = $r_cycle['goods_price_cycle_days'].' '.$cycle_unit[$r_cycle['goods_price_cycle_unit']];
						}
					}
				}
			}
			
			//查询出这个订单的已确认到帐金额
			//已确认到帐
			$where = array(
				'salespay_order_id'=>$order_id,
				'salespay_status'=>1002,
				'salespay_money_type'=>1001,
			);
			$data["ydz_amount"]=$this->salespay->SumGetInfo('salespay_pay_amount',$where); //已到帐


			//还需要获取订单总金额
			$data["orderData"]=$this->order->id_aGetInfo_simple($order_id);

			$data["labels"]=$this->order_d->attributeLabels();
			$data['listLayout']=$this->order_d->listLayout();
			$this->output->enable_profiler(false);
			$this->load->view('www/order_d/detailed_list_view',$data);
		}else{
			echo "不能没有ID";
		}

    }
	public function detailed_list_edit(){
		$this->order_d->db->query('SET NAMES UTF8');
		$data['listData']=$this->order_d->listGetInfo('',1,10);
		$data["labels"]=$this->order_d->attributeLabels();
		$data['listLayout']=$this->order_d->listLayout();
		$this->output->enable_profiler(false);
		//$this->render('www/order_d/detailed_list_edit',$data);
		$this->load->view('www/order_d/detailed_list_edit',$data);
	}

}
?>