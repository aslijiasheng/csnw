<?php
class rel_order_order extends WWW_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('www/order_model','order');
		$this->order->db->query('SET NAMES UTF8');
		$this->load->model('www/rel_order_order_model','rel_order_order');
		$this->rel_order_order->db->query('SET NAMES UTF8');
	}
	
	public function detailed_list_view(){
		
		if(isset($_GET['order_id'])){
			$where=array(
				'rel_order_order_my_order'=>$_GET['order_id']
			);
			$data['listData']=$this->rel_order_order->GetInfo($where);
			//查询出这个订单的相关信息，用户判断不同的订单类型显示不同的内容
			$data['orderData']=$this->order->id_aGetInfo($_GET['order_id']);//获取订单的相关数据
			$this->load->view('www/rel_order_order/detailed_list_view',$data);
		}else{
			echo "不能没有ID";
		}
	}

}
?>