<?php
error_reporting(E_ALL & ~E_NOTICE);
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class Api extends WWW_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('PRC');
		$this->output->enable_profiler(false);
		$this->db->query('set names utf8');
		$this->load->model('www/api_model', 'api');
		$this->load->model('www/api_log_model', 'api_log');
		$this->load->model('admin/timing_task_model', 'timing_task');

		$this->load->model('www/api_log_model','api_log');

		//360safe start
		$getfilter = "'|\b(alert|confirm|prompt)\b|<[^>]*?>|^\\+\/v(8|9)|\\b(and|or)\\b.+?(>|<|=|\\bin\\b|\\blike\\b)|\\/\\*.+?\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";
		$postfilter = "^\\+\/v(8|9)|\\b(and|or)\\b.{1,6}?(=|>|<|\\bin\\b|\\blike\\b)|\\/\\*.+?\\*\\/|<\\s*script\\b|<\\s*img\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";
		$cookiefilter = "\\b(and|or)\\b.{1,6}?(=|>|<|\\bin\\b|\\blike\\b)|\\/\\*.+?\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";
		foreach ($_GET as $key => $value) {
			$this->StopAttack($key, $value, $getfilter);
		}
		foreach ($_POST as $key => $value) {
			$this->StopAttack($key, $value, $postfilter);
		}
		foreach ($_COOKIE as $key => $value) {
			$this->StopAttack($key, $value, $cookiefilter);
		}//360safe end
	}

	//2014-07-17 api start
	/**
	 * 营收接口地址
	 * @return json
	 */
	public function index() {
		if ($_POST) {
			//解析传过来的值
			$diff = array_diff(array('user', 'pwd', 'action', 'object', 'data'), array_keys($_POST));
			if ($diff) $this->api->_msg('缺少参数'); 
			//用户验证
			if ($this->api->userVerify($_POST['user'], $_POST['pwd'])) {
				extract($_POST, EXTR_OVERWRITE);
				//检查数据是否正确
				$data = is_array($data) ? $data : json_decode($data, true);
				if (empty($data))
					$this->api->_msg('数据异常'); 
				//创建日志
				$logData = array(
					'api_log_addtime' => date('Y-m-d H:i:s'),
					'api_log_method' => $user . ' | ' . $action . ' -> ' . $object,
					'api_log_data' => json_encode($data)
				);
				$res_log = $this->api_log->add($logData);
				$log_id = $this->db->insert_id();

				$result = $this->$action($data, $object, $user); //执行
				//更新日志
				$info = isset($result['info']) && !empty($result['info']) ? $result['info'] : '';
				if (!isset($result['res']) || $result['res'] != 'succ') {
					$result['msg'] = isset($result['msg']) ? $result['msg'] : '';
					$logData = array(
						'api_log_status' => 1002,
						'api_log_result' => $result['msg'] . '|' . $info,
					);
				} else {
					$logData = array(
						'api_log_status' => 1001,
						'api_log_result' => $result['msg']
					);
					if (isset($info['order_number']))
						$logData['api_log_order_number'] = $info['order_number'];
				}
				$this->api_log->update($logData, $log_id);
				die(json_encode($result));
			} else
				$this->api->_msg('没有权限');
		} else
			$this->api->_msg('NULL');
	}

    /**
     * 需求分析与客服系统接口
     * 新增：
     * 1、创建完成合同后由合同系统发起生成订单指令，营收接收创建订单； 
     * 2、创建完成订单后同时向客服系统写入工单；
     * 3、客服系统生成开户数据与开通代办数据；（代办状态为未到账状态）；
     * 4、当订单确认到账后更改客服系统中开通代办数据状态；（代办状态为待开通状态）；
     * 5、客服系统申请开通，填写开能参数；（代办状态为待审批）；
     * 6、审批成功后，向开通系统申请开通；（代办状态为已审核）；
     * 7、开通系统开通完成后返回客服系统开通信息；（代办状态为已开通）；更改系统参数；
     * 8、根据来源回写给来源系统信息；
     */
	/**
	 * add action
	 * @param json/array $data
	 * @param string $object
	 * @param string $user
	 * @return array
	 */
	private function add($data, $object, $user) {
		if ($object == 'order' && !isset($data['detailed'])) {
			// return $this->api->_msg('订单：必须带有明细内容'); //打接口的订单都应该带有明细内容，没有明细的只有预收款订单（通过接口抓取的订单）
		} else {
			if (isset($data['detailed'])) {
				$detailed = $data['detailed'];
				unset($data['detailed']);
			} else
				$detailed = array();
		}
		$res_data = $this->api->parseData($data, $object);
		if (!isset($res_data['res']) || $res_data['res'] != 'succ')
			return $res_data;
		$data = $res_data['info'];
		$this->load->model('www/' . $object . '_model', $object); //载入相应对象 Model
		$res_data = $this->$object->_addCheck($data, $user);
		if (!isset($res_data['res']) || $res_data['res'] != 'succ')
			return $res_data;
		$data = $res_data['info'];
		foreach ($detailed as $d_obj => $d_array) {
			$this->load->model('www/' . $d_obj . '_model', $d_obj);
			foreach ($d_array as $k => $v) {
				if ($d_obj != 'instruction_item') { //排除指令明细，特殊处理
					$res_pd = $this->api->parseData($v, $d_obj);
					if (!isset($res_pd['res']) or $res_pd['res'] != 'succ')
						return $res_pd;
					$v = $res_pd['info'];
					$res_pd = $this->$d_obj->_addCheck($v, $user); //_check检查对象内容，是否有必填项
					if (!isset($res_pd['res']) or $res_pd['res'] != 'succ')
						return $res_pd;
					$detailed[$d_obj][$k] = $res_pd['info'];
				}
			}
		}
		$this->db->trans_begin(); //开启事务S
        $this->load->model('www/sequence_model', 'sequence');
        /*
         * 获取指令编号、商品户号、产品户号
         */
        $sequence_data = $this->sequence->get_sequence();
        $sequence_data = _HashData($sequence_data, 'object_sequence_sequence_name');
        $instruction_code_num = $sequence_data['instructions']['object_sequence_sequence_code'] + 1;
        $data['instruction_code'] = 'in' . $instruction_code_num;
		$res_object = $this->api->_insert($data, $object);
		if (!isset($res_object['res']) || $res_object['res'] != 'succ') {
			$this->db->trans_rollback();
			$this->db->close();
			return $this->api->_return($object . ' 数据异常 rollback');
		}
        $object_sequence['where'] = array(
            "object_sequence_sequence_name" => "instructions",
        );
        $object_sequence['data'] = array(
            "object_sequence_sequence_code" => $instruction_code_num,
        );
		$res_sequence_object = $this->sequence->_update($object_sequence);
		if (!isset($res_sequence_object['res']) || $res_sequence_object['res'] != 'succ') {
			$this->db->trans_rollback();
			$this->db->close();
			return $this->api->_return('iii 数据异常 rollback');
		}
		foreach ($detailed as $d_obj => $d_array) {
			$fields = $d_obj . '_' . $object . '_id';
			foreach ($d_array as $k => $v) {
				if($d_obj == 'instruction_item'){
					$res_d = $this->api->dealDetailData($v);
					if ($res_d) {
                        $res_d['sequence_data'] = $sequence_data;
						$res_od = $this->api->saveInstrucItem($res_d, $res_object['instruction_id'], $k); //生成指令单明细数据并返回明细数据
						if ($res_od['res'] != 'succ') {
							$this->db->trans_rollback();
							$this->db->close();
							return $this->api->_return('saveOrderDetail' . ' 数据异常 rollback');
						}
                        /**
                            * 指令主表与明细表数据生成成功开始生成开户数据与待办数据
                            * 生成方式：
                            * 1、先生成开户数据;
                            * 2、开户数据生成成功后再生成待办数据;
                            * 3、完成接口数据生成;
                         */
                        $res_acctopen = $this->api->acctOpen($res_object['instruction_id']); 
                        if ($res_acctopen['res'] != 'succ') {
                            $this->db->trans_rollback();
                            $this->db->close();
                            return $this->api->_return('acctOpen' . ' 数据异常 rollback');
                        }
					} else{
						$this->db->trans_rollback();
						$this->db->close();
						return $this->api->_return('指令明细数据异常');
					}
				} else {
					if ($this->db->field_exists($fields, 'tc_' . $d_obj))
					$v[$fields] = $res_object[$object . '_id'];

					$res_d_obj = $this->api->_insert($v, $d_obj);
					if ($res_d_obj['res'] != 'succ') {
						$this->db->trans_rollback();
						$this->db->close();
						return $this->api->_return($d_obj . ' 数据异常 rollback');
					}
					$res_d_obj_id = $res_d_obj[$d_obj . '_id'];

					$this->_task($v, $d_obj, $res_d_obj_id); //添加代办
				}
			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->db->close();
			return $this->api->_return('sql rollback');
		} else {
			$this->db->trans_commit();
			$this->db->close();
			// $info = $this->_info($res_object[$object . '_id'], $object);
            $info = $data['instruction_source_code'];
			return $this->api->_return('执行成功', $info, 'succ');
		} //结束事务E
	}

	/**
	 * update action
	 * @param json/array $data
	 * @param string $object
	 * @param string $user
	 * @return array
	 */
	private function update($data, $object, $user) {
		if (isset($data['detailed'])) {
			$detailed = $data['detailed'];
			unset($data['detailed']);
		} else
			$detailed = array();

		$res_data = $this->api->parseData($data, $object);
		if (!isset($res_data['res']) || $res_data['res'] != 'succ')
			return $res_data;
		$data = $res_data['info'];

		$this->load->model('www/' . $object . '_model', $object); //载入相应对象 Model
		$res_data = $this->$object->_updateCheck($data, $user);
		if (!isset($res_data['res']) || $res_data['res'] != 'succ')
			return $res_data;
		$data = $res_data['info'];

		//删除订单关联更新 2014-07-17 18:29:41
		if(isset($detailed['rel_order_order']))
			unset($detailed['rel_order_order']);

		foreach ($detailed as $d_obj => $d_array) {
			$this->load->model('www/' . $d_obj . '_model', $d_obj);
			foreach ($d_array as $k => $v) {
				$res_pd = $this->api->parseData($v, $d_obj);
				if (!isset($res_pd['res']) or $res_pd['res'] != 'succ')
					return $res_pd;
				$v = $res_pd['info'];

				$res_pd = $this->$d_obj->_updateCheck($v, $user); //_check检查对象内容，是否有必填项
				if (!isset($res_pd['res']) or $res_pd['res'] != 'succ')
					return $res_pd;
				$detailed[$d_obj][$k] = $res_pd['info'];
			}
		}

		$this->db->trans_begin(); //开启事务S
		$res_object = $this->api->_update($data, $object);
		if (!isset($res_object['res']) || $res_object['res'] != 'succ') {
			$this->db->trans_rollback();
			$this->db->close();
			return$this->api->_return($object . ' 数据异常 rollback');
		}

		foreach ($detailed as $d_obj => $d_array) {
			$fields = $d_obj . '_' . $object . '_id';
			foreach ($d_array as $k => $v) {
				if ($this->db->field_exists(fields, 'tc_' . $d_obj) && $res_object[$object . '_id'] != 0)
					$v[$fields] = $res_object[$object . '_id'];

//				//关联订单 不能没有 my_order = 订单id
//				if ($d_obj == 'rel_order_order') {
//					if (isset($res_object['order_id'])) {
//						$v['rel_order_order_my_order'] = $res_object['order_id']; //关联订单ID
//					} else {
//						$this->db->trans_rollback();
//						$this->db->close();
//						return $this->api->_return('rel_order_order_my_order 数据异常 rollback');
//					}
//				}

				$res_d_obj = $this->api->_update($v, $d_obj);
				if ($res_d_obj['res'] != 'succ') {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->api->_return($d_obj . ' 数据异常 rollback');
				}

				$res_d_obj_id = $res_d_obj[$d_obj . '_id'];
				$this->_task($v, $d_obj, $res_d_obj_id); //添加代办
			}
		}

		$this->_task($data, $object, $res_object[$object . '_id']);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->db->close();
			return $this->api->_return('sql rollback');
		} else {
			$this->db->trans_commit();
			$this->db->close();
			$info = $this->_info($res_object[$object . '_id'], $object);
			return $this->api->_return('执行成功', $info, 'succ');
		} //结束事务E
	}

	/**
	 * 订单删除（暂不开放）
	 * @param order_number $data 订单编号
	 * @param string $object
	 * @param string $user
	 * @return array
	 */
	private function del($data, $object, $user) {
		$this->api->_msg('接口不开放');
		$order_number = isset($data['order_number']) ? $data['order_number'] : '';
		if (!empty($order_number)) {
			$this->load->model('www/order_model', 'order');
			$res_order = $this->order->GetInfo(array('order_number' => $order_number));
			if (!$res_order) {
				return $this->api->_msg('没有此订单');
			} else {
				$this->api->delOrder($res_order[0]['order_id']);
				return $this->api->_return('订单已删除', array('order_number' => $order_number), 'succ');
			}
		}
		return $this->api->_msg('没有订单编号');
	}

	/**
	 * info 返回内容
	 * @param int $id
	 * @param string $object
	 * @return array
	 */
	private function _info($id, $object) {
		if ($id == 0) //更新如果数据没有改动返回0
			return $id;

		$info = array();
		$result = $this->db->get_where('tc_' . $object, array($object . '_id' => $id))->row_array();
		switch ($object) {
			case 'order':
				$info['order_number'] = $result['order_number']; //订单编号
				if (isset($result['order_agreement_no'])) //合同编号
					$info['order_agreement_no'] = $result['order_agreement_no'];
				return $info;
				break;
			case 'salespay':
				return $info;
				break;
		}
	}

	//代办
	private function _task($data, $object, $res_id) {
		$this->load->model('www/message_model', 'message');
		$this->load->model('www/order_model', 'order');

		switch ($object) {
			case 'salespay':
				//此处添加待办信息
				if ($data['salespay_status'] == 1001) {
					$order_data = $this->order->id_aGetInfo($data['salespay_order_id']);
					//cus lee 待办需要分不同类型给不同待办 start
					switch ($data['salespay_money_type_name']) {
						case '1001':
							//日志
							$system_log_operation = '新增入款项';
							$message_owner = $order_data['order_finance'];
							$message_module = 'salespay';
							$message_type = '1001'; //确认到账
							$message_url = 'www/salespay/ajax_confirm_salespay';
							break;
						case '1002':
							//日志
							$system_log_operation = '新增返点款项';
							$message_owner = $data['salespay_reviewer'];
							$message_module = 'rebate';
							$message_type = '1020'; //出账审核
							$message_url = 'www/salespay/ajax_check_salespay';
							break;
						case '1003':
							if (isset($_POST['goods_info'])) {
								$salespay_data['salespay_refund_product_json'] = json_encode($_POST['goods_info']);
							}
							//日志
							$system_log_operation = '新增退款项';
							$message_owner = $salespay_data['salespay_reviewer'];
							$message_module = 'refund';
							$message_type = '1020'; //出账审核
							$message_url = 'www/salespay/ajax_check_salespay';
							break;
					}
					//cus lee 待办需要分不同类型给不同待办 end
					$message_data = array(
						'message_owner' => $message_owner,
						'message_module' => $message_module,
						'message_module_id' => $res_id,
						'message_type' => $message_type,
						'message_department' => $order_data['order_finance_arr']['user_department'],
						'message_url' => $message_url
					);
					$this->message->add($message_data);
				}
				//待办添加结束
				break;
			case 'invoice':
				//添加待办
				if ($data['invoice_status'] == 1001) {
					$order_data = $this->order->id_aGetInfo($data['invoice_order_id']);
					//p($order_data);
					$this->load->model('www/message_model', 'message');
					$message_data = array(
						'message_owner' => $order_data['order_finance'],
						'message_module' => 'invoice',
						'message_module_id' => $res_id,
						'message_type' => 1002,
						'message_url' => 'www/invoice/confirm_invoice',
						'message_department' => $order_data['order_finance_arr']['user_department']
					);
					//p($message_data);
					$this->message->add($message_data);
					//echo $this->db->last_query();
				}
				break;
			default:
				break;
		}
	}

	//2014-07-17 api end
//  注释老接口 2014-07-17 9:19:22
//	/**
//	 * 默认接口地址 post
//	 *
//	 * @return string
//	 */
//	public function index() {
//		error_log(print_r(array('Date' => date('Y-m-d H:i:s'), 'POST' => $_POST, 'GET' => $_GET), true) . "\n", 3, dirname(__FILE__) . '/../../' . '/logs/' . date("Y-m-d_H") . ".log");
//		if ($_POST) {
//			//解析传过来的值
//			$diff = array_diff(array('user', 'pwd', 'action', 'object', 'data'), array_keys($_POST));
//			if ($diff)
//				die(json_encode(array('res' => 'fail', 'msg' => '缺少参数', 'info' => $diff)));
//			//用户验证
//			if ($this->api->userVerify($_POST['user'], $_POST['pwd'])) {
//				$action = $_POST['action'];
//				$object = $_POST['object'];
//				$data = $_POST['data'];
//				switch ($action) {
//					case 'add':
//						$res = $this->api->api_add($data, $object, $_POST['user']);
//						die($res);
//						break;
//					case 'save':
//						$res = $this->api->api_save($data, $object, $_POST['user']);
//						die($res);
//						break;
//					case 'old_add':
//						$res = $this->api->add($data, $object, $_POST['user']);
//						die($res);
//						break;
//					case 'update':
//						$res = $this->api->update($data, $object, $_POST['user']);
//						die($res);
//						break;
//					case 'getSaasOrder':
//						$res = $this->api->getSaasOrder($data, $_POST['user']);
//						die($res);
//						break;
//					default :
//						$this->load->view('www/api');
//						break;
//				}
//			} else
//				die(json_encode(array('res' => 'fail', 'msg' => '没有权限，请检查账号密码')));
//		} else
//			$this->load->view('www/api');
//	}

	/**
	 * 从产品中心抓取商品
	 *
	 * @param string  $starttime 开始时间
	 * @return string error / finish
	 */
	public function goods($starttime = '', $endtime = '') {
		set_time_limit(0);
		$this->load->model('www/goods_model', 'goods');
		$this->load->model('www/bgrelation_model', 'bgrelation');
		$this->load->model('www/ptrelation_model', 'ptrelation');
		$this->load->model('www/gprelation_model', 'gprelation');
		$this->load->model('www/goods_price_model', 'goods_price');
		$this->load->model('www/product_tech_model', 'product_tech');
		$this->load->model('www/product_basic_model', 'product_basic');
		$this->load->model('www/product_oparam_model', 'product_oparam');
		
		$task_id = $_GET['id'];
		$task = $this->timing_task->id_aGetInfo($task_id);
		//$starttime = strtotime($task['timing_task_do_time']) - 1*60*60;//正式
		//$start_date = date("Y-m-d H:i:s", $starttime);//正式
		$starttime = strtotime("2014-12-05 00:00:00");
		$start_date = date("Y-m-d H:i:s", $starttime);
		//$endtime = time();//正式
		//$enddate = date("Y-m-d H:i:s",$endtime);//正式
		$endtime = strtotime("2014-12-05 23:59:59");
		$enddate = date("Y-m-d H:i:s",$endtime);

		// 定时任务
		/*
		if (isset($_GET['id'])) {
			$task_id = $_GET['id'];
			$this->load->model('www/timing_task_model', 'timing_task');
			$task = $this->timing_task->id_aGetInfo($task_id);
			if ($task) {
				$starttime = strtotime($task['timing_task_do_time']);
				if ($starttime <= time() - (5 * 60)) {
					$endtime = time();
					$save['timing_task_do_time'] = date('Y-m-d H:i:s', time());
					$this->timing_task->update($save, $task_id);
					error_log(print_r(array('Date' => date('Y-m-d H:i:s'), $starttime, $endtime), true) . "\n", 3, './logs/goods' . date("Ymd") . "log.log");
				} else
					error_log(print_r(array('Date' => date('Y-m-d H:i:s'), 'Time Interval'), true) . "\n", 3, './logs/goods' . date("Ymd") . "log.log");
			} else
				error_log(print_r(array('Date' => date('Y-m-d H:i:s'), 'None Task'), true) . "\n", 3, './logs/goods' . date("Ymd") . "log.log");
		} else { // 普通方法，传递2个时间戳
			if (empty($starttime)) {
				$starttime = strtotime('2014-05-01'); //1362067200
			} elseif (empty($endtime) && $starttime > time()) {
				die('finish');
			}
			if (empty($endtime))
				$endtime = $starttime + 1000000;
			$url = 'api/goods/' . $endtime;
		}
		*/
		
		$goods_arr = $this->api->getGoods($starttime, $endtime);
		
		//timing add log is update 2014/08/28
		$goods_arr['getDepositOrder_timing_task_do_time'] = $start_date;
		$goods_arr['getDepositOrder_end_time'] = $enddate;
		$date = date("Y-m-d H:i:s");
		$api_log_data = array(
					"api_log_addtime" => $date,
					"api_log_method"  => __FUNCTION__,
					"api_log_data"    => json_encode($goods_arr),
					"api_log_status"  => 1001,
					"api_log_result"  => "timing is success",
				);

		$this->api_log->add( $api_log_data );
		unset($goods_arr['getDepositOrder_timing_task_do_time']);
		unset($goods_arr['getDepositOrder_end_time']);
		

		p($goods_arr);
		if ($goods_arr) { //查询商品信息
			foreach ($goods_arr as $gk => $gv) {
				// goods_type 0：标准商品，1：捆绑商品、套餐商品
				if ($gv['goods_type'] == 0 && !isset($gv['rel_goods'])) { //标准商品 //group_0009 出错 是捆绑商品不是标准商品
					$gv['goods_is_trail'] = $gv['goods_is_trail'] = $gv['goods_is_sale'] = $gv['goods_type'] = $gv['goods_check_pay'] = $gv['goods_trail_days'] = 0;
					$gv['goods_visable'] = $gv['goods_visable'] == '1' ? '是' : '否';
					$gv['goods_is_trail'] = $gv['goods_is_trail'] == '1' ? '是' : '否';
					$gv['goods_is_sale'] = $gv['goods_is_sale'] == '1' ? '是' : '否';
					$gv['goods_type'] = $gv['goods_type'] == '1' ? '捆绑商品' : '标准商品';
					$gv['goods_check_pay'] = $gv['goods_check_pay'] == '1' ? '周期性' : '一次性';
					$enum_sale_type = array('1901' => '1001', '1902' => '1002', '1903' => '1003', '1904' => '1004', '1905' => '1005', '1906' => '1006', '1907' => '1007', '1908' => '1008', '1909' => '1009');
					if (!empty($gv['goods_sale_type'])) {
						if (strpos($gv['goods_sale_type'], ',')) {
							$gv['goods_sale_type'] = explode(',', $gv['goods_sale_type']);
							$sale_type = '';
							foreach ($gv['goods_sale_type'] as $v) {
								$sale_type .= ',' . $enum_sale_type[$v];
							} $sale_type = ltrim($sale_type, ',');
						} else {
							$sale_type = $enum_sale_type[$gv['goods_sale_type']];
						}
					} else
						$sale_type = '';

					$goodsArr = array(
						'code' => $gv['goods_code'], //商品编码
						'name' => $gv['goods_name'], //商品名称
						'desc' => $gv['goods_desc'], //商品简介
						'visable.name' => $gv['goods_visable'], //是否启用
						'is_trail.name' => $gv['goods_is_trail'], //是否可试用
						'is_sale.name' => $gv['goods_is_sale'], //是否允许单独售卖
						'sale_type' => $sale_type, //售卖方分类(1901：直销团队；1902：渠道团队；1903：协运营团队；1904：电商人才中心；1905：海外及政府团队；1906：开发伙伴部；1907：大客户协调部；)
						'trail_days' => $gv['goods_trail_days'], //试用天数（天）
						'type.name' => $gv['goods_type'], //是标准商品或者捆绑商品（0：标准商品，1：捆绑商品）
						'check_pay.name' => $gv['goods_check_pay'], //计费方式（1：周期性，0：一次性）
						'utime' => date('Y-m-d H:i:s', $gv['goods_utime']) //商品信息更新时间
					);

					$res_goods = $this->goods->api_add($goodsArr);
					if ($res_goods['res'] == 'fail') {
						$this->api->apiLog('fail', 'goods add error', $res_goods);
						break;
					}

					if (isset($gv['prosuite'])) {
						foreach ($gv['prosuite'] as $sk => $sv) {
							//技术产品
							$tech_arr = $this->api->getTechProduct($sv['product_code']);
							$techArr = array(
								'code' => $tech_arr['product_code'], //产品编号
								'name' => $tech_arr['product_name'] //产品名称
							);
							$res_tech = $this->product_tech->api_add($techArr);
							if ($res_tech['res'] == 'fail') {
								$this->api->apiLog('fail', 'product tech add error', $res_tech);
								break;
							}

							//基础产品
							$basic_arr = $this->api->getBasicProduct($sv['prosuite_code']);
							$enum_categor = array('1501' => 'SASS', '1502' => 'PS', '1503' => 'Other');
							$enum_sub_category = array('1601' => '前端', '1602' => '后端', '1603' => '数据', '1604' => 'PS', '1605' => 'LIC', '1606' => 'VAS', '1607' => 'Other');
							$enum_sales_type = array('1101' => '单价模式', '1102' => '服务模式', '1103' => '混合模式');
							$basicArr = array(
								'code' => $basic_arr['suite_code'], //产品编号
								'name' => $basic_arr['suite_name'], //产品名称
								'category.name' => $enum_categor[$basic_arr['suite_category']],
								'sub_category.name' => $enum_sub_category[$basic_arr['suite_sub_category']],
								'sales_type.name' => $enum_sales_type[$basic_arr['suite_sales_type']],
								'domain_cost' => $basic_arr['suite_domain_cost'],
								'sms_cost' => $basic_arr['suite_sms_cost'],
								'idc_cost' => $basic_arr['suite_idc_cost'],
								'utime' => date('Y-m-d H:i:s', $basic_arr['suite_utime'])
							);
							$res_basic = $this->product_basic->api_add($basicArr);
							if ($res_basic['res'] == 'fail') {
								$this->api->apiLog('fail', 'product basic add error', $res_basic);
								break;
							}

							//技术商品与基础商品关系
							$ptArr = array(
								'product_tech_id' => $res_tech['info']['product_tech_id'],
								'product_basic_id' => $res_basic['info']['product_basic_id']
							);
							$res_pt = $this->ptrelation->api_add($ptArr);
							if ($res_pt['res'] == 'fail') {
								$this->api->apiLog('fail', 'ptrelation add error', $res_pt);
								break;
							}

							//比例关系
							$gpArr = array(
								'goods_id' => $res_goods['info']['goods_id'],
								'product_tech_id' => $res_tech['info']['product_tech_id'],
								'product_basic_id' => $res_basic['info']['product_basic_id'],
								'rate' => $sv['rate'] //比例
							);
							$res_gp = $this->gprelation->api_add($gpArr);
							if ($res_gp['res'] == 'fail') {
								$this->api->apiLog('fail', 'gprelation add error', $res_gp);
								break;
							}

							//开通参数，不更新
							if (isset($basic_arr['open_params'])) {
								foreach ($basic_arr['open_params'] as $ok => $ov) {
									$oparamArr = array(
										'product_basic_id' => $res_basic['info']['product_basic_id'],
										'name' => $ov['params_name'],
										'type.name' => 'open_params',
										'content' => json_encode($ov),
										'atime' => date('Y-m-d H:i:s', $ov['params_atime']),
										'utime' => date('Y-m-d H:i:s', $ov['params_utime'])
									);
									$res_oparam = $this->product_oparam->api_add($oparamArr);
									if ($res_oparam['res'] == 'fail')
										die('product oparams add error');
								}
							}

							//返回参数，不更新
							if (isset($basic_arr['return_params'])) {
								foreach ($basic_arr['return_params'] as $rk => $rv) {
									$rparamArr = array(
										'product_basic_id' => $res_basic['info']['product_basic_id'],
										'name' => $rv['params_name'],
										'type.name' => 'return_params',
										'content' => json_encode($rv),
										'atime' => date('Y-m-d H:i:s', $rv['params_atime']),
										'utime' => date('Y-m-d H:i:s', $rv['params_utime'])
									);
									$res_rparam = $this->product_oparam->api_add($rparamArr);
									if ($res_rparam['res'] == 'fail')
										die('product oparams add error');
								}
							}
						}
					}

					//价格信息
					if (isset($gv['gprice'])) {
						foreach ($gv['gprice'] as $gpk => $gpv) {
							$gpv['gp_visable'] = $gpv['gp_visable'] == '1' ? '是' : '否';
							$gpv['gp_start_price'] = empty($gpv['gp_start_price']) ? 0.00 : $gpv['gp_start_price'];
							$enum_cycle_unit = array('1401' => '1001', '1402' => '1002', '1403' => '1003');
							$gpriceArr = array(
								'goods_id' => $res_goods['info']['goods_id'],
								'code' => $gpv['gp_code'], //价格编号
								'visable.name' => $gpv['gp_visable'], //是否启用
								'start_price' => $gpv['gp_start_price'], //一次性计费的价格
								'cycle_days' => $gpv['gp_cycle_days'], //周期性计费的周期
								'cycle_unit' => $enum_cycle_unit[$gpv['gp_cycle_unit']], //周期单位（1401：年，1402：月，1403：天）
								'cycle_price' => $gpv['gp_cycle_price'], //周期性计费的价格
								'effective_days' => $gpv['gp_effective_days'] //有效期时间（天）
							);
							$res_gprice = $this->goods_price->api_add($gpriceArr);
							if ($res_gprice['res'] == 'fail') {
								$this->api->apiLog('fail', 'product goods_price add error', $res_gprice);
								break;
							}
						}
					}

					if ($res_goods['res'] == 'fail' || $res_tech['res'] == 'fail' || $res_basic['res'] == 'fail' || $res_gprice['res'] == 'fail')
						die($starttime . ' <-> ' . $endtime . ' ' . $res['msg'] . ' 出错');
				} else { //捆绑商品
					if (isset($gv['rel_goods'])) { //相关商品
						$gv['goods_visable'] = $gv['goods_visable'] == '1' ? '是' : '否';
						$gv['goods_type'] = $gv['goods_type'] == '1' ? '捆绑商品' : '标准商品';
						$goodsArr = array(
							'code' => $gv['goods_code'], //商品编码
							'name' => $gv['goods_name'], //商品名称
							'desc' => $gv['goods_desc'], //商品简介
							'visable.name' => $gv['goods_visable'], //是否启用
							'type.name' => $gv['goods_type'], //是标准商品或者捆绑商品（0：标准商品，1：捆绑商品）
							'utime' => date('Y-m-d H:i:s', $gv['goods_utime']), //商品信息更新时间
							'price' => $gv['price'] //套餐售价
						);
						$res_goods = $this->goods->api_add($goodsArr);
						if ($res_goods['res'] == 'fail') {
							$this->api->apiLog('fail', 'goods add error', $res_goods);
							break;
						}

						foreach ($gv['rel_goods'] as $rk => $rv) {
							$res_g = $this->db->select('goods_id')->get_where('tc_goods', array('goods_code' => $rv['goods_code']))->row_array();
							$res_goods_price = $this->db->select('goods_price_id')->get_where('tc_goods_price', array('goods_price_code' => $rv['gp_code']))->row_array();

							if (!$res_g || !$res_goods_price) {
								$this->api->apiLog('fail', '捆绑商品没有子商品信息 goods_code[' . $rv['goods_code'] . '] goods_price_code[' . $rv['gp_code'] . ']');
								break;
							}
							//比例关系
							$bgArr = array(
								'bundle_id' => $res_goods['info']['goods_id'], //商品id
								'goods_id' => $res_g['goods_id'], //子商品id
								'goods_price_id' => $res_goods_price['goods_price_id'], //对应商品价格编号
								'group_price' => $rv['group_price'] //组合价格
							);
							$res_bg = $this->bgrelation->api_add($bgArr);
							if ($res_bg['res'] == 'fail') {
								$this->api->apiLog('fail', 'bgrelation add error', $res_bg);
								break;
							}
						}
					}
				}
			}

			echo date('Y-m-d', $starttime) . ' <-> ' . date('Y-m-d', $endtime) . ' OK';
			//usleep(500);
			
			$this->timing_check($data,$task_id,$new_date,$enddate);
			
			//redirect($url, 'refresh');
		} else { //没有返回内容，继续抓取数据
			echo date('Y-m-d h:m', $starttime) . ' <-> ' . date('Y-m-d h:m', $endtime) . ' 没有数据 ' . $starttime;
			//usleep(500);
			$this->timing_check($data,$task_id,$new_date,$enddate);
			//redirect($url, 'refresh');
		}
	}

	// 抓取预收款订单
	public function getDepositOrder() {
		set_time_limit(0);
		$task_id = $_GET['id'];
		$task = $this->timing_task->id_aGetInfo($task_id);
		$end_time = date('Y-m-d H:i:s', time());

		$start_payed = strtotime($task['timing_task_do_time']) - 1*60*60;
		$start_date = date("Y-m-d H:i:s", $start_payed);

		//$starttime = strtotime($task['timing_task_do_time']) - 1*60*60;//正式
		//$start_date = date("Y-m-d H:i:s", $starttime);//正式
		$starttime = strtotime("2014-12-05 00:00:00");
		$start_date = date("Y-m-d H:i:s", $starttime);
		//$endtime = time();//正式
		//$enddate = date("Y-m-d H:i:s",$endtime);//正式
		$endtime = strtotime("2014-12-05 23:59:59");
		$end_time = date("Y-m-d H:i:s",$endtime);
		
		
		
		
		$params = array(
						'start_time' => $start_date,
						'end_time' => $end_time,
						'page_no' => 1,
						'page_size' => 100
					);
		// 定时任务
		/*
		if (isset($_GET['id'])) {
			$task_id = $_GET['id'];
			//$this->load->model('www/timing_task_model', 'timing_task');
			//
			
			$task = $this->timing_task->id_aGetInfo($task_id);
			if ($task) {
				$starttime = strtotime($task['timing_task_do_time']);
				if ($starttime <= time() - (60 * 60)) {
					$params = array(
						'start_time' => $task['timing_task_do_time'],
						'end_time' => date('Y-m-d H:i:s', time()),
						'page_no' => 1,
						'page_size' => 100
					);
					$save['timing_task_do_time'] = date('Y-m-d H:i:s', time());
					$this->timing_task->update($save, $task_id);
					error_log(print_r(array('Date' => date('Y-m-d H:i:s'), $params), true) . "\n", 3, './logs/depositOrder' . date("Ymd") . "log.log");
				} else
					error_log(print_r(array('Date' => date('Y-m-d H:i:s'), 'Time Interval'), true) . "\n", 3, './logs/depositOrder' . date("Ymd") . "log.log");
			} else
				error_log(print_r(array('Date' => date('Y-m-d H:i:s'), 'None Task'), true) . "\n", 3, './logs/depositOrder' . date("Ymd") . "log.log");
		} else {
			$params = array(
				'start_time' => '2014-05-01',
				'end_time' => '2014-06-01',
				'page_no' => 1,
				'page_size' => 100
			);
		}
		*/
		
		$url = 'api/goldenstorage/recharge/getfinishlist';
		$result = $this->api->getOpenApi($url, $params);
		p($result);
		//timing add log is update 2014/08/28
		$result['getDepositOrder_timing_task_do_time'] = $start_date;
		$result['getDepositOrder_end_time'] = $end_time;
		$date = date("Y-m-d H:i:s");
		$api_log_data = array(
					"api_log_addtime" => $date,
					"api_log_method"  => __FUNCTION__,
					"api_log_data"    => json_encode($result),
					"api_log_status"  => 1001,
					"api_log_result"  => "timing is success",
				);

		$this->api_log->add( $api_log_data );
		unset($result['getDepositOrder_timing_task_do_time']);
		unset($result['getDepositOrder_end_time']);
		

		if ($result['status'] == 'success') {
			//抓取
			$result = $result['data'];
			$orders = $result['list'];
			$pages = intval($result['count'] / 100);
			if ($result['count'] % 100)
				$pages++;
			if ($pages > 1) {
				for ($i = 2; $i <= $pages; $i++) {
					$params['page_no'] = $i;
					$result = $this->api->getOpenApi($url, $params);
					$result = $result['data'];
					$orders = array_merge($orders, $result['list']); //整合订单
				}
			}

			//处理
			if ($orders) {
				foreach ($orders as $k => $v) {
					$res = $this->api->saveDepositOrder($v);
				}
			}
			
			
			$this->timing_check($data,$task_id,$new_date,$end_time);
			
		} else {
			$this->timing_check($data,$task_id,$new_date,$end_time);
			return 'get order error';
		}
	}

	// 抓取云生意订单
	public function getSaasOrder() {
		set_time_limit(0);
		$task_id = $_GET['id'];
		$task = $this->timing_task->id_aGetInfo($task_id);
		//$enddate = date('Y-m-d H:i:s', time());
		//$start_payed = strtotime($task['timing_task_do_time']) - 1*60*60;
		//$start_date = date("Y-m-d H:i:s", $start_payed);
		
		
		//$starttime = strtotime($task['timing_task_do_time']) - 1*60*60;//正式
		//$start_date = date("Y-m-d H:i:s", $starttime);//正式
		$starttime = strtotime("2014-12-05 00:00:00");
		$start_date = date("Y-m-d H:i:s", $starttime);
		//$endtime = time();//正式
		//$enddate = date("Y-m-d H:i:s",$endtime);//正式
		$endtime = strtotime("2014-12-05 23:59:59");
		$enddate = date("Y-m-d H:i:s",$endtime);
		
		
		$params = array(
						'start_payed' => $start_date,
						'end_payed' => $enddate,
						'page_no' => 1,
						'page_size' => 100
					);
		// 定时任务
		/*
		if (isset($_GET['id'])) {
			$task_id = $_GET['id'];
			$this->load->model('www/timing_task_model', 'timing_task');
			$task = $this->timing_task->id_aGetInfo($task_id);

			if ($task) {
				$starttime = strtotime($task['timing_task_do_time']);
				if ($starttime <= time() - (60 * 60)) {
					$params = array(
						'start_payed' => $task['timing_task_do_time'],
						'end_payed' => date('Y-m-d H:i:s', time()),
						'page_no' => 1,
						'page_size' => 100
					);
					$save['timing_task_do_time'] = date('Y-m-d H:i:s', time());
					$this->timing_task->update($save, $task_id);
					error_log(print_r(array('Date' => date('Y-m-d H:i:s'), $params), true) . "\n", 3, './logs/saasOrder' . date("Ymd") . "log.log");
				} else
					error_log(print_r(array('Date' => date('Y-m-d H:i:s'), 'Time Interval'), true) . "\n", 3, './logs/saasOrder' . date("Ymd") . "log.log");
			} else
				error_log(print_r(array('Date' => date('Y-m-d H:i:s'), 'None Task'), true) . "\n", 3, './logs/saasOrder' . date("Ymd") . "log.log");
		} else {
			$params = array(
				'start_payed' => '2014-05-01',
				'end_payed' => '2014-06-01',
				'page_no' => 1,
				'page_size' => 100
			);
		}
		*/
		$url = 'api/saas/order.search';
		$result = $this->api->getOpenApi($url, $params);

		p($result);
		//timing add log is update 2014/08/28
		$result['getDepositOrder_timing_task_do_time'] = $start_date;
		$result['getDepositOrder_end_time'] = $enddate;
		$date = date("Y-m-d H:i:s");
		$api_log_data = array(
					"api_log_addtime" => $date,
					"api_log_method"  => __FUNCTION__,
					"api_log_data"    => json_encode($result),
					"api_log_status"  => 1001,
					"api_log_result"  => "timing is success",
				);

		$this->api_log->add( $api_log_data );
		unset($result['getDepositOrder_timing_task_do_time']);
		unset($result['getDepositOrder_end_time']);
		
		

		if ($result['status'] == "success") {
			
			//抓取
			$result = $result['data']['ORDER_SEARCH_RESPONSE'];
			$orders = $result['orders'];
			$pages = intval($result['total_results'] / $params['page_size']);
			if ($result['total_results'] % $params['page_size'])
				$pages++;
			if ($pages > 1) {
				for ($i = 2; $i <= $pages; $i++) {
					$params['page_no'] = $i;
					@ $result = $this->api->getOpenApi($url, $params);
					$result = $result['data']['ORDER_SEARCH_RESPONSE'];
					$orders = array_merge($orders, $result['orders']);
				}
			}
			
			//处理
			if ($orders) {
				// 云生意商品对照
				$goods_mapping = array(
					// 问题商品，价格无法定义
					// 'good_ysy_standard' => array(1 => 'goods_0351', 12 => 'goods_0391'),
					'good_sms5k60_std' => array('all' => 'goods_0392'),
					'good_edm200k90_std' => array('all' => 'goods_0393'),
					'good_zsw_base' => array('all' => 'goods_0395'),
					'good_485auth_all' => array('all' => 'goods_0319'),
					'group_0058' => array('all' => 'group_0058'),
					'good_ym1w_std' => array('all' => 'goods_0537'),
					'good_ympv3w_std' => array('all' => 'goods_0499'),
					'good_ympv10w_std' => array('all' => 'goods_0538'),
					'good_ympv20w_std' => array('all' => 'goods_0539'),
				);

				$this->load->model('www/goods_model', 'goods');
				foreach ($orders as $k => $v) {
					if ($v["total_pay_fee"] > 0) { //不获取金额小于等于0的数据
						if (in_array($v['goods_id'], array_keys($goods_mapping))) {
							$v['goods_id'] = $goods_mapping[$v['goods_id']]['all'];
						} elseif ($this->goods->checkGoodsCode($v['goods_id'])) {
							$this->api->saveSaasOrder($v);
						}
					}
				}
				$this->timing_check($data,$task_id,$new_date,$enddate);
				
			}
			else
			{
				
				$this->timing_check($data,$task_id,$new_date,$enddate);
			}
			
		} else {
			$this->timing_check($data,$task_id,$new_date,$enddate);
			return 'get order error';
		}
	}
	
	/***
	* 说明:查询数据接口 2014-08-21
	* @return json 返回数据或者状态值
	*/
	public function api_index() {

		if($_POST) {
			
			//解析传过来的数据并且对比数据键值是否一致,不一致则返回
			$diff = array_diff(array('action','object','user','pwd','where','rel','page','perNumber'),array_keys($_POST));
			if($diff) {
				$this->api->_msg("缺少参数");
			}
			
			//用户验证
			if ($this->api->userVerify($_POST['user'], $_POST['pwd'])) {
				
				extract($_POST, EXTR_OVERWRITE);
				//检查数据是否正确
				$where = is_array($where) ? $where : json_decode($where, true);
				if (empty($where)) {
					$this->api->_msg('数据异常');
				}
				
				$data = array();//定义数组存储处理过后的数据
				//创建日志
				$logData = array(
					'api_log_addtime' => date('Y-m-d H:i:s'),
					'api_log_method' => $user . ' | ' . $action . ' -> ' . $object,
					'api_log_data' => json_encode($where)
				);
				$res_log = $this->api_log->add($logData);
				$log_id = $this->db->insert_id();
				//'where_all', 'rel_all', 'page', 'perNumber', 'obj'
				//重新生成数组,做数据处理
				unset($_POST['user']);//释放用户名变量
				unset($_POST['pwd']); //释放密  码变量
				
				$data['where_all'] = $where;
				$data['rel_all'] = $rel;
				$data['page'] = $page;
				$data['perNumber'] = $perNumber;
				$data['obj'] = $object;
				$action = "api_".$action;//执行动作
				$result = $this->api->$action($data); //执行
				die();
				if (!$result) {
					$this->api->_msg('查询出错');
				} else {
					die(json_encode($result));
				}
			} else {
				$this->api->_msg("账号密码验证失败");
			}
		} else {
			$this->api->_msg(NULL);
		}
	}
	
	//更新定时任务表状态和时间
	private function timing_check($data,$task_id,$new_date,$enddate,$token="1001")
	{
		$new_date = date("Y-m-d H:i:s",time() + 1*60*60);
		$data = array("timing_task_next_time"=>$new_date,"timing_task_do_time"=>$enddate,"timing_token"=>$token);
		$this->timing_task->update($data,$task_id);
	}

	//360safe
	private function StopAttack($StrFiltKey, $StrFiltValue, $ArrFiltReq) {
		$StrFiltValue = $this->arr_foreach($StrFiltValue);
		if (preg_match("/" . $ArrFiltReq . "/is", $StrFiltValue) == 1) {
			$this->slog("<div style=\"padding:10px;\">操作IP: " . $_SERVER["REMOTE_ADDR"] . "<br>操作时间: " . strftime("%Y-%m-%d %H:%M:%S") . "<br>操作页面:" . $_SERVER["PHP_SELF"] . "<br>提交方式: " . $_SERVER["REQUEST_METHOD"] . "<br>提交参数: " . $StrFiltKey . "<br>提交数据: " . $StrFiltValue . "</div>");
			die(json_encode(array('res' => 'fail', 'msg' => '您的提交带有不合法参数')));
			//   die('您的提交带有不合法参数');
		}
		if (preg_match("/" . $ArrFiltReq . "/is", $StrFiltKey) == 1) {
			$this->slog("<div style=\"padding:10px;\">操作IP: " . $_SERVER["REMOTE_ADDR"] . "<br>操作时间: " . strftime("%Y-%m-%d %H:%M:%S") . "<br>操作页面:" . $_SERVER["PHP_SELF"] . "<br>提交方式: " . $_SERVER["REQUEST_METHOD"] . "<br>提交参数: " . $StrFiltKey . "<br>提交数据: " . $StrFiltValue . "</div>");
			die(json_encode(array('res' => 'fail', 'msg' => '您的提交带有不合法参数')));
			//   die('您的提交带有不合法参数');
		}
	}

	//360safe
	private function arr_foreach($arr) {
		static $str;
		if (!is_array($arr)) {
			return $arr;
		}
		foreach ($arr as $key => $val) {
			if (is_array($val)) {
				$this->arr_foreach($val);
			} else {
				$str[] = $val;
			}
		}
		return implode($str);
	}

	//360safe
	private function slog($logs) {
		$toppath = $_SERVER["DOCUMENT_ROOT"] . "/" . date('Y-d-m', time()) . "_safelog.htm";
		$Ts = fopen($toppath, "a+");
		fputs($Ts, $logs . "\r\n");
		fclose($Ts);
	}

	public function __call($method, $args) {
		$this->api->_msg('error action');
		if (method_exists($this, $method)) {
			return call_user_func_array(array($this, $method), $args);
		} else {
			throw new Exception(sprintf('The required method "%s" does not exist for %s', $method, get_class($this)));
		}
	}

}

?>
