<?php

class Demo extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function add() {
		include("lib/snoopy.inc.php");
		$snoopy = new snoopy();
		$url = 'http://192.168.31.41:9888/newpbi/index.php/api';

		//新分销同步账号
		$params = array('user' => 'ndpSync', 'pwd' => '0fe11d6036b2b382', 'action' => 'add', 'object' => 'order');
		$params['data'] = '{"type.name":"\u9500\u552e\u8ba2\u5355","create_user_id.gonghao":"s0329","create_time":"2014-09-15  17:43:30","agreement_no":"SDM2014052900005","amount":"4000.00","owner.gonghao":"s1844","account.account_shopexid":"88140501192620","finance.gonghao":"s1631","review.gonghao":"s0357","rebate_amount":"","subsidiary.name":"\u5546\u6d3e\u7f51\u7edc","cost_400":"","cost_design":"","cost_dsf":"","cost_outsourcing":"","agreement_name":"\u77f3\u5ca9","agreement_note":"\u4e8e\u603b\u5ba1\u6279","typein.login_name":"kuangpeipei","if_renew":"1001","detailed":{"order_d":[{"goods.code":"goods_0439","goods_amount":"4000.00","goods_price.code":"gp00000000508"}]}}';
//		$params['data'] = '{"number":"KX20140716143823739D1243","type.name":"\u9884\u6536\u6b3e\u8ba2\u5355","create_time":"2014-07-16 14:38:23","deposit_paytype.name":"\u8d27\u6b3e","amount":1000,"rebate_amount":0,"agent.shopex_id":"JXS043013374641C33","owner.gonghao":"s0031","review.gonghao":"s0031","finance.gonghao":"s1511","source.name":"\u6e20\u9053","detailed":{"salespay":[{"money_type.name":"\u5165\u8d26\u8bb0\u5f55","number":"KX20140716143823739D1243","pay_amount":1000,"pay_date":"2014-07-16 14:37","pay_note":" ","account_bank_account":"","account_bank_account_name":"","account_bank_name":" ","my_bank_account.name":" ","alipay_order":"\u6d4b\u8bd5\u63a5\u53e3","account_alipay_account":"222@126.com","my_alipay_account.name":"payment@shopex.cn","status.name":"\u672a\u786e\u8ba4\u5230\u5e10","pay_method.name":"\u652f\u4ed8\u5b9d"}]}}';
//		$params['data'] = '{"number":"XS2014071615403013837E12C1","type.name":"\u63d0\u8d27\u9500\u552e\u8ba2\u5355","if_renew.name":"\u5426","create_user_id.gonghao":"s0230","create_time":"2014-07-16 15:40:30","amount":13580,"rebate_amount":0,"agent.shopex_id":"JXS043013374641C33","owner.gonghao":"s0230","review.gonghao":"s0031","nreview.gonghao":"s1511","finance.gonghao":"s1511","department.name":"\u6e20\u9053\u4f19\u4f34\u90e8","source.name":"\u6e20\u9053","remarks":" ","detailed":{"order_d":[{"goods.code":"goods_0529","goods_amount":13580,"goods_price.code":"gp00000000612","number":"KC20140716154155844C5112"}],"rel_order_order":[{"rel_order.number":"doKX201407161504552273F77F"},{"rel_order.number":"doKX201406100957102832C829"}]}}';
//		$params['data'] = '{"number":"KX20140716175859245111DB","type.name":"预收款订单","create_time":"2014-07-16 17:58:59","deposit_paytype.name":"货款","amount":1E+07,"rebate_amount":0.0,"agent.shopex_id":"JXS043013374641C33","owner.gonghao":"s0031","review.gonghao":"s0031","finance.gonghao":"s1511","source.name":"渠道","detailed":{"salespay":[{"money_type.name":"入账记录","number":"KX20140716175859245111DB","pay_amount":1E+07,"pay_date":"2014-07-16 17:58","pay_note":" ","account_bank_account":"123123123123","account_bank_account_name":"测试预存款接口","account_bank_name":"工商银行（个人）","my_bank_account.name":"工行（个人）","my_alipay_account.name":"payment@shopex.cn","status.name":"未确认到帐","pay_method.name":"汇款"}]}}';
		//短信同步账号
//		$params = array('user' => 'smsSync', 'pwd' => '9b358e9e238a954f', 'action' => 'add', 'object' => 'order');
//		$params['data'] = '{"number":"2014051502141860","type.name":"\u7ebf\u4e0a\u8ba2\u5355","create_time":"2014-05-15 14:28:30","amount":"670.00","account.account_shopexid":"111304676030","detailed":{"order_d":[{"goods.code":"goods_0377","goods_amount":"670.00","goods_price.code":""}],"salespay":[{"my_alipay_account.name":"sms_alipay@shopex.cn","alipay_order":"2014051556654187","money_type.name":"\u5165\u8d26\u8bb0\u5f55","money_type_name.name":"\u5165\u6b3e\u9879","pay_method.name":"\u652f\u4ed8\u5b9d","pay_date":"2014-05-15 14:29:33","status.name":"\u672a\u786e\u8ba4\u5230\u5e10","pay_amount":"670.00"}]},"owner.gonghao":"s1143","review.gonghao":"s0757","finance.gonghao":"s1861"}';
//		$params['data'] = '{"number":"201405150214860","type.name":"\u7ebf\u4e0a\u8ba2\u5355","create_time":"2014-05-15 14:28:30","amount":"670.00","account.account_shopexid":"111304676030","detailed":{"order_d":[{"goods.code":"goods_0377","goods_amount":"670.00","goods_price.code":""}],"salespay":[{"my_alipay_account.name":"sms_alipay@shopex.cn","alipay_order":"2014051556654187","money_type.name":"\u5165\u8d26\u8bb0\u5f55","money_type_name.name":"\u5165\u6b3e\u9879","pay_method.name":"\u652f\u4ed8\u5b9d","pay_date":"2014-05-15 14:29:33","status.name":"\u672a\u786e\u8ba4\u5230\u5e10","pay_amount":"670.00"}]},"owner.gonghao":"s1143","review.gonghao":"s0757","finance.gonghao":"s1861"}';
//		//模板堂同步账号
//		$params = array('user' => 'mbtSync', 'pwd' => '8fc35dff6b138dc9', 'action' => 'add', 'object' => 'order');
//		//合同同步账号
//		$params = array('user' => 'htSync', 'pwd' => '36c7ae5c7448695f', 'action' => 'add', 'object' => 'order');
//		$params['data'] = '{"type.name": "销售订单","create_user_id.gonghao": "s0329","create_time": "2014-06-20  10:10:13","agreement_no": "SDM22014060400006","amount": "23840.00","owner.gonghao": "s0829","account.account_shopexid": "88140501192485","finance.gonghao": "s1631","review.gonghao": "s1562","rebate_amount": "","agreement_name": "广州项氏安全防护科技有限公司","agreement_note": "于总审批","typein.login_name": "kuangpeipei","if_renew": "1001","detailed": {"order_d": [{"goods.code": "group_0071","goods_amount": "23840.00","goods_price.code": ""}]}}';
//		$params['data'] = '{"type.name": "销售订单","create_user_id.gonghao": "s0329","create_time": "2014-06-20  17:22:07","agreement_no": "SDM2014061800013","amount": "19000.00","owner.gonghao": "s1512","account.account_shopexid": "88140601197347","finance.gonghao": "s1631","review.gonghao": "s1520","rebate_amount": "","agreement_name": "上海金猫环保科技有限公司","agreement_note": "于总审批","typein.login_name": "kuangpeipei","if_renew": "1001","detailed": {"order_d": [{"goods.code": "goods_0463","goods_amount": "19000.00","goods_price.code": "gp00000000540"}]}}';
//		//CRM同步账号
//		$params = array('user' => 'crmSync', 'pwd' => '34eefcbcf822bff8', 'action' => 'add', 'object' => 'refund');
//		$params['data'] = '{"goods_info": "销售订单"}';
//		//云生意同步账号
//		$params = array('user' => 'ysySync', 'pwd' => 'dea34f072686a73c', 'action' => 'add', 'object' => 'order');
//		$params['data'] = json_encode($params['data']);
		$snoopy->submit($url, $params);
		$res = $snoopy->results;
		die($res);
		$res = json_decode(trim($res, chr(239) . chr(187) . chr(191)), true);
	}

	public function del() {
		include("lib/snoopy.inc.php");
		$snoopy = new snoopy();
		$url = 'http://192.168.56.1/newpbi/index.php/api';

		$params = array('user' => 'ndpSync', 'pwd' => '0fe11d6036b2b382', 'action' => 'del', 'object' => 'order');
		$params['data'] = array('order_number' => 'so201405070001');
		$params['data'] = json_encode($params['data']);
		$snoopy->submit($url, $params);
		$res = $snoopy->results;
		die($res);
	}

	//修改时间戳
	public function gai() {
		$sql = 'SELECT * FROM tc_applyopen WHERE applyopen_return_info LIKE "%actual_enddate\":14%"';
		$result = $this->db->query($sql)->result_array();
		foreach ($result as $k => $v) {
			$applyopen_return_info = json_decode($v['applyopen_return_info'], true);
			if (isset($applyopen_return_info['actual_enddate']))
				$applyopen_return_info['actual_enddate'] = date('Y-m-d H:i:s', $applyopen_return_info['actual_enddate']);
			if (isset($applyopen_return_info['actual_startdate']))
				$applyopen_return_info['actual_startdate'] = date('Y-m-d H:i:s', $applyopen_return_info['actual_startdate']);

			$this->db->where('applyopen_id', $v['applyopen_id']);
			$this->db->update('tc_applyopen', array('applyopen_return_info' => json_encode($applyopen_return_info)));
		}
		die('finish');
	}

	public function up() {
		include("lib/snoopy.inc.php");
		$snoopy = new snoopy();
		$url = 'http://192.168.56.1/newpbi/index.php/api';

		$params = array();
		$params['action'] = 'update';
		$params['object'] = 'salespay';
		$params['user'] = 'ndpSync';
		$params['pwd'] = '0fe11d6036b2b382';
//		$params['data'] = '{"number":"so2014071300023","type.name":"返点订单","create_user_id.gonghao":"s0486","create_time":"2014-07-09 10:07:56","amount":10728.0,"rebate_amount":10728.0,"agent.shopex_id":"330531590374","owner.gonghao":"s0486","review.gonghao":"s0031","nreview.gonghao":"s1511","finance.gonghao":"s1511","department.name":"渠道伙伴部","source.name":"渠道","rebates_remark":"2014财年6月份返点，金额为：107280*10%=10728元","detailed":{"salespay":[{"money_type.name":"出账记录","number":"XSFK20140709100756151652A5","pay_amount":10728.0,"account_bank_type.name":"企业","account_bank_account":"380559531294","account_bank_account_name":"义乌市优亿网络技术有限公司","account_bank_name":"中国银行义乌支行"a,"account_alipay_account":" ","status.name":"未确认到帐","pay_method.name":"汇款","money_type_name.name":"返点款项","reviewer.gonghao":"s0486"}],"rel_order_order":[{"rel_order.number":"so2014071300023"}]},"rebates_type.name":"季度返点"}';
//		$params['data'] = '{"money_type.name":"\u5165\u8d26\u8bb0\u5f55","number":"KX201407161524194257767B","pay_amount":1111,"pay_date":"2014-07-16 15:23","pay_note":" ","account_bank_account":"1111","account_bank_account_name":"\u6d4b\u8bd5\u5546\u6d3e","account_bank_name":"\u5de5\u5546\u94f6\u884c\uff08\u4e2a\u4eba\uff09","my_bank_account.name":"\u5de5\u5546\u94f6\u884c\uff08\u57fa\u672c\u6237\uff09","my_alipay_account.name":"payment@shopex.cn","status.name":"\u672a\u786e\u8ba4\u5230\u5e10","pay_method.name":"\u6c47\u6b3e"}';
		$params['data'] = '{"money_type.name":"\u5165\u8d26\u8bb0\u5f55","number":"KX201407161524194257767B","pay_amount":222333,"pay_date":"2014-07-16 15:23","pay_note":" ","account_bank_account":"1111","account_bank_account_name":"\u6d4b\u8bd5\u5546\u6d3e","account_bank_name":"\u5de5\u5546\u94f6\u884c\uff08\u4e2a\u4eba\uff09","my_bank_account.name":"\u5de5\u5546\u94f6\u884c\uff08\u57fa\u672c\u6237\uff09","my_alipay_account.name":"payment@shopex.cn","status.name":"\u672a\u786e\u8ba4\u5230\u5e10","pay_method.name":"\u6c47\u6b3e"}';
		$snoopy->submit($url, $params);
		$res = $snoopy->results;
		//p($res);
		exit;
	}

}

?>