<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Object extends MY_Controller{
	public $menu1 = 'cus';
	public $menu2 = 'object';

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->obj_list();
	}

	//列表
	public function obj_list(){
		$this->load->model('admin/object_model','obj');
		$this->obj->db->query('SET NAMES UTF8');
		$data["labels"]=$this->obj->attributeLabels();
		$data['list']=$this->obj->objlist();
		$this->render('admin/object/list',$data);
	}

	//新增
	public function add(){
		$this->load->model('admin/object_model','obj');
		$this->load->helper('form');
		$data["labels"]=$this->obj->attributeLabels();
		if($_POST!=null){
			$obj_data = $_POST["obj"];
			$obj_data['create_time'] = date("Y-m-d H:i:s");
			$obj_data['update_time'] = date("Y-m-d H:i:s");
			if (!isset($_POST['obj']['is_detail'])){
				$_POST['obj']['is_detail'] = '0';
			}
			$add_id = $this->obj->add($obj_data);

//添加明细
//			if ($_POST['obj']['is_detail']==1 and $_POST["obj_detail_number"]>=1 and $_POST["obj_detail_number"]<=5){
//				//这个条件也同时保证了不为空
//				for($i=1;$i<=$_POST["obj_detail_number"];$i++){
//					$obj_detail_data = $_POST["detail".$i];
//					$obj_detail_data['create_time']=date("Y-m-d H:i:s");
//					$obj_detail_data['update_time']=date("Y-m-d H:i:s");
//					$obj_detail_data['obj_uid']=$add_id;
//					$this->obj->add($obj_detail_data);
//				}
//			}
			success('admin/object/index','添加成功');
		}
		$this->render('admin/object/add',$data);
	}

	//编辑
	public function edit(){
		$this->load->model('admin/object_model','obj');
		if($_POST!=null){
			$_POST['obj']['update_time']=time();
            if (!isset($_POST['obj']['is_obj_type'])){
				$_POST['obj']['is_obj_type']='0';
			}
            if (!isset($_POST['obj']['is_ref_obj'])){
				$_POST['obj']['is_ref_obj']='0';
			}
            if (!isset($_POST['obj']['is_detail'])){
				$_POST['obj']['is_detail']='0';
			}
			$this->obj->update_obj($_GET['obj_id'],$_POST['obj']);

//            if ($_POST['obj']['is_detail']==1 and $_POST["obj_detail_number"]>=1 and $_POST["obj_detail_number"]<=5){
//				//这个条件也同时保证了不为空
//				for($i=1;$i<=$_POST["obj_detail_number"];$i++){
//					$obj_detail_data = $_POST["detail".$i];
//					$obj_detail_data['update_time']=date("Y-m-d H:i:s");
//					if(empty($obj_detail_data['obj_id'])){
//						$obj_detail_data['obj_uid']=$_GET['obj_id'];
//						$this->obj->add($obj_detail_data);
//					}else $this->obj->update_obj($obj_detail_data['obj_id'],$obj_detail_data);
//				}
//			}
			success('admin/object','编辑成功');
		}
		$this->obj->db->query('set names utf8');
		$data['obj_info']=$this->obj->getinfo($_GET['obj_id']);
		$this->load->helper('form');
		$data["labels"]=$this->obj->attributeLabels();
		$this->render('admin/object/edit',$data);
	}

	//引用专用页面
	public function ajax_list(){
		$this->load->model('admin/object_model','obj');
		$this->obj->db->query('SET NAMES UTF8');
		$data["labels"]=$this->obj->attributeLabels();
		$data['list']=$this->obj->objlist();
		$this->load->view('admin/object/ajax/list',$data);
	}

    public function add_detail(){
        $this->load->model('admin/object_model','obj');
		$this->obj->db->query('SET NAMES UTF8');
        $data['obj'] = $this->obj->getinfo($_GET['obj_id']);
        if($_POST != null){
            $this->obj->add_detail($_POST);
            success('admin/object/index','添加成功');
        }
        $this->render('admin/object/add_detail',$data);
    }

    public function detail(){
        $this->load->model('admin/object_model', 'obj');
        $this->obj->db->query('SET NAMES UTF8');
//        $data["labels"]=$this->obj->attributeLabels();
        if(isset($_GET['type_id'])){
            $data['detail_list'] = $this->obj->getDetail($_GET['obj_id'],$_GET['type_id']);
        }else{
            $data['detail_list'] = $this->obj->getDetail($_GET['obj_id']);
        }
//        p($data);exit;
        $this->render('admin/object/detaillist',$data);
    }

    public function detail_ajax_list(){
        //关闭ci评测
        $this->output->enable_profiler(false);

        $this->load->model('admin/object_model','obj');
        $this->obj->db->query('SET NAMES UTF8');
        $data['list'] = $this->obj->getColumns();
        $this->load->view('admin/object/columns',$data);
    }

    public function obj_ajax_list(){
        //关闭ci评测
        $this->output->enable_profiler(false);

        $this->load->model('admin/object_model','obj');
        $this->obj->db->query('SET NAMES UTF8');
        $data['detail_list'] = $this->obj->objlist();
        $this->load->view('admin/object/detail_list',$data);
    }

    public function columns_ajax_list(){
        //关闭ci评测
        $this->output->enable_profiler(false);

        $this->load->model('admin/object_model','obj');
        $this->obj->db->query('SET NAMES UTF8');
        $data['columns_list'] = $this->obj->columns_list($_GET['detail_obj_id']);
        if(!$data['columns_list']) die('没有引用类型');
        $this->load->view('admin/object/columns_list',$data);
    }

    public function edit_detail(){
        $this->load->model('admin/object_model','obj');
		$this->obj->db->query('SET NAMES UTF8');
        $data['obj'] = $this->obj->getDetailInfo($_GET['detail_id']);
//        p($data);exit;
//        if($_POST != null){
//            $this->obj->add_detail($_POST);
//            success('admin/object/index','添加成功');
//        }
        $this->render('admin/object/add_detail',$data);
    }

    public function del_detail(){
        $this->load->model('admin/object_model','obj');
        $this->obj->delDetail($_GET['detail_id']);
        redirect('admin/object/index', 'refresh');
    }

}
?>