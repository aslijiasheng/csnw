<?php 
class Icon extends MY_Controller{
	public function __construct(){
		parent::__construct();
	}
	
	//调用列表
	public function ajax_list(){
		$this->load->model('admin/icon_model','icon');
		$this->icon->db->query('SET NAMES UTF8');
		$data['list']=$this->icon->iconlist();
		$this->load->view('admin/icon/ajax_list',$data);
	}
}

?>