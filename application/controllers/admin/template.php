<?php
  class template extends MY_Controller{
	public $menu1 = 'template';

	public function __construct(){
		parent::__construct();
		$this->load->model('admin/templates_model','templates');
		$this->db->query('set names utf8');
	}
	public function index(){
		if(isset($_GET['template_type'])){
			$template_type=$_GET['template_type'];
		}else{
			$template_type="view"; //默认为视图
		}
		$this->template_list($template_type);
	}
	public function template_list($template_type=""){
		if($template_type==""){
			$template_type=$_GET['template_type'];
		}
		$this->menu2 = $template_type;
		$data['template_type']=$template_type;
		$data['lists']=$this->templates->tem_list($template_type);
		//p($data);
		$this->render('admin/template/list',$data);

	}

  	public function manage($template_type=""){
  		if($template_type==""){
			$data['lists']=$this->templates->tem_list($_GET['template_type']);
		}else{
			$data['lists']=$this->templates->tem_list($template_type);
		}
  		$this->render('admin/template/tem/index',$data);
  	}
  	public function add(){
  		if($_POST!=null){
        $_POST['template_type']=$_GET['template_type'];

  			$this->templates->add($_POST);
  			success('admin/template/manage?template_type='.$_GET['template_type'],"添加成功");
  		}
  		$this->render('admin/template/tem/add');
  	}
  	public function view(){
  		$url=$_GET['url'];
      $data['url']=$url;
      $arr=explode('/',$url);
      $data['temp_name']=$arr[1];
  		$data["contents"]=file_get_contents("application/template/".$url);
  		$this->render('admin/template/tem/view',$data);
  	}
  	public function edit(){

  		$url=$_GET['url'];
       $data['url']=$url;
      $arr=explode('/',$url);
      $data['temp_name']=$arr[1];
  		$data["contents"]=file_get_contents("application/template/".$url);
  		$this->render('admin/template/tem/edit',$data);
  	}
  	public function doedit(){

  		$newcontent=html_entity_decode($_POST['content']);

  		file_put_contents("application/template/".$_GET['url'], $newcontent);
  		echo "ok";
  	}
  	public function del(){
  		$this->load->model('admin/templates_model','del');
      //$url="application/template/".$_GET['url'];

      $this->del->del($_GET['id']);
       success('admin/template/manage?template_type='.$_GET['template_type'],'删除成功');

  	}


    public function t_list(){
      $this->output->enable_profiler(false);
      //$data['template_list']=$this->templates->db->get_where('dd_template',array('template_type'=>$_GET['type']))->result_array();
       $data['template_list']=$this->templates->tem_list($_GET['type']);
      //p($data['template_list']);die;
      // p($data);exit;
      $this->load->view('admin/template/tem/template_list',$data);
    }
  	//以上是控制器模板处理

  }
 ?>