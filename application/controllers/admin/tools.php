<?php

class Tools extends MY_Controller {
	public $menu1='tools';
	public $menu2='index';

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('PRC');
		$this->output->enable_profiler(false);
	}


	public function index() {
		$data['arr'] = array(
			"timing_task" => "定时任务工具",
			"DeveloperTools/home" => "删除工具",
			"history_order" => "订单BUG修复"
			);
		$this->render('admin/tools/index', $data);
	}
}

?>