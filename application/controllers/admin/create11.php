<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Create extends MY_Controller{
	public function __construct(){
		parent::__construct();
		 $this->load->library('templateengine');

	}

	//生成数据库
	public function c_table(){
		//echo "数据库生成表";
		//获取这个对象数据
		$this->load->model('admin/object_model','obj');
		$this->obj->db->query("set names utf8");
		$data=$this->obj->getinfo($_GET['obj_id']);
		//获取相关的属性数据
		$this->load->model('admin/attribute_model','attr');
		$this->attr->db->query("set names utf8");
		$data["attr"]=$this->attr->check($_GET['obj_id']);
		//p($data);exit;
		//组成sql
		// $sql="SELECT * from dd_icon";
		// $is_exists=$this->db->query($sql);
		// if(!is)
		$sql = " 
        CREATE TABLE IF NOT EXISTS `tc_".$data["obj_name"]."` (
	       `".$data["obj_name"]."_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
	     ";
		//循环所有的属性
		foreach ($data["attr"] as $k=>$v){
			//属性类型相关的写法
			switch ($v["attr_type_arr"]["attrtype_id"]){
				case 5: //数值
				case 8: //短文本类型
				case 9: //长文本类型
				case 10: //自动增长类型
				case 19: //引用类型
				case 14:
				case 20:
				case 11:
				case 15:
					$field_type = $v["attr_type_arr"]["attrtype_field_type"];
					break;
				default:
					echo "该属性类型没有定义！";
					p($v);
					exit;
			}
			$is_requerido="NOT NULL";//是否必填（默认都是不必填的）
			//echo $field_type;
			//p($v["attr_type"]);exit;
			$sql .= "
			`".$v["attr_field_name"]."` ".$field_type." ".$is_requerido." COMMENT '".$v["attr_label"]."',
			";
		}
		$sql .= "
			PRIMARY KEY (`".$data["obj_name"]."_id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;
		";
		//echo $sql;
		
	    $this->db->query($sql);
		echo "创建完毕";

		//p($sql);

	}

	//生成模型
	public function c_model(){
		//obj_id根据获取obj_name
		$this->load->model("admin/object_model",'obj');
		$this->obj->db->query("set names utf8");
		$data=$this->obj->getinfo($_GET['obj_id']);
		$data['obj_name_uc'] = ucfirst($data['obj_name']); //对象名称改成大写
		$data['obj_table_name'] = "tc_".$data['obj_name']; //表名
		//获取对象的字段数据用于添加attributeLabels
		$this->load->model("admin/attribute_model",'attr');
		$data['obj_attr'] = $this->attr->check($_GET['obj_id']);
		//从布局管理里获取列表页面的布局信息
		$this->load->model("admin/layout_model",'layout');
		$where = array(
			'obj_id'=>$_GET['obj_id'],
			'layout_type'=>'list',
			'type_id'=>0,
		);
		$data['list_layout'] = $this->layout->arr_getinfo($where);
		$where = array(
			'obj_id'=>$_GET['obj_id'],
			'layout_type'=>'view',
			'type_id'=>0,
		);
		$data['view_layout'] = $this->layout->arr_getinfo($where);
		$where = array(
			'obj_id'=>$_GET['obj_id'],
			'layout_type'=>'edit',
			'type_id'=>0,
		);
		$data['edit_layout'] = $this->layout->arr_getinfo($where);
		

		//p($data);exit;
		$obj_name=strtolower($data['obj_name']);
		$dir="application/models/www/"; //生成的文件的位子
		//检查是否有这个文件夹，没有则创建
		if(!file_exists($dir)){
			mkdir($dir);
		}
		//获取模本文件信息
		$row = file_get_contents('application/template/models/model.php');
		$row = $this->templateengine->replace_rule($row,$data);
		//写入文件
		file_put_contents($dir.$obj_name.'_model.php',$row);
		echo "model生成成功";
		//p($data);
	}
	
	//生成控制器
	public function c_controller(){
		//obj_id根据获取obj_name
		$this->load->model("admin/object_model",'obj');
		$this->obj->db->query("set names utf8");
		$data=$this->obj->getinfo($_GET['obj_id']);
		$data['obj_name_uc'] = ucfirst($data['obj_name']);
		$obj_name=strtolower($data['obj_name']);
		//获取对象的字段数据用于添加attributeLabels
		$this->load->model("admin/attribute_model",'attr');
		$data['obj_attr'] = $this->attr->check($_GET['obj_id']);
		//生成的文件的位子
		$dir="application/controllers/www/";
		if(!file_exists($dir)){
			mkdir($dir);
		}
		//获取模本文件信息
		$row = file_get_contents('application/template/controllers/controller.php');
		$row = $this->templateengine->replace_rule($row,$data);
		//添加各种行动
		//查询出有哪些行动
		$this->load->model("admin/action_model",'action');
		$data['ac_lists']=$this->action->getActions($data['obj_id']);
        


		//循环所有行动
		//foreach()
		//p($data['ac_lists']);die;
		$function_all = "";
		$function_all .= file_get_contents('application/template/controllers/index.php');
		foreach ($data['ac_lists'] as $arr) {
			$file_name = $arr['controller_template'][0]['template_path'];
			$function_all .= file_get_contents('application/template/'.$file_name);
		}
		
		//p($function_all);exit;
		$function_all = $this->templateengine->replace_rule($function_all,$data);
		/*
		foreach ($data['ac_lists'] as $key=>$value){
			$function_all .= $value['content']."\r\n";
		}
		*/
		$row = str_replace("{#function_all#}",$function_all,$row); //将这些行动写到控制器里去
		//生成控制器
		//写入文件
		file_put_contents($dir.$obj_name.'.php',$row);
		/*
		$fp_view=fopen($dir.$obj_name.'.php','w');
		fwrite($fp_view,$row);
		fclose($fp_view);
		*/
		echo "生成控制器成功";
	}
	
	//生成视图
	public function c_view(){
		//obj_id根据获取obj_name
		$this->load->model("admin/object_model",'obj');
		$this->obj->db->query("set names utf8");
		$data=$this->obj->getinfo($_GET['obj_id']);
		//获取对象的字段数据
		$this->load->model("admin/attribute_model",'attr');
		$data['obj_attr'] = $this->attr->check($_GET['obj_id']);
		//p($data['obj_attr']);exit;
		$obj_name=strtolower($data['obj_name']);

		$dir="application/views/www/".$obj_name."/";
		if(!file_exists($dir)){
			mkdir($dir);
		}
		//从布局管理里获取列表页面的布局信息
		$this->load->model("admin/layout_model",'layout');
		$where = array(
			'obj_id'=>$_GET['obj_id'],
			'layout_type'=>'list',
			'type_id'=>0,
		);
		$data['list_layout'] = $this->layout->arr_getinfo($where);
		$where = array(
			'obj_id'=>$_GET['obj_id'],
			'layout_type'=>'view',
			'type_id'=>0,
		);
		$data['view_layout'] = $this->layout->arr_getinfo($where);
		$where = array(
			'obj_id'=>$_GET['obj_id'],
			'layout_type'=>'edit',
			'type_id'=>0,
		);
		$data['edit_layout'] = $this->layout->arr_getinfo($where);
		//p($data);die;




		//查询行动里的数据然后循环出所有需要生成的视图
		$this->load->model('admin/action_model','action');
		$data['action'] = $this->action->listGetInfo($_GET['obj_id']);
		//p($data['action']);die;
		$row = file_get_contents('application/template/views/list.php');
			$row = $this->templateengine->replace_rule($row,$data);
			file_put_contents($dir.'list.php',$row);
			echo "列表视图生成完毕。"."<br>";
		foreach ($data['action'] as $k=>$v){
			
			if(isset($v['view_template_id_arr'])){ 
			  $row = file_get_contents('application/template/'.$v['view_template_id_arr']['template_path']);
			  $row = $this->templateengine->replace_rule($row,$data);
			//写入文件
			file_put_contents($dir.$v["act_name"].'.php',$row);
			  echo "视图".$v['view_template_id_arr']['template_name']."生成完毕。";echo "<br>";
		   }
		}

		
	}
	
	
}
?>
