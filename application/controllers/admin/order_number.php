<?php

class Order_number extends MY_Controller {
	public $menu1='tools';
	public $menu2='order';

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('PRC');
		$this->output->enable_profiler(false);
	}


	public function index() {
		$data = "";
		$this->render('admin/tools/order_number', $data);
	}
	public function ajax_select(){
		$this->load->view('admin/tools/ajax_select');
	}
}

?>