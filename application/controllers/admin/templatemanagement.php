<?php 
  class Templatemanagement extends MY_Controller{
    public function __construct(){
      parent::__construct();
      $this->load->model('admin/templates_model','templates');
    }
  	public function index(){

  		$this->render('admin/template/index');
  	}
  	public function manage(){ 		
  		$data['lists']=$this->templates->tem_list($_GET['template_type']);
  		$this->render('admin/template/tem/index',$data);
  	}
  	public function add(){
  		if($_POST!=null){
        $_POST['template_type']=$_GET['template_type'];

  			$this->templates->add($_POST);
  			success('admin/templatemanagement/manage?template_type='.$_GET['template_type'],"添加成功");
  		}
  		$this->render('admin/template/tem/add');
  	}
  	public function view(){
  		$url=$_GET['url'];
      $data['url']=$url;
      $arr=explode('/',$url);
      $data['temp_name']=$arr[1];
  		$data["contents"]=file_get_contents("application/template/".$url);
  		$this->render('admin/template/tem/view',$data);
  	}
  	public function edit(){
  		
  		$url=$_GET['url'];
       $data['url']=$url;
      $arr=explode('/',$url);
      $data['temp_name']=$arr[1];
  		$data["contents"]=file_get_contents("application/template/".$url);
  		$this->render('admin/template/tem/edit',$data);
  	}
  	public function doedit(){

  		$newcontent=html_entity_decode($_POST['content']);
  		
  		file_put_contents("application/template/".$_GET['url'], $newcontent);
  		echo "ok";
  	}
  	public function del(){
  		$this->load->model('admin/templates_model','del');
      //$url="application/template/".$_GET['url'];
     
      $this->del->del($_GET['id']);
       success('admin/templatemanagement/manage?template_type='.$_GET['template_type'],'删除成功');

  	}

    public function template_list(){
      echo "12345";
      echo $_GET['type'];
      $data['template_list']=$this->templates->db->get_where('dd_template',array('template_type'=>$_GET['type']))->result_array();
     // p($data['template_list']);die;
      $this->render('admin/template/template_list',$data);
    }
  	//以上是控制器模板处理
  	
  }
 ?>