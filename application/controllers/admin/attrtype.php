<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Attrtype extends MY_Controller{
	public $menu1 = 'cus';
	public $menu2 = 'attrtype';

	public function __construct(){
		parent::__construct();
		$this->load->model('admin/attrtype_model','attr_type');
		$this->attr_type->db->query('SET NAMES UTF8');
	}
    
	public function index(){
		$this->render('admin/attrtype/index');
	}

	
	//列表
	 public function type_list(){
	 	$data['list']=$this->attr_type->check();
	// 	$data["labels"]=$this->attr->attributeLabels();
	 	//$data['list']=$this->attr->attrlist();
   // p($data['list']);
	 	$this->render('admin/attrtype/list',$data);
	 }
	 public function add(){
	 	if(!empty($_POST)){
	 		$this->load->helper("form");
	 		$this->load->library("form_validation");
	 		$this->form_validation->set_rules("attrtype_name","属性类型名","required|is_unique[dd_attr_type.attrtype_name]");
	 		$this->form_validation->set_rules("view1_template","列表视图模板id","required");
	 		$this->form_validation->set_rules("view2_template","编辑模板id","required");
	 		$this->form_validation->set_rules("view3_template","搜索视图模板id","required");
	 		if($this->form_validation->run()!=false){

	 			$data['attrtype_name']=$_POST['attrtype_name'];
	 			$data['view_template_id']=$_POST['view_template'];
	 			$data['edit_template_id']=$_POST['edit_template'];
	 			$data['search_template_id']=$_POST['search_template'];
	 			$this->attr_type->add_type($data);
                success("admin/attrtype/type_list","添加成功");
	 		}
	 	}
	 	$this->render("admin/attrtype/add");
	 } 
	 public function edit(){
	 	$data['type_info']=$this->attr_type->getinfo($_GET['attrtype_id']);
	 	if(!empty($_POST)){
	 		
	 		$updata['attrtype_name']=$_POST['attrtype_name'];
	 		$updata['view_template_id']=$_POST['view_template'];
	 		$updata['edit_template_id']=$_POST['edit_template'];
	 		$updata['search_template_id']=$_POST['search_template'];
	 		$this->attr_type->edit($updata,$_GET['attrtype_id']);
	 		success("admin/attrtype/type_list","编辑成功");
	 	}
	 	$this->render("admin/attrtype/edit",$data);
	 }

	// //新增
	// public function obj_add(){
	// 	$this->load->model('admin/object_model','obj');
	// 	$this->load->helper('form');
	// 	$data["labels"]=$this->obj->attributeLabels();
	// 	if($_POST!=null){
	// 		//echo "<pre>";print_r($_POST);exit;
	// 		$obj_data = $_POST["obj"];
	// 		$obj_data['create_time']=date("Y-m-d H:i:s");
	// 		$obj_data['update_time']=date("Y-m-d H:i:s");
	// 		$add_id = $this->obj->add($obj_data);
	// 		//添加明细
	// 		if (!isset($_POST['obj']['is_detail'])){
	// 			$_POST['obj']['is_detail']=0;
	// 		}
	// 		if ($_POST['obj']['is_detail']==1 and $_POST["obj_detail_number"]>=1 and $_POST["obj_detail_number"]<=5){
	// 			//这个条件也同时保证了不为空
	// 			for($i=1;$i<=$_POST["obj_detail_number"];$i++){
	// 				$obj_detail_data = $_POST["detail".$i];
	// 				$obj_detail_data['create_time']=date("Y-m-d H:i:s");
	// 				$obj_detail_data['update_time']=date("Y-m-d H:i:s");
	// 				$obj_detail_data['obj_uid']=$add_id;
	// 				$this->obj->add($obj_detail_data);
	// 			}
	// 		}
	// 		success('admin/object/index','添加成功');
	// 	}
	// 	$this->render('admin/object/add',$data);
	// }
	
	// //删除
	// public function obj_del(){
	// 	//删除再说吧！
	// }
	
	// //编辑
	// public function obj_edit(){
	// 	$this->load->model('admin/object_model','obj');
	// 	if($_POST!=null){
	// 		$_POST['update_time']=time();
	// 		$this->obj->update_obj($_GET['obj_id'],$_POST);
	// 		success('admin/object','编辑成功');
	// 	}
	// 	$this->obj->db->query('set names utf8');
	// 	$data['obj_info']=$this->obj->getinfo($_GET['obj_id']);
	// 	$this->load->helper('form');
	// 	$data["labels"]=$this->obj->attributeLabels();
	// 	$this->render('admin/object/edit',$data);
	// }

 //     public function attribute_list(){
 //       $this->load->model('admin/attribute_model','attr');
 //       $this->attr->db->query("set names utf8");
 //       $data['list']=$this->attr->check($_GET['obj_id']);
 //        // if($data['list']==null){
 //        //  redirect('index/clientindex/add_attribute?obj_id='.$_GET['obj_id']);
 //        // }
 //       $this->render('admin/object/attribute_list',$data);
 //     }
 //     public function add_attribute(){
 //      $this->load->model('admin/attribute_model','attr');
 //      $this->attr->db->query("set names utf8");
 //      $this->load->helper('form');
      
 //      if($_POST!=null){
       
 //         $this->load->library('form_validation');
 //         $this->form_validation->set_rules('attr_name','属性名','required');
 //         $this->form_validation->set_rules('attr_label','属性标签','required|min_lenght[5]');
 //         if($this->form_validation->run()!=false){
 //             $_POST['obj_id']=$_GET['obj_id'];
 //         $_POST['create_time']=time();
 //         $_POST['update_time']=time();
 //         $this->attr->add($_POST);
 //        success('admin/object/attribute_list?obj_id='.$_GET['obj_id'],'success');
 //         }  
       

 //      }
         
 //        $this->render('admin/object/add_attribute');
 //     }
 //     public function edit_attribute(){
 //       $this->load->model('admin/attribute_model','attr');
 //       $this->attr->db->query("set names utf8");
 //       if($_POST!=null){
 //         $_POST['update_time']=time();
 //         $this->attr->edit_attr($_GET['attr_id'],$_POST);
 //         success('admin/object/attribute_list?obj_id='.$_GET['obj_id'],'修改成功');
 //       } else{
 //         $data['info']=$this->attr->db->get_where('attribute',array('attr_id'=>$_GET['attr_id']))->result_array();
 //       }
 //       $this->render('admin/object/edit_attribute',$data);
 //     }
 //     public function del_attribute(){
 //        $this->load->model('admin/attribute_model','attr');
 //         $this->attr->db->query("set names utf8");
 //        $data['info']=$this->attr->db->get_where('attribute',array('attr_id'=>$_GET['attr_id']))->result_array();
 //        if($data['info'][0]['attr_name']!='Name'){
 //        $this->attr->del_attr($_GET['attr_id']);
 //        $data['list']=$this->attr->check($_GET['obj_id']);
 //        if($data['list']==null){
 //          success('admin/object','属性已经清空,回到首页');
 //        }
 //        success('admin/object/attribute_list?obj_id='.$_GET['obj_id'],'删除成功');
 //      }else{
 //        error('基本属性不能删除哦');
 //      }
 //     }

 //   /*以下是对象表及文件的创建操作*/
 //     public function create_table(){
 //       $this->load->model('admin/table_model','table');
 //       $data=$this->table->get_obj_name($_GET['obj_id']);
 //      $tab_name=$data[0]['obj_name'];
      
 //      $this->table->create_table($tab_name);

 //      success('admin/object/','create successfully');
 //     }
 //     public function create_model(){
 //        $this->load->model('admin/table_model','model');
 //       $data=$this->model->get_obj_name($_GET['obj_id']);
 //       $model_name=$data[0]['obj_name'];
       
 //         $content="public function index(){}";
 //     $html_name=strtolower($model_name)."_model.php";
    
 //        $fp_tmp=fopen(base_url().'application/models/admin/obj_model/temple.html','r');

 //        $fp_html=fopen('application/controllers/www/models/'.$html_name,'w');

 //        while(!feof($fp_tmp)){
 //        $row=fgets($fp_tmp);
 //        $new_row=replace($row,$model_name,$content);
 //        fwrite($fp_html,$new_row);
 //      }
 //       fclose($fp_html);
 //        success('admin/object/','create successfully');
      
 //     }
 //     public function create_controller(){
 //        $this->load->model('admin/table_model','table');
 //        $data=$this->table->get_obj_name($_GET['obj_id']);
 //        $controller_name=$data[0]['obj_name'];
 //        $this->table->db->query('set names utf8');
 //        $data['conlist']=$this->table->get_content($_GET['obj_id']);
 //         $content="";
         
 //          foreach ($data['conlist'] as $v) {
            
 //            $content=$content.'
 //       '.$v['content'];
 //          }
        
        
 //        $file_name=strtolower($controller_name).".php";
 //        $fp_tmp=fopen(base_url().'application/controllers/admin/obj_controller/template.html','r');
 //        $fp_html=fopen('application/controllers/www/'.$file_name,'w');
 //        while(!feof($fp_tmp)){
 //        $row=fgets($fp_tmp);
 //        $new_row=replace($row,$controller_name,$content);
 //        fwrite($fp_html,$new_row);
 //      }
 //       fclose($fp_html);
 //        success('admin/object/','create successfully');
 //     }
   
 //    /*下面是对象行为的一些操作*/ 
 //    public function actions(){
 //      $this->load->model('admin/action_model','action');

 //      $this->action->db->query('set names utf8');
 //      $data['ac_lists']=$this->action->getActions($this->input->get('obj_id'));
      
 //      $this->render('admin/object/actions',$data);
 //    }
 //   public function add_action(){
 //      $this->load->model('admin/action_model','action');
 //      $this->action->db->query("set names utf8");
 //      $this->load->helper('form');
 //       if($this->input->post()!=null){
       	
 //        $this->load->library('form_validation');
 //        $this->form_validation->set_rules('act_name','方法名','required|is_unique[actions.act_name]|alpha');
 //        $this->form_validation->set_rules('act_description','描述','required|min_length[5]|max_length[20]');

 //        $this->form_validation->set_rules('content','内容','required');
 //        if($this->form_validation->run()!=false){
 //            $data=$this->action->get_obj_name($_GET['obj_id']);
 //      $controller_name=$data[0]['obj_name'];
 //       $obj_name=strtolower($this->input->get('obj_name'));

 //       $_POST['obj_id']=$this->input->get('obj_id');
 //       $_POST['create_time']=time();
 //       $this->action->db->query('set names utf8');
 //       $this->action->add_action($this->input->post());
 //       $data['ac_lists']=$this->action->getActions($this->input->get('obj_id'));
 //       $content="";
         
 //          foreach ($data['ac_lists'] as $v) {
            
 //            $content=$content.'
 //              '.$v['content'];
 //               }
 //           $file_name=strtolower($controller_name).".php";
 //        $fp_tmp=fopen(base_url().'application/controllers/admin/obj_controller/template.html','r');
 //        $fp_html=fopen('application/controllers/www/'.$file_name,'w');
 //        while(!feof($fp_tmp)){
 //        $row=fgets($fp_tmp);
 //        $new_row=replace($row,$controller_name,$content);
 //        fwrite($fp_html,$new_row);
 //      }
 //       fclose($fp_html);
 //       success('admin/object/actions?obj_id='.$this->input->get('obj_id'),'添加成功');
       
 //       }
 //        }
        
       
       
 //        $this->render('admin/object/add_action');
 //    }
 //   public function del_action(){
 //      $this->load->model('admin/action_model','action');
 //      //echo $this->input->get('act_id');
 //      $data=$this->action->db->get_where('actions',array('act_id'=>$_GET['act_id']))->result_array();
 //      if($data[0]['act_name']!='index' && $data[0]['act_name']!='add' && $data[0]['act_name']!='edit' && $data[0]['act_name']!='del'){
 //          $this->action->del_action($this->input->get('act_id'));
 //          $data=$this->action->get_obj_name($_GET['obj_id']);
 //          $controller_name=$data[0]['obj_name'];
 //         $_POST['obj_id']=$this->input->get('obj_id');
 //          $this->action->db->query('set names utf8');
 //         $this->action->del_action($this->input->get('act_id'));
        
 //         $data['ac_lists']=$this->action->getActions($this->input->get('obj_id'));
 //       $content="";
         
 //          foreach ($data['ac_lists'] as $v) {
            
 //            $content=$content.'
 //              '.$v['content'];
 //               }
 //           $file_name=strtolower($controller_name).".php";
 //        $fp_tmp=fopen(base_url().'application/controllers/admin/obj_controller/template.html','r');
 //        $fp_html=fopen('application/controllers/www/'.$file_name,'w');
 //        while(!feof($fp_tmp)){
 //        $row=fgets($fp_tmp);
 //        $new_row=replace($row,$controller_name,$content);
 //        fwrite($fp_html,$new_row);
 //      }
 //       fclose($fp_html);
         
 //         success('admin/object/actions?obj_id='.$_GET['obj_id'],'删除成功');
 //    }else{
 //        error('该行为不能被删除');
 //      }
     
 //    }


 	}
  
 ?>