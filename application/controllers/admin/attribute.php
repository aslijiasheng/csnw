<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Attribute extends MY_Controller{
	public $menu1='cus';
	public $menu2='object';
	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->attr_list();
	}

	//列表
	public function attr_list(){
		$this->load->model('admin/attribute_model','attr');
		$this->attr->db->query('SET NAMES UTF8');
		$data["labels"]=$this->attr->attributeLabels();
		$data['list']=$this->attr->check($_GET['obj_id']);
        //p($data['list']);die;
		$this->render('admin/attribute/list',$data);
	}

	//新增
	public function add(){
		//获取通过对象ID获取对象的名称
		$this->load->model('admin/object_model','obj');
		$this->obj->db->query("set names utf8");
		$obj_arr = $this->obj->getinfo($_GET['obj_id']);
		$this->load->model('admin/attribute_model','attr');
		$this->attr->db->query("set names utf8");
		$data["labels"]=$this->attr->attributeLabels();

		//查询出属性类型的所有值
		$this->load->model('admin/attrtype_model','attrtype');
		$this->attrtype->db->query("set names utf8");
		$data['attrtype_list']=$this->attrtype->check();
		if($_POST!=null){
			//echo "<pre>";print_r($_POST);exit;
            $attr_data = $_POST["attr"];
            if (empty($attr_data['attr_quote_id'])) {
                $attr_data['attr_quote_id'] = 0;
            }
			$attr_data['attr_field_name']=$obj_arr['obj_name']."_".$attr_data['attr_name']; //字段名
			$attr_data['attr_obj_id']=$_GET['obj_id'];//所属对象ID
			//p($attr_data);exit;
			$add_id = $this->attr->add($attr_data);
			//echo $add_id;exit;
			success('admin/attribute/?obj_id='.$_GET['obj_id'],'添加成功');
		}
		$this->render('admin/attribute/add',$data);
	}

	//删除
	public function del(){
		//获取通过属性ID获取这个属性的相关内容
		$this->load->model('admin/attribute_model','attr');
		$this->attr->db->query("set names utf8");
		$data = $this->attr->getinfo($_GET['attr_id']);

		$this->load->model('admin/attribute_model','attr');
		$this->attr->db->query("set names utf8");
		$this->attr->del($_GET['attr_id']);
		success('admin/attribute/?obj_id='.$data['attr_obj_id']['obj_id'],'删除成功');
	}

	//编辑
	public function edit(){
		//获取通过属性ID获取这个属性的相关内容
		$this->load->model('admin/attribute_model','attr');
		$this->attr->db->query("set names utf8");
		$data = $this->attr->getinfo($_GET['attr_id']);
		$data["labels"]=$this->attr->attributeLabels();
		//查询出属性类型的所有值
		$this->load->model('admin/attrtype_model','attrtype');
		$this->attrtype->db->query("set names utf8");
		$data['attrtype_list']=$this->attrtype->check();
		
		if($_POST!=null){
			///echo "<pre>";print_r($_POST);exit;
			$attr_data = $_POST["attr"];
			$attr_data['attr_field_name']=$data['attr_obj_id_arr']['obj_name']."_".$attr_data['attr_name']; //字段名
			if($attr_data['attr_type']!=19){
				$attr_data['attr_quote_id']=0;
			}
			$attr_data['attr_obj_id']=$data['attr_obj_id'];//所属对象ID
			$this->attr->update($attr_data,$data['attr_id']); //修改数据
			success('admin/attribute/?obj_id='.$data['attr_obj_id'],'修改成功');
		}
//		p($data);exit;
		$this->render('admin/attribute/edit',$data);
	}
}
?>
