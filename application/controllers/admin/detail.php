<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Detail extends MY_Controller {

	public $menu1 = 'cus';
	public $menu2 = 'object';

	public function __construct() {
		parent::__construct();
		$this->load->model('admin/detail_model', 'detail');
	}

	public function index() {
		$this->detail->db->query('SET NAMES UTF8');

		if (isset($_GET['type_id'])) {
			$data['list'] = $this->detail->getList($_GET['obj_id'], $_GET['type_id']);
		} else {
			$data['list'] = $this->detail->getList($_GET['obj_id']);
		}

		$this->render('admin/detail/detail_list', $data);
	}

	// 添加明细
	public function add() {
		$this->load->model('admin/object_model', 'obj');
		$data['obj'] = $this->obj->getinfo($_GET['obj_id']);

		if (($_POST != null) && $_POST['obj_id'] && $_POST['detail_obj_id']) {
			$this->detail->addDetail($_POST);
			success('admin/object/index', '添加成功');
		}

		$this->render('admin/detail/detail_add', $data);
	}

	// 编辑明细
	public function edit() {
		$this->detail->db->query('SET NAMES UTF8');
		if ($_GET['detail_id']) {
			if (($_POST != null) && $_GET['detail_id']) {
				$this->detail->updateDetail($_GET['detail_id'], $_POST);
			}
			$data['obj'] = $this->detail->getDetailInfo($_GET['detail_id']);
			$this->render('admin/detail/detail_edit', $data);
		} else
			return false;
	}

	// 删除明细
	public function del() {
		if ($_GET['detail_id']) {
			$this->detail->delDetail($_GET['detail_id']);
			redirect('admin/object/index', 'refresh');
		} else
			return false;
	}

	// ajax object
	public function obj_ajax_list() {
		//关闭ci评测
		$this->output->enable_profiler(false);
		$this->detail->db->query('SET NAMES UTF8');

		$this->load->model('admin/object_model', 'obj');
		$data['detail_list'] = $this->obj->objlist();
		$this->load->view('admin/object/detail_list', $data);
	}

	// ajax columns
	public function columns_ajax_list() {
		//关闭ci评测
		$this->output->enable_profiler(false);
		$this->detail->db->query('SET NAMES UTF8');

		$data['columns_list'] = $this->detail->columns_list($_GET['detail_obj_id']);
		if (!$data['columns_list'])
			die('没有引用类型');
		$this->load->view('admin/detail/ajax_columns', $data);
	}

}

?>