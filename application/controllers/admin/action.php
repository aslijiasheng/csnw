<?php
  class Action extends MY_Controller{
    public $menu1="cus";
  	public function __construct(){
       parent::__construct();
       $this->load->model('admin/action_model','action');
       $this->action->db->query('set names utf8');
       $this->output->enable_profiler(TRUE);
  	}
  	public function action_list(){
      $data['ac_lists']=$this->action->getActions($this->input->get('obj_id'));
	  // echo $this->db->last_query();
	  // die();
      $data['obj_name']=$this->action->get_obj_name($_GET['obj_id']);

      $this->render('admin/action/action_list',$data);
    }
   public function add_action(){
      $this->load->helper('form');

       if($this->input->post()!=null){

        $this->load->library('form_validation');
        $this->form_validation->set_rules('act_name','方法名','required');
        $this->form_validation->set_rules('act_description','描述','required');
        $this->form_validation->set_rules('controller_template_id','控制器模板ID','required');
        $this->form_validation->set_rules('view_template_id','视图模板ID','');
         $this->form_validation->set_rules('controller_template_name','控制器模板名','required');
        $this->form_validation->set_rules('view_template_name','视图模板名','');
        if($this->form_validation->run()!=false){
            $data['obj_id']=$_GET['obj_id'];
            $data['act_name']=$_POST['act_name'];
            $data['act_description']=$_POST['act_description'];
            $data['controller_template_id']=$_POST['controller_template_id'];
            $data['view_template_id']=$_POST['view_template_id'];
             $this->action->db->query('set names utf8');
             $this->action->add_action($data);
         success('admin/action/action_list?obj_id='.$this->input->get('obj_id'),'添加成功');
       }
      }


        $this->render('admin/action/add_action');
    }
    public function edit_action(){
      $data['list']=$this->action->search_info($_GET['act_id']);
//      p($data);exit;

      if(!empty($_POST)){
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('act_name','方法名','required');
        $this->form_validation->set_rules('act_description','描述','required');
        $this->form_validation->set_rules('controller_template_id','控制器模板ID','required');
        $this->form_validation->set_rules('view_template_id','视图模板ID','required');
        if($this->form_validation->run()!=false){
          $this->action->edit($_POST,$_GET['act_id']);
           success('admin/action/action_list?obj_id='.$_GET['obj_id'],'编辑成功');
        }
      }
      $this->render('admin/action/edit_action',$data);
    }
   public function del_action(){
      $data=$this->db->get_where('dd_action',array('act_id'=>$_GET['act_id']))->result_array();

      if($data[0]['act_name']!='index' && $data[0]['act_name']!='add' && $data[0]['act_name']!='edit' && $data[0]['act_name']!='del'){
        echo $data[0]['act_name'];
         //p($data);die;
          //$this->action->del_action($this->input->get('act_id'));
          //$data=$this->action->get_obj_name($_GET['obj_id']);
         //$_POST['obj_id']=$this->input->get('obj_id');
          //$this->db->query('set names utf8');
         $this->action->del_action($this->input->get('act_id'));
         success('admin/action/action_list?obj_id='.$_GET['obj_id'],'删除成功');
    }else{
        error('该行为不能被删除');
    }

    }
  }
 ?>