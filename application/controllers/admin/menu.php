<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MY_Controller {

    public $menu1 = 'menu';

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/menu_model', 'menu');
    }

    public function index() {
        $data['labels'] = $this->menu->attributeLabels();
        $this->menu->db->query('SET NAMES UTF8');
        $data['list'] = $this->menu->menulist();
        $this->render('admin/menu/list', $data);
    }

    public function add() {
        $this->menu->db->query('SET NAMES UTF8');
        $data['labels'] = $this->menu->attributeLabels();
        if ($_POST != null) {
            $data = $_POST['menu'];
            $id = $this->menu->add($data);
            $data['menu_path'] = $this->repath($id, $data['menu_order'], $data['menu_uid']);
            $data['menu_order'] = substr_count($data['menu_path'], ',');
            if(empty($data['menu_label'])) $data['menu_label'] = end(explode('/', $data['menu_url']));
            $this->menu->update($data, $id);
            redirect('admin/menu/index', 'refresh');
        }
        $this->render('admin/menu/add', $data);
    }

    public function edit() {
        $this->menu->db->query('SET NAMES UTF8');
        $data['labels'] = $this->menu->attributeLabels();
        $data['list'] = $this->menu->getinfo($_GET['menu_id']);
        $data['order'] = $this->menu->getorder($data['list']);
        $this->render('admin/menu/edit', $data);
    }

    public function save() {
        $this->menu->db->query('SET NAMES UTF8');
        $menu = $this->menu->getinfo($_POST['menu']['menu_id']);
        $_POST['menu']['menu_is_js'] = empty($_POST['menu']['menu_is_js']) ? 0 : $_POST['menu']['menu_is_js'];
        $data = $_POST['menu'];
        if ($this->menu->getorder($menu) != $_POST['order']) {
            $data['menu_path'] = $this->repath($menu['menu_id'], $_POST['order'], $menu['menu_uid']);
            // 数据库替换排序
            $this->db->query('update `dd_menu` set menu_path=replace(menu_path,"' . $menu['menu_path'] . '","' . $data['menu_path'] . '") ');
        }
        if(empty($data['menu_label'])) $data['menu_label'] = end(explode('/',$data['menu_url']));
        $this->menu->update($data, $_POST['menu']['menu_id']);
        redirect('admin/menu/index', 'refresh');
    }

    public function del() {
        $rows = $this->db->get_where('dd_menu', array('menu_uid' => $_GET['menu_id']))->result_array();
        if (count($rows)) {
            success('admin/menu/index', '无法删除');
        } else {
            $this->menu->del($_GET['menu_id']);
            success('admin/menu/index', '成功删除');
        }
    }

    // 前台创建菜单
    public function create(){
        $this->load->library('TemplateEngine', '', 'template');
        $dir="application/views/www/layouts/";
        if(!file_exists($dir)) mkdir($dir);

        // 头部模板
        $hcontent = file_get_contents('application/template/views/layouts/head.php');
        $data['topmenu'] = $this->menu->getuidinfo(0);

        foreach($data['topmenu'] as $k=>$v){
            $data['topmenu'][$k]['select'] = '';
            $submenu = $this->menu->getuidinfo($v['menu_id']);
            if(count($submenu)>0){
                foreach ($submenu as $v) {
                    $data['topmenu'][$k]['select'] .= '|'.$v['menu_label'];
                }
            }
            $data['submenu'][] = $submenu;
        }
        $hcontent = $this->template->replace_rule($hcontent, $data);
        file_put_contents($dir.'head.php',$hcontent);

        // 底部模板
        $bcontent = file_get_contents('application/template/views/layouts/bottom.php');
        file_put_contents($dir.'bottom.php',$bcontent);

        redirect('admin/menu/index', 'refresh');
    }

    private function repath($menu_id, $order, $menu_uid) {
        if ($menu_uid != 0) {
            $um = $this->menu->getinfo($menu_uid);
            return $um['menu_path'] . '-' . str_pad($order, 3, '0', STR_PAD_LEFT) . ',' . str_pad($menu_id, 6, '0', STR_PAD_LEFT);
        } return 'P-' . str_pad($order, 3, '0', STR_PAD_LEFT) . ',' . str_pad($menu_id, 6, '0', STR_PAD_LEFT);
    }

}

?>