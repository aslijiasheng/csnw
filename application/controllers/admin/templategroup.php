<?php
  class templategroup extends MY_Controller
  {

	public function __construct(){
		parent::__construct();
		$this->load->model('admin/templateGroup_model','templatesGroup');
		$this->load->model('admin/templates_model','templates');
		$this->templatesGroup->db->query('SET NAMES UTF8');
	}

	
	public function index(){
		$this->template_list();
	}
	public function template_list(){
		$data['lists']=$this->templatesGroup->GetInfo();
		$this->render('admin/templategroup/list',$data);
	}
	
	public function view()
	{
		$templatetype = $_POST['$templatetype'];
		if( empty( $templatetype ) )
		{
			$templatetype = "view";
		}
		$data['template_type'] = $templatetype;
		$data['lists'] = $this->templates->tem_list($templatetype);
		$list=$this->session->userdata('data_list');
		$this->session->unset_userdata('data_list');
		$data['type_data']=explode(",",$list[$templatetype]);
		array_pop($data['type_data']);
		$this->load->view('admin/templategroup/view', $data);
	}

	public function getName($group_name)
	{
		if( is_array($group_name) )
		{
			$where = array("templategroup_name" => $_POST['templategroup_name']);
			$data=$this->templatesGroup->GetInfo($where);
			return count($data);
		}
	}

	public function del($templategroup_id = "")
	{
		if(!empty($templategroup_id))
		{
			$rows = $this->templatesGroup->del($templategroup_id);
			$rows>0 ? success('admin/templategroup/index',"删除成功") : success('admin/templategroup/index',"删除失败");
		}
		else
		{
			error("删除失败");
		}
	}
	
	private function parasData( &$data )
	{
		foreach($data['attribute'] as $key=>$value)
		{
			foreach($data['t_g_lists'] as $k=>$v)
			{
				if(!empty($v[$key]['template_id']))
				{
					$data['data_lists'][$key].=$v[$key]['template_id'].",";						
				}
				
			}
		}
		return $data;
	}

	public function edit($templategroup_id = "")
	{
		$data['attribute'] = $this->templatesGroup->libattrible();
		if( !empty($templategroup_id) )
		{

			$where = array("templategroup_id" => $templategroup_id);
			$data['lists'] = $this->templatesGroup->GetInfo( $where );
			$data['t_g_lists'] = $this->templatesGroup->tglist_template( $templategroup_id , "" );
			$this->parasData( &$data );
			$diff = array_diff(array("view","controller","model"),array_keys($data["data_lists"]));
			foreach($diff as $kk=>$vv)
			{
				$data["data_lists"][$vv]="";
			}
			//p($data['data_lists']);
			//die();
			$this->session->set_userdata('data_list', $data['data_lists']);
		}
		if(empty($data['data_lists']))
		{
			$data['data_lists']=$data['attribute'];
		}
		$data['tmp_lists'] = $info = $this->templates->GetInfo();
		$this->render('admin/templategroup/tem/edit',$data);
  	}
	
	public function ajax_select()
	{
		$data['type']=$_POST['template_type'];
		$template_ids=$_POST['template_ids'];
		if( !empty($_POST['templateGroup_id']))
		{
			$data['t_g_lists'] = $this->templatesGroup->tglist_template( $_POST['templateGroup_id'] , $_POST['template_type'] );
			//echo $this->db->last_query();
		}
		else
		{
			$result=explode(",",$template_ids);
			array_pop($result);
			$data['t_g_lists'] = $this->templates->selectIn($result);
		}
		//p($data['t_g_lists']);
		//exit;
		$this->output->enable_profiler(false);
		//echo $this->db->last_query();
		//p($data);
		$this->load->view('admin/templategroup/ajax_select',$data);
	}
	
  	public function sub($templategroup_id = "")
  	{
		$bol=$this->templatesGroup->_insert($_POST, $templategroup_id);

		$bol?success('admin/templategroup/index',"执行成功"):error("执行失败");
  	}
	
	



  }
 ?>