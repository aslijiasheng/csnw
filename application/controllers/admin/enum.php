<?php 
  class Enum extends MY_Controller{
    public $menu1 = 'cus';
  	public function __construct(){
  		parent::__construct();
      $this->output->enable_profiler(TRUE);
		$this->db->query('SET NAMES UTF8');
  		$this->load->model("admin/enum_model",'enum');
		$this->enum->db->query('SET NAMES UTF8');
  	}
  	public function enum_list(){
		$attr_name=$this->db->select('attr_name')->from('dd_attribute')->where('attr_id',$_GET['attr_id'])->get()->result_array();
		$obj_name=$this->db->select('obj_name')->from('dd_object')->where('obj_id',$_GET['obj_id'])->get()->result_array();
		$data['attr_name']=$obj_name[0]['obj_name'].'_'.$attr_name[0]['attr_name'];
		$data['enum_list']=$this->enum->getlist($_GET['attr_id']);
		$this->render("admin/enum/list",$data);
  	}
  	public function add(){
      if(!empty($_POST)){
        $arr=$this->db->get_where('dd_enum',array('attr_id'=>$_GET['attr_id']))->result_array();
        $enum_list = trim($_POST['enum_lists']);  
        $enum_arr = explode("\r\n", $enum_list); 
        if(!empty($arr)){
           $last_num=count($arr)-1;
           $start_key=intval($arr[$last_num]['enum_key'])+1;
           $this->enum->add($_GET['attr_id'],$enum_arr,$start_key);
        }else{
          $this->enum->add($_GET['attr_id'],$enum_arr);
        }
        success('admin/enum/enum_list?obj_id='.$_GET['obj_id'].'&attr_id='.$_GET['attr_id'],'添加成功');
      }
  		$this->render("admin/enum/add");
  	}
    public function edit(){
      $enum_list=$this->enum->getlist($_GET['attr_id']);
      for($i=0;$i<count($enum_list);$i++){
        $data['enum_list'][$i]=$enum_list[$i]['enum_name'];
      }
      if(!empty($_POST)){
        $enums_list = trim($_POST['enum_lists']);  
        $enum_arr = explode("\r\n", $enums_list);
        $this->enum->edit($_GET['attr_id'],$enum_arr);
         success('admin/enum/enum_list?obj_id='.$_GET['obj_id'].'&attr_id='.$_GET['attr_id'],'编辑成功');
      }
      
      $this->render("admin/enum/edit",$data);
    }
    public function del(){
       $this->enum->del($_GET['attr_id'],$_GET['enum_key']);
       success('admin/enum/enum_list?obj_id='.$_GET['obj_id'].'&attr_id='.$_GET['attr_id'],'删除成功');
    }

  }
 ?>