<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Layout extends MY_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/layout_model','layout');
		$this->layout->db->query('set names utf8');
		$this->output->enable_profiler(TRUE);
	}
	
	//布局管理新建页面
	public function lists(){
		//获取对象的字段数据
		
		$this->load->model("admin/attribute_model",'attr');
		$arr=$this->layout->attr_id($_GET['type_id'],'list',$_GET['obj_id']);
		if(!empty($arr)){
			$aa=json_decode($arr[0]['layout_json']);
			for ($i=0; $i <count($aa) ; $i++) { 
		 	$choosed_attr_id[$i]=$aa[$i]->attr_id;
		   }
		   $data['obj_attr'] = $this->layout->check_choosed_attrs($_GET['obj_id'],$choosed_attr_id);
		   $data['choosed_attr']=$this->attr->db->where('attr_obj_id',$_GET['obj_id'])->where_in('attr_id',$choosed_attr_id)->get('dd_attribute')->result_array();
		}else{
          $data['obj_attr']=$this->attr->db->get_where('dd_attribute',array('attr_obj_id'=>$_GET['obj_id']))->result_array();
		}
		$this->render('admin/layout/list',$data);
		
	}
	public function dolist(){
		
		$arr['is_exists_data']=$this->layout->db->get_where('dd_layout',array('obj_id'=>$_POST['obj_id'],'type_id'=>$_POST['type_id'],'layout_type'=>$_POST['layout_type']))->result_array();
       // p($arr);die;
        $data['layout_type']=$_POST['layout_type'];
        $data['layout_json'] = $_POST['list_info'];
        if(!empty($arr['is_exists_data'])){
			// $data['choose_attr']=$_POST['list_info'];
			$this->layout->update($data['layout_json'],$_POST['type_id'],$_POST['obj_id'],$_POST['layout_type']);
           echo 0;
		}else{
			$data['type_id']=$_POST['type_id'];
        $data['obj_id']=$_POST['obj_id'];

		
		//print_r($_POST['list_info']);
		//$data['layout_type']=$_POST['layout_type'];
		$this->layout->add($data);
		echo 1;
		}
	
        
	}
	public function edit(){
		$this->load->model("admin/attribute_model",'attr');
		$arr=$this->layout->attr_id($_GET['type_id'],$this->uri->segment(3),$_GET['obj_id']);
	
		if(!empty($arr)){
			$aa=json_decode($arr[0]['layout_json']);
			for ($i=0; $i <count($aa) ; $i++) { 
		 	$choosed_attr_id[$i]=$aa[$i]->attr_id;
		   }
		   $data['obj_attr'] = $this->layout->check_choosed_attrs($_GET['obj_id'],$choosed_attr_id);
		  // p($data['obj_attr']);die;
		   $data['se']=$this->attr->db->get_where('dd_layout',array('obj_id'=>$_GET['obj_id'],'type_id'=>$_GET['type_id'],'layout_type'=>$this->uri->segment(3)))->result_array();
		   
		   $data['choosed_attr']=json_decode($data['se'][0]['layout_json']);
		   
		}else{
          $data['obj_attr']=$this->attr->db->get_where('dd_attribute',array('attr_obj_id'=>$_GET['obj_id']))->result_array();
		}
		$this->render('admin/layout/edit',$data);
	}
	public function doedit(){
		$arr['is_exists_data']=$this->layout->db->get_where('dd_layout',array('obj_id'=>$_POST['obj_id'],'type_id'=>$_POST['type_id'],'layout_type'=>$_POST['layout_type']))->result_array();
        $data['layout_json'] = $_POST['list_info'];
        $data['layout_type']=($_POST['layout_type']=='lists')?'list':$_POST['layout_type'];
        if(!empty($arr['is_exists_data'])){
			 //$data['choose_attr']=$_POST['list_info'];
			$this->layout->update($data['layout_json'],$_POST['type_id'],$_POST['obj_id'],$_POST['layout_type']);
           echo 0;
		}else{
			$data['type_id']=$_POST['type_id'];
        $data['obj_id']=$_POST['obj_id'];

		
		//print_r($_POST['list_info']);
		//$data['layout_type']=$_POST['layout_type'];
		$this->layout->add($data);
		echo 1;
		}
	}
	
	public function view(){
		//获取对象的字段数据
		
		$this->load->model("admin/attribute_model",'attr');
		$arr=$this->layout->attr_id($_GET['type_id'],$this->uri->segment(3),$_GET['obj_id']);
		if(!empty($arr)){

			$aa=json_decode($arr[0]['layout_json']);
			for ($i=0; $i <count($aa) ; $i++) { 
		 	$choosed_attr_id[$i]=$aa[$i]->attr_id;
		   }
       
		   $data['obj_attr'] = $this->layout->check_choosed_attrs($_GET['obj_id'],$choosed_attr_id);
		
		   $data['choosed_attr']=$this->attr->db->where('attr_obj_id',$_GET['obj_id'])->where_in('attr_id',$choosed_attr_id)->get('dd_attribute')->result_array();
		}else{
          $data['obj_attr']=$this->attr->db->get_where('dd_attribute',array('attr_obj_id'=>$_GET['obj_id']))->result_array();
		}
		$this->render('admin/layout/view',$data);
		
	}
	public function doview(){
		$arr['is_exists_data']=$this->layout->db->get_where('dd_layout',array('obj_id'=>$_POST['obj_id'],'type_id'=>$_POST['type_id'],'layout_type'=>$_POST['layout_type']))->result_array();
        $data['layout_json'] = $_POST['list_info'];
        $data['layout_type']=($_POST['layout_type']=='lists')?'list':$_POST['layout_type'];
        if(!empty($arr['is_exists_data'])){
			$this->layout->update($data['layout_json'],$_POST['type_id'],$_POST['obj_id'],$_POST['layout_type']);
           echo 0;
		}else{
			$data['type_id']=$_POST['type_id'];
        $data['obj_id']=$_POST['obj_id'];

		
		//print_r($_POST['list_info']);
		//$data['layout_type']=$_POST['layout_type'];
		$this->layout->add($data);
		echo 1;
		}
	}
}
?>