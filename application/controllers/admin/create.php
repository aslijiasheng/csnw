<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Create extends MY_Controller{
	public function __construct(){
		parent::__construct();
		 $this->load->library('templateengine');

	}

	//生成数据库
	public function c_table(){
		//echo "数据库生成表";
		//获取这个对象数据
		$this->load->model('admin/object_model','obj');
		$this->obj->db->query("set names utf8");
		$data=$this->obj->getinfo($_GET['obj_id']);
		//获取相关的属性数据
		$this->load->model('admin/attribute_model','attr');
		$this->attr->db->query("set names utf8");
		$data["attr"]=$this->attr->check($_GET['obj_id']);

		// 更新数据库
//		$this->load->dbforge(); //数据库维护类
		if($this->db->table_exists('tc_'.$data["obj_name"])){
			$sql = '';
			foreach($data["attr"] as $k=>$v){
				if(!$this->db->field_exists($data["obj_name"].'_'.$v["attr_name"], 'tc_'.$data["obj_name"])){
					// 添加字段
					$sql = 'ALTER TABLE `'.'tc_'.$data["obj_name"].'` ADD `'.$data["obj_name"].'_'.$v["attr_name"].'` '.$v["attr_type_arr"]["attrtype_field_type"].' COMMENT \''.$v["attr_label"].'\';';
					// die($sql);
				}else{
					// 更新字段
				}
			$this->db->query($sql);
			}
			unset($sql);
		}

		//p($data);exit;
		//组成sql
		// $sql="SELECT * from dd_icon";
		// $is_exists=$this->db->query($sql);
		// if(!is)
		$sql = "
        CREATE TABLE IF NOT EXISTS `tc_".$data["obj_name"]."` (
	       `".$data["obj_name"]."_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
	     ";
		//循环所有的属性
//	     p($data["attr"]);die;
		foreach ($data["attr"] as $k=>$v){
			//属性类型相关的写法
			switch ($v["attr_type_arr"]["attrtype_id"]){
                case 5:  //数值
                case 7:  //金额
                case 8:  //短文本类型
                case 9:  //长文本类型
				case 10: //自动增长类型
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
                case 18: //时间类型
				case 19: //引用类型
				case 21:
				case 22:
				case 20:
					$field_type = $v["attr_type_arr"]["attrtype_field_type"];
					break;
				default:
					echo "该属性类型没有定义！";
					p($v);
					exit;
			}
			$is_requerido="";//是否必填（默认都是不必填的）
//			$is_requerido="NOT NULL";//是否必填（默认都是不必填的）
			//echo $field_type;
			//p($v["attr_type"]);exit;
			$sql .= "
			`".$v["attr_field_name"]."` ".$field_type." ".$is_requerido." COMMENT '".$v["attr_label"]."',
			";
		}
		//判断是否包含类型，如果包含类型添加一个type_id
		if($data['is_obj_type']==1){
			$sql .= "`type_id` int(11) NOT NULL COMMENT '所属类型',";
		}
		$sql .= "
			PRIMARY KEY (`".$data["obj_name"]."_id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
		";
		//echo $sql;

	    $this->db->query($sql);
		echo "创建完毕";

		p($sql);

		$this->tools();
	}

	//生成模型
	public function c_model(){
		//obj_id根据获取obj_name
		$this->load->model("admin/object_model",'obj');
		$this->obj->db->query("set names utf8");
		$data=$this->obj->getinfo($_GET['obj_id']);
		$data['obj_name_uc'] = ucfirst($data['obj_name']); //对象名称改成大写
		$data['obj_table_name'] = "tc_".$data['obj_name']; //表名
		//获取对象的字段数据用于添加attributeLabels
		$this->load->model("admin/attribute_model",'attr');
		$data['obj_attr'] = $this->attr->check($_GET['obj_id']);
		//从布局管理里获取列表页面的布局信息
		//p($data);die;
		$this->load->model("admin/layout_model",'layout');





			$where = array(
				'obj_id'=>$_GET['obj_id'],
				'layout_type'=>'list',
				'type_id'=>0,
		    );
			$data['list_layout'] = $this->layout->arr_getinfo($where);
			$where = array(
				'obj_id'=>$_GET['obj_id'],
				'layout_type'=>'view',
				'type_id'=>0,
			);
			$data['view_layout'] = $this->layout->arr_getinfo($where);
			$where = array(
				'obj_id'=>$_GET['obj_id'],
				'layout_type'=>'edit',
				'type_id'=>0,
			);
			$data['edit_layout'] = $this->layout->arr_getinfo($where);

		//p($data['list_layout']);exit;
		$obj_name=strtolower($data['obj_name']);
		$dir="application/apptest/models/www/"; //生成的文件的位子
		//检查是否有这个文件夹，没有则创建
		if(!file_exists($dir)){
			mkdir($dir);
		}
		//获取模本文件信息
		$row = file_get_contents('application/template/models/model.php');
		$row = $this->templateengine->replace_rule($row,$data);
		//写入文件
		file_put_contents($dir.$obj_name.'_model.php',$row);
		echo "model生成成功";
		//p($data);

		$this->tools();
	}

	//生成控制器
	public function c_controller(){
		//obj_id根据获取obj_name
		$this->load->model("admin/object_model",'obj');
		$this->obj->db->query("set names utf8");
		$data=$this->obj->getinfo($_GET['obj_id']);
		$data['obj_name_uc'] = ucfirst($data['obj_name']);
		$obj_name=strtolower($data['obj_name']);
		//获取对象的字段数据用于添加attributeLabels
		$this->load->model("admin/attribute_model",'attr');
		$data['obj_attr'] = $this->attr->check($_GET['obj_id']);

		//首先这里判断下是否包含类型
		if ($data['is_obj_type']==1){
			//查询都有哪些类型
			$this->load->model("admin/type_model",'type');
			$where=array(
				'obj_id'=>$_GET['obj_id'],
			);
			$data['type_arr'] = $this->type->getinfo($where);
		}

		//生成的文件的位子
		$dir="application/apptest/controllers/www/";
		if(!file_exists($dir)){
			mkdir($dir);
		}
		//获取模本文件信息
		$row = file_get_contents('application/template/controllers/controller.php');
		$row = $this->templateengine->replace_rule($row,$data);
		//添加各种行动
		//查询出有哪些行动
		$this->load->model("admin/action_model",'action');
		$data['ac_lists']=$this->action->getActions($data['obj_id']);

		//这里需要组成一个数组 用于给组成查询的json关系(暂时将所有属性作为查询条件)
		//pj($data);exit;
		//pj($data['obj_attr']);exit;
		foreach ($data['obj_attr'] as $k=>$v){
			$data['select_attr'][$k]['attr_field_name']=$v['attr_field_name']; //属性的字段名
			$data['select_attr'][$k]['attr_label']=$v['attr_label']; //属性的中文标签
			$data['select_attr'][$k]['attr_type']=$v["attr_type_arr"]["attrtype_id"]; //属性类型ID
			$data['select_attr'][$k]['attrtype_name']=$v['attr_type_arr']['attrtype_name']; //属性类型中文标签
			//下面是添加各种类型的附加属性
			switch ($v["attr_type_arr"]["attrtype_id"]){
				case 19: //引用类型
					$data['select_attr'][$k]['d_attr']['url']=site_url('www/'.$v['attr_quote_id_arr']['obj_name'].'/ajax_list'); //ajax调用关联的地址
					break;
				case 14: //单选
				case 15: //下拉框
				case 16: //多选   //这3个都是枚举！参数里给该枚举的所有值
					$this->load->model('admin/enum_model','enum');
					$data['select_attr'][$k]['d_attr']['enum_arr']=$this->enum->getlist($v['attr_id']);
					break;
                case 5:  //数值
                case 7:  //金额
                case 8:  //短文本类型
                case 9:  //长文本类型
				case 10: //自动增长类型
				case 11:
				case 12:
				case 13:
				case 17:
                case 18: //时间类型
				case 20:
				case 21:
				case 22:
				case 23:
					break;
				default:
					exit;
			}
		}
		//最后将内容转化成json数组
		$data['select_attr_json']=json_encode($data['select_attr']);
		//pj($data['select_attr']);exit;
		//循环所有行动
		//foreach()
		//pj($data);die;
		$function_all = "";
		foreach ($data['ac_lists'] as $arr) {
			$file_name = $arr['controller_template'][0]['template_path'];
			$function_all .= file_get_contents('application/template/'.$file_name);
			$function_all = $this->templateengine->replace_rule($function_all,$data);
			echo "控制器".$arr['controller_template'][0]['template_name']."生成完毕。";
			echo "<br>";
		}

		//p($function_all);exit;
		//$function_all = $this->templateengine->replace_rule($function_all,$data);
		/*
		foreach ($data['ac_lists'] as $key=>$value){
			$function_all .= $value['content']."\r\n";
		}
		*/
		$row = str_replace("{#function_all#}",$function_all,$row); //将这些行动写到控制器里去
		//生成控制器
		//写入文件
		file_put_contents($dir.$obj_name.'.php',$row);
		/*
		$fp_view=fopen($dir.$obj_name.'.php','w');
		fwrite($fp_view,$row);
		fclose($fp_view);
		*/
		$this->tools();
	}

	//生成视图
	public function c_view(){
		//obj_id根据获取obj_name
		$this->load->model("admin/object_model",'obj');
		$this->obj->db->query("set names utf8");
		$data=$this->obj->getinfo($_GET['obj_id']);
		//获取对象的字段数据
		$this->load->model("admin/attribute_model",'attr');
		$data['obj_attr'] = $this->attr->check($_GET['obj_id']);
		//p($data['obj_attr']);exit;
		$obj_name=strtolower($data['obj_name']);

		$dir="application/apptest/views/www/".$obj_name."/";
		if(!file_exists($dir)){
			mkdir($dir);
		}
		//首先这里判断下是否包含类型
		if ($data['is_obj_type']==1){
			//查询都有哪些类型
			$this->load->model("admin/type_model",'type');
			$where=array(
				'obj_id'=>$_GET['obj_id'],
			);
			$data['type_arr'] = $this->type->getinfo($where);
		}
		//p($data);exit;
		//exit;
		$this->load->model("admin/layout_model",'layout');
		$where = array(
			'obj_id'=>$_GET['obj_id'],
			'layout_type'=>'list',
			'type_id'=>0,
		);
		$data['list_layout'] = $this->layout->arr_getinfo($where);
		$where = array(
			'obj_id'=>$_GET['obj_id'],
			'layout_type'=>'view',
			'type_id'=>0,
		);
		$data['view_layout'] = $this->layout->arr_getinfo($where);
		$where = array(
			'obj_id'=>$_GET['obj_id'],
			'layout_type'=>'edit',
			'type_id'=>0,
		);
		$data['edit_layout'] = $this->layout->arr_getinfo($where);
		//pj($data);die;
		//p($data['edit_layout']);die;
		//查询行动里的数据然后循环出所有需要生成的视图
		$this->load->model('admin/action_model','action');
		$data['action'] = $this->action->listGetInfo($_GET['obj_id']);
		//pj($data);die;
		// $row = file_get_contents('application/template/views/list.php');
		// 	$row = $this->templateengine->replace_rule($row,$data);
		// 	file_put_contents($dir.'list.php',$row);
		// 	echo "列表视图生成完毕。"."<br>";

		foreach ($data['action'] as $k=>$v){

			if(isset($v['view_template_id_arr'])){
			  $row = file_get_contents('application/template/'.$v['view_template_id_arr']['template_path']);
			  $row = $this->templateengine->replace_rule($row,$data);
			//写入文件
			file_put_contents($dir.$v["act_name"].'.php',$row);
			echo "视图 '".$v['act_description']."' 生成完毕。";echo "<br>";
		   }
		}

		$this->tools();

	}

	//为了方便的小工具
	public function tools(){
		echo '<br>';
		echo '<a href="'.site_url('admin/object/index').'">返回</a>';
		echo '<br>';
		echo '<a href="'.site_url('admin/create/c_table?obj_id='.$_GET['obj_id']).'">生成数据库表table</a>';
		echo '<br>';
		echo '<a href="'.site_url('admin/create/c_model?obj_id='.$_GET['obj_id']).'">生成模型model</a>';
		echo '<br>';
		echo '<a href="'.site_url('admin/create/c_controller?obj_id='.$_GET['obj_id']).'">生成控制器controller</a>';
		echo '<br>';
		echo '<a href="'.site_url('admin/create/c_view?obj_id='.$_GET['obj_id']).'">生成视图view</a>';
	}
}
?>
