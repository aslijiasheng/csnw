<?php
  class Templategroupmanagement extends MY_Controller{
    public function __construct(){
      parent::__construct();
      $this->load->model('admin/templates_model','templates');
    }
  	public function index(){

  		$this->render('admin/template/index');
  	}

  	public function add(){
  		$this->render('admin/templategroup/tem/add');
  	}

  }
 ?>