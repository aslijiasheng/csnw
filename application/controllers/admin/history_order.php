<?php
/**
* 处理有些订单没有明细数据方法
*/
class history_order extends MY_Controller {
	
	public $menu1 = "tools";
	public $menu2 = "history";
	
	public function __construct() {
		parent::__construct();
		date_default_timezone_set('PRC');
		$this->output->enable_profiler(false);
		$this->db->query('set names utf8');
		$this->load->model('www/api_model', 'api');
		$this->load->model('www/order_model', 'order');
	}

	/**
	* 入口文件
	*/
	public function index() {
		$this->render('admin/his/add');
	}
	
	/**
	* 初始数据接口
	* 例：[{"goods.code":"goods_0463","goods_amount":"20000.00","goods_price.code":"gp00000000540","order_number":"so201405050018"},{"goods.code":"goods_0463","goods_amount":"20000.00","goods_price.code":"gp00000000540","order_number":"so201405050018"},{"goods.code":"goods_0463","goods_amount":"20000.00","goods_price.code":"gp00000000540","order_number":"so201405050018"}]
	
	*/
	public function his_index() {
		$params = array('user' => 'ndpSync', 'pwd' => '0fe11d6036b2b382', 'action' => 'add', 'object' => 'order');
		$params['data'] = $_POST['json_data'];
		extract($params, EXTR_OVERWRITE);
		$data = is_array($data) ? $data : json_decode($data, true);
		foreach ($data as $key => $value) {
			$this->add($value, $object, $user);
		}
	}
	
	private function add($data, $object, $user) {
		$order_number = $data['order_number'];
		unset($data['order_number']);
		$where = array(
						"order_number" => $order_number
					   );
		$order = $this->order->getOrderId( $where );
		$detailed = $data;
		$this->load->model('www/order_d_model', 'order_d');
		$res_pd = $this->api->parseData($detailed, 'order_d');
		if (!isset($res_pd['res']) or $res_pd['res'] != 'succ')
			return $res_pd;
		$detailed = $res_pd['info'];

		$res_pd = $this->order_d->_addCheck($detailed, $user); //_check检查对象内容，是否有必填项
		if (!isset($res_pd['res']) or $res_pd['res'] != 'succ')
			return $res_pd;
		$detailed['order_d'] = $res_pd['info'];

		$fields = 'order_d' . '_' . $object . '_id';
		$res_d = $this->api->dealDetailData($detailed['order_d']);
		$res_od = $this->api->saveOrderDetail($res_d, $order['order_id'], 0); //返回商品明细

	}
}