<?php

	/**
	* @开发工具实体类
	*/
	class DeveloperTools extends MY_Controller {

		public $menu1='tools';
		public $menu2='DeveloperTools';
		
		//构造函数
		public function __construct() {
			parent::__construct();
			date_default_timezone_set('PRC');
			$this->output->enable_profiler(false);
		}
		
		/**
		* @初始化程序
		*/
		public function home() {
			$this->render('admin/developertools/home');
		}
		
		/**
		* @接口初始化程序
		*/
		public function api_inerface() {
			//获取传输过来的数据
			$api_inerface = $_POST;
			//判断传输过来的数据名称,根据不同名称,跳转不同的方法,进行处理
			switch($api_inerface["tools"])
			{
				case "order":
					$this->api_order($_POST);
					break;
				case "timing":
					break;
			}
			
		}
		
		/**
		* @order接口程序处理方法
		* @params $data 传入的数据
		* @return bool_array 返回状态码是否正常执行
		*/
		private function api_order( $data ) {
			if(isset($data))
			{
				unset($data["tools"]);//释放掉tools键值
				$m = explode( "_", __FUNCTION__ );//根据方法名取出要执行的MODEL
				$module = $m[1];
				$this->load->model( "www/".$module."_model", $module );
				$bol = $this->$module->api_del( $data["order_number"] );
				if ($bol) {
					success('admin/DeveloperTools/home', '删除成功');
				} else {
					//error("删除执行失败");
				}
			}
			else
			{
				error("无数据");
			}
		}
		
	}

 ?>