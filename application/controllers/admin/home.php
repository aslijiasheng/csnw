<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   class Home extends MY_Controller{
   	 public function __construct(){
       parent::__construct();
   	 }
   	public function index(){
		/*
		if(!$this->session->userdata('au_name')){
			redirect('admin/home/login');
		}
		*/
		$this->render('admin/home/home');
   	}

   	public function login(){
   		$this->load->helper('form');
   		$config=array(
           array('field'=>'au_name','label'=>'登陆名','rules'=>'required'),
           array('field'=>'au_password','label'=>'密码','rules'=>'required'),

   		);
       $this->load->library('form_validation', $config);

      if(isset($_POST['submit'])){
        // p($this->input->post(''));
        if($this->form_validation->run() != FALSE){
          $this->load->model('admin/auth_model','vali');
          //echo $this->input->post('au_name');die;
          $data=$this->vali->check($this->input->post('au_name'));
          
          if($data!=null){
          

            if($data[0]['au_password']==md5($this->input->post('au_password'))){
              
              $array = array(
                'au_name' => $data[0]['au_name'],
              );
              
              $this->session->set_userdata( $array );
              
              success('admin/home','登录成功');
            }else{
              $this->session->set_userdata('error_message', '密码错误');
            }
          }else{
           
            $this->session->set_userdata('error_message', '用户名不正确');
          }
         
        }else{
        	$this->session->set_userdata('error_message', null);
        }
          
       }
       
  		
   		
      $this->load->view('admin/home/login');
   	}

   	public function out(){
      $this->session->sess_destroy();
      redirect('admin/home/login');
   	}
   }
 ?>