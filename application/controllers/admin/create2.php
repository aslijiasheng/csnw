<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   class Create extends MY_Controller{
     public function __construct(){
       parent::__construct();

     }

   	 public function c_table(){
   	 	$this->load->model('admin/table_model','table');
       $data=$this->table->get_obj_name($_GET['obj_id']);
       $tabfields=$this->table->db->select('attr_name,attr_field_type')->from('dd_attribute')->where('attr_obj_id',$_GET['obj_id'])->get()->result_array();
       $tab_name=$data[0]['obj_name'];
      $this->table->create_table($tab_name,$tabfields); 
      success('admin/object/','create successfully');
   }
/*
   public function c_model(){
   	   $this->load->model('admin/table_model','model');
       $data=$this->model->get_obj_name($_GET['obj_id']);
       $model_name= ucfirst($data[0]['obj_name']); 
       $content="public function index(){}";
       $html_name=strtolower($model_name)."_model.php";
       $fp_tmp=fopen(base_url().'application/models/admin/obj_model/temple.html','r');
       $fp_html=fopen('application/models/www/'.$html_name,'w');
       while(!feof($fp_tmp)){
       	 $row=fgets($fp_tmp);
       	 $new_row=replace($row,$model_name,$content);
       	 fwrite($fp_html,$new_row);
       }
       fclose($fp_html);
       success('admin/object/','create successfully');
   }


   public function c_controller(){
   	    $this->load->model('admin/table_model','table');
        $data=$this->table->get_obj_name($_GET['obj_id']);
        $controller_name=ucfirst($data[0]['obj_name']);
        $this->table->db->query('set names utf8');
        $data['conlist']=$this->table->get_content($_GET['obj_id']);
        $content="";  
        foreach ($data['conlist'] as $v) {  
          $content=$content.'
           '.$v['content'];
          }
        $file_name=strtolower($controller_name).".php";
        $fp_tmp=fopen(base_url().'application/controllers/admin/obj_controller/template.html','r');
        $fp_html=fopen('application/controllers/www/'.$file_name,'w');
        while(!feof($fp_tmp)){
        	$row=fgets($fp_tmp);
        	$new_row=replace($row,$controller_name,$content);
        	fwrite($fp_html,$new_row);
        }
       fclose($fp_html);
       success('admin/object/','create successfully');
   }
*/	
	//生成视图
	public function c_model(){
		//obj_id根据获取obj_name
		$this->load->model("admin/object_model",'obj');
		$this->obj->db->query("set names utf8");
		$data=$this->obj->getinfo($_GET['obj_id']);
		$data['obj_name_uc'] = ucfirst($data['obj_name']);
		$obj_name=strtolower($data['obj_name']);
		$dir="application/models/www/"; //生成的文件的位子
		//检查是否有这个文件夹，没有则创建
		if(!file_exists($dir)){
			mkdir($dir);
		}
		//获取模本文件信息
		$row = file_get_contents('application/template/models/model.php');
		$row = $this->replace_rule($row,$data);
		echo "<pre>";
		echo $row;
		exit;
	}
	
	//生成控制器
	public function c_controller(){

		//obj_id根据获取obj_name
		$this->load->model("admin/object_model",'obj');
		$this->obj->db->query("set names utf8");
		$data=$this->obj->getinfo($_GET['obj_id']);
		$data['obj_name_uc'] = ucfirst($data['obj_name']);
		$obj_name=strtolower($data['obj_name']);
		//生成的文件的位子
		$dir="application/controllers/www/";
		if(!file_exists($dir)){
			mkdir($dir);
		}
		//获取模本文件信息
		$row = file_get_contents('application/template/controllers/controller.php');
		$row = $this->replace_rule($row,$data);
		//添加各种行动
		//查询出有哪些行动
		$this->load->model("admin/action_model",'action');
		$data['ac_lists']=$this->action->getActions($data['obj_id']);
		//p($data);exit;
		//循环所有行动
		$function_all = "";
		foreach ($data['ac_lists'] as $key=>$value){
			$function_all .= $value['content']."\r\n";
		}
		$row = str_replace("{#function_all#}",$function_all,$row); //将这些行动写到控制器里去
		//echo "<pre>";
		//echo $row;
		//生成控制器
		//写入文件
		$fp_view=fopen($dir.$obj_name.'.php','w');
		fwrite($fp_view,$row);
		fclose($fp_view);
		echo "ok";
	}
	
	//生成视图
	public function c_view(){
		//obj_id根据获取obj_name
		$this->load->model("admin/object_model",'obj');
		$this->obj->db->query("set names utf8");
		$data=$this->obj->getinfo($_GET['obj_id']);
		$obj_name=strtolower($data['obj_name']);

		$dir="application/views/www/".$obj_name."/";
		if(!file_exists($dir)){
			mkdir($dir);
		}
		//生成list视图
		//获取模本文件信息
		$row = file_get_contents('application/template/views/list.php');
		//模板的正则替换规则
		//echo $row;exit;
		$row = $this->replace_rule($row,$data);
		//echo "<pre>";print_r($data);exit;
		//写入文件
		$fp_view=fopen($dir.'list.php','w');
		fwrite($fp_view,$row);
		fclose($fp_view);
		echo "ok";
	}
	
	//模板和参数的替换规则
	public function replace_rule($row,$data){
		//将所有{#...#}中间的内容全部替换成对应的数据
		$zz = "/{#([^#]*)#}/iUs";
		preg_match_all($zz,$row,$arr);
		//p($arr);exit;
		//循环所有的值！然后替换掉
		foreach ($arr[1] as $key=>$value){
			if (isset($data[$value])){
				$row = str_replace($arr[0][$key],$data[$value],$row);
			}else{
				//将所有其他的类型替换成不同的方法来实现
			}
		}
		return $row;
		//$this->lee_cs($row);
		//p($data);
		//exit;
		//p($arr);exit;
	}

	//模板测试
	public function lee_cs($row){
		echo $this->load->view('admin/layouts/head','',true);
		echo $row;
		echo $this->load->view('admin/layouts/bottom','',true);
	}

   public function c_view2(){
    
    //根据获取obj_name
    $this->load->model("admin/object_model",'obj');
    $this->obj->db->query("set names utf8");
    $data=$this->obj->getinfo($_GET['obj_id']);
    //p($data);exit;
    $obj_name=strtolower($data[0]['obj_name']);

    $dir="application/views/www/".$obj_name;
    if(!file_exists($dir)){
      mkdir($dir);
    }
    //生成list视图
    //获取模本文件信息
    $row = file_get_contents('application/template/views/list.php');
    //模板的正则替换规则
    //echo $row;exit;
    $this->replace_rule($row,$data);
    //echo "<pre>";print_r($data);exit;
    //写入文件
    $fp_view=fopen($dir.'list.php','w');
    fwrite($fp_view,$row);
    fclose($fp_view); 


  }
  public function replace_rule($row,$data){
    //p($data);
    //将所有{#...#}中间的内容全部替换成对应的数据
    $zz = "/{#([^#]*)#}/iUs";
    preg_match_all($zz,$row,$arr);
    p($arr);exit;
    //循环所有的值！然后替换掉
    foreach ($arr[1] as $key=>$value){
      if (isset($data[$value])){
        $row = str_replace($arr[0][$key],$data[$value],$row);
      }elseif ($key=="forattr"){

      }
    }
    echo $row;exit;
    p($arr);exit;
  }

   // public function c_view(){
      
   //   $this->load->model("admin/attribute_model",'view');
   //   $this->view->db->query("set names utf8");
   //   $data=$this->view->db->select("obj_name")->from("dd_object")->where("obj_id",$this->input->get("obj_id"))->get()->result_array();
   //   $obj_name=strtolower($data[0]['obj_name']);
   //   $dir="application/views/www/".$obj_name;
     
   //  if(!file_exists($dir)){
   //    mkdir($dir);
   //   }

   //   $data['attr_info']=$this->view->db->get_where("dd_attribute",array("attr_obj_id"=>$this->input->get("obj_id")))->result_array();
   //   $content="";
   //   foreach($data['attr_info'] as $v){
   //     switch($v['attr_field_type']){
   //      case 1:
   //      case 2:
   //        $content.=$this->textField($v, $obj_name);
   //                  break;
   //      case 3:
   //        $content.=$this->reference($v, $obj_name);
   //        break;
   //      case 6:
   //        $content.=$this->radiobutton($v, $obj_name);
   //        break;
   //      case 7:
   //        $content.=$this->checkbox($v, $obj_name);
   //        break;
   //      case 8:
   //        $content.=$this->dropdownlist($v, $obj_name);
   //        break;        
   //     }
   //   }
   //   //创建add视图
   //   $fp_tmp=fopen('application/views/www/add_template.php',"r");
   //   $fp_view=fopen('application/views/www/'.$obj_name.'/add.php','w');
   //    while(!feof($fp_tmp)){
   //      $row=fgets($fp_tmp);
   //      $new_row=replace_view($row, $content);
   //      fwrite($fp_view,$new_row);
   //    }
   //    fclose($fp_view); 
       
   //    //创建index视图
   //    $this->load->library('table');
   //      $data['value']=$this->view->db->query("select * from dd_obj_".$obj_name)->result_array();
   //      $tmpl=array('table_open'=> '<table border="0" cellpadding="4" cellspacing="0" class="table">',);
   //      $data['key']=$this->view->db->select('attr_label')->from('dd_attribute')->where('attr_obj_id',$_GET['obj_id'])->get()->result_array();
   //      for($i=0;$i<count($data['key']);$i++){
   //        $arr[$i]=$data['key'][$i]['attr_label'];
   //      }
   //      $this->table->set_heading($arr);
   //      $this->table->set_template($tmpl); 
   //      $index_content=$this->table->generate($data['value']);
   //      p($index_content);
   //      $fp_tmp=fopen('application/views/www/index_template.php',"r");
   //      $fp_view=fopen('application/views/www/'.$obj_name.'/index.php','w');
   //      while(!feof($fp_tmp)){
   //        $row=fgets($fp_tmp);
   //        $new_row=replace_view($row,$index_content);
   //        fwrite($fp_view,$new_row);
   //      }
   //      fclose($fp_view);
   //     success('admin/object/',"成功生成试图");
   
   // }
}
?>
