<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('element'))
{
	function getArray($filename)
	{
		$path = strtolower(FCPATH."common/".$filename.".php");
		if ( ! file_exists($path) )
		{
			return null;
		}
		return require_once $path;
	}
}




?>