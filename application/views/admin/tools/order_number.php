<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						删除工具
						<span class="mini-title">
							order_number
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<input type="hidden" name="tools" value="order" method="post" action="admin/DeveloperTools/api_inerface">
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
							<a id="newtiming_task" class="btn btn-primary" href="<?php echo site_url('admin/ ') ?>">
								<i class="icon-file"></i>
								新增删除工具
							</a>
						</div>
						<div id="divmessagelist">
							<div class="grid-view">
								<div id="timing_task_list"></div>
								<div>
									<script type="text/javascript">
										$(document).ready(function() {
											$selectAttr = [
												{'value': 'do_time', 'txt': ' '},
												{'value': 'url', 'txt': ' '},
												{'value': 'note', 'txt': ' '},
												{'value': 'status', 'txt': ' '},
												
											];
											$("#timing_task_list").leeDataTable({
												selectAttr: $selectAttr, //简单查询的查询属性
												url: "<?php echo site_url('admin/order_number/ajax_select'); ?>", //ajax查询的地址
												perNumber: 10 //每页显示多少条数据
											});
										});
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>