<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						属性类型管理
						<span class="mini-title">
						Attr Type
						</span>
					</div>
					<span class="tools">
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
							<a href="<?php echo site_url('admin/attrtype/add') ?>" class="btn btn-primary"><i class="icon-file"></i>新增类型</a>
						</div>
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
	<div id="dt_example" class="example_alt_pagination">
		<table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
			<thead>
			<tr>
			  <th>操作</th>
			  <th>属性类型id</th>
			  <th>属性类型名</th>
			  <th>列表页面模板id</th>
			  <th>增加编辑页面模板id</th>
			  <th>查询条件处模板id</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($list as $v): ?>
				<tr>
					<td><a href="<?php echo site_url('admin/attrtype/edit?attrtype_id=').$v['attrtype_id'] ?>" title="编辑"><span data-icon="&#xe005"></span></a></td>
					<td><?php echo $v['attrtype_id']?></td>
					<td><?php echo $v['attrtype_name']?></td>
					<td><?php echo $v['attrtype_view_template']?></td>
					<td><?php echo $v['attrtype_edit_template']?></td>
					<td><?php echo $v['attrtype_select_template']?></td>
				</tr>
			<?php endforeach ?>	
			</tbody>
		</table>
	</div>
</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


    <script src="<?php echo base_url();?>style/admin/js/jquery.dataTables.js">
    </script>
	<script type="text/javascript">
	//Data Tables
	$(document).ready(function () {
		$('#data-table').dataTable({
			"sPaginationType": "full_numbers"
		});
	});

	</script>