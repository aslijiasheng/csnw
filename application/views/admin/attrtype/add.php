<script>
  $(document).ready(function(){
        $("span[class=add-on]").css('cursor','pointer');
        $("span[class=add-on]").eq(0).click(function(){
         $.ajax({
           'type':'get',
           'data':{},
           'success':function(data){

              $("#dialog").html(data);    
           },
           'url':'<?php echo site_url("admin/template/template_list")."?type=view_1" ?>'
         });
         $("#dialog").dialog({
            height: 500,
             width: 800,
            modal: true,

         });

     })
        $("span[class=add-on]").eq(1).click(function(){
         $.ajax({
           'type':'get',
           'data':{},
           'success':function(data){

              $("#dialog").html(data);    
           },
           'url':'<?php echo site_url("admin/template/template_list")."?type=view_2" ?>'
         });
         $("#dialog").dialog({
            height: 500,
             width: 800,
            modal: true,

         });

     })
        $("span[class=add-on]").eq(2).click(function(){
         $.ajax({
           'type':'get',
           'data':{},
           'success':function(data){

              $("#dialog").html(data);    
           },
           'url':'<?php echo site_url("admin/template/template_list")."?type=view_3" ?>'
         });
         $("#dialog").dialog({
            height: 500,
             width: 800,
            modal: true,

         });

     })

  })
</script>

<div class="all-sidebar">
  <div class="row-fluid">
    <div class="span12">
      <div class="widget">
        <div class="widget-header">
          <div class="title">
            添加属性类型
            <span class="mini-title">
            AttrType
            </span>
          </div>
          <span class="tools">
            <!-- 窗口按钮部分
            <a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
            -->
          </span>
        </div>
        <div class="widget-body">
          <div class="row-fluid">
            <div class="leebutton">
              <a id="yw0" class="btn btn-primary" href="<?php echo site_url('admin/attrtype/type_list')?>">
              <i class="icon-undo"></i>
              返回
              </a>
            </div>
            <div id="divmessagelist">
              <div style="overflow:auto;">
<div class="grid-view">
  <form class="form-horizontal" action="<?php echo site_url('admin/attrtype/add') ?>" method="post">
  <div class="control-group">
    <label class="control-label" for="attrtype">属性类型名<span class="required">*</span></label>
    <div class="controls">
      <input type="text" id="attrtype" placeholder="属性类型名" name="attrtype_name" value="<?php echo set_value("attrtype_name") ?>"><?php echo "<span class='required'>".form_error('attrtype_name')."</span>" ?>
    </div>
  </div>
  <div class="control-group input-append">
     <label for="" class="control-label required">列表页面模板1<span class="required">*</span></label>
     <div class="controls"><input type="text" readonly id="view1_template_name" name="view1_template" value="<?php echo set_value("view1_template") ?>"><span class="add-on"><i class="icon-search"></i></span></div>
     <input type="hidden" name="view_template" id="view1_template_id">
     <?php echo "<span>".form_error("view1_template")."</span>" ?>
   </div>
   <br>
     <div class="control-group input-append">
     <label for="" class="control-label required">(编辑/添加)页面模板<span class="required">*</span></label>
     <div class="controls"><input type="text"  readonly id="view2_template_name" name="view2_template" value="<?php echo set_value('view2_template') ?>"><span class="add-on"><i class="icon-search"></i></span></div>
     <input type="hidden" name="edit_template" id="view2_template_id">
     <?php echo "<span>".form_error("view2_template")."</span>" ?>
   </div>
   <br>
     <div class="control-group input-append">
     <label for="" class="control-label required">搜索部分模板<span class="required">*</span></label>
     <div class="controls"><input type="text" readonly id="view3_template_name" name="view3_template" value="<?php echo set_value('view3_template') ?>"><span class="add-on"><i class="icon-search"></i></span></div>
     <input type="hidden" name="search_template" id="view3_template_id">
     <?php echo "<span>".form_error("view3_template")."</span>" ?>
   </div>
  <div class="control-group">
    <div class="controls">
     <input type="submit" value="提交" class="btn">
    </div>
  </div>
</form>
 <div id="dialog">
</div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

