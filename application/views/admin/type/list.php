<div class="container">
   	<div class="leebutton">
      <a href="<?php echo site_url('admin/type/add').'?obj_id='.$_GET['obj_id'] ?>" class="btn btn-primary">
        <span data-icon='&#xe102'></span>
        新增
      </a>
    </div>
    <br>
	<table class="table table-bordered">
		<tr>
		  <th width="10%">操作</th>
		   <th width="15%">类型编号</th>
		   <th width="15%">类型名称</th>
		   <th width="15%">类型菜单标签</th>
           <th width="15%">明细列表</th>
		 </tr>
		 <?php foreach($typelist as $v) :?>
		 	<tr>
		 		<td>
					<a href="<?php echo site_url('admin/type/edit').'?type_id='.$v['type_id'].'&obj_id='.$v['obj_id'] ?>" rel="tooltip" title="编辑"><span data-icon="&#xe005"></span></a>
					<a href="<?php echo site_url('admin/type/del').'?obj_id='.$v['obj_id'].'&type_id='.$v['type_id'] ?>" rel="tooltip" title="删除"><span data-icon="&#xe0a8"></span></a>
					<div class="btn-group">
				      <a href="" rel="tooltip" title="生成" data-toggle="dropdown">
					      <span data-icon="&#xe1b8"></span>
				      </a>
				      <ul class="dropdown-menu" role="menu" class="dropdown-menu">
				        <li><a tabindex="-1" href="<?php echo site_url('admin/layout/lists').'?obj_id='.$v['obj_id'].'&type_id='.$v['type_id'] ?>">列表页面管理</a></li>
					    <li><a tabindex="-1" href="<?php echo site_url('admin/layout/edit').'?obj_id='.$v['obj_id'].'&type_id='.$v['type_id'] ?>">新增编辑页面管理</a></li>
					    <li><a tabindex="-1" href="<?php echo site_url('admin/layout/view').'?obj_id='.$v['obj_id'].'&type_id='.$v['type_id'] ?>">查看页面管理</a></li>

				      </ul>
			       </div>
				   <a href="<?php echo site_url('admin/detail/index').'?obj_id='.$v['obj_id'].'&type_id='.$v['type_id'] ?>" rel="tooltip" title="增加明细"><span class="icon-share"></span></a>
		 		</td>
		 		<td><?php echo $v['type_id'] ?></td>
		 		<td><?php echo $v['type_name'] ?></td>
                <td><?php echo $v['type_label'] ?></td>
                <td>
                    <?php if($v['detaillist']):
                        foreach($v['detaillist'] as $k=>$v):
                            echo '<a href="'.site_url('admin/attribute').'?obj_id='.$v['obj_id'].'">';
                            echo $v['obj_name'].'（'.$v['obj_label'].'）';
                            echo '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                        endforeach;
                    endif; ?>
                </td>
		 	</tr>
		 <?php endforeach ?>
	</table>
</div>