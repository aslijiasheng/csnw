<div>
  <form class="form-horizontal" action="<?php echo site_url('admin/type/add').'?obj_id='.$_GET['obj_id'] ?>" method="POST">
    <div class="control-group">
    <label class="control-label" for="type_name">类型名称（中文）</label>
    <div class="controls">
    <input type="text" id="type_name" name="type_name" placeholder="类型名称（中文）" value="<?php echo set_value('type_name') ?>"><span style="color:red"><?php echo form_error('type_name') ?></span>
    </div>
    </div>
	<div class="control-group">
    <label class="control-label" for="type_label">类型菜单标签（英文）</label>
    <div class="controls">
    <input type="text" id="type_label" name="type_label" placeholder="类型菜单标签（英文）" value="<?php echo set_value('type_label') ?>"><span style="color:red"><?php echo form_error('type_label') ?></span>
    </div>
    </div>
    <div class="control-group">
    <div class="controls">
    <button type="submit" class="btn">提交</button>
    </div>
    </div>
  </form>
</div>