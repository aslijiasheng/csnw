<div class="all-sidebar">
  <div class="row-fluid">
    <div class="span12">
      <div class="widget">
        <div class="widget-header">
          <div class="title">
            行动管理
            <span class="mini-title">
            Actions
            </span>
          </div>
          <span class="tools">
          </span>
        </div>
        <div class="widget-body">
          <div class="row-fluid">
            <div class="leebutton">
              <a href="<?php echo site_url('admin/object/')."?obj_id=".$_GET["obj_id"]?>" class="btn btn-primary"><i class="icon-undo"></i> 返回对象列表</a>
              <a href="<?php echo site_url('admin/action/add_action')."?obj_id=".$_GET["obj_id"]?>" class="btn btn-primary"><i class="icon-file"></i> 新增行动</a>
            </div>
            <div id="divmessagelist">
              <div>
<div id="objlist" class="grid-view">
  <div id="dt_example" class="example_alt_pagination">
    <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
      <caption><h3><?php echo $obj_name[0]['obj_name'].'对象'?></h3></caption>
      <thead>
               <tr>
                <th width="10%">行为Id</th>
               <th width="10%">行为名称</th>
               <th width="10%">描述</th>
               <th width="10%" >创建时间</th>
               <th width="20%">控制器模板id</th>
               <th width="20%">视图模板id</th>
               <th width="20%">操作</th>
              </tr>
      </thead>
      <tbody>
      <?php if(!empty($ac_lists)){ ?>
              <?php  foreach($ac_lists as $v) :?>
              <tr>
                 <td><?php echo $v['act_id']?></td>
                 
                 <td><a href="<?php echo site_url('www').'/'.strtolower($ac_lists[0]['obj_name']).'/'.$v['act_name'] ?>"><?php echo $v['act_name']?></a> </td>
                 <td><?php echo $v['act_description']?></td>
                 <td><?php echo $v['create_time'];?></td>
                 
                 <td><?php echo $v['controller_template_id'] ?></td>
                 <td><?php echo $v['view_template_id'] ?></td>
                 <td><a href="<?php echo site_url('admin/action/edit_action').'?act_id='.$v['act_id'].'&obj_id='.$this->input->get('obj_id'); ?>">编辑</a> | <a href="<?php echo site_url('admin/action/del_action').'?act_id='.$v['act_id'].'&obj_id='.$this->input->get('obj_id'); ?>">删除</a></td>
               </tr>
              <?php endforeach   ?>
      <?php } ?> 
      </tbody>
    </table>

  </div>
</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
//Data Tables
$(document).ready(function () {
  $('#data-table').dataTable({
    "sPaginationType": "full_numbers"
  });
});
</script>
     