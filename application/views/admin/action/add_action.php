<!-- <script src="<?php echo base_url() ?>style/admin/css/bootstrap/js/jquery-ui-1.10.0.custom.min.js"></script> -->
<script>
  $(document).ready(function(){
        $("span[class=add-on]").css('cursor','pointer');
        $("span[class=add-on]").eq(0).click(function(){
         $.ajax({
           'type':'get',
           'data':{},
           'success':function(data){

              $("#dialog").html(data);    
           },
           'url':'<?php echo site_url("admin/template/t_list")."?type=controller" ?>'
         });
         $("#dialog").dialog({
            height: 500,
             width: 800,
            modal: true,

         });

     })
        $("span[class=add-on]").eq(1).click(function(){
         $.ajax({
           'type':'get',
           'data':{},
           'success':function(data){

              $("#dialog").html(data);    
           },
           'url':'<?php echo site_url("admin/template/t_list")."?type=view" ?>'
         });
         $("#dialog").dialog({
            height: 500,
             width: 800,
            modal: true,

         });

     })
  })
</script>
<div class="block">
   
    <div id="chart-container" class="block-body in collapse" style="height: auto;">
<div class="leebutton" style="margin-top:20px;">
<a class="btn btn-primary" id="yw0" href="<?php echo site_url('admin/action/action_list').'?obj_id='.$_GET['obj_id']?>"><i class="icon-undo"></i> 返回行动列表</a></div>
<form class="form-horizontal" id="department-form" action="<?php echo site_url('admin/action/add_action').'?obj_id='.$_GET['obj_id']?>" method="post">
  
  <div class="control-group "><label class="control-label required" for="Department_name">Action名称 <span class="required">*</span></label><div class="controls"><input type="text" class="span3" maxlength="128" name="act_name" id="Department_name" value="<?php echo set_value('act_name') ?>"><?php echo "<span class='required'>".form_error('act_name')."</span>" ?></div></div>

  <div class="control-group "><label class="control-label required" for="Department_author">描述 <span class="required">*</span></label><div class="controls"><input type="text" class="span3" maxlength="128" name="act_description" id="Department_author" value="<?php echo set_value('act_description') ?>"><?php echo "<span class='required'>".form_error('act_description')."</span>" ?></div></div>

  
  <!-- 自定义编辑器 -->

   <div class="control-group input-append">
     <label for="" class="control-label required">选择控制器模板</label>
     <div class="controls"><input type="text" readonly="readonly" id="controller_template_name" name="controller_template_name" value="<?php echo set_value('controller_template_name') ?>"><span class="add-on"><i class="icon-search"></i></span></div>
     <input type="hidden" name="controller_template_id" id="controller_template_id" value="<?php echo set_value('controller_template_id') ?>">
     <?php echo "<span style='color:red'>".form_error('controller_template_id')."</span>" ?>
   </div>
   <br>
   <div class="control-group input-append">
     <label for="" class="control-label required">选择视图模板</label>
     <div class="controls"><input type="text"  readonly="readonly" id="view_template_name" name="view_template_name" value="<?php echo set_value('view_template_name') ?>"><span class="add-on"><i class="icon-search"></i></span><?php echo "<span>".form_error('view_template_id')."</span>" ?></div>
     <input type="hidden" name="view_template_id" id="view_template_id" value="<?php echo set_value('view_template_id') ?>"> 
   </div>
   
    <div id="dialog">
      
    </div>
  
  
    

   <!-- <a class="btn btn-primary btn-mini" id="add">+</a> -->
    <!-- <div class="control-group "><label class="control-label" for="Department_note_content">票据内容</label><div class="controls"><input rows="4" cols="40" class="span5" name="Department[note_content]" id="Department_note_content" type="text" /></div></div>
 -->

  <div class="form-actions">
    <button class="btn btn-primary" id="yw1" type="submit">Create</button> </div>
        </div></form>
    </div>
</div>

  