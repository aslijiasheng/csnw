<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						模板组管理
						<span class="mini-title">
						Object
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<a id="newobj" class="btn btn-primary" href="<?php echo site_url('admin/templategroup/edit')?>">
<i class="icon-file"></i>
新建模板组
</a>
						</div>
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
	<div id="dt_example" class="example_alt_pagination">
		<table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
			<thead>
				<tr>
					<th width="100">操作</th>
					<th width="10%">模板编号</th>
					<th width="20%">模板名称</th>
					<th width="32%" >模板描述</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($lists as $v): ?>
				<tr>
					<td style="font-size: 16px;">
						<a href="<?php echo site_url('admin/templategroup/edit'.'/'.$v['templateGroup_id']) ?>" rel="tooltip" title="编辑"><span class="icon-menu"></span></a>
						<a href="<?php echo site_url('admin/templategroup/del'.'/'.$v['templateGroup_id']) ?>" rel="tooltip" title="删除"><span class="icon-pencil"></span></a>
					</td>
				   	<td><?php echo $v['templateGroup_id'] ?></td>
				   	<td><?php echo $v['templateGroup_name'] ?></td>
				   	<td><?php echo $v['templateGroup_desc'] ?></td>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
//Data Tables
$(document).ready(function () {
	$('#data-table').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength":50,
	});
});
</script>
