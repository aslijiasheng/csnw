<table id="<?=$type;?>" class="table table-striped table-bordered table-hover">
	<tr>
		<th width="10%">模板编号</th>
		<th width="20%">模板名称</th>
		<th width="32%" >模板路径</th>
		<th width="15%" >创建时间</th>
		<th width="15%" >更新时间</th>
	</tr>
	<?php foreach($t_g_lists as $k => $v):?>
		<tr>
			<td><?php echo $v['template_id'] ?></td>
			<td><?php echo $v['template_name'] ?></td>
			<td><?php echo $v['template_path'] ?></td>
			<td><?php echo $v['create_time'] ?></td>
			<td><?php echo $v['update_time'] ?></td>
		</tr>
	<? endforeach ?>
</table>