<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						<?php 
							if($template_type=="controller"){
								echo "控制器";
							}else if($template_type=="model"){
								echo "模型";
							}else if($template_type=="view"){
								echo "视图";
							}
						?> 模板管理
						<span class="mini-title">
						<?php echo ucfirst($template_type)." Template"?>
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div id="divmessagelist">
							<div>
<table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table2">
	<thead>
		<tr>
			<th width="100">操作</th>
			<th width="10%">模板编号</th>
			<th width="20%">模板名称</th>
			<th width="32%" >模板路径</th>
			<th width="15%" >创建时间</th>
			<th width="15%" >更新时间</th>

		</tr>
	</thead>
	<tbody>
		<?php foreach($lists as $v): ?>

		<tr>
			<td>
			   <input type="checkbox"
				<?php if(in_array($v['template_id'],$type_data)){?>
				checked=checked
				<?}?>
			   id="<?php echo $template_type;?>" name="<?php echo $template_type;?>" value="<?php echo $v['template_id'] ?>" />
			</td>
			<td><?php echo $v['template_id'] ?></td>
			<td><?php echo $v['template_name'] ?></td>
			<td><?php echo $v['template_path'] ?></td>
			<td><?php echo $v['create_time'] ?></td>
			<td><?php echo $v['update_time'] ?></td>
		</tr>
		
		<?php endforeach ?>
	</tbody>
</table>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
//Data Tables
$(document).ready(function () {
	$('#data-table2').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength":50,
	});
});
</script>