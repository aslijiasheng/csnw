<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">模板组管理
						<span class="mini-title">
						Object
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">

						</div>
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
   <form class="form-horizontal" action="<?php echo site_url('admin/templategroup/sub/'.$lists['0']['templateGroup_id']); ?>" method="POST">
      <div class="control-group">
        <label class="control-label" for="inputEmail">模板组名</label>
        <div class="controls">
          <input type="text" id="inputEmail" value="<?php echo $lists['0']['templateGroup_name'];?>" placeholder="模板组名" name="templategroup_name">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="inputPassword">模板组描述</label>
        <div class="controls">
          <input type="text" id="inputPassword" value="<?php echo $lists['0']['templateGroup_desc'];?>" name="templategroup_desc" placeholder="模板组描述">
        </div>
      </div>
	  
</div>
	<?php foreach($attribute as $key => $value): ?>
			  <fieldset>
			    <legend><?=$value;?>
						<a id="order_delivery_export" onClick="ajax_select('<?=$key;?>')" class="btn btn-primary">
							<i class="icon-upload-2"></i>
							选择
						</a>
				</legend>
				<div id="<?=$key;?>">
					
				</div>
			  </fieldset>
	  <?php endforeach ?>
	  <?php foreach($data_lists as $kk=>$vv): ?>
		<input type="text" value="<?php echo preg_replace('/[\x{4e00}-\x{9fa5}\s]+/u', '', $vv);?>" name="text_<?=$kk;?>" id="text_<?=$kk;?>" value=""/>
	  <?php endforeach ?>
      <div class="control-group">
        <div class="controls">
		
          <input type="submit" value="提交" class="btn">
        </div>
      </div>
</form>


</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id='order_delivery_export_div' style='display:none;'></div>

<script type="text/javascript">

$(document).ready(function(){
	var template_type=["view","controller","model"];
	$.each(template_type,function(i,item){
		div_ajax_select( item,"" );
	})
})

function div_ajax_select( template_type, template_ids="" )
{
	var url="<?php echo site_url('admin/templategroup/ajax_select')?>";
	if(template_ids=="" || template_ids==null)
	{
		var params={templateGroup_id:"<?=$lists['0']['templateGroup_id'];?>",template_type:template_type};
	}
	else
	{
		var params={template_ids:template_ids,template_type:template_type};
	}
	//console.debug(params);
	$.post(url,params,function(data){
		$("#"+template_type).html('');
		$("#"+template_type).html(data);
	})
}

function ajax_select( arr_type )
{

	$div_dom = $("#order_delivery_export_div");
	$type_dom = $("#text_"+arr_type);
	//$type_dom.val('');
	$url = "<?php echo site_url('admin/templategroup/view')?>";
	$params = {templatetype:arr_type};
	$.post($url,params,function(data){
		$div_dom.html('');
		$div_dom.html(data);
		//console.debug(data);
		//return false;
	})

	$("#order_delivery_export_div").dialog({
		title:"选择模板",
		modal: true,
		width:1100,
		height:500,
		buttons: [{
			text: "确认",
			Class: "btn bottom-margin btn-primary",
			click: function() {
				var text="";
				$check_box = $("input[name="+arr_type+"]");
				$($check_box).each(function(){
					if($(this).attr("checked"))
					{
						text += $(this).val() + ",";
					}
				})
				
				$type_dom.val('');
				$type_dom.val(text);
				console.debug(arr_type);
				div_ajax_select( arr_type , text );
				$(this).dialog("close");
				return false;
			}
		},{
			text: "取消",
			Class: "btn bottom-margin btn-danger ",
			click: function() {
				if(confirm("确认取消选择吗？"))
				{
					$(this).dialog("close");
					return false;
				}
			}
		}]
	});
	
	$("#order_delivery_export_div").dialog('open');
	
}
</script>