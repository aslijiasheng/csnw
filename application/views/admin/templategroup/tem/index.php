
<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						<?php echo $_GET['template_type']?>模板管理
						<span class="mini-title">
						Object
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<a id="newobj" class="btn btn-primary" href="<?php echo site_url('admin/template/add?template_type='.$_GET['template_type'])?>">
<i class="icon-file"></i>
新增模板
</a>
						</div>
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
          <div id="dialog" style="width:500px;"></div>
				
			</div>
<table class="table table-striped table-bordered table-hover">
	<tr>
		<th width="6%">操作</th>
		<th width="10%">模板编号</th>
		<th width="20%">模板名称</th>
		<th width="32%" >模板路径</th>
		<th width="15%" >创建时间</th>
		<th width="15%" >更新时间</th>
		
	</tr>
	
	<?php foreach($lists as $v): ?>

	<tr>
	    <td>
	       <a href="<?php echo site_url('admin/template/view').'?url='.$v["template_path"].'&template_type='.$_GET['template_type'].'&id='.$v['template_id'] ?>" rel="tooltip" title="查看"><span  data-icon="&#xe0c6"></span></a>
		   <a href="<?php echo site_url('admin/template/edit').'?url='.$v["template_path"].'&template_type='.$_GET['template_type'].'&id='.$v['template_id'] ?>" rel="tooltip" title="编辑"><span data-icon="&#xe005"></span></a>
		   <a href="<?php echo site_url('admin/template/del').'?url='.$v["template_path"].'&id='.$v['template_id'].'&template_type='.$_GET['template_type'] ?>" rel="tooltip" title="删除"><span  data-icon="&#xe101"></span></a>
		   <!-- <span data-icon="&#xe005" style="cursor:pointer" title="编辑"></span>
		  <span data-icon="&#xe101" style="cursor:pointer" title="删除"></span> -->
		</td>

	   	<td><?php echo $v['template_id'] ?></td>
	   	<td><?php echo $v['template_name'] ?></td>
	   	<td><?php echo $v['template_path'] ?></td>
	   	<td><?php echo $v['create_time'] ?></td>
	   	<td><?php echo $v['update_time'] ?></td>	
	</tr>
	<?php endforeach ?>
</table>
<div class="summary">第 1-2 条, 共 2 条.</div>
</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>