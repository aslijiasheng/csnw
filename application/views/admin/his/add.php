<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						订单BUG处理
						<span class="mini-title">
						Object
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部分
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div id="divmessagelist">
							<div style="overflow:auto;">
								<div class="grid-view">
									<form class="form-horizontal" id="department-form" action="<?php echo site_url('admin/history_order/his_index')?>" method="post">
										<div class="control-group">
											<label class="control-label required" for="json_data">
												填写JSON数据：
												<span class="required">*</span>
											</label>
											<div class="controls">
												<textarea type="text" name="json_data" id="json_data" value=''></textarea>
											</div>
										</div>
										

										<!-- 按钮区域 start -->
										<div class="form-actions">
											<button class="btn btn-primary" type="submit">保存</button>
										</div>
										<!-- 按钮区域 end -->
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>