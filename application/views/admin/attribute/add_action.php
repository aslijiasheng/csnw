 <link rel=stylesheet href="<?php echo base_url() ?>style/public/cpp/lib/codemirror.css">
<script src="<?php echo base_url() ?>style/public/cpp/lib/codemirror.js"></script>
<script src="<?php echo base_url() ?>style/public/cpp/mode/xml/xml.js"></script>
<script src="<?php echo base_url() ?>style/public/cpp/mode/php/php.js"></script>
<script src="<?php echo base_url() ?>style/public/cpp/mode/htmlmixed/htmlmixed.js"></script>
<script src="<?php echo base_url() ?>style/public/cpp/mode/javascript/javascript.js"></script>
<script src="<?php echo base_url() ?>style/public/cpp/mode/css/css.js"></script>
<script src="<?php echo base_url() ?>style/public/cpp/mode/clike/clike.js"></script>
<div class="block">
   
    <div id="chart-container" class="block-body in collapse" style="height: auto;">
<div class="leebutton" style="margin-top:20px;">
<a class="btn btn-primary" id="yw0" href="<?php echo site_url('admin/object/obj_activity').'?obj_id='.$_GET['obj_id']?>"><i class="icon-undo"></i> 返回列表</a></div>
<form class="form-horizontal" id="department-form" action="<?php echo site_url('admin/object/add_action').'?obj_id='.$_GET['obj_id']?>" method="post">
  
  <div class="control-group "><label class="control-label required" for="Department_name">Action名称 <span class="required">*</span></label><div class="controls"><input type="text" class="span3" maxlength="128" name="act_name" id="Department_name" value="<?php echo set_value('act_name') ?>"><?php echo "<span>".form_error('act_name')."</span>" ?></div></div>

  <div class="control-group "><label class="control-label required" for="Department_author">描述 <span class="required">*</span></label><div class="controls"><input type="text" class="span3" maxlength="128" name="act_description" id="Department_author" value="<?php echo set_value('act_description') ?>"><?php echo "<span>".form_error('act_description')."</span>" ?></div></div>

  
  <!-- 自定义编辑器 -->
  <div class="controls">
              <style>
                .CodeMirror {border: 1px solid #cccccc;}
                .CodeMirror {line-height: 1.2;}
              </style>
              <textarea id="code" name="content" value="<?php echo set_value('content') ?>"></textarea><?php echo "<span>".form_error('content')."</span>" ?>
              <script>
                var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
                lineNumbers: true,
                matchBrackets: true,
                mode: "application/x-httpd-php",
                indentUnit: 4,
                indentWithTabs: true,
                enterMode: "keep",
                tabMode: "shift"
                });
              </script>
            </div>
 
    
    

   <!-- <a class="btn btn-primary btn-mini" id="add">+</a> -->
    <!-- <div class="control-group "><label class="control-label" for="Department_note_content">票据内容</label><div class="controls"><input rows="4" cols="40" class="span5" name="Department[note_content]" id="Department_note_content" type="text" /></div></div>
 -->

  <div class="form-actions">
    <button class="btn btn-primary" id="yw1" type="submit">Create</button> </div>
        </div></form>
    </div>
</div>

  