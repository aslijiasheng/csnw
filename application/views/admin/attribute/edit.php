<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						新建属性
						<span class="mini-title">
						Add Attribute
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部分
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
							<a id="yw0" class="btn btn-primary" href="<?php echo site_url('admin/attribute/')."?obj_id=".$attr_obj_id;?>">
							<i class="icon-undo"></i>
							返回列表
							</a>
						</div>
						<div id="divmessagelist">
							<div style="overflow:auto;">
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php echo site_url('admin/attribute/edit')."?attr_id=".$attr_id;?>" method="post">
		<div class="control-group <?php if(form_error('attr_name')){echo "error";}?>">
			<label class="control-label required" for="attr_name">
				<?php echo $labels["attr_name"];?>
			</label>
			<div class="controls">
				<input type="text" name="attr[attr_name]" id="attr_name" value='<?php echo $attr_name;?>'>
				<span class="help-inline"><?php echo form_error('attr_name');?></span>
			</div>
		</div>
		<div class="control-group <?php if(form_error('attr_label')){echo "error";}?>">
			<label class="control-label required" for="attr_label">
				<?php echo $labels["attr_label"];?>
			</label>
			<div class="controls">
				<input type="text" name="attr[attr_label]" id="attr_label" value='<?php echo $attr_label;?>'>
				<span class="help-inline"><?php echo form_error('attr_label');?></span>
			</div>
		</div>
		<div class="control-group <?php if(form_error('attr_type')){echo "error";}?>">
			<label class="control-label required" for="attr_type">
				<?php echo $labels["attr_type"];?>
			</label>
			<div class="controls">
				<select name="attr[attr_type]" id="attr_type">
					<option value=''>--请选择字段类型--</option>
					<?php
					//循环出所有的属性类型
						foreach ($attrtype_list as $k=>$v){
							echo "<option value='".$v["attrtype_id"]."' ";
							if($attr_type==$v["attrtype_id"]){
								echo "selected='selected'";
							}
							echo " title='".$v['attrtype_field_type']."'>".$v["attrtype_name"]."</option>";
						}
					?>
				</select>
				<span class="help-inline"></span>
			</div>
		</div>
		
<div class="control-group <?php if(form_error('attr_is_caculated')){echo "error";}?>" >
			<label class="control-label required" for="attr_is_caculated" id="is_caculated">
				<?php echo $labels["attr_is_caculated"];?>
			</label>
			<div class="controls">
				<label class="radio inline" >
				<input  type="radio"  value="1" name="attr[attr_is_caculated]" id="attr_is_caculated" <?php if($attr_is_caculated==1){echo "checked";}?>>是
				</label>
				<label class="radio inline" >
				<input  type="radio"  value="0" name="attr[attr_is_caculated]" id="attr_is_caculated2" <?php if($attr_is_caculated==0){echo "checked";}?>>否
				</label>
			</div>
		</div>
		<div class="control-group <?php if(form_error('attr_formula')){echo "error";}?>" id="formula" <?php if($attr_is_caculated==0){echo "style='display:none;'";}?>>
			<label class="control-label required" for="attr_formula">
				<?php echo $labels["attr_formula"];?>
			</label>
			<div class="controls">
				<textarea name="attr[attr_formula]" id="attr_formula"><?php echo $attr_formula;?></textarea>
				<span class="help-inline"><?php echo form_error('attr_formula');?></span>
			</div>
		</div>
		<!-- 2014-08-15 更新计算型字段 结束 -->



		<!-- 隐藏字段属性（根据属性类型来分别显示） start -->
			<!-- 引用类型 start -->
			<div id="attrtype_quote" class="slide">
				<div class="control-group">
					<label class="control-label required" for="attr_quote_id">
						<?php echo $labels["attr_quote_id"];?>
					</label>
					<div class="controls">
						<input type="text" name="attr[attr_quote_id]" id="attr_quote_id" value='<?php echo $attr_quote_id_arr['obj_label'];?>' value_id='<?php echo $attr_quote_id_arr['obj_id'];?>' leetype="quote" url="<?php echo site_url('admin/object/ajax_list');?>" title="<?php echo $labels["attr_quote_id"];?>">
						<span class="help-inline"></span>
					</div>
				</div>
			</div>
			<!-- 引用类型 end -->
		<!-- 隐藏字段属性（根据属性类型来分别显示） end -->


		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	//初始化
	$v = $('#attr_type').val();
	slide($v,0);
	$('#attr_type').change(function(){
		$v = $(this).val();
		slide($v,200);
	});
	function slide($v,$s){
		switch($v){
			case "19":
				//alert("引用类型");
				$('.slide').slideUp($s);
				$('#attrtype_quote').slideDown($s);
			break;
			default:
				$('.slide').slideUp($s);
		}
	}
</script>
<script type="text/javascript">
$(document).ready(function(){
//$('#formula').hide();
$('#reset').hide();
	$("#attr_is_caculated").click(function(){
		
		$("#formula").show(200);

});
$("#attr_is_caculated2").click(function(){
		
		$("#formula").hide(200);

});

})

</script>

