<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						属性管理
						<span class="mini-title">
						Attribute
						</span>
					</div>
					<span class="tools">
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
							<a href="<?php echo site_url('admin/object/')?>" class="btn btn-primary"><i class="icon-undo"></i> 返回对象列表</a>
							<a href="<?php echo site_url('admin/attribute/add')."?obj_id=".$_GET["obj_id"]?>" class="btn btn-primary"><i class="icon-file"></i> 新增属性</a>
						</div>
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
	<div id="dt_example" class="example_alt_pagination">
		<table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
			<thead>
				<tr>
					<th width="60">操作</th>
					<th width="10%"><?php echo $labels['attr_id'];?></th>
					<th width="10%"><?php echo $labels['attr_obj_id'];?></th>
					<th width="10%"><?php echo $labels['attr_name'];?></th>
					<th><?php echo $labels['attr_label'];?></th>
					<th><?php echo $labels['attr_type'];?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($list as $v): ?>
				<tr>
					<td style="font-size: 16px;">
						<a href="<?php echo site_url('admin/attribute/edit?attr_id=').$v['attr_id']; ?>" title="编辑"><span class="icon-pencil"></span></a>
						<a href="<?php echo site_url('admin/attribute/del?attr_id=').$v['attr_id']; ?>" title="删除"><span class="icon-remove-2"></span></a>
				   <?php

				    if(in_array($v['attr_type'], array(14,15,16))){ ?>
                        <a href="<?php echo site_url('admin/enum/enum_list?obj_id='.$_GET['obj_id'].'&attr_id='.$v['attr_id']) ?>" title="枚举"><span class="icon-file-pdf"></span></a>
                   <?php } ?>
					</td>
					<td><?php echo $v['attr_id'];?></td>
					<td><?php echo $v['attr_obj_id_arr']['obj_label'];?></td>
					<td><?php echo $v['attr_name'];?></td>
					<td><?php echo $v['attr_label'];?></td>
					<td><?php echo $v['attr_type_arr']['attrtype_name'];?></td>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
//Data Tables
$(document).ready(function () {
	$('#data-table').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength":50,
	});
});
</script>