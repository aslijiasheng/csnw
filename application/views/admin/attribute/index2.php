<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						对象管理
						<span class="mini-title">
						Object
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<a id="newobj" class="btn btn-primary" href="<?php echo site_url('admin/object/add')?>">
<i class="icon-file"></i>
新增对象
</a>
						</div>
						<div id="divmessagelist">
							<div style="overflow:auto;">

<div id="objlist" class="grid-view">
<table class="table table-striped table-bordered table-hover">
	<tr>
		<th width="35%">操作</th>
		<th width="10%">Id</th>
		<th width="10%">父级ID</th>
		<th width="10%">名称</th>
		<th width="10%">标签名</th>
		<th width="*" >图标</th>
		<th width="10%">能否被引用</th>
		<th width="10%">是否包含明细</th>
	</tr>
	<?php foreach($list as $v) :?>
	<tr>
		<td>
			<a href="<?php echo site_url('index/clientplateform/edit_object').'?obj_id='.$v['obj_id'] ?>">编辑</a> | 
			<a href="<?php echo site_url('index/clientplateform/attribute_list').'?obj_id='.$v['obj_id'] ?>">属性设置 </a> | 
			<a href="<?php echo site_url('index/clientplateform/create_table').'?obj_id='.$v['obj_id'] ?>">建立对象表</a>| <a href="<?php echo site_url('index/clientplateform/create_model').'?obj_id='.$v['obj_id'] ?>">建立对象模型</a>| 
			<a href="<?php echo site_url('index/clientplateform/create_controller').'?obj_id='.$v['obj_id'] ?>">建立对象控制器</a>| 
			<a href="<?php echo site_url('index/clientplateform/actions').'?obj_id='.$v['obj_id'] ?>">对象行为</a>
		</td>
		<td><?php echo $v['obj_id']?></td>
		<td><?php echo $v['obj_uid']?></td>
		<td><a href="<?php echo site_url('index').'/'.strtolower($v['obj_name'])?>"><?php echo $v['obj_name']?></a> </td>
		<td><?php echo $v['obj_label']?></td>
		<td><?php echo $v['obj_icon']?></td>
		<td><?php echo $v['is_ref_obj']?></td>
		<td><?php echo $v['is_detail']?></td>
	</tr>
	<?php endforeach ?>
</table>
<div class="summary">第 1-2 条, 共 2 条.</div>
</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
