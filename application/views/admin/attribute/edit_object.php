<div class="block">
    <p class="block-heading" data-target="#chart-container" data-toggle="collapse">编辑对象</p>
    <div id="chart-container" class="block-body in collapse" style="height: auto;">
<div class="leebutton" style="margin-top:20px;">
<a class="btn btn-primary" id="yw0" href="<?php echo site_url('admin/object') ?>"><i class="icon-undo"></i> 返回列表</a></div>
<form class="form-horizontal" id="department-form" action="<?php echo site_url('admin/object/edit_object').'?obj_id='.$_GET['obj_id']?>" method="post">
  
  <div class="control-group "><label class="control-label required" for="Department_name">对象名称 <span class="required">*</span></label><div class="controls"><input type="text" class="span4" maxlength="128" name="obj_name" id="Department_name" value="<?php echo $obj_info[0]['obj_name'] ?>"></div></div>

  <div class="control-group "><label class="control-label required" for="Department_author">中文标签 <span class="required">*</span></label><div class="controls"><input type="text" class="span4" maxlength="128" name="obj_label" id="Department_author" value="<?php echo $obj_info[0]['obj_label'] ?>"></div></div>

  <div class="control-group "><label class="control-label" for="Department_parent_id">父级角色</label><div class="controls">
<select name="obj_uid" id="Department_parent_id" value="<?php echo $obj_info[0]['obj_uid'] ?>">
	<option value="0" selected="selected">--无父级--</option>
	<?php foreach($obj_list as $v) :?>
    
    <option id="ops" value="<?php  echo $v['obj_id']?>">|----<?php echo $v['obj_label']?></option> 
    <?php endforeach ?>
	 <script>
     $(function(){
        var uid="<?php echo $obj_info[0]['obj_uid'] ?>";
        $("#Department_parent_id option").each(function(index, el) {
           if($(this).val()==uid){
             $(this).attr("selected","selected");
           }else{   
           }
       });
        var is_ref_obj="<?php echo $obj_info[0]['is_ref_obj'] ?>";
        if(is_ref_obj==0){
          $("#is_ref_obj_0").attr('checked','checked');
        }else{
          $("#is_ref_obj_1").attr('checked','checked');
        }
        var is_detail="<?php echo $obj_info[0]['is_detail'] ?>";
        if(is_detail==0){
          $("#is_detail_0").attr('checked','checked');
        }else{
          $("#is_detail_1").attr('checked','checked');
        }
       })
   </script>
</select></div></div>

  <div class="control-group ">
    <label class="control-label">角色图标</label>
    <div class="controls">
  <div class="btn-group">
  <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">
    --请选择角色图标-- <span class="caret"></span>
  </button>
  <ul role="menu" class="dropdown-menu">
    <input type="hidden" name="obj_icon">
    <li><a href="#" name="obj_icon" value='icon-user'><i class="icon-user"></i></a></li>
    <li><a href="#" name="obj_icon" value='icon-heart'><i class="icon-heart"></i></a></li>
    <li><a href="#" name="obj_icon" value='icon-fire'><i class="icon-fire"></i></a></li>
    <li class="divider"></li>
    <li><a href="#" name="obj_icon" value='icon-music'><i class="icon-music"></i></a></li>
  </ul>
</div>
</div>

  <div class="control-group "><label class="control-label required" for="Department_is_u8">能否被引用 <span class="required">*</span></label><div class="controls"><input type="hidden" id="ytDepartment_is_u8" value="<?php echo $obj_info[0]['is_ref_obj'] ?>" name="is_ref_obj"><label class="radio inline"><input type="radio" id="is_ref_obj_0" value="0"  name="is_ref_obj"><label for="Department_is_u8_0">不能被引用</label></label><label class="radio inline"><input type="radio" id="is_ref_obj_1" value="1" name="is_ref_obj"><label for="Department_is_u8_1">能被引用</label></label></div></div>

  <div class="control-group "><label class="control-label required" for="Department_is_u8">是否包含明细 <span class="required">*</span></label><div class="controls"><input type="hidden" id="ytDepartment_is_u8" value="<?php echo $obj_info[0]['is_detail'] ?>" name="is_detail"><label class="radio inline"><input type="radio" id="is_detail_0" value="0" checked="checked" name="is_detail"><label for="Department_is_u8_0">不包含明细</label></label><label class="radio inline"><input type="radio" id="is_detail_1" value="1" name="is_detail"><label for="Department_is_u8_1">包含明细</label></label></div></div>
    
    

   <!-- <a class="btn btn-primary btn-mini" id="add">+</a> -->
    <!-- <div class="control-group "><label class="control-label" for="Department_note_content">票据内容</label><div class="controls"><input rows="4" cols="40" class="span5" name="Department[note_content]" id="Department_note_content" type="text" /></div></div>
 -->

  <div class="form-actions">
    <button class="btn btn-primary" id="yw1" type="submit">Edit</button> </div>
        </div></form>
    </div>
</div>