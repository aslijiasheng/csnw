<h2><?php echo $_GET['type'] ?></h2>
<div>
    <table class="table table-bordered">
        <tr>
            <th width="10%">选择</th>
            <th width="20%">类型ID</th>
            <th width="30%">类型名称</th>
            <th width="40%">类型标签</th>
        </tr>
        <?php foreach ($columns_list as $v): ?>
            <tr>
                <td>
                    <input type="radio" name="attr_name"  attr_name="<?php echo $v['attr_name'].'（'.$v['attr_label'].'）' ?>" value="<?php echo $v['attr_id'] ?>">
                </td>
                <td>
                    <?php echo $v['attr_id']; ?>
                </td>
                <td>
                    <?php echo $v['attr_name'] ?>
                </td>
                <td>
                    <?php echo $v['attr_label'] ?>
                </td>
            </tr>
        <?php endforeach ?>
    </table>
    <input type="button" value="提交" class="btn btn-default">
</div>
<script>
    $(document).ready(function() {
        $("input[type=button]").click(function() {
            $("#detail_attr").val($("input[type=radio]:checked").attr('attr_name'));
            $("#detail_attr_id").val($("input[type=radio]:checked").val());
			$("#dialog").dialog("close");
        })
    })
</script>
