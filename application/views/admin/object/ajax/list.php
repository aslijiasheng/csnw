<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						选择对象
						<span class="mini-title">
						Object
						</span>
					</div>
					<span class="tools">
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
	<div id="dt_example" class="example_alt_pagination">
		<table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
			<thead>
				<tr>
					<th width="60">操作</th>
					<th width="30%"><?php echo $labels["obj_name"];?></th>
					<th><?php echo $labels["obj_label"];?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($list as $v) :?>
				<tr>
					<td style="font-size: 16px;">
						<input type="radio" name="optionsRadios" value="<?php echo $v['obj_id'];?>" name_val="<?php echo $v['obj_label']?>" <?php if($_GET["hidden_v"]==$v['obj_id']){echo "checked='checked'";}?>>
					</td>
					<td><?php echo $v['obj_name']?></td>
					<td><?php echo $v['obj_label']?></td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
//Data Tables
$(document).ready(function () {
	$('#data-table').dataTable({
		"sPaginationType": "full_numbers"
	});
});

</script>

