<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						明细管理
						<span class="mini-title">
						Detail Manage
						</span>
					</div>
					<span class="tools">
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<a id="newobj" class="btn btn-primary" href="<?php echo empty($_GET['type_id']) ? site_url('admin/object/add_detail').'?obj_id='.$_GET['obj_id'] : site_url('admin/object/add_detail').'?obj_id='.$_GET['obj_id'].'&type_id='.$_GET['type_id'];?>">
<i class="icon-file"></i>
新增明细
</a>
						</div>
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
	<div id="dt_example" class="example_alt_pagination">
		<table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
			<thead>
				<tr>
					<th width="10">操作</th>
					<th width="30">对象</th>
					<th width="30">对应对象</th>
					<th width="30">对应对象字段</th>
      				</tr>
			</thead>
			<tbody>
			<?php foreach($detail_list as $v): ?>
				<tr>
                    <td>
                        <a href="<?php echo site_url('admin/object/edit_detail').'?detail_id='.$v['id'].'&obj_id='.$v['obj_id'].'&type_id='.$v['type_id'] ?>" rel="tooltip" title="编辑"><span class="icon-pencil"></span></a>
                        <a href="<?php echo site_url('admin/object/del_detail').'?detail_id='.$v['id'].'&obj_id='.$v['obj_id'].'&type_id='.$v['type_id'] ?>" rel="tooltip" title="删除"><span data-icon="&#xe0a8"></span></a>
                    </td>
					<td><?php echo $v['obj']['obj_name'].$v['obj']['obj_label'] ?></td>
					<td><?php echo $v['detail_obj']['obj_name'].$v['detail_obj']['obj_label'] ?></td>
                    <td><?php echo $v['detail_attr']['attr_name'].$v['detail_attr']['attr_label'] ?></td>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
//Data Tables
$(document).ready(function () {
	$('#data-table').dataTable({
		"sPaginationType": "full_numbers"
	});
});
</script>
