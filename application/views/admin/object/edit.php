<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						对象管理
						<span class="mini-title">
						Object
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部分
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
							<a id="yw0" class="btn btn-primary" href="<?php echo site_url('admin/object/')?>">
							<i class="icon-undo"></i>
							返回列表
							</a>
						</div>
						<div id="divmessagelist">
							<div style="overflow:auto;">
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php echo site_url('admin/object/edit').'?obj_id='.$obj_info['obj_id']?>" method="post">
		<div class="control-group <?php if(form_error('obj_name')){echo "error";}?>">
			<label class="control-label required" for="obj_name">
				<?php echo $labels["obj_name"];?>
				<span class="required">*</span>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="obj[obj_name]" id="obj_name" value='<?php echo $obj_info['obj_name']?>'>
				<span class="help-inline"><?php echo form_error('obj_name');?></span>
			</div>
		</div>
		<div class="control-group <?php if(form_error('obj_label')){echo "error";}?>">
			<label class="control-label required" for="obj_label">
				<?php echo $labels["obj_label"];?>
				<span class="required">*</span>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="obj[obj_label]" id="obj_label" value='<?php echo $obj_info['obj_label']?>'>
				<span class="help-inline"></span>
			</div>
		</div>
		<div class="control-group ">
			<label class="control-label required" for="obj_icon">
				<?php echo $labels["obj_icon"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
				<input id="obj_icon" type="text" name="obj[obj_icon]" maxlength="128" value="<?php echo $obj_info["obj_icon"];?>" leetype="icondialog" url="<?php echo site_url('admin/icon/ajax_list')?>" title="选择图标">
				<span class="text-info"></span>
			</div>
		</div>
		<div class="control-group ">
			<label class="control-label required">
				功能设定
				<span class="required"></span>
			</label>
			<div class="controls">
				<ul class="lee_checkbox">
					<li>
						<input id="is_obj_type" type="checkbox" value="1" name="obj[is_obj_type]" <?php echo empty($obj_info['is_obj_type']) ? '' :"checked" ?>>
						<label for="is_obj_type"><?php echo $labels["is_obj_type"];?></label>
					</li>
					<li>
						<input id="is_ref_obj" type="checkbox" value="1" name="obj[is_ref_obj]" <?php echo empty($obj_info['is_ref_obj']) ? '' : "checked" ?>>
						<label for="is_ref_obj"><?php echo $labels["is_ref_obj"];?></label>
					</li>
					<li>
						<input id="object_is_xxxx" type="checkbox" value="1" name="is_xxxx">
						<label for="object_is_xxxx">其他功能暂定</label>
					</li>
				</ul>
			</div>
		</div>
        <div class="control-group">
			<label class="control-label" for="object_is_detail">
				<?php echo $labels["is_detail"];?>
			</label>
			<div class="controls">
				<ul class="lee_checkbox">
					<li>
						<input id="object_is_detail" type="checkbox" name="obj[is_detail]" value="1" <?php echo empty($obj_info['is_detail']) ? '' : "checked" ?>>
						<label for="object_is_detail">是</label>
					</li>
				</ul>
			</div>
		</div>
		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>