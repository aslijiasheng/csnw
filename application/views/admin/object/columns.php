<h2></h2>
<div id="columns">
    <table class="table table-bordered">
    <?php foreach($list as $k=>$v): ?>
        <tr>
            <td width="50%">
                <?php echo $v['obj_name'].'（'.$v['obj_label'].'）'; ?>
            </td>
            <td>
                <ul style="display:none;">
                    <?php foreach($v['columns'] as $ck=>$cv): ?>
                    <li><?php echo $cv['Field']?></li>
                    <?php endforeach; ?>
                </ul>
            </td>
        </tr>
    <?php endforeach;?>
    </table>
    <div class="clear"></div>
    <input type="hidden" name="columns" value="11">
    <input type="button" value="提交" class="btn btn-default">
</div>
<script>
$(function(){
    $("#columns tr").each(function(){
        $(this).find("td:first").click(function(){
            $("#columns tr ul").css("display","none");
            $(this).next().children("ul").css("display","block");
        });
    })
    $("li").click(function(){
        alert($(this).html());
    })
});
</script>
