<div class="block">
    <p class="block-heading" data-target="#chart-container" data-toggle="collapse">编辑属性</p>
    <div id="chart-container" class="block-body in collapse" style="height: auto;">
<div class="leebutton" style="margin-top:20px;">
<a class="btn btn-primary" id="yw0" href="<?php echo site_url('admin/object/attribute_list').'?obj_id='.$_GET['obj_id'] ?>"><i class="icon-undo"></i> 返回列表</a></div>
<form class="form-horizontal" id="department-form" action="<?php echo site_url('admin/object/edit_attribute').'?obj_id='.$_GET['obj_id'].'&attr_id='.$_GET['attr_id'].'&old_name='.$info[0]['attr_name']?>" method="post">
  
  <div class="control-group "><label class="control-label required" for="Department_name">属性名称 <span class="required">*</span></label><div class="controls"><input type="text" class="span4" maxlength="128" name="attr_name" id="Department_name" value="<?php echo $info[0]['attr_name']?>"></div></div>

  <div class="control-group "><label class="control-label required" for="Department_author">属性中文标签 <span class="required">*</span></label><div class="controls"><input type="text" class="span4" maxlength="128" name="attr_label" id="Department_author" value="<?php echo $info[0]['attr_label']?>"></div></div>

  <div class="form-actions">
    <button class="btn btn-primary" id="yw1" type="submit">提交</button> </div>
        </div></form>
    </div>
</div>
