<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						对象管理
						<span class="mini-title">
						Object
						</span>
					</div>
					<span class="tools">
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<a id="newobj" class="btn btn-primary" href="<?php echo site_url('admin/object/add')?>">
<i class="icon-file"></i>
新增对象
</a>
						</div>
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
	<div id="dt_example" class="example_alt_pagination">
		<table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
			<thead>
				<tr>
					<th width="140">操作</th>
					<th width="10%"><?php echo $labels["obj_name"];?></th>
					<th width="10%"><?php echo $labels["obj_label"];?></th>
					<th width="10%" ><?php echo $labels["obj_icon"];?></th>
					<th width="10%"><?php echo $labels["is_obj_type"];?></th>
					<th width="10%"><?php echo $labels["is_ref_obj"];?></th>
					<th width="*">明细属性设置</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($list as $v): ?>
				<tr>
					<td style="font-size: 16px;">
						<!--
						<a class="delete-row" href="<?php echo site_url('admin/object/obj_view').'?obj_id='.$v['obj_id'] ?>" rel="tooltip" data-original-title="查看"><span class="icon-file"></span></a>
						-->
						<a href="<?php echo site_url('admin/object/edit').'?obj_id='.$v['obj_id'] ?>" rel="tooltip" title="编辑"><span class="icon-pencil"></span></a>
						<?php if ($v['is_obj_type']==1){;?>
						<a href="<?php echo site_url('admin/type/index').'?obj_id='.$v['obj_id'] ?>" rel="tooltip" title="类型设置"><span class="icon-tree"></span></a>
						<?php }?>
						<a href="<?php echo site_url('admin/attribute/').'?obj_id='.$v['obj_id'] ?>" rel="tooltip" title="属性管理"><span class="icon-menu"></span></a>
						<a href="<?php echo site_url('admin/action/action_list').'?obj_id='.$v['obj_id'] ?>" rel="tooltip" title="对象行动"><span class="icon-truck"></span></a>

						<div class="btn-group">
							<a href="<?php echo site_url('admin/object/create').'?obj_id='.$v['obj_id'] ?>" rel="tooltip" title="页面布局" data-toggle="dropdown">
								<span class="icon-file-css"></span>
							</a>
							<ul class="dropdown-menu" role="menu" class="dropdown-menu">
								<li><a tabindex="-1" href="<?php echo site_url('admin/layout/lists').'?obj_id='.$v['obj_id'].'&type_id=0' ?>">列表页面布局</a></li>
								<li><a tabindex="-1" href="<?php echo site_url('admin/layout/edit').'?obj_id='.$v['obj_id'].'&type_id=0' ?>">新增编辑页面布局</a></li>
								<li><a tabindex="-1" href="<?php echo site_url('admin/layout/view').'?obj_id='.$v['obj_id'].'&type_id=0' ?>">查看页面布局</a></li>
							</ul>
						</div>
						<div class="btn-group">
							<a href="<?php echo site_url('admin/object/create').'?obj_id='.$v['obj_id'] ?>" rel="tooltip" title="生成" data-toggle="dropdown">
								<span class="icon-file-xml"></span>
							</a>
							<ul class="dropdown-menu" role="menu" class="dropdown-menu">
								<li><a tabindex="-1" href="<?php echo site_url('admin/create/c_table?obj_id='.$v['obj_id']);?>">生成对象表</a></li>
								<li><a tabindex="-1" href="<?php echo site_url('admin/create/c_model').'?obj_id='.$v['obj_id'] ?>">生成对象模型</a></li>
								<li><a tabindex="-1" href="<?php echo site_url('admin/create/c_controller').'?obj_id='.$v['obj_id'] ?>">生成对象控制器</a></li>
								<li><a tabindex="-1" href="<?php echo site_url('admin/create/c_view').'?obj_id='.$v['obj_id'] ?>">生成对象视图</a></li>
							</ul>
						</div>
                        <?php if ($v['is_detail']==1 && $v['is_obj_type']==0): ?>
                            <a href="<?php echo site_url('admin/detail/index').'?obj_id='.$v['obj_id'] ?>" rel="tooltip" title="增加明细"><span class="icon-share"></span></a>
                        <?php endif; ?>
					</td>
					<td><a href="<?php echo site_url('www').'/'.strtolower($v['obj_name'])?>"><?php echo $v['obj_name']?></a> </td>
					<td><?php echo $v['obj_label']?></td>
					<td><span class="<?php echo $v['obj_icon']?>" title="<?php echo $v['obj_icon']?>"></span></td>
					<td><?php if ($v['is_obj_type']==1){echo "是";}else{echo "否";}?></td>
					<td><?php if ($v['is_ref_obj']==1){echo "是";}else{echo "否";}?></td>
					<td>
						<ul class="inline">
							<?php
								if(!empty($v['detail_list'])){
									foreach($v['detail_list'] as $v2){
										echo "<li><a href='".site_url('admin/attribute')."?obj_id=".$v2['detail_obj_id']."'>".$v2['obj_label']."</a></li>";
									}
								}
							?>
						</ul>
					</td>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
//Data Tables
$(document).ready(function () {
	$('#data-table').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength":50,
	});
});
</script>
