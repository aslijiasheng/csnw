<div class="block">
    <p class="block-heading" data-target="#chart-container" data-toggle="collapse">创建属性</p>
    <div id="chart-container" class="block-body in collapse" style="height: auto;">
<div class="leebutton" style="margin-top:20px;">
<a class="btn btn-primary" id="yw0" href="<?php echo site_url('admin/object/attribute_list').'?obj_id='.$_GET['obj_id'] ?>"><i class="icon-undo"></i> 返回列表</a></div>
<form class="form-horizontal" id="department-form" action="<?php echo site_url('admin/object/add_attribute').'?obj_id='.$_GET['obj_id']?>" method="post">
  
  <div class="control-group "><label class="control-label required" for="Department_name">属性名称<span class="required"> *</span></label><div class="controls"><input type="text"  maxlength="128" name="attr_name" id="Department_name" value="<?php echo set_value('attr_name') ?>"><?php echo "<span class='required'>".form_error('attr_name')."</span>" ?></div></div>

  <div class="control-group "><label class="control-label required" for="Department_author">属性中文标签 <span class="required"> *</span></label><div class="controls"><input type="text"  maxlength="128" name="attr_label" id="Department_author" value="<?php echo set_value('attr_label') ?>"><?php echo "<span class='required'>".form_error('attr_label')."</span>" ?></div></div>
  
  <div class="control-group "><label class="control-label required" for="Department_author">字段类型<span class="required"> *</span></label><div class="controls">
	  <select name="attr_field_type" id="Department_parent_id">
	    <option value='' selected="selected">--请选择字段类型--</option>
	  	<option value="1">数值</option>
	  	<option value="2">普通文本类型</option>
	  	<option value="3">引用</option>
	  	<option value="4">时间</option>  
	  	<option value="5">时间戳</option>
	  	<option value="6">单选类型</option>
	  	<option value="7">复选类型</option>
	  	<option value="8">下拉框类型</option>	  
	  	<option>
	  </select><?php echo "<span class='required'>".form_error('attr_field_type')."</span>"?>
 </div>
  
  <div class="form-actions">
    <button class="btn btn-primary" id="yw1" type="submit">提交</button> </div>
        </div></form>
    </div>
</div>

		