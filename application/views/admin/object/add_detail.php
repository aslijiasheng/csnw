<script>
    $(document).ready(function() {
        $("span[class=add-on]").css('cursor', 'pointer');
        $("span[class=add-on]").eq(0).click(function() {
            $.ajax({
                'type': 'get',
                'data': {},
                'success': function(data) {
                    $("#dialog").html(data);
                },
                'url': '<?php echo site_url("admin/object/obj_ajax_list")."?type=选择明细对象" ?>'
            });
            $("#dialog").dialog({
                height: 500,
                width: 800,
                modal: true,
            });
        })
        $("span[class=add-on]").eq(1).click(function() {
            $.ajax({
                'type': 'get',
                'data': {},
                'success': function(data) {
                    $("#dialog").html(data);
                },
                'url': '<?php echo site_url("admin/object/columns_ajax_list") . "?type=选择明细字段&detail_obj_id=" ?>'+$("#detail_obj_id").val()
            });
            $("#dialog").dialog({
                height: 500,
                width: 800,
                modal: true,
            });

        })

    })
</script>
<div class="block">
    <div class="leebutton" style="margin:20px 0px;">
        <a class="btn btn-primary" id="yw0" href="<?php echo site_url('admin/object/detail') . '?obj_id=' . $_GET['obj_id'] ?>"><i class="icon-undo"></i> 返回列表</a>
    </div>
    <form class="form-horizontal" id="department-form" action="
        <?php echo site_url('admin/object/add_detail') . '?obj_id=' . $_GET['obj_id'] ?>" method="post">

        <div class="control-group ">
            <label class="control-label required" for="Department_name">对象名称<span class="required"></span></label>
            <div class="controls">
                <input type="text"  maxlength="128" name="attr_name" id="Department_name" disabled="disabled" value="<?php echo isset($obj['obj_name']) ? $obj['obj_name']:$obj['obj']['obj_name']; ?>">
            </div>
        </div>

        <div class="control-group ">
            <label class="control-label required" for="Department_author">中文标签</label>
            <div class="controls">
                <input type="text"  maxlength="128" name="attr_label" id="Department_author" disabled="disabled" value="<?php echo isset($obj['obj_label']) ? $obj['obj_label']:$obj['obj']['obj_label']; ?>">
            </div>
        </div>

        <div class="control-group input-append">
            <label for="" class="control-label required">选择明细对象</label>
            <div class="controls"><input type="text"  readonly='readonly' id="detail_obj_name" value="<?php echo isset($obj['detail_obj']) ? $obj['detail_obj']['obj_label']:'' ?>"><span class="add-on"><i class="icon-search"></i></span></div>
            <input type="hidden" name="detail_obj_id" id="detail_obj_id" value="<?php echo isset($obj['detail_obj']) ? $obj['detail_obj']['obj_id']:'' ?>">
        </div>
        <br>
        <div class="control-group input-append">
            <label for="" class="control-label required">选择明细字段</label>
            <div class="controls"><input type="text" readonly='readonly' id="detail_attr" value="<?php echo isset($obj['detail_attr']) ? $obj['detail_attr']['attr_label']:'' ?>"><span class="add-on"><i class="icon-search"></i></span></div>
            <input type="hidden" name="detail_attr_id" id="detail_attr_id" value="<?php echo isset($obj['detail_attr']) ? $obj['detail_attr']['attr_label']:'' ?>">
        </div>
        <div id="dialog"></div>

        <input type="hidden" name="obj_id" value="<?php echo $obj['obj_id'] ?>" />
        <input type="hidden" name="type_id" value="<?php
        if (isset($_GET['type_id'])):
            echo $_GET['type_id'];
        else:
            echo '0';
        endif;
        ?>" />
        <div class="form-actions">
            <button class="btn btn-primary" id="yw1" type="submit">提交</button>
        </div>
</div>
</form>
</div>
</div>