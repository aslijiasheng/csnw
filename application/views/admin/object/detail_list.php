<h2><?php echo $_GET['type'] ?></h2>
<div>
    <table class="table table-bordered">
        <tr>
            <th width="10%">选择</th>
            <th width="20%">对象ID</th>
            <th width="30%">对象名称</th>
            <th width="40%">对象标签</th>
        </tr>
        <?php foreach ($detail_list as $v): ?>
            <tr>
                <td>
                    <input type="radio" name="obj_name"  obj_name="<?php echo $v['obj_name'].'（'.$v['obj_label'].'）' ?>" value="<?php echo $v['obj_id'] ?>">
                </td>
                <td>
                    <?php echo $v['obj_id']; ?>
                </td>
                <td>
                    <?php echo $v['obj_name'] ?>
                </td>
                <td>
                    <?php echo $v['obj_label'] ?>
                </td>
            </tr>
        <?php endforeach ?>
    </table>
    <input type="button" value="提交" class="btn btn-default">
</div>
<script>
    $(document).ready(function() {
        $("input[type=button]").click(function() {
            $("#detail_obj_name").val($("input[type=radio]:checked").attr('obj_name'));
            $("#detail_obj_id").val($("input[type=radio]:checked").val());
			$("#dialog").dialog("close");
        })
    })
</script>
