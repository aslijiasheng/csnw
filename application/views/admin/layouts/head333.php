<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js">
    </script>
	<title>客户化平台</title>
	<link href="<?php echo base_url();?>style/admin/css/bootstrap/css/bootstrap.css" rel="stylesheet"></link>
	<link href="<?php echo base_url();?>style/admin/css/jquery-ui-bootstrap.css" rel="stylesheet"></link>
	<link href="<?php echo base_url();?>style/BlueMoon/icomoon/style.css" rel="stylesheet"></link>
	<link href="<?php echo base_url();?>style/BlueMoon/css/main.css" rel="stylesheet"></link>
	<link href="<?php echo base_url();?>style/admin/css/lee.css" rel="stylesheet"></link>
	<script src="<?php echo base_url();?>style/BlueMoon/js/jquery.min.js"></script>
	<script src="<?php echo base_url();?>style/admin/js/jui/js/jquery-ui.min.js"></script>
	<script src="<?php echo base_url();?>style/admin/css/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>
    <header>
      <a href="#" class="logo">
        <img src="img/logo.png" alt="Logo" />
      </a>
      <div class="btn-group">
        <button class="btn btn-primary">
          Srinu Baswa
        </button>
        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">
          <span class="caret">
          </span>
        </button>
        <ul class="dropdown-menu pull-right">
          <li>
            <a href="#">
              Edit Profile
            </a>
          </li>
          <li>
            <a href="#">
              Account Settings
            </a>
          </li>
          <li>
            <a href="#">
              Logout
            </a>
          </li>
        </ul>
      </div>
      <ul class="mini-nav">
        <li>
          <a href="#">
            <div class="fs1" aria-hidden="true" data-icon="&#xe040;"></div>
            <span class="info-label badge badge-warning">
              3
            </span>
          </a>
        </li>
        <li>
          <a href="#">
            <div class="fs1" aria-hidden="true" data-icon="&#xe04c;"></div>
            <span class="info-label badge badge-info">
              5
            </span>
          </a>
        </li>
        <li>
          <a href="#">
            <div class="fs1" aria-hidden="true" data-icon="&#xe037;"></div>
            <span class="info-label badge badge-success">
              9
            </span>
          </a>
        </li>
      </ul>
    </header>


	

    <div class="container-fluid">
      <div class="dashboard-container">
        <div class="top-nav">
			<ul>
            <li>
              <a data-original-title="" name='taglink'  href="<?php echo site_url('admin/home/index') ?>" class="">
                <div class="fs1" aria-hidden="true" data-icon="&#xe000"></div>
				首页
              </a>
            </li>
            <li>
              <a data-original-title="" name='taglink'  href="<?php echo site_url('admin/object/index')?>" <?php if ($this->menu1=='cus'){echo 'class="selected"';}?>>
                <div class="fs1" aria-hidden="true" data-icon="&#xe0bb"></div>
                客户化平台
              </a>
            </li>
            <li>
              <a data-original-title="" name='taglink' href="<?php echo site_url('admin/template')?>" <?php if ($this->menu1=='template'){echo 'class="selected"';}?>>
                <div class="fs1" aria-hidden="true" data-icon="&#xe003"></div>
                模板管理
              </a>
            </li>
            <li>
              <a data-original-title="" name='taglink' href="#">
                <div class="fs1" aria-hidden="true" data-icon="&#xe092"></div>
                权限管理
              </a>
            </li>
          </ul>
          <div class="clearfix">
          </div>
        </div>
        <div class="sub-nav">
		  <?php if ($this->menu1=='cus'){ ?>
          <ul>
            <li>
              <a data-original-title="" href="<?php echo site_url('admin/object/index')?>" <?php if ($this->menu2=='object'){echo 'class="selected"';}?>>对象管理</a>
            </li>
            <li>
              <a data-original-title="" href="<?php echo site_url('admin/attrtype/type_list') ?>">
                属性类型管理
              </a>
            </li>
          </ul>
          <?php }else if ($this->menu1=='template'){ ?>
			<ul>
				<li>
					<a data-original-title="" href="<?php echo site_url('admin/template/index?template_type=view')?>" <?php if ($this->menu2=='view'){echo 'class="selected"';}?>>
					视图 模板
					</a>
				</li>
				<li>
					<a data-original-title="" href="<?php echo site_url('admin/template/index?template_type=controller')?>" <?php if ($this->menu2=='controller'){echo 'class="selected"';}?>>控制器 模板</a>
				</li>
				<li>
					<a data-original-title="" href="<?php echo site_url('admin/template/index?template_type=model')?>" <?php if ($this->menu2=='model'){echo 'class="selected"';}?>>
					模型 模板
					</a>
				</li>
			</ul>
          <?php } ?>
        </div>
        <div class="dashboard-wrapper">