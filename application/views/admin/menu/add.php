<div class="all-sidebar">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        菜单管理
                        <span class="mini-title">
                            Menu Manager
                        </span>
                    </div>
                    <span class="tools">
                        <!-- 窗口按钮部分
                        <a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
                        -->
                    </span>
                </div>
                <div class="widget-body">
                    <div class="row-fluid">
                        <div class="leebutton">
                            <a id="yw0" class="btn btn-primary" href="<?php echo site_url('admin/menu/') ?>">
                                <i class="icon-undo"></i>
                                返回列表
                            </a>
                        </div>
                        <div id="divmessagelist">
                            <div style="overflow:auto;">
                                <div class="grid-view">
                                    <form class="form-horizontal" id="department-form" action="<?php echo site_url('admin/menu/add') ?>" method="post">
                                        <div class="control-group <?php
                                        if (form_error('menu_name')) {
                                            echo "error";
                                        }
                                        ?>">
                                            <label class="control-label required" for="menu_name">
                                                <?php echo $labels["menu_name"]; ?>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="controls">
                                                <input type="text" class="span3" maxlength="128" name="menu[menu_name]" id="menu_name" value='<?php echo set_value('menu_name') ?>'>
                                                <span class="help-inline"><?php echo form_error('menu_name'); ?></span>
                                            </div>
                                        </div>
                                        <div class="control-group <?php
                                        if (form_error('menu_url')) {
                                            echo "error";
                                        }
                                        ?>">
                                            <label class="control-label required" for="menu_url">
                                                <?php echo $labels["menu_url"]; ?>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="controls">
                                                <input type="text" class="span3" maxlength="128" name="menu[menu_url]" id="menu_url" value='<?php echo set_value('menu_url') ?>'>
                                                <span class="help-inline"><?php echo form_error('menu_url'); ?></span>
                                            </div>
                                        </div>
                                        <div class="control-group <?php
                                        if (form_error('menu_label')) {
                                            echo "error";
                                        }
                                        ?>">
                                            <label class="control-label required" for="menu_label">
                                                <?php echo $labels["menu_label"]; ?>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="controls">
                                                <input type="text" class="span3" maxlength="128" name="menu[menu_label]" id="menu_label" value='<?php echo set_value('menu_label') ?>'>
                                                <span class="help-inline"><?php echo form_error('menu_label'); ?></span>
                                            </div>
                                        </div>
                                        <div class="control-group ">
                                            <label class="control-label required" for="menu_icon">
                                                <?php echo $labels["menu_icon"]; ?>
                                                <span class="required"></span>
                                            </label>
                                            <div class="controls">
                                                <input id="obj_icon" class="span8" type="text" name="menu[menu_icon]" maxlength="128" leetype="icondialog" url="<?php echo site_url('admin/icon/ajax_list') ?>" title="选择图标">
                                                <span class="text-info">使用的时候要带上&#x</span>
                                            </div>
                                        </div>
                                        <div class="control-group ">
                                            <label class="control-label required">
                                                功能设定
                                                <span class="required"></span>
                                            </label>
                                            <div class="controls">
                                                <ul class="lee_checkbox">
                                                    <li>
                                                        <input id="menu_is_js" type="checkbox" value="1" name="menu[menu_is_js]">
                                                        <label for="menu_is_js"><?php echo $labels["menu_is_js"]; ?></label>
                                                    </li>
                                                </ul>
                                                <div class="control-group" id="menu_js_content" style="display:none;">
                                                    <textarea name="menu[menu_js_content]" style="width:500px;" rows="5"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="menu[menu_uid]" value='<?php echo empty($_GET['menu_id']) ? 0 : $_GET['menu_id'] ?>' />
                                        <input type="hidden" name="menu[menu_order]" value='1' />
                                        <!-- 按钮区域 start -->
                                        <div class="form-actions">
                                            <button class="btn btn-primary" type="submit">保存</button>
                                        </div>
                                        <!-- 按钮区域 end -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('#menu_is_js').removeAttr("checked");
        $('#menu_js_content textarea').val('');
        $('#menu_is_js').click(function() {
            if ($(this).attr('checked')) {
                $('#menu_js_content').slideDown(400);
            } else {
                $('#menu_js_content').slideUp(400);
            }
        });
    });
</script>