<style>
#sortable1, #sortable2 { list-style-type: none; margin: 0; padding: 0 0 2.5em; float: left; margin-right: 10px; }
#sortable1 li, #sortable2 li { margin: 0 5px 5px 5px; padding: 5px; font-size: 1.2em; width: 160px; 
}
</style>
<script>
$(function() {

   $( "#sortable1" ).sortable({
		connectWith: "#sortable2",
		placeholder: "ui-state-highlight",
		dropOnEmpty:true,
		forceHelperSize: true,
		cancel:".stop"
	});
	$( "#sortable2" ).sortable({
		connectWith: "#sortable1",
		placeholder: "ui-state-highlight",
		dropOnEmpty:true,
		cancel:".stop"
	});
	$( "#sortable1,#sortable2" ).disableSelection();
	$(".ui-state-default").dblclick(function(){
		var $ul_id = $(this).closest("ul").attr("id");
		$(this).hide( "slow", function() {
			if($ul_id=="sortable2"){
				$(this).appendTo("#sortable1").show( "slow" );
			}
			if($ul_id=="sortable1"){
				$(this).appendTo("#sortable2").show( "slow" );
			}
		});
	});

   

   $( "#sortable1, #sortable2" ).sortable({
      connectWith: ".connectedSortable"
    }).disableSelection();
   $("#sure").click(function(){

   		//  var data=[];
   		//  data[0]=[];
	    //  data[0]['a']="username";
	    //  data[0]['b'] = "password";
	    // // data[2] = "repassword";
	    //  alert(data[0]['a']);
	   
	    //alert(data);
        var li=$("#sortable2 li[class=ui-state-default]");
         if(li.length==0){
        	alert('请选择属性');
        }else{
        	 var list_info=[];
        for(var i=0;i<li.length;i++){
        	list_info[i]={};
          list_info[i].attr_id=li.eq(i).attr('attr_id');
          list_info[i].attr_name=li.eq(i).html();
        }
        
        $.ajax({
		'type':'post',

		'data':{
			'obj_id':"<?php echo $_GET['obj_id'] ?>",
			'type_id':"<?php echo $_GET['type_id'] ?>",
			'list_info':JSON.stringify(list_info),
			'page_type':"<?php echo $this->uri->segment(3) ?>"
		},
		'success':function(data){
			if(data.substr(0,1)=='1'){
				alert('添加成功');
				window.location.href='<?php echo site_url("admin/type/index")."?obj_id=".$_GET["obj_id"] ?>';
			}else{
				alert('表中已存在属性信息，此次修改成功');
				window.location.href='<?php echo site_url("admin/type/index")."?obj_id=".$_GET["obj_id"] ?>';
			}
			
		},
		'url':"<?php echo site_url('admin/layout/doview') ?>",
		'cache':false
	  });
     }   
       

   })
});
</script>


<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						列表页面布局
						<span class="mini-title">
						List Layout
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部分
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
							<a id="yw0" class="btn btn-primary" href="<?php echo site_url('admin/type/index').'?obj_id='.$_GET['obj_id']?>">
							<i class="icon-undo"></i>
							返回列表
							</a>
						</div>
						<div id="divmessagelist">
							<div style="overflow:auto;">

	<div class="span3 well">
		<h5 class="heading">不显示属性</h5>
		<div class="articles">
			<ul id="sortable1" class="connectedSortable">
			   <?php foreach ($obj_attr as $k => $v): ?>
			   
			   		<li class="ui-state-default" title="<?php echo $v['attr_label'] ?>" attr_id="<?php echo $v['attr_id'] ?>"><?php echo $v['attr_name'] ?></li>

			   <?php endforeach ?>
				
			</ul>
		</div>
	</div>

  
	<div class="span3 well">
	  
		    <h5 class="heading">显示属性</h5>
		   <ul id="sortable2" class="connectedSortable">
		   <?php if(isset($choosed_attr)){ ?>
		    <?php foreach ($choosed_attr as $k => $v): ?>
			   
			   		<li class="ui-state-default"  attr_id="<?php echo $v['attr_id'] ?>"><?php echo $v['attr_name'] ?></li>

			   <?php endforeach ?>
			   <?php } ?>
		   </ul>
		  
		  
		  
	</div>
	
	
</div>
 <div class="control-group">
    <input type="button" id="sure" value="确定" class="btn btn-primary">
</div>  

<!--内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url();?>style/admin/js/lee/dialog.js"></script>

<script src="<?php echo base_url();?>style/admin/js/tiny-scrollbar.js"></script>