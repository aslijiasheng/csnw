<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						枚举操作
						<span class="mini-title">
						Enum
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部分
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
							<a id="yw0" class="btn btn-primary" href="<?php echo site_url('admin/attribute').'?obj_id='.$_GET['obj_id'].'&attr_id='.$_GET['attr_id']?>">
							<i class="icon-undo"></i>
							返回属性列表
							</a>
						</div>
						<div id="divmessagelist">
							<div style="overflow:auto;">
    
    	<div class="span3 well">
		   <h5 class="heading">待添加的枚举</h5>
		   <div class="articles">
		   <form action="<?php echo site_url('admin/enum/add?attr_id='.$_GET['attr_id']) ?>" method="post" id>
		   	  <textarea name="enum_lists" id="" cols="10" rows="15"></textarea>
		   </form>
		   
		   </div>
	    </div>
    
	

  
	<div class="span3 well">
	   
		    <h5 class="heading">已添加的枚举</h5>
		   <ul id="sortable2" class="connectedSortable">
		   </ul>
	</div>
	
	
</div>
 <div class="control-group">
    <input type="button" id="sure" value="确定" class="btn btn-primary">
</div>  

<!--内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$("#sure").click(function(){
			$("form").submit();
		})
	})
</script>
<script src="<?php echo base_url();?>style/admin/js/lee/dialog.js"></script>

<script src="<?php echo base_url();?>style/admin/js/tiny-scrollbar.js"></script>