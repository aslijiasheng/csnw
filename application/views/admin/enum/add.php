<div class="all-sidebar">
  <div class="row-fluid">
    <div class="span12">
      <div class="widget">
        <div class="widget-header">
          <div class="title">
            添加枚举
            <span class="mini-title">
            Enum
            </span>
          </div>
          <span class="tools">
            <!-- 窗口按钮部分
            <a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
            -->
          </span>
        </div>
        <div class="widget-body">
          <div class="row-fluid">
            <div class="leebutton">
              <a id="yw0" class="btn btn-primary" href="<?php echo site_url('admin/enum/enum_list?obj_id='.$_GET['obj_id'].'&attr_id='.$_GET['attr_id'])?>">
              <i class="icon-undo"></i>
              返回
              </a>
            </div>
            <div id="divmessagelist">
              <div style="overflow:auto;">
<div class="grid-view">
  <form class="form-horizontal" action="<?php echo site_url('admin/enum/add?obj_id='.$_GET['obj_id'].'&attr_id='.$_GET['attr_id']) ?>" method="post">
  <div class="control-group">
    <label for="enum_name" class="control-label">枚举名称<span class="required">*</span></label>
    <div class="controls">
      <textarea name="enum_lists" id="" cols="10" rows="15"></textarea>
    </div>
  </div>
   
  <div class="control-group">
    <div class="controls">
     <input type="submit" value="提交" class="btn">
    </div>
  </div>
</form>
 <div id="dialog">
</div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
