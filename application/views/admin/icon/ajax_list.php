<?php
//echo "<pre>";print_r($list);echo "</pre>";exit;
?>
<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						图标列表
						<span class="mini-title">Icomoon Icons</span>
					</div>
					<span class="tools">
						<a class="fs1" data-icon="&#xe090" aria-hidden="true"></a>
					</span>
				</div>
				<div class="widget-body">
					<ul class="icomoon-icons-container">
						<?php
						foreach($list as $v){
							echo '<li><span class="icon_span" data-icon="&#x'.$v["icon_code"].'" data-val="'.$v["icon_name"].'"></span></li>';
						}
						?>
						<?php
						/*
						for($i=0;$i<=476;$i++){
							$i16 = dechex($i); //转换成16进制
							//echo strlen($i16);echo "<br>";
							//$i3w = sprintf("%03d",$i16); //转换成3位数前面补0 这个对于16进制不支持
							if (strlen($i16)==1){
								$i3w = "00".$i16;
							}elseif (strlen($i16)==2){
								$i3w = "0".$i16;
							}elseif (strlen($i16)==3){
								$i3w = $i16;
							}else{
								$i3w = substr($i16,0,3); //超过3位截取成3位
							}
							echo '<li><span class="icon_span" data-icon="&#xe'.$i3w.'" data-val="e'.$i3w.'"></span></li>';
						}
						*/
						?>
					</ul>
					<div class="clearfix">   </div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.icon_span').dblclick(function(){
		seldata = $(this).attr('data-icon');
		selval = $(this).attr('data-val');
		$("#<?php echo $_GET["input_id"];?>").val(selval);
		$("#<?php echo $_GET["span_id"];?>").find("span").attr('class',selval);
		$("#<?php echo $_GET["div_id"];?>").dialog('close');
	});
</script>