<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>非法操作</title>
<style type="text/css">

::selection{ background-color: #E13300; color: white; }
::moz-selection{ background-color: #E13300; color: white; }
::webkit-selection{ background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: transparent;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
	padding: 14px 15px 10px 15px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	border: 1px solid #D0D0D0;
	-webkit-box-shadow: 0 0 8px #D0D0D0;
	
}

p {
	margin: 12px 15px 12px 15px;
}
#container div{
	margin-left: 10px;
	font-size: 12px;
}
#container span{
	color:#0C9BBC;
	font-size: 13px;
}
</style>
<script src="<?php echo base_url();?>style/admin/js/jquery.js"></script>
</head>
<body>
	<div id="container">
		<h1>非法操作</h1>
			<div>您的操作不合法, <span></span>秒后将跳回上页</div>
	</div>
</body>
</html>
<script>
	$(document).ready(function(){
		var t = 3;
		setInterval(function time(){
			if(t>0){
				$("span").html(t);
				t--;
			}else{
				window.history.back();
			}
			
		},1000);
	})
</script>