<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="15%"><?php echo $labels['api_log_addtime']; ?></th>
			<th width="15%"><?php echo $labels['api_log_method']; ?></th>
			<th width="4%"><?php echo $labels['api_log_status']; ?></th>
			<th width="11%"><?php echo $labels['api_log_order_number']; ?></th>
			<th width="45%"><?php echo $labels['api_log_data']; ?></th>
			<th width="15%"><?php echo $labels['api_log_result']; ?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($listData as $listData_value) { ?>
			<tr>
				<td>
					<?php echo $listData_value['api_log_addtime']; ?>
				</td>
				<td>
					<?php echo $listData_value['api_log_method']; ?>
				</td>
				<td>
					<?php echo $listData_value['api_log_status_arr']['enum_name']; ?>
				</td>
				<td>
					<?php echo $listData_value['api_log_order_number']; ?>
				</td>
				<td>
					<pre><?php echo $listData_value['api_log_data']; ?></pre>
				</td>
				<td>
					<?php echo $listData_value['api_log_result']; ?>
				</td>
			</tr>
		<?php } ?>
	</tbody>

</table>
<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber; ?>条</div>
<input type='hidden' id='totalNumber' value="<?php echo $totalNumber; ?>">
</div>

