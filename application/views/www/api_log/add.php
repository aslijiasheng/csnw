<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						api日志新建页面
						<span class="mini-title">
						api_log
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/api_log/')?>">
<i class="icon-undo"></i>
返回api日志列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php if(!isset($_GET['type_id'])){echo site_url('www/api_log/add');}else{echo site_url('www/api_log/add?type_id='.$_GET['type_id']);}?>" method="post">
		 	<?php if(0==1){?>
		 	<?php if(!isset($_GET['type_id'])){ ?>
		 	  
						<div class="control-group">
							<label class="control-label required" for="api_log_order_number">
								<?php echo $labels["api_log_order_number"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="api_log[api_log_order_number]" value="<?php echo $id_aData['api_log_order_number'] ?>" id="api_log_order_number">
<?php }else{ ?>
<input type="text" maxlength="128" name="api_log[api_log_order_number]" id="api_log_order_number">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_method">
								<?php echo $labels["api_log_method"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="api_log[api_log_method]" value="<?php echo $id_aData['api_log_method'] ?>" id="api_log_method">
<?php }else{ ?>
<input type="text" maxlength="128" name="api_log[api_log_method]" id="api_log_method">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_addtime">
								<?php echo $labels["api_log_addtime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="api_log_addtime">
	<input size="16" type="text" name="api_log[api_log_addtime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['api_log_addtime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['api_log_addtime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#api_log_addtime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_data">
								<?php echo $labels["api_log_data"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="api_log[api_log_data]" value="<?php echo $id_aData['api_log_data'] ?>" id="api_log_data">
<?php }else{ ?>
<input type="text" maxlength="128" name="api_log[api_log_data]" id="api_log_data">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_status">
								<?php echo $labels["api_log_status"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($api_log_status_enum as $status_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $status_v['enum_key']==$id_aData['api_log_status_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $status_v['enum_key']?>" name="api_log[api_log_status]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $status_v['enum_key']?>" name="api_log[api_log_status]">
<?php }?>
<?php echo $status_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_result">
								<?php echo $labels["api_log_result"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="api_log[api_log_result]" value="<?php echo $id_aData['api_log_result'] ?>" id="api_log_result">
<?php }else{ ?>
<input type="text" maxlength="128" name="api_log[api_log_result]" id="api_log_result">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
		 	<?php }else{ ?>
		    <?php switch ($_GET["type_id"]){ 
							case -1:
							break;
						?>
						
						<?php default: ?>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_order_number">
								<?php echo $labels["api_log_order_number"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="api_log[api_log_order_number]" value="<?php echo $id_aData['api_log_order_number'] ?>" id="api_log_order_number">
<?php }else{ ?>
<input type="text" maxlength="128" name="api_log[api_log_order_number]" id="api_log_order_number">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_method">
								<?php echo $labels["api_log_method"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="api_log[api_log_method]" value="<?php echo $id_aData['api_log_method'] ?>" id="api_log_method">
<?php }else{ ?>
<input type="text" maxlength="128" name="api_log[api_log_method]" id="api_log_method">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_addtime">
								<?php echo $labels["api_log_addtime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="api_log_addtime">
	<input size="16" type="text" name="api_log[api_log_addtime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['api_log_addtime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['api_log_addtime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#api_log_addtime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_data">
								<?php echo $labels["api_log_data"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="api_log[api_log_data]" value="<?php echo $id_aData['api_log_data'] ?>" id="api_log_data">
<?php }else{ ?>
<input type="text" maxlength="128" name="api_log[api_log_data]" id="api_log_data">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_status">
								<?php echo $labels["api_log_status"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($api_log_status_enum as $status_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $status_v['enum_key']==$id_aData['api_log_status_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $status_v['enum_key']?>" name="api_log[api_log_status]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $status_v['enum_key']?>" name="api_log[api_log_status]">
<?php }?>
<?php echo $status_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_result">
								<?php echo $labels["api_log_result"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="api_log[api_log_result]" value="<?php echo $id_aData['api_log_result'] ?>" id="api_log_result">
<?php }else{ ?>
<input type="text" maxlength="128" name="api_log[api_log_result]" id="api_log_result">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
					<?php }?>
                <?php }}else{ ?>
                  
						<div class="control-group">
							<label class="control-label required" for="api_log_order_number">
								<?php echo $labels["api_log_order_number"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="api_log[api_log_order_number]" value="<?php echo $id_aData['api_log_order_number'] ?>" id="api_log_order_number">
<?php }else{ ?>
<input type="text" maxlength="128" name="api_log[api_log_order_number]" id="api_log_order_number">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_method">
								<?php echo $labels["api_log_method"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="api_log[api_log_method]" value="<?php echo $id_aData['api_log_method'] ?>" id="api_log_method">
<?php }else{ ?>
<input type="text" maxlength="128" name="api_log[api_log_method]" id="api_log_method">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_addtime">
								<?php echo $labels["api_log_addtime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="api_log_addtime">
	<input size="16" type="text" name="api_log[api_log_addtime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['api_log_addtime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['api_log_addtime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#api_log_addtime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_data">
								<?php echo $labels["api_log_data"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="api_log[api_log_data]" value="<?php echo $id_aData['api_log_data'] ?>" id="api_log_data">
<?php }else{ ?>
<input type="text" maxlength="128" name="api_log[api_log_data]" id="api_log_data">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_status">
								<?php echo $labels["api_log_status"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($api_log_status_enum as $status_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $status_v['enum_key']==$id_aData['api_log_status_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $status_v['enum_key']?>" name="api_log[api_log_status]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $status_v['enum_key']?>" name="api_log[api_log_status]">
<?php }?>
<?php echo $status_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="api_log_result">
								<?php echo $labels["api_log_result"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="api_log[api_log_result]" value="<?php echo $id_aData['api_log_result'] ?>" id="api_log_result">
<?php }else{ ?>
<input type="text" maxlength="128" name="api_log[api_log_result]" id="api_log_result">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
                 <?php }?>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>