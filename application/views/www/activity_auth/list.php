<style>
	/*#sale_other ul{
	  position: relative;
	}*/
	td ul li{
		float: left;
		margin-left: 15px;

	}
	ul label{
		display:block;
	}
</style>
<div class="all-sidebar">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        功能权限选择
                        <span class="mini-title">
                            Menu Manager
                        </span>
                    </div>
                    <span class="tools">
                    </span>
                </div>
                <div class="widget-body">
                    <div class="row-fluid">
						<div class="leebutton">
							<!--lee 按钮部分 start-->
							<a href="<?php echo site_url('www/role') ?>" class="btn btn-primary" id="yw0">
								<i class="icon-undo"></i>
								返回角色列表
							</a>
							<!--lee 按钮部分 end-->
                        </div>
                        <div id="divmessagelist">
                            <div>
                                <div id="objlist" class="grid-view">
                                    <div id="dt_example" class="example_alt_pagination">
										<form action="<?php echo site_url('www/activity_auth/add?role_id=' . $_GET['role_id']) ?>" method="post" id="ff" name="a_form">
											<table class="table table-striped table-bordered table-hover">
												<tbody><tr>
														<th width="200px">对象</th>
														<th width="30px">列表</th>
														<th width="30px">访问</th>
														<th width="30px">新建</th>
														<th width="30px">编辑</th>
														<th width="30px">删除</th>
														<th>其他</th>
													</tr>
													<tr>
														<td>客户</td>
														<td><input type="checkbox" name="account_list">
														</td>
														<td><input type="checkbox" name="account_view">
														</td>
														<td><input type="checkbox" name="account_add">
														</td>
														<td><input type="checkbox" name="account_edit">
														</td>
														<td><input type="checkbox" name="account_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="account_update">
																		客户编辑
																	</label>
																</li>
															</ul>
														</td>

													</tr>
													<tr>
														<td>&nbsp;&nbsp;&nbsp;&nbsp;个人客户</td>
														<td><input type="checkbox" name="account_type_2_list">
														</td>
														<td><input type="checkbox" name="account_type_1_view">
														</td>
														<td><input type="checkbox" name="account_type_1_add">
														</td>
														<td><input type="checkbox" name="account_type_1_edit">
														</td>
														<td><input type="checkbox" name="account_type_1_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>&nbsp;&nbsp;&nbsp;&nbsp;企业客户</td>
														<td><input type="checkbox" name="account_type_2_list">
														</td>
														<td><input type="checkbox" name="account_type_2_view">
														</td>
														<td><input type="checkbox" name="account_type_2_add">
														</td>
														<td><input type="checkbox" name="account_type_2_edit">
														</td>
														<td><input type="checkbox" name="account_type_2_del">
														</td>
														<td></td>
													</tr>

													<tr>
														<td>部门</td>
														<td><input type="checkbox" name="department_list">
														</td>
														<td><input type="checkbox" name="department_view">
														</td>
														<td><input type="checkbox" name="department_add">
														</td>
														<td><input type="checkbox" name="department_edit">
														</td>
														<td><input type="checkbox" name="department_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>人员</td>
														<td><input type="checkbox" name="user_list">
														</td>
														<td><input type="checkbox" name="user_view">
														</td>
														<td><input type="checkbox" name="user_add">
														</td>
														<td><input type="checkbox" name="user_edit">
														</td>
														<td><input type="checkbox" name="user_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>订单</td>
														<td><input type="checkbox" name="order_list">
														</td>
														<td><input type="checkbox" name="order_view">
														</td>
														<td><input type="checkbox" name="order_add">
														</td>
														<td><input type="checkbox" name="order_edit">
														</td>
														<td><input type="checkbox" name="order_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="order_cancel">
																		订单作废
																	</label>
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>&nbsp;&nbsp;&nbsp;&nbsp;销售订单</td>
														<td><input type="checkbox" name="Sale_list">
														</td>
														<td><input type="checkbox" name="Sale_view">
														</td>
														<td><input type="checkbox" name="Sale_add">
														</td>
														<td><input type="checkbox" name="Sale_edit">
														</td>
														<td><input type="checkbox" name="Sale_del">
														</td>
														<td id="sale_other">
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Sale_add_salespay">
																		新增入款项
																	</label>
																</li>

																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Sale_apply_invoice_1">
																		申请开票
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Sale_apply_invoice_2">
																		申请预开票
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Sale_apply_invoice_3">
																		财务人员自主开票
																	</label>
																</li>

																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Sale_add_rebate">
																		申请返点
																	</label>
																</li>

																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Sale_add_refund">
																		申请退款
																	</label>
																</li>

																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Sale_deal_invoice">
																		处理票据
																	</label>
																</li>

																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Sale_deal_rebate">
																		处理返点
																	</label>
																</li>

																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Sale_deal_salespay">
																		处理入款项
																	</label>
																</li>

																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Sale_deal_refund">
																		处理退款
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="cancel_check">
																		审核作废
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="cancel_affirm">
																		确认作废
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="sale_rzdr">
																		认账导入
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="sale_plkp">
																		批量开票
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="sale_gjss">
																		高级搜索
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="sale_pldc">
																		批量导出
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="order_shift">
																		订单转移
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="order_adjunct">
																		上传附件
																	</label>
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>&nbsp;&nbsp;&nbsp;&nbsp;线上订单</td>
														<td><input type="checkbox" name="OnLine_list">
														</td>
														<td><input type="checkbox" name="OnLine_view">
														</td>
														<td><input type="checkbox" name="OnLine_add">
														</td>
														<td><input type="checkbox" name="OnLine_edit">
														</td>
														<td><input type="checkbox" name="OnLine_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="online_dr">
																		订单导入
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="online_pldz">
																		批量对账
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="online_gjss">
																		高级搜索
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="online_pldc">
																		批量导出
																	</label>
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>&nbsp;&nbsp;&nbsp;&nbsp;返款订单</td>
														<td><input type="checkbox" name="Rebates_list">
														</td>
														<td><input type="checkbox" name="Rebates_view">
														</td>
														<td><input type="checkbox" name="Rebates_add">
														</td>
														<td><input type="checkbox" name="Rebates_edit">
														</td>
														<td><input type="checkbox" name="Rebates_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Rebates_gjss">
																		高级搜索
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Rebates_pldc">
																		批量导出
																	</label>
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>&nbsp;&nbsp;&nbsp;&nbsp;内部划款订单</td>
														<td><input type="checkbox" name="Penny_list">
														</td>
														<td><input type="checkbox" name="Penny_view">
														</td>
														<td><input type="checkbox" name="Penny_add">
														</td>
														<td><input type="checkbox" name="Penny_edit">
														</td>
														<td><input type="checkbox" name="Penny_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Penny_gjss">
																		高级搜索
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Penny_pldc">
																		批量导出
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Penny_check">
																		审核内划订单
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Penny_edit">
																		编辑内划订单
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Penny_del">
																		删除内划订单
																	</label>
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>&nbsp;&nbsp;&nbsp;&nbsp;预收款订单</td>
														<td><input type="checkbox" name="Deposit_list">
														</td>
														<td><input type="checkbox" name="Deposit_view">
														</td>
														<td><input type="checkbox" name="Deposit_add">
														</td>
														<td><input type="checkbox" name="Deposit_edit">
														</td>
														<td><input type="checkbox" name="Deposit_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Deposit_apply_invoice">
																		申请开票
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Deposit_gjss">
																		高级搜索
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Deposit_pldc">
																		批量导出
																	</label>
																</li>
															</ul>
														</td>
													</tr>

													<tr>
														<td>&nbsp;&nbsp;&nbsp;&nbsp;提货销售订单</td>
														<td><input type="checkbox" name="Delivery_list">
														</td>
														<td><input type="checkbox" name="Delivery_view">
														</td>
														<td><input type="checkbox" name="Delivery_add">
														</td>
														<td><input type="checkbox" name="Delivery_edit">
														</td>
														<td><input type="checkbox" name="Delivery_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Delivery_gjss">
																		高级搜索
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="Delivery_pldc">
																		批量导出
																	</label>
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>订单产品明细</td>
														<td><input type="checkbox" name="order_d_list">
														</td>
														<td><input type="checkbox" name="order_d_view">
														</td>
														<td><input type="checkbox" name="order_d_add">
														</td>
														<td><input type="checkbox" name="order_d_edit">
														</td>
														<td><input type="checkbox" name="order_d_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>角色</td>
														<td><input type="checkbox" name="role_list">
														</td>
														<td><input type="checkbox" name="role_view">
														</td>
														<td><input type="checkbox" name="role_add">
														</td>
														<td><input type="checkbox" name="role_edit">
														</td>
														<td><input type="checkbox" name="role_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="role_assign">
																		分配角色
																	</label>
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>合作伙伴</td>
														<td><input type="checkbox" name="partner_list">
														</td>
														<td><input type="checkbox" name="partner_view">
														</td>
														<td><input type="checkbox" name="partner_add">
														</td>
														<td><input type="checkbox" name="partner_edit">
														</td>
														<td><input type="checkbox" name="partner_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>日记账</td>
														<td><input type="checkbox" name="books_list">
														</td>
														<td><input type="checkbox" name="books_view">
														</td>
														<td><input type="checkbox" name="books_add">
														</td>
														<td><input type="checkbox" name="books_edit">
														</td>
														<td><input type="checkbox" name="books_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="books_press">
																		取消日记账关联
																	</label>
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>入款项</td>
														<td><input type="checkbox" name="salespay_list">
														</td>
														<td><input type="checkbox" name="salespay_view">
														</td>
														<td><input type="checkbox" name="salespay_add">
														</td>
														<td><input type="checkbox" name="salespay_edit">
														</td>
														<td><input type="checkbox" name="salespay_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="confirm_salespay">
																		确认到账
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="check_salespay">
																		审核到账
																	</label>
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>系统日志</td>
														<td><input type="checkbox" name="system_log_list">
														</td>
														<td><input type="checkbox" name="system_log_view">
														</td>
														<td><input type="checkbox" name="system_log_add">
														</td>
														<td><input type="checkbox" name="system_log_edit">
														</td>
														<td><input type="checkbox" name="system_log_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>开票记录</td>
														<td><input type="checkbox" name="invoice_list">
														</td>
														<td><input type="checkbox" name="invoice_view">
														</td>
														<td><input type="checkbox" name="invoice_add">
														</td>
														<td><input type="checkbox" name="invoice_edit">
														</td>
														<td><input type="checkbox" name="invoice_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="confirm_invoice">
																		执行开票
																	</label>
																</li>

																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="verify_invoice">
																		审批预开票申请
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="apply_invalidate_invoice">
																		申请废票
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="do_invalidate_invoice">
																		执行废票
																	</label>
																</li>

															</ul>
														</td>
													</tr>
													<tr>
														<td>商品</td>
														<td><input type="checkbox" name="goods_list">
														</td>
														<td><input type="checkbox" name="goods_view">
														</td>
														<td><input type="checkbox" name="goods_add">
														</td>
														<td><input type="checkbox" name="goods_edit">
														</td>
														<td><input type="checkbox" name="goods_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>商品与基础产品关系表</td>
														<td><input type="checkbox" name="gprelation_list">
														</td>
														<td><input type="checkbox" name="gprelation_view">
														</td>
														<td><input type="checkbox" name="gprelation_add">
														</td>
														<td><input type="checkbox" name="gprelation_edit">
														</td>
														<td><input type="checkbox" name="gprelation_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>技术产品</td>
														<td><input type="checkbox" name="product_tech_list">
														</td>
														<td><input type="checkbox" name="product_tech_view">
														</td>
														<td><input type="checkbox" name="product_tech_add">
														</td>
														<td><input type="checkbox" name="product_tech_edit">
														</td>
														<td><input type="checkbox" name="product_tech_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>产品开通参数表</td>
														<td><input type="checkbox" name="product_oparam_list">
														</td>
														<td><input type="checkbox" name="product_oparam_view">
														</td>
														<td><input type="checkbox" name="product_oparam_add">
														</td>
														<td><input type="checkbox" name="product_oparam_edit">
														</td>
														<td><input type="checkbox" name="product_oparam_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>基础产品</td>
														<td><input type="checkbox" name="product_basic_list">
														</td>
														<td><input type="checkbox" name="product_basic_view">
														</td>
														<td><input type="checkbox" name="product_basic_add">
														</td>
														<td><input type="checkbox" name="product_basic_edit">
														</td>
														<td><input type="checkbox" name="product_basic_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>产品、技术产品关系表</td>
														<td><input type="checkbox" name="ptrelation_list">
														</td>
														<td><input type="checkbox" name="ptrelation_view">
														</td>
														<td><input type="checkbox" name="ptrelation_add">
														</td>
														<td><input type="checkbox" name="ptrelation_edit">
														</td>
														<td><input type="checkbox" name="ptrelation_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>商品价格</td>
														<td><input type="checkbox" name="goods_price_list">
														</td>
														<td><input type="checkbox" name="goods_price_view">
														</td>
														<td><input type="checkbox" name="goods_price_add">
														</td>
														<td><input type="checkbox" name="goods_price_edit">
														</td>
														<td><input type="checkbox" name="goods_price_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>捆绑商品关系表</td>
														<td><input type="checkbox" name="bgrelation_list">
														</td>
														<td><input type="checkbox" name="bgrelation_view">
														</td>
														<td><input type="checkbox" name="bgrelation_add">
														</td>
														<td><input type="checkbox" name="bgrelation_edit">
														</td>
														<td><input type="checkbox" name="bgrelation_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>返点记录</td>
														<td><input type="checkbox" name="rebate_list">
														</td>
														<td><input type="checkbox" name="rebate_view">
														</td>
														<td><input type="checkbox" name="rebate_add">
														</td>
														<td><input type="checkbox" name="rebate_edit">
														</td>
														<td><input type="checkbox" name="rebate_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="examine_rebate">
																		审批返点
																	</label>

																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="confirm_rebate">
																		确认返点
																	</label>

																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>退款记录</td>
														<td><input type="checkbox" name="refund_list">
														</td>
														<td><input type="checkbox" name="refund_view">
														</td>
														<td><input type="checkbox" name="refund_add">
														</td>
														<td><input type="checkbox" name="refund_edit">
														</td>
														<td><input type="checkbox" name="refund_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="examine_rebate">
																		审批退款
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="confirm_refund">
																		确认退款
																	</label>
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>订单记录</td>
														<td><input type="checkbox" name="order_record_list">
														</td>
														<td><input type="checkbox" name="order_record_view">
														</td>
														<td><input type="checkbox" name="order_record_add">
														</td>
														<td><input type="checkbox" name="order_record_edit">
														</td>
														<td><input type="checkbox" name="order_record_del">
														</td>
														<td></td>
													</tr>
													<tr>
														<td>开通记录</td>
														<td><input type="checkbox" name="applyopen_list">
														</td>
														<td><input type="checkbox" name="applyopen_view">
														</td>
														<td><input type="checkbox" name="applyopen_add">
														</td>
														<td><input type="checkbox" name="applyopen_edit">
														</td>
														<td><input type="checkbox" name="applyopen_del">
														</td>
														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="apply_open">
																		申请开通
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="confirm_open">
																		确认开通
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="below_line_open">
																		线下开通
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="see_params">
																		查看参数
																	</label>
																</li>
															</ul>
														</td>

													<tr>
														<td>代办信息</td>
														<td colspan="5">
														</td>

														<td>
															<ul>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="mess_invoice">
																		票据
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="mess_salespay">
																		入款项
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="mess_rebate">
																		返点
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="mess_refund">
																		退款
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="mess_applyopen">
																		开通
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="mess_audit">
																		待查账
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="mess_pennyOrder">
																		内划订单
																	</label>
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>报表</td>
														<td colspan="5">
														</td>
														<td>
															<ul>

																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="xssrmxDownload">
																		导出 销售收入明细（财务对账专用）
																	</label>
																</li>
																<li>
																	<label class="checkbox">
																		<input type="checkbox" name="xssrjsmxDownload">
																		导出 销售收入结算明细
																	</label>
																</li>
															</ul>
														</td>
													</tr>
												</tbody></table>
                                            <!-- 按钮区域 start -->
                                            <div class="form-actions">
                                                <button class="btn btn-primary" type="submit">保存</button>
                                            </div>
											<!-- 按钮区域 end -->
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="<?php echo base_url(); ?>style/jquerytree/jquery.treeview.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>style/jquerytree/screen.css" />
<script src="<?php echo base_url(); ?>style/jquerytree/jquery.cookie.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>style/jquerytree/jquery.treeview.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>style/jquerytree/demo.js"></script>
<script src="<?php echo base_url(); ?>style/admin/js/jquery.dataTables.js">
</script>
<script type="text/javascript">
	//Data Tables
	$(document).ready(function() {
		var select_json = <?php echo json_encode($activity_auth_checked); ?>;

		$.each(select_json, function(key, val) {
			$('[name=' + val + ']').attr('checked', 'true');

		})
	});
</script>