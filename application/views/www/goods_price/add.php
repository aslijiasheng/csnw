<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						商品价格新建页面
						<span class="mini-title">
						goods_price
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/goods_price/')?>">
<i class="icon-undo"></i>
返回商品价格列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php if(!isset($_GET['type_id'])){echo site_url('www/goods_price/add');}else{echo site_url('www/goods_price/add?type_id='.$_GET['type_id']);}?>" method="post">
		 	<?php if(0==1){?>
		 	<?php if(!isset($_GET['type_id'])){ ?>
		 	  
						<div class="control-group">
							<label class="control-label required" for="goods_price_effective_days">
								<?php echo $labels["goods_price_effective_days"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_effective_days]" value="<?php echo $id_aData['goods_price_effective_days'] ?>" id="goods_price_effective_days">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_effective_days]" id="goods_price_effective_days">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_cycle_unit">
								<?php echo $labels["goods_price_cycle_unit"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="goods_price[goods_price_cycle_unit]">
<?php foreach($goods_price_cycle_unit_enum as $cycle_unit_v){ ?>
   <?php if(isset($id_aData) && $cycle_unit_v['enum_key']==$id_aData['goods_price_cycle_unit_arr']['enum_key'] ){ ?>
     <option value="<?php echo $cycle_unit_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $cycle_unit_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $cycle_unit_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_cycle_price">
								<?php echo $labels["goods_price_cycle_price"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_cycle_price]" value="<?php echo $id_aData['goods_price_cycle_price'] ?>" id="goods_price_cycle_price">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_cycle_price]" id="goods_price_cycle_price">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_start_price">
								<?php echo $labels["goods_price_start_price"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_start_price]" value="<?php echo $id_aData['goods_price_start_price'] ?>" id="goods_price_start_price">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_start_price]" id="goods_price_start_price">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_cycle_days">
								<?php echo $labels["goods_price_cycle_days"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_cycle_days]" value="<?php echo $id_aData['goods_price_cycle_days'] ?>" id="goods_price_cycle_days">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_cycle_days]" id="goods_price_cycle_days">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_code">
								<?php echo $labels["goods_price_code"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_code]" value="<?php echo $id_aData['goods_price_code'] ?>" id="goods_price_code">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_code]" id="goods_price_code">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_visable">
								<?php echo $labels["goods_price_visable"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_price_visable_enum as $visable_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $visable_v['enum_key']==$id_aData['goods_price_visable_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $visable_v['enum_key']?>" name="goods_price[goods_price_visable]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $visable_v['enum_key']?>" name="goods_price[goods_price_visable]">
<?php }?>
<?php echo $visable_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_goods_id">
								<?php echo $labels["goods_price_goods_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="goods_price[goods_price_goods_id]" id="goods_price_goods_id" value_id ="<?php if(isset($id_aData['goods_price_goods_id_arr']['goods_id'])){echo $id_aData['goods_price_goods_id_arr']['goods_id']; } ?>" value="<?php if(isset($id_aData['goods_price_goods_id_arr']['goods_name'])){echo $id_aData['goods_price_goods_id_arr']['goods_name'];} ?>" url="<?php echo site_url('www/goods/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
		 	<?php }else{ ?>
		    <?php switch ($_GET["type_id"]){ 
							case -1:
							break;
						?>
						
						<?php default: ?>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_effective_days">
								<?php echo $labels["goods_price_effective_days"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_effective_days]" value="<?php echo $id_aData['goods_price_effective_days'] ?>" id="goods_price_effective_days">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_effective_days]" id="goods_price_effective_days">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_cycle_unit">
								<?php echo $labels["goods_price_cycle_unit"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="goods_price[goods_price_cycle_unit]">
<?php foreach($goods_price_cycle_unit_enum as $cycle_unit_v){ ?>
   <?php if(isset($id_aData) && $cycle_unit_v['enum_key']==$id_aData['goods_price_cycle_unit_arr']['enum_key'] ){ ?>
     <option value="<?php echo $cycle_unit_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $cycle_unit_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $cycle_unit_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_cycle_price">
								<?php echo $labels["goods_price_cycle_price"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_cycle_price]" value="<?php echo $id_aData['goods_price_cycle_price'] ?>" id="goods_price_cycle_price">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_cycle_price]" id="goods_price_cycle_price">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_start_price">
								<?php echo $labels["goods_price_start_price"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_start_price]" value="<?php echo $id_aData['goods_price_start_price'] ?>" id="goods_price_start_price">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_start_price]" id="goods_price_start_price">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_cycle_days">
								<?php echo $labels["goods_price_cycle_days"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_cycle_days]" value="<?php echo $id_aData['goods_price_cycle_days'] ?>" id="goods_price_cycle_days">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_cycle_days]" id="goods_price_cycle_days">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_code">
								<?php echo $labels["goods_price_code"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_code]" value="<?php echo $id_aData['goods_price_code'] ?>" id="goods_price_code">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_code]" id="goods_price_code">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_visable">
								<?php echo $labels["goods_price_visable"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_price_visable_enum as $visable_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $visable_v['enum_key']==$id_aData['goods_price_visable_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $visable_v['enum_key']?>" name="goods_price[goods_price_visable]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $visable_v['enum_key']?>" name="goods_price[goods_price_visable]">
<?php }?>
<?php echo $visable_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_goods_id">
								<?php echo $labels["goods_price_goods_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="goods_price[goods_price_goods_id]" id="goods_price_goods_id" value_id ="<?php if(isset($id_aData['goods_price_goods_id_arr']['goods_id'])){echo $id_aData['goods_price_goods_id_arr']['goods_id']; } ?>" value="<?php if(isset($id_aData['goods_price_goods_id_arr']['goods_name'])){echo $id_aData['goods_price_goods_id_arr']['goods_name'];} ?>" url="<?php echo site_url('www/goods/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
					<?php }?>
                <?php }}else{ ?>
                  
						<div class="control-group">
							<label class="control-label required" for="goods_price_effective_days">
								<?php echo $labels["goods_price_effective_days"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_effective_days]" value="<?php echo $id_aData['goods_price_effective_days'] ?>" id="goods_price_effective_days">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_effective_days]" id="goods_price_effective_days">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_cycle_unit">
								<?php echo $labels["goods_price_cycle_unit"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="goods_price[goods_price_cycle_unit]">
<?php foreach($goods_price_cycle_unit_enum as $cycle_unit_v){ ?>
   <?php if(isset($id_aData) && $cycle_unit_v['enum_key']==$id_aData['goods_price_cycle_unit_arr']['enum_key'] ){ ?>
     <option value="<?php echo $cycle_unit_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $cycle_unit_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $cycle_unit_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_cycle_price">
								<?php echo $labels["goods_price_cycle_price"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_cycle_price]" value="<?php echo $id_aData['goods_price_cycle_price'] ?>" id="goods_price_cycle_price">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_cycle_price]" id="goods_price_cycle_price">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_start_price">
								<?php echo $labels["goods_price_start_price"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_start_price]" value="<?php echo $id_aData['goods_price_start_price'] ?>" id="goods_price_start_price">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_start_price]" id="goods_price_start_price">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_cycle_days">
								<?php echo $labels["goods_price_cycle_days"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_cycle_days]" value="<?php echo $id_aData['goods_price_cycle_days'] ?>" id="goods_price_cycle_days">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_cycle_days]" id="goods_price_cycle_days">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_code">
								<?php echo $labels["goods_price_code"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_code]" value="<?php echo $id_aData['goods_price_code'] ?>" id="goods_price_code">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods_price[goods_price_code]" id="goods_price_code">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_visable">
								<?php echo $labels["goods_price_visable"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_price_visable_enum as $visable_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $visable_v['enum_key']==$id_aData['goods_price_visable_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $visable_v['enum_key']?>" name="goods_price[goods_price_visable]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $visable_v['enum_key']?>" name="goods_price[goods_price_visable]">
<?php }?>
<?php echo $visable_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_price_goods_id">
								<?php echo $labels["goods_price_goods_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="goods_price[goods_price_goods_id]" id="goods_price_goods_id" value_id ="<?php if(isset($id_aData['goods_price_goods_id_arr']['goods_id'])){echo $id_aData['goods_price_goods_id_arr']['goods_id']; } ?>" value="<?php if(isset($id_aData['goods_price_goods_id_arr']['goods_name'])){echo $id_aData['goods_price_goods_id_arr']['goods_name'];} ?>" url="<?php echo site_url('www/goods/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
                 <?php }?>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>