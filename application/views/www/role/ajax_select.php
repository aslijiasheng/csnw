	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="100">操作</th>
				
				
					
					<th><?php echo $labels['role_name'];?></th>
					
					<th><?php echo $labels['role_desc'];?></th>
					
				

			</tr>
		</thead>
		<tbody>
		<?php foreach($listData as $listData_value){?>
			<tr>
				<td style="font-size:16px;">
					<a href="<?php echo site_url('www/role/view').'?role_id='.$listData_value['role_id'].'&type_id='.$_GET['type_id'] ?>" rel="tooltip" title="查看"><span class="icon-eye"></span></a>
				    <a href="<?php echo site_url('www/role/update').'?role_id='.$listData_value['role_id'].'&type_id='.$_GET['type_id'] ?>" rel="tooltip" title="更新"><span class="icon-pencil"></span></a>
				    <a href="<?php echo site_url('www/role/del').'?role_id='.$listData_value['role_id'].'&type_id='.$_GET['type_id'] ?>" rel="tooltip" title="删除"><span class="icon-remove-2"></span></a> 
				    <div class="btn-group">
				    <a href="" title="分配权限" data-toggle="dropdown" rel="tooltip">
				    <span class="icon-file-xml"></span>
				    	
				    </a>
				    <ul role="menu" class="dropdown-menu">
								<li><a href="<?php echo site_url('www/menu_auth?role_id='.$listData_value['role_id']) ?>" tabindex="-1">配置菜单权限</a></li>
								<li><a href="<?php echo site_url('www/data_auth?role_id='.$listData_value['role_id']) ?>" tabindex="-1">配置数据权限</a></li>
								<li><a href="<?php echo site_url('www/activity_auth?role_id='.$listData_value['role_id']) ?>" tabindex="-1">配置功能权限</a></li>
						</ul>
					</div>	
				</td>
				
				
					
					<td>
					<?php  echo $listData_value['role_name'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['role_desc'];?>
					</td>
					
				
				
				
			</tr>
		<?php }?>
		</tbody>
		
	</table>
	<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber;?>条</div>
	<input type='hidden' id='totalNumber' value="<?php echo $totalNumber;?>">
	</div>

