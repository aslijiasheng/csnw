<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						角色分配页面
						<span class="mini-title">
						role
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/user/view?user_id='.$_GET['user_id'])?>">
<i class="icon-undo"></i>
返回
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php echo site_url('www/role/doassign?user_id='.$_GET['user_id']);?>" method="post">
		<div class="control-group">
			<label class="control-label required" for="role_name">
				姓名:
				<span class="required">*</span>
			</label>
			<div class="controls">
			<input type="text" maxlength="128" readonly="readonly" value="<?php echo $user_info['user_name'] ?>">
			</div>
	    </div>
	   
	    <div class="control-group ">
			<label class="control-label required">
				角色设定
				<span class="required"></span>
			</label>
			<div class="controls">
				<ul class="lee_checkbox">
				 <?php foreach ($listData as $listData_value) :?>
					<li>
					    <?php if(!empty($role_arr)){if(in_array($listData_value['role_id'], $role_arr)){ ?>
						<input type="checkbox" name="role[]" value="<?php echo $listData_value['role_id'] ?>" id="is_obj_type" checked="checked">
						<?php }else{ ?>
						<input type="checkbox" name="role[]" value="<?php echo $listData_value['role_id'] ?>" id="is_obj_type">
						<?php }}else{ ?>
						<input type="checkbox" name="role[]" value="<?php echo $listData_value['role_id'] ?>" id="is_obj_type">
						<?php } ?>
						<label for="is_obj_type"><?php echo $listData_value['role_desc']; ?></label>
						<!-- <input type="hidden" name="role_id[]" value="<?php echo $listData_value['role_id'] ?>"> -->
					</li>
				
				<?php endforeach?>
				</ul>
			</div>
		</div>
	    
		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>