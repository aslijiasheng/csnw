	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="75">操作</th>
				<th><?php echo $labels['refund_manage'];?></th>
				<th><?php echo $labels['refund_pay_method'];?></th>
				<th><?php echo $labels['refund_status'];?></th>
				<th><?php echo $labels['refund_create_user'];?></th>
				<th><?php echo $labels['refund_create_time'];?></th>
				<th><?php echo $labels['refund_order_id'];?></th>
				<th>本次退款金额</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($listData as $listData_value){?>
		<?php  $user_auth = $this->user->user_auth($this->session->userdata('user_id')); ?>
			<tr>
				<td style="font-size:16px;">
					<div class="btn-toolbar">
						<div class="btn-group">
						   	  	<a class="btn btn-small btn-primary refund_view" id="<?php  echo $listData_value['refund_id'];?>" <?php if(!in_array('refund_view', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>查看</a>
								<button class="btn btn-small dropdown-toggle btn-primary" data-toggle="dropdown" <?php if(!in_array('refund_view', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
									<span class="caret"></span>
								</button>

							<ul class="dropdown-menu">
								<li <?php if(!in_array('examine_rebate', $user_auth['activity_auth_arr']) or $listData_value['refund_status'] != 1001 or $this->session->userdata('user_id')!=$listData_value['refund_manage']){ echo 'style="display:none;"';} ?>>
									<a id="<?php  echo $listData_value['refund_id'];?>" class="examine_refund">审批退款</a>
								</li>
								<li <?php if(!in_array('confirm_refund', $user_auth['activity_auth_arr']) or $listData_value['refund_status'] != 1002){ echo 'style="display:none;"';} ?>>
									<a id="<?php  echo $listData_value['refund_id'];?>" class="confirm_refund">确认退款</a>
								</li>
								<li <?php if(!in_array('refund_edit', $user_auth['activity_auth_arr']) or !in_array($listData_value['refund_status'], array(1003,1005))){ echo 'style="display:none;"';} ?>>
									<a id="<?php  echo $listData_value['refund_id'];?>" class="edit_refund">编辑</a>
								</li>
								<li <?php if(!in_array('refund_del', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
									<a id="<?php  echo $listData_value['refund_id'];?>" class="del_refund">删除</a>
								</li>
							</ul>
						</div>
					</div>
				</td>



					<td>
							<?php
			if ($listData_value['refund_manage']!=0 and $listData_value['refund_manage']!=""){
				echo $listData_value['refund_manage_arr']['user_name'];
			}else{
				echo "&nbsp";
			}
		?>
					</td>

					<td>
					<?php echo $listData_value['refund_pay_method_arr']['enum_name'];?>
					</td>

					<td>
					<?php echo $listData_value['refund_status_arr']['enum_name'];?>
					</td>

					<td>
							<?php
			if ($listData_value['refund_create_user']!=0 and $listData_value['refund_create_user']!=""){
				echo $listData_value['refund_create_user_arr']['user_name'];
			}else{
				echo "&nbsp";
			}
		?>
					</td>

					<td>
					<?php  echo $listData_value['refund_create_time'];?>
					</td>

					<td>
						<?php
							if ($listData_value['refund_order_id']!=0 and $listData_value['refund_order_id']!=""){
								echo $listData_value['refund_order_id_arr']['order_number'];
							}else{
								echo "&nbsp";
							}
						?>
					</td>

					<td>
						<?php  echo $listData_value['refund_amount'];?>
					</td>
			</tr>
		<?php }?>
		</tbody>

	</table>

<script type="text/javascript">
$(document).ready(function() {
	$(".refund_view").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'refund_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/refund/ajax_view'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'退款详细信息',
			width:800,
			height:450,
			buttons:''
		});
		$('#operation_dialog').dialog('open');
	});

	$(".examine_refund").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'refund_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/refund/ajax_examine_refund'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'审批退款',
			width:800,
			height:450,
			buttons: [{
				text:"审批通过",
				Class:"btn btn-primary",
				click: function(){
					formdata = $("#examine_refund_form").serializeArray();
					formdata[formdata.length]={"name":"refund[refund_status]","value":"1002"}; //添加1个参数
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("审批通过");
								$('#refund').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");

							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/refund/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"驳回审批",
				Class:"btn btn-success",
				click: function(){
					formdata = $("#examine_refund_form").serializeArray();
					formdata[formdata.length]={"name":"refund[refund_status]","value":"1003"}; //添加1个参数
					//alert(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("驳回审批");
								$('#refund').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/refund/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了审批返点");
					$(this).dialog("close");
				}
			}]

		});
		$('#operation_dialog').dialog('open');
	});

	$(".confirm_refund").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'refund_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/refund/ajax_confirm_refund'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'确认退款',
			width:800,
			height:450,
			buttons: [{
				text:"退款成功",
				Class:"btn btn-primary",
				click: function(){
					formdata = $("#examine_refund_form").serializeArray();
					formdata[formdata.length]={"name":"refund[refund_status]","value":"1004"}; //添加1个参数
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("已退款");
								$('#refund').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");

							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/refund/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"退款不成功",
				Class:"btn btn-success",
				click: function(){
					formdata = $("#examine_refund_form").serializeArray();
					formdata[formdata.length]={"name":"refund[refund_status]","value":"1005"}; //添加1个参数
					//alert(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("退款不成功");
								$('#refund').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/refund/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了审批返点");
					$(this).dialog("close");
				}
			}]

		});
		$('#operation_dialog').dialog('open');
	});

	$(".edit_refund").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'refund_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/refund/ajax_edit'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'修改退款',
			width:800,
			height:450,
			buttons: [{
				text:"保存",
				Class:"btn btn-primary",
				click: function(){
					formdata = $("#refund-form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//这里需要通过产品单个金额汇总【本次退款金额】
					var $refund_amount = 0; //本次退款金额
					$('.goods_refund_amount').each(function(){
						$goods_amount = $(this).closest('td').siblings('.goods_amount').html(); //这个商品的折后价
						$goods_refund_amount = $(this).val(); //这个商品的返款金额
						if($goods_refund_amount==""){
							$goods_refund_amount=0;
						}
						if(($goods_amount-$goods_refund_amount)<0){
							alert($goods_amount+"-"+$goods_refund_amount);
							alertify.alert('商品的【退款金额】不得大于商品【折后价】');
							return false;
						}
						$refund_amount += parseInt($goods_refund_amount);
					});
					//可退款金额
					$ktk_amount = $('#ktk_amount').html();
					if(($ktk_amount-$refund_amount)<0){
						alertify.alert("【本次退款金额】不得大于【可退款金额】");
						return false;
					}
					//return false;
					//将本次退款金额添加到formdata里
					formdata[formdata.length]={"name":"refund[refund_amount]","value":$refund_amount};

					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("修改退款成功");
								$('#refund').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");
							}else{
								alertify.alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/refund/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了修改");
					$(this).dialog("close");
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

	$(".del_refund").click(function(){
		id=$(this).attr('id');
		alertify.confirm("是否删除这个退款记录", function (e) {
			if (e) {
				$.ajax({
					'type':'post',
					'data':'refund_id='+id+'&order_id='+<?php echo $_GET['order_id'] ?>,
					'success':function(data){
						if(data==1){
							alertify.success("删除成功");
							$('#refund').ajaxHtml(); //再次加载1次页面
							$('#operation_log').ajaxHtml();
						}else{
							alertify.alert('失败'+data);
						}
					},
					'url':'<?php echo site_url('www/refund/ajax_del_post'); ?>',
					'async':false
				});
			} else {
				alertify.error("你放弃了删除");
			}
		});
	});
});
</script>