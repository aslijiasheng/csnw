<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="refund-form" method="post">
		<!-- 用一个隐藏文本框来存order_id -->
		<input size="16" type="hidden" name="refund[refund_id]" value='<?php echo $refund_data['refund_id'];?>'>
		<input size="16" type="hidden" name="refund[refund_order_id]" value='<?php echo $refund_data['refund_order_id'];?>'>
		<!-- <input type="hidden" name="order_finance_department" value="<?php echo $refund_data['refund_pay_info_arr']['department'] ?>"> -->
		<div class="control-group">
			<label class="control-label required" for="refund_create_time">
				订单信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td width="10%">订单总金额</td>
								<td width="10%"><?php echo $refund_data['refund_order_id_arr']['order_amount'];?></td>
								<td width="10%">已到帐金额</td>
								<td width="10%"><?php echo $ydz_amount;?></td>
							<tr>
							</tr>
								<td>已确认退款</td>
								<td><?php echo $ytk_amount?></td>
								<td>未确认退款</td>
								<td><?php echo $wtk_amount?></td>
							</tr>
							</tr>
								<td>客户名称</td>
								<td><?php if($refund_data['refund_order_id_arr']['order_account']!=0 and $refund_data['refund_order_id_arr']['order_account']!=""){echo $refund_data['refund_order_id_arr']['order_account_arr']['account_name'];}?></td>
								<td>可退款金额</td>
								<td id="ktk_amount"><?php echo $ktk_amount?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="refund_manage">
				业务总监
				<span class="required"></span>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="refund[refund_manage]" required="required" id="refund_manage" value_id ="<?php echo $refund_data['refund_manage_arr']['user_id'];?>" value="<?php echo $refund_data['refund_manage_arr']['user_name'];?>" >
				<script type="text/javascript">
					$(document).ready(function () {
						$('#refund_manage').leeQuote({
							url:'<?php echo site_url('www/user/ajax_list?tag_name=refund_manage');?>',
							title:'选择业务总监',
							data:{
								'rel_role_id':10,
							}
						});
					});
				</script>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="refund_goods_info">
				退款商品信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>商品名称</hr>
							<th>折后价格</hr>
							<th width="80">已使用天数</th>
							<th width="90">已使用费用</th>
							<th width="120">退款金额</th>
						</tr>
					</thead>
					<tbody>
						<?php
							//这里循环出所有的商品
							foreach ($refund_data['refund_order_id_arr']['detailed']['order_d_goods_arr'] as $k=>$v){
						?>
						<tr>
							<td><?php echo $v['order_d_goods_name'];?></td>
							<td  class='goods_amount'><?php echo $v['order_d_product_basic_disc']?></td>
							<td><input type="text" style='width:50px' name="goods_info[days_<?php echo $v['order_d_goods_code'];?>]" value='<?php echo $refund_data['refund_goods_info_arr']['days_'.$v['order_d_goods_code']];?>'>天</td>
							<td><input type="text" style='width:70px' name="goods_info[cost_<?php echo $v['order_d_goods_code'];?>]" value='<?php echo $refund_data['refund_goods_info_arr']['cost_'.$v['order_d_goods_code']];?>'></td>
							<td><input type="text" style='width:100px' name="goods_info[amount_<?php echo $v['order_d_goods_code'];?>]" value='<?php echo $refund_data['refund_goods_info_arr']['amount_'.$v['order_d_goods_code']];?>' class='goods_refund_amount'></td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="refund_pay_method">
				付款方式
				<span class="required"></span>
			</label>
			<div class="controls">
				<select id="refund_pay_method" name="refund[refund_pay_method]">
					<?php foreach($refund_pay_method_enum as $pay_method_v){ ?>
					<option value="<?php echo $pay_method_v['enum_key'];?>" <?php
						if($refund_data['refund_pay_method']==$pay_method_v['enum_key']){echo 'selected';}
					?>>
						<?php echo $pay_method_v['enum_name'];?>
					</option>
					<?php } ?>
				</select>
			</div>
		</div>

						<div class="control-group">
							<div class="controls">
<div id="remit_info" class="well payinfo" <?php
	if ($refund_data['refund_pay_method']!=1001){
		echo 'style="display:none;"';
	}
?>>
	<div class="form-horizontal">
		<div class="control-group">
			<label class="control-label" style="width: 120px;">账户类型：</label>
			<div class="controls" style="margin-left: 120px;">
				<label class="radio inline">
					<input type="radio" name="payinfo[bank_type]" value="1001" <?php
						if($refund_data['refund_pay_info_arr']['bank_type']==1001){echo 'checked';}
					?>>
					企业
				</label>
				<label class="radio inline">
					<input type="radio" name="payinfo[bank_type]" value="1002" <?php
						if($refund_data['refund_pay_info_arr']['bank_type']==1002){echo 'checked';}
					?>>
					个人
				</label>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;" for="remit_bank">银行账户：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="payinfo[remit_bank]" id="remit_bank" required="required" class="isrequired" value='<?php echo $refund_data['refund_pay_info_arr']['bank_account']?>'>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;" for="remit_person">账户名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="payinfo[remit_person]" id="remit_person" required="required" class="isrequired" value='<?php echo $refund_data['refund_pay_info_arr']['remit_person']?>'>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">开户银行名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="payinfo[remit_account]" value='<?php echo $refund_data['refund_pay_info_arr']['remit_account']?>'>
			</div>
		</div>
	</div>
</div>
<div id="alipay_info" class="well payinfo" <?php
	if ($refund_data['refund_pay_method']!=1002){
		echo 'style="display:none;"';
	}
?>>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="alipay_order">交易号：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="payinfo[alipay_order]" id="alipay_order" required="required" class="isrequired" value='<?php echo $refund_data['refund_pay_info_arr']['alipay_order']?>'>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="alipay_account">支付宝账号：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="payinfo[alipay_account]" id="alipay_account" required="required" class="isrequired" value='<?php echo $refund_data['refund_pay_info_arr']['alipay_account']?>'>
		</div>
	</div>
</div>
<div id="cash_info" class="payinfo" <?php
	if ($refund_data['refund_pay_method']!=1003){
		echo 'style="display:none;"';
	}
?>> </div>

							</div>
						</div>
<script type="text/javascript">
$(document).ready(function () {
	var refund_pay_method = '<?php echo $refund_data['refund_pay_method']; ?>';
	if(refund_pay_method ==1001){
		$("#alipay_info").find('.isrequired').removeAttr('required');

	}else if(refund_pay_method==1002){
		$("#remit_info").find('.isrequired').removeAttr('required');
	}else{
		$("#remit_info").find('.isrequired').removeAttr('required');
		$("#alipay_info").find('.isrequired').removeAttr('required');
	}
	$('#refund_pay_method').change(function(){
		//获取选择的value用作判断
		enumkey = $(this).val();
		if(enumkey==1001){
			$(".payinfo").hide();
			$("#remit_info").show();
			$("#remit_info").find('.isrequired').attr('required','required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1002){
			$(".payinfo").hide();
			$("#alipay_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').attr('required','required');
		}
		if(enumkey==1003){
			$(".payinfo").hide();
			$("#cash_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
		}
	});
});
</script>
		<div class="control-group">
			<label class="control-label required" for="refund_reason">
				退款理由
			</label>
			<div class="controls">
				<textarea name="refund[refund_reason]" id="refund_reason" style="width:400px" rows="6"><?php echo $refund_data['refund_reason'];?></textarea>
			</div>
		</div>
	</form>
</div>
<!--lee 内容部分 end-->