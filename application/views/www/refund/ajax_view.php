<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="refund-form" method="post">
		<!-- 用一个隐藏文本框来存order_id -->
		<input size="16" type="hidden" name="refund[refund_id]" value='<?php echo $refund_data['refund_id'];?>'>
		<div class="control-group">
			<label class="control-label required" for="rebate_create_time">
				订单信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td width="10%">订单总金额</td>
								<td width="10%"><?php echo $refund_data['refund_order_id_arr']['order_amount'];?></td>
								<td width="10%">已到帐金额</td>
								<td width="10%"><?php echo $ydz_amount;?></td>
							<tr>
							</tr>
								<td>已确认退款</td>
								<td><?php echo $ytk_amount?></td>
								<td>未确认退款</td>
								<td><?php echo $wtk_amount?></td>
							</tr>
							</tr>
								<td>客户名称</td>
								<td><?php if($refund_data['refund_order_id_arr']['order_account']!=0 and $refund_data['refund_order_id_arr']['order_account']!=""){echo $refund_data['refund_order_id_arr']['order_account_arr']['account_name'];}
								?></td>
								<td>可退款金额</td>
								<td id="ktk_amount"><?php echo $ktk_amount?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="refund_manage">
				业务总监
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php 
						if($refund_data['refund_manage']!="" and $refund_data['refund_manage']!=0){
							echo $refund_data['refund_manage_arr']['user_name'];
						}
					?>
				</div>
			</div>
		</div>
<script type="text/javascript">
$(document).ready(function () {
	//这里的引用需要触发一下
	$('#refund_manage').leeQuote();
});
</script>
		
		<div class="control-group">
			<label class="control-label required" for="refund_goods_info">
				退款商品信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>商品名称</hr>
							<th>折后价格</hr>
							<th width="80">已使用天数</th>
							<th width="90">已使用费用</th>
							<th width="120">退款金额</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							//这里循环出所有的商品
							foreach ($refund_data['refund_order_id_arr']['detailed']['order_d_goods_arr'] as $k=>$v){
						?>
						<tr>
							<td><?php echo $v['order_d_goods_name'];?></td>
							<td  class='goods_amount'><?php echo $v['order_d_product_basic_disc']?></td>
							<td><?php echo $refund_data['refund_goods_info_arr']['days_'.$v['order_d_goods_code']];?>天</td>
							<td><?php echo $refund_data['refund_goods_info_arr']['cost_'.$v['order_d_goods_code']];?></td>
							<td><?php echo $refund_data['refund_goods_info_arr']['amount_'.$v['order_d_goods_code']];?></td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label required" for="refund_pay_method">
				付款方式
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $refund_data['refund_pay_method_arr']['enum_name'];?>
				</div>
			</div>
		</div>
						
						<div class="control-group">
							<div class="controls">
<div id="remit_info" class="well payinfo">
	<div class="form-horizontal">
		<div class="control-group">
			<label class="control-label" style="width: 120px;">账户类型：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php if($refund_data['refund_pay_info_arr']['bank_type']==1001){echo '企业';}?>
					<?php if($refund_data['refund_pay_info_arr']['bank_type']==1002){echo '个人';}?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">银行账户：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $refund_data['refund_pay_info_arr']['remit_bank']?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">账户名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $refund_data['refund_pay_info_arr']['remit_person']?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">开户银行名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $refund_data['refund_pay_info_arr']['remit_account']?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="alipay_info" class="well payinfo"  style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;">交易号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $refund_data['refund_pay_info_arr']['alipay_order']?>
			</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;">支付宝账号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $refund_data['refund_pay_info_arr']['alipay_account']?>
			</div>
		</div>
	</div>
</div>
<div id="cash_info" class="payinfo" style="display:none;"> </div>
								
							</div>
						</div>
<script type="text/javascript">
$(document).ready(function () {
	if(<?php echo $refund_data['refund_pay_method'];?>==1001){
		$(".payinfo").hide();
		$("#remit_info").show();
	}
	if(<?php echo $refund_data['refund_pay_method'];?>==1002){
		$(".payinfo").hide();
		$("#alipay_info").show();
	}
	if(<?php echo $refund_data['refund_pay_method'];?>==1003){
		$(".payinfo").hide();
		$("#cash_info").show();
	}
});
</script>
		<div class="control-group">
			<label class="control-label required" for="refund_reason">
				退款理由
			</label>
			<div class="controls">
				<div class="group-text">
					<pre><?php echo $refund_data['refund_reason'];?></pre>
				</div>
			</div>
		</div>
	</form>
</div>
<!--lee 内容部分 end-->