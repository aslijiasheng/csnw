
<div class="all-sidebar">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget span5">
                <div class="widget-header">
                    <div class="title">
                        功能权限选择
                        <span class="mini-title">
                            Menu Manager
                        </span>
                    </div>
                    <span class="tools">
                    </span>
                </div>
                <div class="widget-body">
                    <div class="row-fluid">
                        <div class="leebutton">
                        <!--lee 按钮部分 start-->
                        <a href="<?php echo site_url('www/role') ?>" class="btn btn-primary" id="yw0">
                        <i class="icon-undo"></i>
                        返回角色列表
                        </a>
                        <!--lee 按钮部分 end-->
                        </div>
                        <div id="divmessagelist">
                            <div>
                                <div id="objlist" class="grid-view">
                                    <div id="dt_example" class="example_alt_pagination">
                                    <form action="<?php echo site_url('www/data_auth/add?role_id='.$_GET['role_id']) ?>" method="post" id="ff" name="a_form">
                                       <table class="table table-striped table-bordered table-hover">
                                            <tbody>
                                                <tr>
                                                  <th>对象</th>
                                                  <th>数据权限</th>
                                                </tr>

                                                <tr>
                                                  <td>客户</td>
                                                     <td>
                                                      <!--   <select id="" name="account">
                                                              <option value="">无</option>
                                                              <option name="" value="all_privileges">全部</option>
                                                              <option name="" value="self_department">本级</option>
                                                              <option name="" value="self_department_following">本级及以下</option>
                                                              <option name="" value="charge">负责人</option>
                                                        </select> -->
                                                     </td>
                                                </tr>
                                                 <tr>
                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;个人客户</td>
                                                     <td>
                                                        <select id="" name="individual_account">
                                                              <option value="">无</option>
                                                              <option name="" value="all_privileges">全部</option>
                                                              <option name="" value="self_department">本级</option>
                                                              <option name="" value="self_department_following">本级及以下</option>
                                                              <option name="" value="charge">负责人</option>
                                                        </select>
                                                     </td>
                                                </tr>

                                                <tr>
                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;企业</td>
                                                     <td>
                                                        <select id="" name="entreprise">
                                                              <option value="">无</option>
                                                              <option name="" value="all_privileges">全部</option>
                                                              <option name="" value="self_department">本级</option>
                                                              <option name="" value="self_department_following">本级及以下</option>
                                                              <option name="" value="charge">负责人</option>
                                                        </select>
                                                     </td>
                                                </tr>

                                                <tr>
                                                  <td>部门</td>
                                                     <td>
                                                        <select id="" name="department">
                                                              <option value="">无</option>
                                                              <option name="" value="all_privileges">全部</option>
                                                              <option name="" value="self_department">本级</option>
                                                              <option name="" value="self_department_following">本级及以下</option>
                                                              <option name="" value="charge">负责人</option>
                                                        </select>
                                                     </td>
                                                </tr>

                                            <tr>
                                              <td>人员</td>
                                                 <td>
                                                    <select id="" name="user">
                                                          <option value="">无</option>
                                                          <option name="" value="all_privileges">全部</option>
                                                          <option name="" value="self_department">本级</option>
                                                          <option name="" value="self_department_following">本级及以下</option>
                                                          <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                           </tr>

                                            <tr>
                                              <td>订单</td>
                                              <td></td>
                                               <!--   <td>
                                                    <select id="" name="order">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>  -->
                                            </tr>

                                            <tr>
                                              <td>&nbsp;&nbsp;&nbsp;&nbsp;销售订单</td>
                                                 <td>
                                                    <select id="" name="sale_order">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>&nbsp;&nbsp;&nbsp;&nbsp;线上订单</td>
                                                 <td>
                                                    <select id="" name="online_order">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                             <tr>
                                              <td>&nbsp;&nbsp;&nbsp;&nbsp;返款订单</td>
                                                 <td>
                                                    <select id="" name="rebates_order">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                             <tr>
                                              <td>&nbsp;&nbsp;&nbsp;&nbsp;内部划款订单</td>
                                                 <td>
                                                    <select id="" name="penny_order">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                             <tr>
                                              <td>&nbsp;&nbsp;&nbsp;&nbsp;预收款订单</td>
                                                 <td>
                                                    <select id="" name="deposit_order">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                               <tr>
                                              <td>&nbsp;&nbsp;&nbsp;&nbsp;提货销售订单</td>
                                                 <td>
                                                    <select id="" name="delivery_order">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>订单产品明细</td>
                                                 <td>
                                                    <select id="" name="order_d">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                                 </td>
                                            </tr>

                                            <tr>
                                              <td>角色</td>
                                                 <td>
                                                    <select id="" name="role">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>合作伙伴</td>
                                                 <td>
                                                    <select id="" name="partner">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>日记账</td>
                                                 <td>
                                                    <select id="" name="books">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>入款项</td>
                                                 <td>
                                                    <select id="" name="salespay">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>系统日志</td>
                                                 <td>
                                                    <select id="" name="system_log">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>开票记录</td>
                                                 <td>
                                                    <select id="" name="invoice">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>商品</td>
                                                 <td>
                                                    <select id="" name="goods">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>商品与基础产品关系表</td>
                                                 <td>
                                                    <select id="" name="gprelation">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>技术产品</td>
                                                 <td>
                                                    <select id="" name="product_tech">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>产品开通参数表</td>
                                                 <td>
                                                    <select id="" name="product_oparam">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>基础产品</td>
                                                 <td>
                                                    <select id="" name="product_basic">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>产品、技术产品关系表</td>
                                                 <td>
                                                    <select id="" name="ptrelation">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>商品价格</td>
                                                 <td>
                                                    <select id="" name="goods_price">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>捆绑商品关系表</td>
                                                 <td>
                                                    <select id="" name="bgrelation">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>返点记录</td>
                                                 <td>
                                                    <select id="" name="rebate">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>退款记录</td>
                                                 <td>
                                                    <select id="" name="refund">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>订单记录</td>
                                                 <td>
                                                    <select id="" name="order_record">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                           <tr>
                                              <td>代办信息</td>
                                                 <td>
                                                    <select id="" name="message">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>

                                            <tr>
                                              <td>报表查看</td>
                                                 <td>
                                                    <select id="" name="report">
                                                      <option value="">无</option>
                                                      <option name="" value="all_privileges">全部</option>
                                                      <option name="" value="self_department">本级</option>
                                                      <option name="" value="self_department_following">本级及以下</option>
                                                      <option name="" value="charge">负责人</option>
                                                    </select>
                                               </td>
                                            </tr>


                                        </tbody>
                                        </table>
                                            <!-- 按钮区域 start -->
                                            <div class="form-actions">
                                                <button class="btn btn-primary" type="submit">保存</button>
                                            </div>
        <!-- 按钮区域 end -->
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="<?php echo base_url(); ?>style/jquerytree/jquery.treeview.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>style/jquerytree/screen.css" />
    <script src="<?php echo base_url(); ?>style/jquerytree/jquery.cookie.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>style/jquerytree/jquery.treeview.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>style/jquerytree/demo.js"></script>
<script src="<?php echo base_url(); ?>style/admin/js/jquery.dataTables.js">
</script>

<script type="text/javascript">
    //Data Tables

    $(document).ready(function() {

        var select_json = <?php echo json_encode($data_auth_checked);?>;
        $.each(select_json,function(k,v){
           $('select[name='+k+']').val(v);
        })
    });
</script>