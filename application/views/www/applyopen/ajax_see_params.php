<!--lee 内容部分 start-->
<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						开通参数
						<span class="mini-title">
						Open Params
						</span>
					</div>
				</div>
				<div class="widget-body">
					<table class="leetable table table-bordered">
						<tbody>
						<?php if ($applyopen['open']==""){echo "还没有开通参数";}else{?>
							<?php foreach($params['open'] as $v){?>
								<?php if($v['params_visable']==1){ //这里是用来判断未启用的不显示?>
								<tr>
									<td class="TbLeft"> <?php echo $v['params_name'];?> </td>
									<td class="TbRight"> <?php 
									if($v['params_type1']=='user'){ //这种类型属于用户填写的
										if(isset($applyopen['open'][$v['params_code']])){echo $applyopen['open'][$v['params_code']];};
									}else{ //这种是属于系统默认值得
										echo $v['params_value'];
									}
									?> </td>
								</tr>
								<?php } //判断是否停用end?>
							<?php }?>
							<tr>
								<td class="TbLeft"> 备注 </td>
								<td class="TbRight"> <?php echo $applyopen['open']['note'];?> </td>
							</tr>
						<?php }?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						返回参数
						<span class="mini-title">
						Return Params
						</span>
					</div>
				</div>
				<div class="widget-body">
					<table class="leetable table table-bordered">
						<tbody>
						<?php if ($applyopen['return']==""){echo "还没有返回参数";}else{?>
							<?php foreach($params['return'] as $v){?>
								<?php if($v['params_visable']==1){ //这里是用来判断未启用的不显示?>
								<tr>
									<td class="TbLeft"> <?php echo $v['params_name'];?> </td>
									<td class="TbRight"> <?php 
									if($v['params_type1']=='user'){ //这种类型属于用户填写的
										if(isset($applyopen['return'][$v['params_code']])){echo $applyopen['return'][$v['params_code']];}
									}else{ //这种是属于系统默认值得
										echo $v['params_value'];
									}
									?> </td>
								</tr>
								<?php } //判断是否停用end?>
							<?php }?>
							<tr>
								<td class="TbLeft"> 备注 </td>
								<td class="TbRight"> <?php echo $applyopen['return']['note'];?> </td>
							</tr>
						<?php }?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!--lee 内容部分 end-->