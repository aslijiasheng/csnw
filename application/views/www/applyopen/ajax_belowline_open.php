<!--lee 线下开通 start-->
<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						开通参数
						<span class="mini-title">
						Open Params
						</span>
					</div>
				</div>
				<div class="widget-body">
					<table class="leetable table table-bordered">
						<tbody>
							<?php foreach($params['open'] as $v){?>
								<?php if($v['params_visable']==1){ //这里是用来判断未启用的不显示?>
									<tr>
										<td class="TbLeft"> <?php echo $v['params_name'];?> </td>
										<td class="TbRight"> <?php echo $applyopen['open'][$v['params_code']];?> </td>
									</tr>
								<?php } //判断是否停用end?>
							<?php }?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						填写返回参数
						<span class="mini-title">
						Return Params
						</span>
					</div>
				</div>
				<div class="widget-body">
<div class="grid-view">
	<form class="form-horizontal" id="belowline-form" method="post">
		<input type="hidden" name="order_d_id" value="<?php echo $order_d_id;?>">
		<?php if(isset($params['return'])){?>
		  <?php foreach ($params['return'] as $p){?>
			<?php if($p['params_visable']==1){ //这里是用来判断未启用的不显示?>
				<?php if($p['params_type1']=='user'){ ?>
				<div class="control-group">
					<label class="control-label required" for="<?php echo $p['params_code'];?>">
						<?php echo $p['params_name'];?>
					</label>
					<div class="controls">
						<?php
						if($p['params_input_type']=='input'){
							if($p['params_code'] == 'start_date' || $p['params_code'] == 'end_date' || $p['params_code'] == 'actual_startdate' || $p['params_code'] == 'actual_enddate'){
						?>
								<div class="input-append date form_date" id="date_<?php echo $p['params_code'];?>">
									<input type="text" name="params[return][<?php echo $p['params_code'];?>]" style="width: 152px;" id="<?php echo $p['params_code'];?>">
									<span class="add-on"><i class="icon-close"></i></span>
									<span class="add-on"><i class="icon-clock"></i></span>
								</div>
								<script type="text/javascript">
									$('#date_<?php echo $p['params_code'];?>').datetimepicker({
										language:  'zh-CN',
										weekStart: 1,
										todayBtn:  1,
										autoclose: 1,
										todayHighlight: 1,
										startView: 2,
										minView: 2,
										forceParse: 0,
										format:'yyyy-mm-dd'
									});
								</script>
							<?php
							}else{
							?>
								<input type="text" name="params[return][<?php echo $p['params_code'];?>]" id="<?php echo $p['params_code'];?>">
						<?php
							}
						}
						if($p['params_input_type']=='text'){
						?>
							<input type="text" name="params[return][<?php echo $p['params_code'];?>]" id="<?php echo $p['params_code'];?>">
						<?php
						}
						if($p['params_input_type']=='radio'){
							foreach ($p['params_option'] as $k=>$op){
							?>
								<label class="radio inline">
								<input type="radio" name="params[return][<?php echo $p['params_code'];?>]" value="<?php echo $op['value'];?>" <?php if($k=='0'){echo "checked";}?>>&nbsp;<?php echo $op['name'];?>&nbsp;&nbsp;
								</label>
							<?php	
							}
						}
						if($p['params_input_type']=='checkbox'){
							foreach ($p['params_option'] as $k=>$op){
							?>
								<label class="checkbox inline">
								<input type="checkbox" name="params[return][<?php echo $p['params_code'];?>]" value="<?php echo $op['value'];?>" <?php if($k=='0'){echo "checked";}?>>&nbsp;<?php echo $op['name'];?>&nbsp;&nbsp;
								</label>
							<?php	
							}
						}
						if($p['params_input_type']=='select'){
						?>
							<select class="x-input" name="params[return][<?php echo $p['params_code'];?>]">
							<?php
							foreach ($p['params_option'] as $k=>$op){
							?>
								<option value="<?php echo $op['value'];?>" <?php if($k=='0'){echo "selected";}?> ><?php echo $op['name'];?></option>
							<?php	
							}
							?>
							</select>
						<?php
						}
						?>
					</div>
				</div>
				<?php }else{?>
				<div class="control-group">
					<label class="control-label required" for="<?php echo $p['params_code'];?>">
						<?php echo $p['params_name'];?>
					</label>
					<div class="controls">
						<input type="text" name="params[return][<?php echo $p['params_code'];?>]" id="<?php echo $p['params_code'];?>" value="<?php echo $p['params_value'];?>" readonly>
					</div>
				</div>
				<?php }?>
			<?php } //判断是否停用end?>
		  <?php }?>
		<?php }else{?>
			该产品没有设置返回参数
		<?php }?>
			<div class="control-group">
				<label class="control-label required" for="note">
					开通备注
				</label>
				<div class="controls">
					<textarea rows="6" style="width:400px" id="note" name="note"></textarea>
				</div>
			</div>
	</form>
</div>
				</div>
			</div>
		</div>
	</div>
</div>




<!--lee 内容部分 end-->