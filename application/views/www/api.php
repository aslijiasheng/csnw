<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>新营收接口文档</title>
		<style>
			html{color:#000;background:#FFF}body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,button,textarea,select,p,blockquote,th,td{margin:0;padding:0}table{border-collapse:collapse;border-spacing:0}fieldset,img{border:0}address,button,caption,cite,code,dfn,em,input,optgroup,option,select,strong,textarea,th,var{font:inherit}del,ins{text-decoration:none}li{list-style:none}caption,th{text-align:left}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal}q:before,q:after{content:''}abbr,acronym{border:0;font-variant:normal}sup{vertical-align:baseline}sub{vertical-align:baseline}legend{color:#000}
			.main{width:920px; margin:0 auto; font-size:12px;}
			h1{font-size:18px; margin-top:30px;}
			h2{font-size:16px;}
			th, td{padding:5px;}
		</style>
	</head>
	<body>
		<div class="main">
			<h1>新营收接口文档</h1>
			<ul>
				<li>接口地址 : <a href="http://192.168.35.18/index.php/api">http://192.168.35.18/index.php/api</a></li>
				<li>接口形式 : HTTP POST</li>
				<li>功能描述 : 新增</li>
			</ul>

			<h1>参数</h1>
			<table border="1">
				<tr>
					<th>名称</th>
					<th>类型</th>
					<th>是否必须</th>
					<th>描述</th>
					<th>示例</th>
				</tr>
				<tr>
					<th>user</th>
					<th>String</th>
					<th>是</th>
					<th>账号</th>
					<th>licheng</th>
				</tr>
				<tr>
					<th>pwd</th>
					<th>String</th>
					<th>是</th>
					<th>密码，暂时没加密</th>
					<th>1</th>
				</tr>
				<tr>
					<td>action</td>
					<td>String</td>
					<td>是</td>
					<td>操作方法</td>
					<td>add、select</td>
				</tr>
				<tr>
					<td>object</td>
					<td>String</td>
					<td>是</td>
					<td>对象名称</td>
					<td>order、user……</td>
				</tr>
				<tr>
					<td>data</td>
					<td>Json</td>
					<td>是</td>
					<td>操作数据</td>
					<td>……</td>
				</tr>
			</table>

			<h1>示例 PHP 销售订单 add</h1>
			<pre>
header('content-type:text/html; charset=utf-8');
include('snoopy.inc.php');

$url = 'http://192.168.35.18/index.php/api';
$snoopy = new snoopy();
$params['app_type'] = 'post';
$params['format'] = 'json';
$params['version'] = '1.0';
$params['call_times'] = 'time';
$params['return'] = 'has';
$params['user'] = 'licheng';
$params['pwd'] = '1';

$order_params = array(
	'action' => 'add',
	'object' => 'order',
	'data' => array(
        'department.name' => '直销业务中心', //所属部门
        'owner.name' => '宋雪', //所属销售 或工号 owner.gonghao=>s1137
        'account.name' => '淘宝（中国）软件有限公司', //所属客户
        'agreement_no' => 'iowkskldfjakljfl', //合同编号
        'amount' => 9999.000, //订单金额
        'finance.name' => '宋雪', //财务人员 或工号 owner.gonghao=>s1137
        'agreement_name' => 'xxx合同', //合同名称
		<!-- 'agent.name' => '1231', //所属代理商 -->
		'review.name' => '宋雪', //业务主管 或工号 owner.gonghao=>s1137
        'nreview.name' => '宋雪', //内审人员或工号 owner.gonghao=>s1137
		'if_renew.name' => '是',//是否续费订单
		'order_PassID'=>  //PassID  shopexid
		'type.name' => '销售订单',
		'detailed' => array(
			'order_d' => array(
				array('goods.code' => 'goods_0141', 'goods_amount' => 123000, 'goods_price.code' => 'gp00000000393'),
				array('goods.code' => 'group_0030', 'goods_amount' => 123000, 'goods_price.code' => 'gp00000000392')
			)
			<!-- 'salespay' => array(
				array('pay_method.name' => '支付宝', 'pay_info' => 'asdfsafsadf', 'pay_note' => '123123', 'pay_amount' => 1000.000, 'remit_bank.name' => '农业银行', 'trade_no' => 'BO20140314111452148202')
			) -->
		)
	);
);

$params = array_merge($params, $order_params);

$snoopy->submit($url,$params);
$res = $snoopy->results;
$json_arr = json_decode($res,true);
print_r($json_arr);
			</pre>
			<h2>正确 返回</h2>
			<pre>
{
    "res": "succ",
    "msg": "add success",
    "info": {
        "order_number": "soyyytetet3121313", //订单编号
        "order_agreement_no": "iowkskldfjakljfl" //合同编号
    }
}
			</pre>
			<h2>失败 返回</h2>
			<pre>
{
    "res": "fail",
    "msg": "add error _add error  订单编号已存在",
    "info": ""
}
			</pre>


			<h1>示例 PHP add 线上订单</h1>
			<pre>
header('content-type:text/html; charset=utf-8');
include('snoopy.inc.php');

$url = 'http://192.168.35.18/index.php/api';
$snoopy = new snoopy();
$params['app_type'] = 'post';
$params['format'] = 'json';
$params['version'] = '1.0';
$params['call_times'] = 'time';
$params['return'] = 'has';
$params['user'] = 'licheng';
$params['pwd'] = '1';

$order_params = array(
	'action' => 'add',
	'object' => 'order',
	'data' => array(
		'department.name' => '直销业务中心', //所属部门
		'owner.name' => '宋雪', //所属销售 或工号 owner.gonghao=>s1137
		'account.name' => '淘宝（中国）软件有限公司', //所属客户
		'agreement_no' => 'iowkskldfjakljfl', //合同编号
		'amount' => 9999.000, //订单金额
		'finance.name' => '宋雪', //财务人员 或工号 owner.gonghao=>s1137
		<!-- 'agreement_name' => 'xxx合同', //合同名称 -->
		<!-- 'agent.name' => '1231', //所属代理商 -->
		'review.name' => '宋雪', //业务主管 或工号 owner.gonghao=>s1137
		'nreview.name' => '宋雪', //内审人员或工号 owner.gonghao=>s1137
	   <!--  'if_renew.name' => '是',//是否续费订单 -->
		'order_PassID'=>  //PassID  shopexid
		'type.name' => '线上订单',
		'detailed' => array(
			'order_d' => array(
				array('goods.code' => 'goods_0141', 'goods_amount' => 123000, 'goods_price.code' => 'gp00000000393'),
				array('goods.code' => 'group_0030', 'goods_amount' => 123000, 'goods_price.code' => 'gp00000000392')
			)
			'salespay' => array(
				array('pay_method.name' => '支付宝', 'pay_info' => 'asdfsafsadf', 'pay_note' => '123123', 'pay_amount' => 1000.000, 'remit_bank.name' => '农业银行', 'trade_no' => 'BO20140314111452148202')
			)
		)
	);
);

$params = array_merge($params, $order_params);

$snoopy->submit($url,$params);
$res = $snoopy->results;
$json_arr = json_decode($res,true);
print_r($json_arr);
			</pre>
			<h2>正确 返回</h2>
			<pre>
{
    "res": "succ",
    "msg": "add success",
    "info": {
        "order_number": "soyyytetet3121313", //订单编号
        "order_agreement_no": "iowkskldfjakljfl" //合同编号
    }
}
			</pre>
			<h2>失败 返回</h2>
			<pre>
{
    "res": "fail",
    "msg": "add error _add error  订单编号已存在",
    "info": ""
}
			</pre>

			<h1>示例 PHP add 返点订单</h1>
			<pre>
header('content-type:text/html; charset=utf-8');
include('snoopy.inc.php');

$url = 'http://192.168.35.18/index.php/api';
$snoopy = new snoopy();
$params['app_type'] = 'post';
$params['format'] = 'json';
$params['version'] = '1.0';
$params['call_times'] = 'time';
$params['return'] = 'has';
$params['user'] = 'licheng';
$params['pwd'] = '1';

$order_params = array(
	'action' => 'add',
	'object' => 'order',
	'data' => array(
		'agent.name' => '1231', //所属代理商
		'department.name' => '直销业务中心', //所属部门
		'finance.name' => '宋雪', //财务人员 或工号 owner.gonghao=>s1137
		'order_PassID'=>  ,//PassID  shopexid
		'rmb_type.name'=>'返款奖励',   // 返款类型(返款奖励 or 活动经费)
		 'rmb_amount'=>100,   //申请支出金额
		 'rmb_pay_method.name'=>'银行汇款', //打款方式(银行汇款,支付宝 or 现金)
		 'rmb_bank.name'=>'工商银行（公司）', //汇款银行(工商银行（公司）,工商银行（个人）,招商银行,建设银行,农业银行,中国银行,Paypal（贝宝）)
		 'account_type.name'=> , //账户类型
		 'account_name'=> ,//账户名称
		 'account_number'=>  ,//银行账户
		 'bank_name'=> , //开户银行名称
		 'bank_country.name'=> , //开户银行所在城市
		 'payment_number'=> , //支付宝账号
		 'rmb_reason'=> , //返款原因
		 'rmb_person'=> ,  //业务审核人
		 'rmb_user'=> ,  //经手人
		 'rebates_type.name'=> , //返点订单类型(季度返点,交付返点,激励返点)
		 'order_number'=> , //关联销售订单
		 'rebates_state'=> , //返点订单状态

	);
);

$params = array_merge($params, $order_params);

$snoopy->submit($url,$params);
$res = $snoopy->results;
$json_arr = json_decode($res,true);
print_r($json_arr);
			</pre>
			<h2>正确 返回</h2>
			<pre>
{
    "res": "succ",
    "msg": "add success",
    "info": {
        "order_number": "soyyytetet3121313", //订单编号
        "order_agreement_no": "iowkskldfjakljfl" //合同编号
    }
}
			</pre>
			<h2>失败 返回</h2>
			<pre>
{
    "res": "fail",
    "msg": "add error _add error  订单编号已存在",
    "info": ""
}
			</pre>

			<h1>示例 PHP add 预收款订单</h1>
			<pre>
header('content-type:text/html; charset=utf-8');
include('snoopy.inc.php');

$url = 'http://192.168.35.18/index.php/api';
$snoopy = new snoopy();
$params['app_type'] = 'post';
$params['format'] = 'json';
$params['version'] = '1.0';
$params['call_times'] = 'time';
$params['return'] = 'has';
$params['user'] = 'licheng';
$params['pwd'] = '1';

$order_params = array(
	'action' => 'add',
	'object' => 'order',
	'data' => array(
		'department.name' => '直销业务中心', //所属部门
		'owner.name' => '宋雪', //所属销售 或工号 owner.gonghao=>s1137
		'agent.name' => '1231', //所属代理商
		<!-- 'account.name' => '淘宝（中国）软件有限公司', //所属客户 -->
	   <!--  'agreement_no' => 'iowkskldfjakljfl', //合同编号 -->
		'amount' => 9999.000, //订单金额
		'finance.name' => '宋雪', //财务人员 或工号 owner.gonghao=>s1137
		<!-- 'agreement_name' => 'xxx合同', //合同名称 -->
		'transfer_name'=> , //款项名称
		'order_PassID'=>  //PassID  shopexid
		'customer_type'=> //客户类型 个人理解:(企业客户，个人客户，经销商)
		'state.name'=> , //订单状态
		'type.name' => '预收款订单',
		'detailed' => array(
			'salespay' => array(
				array('pay_date'=>'2014-03-30','remit_bank'=>'招商银行','pay_amount' => 1000.000,'pay_method.name' => '支付宝', 'pay_info' => 'asdfsafsadf', 'pay_note' => '123123', 'remit_bank.name' => '农业银行', 'trade_no' => 'BO20140314111452148202') //此处字段貌似有问题
			)
		)
		'deposit_paytype.name'=>  , //款项类型(保证金,预收款,货款,退预收款,退保证金)
	);
);

$params = array_merge($params, $order_params);

$snoopy->submit($url,$params);
$res = $snoopy->results;
$json_arr = json_decode($res,true);
print_r($json_arr);
			</pre>
			<h2>正确 返回</h2>
			<pre>
{
    "res": "succ",
    "msg": "add success",
    "info": {
        "order_number": "soyyytetet3121313", //订单编号
        "order_agreement_no": "iowkskldfjakljfl" //合同编号
    }
}
			</pre>
			<h2>失败 返回</h2>
			<pre>
{
    "res": "fail",
    "msg": "add error _add error  订单编号已存在",
    "info": ""
}
			</pre>

			<h1>示例 PHP add 提货销售订单</h1>
			<pre>
header('content-type:text/html; charset=utf-8');
include('snoopy.inc.php');

$url = 'http://192.168.35.18/index.php/api';
$snoopy = new snoopy();
$params['app_type'] = 'post';
$params['format'] = 'json';
$params['version'] = '1.0';
$params['call_times'] = 'time';
$params['return'] = 'has';
$params['user'] = 'licheng';
$params['pwd'] = '1';

$order_params = array(
	'action' => 'add',
	'object' => 'order',
	'data' => array(
		'department.name' => '直销业务中心', //所属部门
		'owner.name' => '宋雪', //所属销售 或工号 owner.gonghao=>s1137
		'account.name' => '淘宝（中国）软件有限公司', //所属客户
		'agent.name' => '1231', //所属代理商
		'amount' => 9999.000, //订单金额
		'finance.name' => '宋雪', //财务人员 或工号 owner.gonghao=>s1137
		'review.name' => '宋雪', //业务主管 或工号 owner.gonghao=>s1137
		'order_PassID'=>  //PassID  shopexid
		'type.name' => '提货销售订单',
		'detailed' => array(
			 'salespay' => array(
				array('pay_date'=>'2014-03-30','pay_method.name' => '支付宝','status.name'=>'未确认到帐','sp_date'=>'2014-04-13')
			)
			'order_d' => array(
				array('goods.code' => 'goods_0141', 'goods_amount' => 123000, 'goods_price.code' => 'gp00000000393'),
				array('goods.code' => 'group_0030', 'goods_amount' => 123000, 'goods_price.code' => 'gp00000000392')
			)

		)
	);
);

$params = array_merge($params, $order_params);

$snoopy->submit($url,$params);
$res = $snoopy->results;
$json_arr = json_decode($res,true);
print_r($json_arr);
			</pre>
			<h2>正确 返回</h2>
			<pre>
{
    "res": "succ",
    "msg": "add success",
    "info": {
        "order_number": "soyyytetet3121313", //订单编号
        "order_agreement_no": "iowkskldfjakljfl" //合同编号
    }
}
			</pre>
			<h2>失败 返回</h2>
			<pre>
{
    "res": "fail",
    "msg": "add error _add error  订单编号已存在",
    "info": ""
}
			</pre>

			<div>
				{"res":true,"msg":"add Success","atime":"2014-04-17 12:07:29"}
			</div>
			<hr/>
			<h1>示例 PHP select</h1>
			<pre>
header('content-type:text/html; charset=utf-8');
include('snoopy.inc.php');

$url = 'http://192.168.35.18/index.php/api';
$snoopy = new snoopy();
$params['app_type'] = 'post';
$params['format'] = 'json';
$params['version'] = '1.0';
$params['call_times'] = 'time';
$params['return'] = 'has';

$order_params = array(
	'action' => 'select',
	'object' => 'order',
	'data' => array(
		'where_all' => array(
			's1' => array(
				[attr] => 'type_id',
				[value] => 6,
				[action] => '='),
			's2' => array(
				[attr] => 'number',
				[value] => 'so201404170002',
				[action] => 'like')
		),
		'rel_all' => '(ga1) and (ga2)',
		'page' => 1,
		'perNumber' => 20,
		'obj' => 'order'
	)
);

$params = array_merge($params, $order_params);

$snoopy->submit($url,$params);
$res = $snoopy->results;
$json_arr = json_decode($res,true);
print_r($json_arr);
			</pre>

			<h2>select 返回</h2>
			<pre>
Array
(
    [0] => Array
        (
            [order_id] => 3
            [order_number] => so201404170002
            [order_create_user_id] =>
            [order_create_time] => 2014-03-31 13:57:56
            [order_department] => 509
            [order_owner] => 1341
            [order_account] => 66
            [order_agent] => 4
            [order_agreement_no] => iowkskldfjakljfl
            [order_amount] => 9999.000
            [order_finance] => 1341
            [order_agreement_name] => xxx合同
            [order_review] => 1341
            [order_nreview] => 1341
            [order_if_renew] => 1002
            [type_id] => 6
            [order_rebate_amount] =>
            [order_cancel_node] => 0
            [order_relation_order_id] =>
            [order_transfer_name] =>
            [order_change_out] =>
            [order_change_into] =>
            [order_out_examine] =>
            [order_change_into_money] =>
            [order_transfer_state] =>
            [order_transfer_date] =>
            [order_is_cancel] => 1001
            [order_PassID] =>
            [order_customer_type] =>
            [order_rmb_type] =>
            [order_rmb_amount] =>
            [order_rmb_handlingcharge] =>
            [order_rmb_pay_method] =>
            [order_rmb_bank] =>
            [order_state] =>
            [order_account_type] =>
            [order_account_name] =>
            [order_account_number] =>
            [order_bank_name] =>
            [order_bank_country] =>
            [order_payment_number] =>
            [order_rmb_reason] =>
            [order_rmb_person] =>
            [order_rmb_user] =>
            [order_rmb_finance_id] =>
            [order_prepay_company] =>
            [order_sale_source] =>
            [order_transfer_reason] =>
            [order_rebates_type] =>
            [order_relation_order_all] =>
            [order_rebates_state] =>
            [order_rebates_remark] =>
            [order_deposit_remark] =>
            [order_deposit_paytype] =>
            [order_source] =>
            [order_department_arr] => Array
                (
                    [department_id] => 509
                    [department_name] => 直销业务中心
                    [department_uid] => 0
                    [department_code] => ZHIXIAO
                    [department_treepath] => 0000,0509
                    [department_treelevel] => 1
                )

            [order_owner_arr] => Array
                (
                    [user_id] => 1341
                    [user_name] => 宋雪
                    [user_sex] => 1002
                    [user_password] => 1
                    [user_email] => songxue@shopex.cn
                    [user_department] => 85
                    [user_gonghao] => s1137
                    [user_duty_name] => 0
                    [user_status] => 1002
                    [user_login_name] => songxue
                )

            [order_account_arr] => Array
                (
                    [account_id] => 66
                    [account_name] => 淘宝（中国）软件有限公司
                    [account_leader] => 咏苍
                    [account_update_time] => 2012-08-02 07:11:24
                    [account_account_shopexid] => 331111096984
                    [account_create_time] => 2012-08-02 07:11:24
                    [account_telephone] => 13472517774
                    [type_id] => 0
                    [account_email] => yongcang@taobao.com
                    [account_department] => 509
                )

            [order_agent_arr] => Array
                (
                    [partner_id] => 4
                    [partner_name] => 1231
                    [partner_shopex_id] => 11121212
                    [partner_department] => 3
                    [partner_manager] => 1231231
                    [partner_level] => 1002
                    [partner_disc] => 123123
                    [partner_CooperativeTime] => 2014-03-31 13:40:19
                    [partner_endingdate] => 2014-03-31 13:40:21
                    [partner_contacts] => 12312
                    [partner_email] => 3123
                    [partner_address] => 12321312
                    [partner_fax] => 312312
                    [partner_region] => 3213
                    [partner_contract] => 123123
                )

            [order_finance_arr] => Array
                (
                    [user_id] => 1341
                    [user_name] => 宋雪
                    [user_sex] => 1002
                    [user_password] => 1
                    [user_email] => songxue@shopex.cn
                    [user_department] => 85
                    [user_gonghao] => s1137
                    [user_duty_name] => 0
                    [user_status] => 1002
                    [user_login_name] => songxue
                )

            [order_review_arr] => Array
                (
                    [user_id] => 1341
                    [user_name] => 宋雪
                    [user_sex] => 1002
                    [user_password] => 1
                    [user_email] => songxue@shopex.cn
                    [user_department] => 85
                    [user_gonghao] => s1137
                    [user_duty_name] => 0
                    [user_status] => 1002
                    [user_login_name] => songxue
                )

            [order_nreview_arr] => Array
                (
                    [user_id] => 1341
                    [user_name] => 宋雪
                    [user_sex] => 1002
                    [user_password] => 1
                    [user_email] => songxue@shopex.cn
                    [user_department] => 85
                    [user_gonghao] => s1137
                    [user_duty_name] => 0
                    [user_status] => 1002
                    [user_login_name] => songxue
                )

            [order_if_renew_arr] => Array
                (
                    [enum_id] => 139
                    [attr_id] => 227
                    [enum_name] => 是
                    [enum_key] => 1002
                    [disp_order] => 0
                    [is_default] => 1
                    [system_flag] => 0
                )

            [order_is_cancel_arr] => Array
                (
                    [enum_id] => 441
                    [attr_id] => 466
                    [enum_name] => 未作废
                    [enum_key] => 1001
                    [disp_order] => 0
                    [is_default] => 1
                    [system_flag] => 0
                )

            [ydz_amount] => 0
        )
)
			</pre>
		</div>
	</body>
</html>