<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="100">操作</th>
			<th><?php echo $labels['timing_task_do_time']; ?></th>
			<th><?php echo $labels['timing_task_next_time']; ?></th>
			<th><?php echo $labels['timing_task_url']; ?></th>
			<th><?php echo $labels['timing_task_note']; ?></th>
			<th><?php echo $labels['timing_task_status']; ?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($listData as $listData_value) { ?>
			<tr>
				<td style="font-size:16px;">
					<a href="<?php echo site_url('www/timing_task/update') . '?timing_task_id=' . $listData_value['timing_task_id'] ?>" rel="tooltip" title="更新"><span class="icon-pencil"></span></a>
					<a href="<?php echo site_url('www/timing_task/del') . '?timing_task_id=' . $listData_value['timing_task_id'] ?>" rel="tooltip" title="删除"><span class="icon-remove-2"></span></a>
					<a href="<?php echo site_url('www/timing_task/do_status') . '?timing_task_id=' . $listData_value['timing_task_id'] ?>" rel="tooltip" title="启用/停用"><?php echo $listData_value['timing_task_status'] == '1001' ? '<span class="icon-history"></span>' : '<span class="icon-alarm-2"></span>'; ?></a>
				</td>

				<td>
					<?php echo $listData_value['timing_task_do_time']; ?>
				</td>

				<td>
					<?php echo $listData_value['timing_task_url']; ?>
				</td>

				<td>
					<?php echo $listData_value['timing_task_note']; ?>
				</td>

				<td>
					<?php echo $listData_value['timing_task_status_arr']['enum_name']; ?>
				</td>
			</tr>
		<?php } ?>
	</tbody>

</table>
<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber; ?>条</div>
<input type='hidden' id='totalNumber' value="<?php echo $totalNumber; ?>">
</div>