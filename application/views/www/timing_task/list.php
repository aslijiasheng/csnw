<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						定时任务
						<span class="mini-title">
							timing_task
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
							<a id="newtiming_task" class="btn btn-primary" href="<?php echo site_url('www/timing_task/add') ?>">
								<i class="icon-file"></i>
								新增定时任务
							</a>
						</div>
						<div id="divmessagelist">
							<div class="grid-view">
								<div id="timing_task_list"></div>
								<div>
									<script type="text/javascript">
										$(document).ready(function() {
											$selectAttr = [
												{'value': 'do_time', 'txt': '执行时间'},
												{'value': 'url', 'txt': '链接'},
												{'value': 'note', 'txt': '说明'},
												{'value': 'status', 'txt': '状态'},
												{'value': 'name', 'txt': '名称'},
												{'value': 'param', 'txt': '参数'},
											];
											$("#timing_task_list").leeDataTable({
												selectAttr: $selectAttr, //简单查询的查询属性
												url: "<?php echo site_url('www/timing_task/ajax_select'); ?>", //ajax查询的地址
												perNumber: 5 //每页显示多少条数据
											});
										});
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

