       <div class="control-group">
			<label class="control-label required" for="invoice_reviewer">
				<?php echo $labels["invoice_reviewer"];?>
				<span class="required">*</span>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="invoice[invoice_reviewer]" id="invoice_reviewer" value="<?php echo $listData['invoice_content']['invoice_reviewer'] ?>">
			</div>
		</div>
			<div class="control-group">
							<label class="control-label required" for="invoice_type">
								<?php echo $labels["invoice_type"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php foreach($invoice_type_enum as $type_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $type_v['enum_key']==$id_aData['invoice_type_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $type_v['enum_key']?>" name="invoice[invoice_type]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $type_v['enum_key']?>" name="invoice[invoice_type]">
<?php }?>
<?php echo $type_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
                        <div class="control-group" id="invoice_type_2">
							<label class="control-label required" for="invoice_title">
								<?php echo $labels["invoice_title"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="invoice_type_2[invoice_title]" value="<?php echo $id_aData['invoice_title'] ?>"  id="invoice_title">
<?php }else{ ?>
<input type="text" maxlength="128" name="invoice_type_2[invoice_title]" id="invoice_title">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div id="invoice_type_1">
						<div class="control-group">
							<label class="control-label required" for="invoice_customer_type">
								<?php echo $labels["invoice_customer_type"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php foreach($invoice_customer_type_enum as $customer_type_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $customer_type_v['enum_key']==$id_aData['invoice_customer_type_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $customer_type_v['enum_key']?>" name="invoice_type_1[invoice_customer_type]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $customer_type_v['enum_key']?>" name="invoice_type_1[invoice_customer_type]">
<?php }?>
<?php echo $customer_type_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
                     
                     <div class="customer_2">
                     	<div class="control-group" >
							<label class="control-label required" for="invoice_title">
								<?php echo $labels["invoice_title"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_2[invoice_title]" value="<?php echo $id_aData['invoice_title'] ?>" id="invoice_title">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_2[invoice_title]" id="invoice_title">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="invoice_taxpayer">
								<?php echo $labels["invoice_taxpayer"];?>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_2[invoice_taxpayer]" value="<?php echo $id_aData['invoice_taxpayer'] ?>" id="invoice_taxpayer">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_2[invoice_taxpayer]" id="invoice_taxpayer">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="invoice_invoice_no">
								<?php echo $labels["invoice_invoice_no"];?>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_2[invoice_invoice_no]" value="<?php echo $id_aData['invoice_invoice_no'] ?>" id="invoice_invoice_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_2[invoice_invoice_no]" id="invoice_invoice_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
					
						
			
						
						<div class="control-group">
							<label class="control-label required" for="invoice_bank">
								<?php echo $labels["invoice_bank"];?>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_2[invoice_bank]" value="<?php echo $id_aData['invoice_bank'] ?>" id="invoice_bank">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_2[invoice_bank]" id="invoice_bank">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="invoice_tel">
								<?php echo $labels["invoice_tel"];?>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_2[invoice_tel]" value="<?php echo $id_aData['invoice_tel'] ?>" id="invoice_tel">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_2[invoice_tel]" id="invoice_tel">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="invoice_address">
								<?php echo $labels["invoice_address"];?>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_2[invoice_address]" value="<?php echo $id_aData['invoice_address'] ?>" id="invoice_address">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_2[invoice_address]" id="invoice_address">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="invoice_memo">
								<?php echo $labels["invoice_memo"];?>
							</label>
							<div class="controls">
						
								<?php if(isset($id_aData)){ ?>
<textarea name="customer_2[invoice_memo]" id="" cols="100" rows="5"  ></textarea>
<?php }else{ ?>
<textarea name="customer_2[invoice_memo]" id="" cols="100" rows="5" ></textarea>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						 </div>	
                     </div>
					<div class="customer_1">
						<div class="control-group" >
							<label class="control-label required" for="invoice_title">
								<?php echo $labels["invoice_title"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_1[invoice_title]" value="<?php echo $id_aData['invoice_title'] ?>" id="invoice_title">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_1[invoice_title]" id="invoice_title">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="invoice_taxpayer">
								<?php echo $labels["invoice_taxpayer"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_1[invoice_taxpayer]" value="<?php echo $id_aData['invoice_taxpayer'] ?>" id="invoice_taxpayer">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_1[invoice_taxpayer]" id="invoice_taxpayer">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="invoice_invoice_no">
								<?php echo $labels["invoice_invoice_no"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_1[invoice_invoice_no]" value="<?php echo $id_aData['invoice_invoice_no'] ?>" id="invoice_invoice_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_1[invoice_invoice_no]" id="invoice_invoice_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
				
						
					
						
						<div class="control-group">
							<label class="control-label required" for="invoice_bank">
								<?php echo $labels["invoice_bank"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_1[invoice_bank]" value="<?php echo $id_aData['invoice_bank'] ?>" id="invoice_bank">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_1[invoice_bank]" id="invoice_bank">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="invoice_tel">
								<?php echo $labels["invoice_tel"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_1[invoice_tel]" value="<?php echo $id_aData['invoice_tel'] ?>" id="invoice_tel">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_1[invoice_tel]" id="invoice_tel">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="invoice_address">
								<?php echo $labels["invoice_address"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_1[invoice_address]" value="<?php echo $id_aData['invoice_address'] ?>" id="invoice_address">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_1[invoice_address]" id="invoice_address">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="invoice_memo">
								<?php echo $labels["invoice_memo"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
						
								<?php if(isset($id_aData)){ ?>
<textarea name="customer_1[invoice_memo]" id="" cols="100" rows="5" maxlength="128" ></textarea>
<?php }else{ ?>
<textarea name="customer_1[invoice_memo]" id="" cols="100" rows="5" maxlength="128" >
	
</textarea>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
					</div>	
                     </div>
					</div>	

  <script type="text/javascript">
   $(document).ready(function(){
   	   $("#invoice_type_2").hide();
   	   $(".customer_2").hide();
   	    $("[name='invoice[invoice_type]']").change(function(){
   	       //checked = $(this).attr('checked');
		   val = $(this).val();
		   if(val==1001){
               $("#invoice_type_1").show();
                $("#invoice_type_2").hide();
		   }else{
		   	  $("#invoice_type_2").show();
              $("#invoice_type_1").hide();

		   }
        })

        $("[name='invoice_type_1[invoice_customer_type]']").change(function(){
  			val = $(this).val();
  			if(val==1001){
  			  $(".customer_1").hide();	
              $(".customer_2").show();
  			}else{
               $(".customer_1").show();	
              $(".customer_2").hide();
  			}
        })
   })
  </script>                