<style>
	.confirm_invoice_table th, .confirm_invoice_table td{
     border-top-style: none;
}
</style>
<div class="grid-view">
	<form class="form-horizontal" id="verify_invoice_form">
	<input type="hidden" name="invoice[invoice_id]" value="<?php echo $listData['invoice_id'] ?>">
	<input type="hidden" name="invoice[invoice_order_id]" value="<?php echo $listData['invoice_order_id']?>">
	<input type="hidden" name="order_finance" value="<?php echo $listData['invoice_reviewer'] ?>">
	<input type="hidden" name="order_finance_department" value='<?php echo $listData['invoice_content']['department'] ?>'>

		<div class="goods_content_info" <?php if(!isset($listData['invoice_content']['invoice_content_class']) or $listData['invoice_content']['invoice_content_class']==1001){ echo 'style="display:none"';} ?>>
					<table class="table table-bordered">
						<tbody>

							<tr <?php if(!isset($listData['invoice_content']['service_charge'])){ echo 'style="display:none"';} ?>>
							    <?php if(isset($listData['invoice_content']['service_charge'])){ ?>
								<td></td>
								<?php } ?>

								<td>服务费</td>
								<td><?php echo $listData['invoice_content']['other_price'][1001]; ?></td>
							</tr>
							<tr id='invoice_other_content_1001' <?php if(empty($listData['invoice_content']['service_select'])){ echo '"style="display:none"';} ?>>
								<td></td>


                               <td colspan="2">
								<?php if(isset($order_data['detailed']['order_d_goods_arr'])){foreach($order_data['detailed']['order_d_goods_arr'] as $goods_arr): ?>
									<?php if(isset($listData['invoice_content']['service_select'][$goods_arr['order_d_goods_code']])){ ?>

										<label class="checkbox">
											 <?php echo $goods_arr['order_d_goods_name']; ?>
										</label>

									<?php } ?>
								<?php endforeach ?>
								<?php } ?>
								</td>

							</tr>

							<tr <?php if(!isset($listData['invoice_content']['net_service_charge'])){ echo 'style="display:none"';} ?>>
							    <?php if(isset($listData['invoice_content']['service_charge'])){ ?>
								<td></td>
								<?php } ?>
								<td>网络服务费</td>
								<td><?php echo $listData['invoice_content']['other_price'][1002]; ?></td>
							</tr>
							<tr id='invoice_other_content_1002' <?php if(!isset($listData['invoice_content']['net_service_charge'])){ echo 'style="display:none"';} ?>>
								<td></td>

                                <td colspan="2">
								<?php if(isset($order_data['detailed']['order_d_goods_arr'])){ foreach($order_data['detailed']['order_d_goods_arr'] as $goods_arr): ?>
									<?php if(isset($listData['invoice_content']['net_service_select'][$goods_arr['order_d_goods_code']])){ ?>

										<label class="checkbox">
											 <?php echo $goods_arr['order_d_goods_name'];?>
										</label>

									<?php } ?>
								<?php endforeach ?>
								<?php } ?>
								</td>

							</tr>
						</tbody>
					</table>
				</div>

                 <div class="group-text" <?php if(!isset($listData['invoice_content']['invoice_content_class']) or $listData['invoice_content']['invoice_content_class']==1002 or $listData['invoice_type']==1002){ echo 'style="display:none"';} ?>>
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th>商品名称</th>
								<th>折后价</th>
								<th>本次申请开票金额</th>
							</tr>

								<?php $i=1; ?>
								<?php if(isset($order_data['detailed']['order_d_goods_arr'])){ foreach($order_data['detailed']['order_d_goods_arr'] as $goods_arr): ?>
								<tr>
									<td><?php echo $goods_arr['order_d_goods_name']; ?></td>
									<td><?php echo money($goods_arr['order_d_product_basic_primecost']); ?></td>
									<td><?php echo $listData['invoice_content']['invoice_price'][$i]; ?></td>
								</tr>
							<?php $i++;  endforeach ?>
							<?php } ?>
					</tbody>
					</table>
				</div>
	  <table class="table confirm_invoice_table" >
					<tbody>
						<tr style="">
							<th width="15%">票据抬头：</th>
							<td width="35%"><?php echo $listData['invoice_title']; ?></td>
							<th width="15%">开票类型：</th>
							<td width="35%"><?php echo $listData['invoice_type_arr']['enum_name'];  ?></td>
						</tr>
						<tr>
							<th>业务总监：</th>
							<td> <?php echo $listData['invoice_content']['invoice_reviewer']; ?></td>
							<th>审核状态：</th>
							<td><?php $listData['invoice_process_status_arr']['enum_name'];  ?></td>
						</tr>
						<tr>
							<th>客户类型：</th>
							<td><?php echo $listData['invoice_customer_type_arr']['enum_name']; ?></td>
							<th>纳税人识别号：</th>
							<td><?php echo $listData['invoice_taxpayer']; ?></td>
						</tr>
						<tr>
							<th>地址：</th>
							<td><?php echo $listData['invoice_address']; ?></td>
							<th>电话：</th>
							<td><?php echo $listData['invoice_tel']; ?></td>
						</tr>
						<tr>
							<th>开户行：</th>
							<td><?php echo $listData['invoice_bank']; ?></td>
							<th><label for="invoice_process_time">审批日期：</label></th>
							<td>
								<div id="salespay_pay_date" class="input-append date form_date">

									<input type="text" value="" style="width: 154px;" id='invoice_process_time' name="invoice[invoice_process_time]" required="required" title="审批日期">
									<span class="add-on"><i class="icon-close"></i></span>
									<span class="add-on"><i class="icon-clock"></i></span>
								</div>
								<script type="text/javascript">
									$(document).ready(function () {
										$('#salespay_pay_date').datetimepicker({
									        language:  'zh-CN',
									        weekStart: 1,
									        todayBtn:  1,
											autoclose: 1,
											todayHighlight: 1,
											startView: 2,
											minView: 2,
											forceParse: 0,
											format:'yyyy-mm-dd'
									    });
									});
								</script>
								<span class="help-inline"></span>
							</td>
						</tr>
						<tr>
							<th>开票金额：</th>
							<td><?php echo $listData['invoice_amount'] ?></td>
							<th>开票状态：</th>
							<td><?php echo $listData['invoice_do_status_arr']['enum_name']; ?></td>
						</tr>


						<tr>
							<th><label for="invoice_process_note">审批备注：</label></th>

							<td colspan="3">
								<textarea name="invoice[invoice_process_note]" rows="5" id="invoice_process_note" class="span6" required="required" title="审批备注"></textarea>
							</td>
						</tr>
					</tbody>
				</table>
	</form>
</div>