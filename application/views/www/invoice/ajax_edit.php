<!--lee 内容部分 start-->
<div class="grid-view">

	<form class="form-horizontal" id="invoice-form" action="" method="post">
		<!-- 用一个隐藏文本框来存order_id -->
		<input size="16" type="hidden" name="invoice[invoice_order_id]" value='<?php echo $order_data['order_id'];?>'>
		<input size="16" type="hidden" name="invoice[invoice_id]" value='<?php echo $listData['invoice_id'];?>'>
		<input type="hidden" name="order_finance" value="<?php echo $listData['invoice_reviewer'] ?>">
		<input type="hidden" name="order_finance_department" value='<?php echo $order_data['order_finance_arr']['user_department']; ?>'>
		
		
		
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				基本信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text1">
					<table class="table table-bordered">
						<tbody><tr>
					<td width="12%">
						客户名称：
					</td>
					<td width="38%">
					  <?php if(!empty($account_data)){ echo $invoice_data['account_name'];} ?>
					</td>
					<td width="12%">
						企业ID：
					</td>
					<td width="38%">
					  <?php if(!empty($account_data)){ echo $invoice_data['account_id'];} ?>
					</td>
				</tr>
				<tr>
					<td>
						订单号：
					</td>
					<td>
					  <?php echo $order_data['order_number']; ?>
					</td>
					<td>
						订单金额：
					</td>
					<td>
						<?php echo number_format($order_data['order_amount'],2,'.',''); ?>
					</td>
				</tr>
				<tr>
					<td>
						已申请开票金额：
					</td>
					<td>
						<?php echo $listData['invoice_content']['invoice_value']; ?>
						<input type="hidden" name="invoice[invoice_value]" value="<?php  echo $listData['invoice_content']['invoice_value']?>">
					</td>
					<td>
						剩余开票金额：
					</td>
					<td>
						<?php  echo number_format($listData['invoice_content']['odd_invoice_value'],2,'.','');?>
						<input type="hidden" name="invoice[odd_invoice_value]" value="<?php  echo $listData['invoice_content']['odd_invoice_value']?>">
					</td>
				</tr>
			</tbody>
					</table>
				</div>
			</div>
		</div>

       <div id="ff">
		<!--新增所属子公司 2014/8/25-->			
			<div class="control-group">
				<label class="control-label required" for="invoice_subsidiary">
					<?php echo $labels["invoice_subsidiary"]; ?>
					<span class="required">*</span>
				</label>
				<div class="controls">
					<?php foreach($invoice_subsidiary_enum as $type_v){ ?>
					<label class="radio inline">
					<?php if($type_v['enum_key']==$listData['invoice_content']['invoice_subsidiary']){ ?>
						<input id="inlineRadioB" type="radio" value="<?php echo $type_v['enum_key']?>" name="invoice[invoice_subsidiary]" checked>
					<?php }else{ ?>
						<input id="inlineRadioB" type="radio" value="<?php echo $type_v['enum_key']?>" name="invoice[invoice_subsidiary]">
					<?php }?>
					<?php echo $type_v['enum_name'];?>
					</label>
					<?php } ?>
					<span class="help-inline"></span>
				</div>
			</div>
		<!--新增所属子公司 2014/8/25-->
				<div class="control-group" <?php if($listData['invoice_kp_type'] !=2){ echo 'style="display:none"';} ?>>
					<label class="control-label required" for="invoice_reviewer">
						业务负责人
					</label>
				<div class="controls">
					<input type="text" name="invoice[invoice_reviewer]" id="invoice_reviewer" <?php if($listData['invoice_kp_type'] ==2){ echo 'required="required"';} ?> title="业务负责人" value_id ="<?php echo $listData['invoice_content']['invoice_reviewer_id'] ?>" value="<?php echo $listData['invoice_content']['invoice_reviewer'] ?>">
					<script type="text/javascript">
					$(document).ready(function () {
						$('#invoice_reviewer').leeQuote({
							url:'<?php echo site_url('www/user/ajax_list?tag_name=invoice_reviewer');?>',
							title:'选择主管人员',
							data:{
								'rel_role_id':10,
							}
						});
					});
					</script>
				</div>
			</div>

			<div class="control-group">
							<label class="control-label required" for="invoice_type">
								<?php echo $labels["invoice_type"]; //票据类型?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php foreach($invoice_type_enum as $type_v){ ?>
<label class="radio inline">
<?php if($type_v['enum_key']==$listData['invoice_content']['invoice_type']){ ?>
	<input id="inlineRadioB" type="radio" value="<?php echo $type_v['enum_key']?>" name="invoice[invoice_type]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio" value="<?php echo $type_v['enum_key']?>" name="invoice[invoice_type]">
<?php }?>
<?php echo $type_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>



					<div id="invoice_type_2">
                        <div class="control-group"  <?php if($listData['invoice_type']==1001){ echo 'style="display:none;"';} ?>>
							<label class="control-label required" for="invoice_title">
								<?php echo $labels["invoice_title"];?>
							</label>
							<div class="controls">

						<input type="text" maxlength="128" name="invoice_type_2[invoice_title]" class="isrequired" required="required" title="<?php echo $labels["invoice_title"];?>" value="<?php echo $listData['invoice_title'] ?>"  id="invoice_title">

								<span class="help-inline"></span>
							</div>
						</div>

						  <div class="control-group">
                      	<label class="control-label required" for="invoice_amount">
								本次申请开票金额
								<span class="required">*</span>
							</label>
							<div class="controls">
<input type="text" maxlength="128" name="invoice_type_2[invoice_amount]" required="required" class="isrequired" id="invoice_amount" title="本次申请开票金额" value="<?php echo $listData['invoice_amount']; ?>">

								<span class="help-inline"></span>
							</div>
                      </div>

                        <div class="control-group">
                      	<label class="control-label required" for="invoice_receipt_content">
								收据内容
								<span class="required">*</span>
							</label>
							<div class="controls">
<textarea name="invoice_type_2[invoice_receipt_content]" id="invoice_receipt_content"  title="收据内容" style="width:400px;" cols="100" rows="5">
<?php echo $listData['invoice_receipt_content']; ?>
</textarea>
								<span class="help-inline"></span>
							</div>
                      </div>
				  </div>
						<div id="invoice_type_1" <?php if($listData['invoice_type']==1002){ echo 'style="display:none;"';} ?>>
						<div class="control-group">
							<label class="control-label required" for="invoice_customer_type">
								<?php echo $labels["invoice_customer_type"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php foreach($invoice_customer_type_enum as $customer_type_v){ ?>
<label class="radio inline">
<input id="inlineRadioB" type="radio" value="<?php echo $customer_type_v['enum_key']?>" name="invoice_type_1[invoice_customer_type]" <?php if($listData['invoice_customer_type']==$customer_type_v['enum_key']){ echo 'checked="checked"';} ?>>

<?php echo $customer_type_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						 <div class="control-group">
			<label style="width: 170px;" class="control-label">开票内容分类 <?php echo $listData['invoice_content']['invoice_content_class']; ?></label>
			<div class="controls">
			<select name="invoice[invoice_content_class]" id="invoice_content_class">
				<?php foreach($invoice_content_class_enum as $invoice_content_class_v){ ?>
				   <?php if($invoice_content_class_v['enum_key'] == $listData['invoice_content']['invoice_content_class']){ ?>
					<option value="<?php echo $invoice_content_class_v['enum_key'];?>" selected="selected">
						<?php echo $invoice_content_class_v['enum_name'];?>
					</option>
					<?php }else{ ?>
					  <option value="<?php echo $invoice_content_class_v['enum_key'];?>">
						<?php echo $invoice_content_class_v['enum_name'];?>
					</option>
					<?php } ?>
				<?php } ?>
			</select>
		  </div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				开票内容
				<span class="required"></span>
			</label>
			<div class="controls">
<div class="goods_content_info" <?php if($listData['invoice_content']['invoice_content_class']==1001){ echo 'style="display:none"';} ?>>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width='20'></th>
				<th>商品名称</th>
				<th>本次申请开票金额</th>
			</tr>
		</thead>
		<tbody>


			<tr>
			    <?php if(isset($listData['invoice_content']['service_charge'])){ ?>
				<td><input type="checkbox" value="1001" id="" name="invoice[service_charge]" checked="checked"></td>
				<?php }else{ ?>
				<td><input type="checkbox" value="1001" id="" name="invoice[service_charge]" checked="checked"></td>
				<?php } ?>
				<td>服务费</td>
				<td><input type="text" name="invoice[other_price][1001]" value="<?php echo $listData['invoice_content']['other_price'][1001]; ?>"></td>
			</tr>
			<tr id='invoice_other_content_1001' <?php if(!isset($listData['invoice_content']['service_charge'])){ echo '"style="display:none"';} ?>>
				<td></td>
				<td colspan="2">

				<?php if(isset($order_data['detailed']['order_d_goods_arr'])){foreach($order_data['detailed']['order_d_goods_arr'] as $goods_arr): ?>
					<label class="checkbox">
						<input type="checkbox"  name="service_select[<?php echo $goods_arr['order_d_goods_code']; ?>]" value="1" <?php if(isset($listData['invoice_content']['service_select'][$goods_arr['order_d_goods_code']])){ echo 'checked="checked"';} ?>>
						<?php echo $goods_arr['order_d_goods_name']; ?>
					</label>
				<?php endforeach ?>
				<?php } ?>

				</td>
			</tr>

			<tr>
			    <?php if(isset($listData['invoice_content']['net_service_charge'])){ ?>
				<td><input type="checkbox" value="1002" id="" name="invoice[net_service_charge]" checked="checked"></td>
				<?php }else{ ?>
				<td><input type="checkbox" value="1002" id="" name="invoice[net_service_charge]"></td>
				<?php }?>
				<td>网络服务费</td>
				<td><input type="text" name="invoice[other_price][1002]" value="<?php echo $listData['invoice_content']['other_price'][1002]; ?>"></td>
			</tr>
			<tr id='invoice_other_content_1002' <?php if(!isset($listData['invoice_content']['net_service_charge'])){ echo 'style="display:none"';} ?>>
				<td></td>
				<td colspan="2">

				<?php foreach($order_data['detailed']['order_d_goods_arr'] as $goods_arr): ?>
					<label class="checkbox">
						<input type="checkbox"  name="net_service_select[<?php echo $goods_arr['order_d_goods_code']; ?>]" value="1" <?php if(isset($listData['invoice_content']['net_service_select'][$goods_arr['order_d_goods_code']])){ echo 'checked="checked"';} ?>>
						<?php echo $goods_arr['order_d_goods_name']; ?>
					</label>
				<?php endforeach ?>

				</td>
			</tr>
		</tbody>
	</table>
</div>
				<div class="group-text" <?php if($listData['invoice_content']['invoice_content_class']==1002){ echo 'style="display:none"';} ?>>
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th>商品名称</th>
								<th>折后价</th>
								<th>本次申请开票金额</th>
							</tr>

								<?php $i=1; ?>
								<?php foreach($order_data['detailed']['order_d_goods_arr'] as $goods_arr): ?>
								<tr>
									<td><?php echo $goods_arr['order_d_goods_name']; ?><input type="hidden" value="<?php echo $goods_arr['order_d_goods_name']; ?>" name="invoice[goods_name][<?php echo $i; ?>]" class="span1" value="<?php echo $goods_arr['order_d_goods_name']; ?>"></td>
									<td><?php echo money($goods_arr['order_d_product_basic_primecost']); ?><input type="hidden" value="<?php echo $goods_arr['order_d_product_basic_primecost'];?>" name="invoice[goods_name][<?php echo $i; ?>]" class="span1"></td>
									<td><input type="text"  name="invoice[invoice_price][<?php echo $i; ?>]" value="<?php echo $listData['invoice_content']['invoice_price'][$i]; ?>" class="span2 num"></td>
								</tr>
							<?php $i++;  endforeach ?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
                     <div class="customer_2" <?php if($listData['invoice_customer_type'] == 1002){ echo 'style="display:none"';} ?>>



                     	<div class="control-group" >
							<label class="control-label required" for="invoice_title">
								<?php echo $labels["invoice_title"];?>
							</label>
							<div class="controls">

					<input type="text" maxlength="128" name="customer_2[invoice_title]" required="required" class="isrequired" title="<?php echo $labels["invoice_title"];?>" value="<?php echo $listData['invoice_title'] ?>"  id="invoice_title">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_taxpayer">
								<?php echo $labels["invoice_taxpayer"];?>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_2[invoice_taxpayer]" id="invoice_taxpayer" <?php if($listData['invoice_customer_type'] == 1001){ echo 'value="'.$listData["invoice_taxpayer"].'"';} ?>>

								<span class="help-inline"></span>
							</div>
						</div>


						<div class="control-group">
							<label class="control-label required" for="invoice_bank">
								<?php echo $labels["invoice_bank"];?>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_2[invoice_bank]" id="invoice_bank" <?php if($listData['invoice_customer_type'] == 1001){ echo 'value="'.$listData["invoice_bank"].'"';} ?>>

								<span class="help-inline"></span>
							</div>
						</div>

						 <div class="control-group">
							<label class="control-label required" for="invoice_account">
								<?php echo $labels["invoice_account"];?>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_2[invoice_account]" id="invoice_account" <?php if($listData['invoice_customer_type'] == 1001){ echo 'value="'.$listData["invoice_account"].'"';} ?>>

								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_tel">
								<?php echo $labels["invoice_tel"];?>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_2[invoice_tel]" id="invoice_tel" <?php if($listData['invoice_customer_type'] == 1001){ echo 'value="'.$listData["invoice_tel"].'"';} ?>>

								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_address">
								<?php echo $labels["invoice_address"];?>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_2[invoice_address]" id="invoice_address" <?php if($listData['invoice_customer_type'] == 1001){ echo 'value="'.$listData["invoice_address"].'"';} ?>>

								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_memo">
								<?php echo $labels["invoice_memo"];?>
							</label>
							<div class="controls">


<textarea name="customer_2[invoice_memo]" id="" style="width:400px;" cols="100" style="width:400px;" rows="5" ><?php if($listData['invoice_customer_type'] == 1001){ echo $listData["invoice_memo"];} ?></textarea>

								<span class="help-inline"></span>
							</div>
						 </div>
                     </div>

					<div class="customer_1" <?php if($listData['invoice_customer_type'] == 1001){ echo 'style="display:none"';} ?>>
						<div class="control-group" >
							<label class="control-label required" for="invoice_title">
								<?php echo $labels["invoice_title"];?>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="customer_1[invoice_title]" class="isrequired" required="required" title="<?php echo $labels["invoice_title"];?>" value="<?php echo $listData['invoice_title'] ?>"  id="invoice_title">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_taxpayer">
								<?php echo $labels["invoice_taxpayer"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">


<input type="text" maxlength="128" name="customer_1[invoice_taxpayer]" id="invoice_taxpayer" required="required" class="isrequired" title="<?php echo $labels["invoice_taxpayer"];?>"  <?php if($listData['invoice_customer_type'] == 1002){ echo 'value="'.$listData["invoice_taxpayer"].'"';} ?>>

								<span class="help-inline"></span>
							</div>
						</div>




						<div class="control-group">
							<label class="control-label required" for="invoice_bank">
								<?php echo $labels["invoice_bank"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_1[invoice_bank]" required="required" title="开户行" class="isrequired" id="invoice_bank" <?php if($listData['invoice_customer_type'] == 1002){ echo 'value="'.$listData["invoice_bank"].'"';} ?>>

								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_account">
								<?php echo $labels["invoice_account"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_1[invoice_account]" required="required" title="账号" class="isrequired" id="invoice_account" <?php if($listData['invoice_customer_type'] == 1002){ echo 'value="'.$listData["invoice_account"].'"';} ?>>

								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_tel">
								<?php echo $labels["invoice_tel"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_1[invoice_tel]" required="required" title="<?php echo $labels["invoice_tel"];?>" class="isrequired" id="invoice_tel" <?php if($listData['invoice_customer_type'] == 1002){ echo 'value="'.$listData["invoice_tel"].'"';} ?>>

								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_address">
								<?php echo $labels["invoice_address"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_1[invoice_address]" id="invoice_address" required="required" title="<?php echo $labels["invoice_address"];?>" class="isrequired" <?php if($listData['invoice_customer_type'] == 1002){ echo 'value="'.$listData["invoice_address"].'"';} ?>>
				<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_memo">
								<?php echo $labels["invoice_memo"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">

<textarea name="customer_1[invoice_memo]" id="" required="required" title="<?php echo $labels["invoice_memo"];?>" class="isrequired" style="width:400px" cols="100" rows="5" maxlength="128" >
	<?php if($listData['invoice_customer_type'] == 1002){ echo $listData["invoice_memo"];} ?>
</textarea>

								<span class="help-inline"></span>
							</div>
						</div>
					</div>
                     </div>
					</div>

  <script type="text/javascript">
   $(document).ready(function(){
   	  // $("#invoice_type_2").hide();
   	   //$(".customer_2").hide();
   	   	var invoice_type = <?php echo $listData['invoice_type'] ?>;
   	   	if(invoice_type==1001){
   	   		var invoice_customer_type = <?php echo $listData['invoice_customer_type']?>;
   	   		$("#invoice_type_2").css('display','none');
   	   		if(invoice_customer_type == 1001){
   	   			$(".customer_1").find('.isrequired').removeAttr('required');
          		$(".customer_2").find('.isrequired').attr('required','required');
   	   		}else{
   	   			 $(".customer_2").find('.isrequired').removeAttr('required');
           		 $(".customer_1").find('.isrequired').attr('required','required');
   	   		}
   	   	}else{
   	   		 $("[name='invoice_type_2[invoice_title]']").attr('required','required');
              $(".customer_2").find('.isrequired').removeAttr('required');
               $(".customer_1").find('.isrequired').removeAttr('required');
   	   	}

   	    $("[name='invoice_type_2[invoice_title]']").removeAttr('required');
   	 	$(".customer_2").find('.isrequired').removeAttr('required');
   	    $("[name='invoice[invoice_type]']").change(function(){
   	       //checked = $(this).attr('checked');
		   val = $(this).val();
		   if(val==1001){
               $("#invoice_type_1").show();
                $("#invoice_type_2").hide();
                 $("[name='invoice_type_2[invoice_title]']").removeAttr('required');
                if($("[name='invoice_type_1[invoice_customer_type]']").val==1001){
                	 $(".customer_1").find('.isrequired').removeAttr('required');
              		$(".customer_2").find('.isrequired').attr('required','required');
                }else{
                	 $(".customer_2").find('.isrequired').removeAttr('required');
               		$(".customer_1").find('.isrequired').attr('required','required');
                }
		   }else{
		   	  $("#invoice_type_2").show();
              $("#invoice_type_1").hide();
               $("[name='invoice_type_2[invoice_title]']").attr('required','required');
              $(".customer_2").find('.isrequired').removeAttr('required');
               $(".customer_1").find('.isrequired').removeAttr('required');

		   }
        })

        $("[name='invoice_type_1[invoice_customer_type]']").change(function(){
  			val = $(this).val();
  			if(val==1001){
  			  $(".customer_1").hide();
              $(".customer_2").show();
              $(".customer_1").find('.isrequired').removeAttr('required');
              $(".customer_2").find('.isrequired').attr('required','required');
  			}else{
               $(".customer_1").show();
              $(".customer_2").hide();
                $(".customer_2").find('.isrequired').removeAttr('required');
               $(".customer_1").find('.isrequired').attr('required','required');
  			}
        })
   })
  </script>
       </div>


	</form>
</div>
<!--lee 内容部分 end-->

<script type="text/javascript">
$(document).ready(function(){
	//alert($("#invoice_content_class").val());
	// if($("#invoice_content_class").val()==1001){

	// }

	$('#invoice_content_class').change(function(){
		//获取选择的value用作判断
		enumkey = $(this).val();
		if(enumkey==1001){
			$(".goods_content_info").hide();
			$(".group-text").show();
		}
		if(enumkey==1002){
			$(".goods_content_info").show();
			$(".group-text").hide();


		}
	});
    //$("[name='invoice[net_service_charge]']")
   // alert($('#invoice_content_class').val());
  //   if($('#invoice_content_class').val()==1001){
  //       $(".goods_content_info").hide();
		// $(".group-text").show();
  //   }else{
  //      $(".goods_content_info").show();
		// $(".group-text").hide();
  //   }
	 // if($("[name='invoice[invoice_type]']:checked").val()==1001){
	 // 	 $("#invoice_type_1").show();
  //         $("#invoice_type_2").hide();
	 // }else{
	 // 	  $("#invoice_type_2").show();
  //         $("#invoice_type_1").hide();
	 // }

	// $('#invoice_content_class').change(function(){
	// 	//获取选择的value用作判断
	// 	enumkey = $(this).val();
	// 	if(enumkey==1001){
	// 		$(".goods_content_info").hide();
	// 		$(".group-text").show();
	// 	}
	// 	if(enumkey==1002){
	// 		$(".goods_content_info").show();
	// 		$(".group-text").hide();


	// 	}
	// });
	$("[name='invoice[service_charge]']").change(function(){
     checked = $(this).attr('checked');
     if(checked){
			///alert('开');
			//显形对应的子tr
			$("#invoice_other_content_1001").show();
			//alert(val);
		}else{

			//alert('关');
			//消失
			$("#invoice_other_content_1001").hide();
		}
  })

    $("[name='invoice[net_service_charge]']").change(function(){
     checked = $(this).attr('checked');
     if(checked){
			///alert('开');
			//显形对应的子tr
			$("#invoice_other_content_1002").show();
			//alert(val);
		}else{
			//alert('关');
			//消失
			$("#invoice_other_content_1002").hide();
		}
    })

  // 	 $.ajax({
  // 	 	'type':'get',
  // 	 	'success':function(data){
		// 		$('#ff').html(data);
		// 	},
		// 'url':'<?php echo site_url('www/invoice/update'); ?>',
		// 'cache':false
  // 	 })
  })
</script>