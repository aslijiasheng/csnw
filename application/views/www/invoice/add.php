<?php if(isset($_GET['order_type']) & $_GET['order_type']==10 ){ ?>
	<input type="hidden" name="order_type">
	<div class="control-group" >
							<label class="control-label required" for="invoice_type" >
								票据类型:
								<span class="required">*</span>
							</label>
							<div class="controls">收据

							</div>
							</div>
<?php } ?>
			<div class="control-group" <?php if(isset($_GET['order_type']) & $_GET['order_type']==10){echo 'style="display:none"';} ?>>
							<label class="control-label required" for="invoice_type" >
								票据类型
								<span class="required">*</span>
							</label>
							<div class="controls">

<label class="radio inline">

	<input id="inlineRadioB" type="radio"  value="1001" name="invoice[invoice_type]" class='isrequired' required="required" title="票据类型" checked="checked">

	增值税发票
</label>
<label class="radio inline">

	<input id="inlineRadioB" type="radio"  value="1002" name="invoice[invoice_type]" class='isrequired' required="required" title="票据类型">

	收据
</label>

								<span class="help-inline"></span>
							</div>
						</div>
						<div id="invoice_type_2">
							 <div class="control-group">
							<label class="control-label required" for="invoice_title">
								<?php echo $labels["invoice_title"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="invoice_type_2[invoice_title]" required="required" class="isrequired" id="invoice_title" title="<?php echo $labels["invoice_title"];?>">

								<span class="help-inline"></span>
							</div>
						</div>
						
						<!--新增所属子公司 2014/8/25-->
						<div class="control-group">
							<label class="control-label required" for="invoice_subsidiary">
								<?php echo $labels["invoice_subsidiary"]; ?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach ($invoice_subsidiary_enum as $company_v) { ?>
									<label class="radio inline">
											<input id="inlineRadioB" type="radio"  value="<?php echo $company_v['enum_key'] ?>" name="invoice[invoice_subsidiary]">
										<?php echo $company_v['enum_name']; ?>
									</label>
								<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						<!--新增所属子公司 2014/8/25-->
						
                      <div class="control-group">
                      	<label class="control-label required" for="invoice_amount">
								本次申请开票金额
								<span class="required">*</span>
							</label>
							<div class="controls">
<input type="text" maxlength="128" name="invoice_type_2[invoice_amount]" required="required" class="isrequired" id="invoice_amount" title="本次申请开票金额">

								<span class="help-inline"></span>
							</div>
                      </div>

                        <div class="control-group">
                      	<label class="control-label required" for="invoice_receipt_content">
								收据内容
								<span class="required">*</span>
							</label>
							<div class="controls">
<textarea name="invoice_type_2[invoice_receipt_content]" id="invoice_receipt_content"  title="收据内容" style="width:400px;" cols="100" rows="5">
</textarea>
								<span class="help-inline"></span>
							</div>
                      </div>
						</div>



						<div id="invoice_type_1">
						<div class="control-group">
							<label class="control-label required" for="invoice_customer_type">
								客户类型
								<span class="required">*</span>
							</label>
							<div class="controls">

<label class="radio inline">
	<input id="inlineRadioB" type="radio"  value="1001"  required="required" class='isrequired' title='<?php echo $labels["invoice_customer_type"];?>' name="invoice_type_1[invoice_customer_type]" >
个人
</label>
<label class="radio inline">
	<input id="inlineRadioB" type="radio"  value="1002"  required="required" class='isrequired' title='<?php echo $labels["invoice_customer_type"];?>' name="invoice_type_1[invoice_customer_type]" checked="checked">
企业
</label>

								<span class="help-inline"></span>
							</div>
						</div>

		<div class="control-group">
			<label style="width: 170px;" class="control-label">开票内容分类</label>
			<div class="controls">
			<select name="invoice[invoice_content_class]" id="invoice_content_class">
				<?php foreach($invoice_content_class_enum as $invoice_content_class_v){ ?>
					<option value="<?php echo $invoice_content_class_v['enum_key'];?>">
						<?php echo $invoice_content_class_v['enum_name'];?>
					</option>
				<?php } ?>
			</select>
		  </div>
		</div>
		<!--新增所属子公司 2014/8/25-->
		<div class="control-group">
			<label class="control-label required" for="invoice_subsidiary">
				<?php echo $labels["invoice_subsidiary"]; ?>
				<span class="required"></span>
			</label>
			<div class="controls">
				<?php foreach ($invoice_subsidiary_enum as $company_v) { ?>
					<label class="radio inline">
							<input id="inlineRadioB" type="radio"  value="<?php echo $company_v['enum_key'] ?>" name="invoice[invoice_subsidiary]">
						<?php echo $company_v['enum_name']; ?>
					</label>
				<?php } ?>
				<span class="help-inline"></span>
			</div>
		</div>
		<!--新增所属子公司 2014/8/25-->
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				开票内容
				<span class="required"></span>
			</label>
			<div class="controls">
<div id="goods_content_info">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width='20'></th>
				<th>商品名称</th>
				<th>本次申请开票金额</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><input type="checkbox" value="1001" id="" name="invoice[service_charge]"></td>
				<td>服务费</td>
				<td><input type="text" name="invoice[other_price][1001]"></td>
			</tr>
			<tr id='invoice_other_content_1001' style="display:none;">
				<td></td>
				<td colspan="2">
                 <?php if(isset($order_data['detailed']['order_d_goods_arr'])){ ?>
				<?php foreach($order_data['detailed']['order_d_goods_arr'] as $goods_arr): ?>
					<label class="checkbox">
						<input type="checkbox"  name="service_select[<?php echo $goods_arr['order_d_goods_code']; ?>]" value="1">
						<?php echo $goods_arr['order_d_goods_name']; ?>
					</label>
				<?php endforeach ?>
				<?php } ?>

				</td>
			</tr>

			<tr>
				<td><input type="checkbox" value="1002" id="" name="invoice[net_service_charge]"></td>
				<td>网络服务费</td>
				<td><input type="text" name="invoice[other_price][1002]"></td>
			</tr>
			<tr id='invoice_other_content_1002' style="display:none;">
				<td></td>
				<td colspan="2">
                <?php if(isset($order_data['detailed']['order_d_goods_arr'])){ ?>
				<?php foreach($order_data['detailed']['order_d_goods_arr'] as $goods_arr): ?>
					<label class="checkbox">
						<input type="checkbox"  name="net_service_select[<?php echo $goods_arr['order_d_goods_code']; ?>]" value="1">
						<?php echo $goods_arr['order_d_goods_name']; ?>
					</label>
				<?php endforeach ?>
				<?php } ?>

				</td>
			</tr>

		</tbody>
	</table>
</div>
				<div class="group-text">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th>商品名称</th>
								<th>折后价</th>
								<th>本次申请开票金额</th>
							</tr>

								<?php $i=1;if(isset($order_data['detailed']['order_d_goods_arr'])){ ?>
								<?php  foreach($order_data['detailed']['order_d_goods_arr'] as $goods_arr): ?>
								<tr>
									<td><?php echo $goods_arr['order_d_goods_name']; ?><input type="hidden" value="<?php echo $goods_arr['order_d_goods_name']; ?>" name="invoice[goods_name][<?php echo $i; ?>]" class="span1" value="<?php echo $goods_arr['order_d_goods_name']; ?>"></td>
									<td><?php echo money($goods_arr['order_d_product_basic_disc']); ?><input type="hidden" value="<?php echo $goods_arr['order_d_product_basic_disc'];?>" name="invoice[goods_disc][<?php echo $i; ?>]" class="span1"></td>
									<td><input type="text" name="invoice[invoice_price][<?php echo $i; ?>]" class="span2 num"></td>
								</tr>
							<?php  $i++;endforeach  ?>
							<?php } ?>
					</tbody>
					</table>
				</div>
			</div>
		</div>

                     <div class="customer_2">
                     	<div class="control-group" >
							<label class="control-label required" for="invoice_title">
								<?php echo $labels["invoice_title"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_2[invoice_title]" id="invoice_title" required="required" class='isrequired' title="<?php echo $labels["invoice_title"];?>">

								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_taxpayer">
								<?php echo $labels["invoice_taxpayer"];?>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_2[invoice_taxpayer]" id="invoice_taxpayer">

								<span class="help-inline"></span>
							</div>
						</div>

						<!--新增票据编码-->
						<div class="control-group">
							<label class="control-label required" for="invoice_code">
								<?php echo $labels["invoice_code"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_2[invoice_code]" value="<?php echo $id_aData['invoice_code'] ?>" id="invoice_code">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_2[invoice_code]" id="invoice_code">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						<!--新增票据编码-->
						<!--新增票据号码-->
					 	<div class="control-group">
							<label class="control-label required" for="invoice_invoice_no">
								<?php echo $labels["invoice_invoice_no"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_2[invoice_invoice_no]" value="<?php echo $id_aData['invoice_invoice_no'] ?>" id="invoice_invoice_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_2[invoice_invoice_no]" id="invoice_invoice_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						<!--新增票据号码-->
						<div class="control-group">
							<label class="control-label required" for="invoice_bank">
								<?php echo $labels["invoice_bank"];?>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_2[invoice_bank]" id="invoice_bank">

								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_tel">
								<?php echo $labels["invoice_tel"];?>
							</label>
							<div class="controls">

<input type="text" maxlength="128" name="customer_2[invoice_tel]" id="invoice_tel">

								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_address">
								<?php echo $labels["invoice_address"];?>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_2[invoice_address]" value="<?php echo $id_aData['invoice_address'] ?>" id="invoice_address">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_2[invoice_address]" id="invoice_address">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_memo">
								<?php echo $labels["invoice_memo"];?>
							</label>
							<div class="controls">


<textarea name="customer_2[invoice_memo]" id="" style="width:400px;" cols="100" rows="5" ></textarea>

								<span class="help-inline"></span>
							</div>
						 </div>
                     </div>
					<div class="customer_1">
						<div class="control-group" >
							<label class="control-label required" for="invoice_title">
								<?php echo $labels["invoice_title"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
<input type="text" maxlength="128" name="customer_1[invoice_title]" id="invoice_title" required="required" class='isrequired'  title="<?php echo $labels["invoice_title"];?>">

								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_taxpayer">
								<?php echo $labels["invoice_taxpayer"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
<input type="text" maxlength="128" name="customer_1[invoice_taxpayer]" id="invoice_taxpayer" required="required" class='isrequired' title="<?php echo $labels["invoice_taxpayer"];?>">
								<span class="help-inline"></span>
							</div>
						</div>
						<!--新增票据编码-->
						<div class="control-group">
							<label class="control-label required" for="invoice_code">
								<?php echo $labels["invoice_code"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_1[invoice_code]" value="<?php echo $id_aData['invoice_code'] ?>" id="invoice_code">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_1[invoice_code]" id="invoice_code">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						<!--新增票据编码-->
						<!--新增票据号码-->
					 	<div class="control-group">
							<label class="control-label required" for="invoice_invoice_no">
								<?php echo $labels["invoice_invoice_no"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="customer_1[invoice_invoice_no]" value="<?php echo $id_aData['invoice_invoice_no'] ?>" id="invoice_invoice_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="customer_1[invoice_invoice_no]" id="invoice_invoice_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						<!--新增票据号码-->
						<div class="control-group">
							<label class="control-label required" for="invoice_bank">
								<?php echo $labels["invoice_bank"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
<input type="text" maxlength="128" name="customer_1[invoice_bank]" id="invoice_bank" required="required" class='isrequired' title="<?php echo $labels["invoice_bank"];?>">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_account">
								<?php echo $labels["invoice_account"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
<input type="text" maxlength="128" name="customer_1[invoice_account]" id="invoice_account" required="required" class='isrequired' title="<?php echo $labels["invoice_account"];?>">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_tel">
								<?php echo $labels["invoice_tel"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
<input type="text" maxlength="128" name="customer_1[invoice_tel]" id="invoice_tel" required="required" class='isrequired' title="<?php echo $labels["invoice_tel"];?>">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_address">
								<?php echo $labels["invoice_address"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
<input type="text" maxlength="128" name="customer_1[invoice_address]" id="invoice_address" required="required" class='isrequired' title="<?php echo $labels["invoice_address"];?>">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_memo">
								<?php echo $labels["invoice_memo"];?>
							</label>
							<div class="controls">
<textarea name="customer_1[invoice_memo]" style="width:400px;" id="" cols="100" rows="5" maxlength="128" class='isrequired' title="<?php echo $labels["invoice_memo"];?>"></textarea>
								<span class="help-inline"></span>
							</div>
						</div>
					</div>
                     </div>
					</div>

  <script type="text/javascript">
   $(document).ready(function(){
   		var order_type=<?php echo $_GET['order_type']; ?>;
   		if(order_type && order_type==10){
   			// $("#invoice_type_2").show();
              $("#invoice_type_1").hide();
               $("#invoice_type_2").find('.isrequired').attr('required','required');
              $("[name='invoice_type_2[invoice_title]']").attr('required','required');
              $(".customer_2").find('.isrequired').removeAttr('required');
               $(".customer_1").find('.isrequired').removeAttr('required');

   		}else{
   			$("#invoice_type_2").hide();
   			  $("#invoice_type_2").find('.isrequired').removeAttr('required');
   		}
   		//alert(order_type);
        //$("#invoice_type_2").hide();
     $("#goods_content_info").hide();
			$(".group-text").show();
	$('#invoice_content_class').change(function(){
		//获取选择的value用作判断
		enumkey = $(this).val();
		if(enumkey==1001){
			$("#goods_content_info").hide();
			$(".group-text").show();
		}
		if(enumkey==1002){
			$("#goods_content_info").show();
			$(".group-text").hide();


		}
	});
	//alert($("[name='invoice[service_charge]']").val());
  $("[name='invoice[service_charge]']").change(function(){
     checked = $(this).attr('checked');
     if(checked){
			///alert('开');
			//显形对应的子tr
			$("#invoice_other_content_1001").show();
			//alert(val);
		}else{

			//alert('关');
			//消失
			$("#invoice_other_content_1001").hide();
		}
  })

    $("[name='invoice[net_service_charge]']").change(function(){
     checked = $(this).attr('checked');
     if(checked){
			///alert('开');
			//显形对应的子tr
			$("#invoice_other_content_1002").show();
			//alert(val);
		}else{
			//alert('关');
			//消失
			$("#invoice_other_content_1002").hide();
		}
    })


   	   //$("#invoice_type_2").hide();

   	   $(".customer_2").hide();

   	   $(".customer_2").find('.isrequired').removeAttr('required');
   	    $("[name='invoice[invoice_type]']").change(function(){
   	       //checked = $(this).attr('checked');
		   val = $(this).val();

		   if(val==1001){
               $("#invoice_type_1").show();
                $("#invoice_type_2").hide();
                $("#invoice_type_2").find('.isrequired').removeAttr('required');
                $("[name='invoice_type_2[invoice_title]']").removeAttr('required');
                if($("[name='invoice_type_1[invoice_customer_type]']").val==1001){
                	 $(".customer_1").find('.isrequired').removeAttr('required');
              		$(".customer_2").find('.isrequired').attr('required','required');
                }else{
                	 $(".customer_2").find('.isrequired').removeAttr('required');
               		$(".customer_1").find('.isrequired').attr('required','required');
                }

		   }else{
		   	  $("#invoice_type_2").show();
              $("#invoice_type_1").hide();
               $("#invoice_type_2").find('.isrequired').attr('required','required');
              $("[name='invoice_type_2[invoice_title]']").attr('required','required');
              $(".customer_2").find('.isrequired').removeAttr('required');
               $(".customer_1").find('.isrequired').removeAttr('required');

		   }
        })

        $("[name='invoice_type_1[invoice_customer_type]']").change(function(){
  			val = $(this).val();
  			if(val==1001){
  			  $(".customer_1").hide();
              $(".customer_2").show();
              $(".customer_1").find('.isrequired').removeAttr('required');
              $(".customer_2").find('.isrequired').attr('required','required');
  			}else{
               $(".customer_1").show();
              $(".customer_2").hide();
               $(".customer_2").find('.isrequired').removeAttr('required');
               $(".customer_1").find('.isrequired').attr('required','required');

  			}
        })
   })
  </script>