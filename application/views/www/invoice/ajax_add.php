<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="invoice-form" action="" method="post">
		<!-- 用一个隐藏文本框来存order_id -->
		<input size="16" type="hidden" name="invoice[invoice_order_id]" value='<?php echo $order_data['order_id'];?>'>
		<input size="16" type="hidden" name="order_finance" value='<?php echo $order_data['order_finance'];?>'>
		<input type="hidden" name="order_finance_department" value='<?php echo $order_data['order_finance_arr']['user_department']; ?>'>
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				基本信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text1">
					<table class="table table-bordered">
						<tbody><tr>
					<td width="12%">
						客户名称：
					</td>
					<td width="38%">
					  <?php if(!empty($account_data)){ echo $account_data['account_name'];} ?>
					</td>
					<td width="12%">
						企业ID：
					</td>
					<td width="38%">
					  <?php if(!empty($account_data)){ echo $account_data['account_id'];} ?>
					</td>
				</tr>
				<tr>
					<td>
						订单号：
					</td>
					<td>
					  <?php echo $order_data['order_number']; ?>
					</td>
					<td>
						订单金额：
					</td>
					<td>
						<?php echo number_format($order_data['order_amount'],2,'.',''); ?>
					</td>
				</tr>
				<tr>
					<td>
						已申请开票金额：
					</td>
					<td>
					   <input type="hidden" name="invoice[invoice_value]" value="<?php if(!empty($invoice_value)){ echo number_format($invoice_value,2,'.','');}else{ echo "0.00";} ?>">
						<?php if(!empty($invoice_value)){ echo number_format($invoice_value,2,'.','');}else{ echo "0.00";} ?>
					</td>
					<td>
						剩余开票金额：
					</td>
					<td>
						<?php if(floatval($odd_invoice_value)>=0){echo number_format($odd_invoice_value,2,'.','');}else{ echo 0.00;}?>
						<!--input type="hidden" name="invoice[odd_invoice_value]" value="<?php //if(!empty($odd_invoice_value)){echo $odd_invoice_value;}else{ echo $max_invoice_amount;}?>"-->
						<input type="hidden" name="invoice[odd_invoice_value]" value="<?php echo $order_data['order_amount'];echo$invoice_value;?>">
					</td>
				</tr>
			</tbody>
					</table>
				</div>
			</div>
		</div>



       <div id="ff">

       </div>


	</form>
</div>
<!--lee 内容部分 end-->

<script type="text/javascript">
$(document).ready(function(){
	 $("#invoice_type_2").hide();
     $("#goods_content_info").hide();
			$(".group-text").show();
	$('#invoice_content_class').change(function(){
		//获取选择的value用作判断
		enumkey = $(this).val();
		if(enumkey==1001){
			$("#goods_content_info").hide();
			$(".group-text").show();
		}
		if(enumkey==1002){
			$("#goods_content_info").show();
			$(".group-text").hide();


		}
	});
	//alert($("[name='invoice[service_charge]']").val());
  $("[name='invoice[service_charge]']").change(function(){
     checked = $(this).attr('checked');
     if(checked){
			///alert('开');
			//显形对应的子tr
			$("#invoice_other_content_1001").show();
			//alert(val);
		}else{

			//alert('关');
			//消失
			$("#invoice_other_content_1001").hide();
		}
  })

    $("[name='invoice[net_service_charge]']").change(function(){
     checked = $(this).attr('checked');
     if(checked){
			///alert('开');
			//显形对应的子tr
			$("#invoice_other_content_1002").show();
			//alert(val);
		}else{
			//alert('关');
			//消失
			$("#invoice_other_content_1002").hide();
		}
    })

	// $("[name='invoice[invoice_other_content]']").change(function(){
	// 	checked = $(this).attr('checked');
	// 	val = $(this).val();
	// 	if(checked){
	// 		//alert('开');
	// 		//显形对应的子tr
	// 		$("#invoice_other_content_"+val).show();
	// 		//alert(val);
	// 	}else{
	// 		//alert('关');
	// 		//消失
	// 		$("#invoice_other_content_"+val).hide();
	// 	}
	// });


  	 $.ajax({
  	 	'type':'get',
  	 	'data':{},
  	 	'success':function(data){

				$('#ff').html(data);
			},
		'url':'<?php echo site_url('www/invoice/add?order_id='.$_GET['order_id'].'&order_type='.$_GET['order_type']); ?>',
		'cache':false
  	 })
  })


//alert(11);
</script>