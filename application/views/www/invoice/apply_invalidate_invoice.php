<style>
	.confirm_invoice_table th, .confirm_invoice_table td{
     border-top-style: none;
}
</style>
<div class="grid-view">
	<form class="form-horizontal" id="apply_invalidate_invoice-form">
	<input type="hidden" name="invoice[invoice_id]" value="<?php echo $listData['invoice_id'] ?>">
	<input size="16" type="hidden" name="invoice[invoice_order_id]" value='<?php echo $listData['invoice_order_id'];?>'>
	<input type="hidden" name="order_finance" value="<?php echo $listData['invoice_reviewer'] ?>">
	  <table class="table confirm_invoice_table" >
					<tbody>
						<tr style="">
							<th width="15%">票据抬头：</th>
							<td width="35%"><?php echo $listData['invoice_title']; ?></td>
							<th width="15%">开票类型：</th>
							<td width="35%"><?php echo $listData['invoice_type_arr']['enum_name'];  ?></td>
						</tr>
						<tr>
							<th>业务总监：</th>
							<td> <?php if(isset($listData['invoice_content']['invoice_reviewer'])){echo $listData['invoice_content']['invoice_reviewer'];}else{ echo '--';} ?></td>
							<th>审核状态：</th>
							<td><?php $listData['invoice_process_status_arr']['enum_name'];  ?></td>
						</tr>
						<tr>
							<th>客户类型：</th>
							<td><?php echo $listData['invoice_customer_type_arr']['enum_name']; ?></td>
							<th>纳税人识别号：</th>
							<td><?php echo $listData['invoice_taxpayer']; ?></td>
						</tr>
						<tr>
							<th>地址：</th>
							<td><?php echo $listData['invoice_address']; ?></td>
							<th>电话：</th>
							<td><?php echo $listData['invoice_tel']; ?></td>
						</tr>
						<tr>
							<th>开户行：</th>
							<td><?php echo $listData['invoice_bank']; ?></td>

						</tr>
						<tr>
							<th>开票金额：</th>
							<td><?php echo $listData['invoice_amount'] ?></td>
							<th>开票状态：</th>
							<td><?php echo $listData['invoice_do_status_arr']['enum_name']; ?></td>
						</tr>


						<tr>
							<th><label for="invoice_apply_uninvoice_note">申请作废备注：</label></th>
							<td colspan="3">
								<textarea name="invoice[invoice_apply_uninvoice_note]" id="invoice_apply_uninvoice_note" required="required" title="申请作废备注" rows="5" class="span6"></textarea>
							</td>
						</tr>
					</tbody>
				</table>
	</form>
</div>