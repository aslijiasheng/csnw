
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="75">操作</th>
				<th><?php echo $labels['invoice_invoice_no'];?></th>
				<th><?php echo $labels['invoice_type'];?></th>
				<th><?php echo $labels['invoice_addtime'];?></th>
				<th><?php echo $labels['invoice_title'];?></th>
				<th><?php echo $labels['invoice_amount'];?></th>
				<th><?php //echo $labels['invoice_customer_type'];?>发票类型</th>
				<th><?php echo $labels['invoice_status'];?></th>

			</tr>
		</thead>
		<tbody>
		<?php foreach($listData as $listData_value){?>
		<?php  $user_auth = $this->user->user_auth($this->session->userdata('user_id')); ?>
			<tr>
				<td style="font-size:16px;">
					<div class="btn-toolbar">
						<div class="btn-group">
							<a class="btn btn-small btn-primary invoice_view" id="<?php echo $listData_value['invoice_id'] ?>" <?php if(!in_array('invoice_view', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>查看</a>
							<button class="btn btn-small dropdown-toggle btn-primary" data-toggle="dropdown" <?php if(!in_array('invoice_view', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
								<span class="caret"></span>
							</button>

							<ul class="dropdown-menu">
								<li <?php if(!in_array($listData_value['invoice_status'], array(1001,1005)) or (!in_array(3, $this->session->userdata('role_id_arr')) and !in_array(6, $this->session->userdata('role_id_arr')))){ echo 'style="display:none;"';} ?>>

									<a id="<?php echo $listData_value['invoice_id'] ?>" class="confirm_invoice" <?php if(!in_array('confirm_invoice', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>执行开票  </a>
								</li>
								<li <?php if($listData_value['invoice_status'] !=1003){ echo 'style="display:none;"'; } ?>>
									<a id="<?php echo $listData_value['invoice_id'] ?>" class="edit_invoice" <?php if(!in_array('invoice_edit', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>编辑</a>
								</li>
								<li>
									<a id="<?php echo $listData_value['invoice_id'] ?>" class="del_invoice" <?php if(!in_array('invoice_del', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>删除</a>
								</li>
								<li <?php if($listData_value['invoice_status'] !=1004){ echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['invoice_id'] ?>" class="verify_invoice" <?php if(!in_array('verify_invoice', $user_auth['activity_auth_arr']) or $this->session->userdata('user_id')!=$listData_value['invoice_reviewer']){ echo 'style="display:none;"';} ?>>审核预开票申请</a>
								</li>
								<li <?php if(!in_array($listData_value['invoice_status'],array(1002,1009))){ echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['invoice_id'] ?>" class="apply_invalidate_invoice" <?php if(!in_array('apply_invalidate_invoice', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>申请废票</a>
								</li>
								<li <?php if($listData_value['invoice_status'] !=1006){ echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['invoice_id'] ?>" class="do_invalidate_invoice" <?php if(!in_array('do_invalidate_invoice', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>执行废票</a>
								</li>
							</ul>
						</div>
					</div>
				</td>
				 <?php if($listData_value['invoice_status']==1008){ ?>
					<td style="color:red">
					  <?php echo $listData_value['invoice_invoice_no']; ?>
					</td>

					<td style="color:red">
					  <?php echo $listData_value['invoice_type_arr']['enum_name']; ?>
					</td>

					<td style="color:red">
					  <?php echo $listData_value['invoice_addtime']; ?>
					</td>

					<td style="color:red">
			          <?php echo $listData_value['invoice_title']; ?>
					</td>
					
					<td style="color:red">
						-<?php echo $listData_value['invoice_amount'];?>
					</td>

					<td style="color:red">
					  <?php echo $listData_value['invoice_customer_type_arr']['enum_name']; ?>
					</td>
					<td style="color:red">
						<?php echo $listData_value['invoice_status_arr']['enum_name']; ?>
					</td>
				 <?php }else{ ?>
				   <td>
					  <?php echo $listData_value['invoice_invoice_no']; ?>
					</td>

					<td>
					  <?php echo $listData_value['invoice_type_arr']['enum_name']; ?>
					</td>

					<td>
					  <?php echo $listData_value['invoice_do_time']; ?>
					</td>

					<td>
			          <?php echo $listData_value['invoice_title']; ?>
					</td>
					
					<td>
						<?php echo $listData_value['invoice_amount'];?>
					</td>

					<td>
					  <?php echo $listData_value['invoice_customer_type_arr']['enum_name']; ?>
					</td>
					<td>
						<?php echo $listData_value['invoice_status_arr']['enum_name']; ?>
					</td>
				<?php } ?>
			</tr>
		<?php }?>
		</tbody>
	</table>
<script type="text/javascript">
$(document).ready(function() {

    //这里开始是开票

   $(".invoice_view").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'invoice_id='+id,
			'success':function(data){

				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/invoice/ajax_view'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'查看发票明细信息',
			width:800,
			height:450,
		});
		$('#operation_dialog').dialog('open');
	});

   //编辑发票

   $(".edit_invoice").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'invoice_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/invoice/ajax_edit'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'编辑发票',
			width:800,
			height:450,
			buttons: [{
				text:"保存",
				Class:"btn btn-primary",
				click: function(){
					$this = $(this);
					formdata = $("#invoice-form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//本次入账金额
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("操作成功");
								$('#invoice').ajaxHtml();
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/invoice/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					$(this).dialog("close");
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});
    //删除发票
	$(".del_invoice").click(function(){
		id=$(this).attr('id');
		alertify.confirm("是否删除这条开票记录", function (e) {
			if (e) {
				$.ajax({
					'type':'post',
					'data':'invoice_id='+id+'&order_id='+<?php echo $_GET['order_id'] ?>,
					'success':function(data){
						if(data==1){
							alertify.success("删除成功");
							//删除成功后再次加载1次页面
							$('#invoice').ajaxHtml();
							$('#operation_log').ajaxHtml();
						}else{
							alertify.alert('失败'+data);
						}
					},
					'url':'<?php echo site_url('www/invoice/ajax_del_post'); ?>',
					'async':false
				});
			} else {
				alertify.error("你放弃了删除");
			}
		});
		//return false;
	});
     //执行开票
	$(".confirm_invoice").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'invoice_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/invoice/confirm_invoice'); ?>',
			'cache':false
		});

		$('#operation_dialog').dialog({
			modal: true,
			title:'执行开票',
			width:800,
			height:450,
			buttons: [{
				text:"确认",
				Class:"btn btn-primary",
				click: function(){
					formdata = $("#confirm_invoice_form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//formdata[formdata.length]={"name":"refund[refund_status]","value":"1002"}; //添加1个参数
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							//alert(data);
							if(data==1){
								alertify.alert("通过开票");
								$('#invoice').ajaxHtml();//再次加载1次页面
								$('#operation_log').ajaxHtml();//

								$('#operation_dialog').dialog("close");

							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/invoice/update_confirm_invoice'); ?>',
						'async':false
					});
				}
			},{
				text:"驳回",
				Class:"btn btn-success",
				click: function(){
					formdata = $("#confirm_invoice_form").serializeArray();
					//formdata[formdata.length]={"name":"refund[refund_status]","value":"1003"}; //添加1个参数
					//alert(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("驳回开票");
								$('#invoice').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/invoice/return_do_invoice'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了审批返点");
					$(this).dialog("close");
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});


	//审批预开票申请
	$(".verify_invoice").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'invoice_id='+id,
			'success':function(data){

				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/invoice/verify_invoice'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'审批预开票申请',
			width:800,
			height:450,
			buttons: [{
				text:"确认",
				Class:"btn btn-primary",
				click: function(){
					formdata = $("#verify_invoice_form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//formdata[formdata.length]={"name":"refund[refund_status]","value":"1002"}; //添加1个参数
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("通过审批");
								$('#invoice').ajaxHtml();//再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");

							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/invoice/update_verify_invoice'); ?>',
						'async':false
					});
				}
			},{
				text:"驳回",
				Class:"btn btn-success",
				click: function(){
					formdata = $("#verify_invoice_form").serializeArray();
					//formdata[formdata.length]={"name":"refund[refund_status]","value":"1003"}; //添加1个参数
					//alert(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("驳回审批");
								$('#invoice').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/invoice/return_verify_invoice'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了审批预开票申请");
					$(this).dialog("close");
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});


   $(".apply_invalidate_invoice").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'invoice_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/invoice/apply_invalidate_invoice'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'申请废票',
			width:800,
			height:450,
			buttons: [{
				text:"保存",
				Class:"btn btn-primary",
				click: function(){
					$this = $(this);
					formdata = $("#apply_invalidate_invoice-form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//本次入账金额
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("操作成功");
								$('#invoice').ajaxHtml();
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/invoice/do_apply_invalidate_invoice'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					$(this).dialog("close");
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});


//执行废票

  $(".do_invalidate_invoice").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'invoice_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/invoice/invalidate_invoice'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'执行废票',
			width:800,
			height:450,
			buttons: [{
				text:"保存",
				Class:"btn btn-primary",
				click: function(){
					$this = $(this);
					formdata = $("#invalidate_invoice-form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//本次入账金额
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("操作成功");
								$('#invoice').ajaxHtml();
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/invoice/do_invalidate_invoice'); ?>',
						'async':false
					});
				}
			},{
				text:"驳回",
				Class:"btn btn-success",
				click: function(){
					formdata = $("#invalidate_invoice-form").serializeArray();
					//formdata[formdata.length]={"name":"refund[refund_status]","value":"1003"}; //添加1个参数
					//alert(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("作废失败");
								$('#invoice').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/invoice/return_invalidate_invoice'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					$(this).dialog("close");
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

});

</script>
