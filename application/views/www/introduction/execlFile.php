<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						订单新建页面
						<span class="mini-title">
						order
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="introduction" class="btn btn-primary">
<i class="icon-download-3"></i>
确认导入
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
<form id="introduction-form">
	<table class="leetable table table-bordered">
		<thead>
			<tr>
				<th>营收的属性名称</th>
				<th>导入属性</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($obj as $k=>$v){ ?>
				<tr>
					<td><?php echo $v;?></td>
					<td>
						<select id="country" name="file[<?php echo $k;?>]">
								<option value=""></option>
							<?php foreach($execl_data[0] as $execl_data_k=>$execl_data_v){ ?>
								<option value="<?php echo $execl_data_k;?>" <?php
									if ($v==$execl_data_v){
										echo 'selected';
									}
								?>>
									<?php echo $execl_data_v;?>
								</option>
							<?php } ?>
						</select>
					</td>
				</tr>
			<?php }?>
		</tbody>
	</table>
</form>
</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function () {
	$("#introduction").click(function(){
		url='<?php echo site_url('www/introduction/confirm_introduction'); ?>';
		data = $("#introduction-form").serializeArray();
		//添加属性
		data[data.length]={"name":"execl_data","value":'<?php echo json_encode($execl_data);?>'};
		$.ajax({
			'type':'post',
			'data':data,
			'success':function(data){
				console.debug(data);
				alert(data);
			},
			'url':url,
			'async':false
		});
	});
});
</script>