<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						批量开票
						<span class="mini-title">
						Bulk Billing
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
						</div>
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
<form id="introduction-form" class="form-horizontal" method="post">
	<table class="leetable table table-bordered">
		<thead>
			<tr>
				<th>序号</th>
				<th>订单编号</th>
				<th>匹配结果</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$i=0;
				foreach ($execl_data as $k=>$v){
					$i++;
			?>
				<tr <?php
					if($v['state']==1003){
						echo "class='error'";
					}else if ($v['state']==1002){
						echo "class='info'";
					}else{
						echo "class='warning'";
					}
				?>>
					<td><?php echo $i;?></td>
					<td><?php echo $v['number'];?></td>
					<td><?php echo $v['state_name'];?></td>
				</tr>
			<?php }?>
		</tbody>
	</table>

<div <?php if($flag == 0){ echo 'style="display:none;"';} ?> style="border:1px solid #D9D9D9; padding-top:10px;" >
<div>
	<span style="padding-left:20px;"><?php echo $invoice_num; ?>条未开票记录将被执行,累积总金额为<?php echo $invoice_amount;  ?><input type="hidden" name="order_info" value='<?php echo $order_info ?>'></span>
</div>

	      	<div class="control-group" style="margin-top:10px;">
	      		<label class="control-label required" for="invoice_do_time" >
					开票时间
				</label>
				<div class="controls">
					<div id="salespay_pay_date" class="input-append date form_date">
									<input type="text" value="" style="width: 154px;" required="required" title="开票时间" id="invoice_do_time" name="invoice[invoice_do_time]">
									<span class="add-on"><i class="icon-close"></i></span>
									<span class="add-on"><i class="icon-clock"></i></span>
								</div>
								<script type="text/javascript">
									$(document).ready(function () {
										$('#salespay_pay_date').datetimepicker({
									        language:  'zh-CN',
									        weekStart: 1,
									        todayBtn:  1,
											autoclose: 1,
											todayHighlight: 1,
											startView: 2,
											forceParse: 0,
											showMeridian: 1,
											format:'yyyy-mm-dd hh:ii:ss'
									    });
									});
								</script>
				</div>
	      	</div>
			<div class="control-group">
				<label class="control-label required" for="invoice_type" >
					票据类型
				</label>
				<div class="controls">
					<label class="radio inline">

						<input id="inlineRadioB" type="radio"  value="1001" name="invoice[invoice_type]" class='isrequired' required="required" title="票据类型" checked="checked">

						增值税发票
					</label>
					<label class="radio inline">

						<input id="inlineRadioB" type="radio"  value="1002" name="invoice[invoice_type]" class='isrequired' required="required" title="票据类型">

						收据
					</label>
					<span class="help-inline"></span>
				</div>
			</div>
			<div id="invoice_type_2">
				 <div class="control-group" >
					<label class="control-label required" for="invoice_title">
						票据抬头
						<span class="required">*</span>
					</label>
					<div class="controls">
						<input type="text" maxlength="128" name="invoice_type_2[invoice_title]" required="required" class="isrequired" id="invoice_title" title="票据抬头">
						<span class="help-inline"></span>
					</div>
				</div>
			 <div class="control-group">
                      	<label class="control-label required" for="invoice_amount">
								本次申请开票金额
								<span class="required">*</span>
							</label>
							<div class="controls">
<input type="text" maxlength="128" name="invoice_type_2[invoice_amount]" required="required" class="isrequired" id="invoice_amount" title="本次申请开票金额">

								<span class="help-inline"></span>
							</div>
                      </div>

                        <div class="control-group">
                      	<label class="control-label required" for="invoice_receipt_content">
								收据内容
								<span class="required">*</span>
							</label>
							<div class="controls">
<textarea name="invoice_type_2[invoice_receipt_content]" id="invoice_receipt_content"  title="收据内容" style="width:400px;" cols="100" rows="5">
</textarea>
								<span class="help-inline"></span>
							</div>
                      </div>
			</div>




						<div id="invoice_type_1">
						<div class="control-group">
							<label class="control-label required" for="invoice_customer_type">
								客户类型
							</label>
							<div class="controls">
								<label class="radio inline">
									<input id="inlineRadioB" type="radio"  value="1001"  required="required" class='isrequired' title='客户类型' name="invoice_type_1[invoice_customer_type]" >
								个人
								</label>
								<label class="radio inline">
									<input id="inlineRadioB" type="radio"  value="1002"  required="required" class='isrequired' title='客户类型' name="invoice_type_1[invoice_customer_type]" checked="checked">
								企业
								</label>
								<span class="help-inline"></span>
							</div>
						</div>

                     <!-- customer_2开始 -->
                     <div class="customer_2">
                     	<div class="control-group" >
							<label class="control-label required" for="invoice_title">
								票据抬头
								<span class="required">*</span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="customer_2[invoice_title]" id="invoice_title" required="required" class='isrequired' title="票据抬头">

								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_taxpayer">
								纳税人识别号
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="customer_2[invoice_taxpayer]" id="invoice_taxpayer">
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label required" for="invoice_code">
							票据编码
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="customer_2[invoice_code]" id="invoice_code"  title="票据编码" required="required" class='isrequired'>
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label required" for="invoice_invoice_no">
							票据号码
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="customer_2[invoice_invoice_no]" id="invoice_invoice_no"  title="票据号码" required="required" class='isrequired'>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_bank">
								开户行
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="customer_2[invoice_bank]" id="invoice_bank">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_tel">
								电话
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="customer_2[invoice_tel]" id="invoice_tel">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_address">
							地址
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="customer_2[invoice_address]" id="invoice_address">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_memo">
								开票备注
							</label>
							<div class="controls">
								<textarea name="customer_2[invoice_memo]" id="" style="width:400px;" cols="100" rows="5" ></textarea>
								<span class="help-inline"></span>
							</div>
						 </div>
                     </div>

                  <!-- customer_2结束 -->

                  <!-- customer_1开始 -->
					<div class="customer_1">
						<div class="control-group" >
							<label class="control-label required" for="invoice_title">
								票据抬头
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="customer_1[invoice_title]" id="invoice_title" required="required" class='isrequired'  title="票据抬头">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_taxpayer">
								纳税人识别号
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="customer_1[invoice_taxpayer]" id="invoice_taxpayer" required="required" class='isrequired' title="纳税人识别号">
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label required" for="invoice_code">
							票据编码
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="customer_1[invoice_code]" id="invoice_code" required="required" class='isrequired' title="票据编码">
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label required" for="invoice_invoice_no">
							票据号码
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="customer_1[invoice_invoice_no]" id="invoice_invoice_no" required="required" class='isrequired' title="票据号码">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_bank">
								开户行
							</label>
							<div class="controls">
							<input type="text" maxlength="128" name="customer_1[invoice_bank]" id="invoice_bank" required="required" class='isrequired' title="开户行">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_tel">
								电话
							</label>
							<div class="controls">
							<input type="text" maxlength="128" name="customer_1[invoice_tel]" id="invoice_tel" required="required" class='isrequired' title="电话">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_address">
								地址
							</label>
							<div class="controls">
							<input type="text" maxlength="128" name="customer_1[invoice_address]" id="invoice_address" required="required" class='isrequired' title="地址">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="invoice_memo">
								开票备注
							</label>
							<div class="controls">
								<textarea name="customer_1[invoice_memo]" style="width:400px;" id="" cols="100" rows="5" maxlength="128" required="required" class='isrequired' title="开票备注"></textarea>
								<span class="help-inline"></span>
							</div>
						</div>
					</div>

					<!-- customer_1结束 -->

                     </div>

                     <!-- invoice_type_1 结束 -->
					</div>

  <script type="text/javascript">
   $(document).ready(function(){

   	   $("#invoice_type_2").hide();
   	   $(".customer_2").hide();
   	    $("#invoice_type_2").find('.isrequired').removeAttr('required');
   	   $(".customer_2").find('.isrequired').removeAttr('required');
   	    $("[name='invoice[invoice_type]']").change(function(){
   	       //checked = $(this).attr('checked');
		   val = $(this).val();

		   if(val==1001){
               $("#invoice_type_1").show();
                $("#invoice_type_2").hide();
                $("#invoice_type_2").find('.isrequired').removeAttr('required');
                if($("[name='invoice_type_1[invoice_customer_type]']").val==1001){
                	 $(".customer_1").find('.isrequired').removeAttr('required');
              		$(".customer_2").find('.isrequired').attr('required','required');
                }else{
                	 $(".customer_2").find('.isrequired').removeAttr('required');
               		$(".customer_1").find('.isrequired').attr('required','required');
                }

		   }else{
		   	  $("#invoice_type_2").show();
              $("#invoice_type_1").hide();
              $("#invoice_type_2").find('.isrequired').attr('required','required');
              $(".customer_2").find('.isrequired').removeAttr('required');
               $(".customer_1").find('.isrequired').removeAttr('required');

		   }
        })

        $("[name='invoice_type_1[invoice_customer_type]']").change(function(){
  			val = $(this).val();
  			if(val==1001){
  			  $(".customer_1").hide();
              $(".customer_2").show();
              $(".customer_1").find('.isrequired').removeAttr('required');
              $(".customer_2").find('.isrequired').attr('required','required');
  			}else{
               $(".customer_1").show();
              $(".customer_2").hide();
               $(".customer_2").find('.isrequired').removeAttr('required');
               $(".customer_1").find('.isrequired').attr('required','required');

  			}
        })
   })
  </script>
	<div class="form-actions no-margin" <?php if($flag == 0){ echo 'style="display:none;"';} ?>>
	    <button type="submit" class="btn btn-info pull-right">
	      提交开票
	    </button>
	    <div class="clearfix">
	    </div>
      </div>

</div>

</form>
</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function () {

});
</script>