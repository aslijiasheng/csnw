<div class="grid-view">
	<form class="form-horizontal" id="cancel-form" action="" method="post">
		<input size="16" type="hidden" name="cancel[order_id]" value='<?php echo $order_data['order_id'];?>'>
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				基本信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text1">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td width="12%">
									订单编号：
								</td>
								<td width="38%">
									<?php if ( !empty( $order_data ) ) { echo $order_data['order_number'];} ?>
								</td>
								<td width="12%">
								</td>
								<td width="38%">
								</td>
							</tr>
							<tr>
								<td>所属销售：</td>
								<td>
									<?php if ( !empty( $order_data['order_owner'] ) ) { echo $order_data['order_owner_arr']['user_name'];} ?>
								</td>
								<td>所属部门：</td>
								<td>
									<?php if ( !empty( $order_data['order_department'] ) ) { echo $order_data['order_department_arr']['department_name'];} ?>
								</td>
							</tr>
							<tr>
								<td>客户：</td>
								<td>
									<?php if ( !empty( $order_data['order_account'] ) ) { echo $order_data['order_account_arr']['account_name'];} ?>
								</td>
								<td>代理商：</td>
								<td>
									<?php if ( !empty( $order_data['order_agent'] ) ) { echo $order_data['order_agent_arr']['partner_name'];} ?>
								</td>
							</tr>
							<tr>
								<td>订单号：</td>
								<td>
									<?php echo $order_data['order_number']; ?>
								</td>
								<td>订单金额：</td>
								<td>
									<?php echo number_format( $order_data['order_amount'], 2, '.', '' ); ?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!--订单明细-->
			<div class="controls">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th width="20" style='text-align:center;'><span id='contraction_btn' class="icon-contract"></span></th>
							<th width="200">商品名称</th>
							<th width="200">商品编码</th>
							<th width="200">商品原价</th>
							<th width="200">商品折后价</th>
						</tr>
					</thead>
					<tbody>
					<?php if(isset($listData)){foreach($listData as $k=>$listData_value){?>
						<tr>
							<td style='text-align:center;' leetype='contraction' leefor="#contraction_tr_<?php echo $k;?>" class='contraction_icon'><span class="icon-minus"></span></td>
							<td><?php echo $listData_value['order_d_goods_name'];?></td>
							<td><?php echo $listData_value['order_d_goods_code'];?></td>
							<td><?php echo $listData_value['order_d_order_primecost'];?></td>
							<td><?php echo $listData_value['order_d_order_disc'];?></td>
						</tr>
						<tr id='contraction_tr_<?php echo $k;?>' class='contraction_tr'>
							<td style="font-size:16px;"></td>
							<td colspan="4">
								<table class="table table-bordered ">
									<thead>
										<tr>
											<th width="200">产品名称</th>
											<th width="200">产品编码</th>
											<th width="200">产品原价</th>
											<th width="200">产品折后价</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($listData_value['product_arr'] as $kk=>$vv){ ?>
										<tr>
											<td><?php echo $vv['order_d_product_basic_name'];?></td>
											<td><?php echo $vv['order_d_product_basic_code'];?></td>
											<td><?php echo $vv['order_d_product_basic_primecost'];?></td>
											<td><?php echo $vv['order_d_product_basic_disc'];?></td>
										</tr>
										<?php }?>
									</tbody>
								</table>
							</td>
						</tr>
					<?php }?>
					<?php } ?>
					</tbody>
				</table>
			<script type="text/javascript">
			$(document).ready(function () {
				$('[leetype="contraction"]').click(function(){
					leefor = $(this).attr('leefor');
					if($(leefor).is(":hidden")){
						//隐藏了则将它显示出来
						$(leefor).show(500);
						$(this).html('<span class="icon-minus"></span>');
					}else{
						//没隐藏则将它隐藏起来
						$(leefor).hide(500);
						$(this).html('<span class="icon-plus"></span>');
					}
				});
				$('#contraction_btn').click(function(){
					btncss=$(this).attr('class');
					if(btncss=='icon-contract'){
						$(this).attr('class','icon-expand');
						$('.contraction_tr').hide(500);
						$('.contraction_icon').html('<span class="icon-plus"></span>');
					}
					if(btncss=='icon-expand'){
						$(this).attr('class','icon-contract');
						$('.contraction_tr').show(500);
						$('.contraction_icon').html('<span class="icon-minus"></span>');
					}
				});
			});
			</script>
			</div>
			<!--订单明细end-->
		</div>

		<div class="control-group">
			<label style="width: 170px;" class="control-label">操作</label>
			<div class="controls">
				<select name="cancel[operation]" id="operation">
					<option value="cancel">申请作废</option>
					<option value="change">订单变更</option>
				</select>
			</div>
		</div>

		<div class="control-group" id="cOrder" style="display:none;">
			<label style="width: 170px;" class="control-label">选择订单</label>
			<div class="controls">
				<input type="text" maxlength="128" name="cancel[cancel_order_number]" id="cancel_order_number" value_id ="" value="" url="<?php echo site_url('www/order/ajax_list?tag_name=cancel_order_number&order_id=1') ?>" leetype="quote" dialogwidth="980" dialogheight="550" readonly>
			</div>
		</div>

		<div class="control-group">
			<label style="width: 170px;" class="control-label">备注</label>
			<div class="controls">
				<textarea name="cancel[remark]" id="remark" style="width:500px;height:80px;"></textarea>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#cancel_order_number').leeQuote();

	$('#operation').change(function(){
		var t_val = $(this).val();
		if(t_val=='cancel'){
			$("#cOrder").hide();
		}
		if(t_val=='change'){
			$("#cOrder").show();
		}
	});
})
</script>