<div  id='order_penny_sel_json' style='display:none;'><?php echo json_encode($sel_data);?></div>
<?php //p($listData);?>
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="75">操作</th>
				<th><?php echo $labels['order_transfer_name'];?></th>
				<th><?php echo $labels['order_number'];?></th>
				<th><?php echo $labels['order_transfer_state'];?></th>
				<th><?php echo $labels['order_change_into'];?></th>
				<th><?php echo $labels['order_change_out'];?></th>
				<th><?php echo $labels['order_out_examine'];?></th>
				<!--<th><?php echo $labels['order_relation_order_id'];?></th>-->
				<th><?php echo $labels['order_change_into_money'];?></th>
				<th><?php echo $labels['order_transfer_date'];?></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($listData as $listData_value){?>
			<tr>
				<td style="text-align:left">
					<div class="btn-toolbar">
						<div class="btn-group">
							<a class="btn btn-small btn-primary" href="<?php echo site_url('www/order/view').'?order_id='.$listData_value['order_id']; ?>"<?php $user_auth = $this->user->user_auth($this->session->userdata('user_id'));if(!in_array('Sale_view',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?> target="_black">查看</a>
							<button class="btn btn-small dropdown-toggle  btn-primary" data-toggle="dropdown"<?php if(!in_array('Sale_view',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
							<?php if($listData_value['order_transfer_state']=="1001"){?>
								<li <?php if(!in_array('Penny_check',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a class='examine_penny' id="<?php echo $listData_value['order_id'];?>">审核内划订单</a>
								</li>
							<?php }?>
							<?php if($listData_value['order_transfer_state']=="1003"){?>
								<li <?php if(!in_array('Penny_edit',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a href="<?php echo site_url('www/order/update').'?order_id='.$listData_value['order_id']; ?>" >编辑</a>
								</li>
							<?php }?>
								<li <?php if(!in_array('Penny_del',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="del_penny">删除</a>
								</li>
							</ul>
						</div>
					</div>
				</td>
				<td><?php echo $listData_value['order_transfer_name'];?></td>
				<td><?php echo $listData_value['order_number'];?></td>
				<td>
				<?php
					if($listData_value['order_transfer_state']!=0 and $listData_value['order_transfer_state']!=""){
						echo $listData_value['order_transfer_state_arr']['enum_name'];
					}else{
						echo "&nbsp";
					}
				?>
				</td>
				<td>
					<?php
						if ($listData_value['order_change_into']!=0 and $listData_value['order_change_into']!=""){
							echo $listData_value['order_change_into_arr']['user_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>
				<td>
					<?php
						if ($listData_value['order_change_out']!=0 and $listData_value['order_change_out']!=""){
							echo $listData_value['order_change_out_arr']['user_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>
				<td>
					<?php
						if ($listData_value['order_out_examine']!=0 and $listData_value['order_out_examine']!=""){
							echo $listData_value['order_out_examine_arr']['user_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>
<!--				<td>
					<?php
//						if ($listData_value['order_relation_order_id']!=0 and $listData_value['order_relation_order_id']!=""){
//							echo $listData_value['order_relation_order_id_arr']['order_number'];
//						}else{
//							echo "&nbsp";
//						}
					?>
				</td>-->
				<td><?php echo $listData_value['order_change_into_money'];?></td>
				<td>
				<?php
					if($listData_value['order_transfer_date']!="" and $listData_value['order_transfer_date']!="0000-00-00"){
						echo date("Y-m-d",strtotime($listData_value['order_transfer_date']));
					}else{
						echo "&nbsp";
					}
				?>
				</td>
			</tr>
		<?php }?>
		</tbody>

	</table>
	<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber;?>条</div>
	<input type='hidden' id='totalNumber' value="<?php echo $totalNumber;?>">
	</div>

<script type="text/javascript">
$(document).ready(function () {
	//刷新分页
	$("#order_list").RefreshPage({
		page:<?php if(isset($page)){echo $page;}else{echo 1;}?>, //当前页码
		perNumber:20, //每页显示多少条数据
		totalNumber:<?php echo $totalNumber;?>, //数据总数
		sel_data:<?php echo json_encode($sel_data)?>,
		listUrl:'<?php echo site_url('www/order/ajax_select_penny'); ?>',
	});

	//审批内划订单
	$('.examine_penny').click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'order_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/order/ajax_examine_penny'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'审批内划订单',
			width:800,
			height:450,
			buttons: [{
				text:"审批通过",
				Class:"btn btn-primary",
				click: function(){
					formdata = $("#examine_penny_form").serializeArray();
					formdata[formdata.length]={"name":"order[order_transfer_state]","value":"1002"}; //添加1个参数
					formdata[formdata.length]={"name":"order[order_transfer_date]","value":"<?php echo date("Y-m-d H:i:s");?>"}; //添加1个参数
					//alert(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("审批通过");
								//再次加载1次列表页面
								$("#order_list").ajaxHtml({
									url:"<?php echo site_url('www/order/new_ajax_select?type_id=9'); ?>",
									data:<?php echo json_encode($sel_data);?>,
								});
								$('#operation_dialog').dialog("close");

							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/order/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"驳回审批",
				Class:"btn btn-success",
				click: function(){
					formdata = $("#examine_penny_form").serializeArray();
					formdata[formdata.length]={"name":"order[order_transfer_state]","value":"1003"}; //添加1个参数
					//alert(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("驳回审批");
								//再次加载1次列表页面
								$("#order_list").ajaxHtml({
									url:"<?php echo site_url('www/order/new_ajax_select?type_id=9'); ?>",
									data:<?php echo json_encode($sel_data);?>,
								});
								$('#operation_dialog').dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/order/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了审批返点");
					$(this).dialog("close");
				}
			}]

		});
		$('#operation_dialog').dialog('open');
	});

	//删除
	$(".del_penny").click(function(){
		id=$(this).attr('id');
		alertify.confirm("是否删除这个内划订单", function (e) {
			if (e) {
				$.ajax({
					'type':'post',
					'data':'order_id='+id,
					'success':function(data){
						if(data==1){
							alertify.success("删除成功");
							//再次加载1次列表页面
							$("#order_list").ajaxHtml({
								url:"<?php echo site_url('www/order/new_ajax_select?type_id=9'); ?>",
								data:<?php echo json_encode($sel_data);?>,
							});
						}else{
							alertify.alert('失败'+data);
						}
					},
					'url':'<?php echo site_url('www/order/ajax_del_post'); ?>',
					'async':false
				});
			} else {
				alertify.error("你放弃了删除");
			}
		});
	});
});

</script>