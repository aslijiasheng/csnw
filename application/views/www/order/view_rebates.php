<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						返点订单
						<span class="mini-title">
							Rebates Order
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
							<!--lee 按钮部分 start-->
							<a id="neworder" class="btn btn-primary" href="<?php echo site_url('www/order?type_id=' . $data['type_id']); ?>">
								<i class="icon-file"></i>
								返回列表
							</a>
							<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
								<div id="objlist" class="grid-view">
									<table class="leetable table table-bordered">
										<tbody>
											<tr>
												<td class="TbLeft"><?php echo $labels['order_number']; ?></td>
												<td class="TbRight" colspan='3'><?php echo $data['order_number']; ?></td>
											</tr>
											<!--所属子公司 2014/8/25-->
											<tr>
												<td class="TbLeft">
													<?php echo  $labels['order_subsidiary'];?>
												</td>
												<td class="TbRight" colspan='3'>
													<?php
													if ($data['order_subsidiary']!=0 and $data['order_subsidiary']!=""){
														echo $data['order_subsidiary_arr']['enum_name'];
													}else{
														echo "&nbsp";
													}
													?>
												</td>
											</tr>
											<!--所属子公司 2014/8/25-->
											<tr>
												<td class="TbLeft">代理商名称</td>
												<td class="TbRight">
													<?php echo !empty($data['order_agent']) ? $data['order_agent_arr']['partner_name'] : ''; ?>
												</td>
												<td class="TbLeft">代理商商业ID</td>
												<td class="TbRight">
													<?php echo !empty($data['order_agent']) ? $data['order_agent_arr']['partner_shopex_id'] : ''; ?>
												</td>
											</tr>
											<tr>
												<td class="TbLeft"><?php echo $labels['order_rebates_type']; ?></td>
												<td class="TbRight">
													<?php echo !empty($data['order_rebates_type']) ? $data['order_rebates_type']['enum_name'] : ''; ?>
												</td>
												<td class="TbLeft">创建者</td>
												<td class="TbRight">
													<?php echo !empty($data['order_create_user_id']) ? $data['order_create_user_id_arr']['user_name'] : ''; ?>
												</td>
											</tr>
											<tr>
												<td class="TbLeft"><?php echo $labels['order_review']; ?></td>
												<td class="TbRight">
													<?php echo !empty($data['order_review']) ? $data['order_review_arr']['user_name'] : ''; ?>
												</td>
												<td class="TbLeft"><?php echo $labels['order_finance']; ?></td>
												<td class="TbRight">
													<?php echo !empty($data['order_finance']) ? $data['order_finance_arr']['user_name'] : ''; ?>
												</td>
											</tr>
											<tr>
												<td class="TbLeft">返点金额</td>
												<td class="TbRight">
													<?php echo !empty($data['order_amount']) ? $data['order_amount']: ''; ?>
												</td>
												<td class="TbLeft">订单金额合计</td>
												<td class="TbRight"><?php echo $all_amount;?></td>
											</tr>
											<tr>
												<td class="TbLeft"><?php echo $labels['order_rebates_state']; ?></td>
												<td class="TbRight">
													<?php echo !empty($data['order_rebates_state']) ? $data['order_rebates_state']['enum_name'] : ''; ?>
												</td>
												<td class="TbLeft">渠道经理</td>
												<td class="TbRight"><?php if(isset($data['order_owner_arr']['user_name'])){echo $data['order_owner_arr']['user_name'];}?>&nbsp</td>
											</tr>
											<tr>
												<td class="TbLeft"><?php echo $labels['order_rebates_remark']; ?></td>
												<td class="TbRight" colspan='3'><?php echo !empty($data['order_rebates_remark']) ? $data['order_rebates_remark'] : ''; ?></td>
											</tr>
											<?php if(isset($data['order_if_renew']) && $data['order_if_renew']=='1002'): ?>
											<tr>
												<td class="TbLeft">
													是否续费订单
												</td>
												<td class="TbRight" colspan='3'>
													是
												</td>
											</tr>
											<?php endif; ?>
										</tbody>
									</table>
<!--这里是明细部分-->
<ul class="nav nav-tabs no-margin myTabBeauty">
	<li class="active">
		<a href="#rel_order" data-toggle="tab" data-original-title="">关联销售订单</a>
	</li>
	<li>
		<a href="#salespay_out" data-toggle="tab" data-original-title="">出账记录</a>
	</li>
	<li>
		<a href="#invoice" data-toggle="tab" data-original-title="">开票记录</a>
	</li>
	<li>
		<a href="#operation_log" data-toggle="tab" data-original-title="">操作日志</a>
	</li>
</ul>
<div id="myTabContent" class="tab-content">
	<div id="rel_order" class="tab-pane fade active in" leetype='ajaxHtml' url="<?php echo site_url('www/rel_order_order/detailed_list_view?order_id='.$data['order_id']);?>">关联销售订单</div>
	<div id="salespay_out" class="tab-pane fade" leetype='ajaxHtml' url="<?php echo site_url('www/salespay/detailed_list_view_out?order_id='.$data['order_id']);?>">出账记录</div>
	<div id="invoice" class="tab-pane fade" leetype='ajaxHtml' url="<?php echo site_url('www/invoice/detailed_list_view?order_id='.$data['order_id']);?>">开票记录</div>
	<div id="operation_log" class="tab-pane fade" leetype='ajaxHtml' url="<?php echo site_url('www/system_log/detailed_list_view?order_id='.$data['order_id']).'&module=Order';?>">操作日志</div>
</div>

<!--这里是明细部分-->
<script type="text/javascript">
$(document).ready(function () {

	//遍历所有的ajaxHtml,然后加载页面
	$("[leetype='ajaxHtml']").each(function(){
		$this=$(this);
		url=$this.attr('url');
		$.ajax({
			'type':'post',
			'success':function(data){
				$this.html(data);
			},
			'url':url,
			'async':false
		});
	});
	//通过active移动到这个页签的位子
	if ($_GET['active'])
	{
		$('[href="#'+$_GET['active']+'"]').tab('show');
		$('html,body').animate({scrollTop:$('[href="#'+$_GET['active']+'"]').position().top},500);//移动到#profile位子
	}


});
</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="operation_dialog"></div>


