	<div  id='order_sale_sel_json' style='display:none;'><?php echo json_encode($sel_data);?></div>

	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="75">操作</th>
				<th>订单编号</th>
				<th>订单总金额</th>
				<th>已到帐金额</th>
				<th>已到帐金额(测试)</th>
				<th>客户名称</th>
				<th>销售人员</th>
				<th>所属部门</th>
				<th>财务人员</th>
				<th>创建时间</th>
			</tr>
		</thead>

		<tbody>

		<?php foreach($listData as $listData_value){?>
			<?php  $user_auth = $this->user->user_auth($this->session->userdata('user_id')); ?>
			<tr>
				<td style="text-align:left">
					<div class="btn-toolbar">
						<div class="btn-group">
							<a class="btn btn-small btn-primary" href="<?php echo site_url('www/order/view').'?order_id='.$listData_value['order_id']; ?>"<?php $user_auth = $this->user->user_auth($this->session->userdata('user_id'));if(!in_array('Sale_view',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?> target="_black">查看</a>
						</div>
					</div>
				</td>
				<td>
				<?php echo $listData_value['order_number'];?>
				<?php echo $listData_value['order_is_cancel'] != '1001' ? '('.$listData_value['order_is_cancel_arr']['enum_name'].')' : '' ?>
				</td>

				<td>
				<?php  echo $listData_value['order_amount'];?>
				</td>

				<td>
				<?php  echo $listData_value['ydz_amount'];
					//echo $listData_value['order_having_account'];
				?>
				</td>
				
				<td>
				<?php  
					echo $listData_value['order_having_account'];
				?>
				</td>				

				<td>
					<?php
						if ($listData_value['order_account']!=0 and $listData_value['order_account']!=""){
							echo $listData_value['order_account_arr']['account_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>

				<td>
					<?php
						if ($listData_value['order_owner']!=0 and $listData_value['order_owner']!=""){
							echo $listData_value['order_owner_arr']['user_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>

				<td>
					<?php
						if ($listData_value['order_department']!=0 and $listData_value['order_department']!=""){
							echo $listData_value['order_department_arr']['department_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>

				<td>
					<?php
						if ($listData_value['order_finance']!=0 and $listData_value['order_finance']!=""){
							echo $listData_value['order_finance_arr']['user_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>

				<td>
				<?php  echo $listData_value['order_create_time'];?>
				</td>
			</tr>
		<?php }?>
		</tbody>
	</table>
	<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber;?>条</div>
	<input type='hidden' id='totalNumber' value="<?php echo $totalNumber;?>">
	</div>