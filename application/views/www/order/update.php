<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						订单新建页面
						<span class="mini-title">
						order
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/order/')?>">
<i class="icon-undo"></i>
返回订单列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php if(!isset($_GET['type_id'])){echo site_url('www/order/update?order_id='.$_GET['order_id']);}else{echo site_url('www/order/update?order_id='.$_GET['order_id'].'&type_id='.$_GET['type_id']);}?>" method="post">
		<?php if(1==1){?>
		 	<?php if(!isset($_GET['type_id'])){ ?>
		 	  
						<div class="control-group">
							<label class="control-label required" for="order_number">
								<?php echo $labels["order_number"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_number]" value="<?php echo $id_aData['order_number'] ?>" id="order_number">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_number]" id="order_number">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_user_id">
								<?php echo $labels["order_create_user_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_create_user_id]" id="order_create_user_id" value_id ="<?php if(isset($id_aData['order_create_user_id_arr']['user_id'])){echo $id_aData['order_create_user_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_create_user_id_arr']['user_name'])){echo $id_aData['order_create_user_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_time">
								<?php echo $labels["order_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="order_create_time">
	<input size="16" type="text" name="order[order_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#order_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_department">
								<?php echo $labels["order_department"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_department]" id="order_department" value_id ="<?php if(isset($id_aData['order_department_arr']['department_id'])){echo $id_aData['order_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['order_department_arr']['department_name'])){echo $id_aData['order_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_owner">
								<?php echo $labels["order_owner"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_owner]" id="order_owner" value_id ="<?php if(isset($id_aData['order_owner_arr']['user_id'])){echo $id_aData['order_owner_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_owner_arr']['user_name'])){echo $id_aData['order_owner_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_account">
								<?php echo $labels["order_account"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_account]" id="order_account" value_id ="<?php if(isset($id_aData['order_account_arr']['account_id'])){echo $id_aData['order_account_arr']['account_id']; } ?>" value="<?php if(isset($id_aData['order_account_arr']['account_name'])){echo $id_aData['order_account_arr']['account_name'];} ?>" url="<?php echo site_url('www/account/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_agent">
								<?php echo $labels["order_agent"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_agent]" id="order_agent" value_id ="<?php if(isset($id_aData['order_agent_arr']['account_id'])){echo $id_aData['order_agent_arr']['account_id']; } ?>" value="<?php if(isset($id_aData['order_agent_arr']['account_name'])){echo $id_aData['order_agent_arr']['account_name'];} ?>" url="<?php echo site_url('www/account/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_htbh">
								<?php echo $labels["order_agreement_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_agreement_no]" value="<?php echo $id_aData['order_agreement_no'] ?>" id="order_htbh">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_agreement_no]" id="order_htbh">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
		 	<?php }else{ ?>
		    <?php switch ($_GET["type_id"]){ 
							case -1:
							break;
						?>
						
						<?php case 6: ?>
						
						<div class="control-group">
							<label class="control-label required" for="order_number">
								<?php echo $labels["order_number"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_number]" value="<?php echo $id_aData['order_number'] ?>" id="order_number">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_number]" id="order_number">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_user_id">
								<?php echo $labels["order_create_user_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_create_user_id]" id="order_create_user_id" value_id ="<?php if(isset($id_aData['order_create_user_id_arr']['user_id'])){echo $id_aData['order_create_user_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_create_user_id_arr']['user_name'])){echo $id_aData['order_create_user_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_time">
								<?php echo $labels["order_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="order_create_time">
	<input size="16" type="text" name="order[order_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#order_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_department">
								<?php echo $labels["order_department"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_department]" id="order_department" value_id ="<?php if(isset($id_aData['order_department_arr']['department_id'])){echo $id_aData['order_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['order_department_arr']['department_name'])){echo $id_aData['order_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_owner">
								<?php echo $labels["order_owner"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_owner]" id="order_owner" value_id ="<?php if(isset($id_aData['order_owner_arr']['user_id'])){echo $id_aData['order_owner_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_owner_arr']['user_name'])){echo $id_aData['order_owner_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_account">
								<?php echo $labels["order_account"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_account]" id="order_account" value_id ="<?php if(isset($id_aData['order_account_arr']['account_id'])){echo $id_aData['order_account_arr']['account_id']; } ?>" value="<?php if(isset($id_aData['order_account_arr']['account_name'])){echo $id_aData['order_account_arr']['account_name'];} ?>" url="<?php echo site_url('www/account/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_agent">
								<?php echo $labels["order_agent"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_agent]" id="order_agent" value_id ="<?php if(isset($id_aData['order_agent_arr']['account_id'])){echo $id_aData['order_agent_arr']['account_id']; } ?>" value="<?php if(isset($id_aData['order_agent_arr']['account_name'])){echo $id_aData['order_agent_arr']['account_name'];} ?>" url="<?php echo site_url('www/account/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_agreement_no">
								<?php echo $labels["order_agreement_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_agreement_no]" value="<?php echo $id_aData['order_agreement_no'] ?>" id="order_agreement_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_agreement_no]" id="order_agreement_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_amount">
								<?php echo $labels["order_amount"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_amount]" value="<?php echo $id_aData['order_amount'] ?>" id="order_amount">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_amount]" id="order_amount">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_finance">
								<?php echo $labels["order_finance"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_finance]" id="order_finance" value_id ="<?php if(isset($id_aData['order_finance_arr']['user_id'])){echo $id_aData['order_finance_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_finance_arr']['user_name'])){echo $id_aData['order_finance_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_agreement_name">
								<?php echo $labels["order_agreement_name"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_agreement_name]" value="<?php echo $id_aData['order_agreement_name'] ?>" id="order_agreement_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_agreement_name]" id="order_agreement_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_review">
								<?php echo $labels["order_review"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_review]" id="order_review" value_id ="<?php if(isset($id_aData['order_review_arr']['user_id'])){echo $id_aData['order_review_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_review_arr']['user_name'])){echo $id_aData['order_review_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_nreview">
								<?php echo $labels["order_nreview"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_nreview]" id="order_nreview" value_id ="<?php if(isset($id_aData['order_nreview_arr']['user_id'])){echo $id_aData['order_nreview_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_nreview_arr']['user_name'])){echo $id_aData['order_nreview_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_if_renew">
								<?php echo $labels["order_if_renew"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($order_if_renew_enum as $if_renew_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $if_renew_v['enum_key']==$id_aData['order_if_renew_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $if_renew_v['enum_key']?>" name="order[order_if_renew]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $if_renew_v['enum_key']?>" name="order[order_if_renew]">
<?php }?>
<?php echo $if_renew_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
				       <?php break; ?>
						
						<?php case 7: ?>
						
						<div class="control-group">
							<label class="control-label required" for="order_number">
								<?php echo $labels["order_number"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_number]" value="<?php echo $id_aData['order_number'] ?>" id="order_number">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_number]" id="order_number">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_user_id">
								<?php echo $labels["order_create_user_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_create_user_id]" id="order_create_user_id" value_id ="<?php if(isset($id_aData['order_create_user_id_arr']['user_id'])){echo $id_aData['order_create_user_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_create_user_id_arr']['user_name'])){echo $id_aData['order_create_user_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_time">
								<?php echo $labels["order_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="order_create_time">
	<input size="16" type="text" name="order[order_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#order_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_department">
								<?php echo $labels["order_department"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_department]" id="order_department" value_id ="<?php if(isset($id_aData['order_department_arr']['department_id'])){echo $id_aData['order_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['order_department_arr']['department_name'])){echo $id_aData['order_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
				       <?php break; ?>
						
						<?php case 8: ?>
						
						<div class="control-group">
							<label class="control-label required" for="order_number">
								<?php echo $labels["order_number"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_number]" value="<?php echo $id_aData['order_number'] ?>" id="order_number">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_number]" id="order_number">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_user_id">
								<?php echo $labels["order_create_user_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_create_user_id]" id="order_create_user_id" value_id ="<?php if(isset($id_aData['order_create_user_id_arr']['user_id'])){echo $id_aData['order_create_user_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_create_user_id_arr']['user_name'])){echo $id_aData['order_create_user_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_time">
								<?php echo $labels["order_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="order_create_time">
	<input size="16" type="text" name="order[order_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#order_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_department">
								<?php echo $labels["order_department"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_department]" id="order_department" value_id ="<?php if(isset($id_aData['order_department_arr']['department_id'])){echo $id_aData['order_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['order_department_arr']['department_name'])){echo $id_aData['order_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
				       <?php break; ?>
						
						<?php case 9: ?>
						
						<div class="control-group">
							<label class="control-label required" for="order_number">
								<?php echo $labels["order_number"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_number]" value="<?php echo $id_aData['order_number'] ?>" id="order_number">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_number]" id="order_number">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_user_id">
								<?php echo $labels["order_create_user_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_create_user_id]" id="order_create_user_id" value_id ="<?php if(isset($id_aData['order_create_user_id_arr']['user_id'])){echo $id_aData['order_create_user_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_create_user_id_arr']['user_name'])){echo $id_aData['order_create_user_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_time">
								<?php echo $labels["order_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="order_create_time">
	<input size="16" type="text" name="order[order_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#order_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_department">
								<?php echo $labels["order_department"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_department]" id="order_department" value_id ="<?php if(isset($id_aData['order_department_arr']['department_id'])){echo $id_aData['order_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['order_department_arr']['department_name'])){echo $id_aData['order_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
				       <?php break; ?>
						
						<?php case 10: ?>
						
						<div class="control-group">
							<label class="control-label required" for="order_number">
								<?php echo $labels["order_number"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_number]" value="<?php echo $id_aData['order_number'] ?>" id="order_number">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_number]" id="order_number">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_user_id">
								<?php echo $labels["order_create_user_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_create_user_id]" id="order_create_user_id" value_id ="<?php if(isset($id_aData['order_create_user_id_arr']['user_id'])){echo $id_aData['order_create_user_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_create_user_id_arr']['user_name'])){echo $id_aData['order_create_user_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_time">
								<?php echo $labels["order_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="order_create_time">
	<input size="16" type="text" name="order[order_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#order_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_department">
								<?php echo $labels["order_department"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_department]" id="order_department" value_id ="<?php if(isset($id_aData['order_department_arr']['department_id'])){echo $id_aData['order_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['order_department_arr']['department_name'])){echo $id_aData['order_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
				       <?php break; ?>
						
						<?php default: ?>
						
						<div class="control-group">
							<label class="control-label required" for="order_number">
								<?php echo $labels["order_number"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_number]" value="<?php echo $id_aData['order_number'] ?>" id="order_number">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_number]" id="order_number">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_user_id">
								<?php echo $labels["order_create_user_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_create_user_id]" id="order_create_user_id" value_id ="<?php if(isset($id_aData['order_create_user_id_arr']['user_id'])){echo $id_aData['order_create_user_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_create_user_id_arr']['user_name'])){echo $id_aData['order_create_user_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_time">
								<?php echo $labels["order_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="order_create_time">
	<input size="16" type="text" name="order[order_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#order_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_department">
								<?php echo $labels["order_department"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_department]" id="order_department" value_id ="<?php if(isset($id_aData['order_department_arr']['department_id'])){echo $id_aData['order_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['order_department_arr']['department_name'])){echo $id_aData['order_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_owner">
								<?php echo $labels["order_owner"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_owner]" id="order_owner" value_id ="<?php if(isset($id_aData['order_owner_arr']['user_id'])){echo $id_aData['order_owner_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_owner_arr']['user_name'])){echo $id_aData['order_owner_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_account">
								<?php echo $labels["order_account"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_account]" id="order_account" value_id ="<?php if(isset($id_aData['order_account_arr']['account_id'])){echo $id_aData['order_account_arr']['account_id']; } ?>" value="<?php if(isset($id_aData['order_account_arr']['account_name'])){echo $id_aData['order_account_arr']['account_name'];} ?>" url="<?php echo site_url('www/account/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_agent">
								<?php echo $labels["order_agent"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_agent]" id="order_agent" value_id ="<?php if(isset($id_aData['order_agent_arr']['account_id'])){echo $id_aData['order_agent_arr']['account_id']; } ?>" value="<?php if(isset($id_aData['order_agent_arr']['account_name'])){echo $id_aData['order_agent_arr']['account_name'];} ?>" url="<?php echo site_url('www/account/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_htbh">
								<?php echo $labels["order_agreement_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_agreement_no]" value="<?php echo $id_aData['order_agreement_no'] ?>" id="order_htbh">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_agreement_no]" id="order_htbh">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
					<?php }?>
                <?php }}else{ ?>
                  
						<div class="control-group">
							<label class="control-label required" for="order_number">
								<?php echo $labels["order_number"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_number]" value="<?php echo $id_aData['order_number'] ?>" id="order_number">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_number]" id="order_number">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_user_id">
								<?php echo $labels["order_create_user_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_create_user_id]" id="order_create_user_id" value_id ="<?php if(isset($id_aData['order_create_user_id_arr']['user_id'])){echo $id_aData['order_create_user_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_create_user_id_arr']['user_name'])){echo $id_aData['order_create_user_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_create_time">
								<?php echo $labels["order_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="order_create_time">
	<input size="16" type="text" name="order[order_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['order_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#order_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_department">
								<?php echo $labels["order_department"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_department]" id="order_department" value_id ="<?php if(isset($id_aData['order_department_arr']['department_id'])){echo $id_aData['order_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['order_department_arr']['department_name'])){echo $id_aData['order_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_owner">
								<?php echo $labels["order_owner"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_owner]" id="order_owner" value_id ="<?php if(isset($id_aData['order_owner_arr']['user_id'])){echo $id_aData['order_owner_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_owner_arr']['user_name'])){echo $id_aData['order_owner_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_account">
								<?php echo $labels["order_account"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_account]" id="order_account" value_id ="<?php if(isset($id_aData['order_account_arr']['account_id'])){echo $id_aData['order_account_arr']['account_id']; } ?>" value="<?php if(isset($id_aData['order_account_arr']['account_name'])){echo $id_aData['order_account_arr']['account_name'];} ?>" url="<?php echo site_url('www/account/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_agent">
								<?php echo $labels["order_agent"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order[order_agent]" id="order_agent" value_id ="<?php if(isset($id_aData['order_agent_arr']['account_id'])){echo $id_aData['order_agent_arr']['account_id']; } ?>" value="<?php if(isset($id_aData['order_agent_arr']['account_name'])){echo $id_aData['order_agent_arr']['account_name'];} ?>" url="<?php echo site_url('www/account/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_htbh">
								<?php echo $labels["order_agreement_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order[order_agreement_no]" value="<?php echo $id_aData['order_agreement_no'] ?>" id="order_htbh">
<?php }else{ ?>
<input type="text" maxlength="128" name="order[order_agreement_no]" id="order_htbh">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
                 <?php }?>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>