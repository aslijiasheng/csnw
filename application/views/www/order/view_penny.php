<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						内部划款订单
						<span class="mini-title">
						Penny Order
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="neworder" class="btn btn-primary" href="<?php echo site_url('www/order?type_id='.$data['type_id']);?>">
<i class="icon-file"></i>
返回列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
<table class="leetable table table-bordered">
	<tbody>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_transfer_name'];?>
			</td>
			<td class="TbRight" colspan='3'>
				<?php echo $data['order_transfer_name'];?>
			</td>
		</tr>
		<!--所属子公司 2014/8/25-->
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_subsidiary'];?>
			</td>
			<td class="TbRight" colspan='3'>
				<?php
				if ($data['order_subsidiary']!=0 and $data['order_subsidiary']!=""){
					echo $data['order_subsidiary_arr']['enum_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>
		<!--所属子公司 2014/8/25-->
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_number'];?>
			</td>
			<td class="TbRight">
				<?php echo $data['order_number'];?>
			</td>
			<td class="TbLeft">
				<?php echo $labels['order_transfer_date'];?>
			</td>
			<td class="TbRight">
				<?php 
					if($data['order_transfer_date']!="" and $data['order_transfer_date']!="0000-00-00"){
						echo date("Y-m-d",strtotime($data['order_transfer_date']));
					}else{
						echo "&nbsp";
					}
				?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_change_into'];?>
			</td>
			<td class="TbRight">
				<?php 
				if ($data['order_change_into']!=0 and $data['order_change_into']!=""){
					echo $data['order_change_into_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft">
				<?php echo  $labels['order_change_out'];?>
			</td>
			<td class="TbRight">
				<?php 
				if ($data['order_change_out']!=0 and $data['order_change_out']!=""){
					echo $data['order_change_out_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_out_examine'];?>
			</td>
			<td class="TbRight">
				<?php 
				if ($data['order_out_examine']!=0 and $data['order_out_examine']!=""){
					echo $data['order_out_examine_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft">
				<?php echo $labels['order_transfer_state'];?>
			</td>
			<td class="TbRight">
				<?php 
				if ($data['order_transfer_state']!=0 and $data['order_transfer_state']!=""){
					echo $data['order_transfer_state_arr']['enum_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_relation_order_id'];?>
			</td>
			<td class="TbRight">
				<?php 
				if ($data['order_relation_order_id']!=0 and $data['order_relation_order_id']!=""){
					echo $data['order_relation_order_id_arr']['order_number'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft">
				<?php echo  $labels['order_change_into_money'];?>
			</td>
			<td class="TbRight">
				<?php echo $data['order_change_into_money'];?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_transfer_reason'];?>
			</td>
			<td class="TbRight" colspan='3'>
				<pre><?php echo $data['order_transfer_reason'];?></pre>
			</td>
		</tr>
	</tbody>
</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="operation_dialog"></div>


