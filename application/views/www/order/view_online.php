<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						线上订单
						<span class="mini-title">
						On-line Order
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="neworder" class="btn btn-primary" href="<?php echo site_url('www/order?type_id='.$data['type_id']);?>">
<i class="icon-file"></i>
返回列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
<table class="leetable table table-bordered">
	<tbody>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_number'];?>
			</td>
			<td class="TbRight" colspan='3'>
				<?php echo $data['order_number'];?>
			</td>
		</tr>
		<!--所属子公司 2014/8/25-->
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_subsidiary'];?>
			</td>
			<td class="TbRight" colspan='3'>
				<?php
				if ($data['order_subsidiary']!=0 and $data['order_subsidiary']!=""){
					echo $data['order_subsidiary_arr']['enum_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>
		<!--所属子公司 2014/8/25-->
		<tr>
			<td class="TbLeft">
				录入人
			</td>
			<td class="TbRight">
				<?php
				if ($data['order_typein']!=0 and $data['order_typein']!=""){
					echo $data['order_typein_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft">
				<?php echo  $labels['order_create_time'];?>
			</td>
			<td class="TbRight">
				<?php echo $data['order_create_time'];?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_owner'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($data['order_owner']!=0 and $data['order_owner']!=""){
					echo $data['order_owner_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft">
				<?php echo  $labels['order_department'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($data['order_department']!=0 and $data['order_department']!=""){
					//直接判断，不使用递归
					$d1=$d2=$d3=$d4=$d5='';
					$d1 = $data['order_department_arr']['department_name'];
					if(isset($data['order_department_arr']['department_uid_arr'])){
						$d2 = $data['order_department_arr']['department_uid_arr']['department_name'];
						if(isset($data['order_department_arr']['department_uid_arr']['department_uid_arr'])){
							$d3 = $data['order_department_arr']['department_uid_arr']['department_uid_arr']['department_name'];
							if(isset($data['order_department_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr'])){
								$d4 = $data['order_department_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr']['department_name'];
								if(isset($data['order_department_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr'])){
									$d5 = $data['order_department_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr']['department_name'];
								}
							}
						}
					}
					$s = trim($d5 .' - '.$d4.' - '.$d3.' - '.$d2.' - '.$d1 , ' - ');
					echo $s;
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo $labels['order_agreement_name'];?>
			</td>
			<td class="TbRight">
				<?php echo $data['order_agreement_name'];?>
			</td>
			<td class="TbLeft">
				<?php echo $labels['order_agreement_no'];?>
			</td>
			<td class="TbRight">
				<?php echo $data['order_agreement_name'];?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_account'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($data['order_account']!=0 and $data['order_account']!=""){
					echo $data['order_account_arr']['account_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft">
				客户商业ID
			</td>
			<td class="TbRight">
				<?php
				if ($data['order_account']!=0 and $data['order_account']!=""){
					echo $data['order_account_arr']['account_account_shopexid'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_agent'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($data['order_agent']!=0 and $data['order_agent']!=""){
					echo $data['order_agent_arr']['partner_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft">
				邮箱
			</td>
			<td class="TbRight">
				<?php
				if ($data['order_account']!=0 and $data['order_account']!=""){
					echo $data['order_account_arr']['account_email'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_finance'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($data['order_finance']!=0 and $data['order_finance']!=""){
					echo $data['order_finance_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft">
				<?php echo  $labels['order_review'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($data['order_review']!=0 and $data['order_review']!=""){
					echo $data['order_review_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_nreview'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($data['order_nreview']!=0 and $data['order_nreview']!=""){
					echo $data['order_nreview_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft"></td>
			<td class="TbRight"></td>
		</tr>
	</tbody>
</table>

<!--这里是明细部分-->

<ul class="nav nav-tabs no-margin myTabBeauty">
	<li class="active">
		<a href="#home" data-toggle="tab" data-original-title="111">产品明细</a>
	</li>
	<li>
		<a href="#salespay_into" data-toggle="tab" data-original-title="">入账记录</a>
	</li>
	<li>
		<a href="#invoice" data-toggle="tab" data-original-title="">开票记录</a>
	</li>
</ul>
<div id="myTabContent" class="tab-content">
	<div id="home" class="tab-pane fade  active in" leetype='ajaxHtml' url="<?php echo site_url('www/order_d/detailed_list_view?order_id='.$data['order_id']);?>">产品明细</div>
	<div id="salespay_into" class="tab-pane fade" leetype='ajaxHtml' url="<?php echo site_url('www/salespay/detailed_list_view_into?order_id='.$data['order_id']);?>">入账记录</div>
	<div id="invoice" class="tab-pane fade" leetype='ajaxHtml' url="<?php echo site_url('www/invoice/detailed_list_view?order_id='.$data['order_id']);?>">开票记录</div>
</div>

<!--这里是明细部分-->

<script type="text/javascript">
$(document).ready(function () {

	//遍历所有的ajaxHtml,然后加载页面
	$("[leetype='ajaxHtml']").each(function(){
		$this=$(this);
		url=$this.attr('url');
		$.ajax({
			'type':'post',
			'success':function(data){
				$this.html(data);
			},
			'url':url,
			'async':false
		});
	});
	//通过active移动到这个页签的位子
	if ($_GET['active'])
	{
		$('[href="#'+$_GET['active']+'"]').tab('show');
		$('html,body').animate({scrollTop:$('[href="#'+$_GET['active']+'"]').position().top},500);//移动到#profile位子
	}


});
</script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="operation_dialog"></div>


