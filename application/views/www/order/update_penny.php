<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						编辑内划订单
						<span class="mini-title">
						Edit Penny Order
						</span>
					</div>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/order/index?type_id=9')?>">
<i class="icon-undo"></i>
返回内划订单列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php echo site_url('www/order/update?order_id='.$_GET['order_id']);?>" method="post">
		<!--隐藏输入框，默认每次新建，内划状态都变为'新订单'-->
		<input type="hidden" name="order[order_transfer_state]" value="1001" id="transfer_state">
		<div class="control-group">
			<label class="control-label required" for="transfer_name">
				<?php echo $labels["order_transfer_name"];?>
			</label>
			<div class="controls">
				<input type="text" name="order[order_transfer_name]" value="<?php echo $id_aData["order_transfer_name"];?>" id="transfer_name">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="change_out">
				<?php echo $labels["order_change_out"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="order[order_change_out]" id="change_out" value_id ="<?php echo $id_aData["order_change_out"];?>" value="<?php if($id_aData["order_change_out"]!="" and $id_aData["order_change_out"]!=0){echo $id_aData["order_change_out_arr"]['user_name'];}?>" url="<?php echo site_url('www/user/ajax_list?tag_name=change_out') ?>" leetype="quote">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="change_into">
				<?php echo $labels["order_change_into"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="order[order_change_into]" id="change_into" value_id ="<?php echo $id_aData["order_change_into"];?>" value="<?php if($id_aData["order_change_into"]!="" and $id_aData["order_change_into"]!=0){echo $id_aData["order_change_into_arr"]['user_name'];}?>" url="<?php echo site_url('www/user/ajax_list?tag_name=change_into') ?>" leetype="quote">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="out_examine">
				<?php echo $labels["order_out_examine"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
<input type="text" name="order[order_out_examine]" id="out_examine" value_id ="<?php echo $id_aData["order_out_examine"];?>" value="<?php if($id_aData["order_out_examine"]!="" and $id_aData["order_out_examine"]!=0){echo $id_aData["order_out_examine_arr"]['user_name'];}?>">
<script type="text/javascript">
$(document).ready(function () {
	$('#out_examine').leeQuote({
		url:'<?php echo site_url('www/user/ajax_list?tag_name=out_examine');?>',
		title:'选择主管人员',
		data:{
			'rel_role_id':2,
		}
	});
});
</script>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="order_relation_order_id">
				<?php echo $labels["order_relation_order_id"];?>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="order[order_relation_order_id]" id="order_relation_order_id" value_id ="<?php echo $id_aData["order_relation_order_id"];?>" value="<?php if($id_aData["order_relation_order_id"]!="" and $id_aData["order_relation_order_id"]!=0){echo $id_aData["order_relation_order_id_arr"]['order_number'];}?>" url="<?php echo site_url('www/order/ajax_list?type_id=6&tag_name=order_relation_order_id') ?>" leetype="quote">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="order_change_into_money">
				<?php echo $labels["order_change_into_money"];?>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="order[order_change_into_money]" value="<?php echo $id_aData["order_change_into_money"];?>" id="order_change_into_money">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="order_transfer_reason">
				<?php echo $labels["order_transfer_reason"];?>
			</label>
			<div class="controls">
				<textarea id="order_transfer_reason" rows="6" style="width:400px" name="order[order_transfer_reason]"><?php echo $id_aData["order_transfer_reason"];?></textarea>
			</div>
		</div>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>