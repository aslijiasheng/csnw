<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						<?php if(isset($_GET['type_id'])){
							if(isset($type_arr[$_GET['type_id']]['type_name'])){
								echo $type_arr[$_GET['type_id']]['type_name'];
							}else{
								echo '订单';
							}
						}else{
							echo '订单';
						}
						?>
						<span class="mini-title">
						order
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<div class="btn-group">
<a id="neworder" class="btn btn-primary" href="<?php echo site_url('www/order/add')?>" data-toggle="dropdown">
<i class="icon-file"></i>
新增订单
</a>
<ul class="dropdown-menu" role="menu">
	<?php foreach ($type_arr as $k=>$v){ ?>
	<li>
	<a href="<?php echo site_url('www/order/add?type_id='.$v['type_id']);?>">新增<?php echo $v['type_name'];?></a>
	</li>
	<?php }?>
</ul>
</div>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">

<div class="grid-view">
	<div id="order_list"></div>
<div>
<script type="text/javascript">
$(document).ready(function () {
	$selectAttr=[
		
		{'value':'number','txt':'订单编号'},
		
		{'value':'create_time','txt':'订单创建时间'},
		
		{'value':'department','txt':'所属部门'},
		
		{'value':'owner','txt':'所属销售'},
		
		{'value':'create_user_id','txt':'订单创建者id'},
		
		{'value':'account','txt':'所属客户'},
		
		{'value':'agent','txt':'所属代理商'},
		
		{'value':'agreement_no','txt':'合同编号'},
		
	];
	
	$("#order_list").leeDataTable({
		selectAttr:$selectAttr, //简单查询的查询属性
		url:"<?php echo site_url('www/order/ajax_select'); ?>", //ajax查询的地址
		perNumber:5 //每页显示多少条数据
	});

	//添加新增入款项功能
	$('.add_salespay').click(function(){
		alert($(this).attr('id'));//首先获取者个按钮上的ID属性
		//alert("新增入款项");
	});


});

</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

