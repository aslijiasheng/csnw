<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						<?php
						if (isset($_GET['type_id'])) {
							if (isset($type_arr[$_GET['type_id']]['type_name'])) {
								echo $type_arr[$_GET['type_id']]['type_name'];
							} else {
								echo '订单';
							}
						} else {
							echo '订单';
						}
						?>
						<span class="mini-title">
							<?php echo $type_arr[$_GET['type_id']]['type_label']; ?> order
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
							<!--lee 按钮部分 start-->
							<!--
							<div class="btn-group">
							<a id="neworder" class="btn btn-primary" href="<?php // echo site_url('www/order/add')                  ?>" data-toggle="dropdown" <?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (!in_array('order_add', $user_auth['activity_auth_arr'])) {
								echo 'style="display:none;"';
							}
							?>>
							<i class="icon-file"></i>
							新增订单
							</a>
							<ul class="dropdown-menu" role="menu">
							<?php foreach ($type_arr as $k => $v) { ?>
																						<li <?php
								if (!in_array($v['type_label'] . '_add', $user_auth['activity_auth_arr'])) {
									echo 'style="display:none;"';
								}
								?>><a href="<?php echo site_url('www/order/add?type_id=' . $v['type_id']); ?>">新增<?php echo $v['type_name']; ?></a></li>
							<?php } ?>
							</ul>
							</div>
							-->

							<?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (in_array('sale_gjss', $user_auth['activity_auth_arr'])) {
								?>
								<a id="order_sale_seniorquery" class="btn btn-primary">
									<i class="icon-search"></i>
									高级查询
								</a>
								<script type="text/javascript">
									//高级查询
									$(document).ready(function() {
										$seniorquery_attr = <?php echo json_encode($seniorquery_attr); ?>;
										$("#order_sale_seniorquery").seniorquery({
											selectAttr: $seniorquery_attr,
											list_id: 'order_list',
											url: "<?php echo site_url('www/order/ajax_select_renew'); ?>", //ajax查询的地址
											perNumber: 20 //每页显示多少条数据(因为暂时没想通这里要怎么获取！所以直接赋值先)
										});
									});
								</script>
							<?php } ?>
						</div>
						<div id="divmessagelist">
							<div class="grid-view">
								<div id="order_list"></div>
								<div>
									<script type="text/javascript">
										$(document).ready(function() {
											$selectAttr = [
												{'value': 'number', 'txt': '订单编号'},
												{'value': 'owner.name', 'txt': '销售人员'},
												{'value': 'account.name', 'txt': '客户名称'},
												{'value': 'department.name', 'txt': '所属部门'},
												{'value': 'account.account_shopex_id', 'txt': '客户商业ID'},
												{'value': 'agent.shopex_id', 'txt': '代理商ID'},
												{'value': 'agreement_no', 'txt': '合同编号'},
												{'value': 'review.name', 'txt': '业务主管'},
												{'value': 'finance.name', 'txt': '财务人员'},
												{'value': 'if_renew.name', 'txt': '否续费订单'}
											];
											$("#order_list").leeDataTable({
												selectAttr: $selectAttr, //简单查询的查询属性
												url: "<?php echo site_url('www/order/ajax_select'); ?>", //ajax查询的地址
												perNumber: 20, //每页显示多少条数据
												obj: 'order',
												where:<?php echo $data_auth['where']; ?>,
												where_rel: '<?php echo $data_auth['where_rel']; ?>',
											});
											$('#order_list_select_attr').change(function() {
												$('#order_list_select_attr').parent().next().empty();
												if ($(this).val() == 'if_renew.name') {
													var html = '<select name="order_list_select_value_name"><option value="是">是</option><option value="否">否</option></select>';
												} else {
													var html = '<input type="text" name="order_list_select_value_name">';
												}
												$('#order_list_select_attr').parent().next().append(html);
											});
										});
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="operation_dialog"></div>