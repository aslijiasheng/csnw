<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						新建内部划款订单
						<span class="mini-title">
						Add Penny Order
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/order/index?type_id=9')?>">
<i class="icon-undo"></i>
返回内划订单列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php if(!isset($_GET['type_id'])){echo site_url('www/order/add');}else{echo site_url('www/order/add?type_id='.$_GET['type_id']);}?>" method="post">
		<!--隐藏输入框，默认每次新建，内划状态都变为'新订单'-->
		<input type="hidden" name="order[transfer_state]" value="1001" id="transfer_state">
		<div class="control-group">
			<label class="control-label required" for="transfer_name">
				<?php echo $labels["order_transfer_name"];?>
			</label>
			<div class="controls">
				<input type="text" name="order[transfer_name]" value="" id="transfer_name">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="change_out">
				<?php echo $labels["order_change_out"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="order[change_out]" id="change_out" value_id ="" value="" url="<?php echo site_url('www/user/ajax_list?tag_name=change_out') ?>" leetype="quote">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="change_into">
				<?php echo $labels["order_change_into"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="order[change_into]" id="change_into" value_id ="" value="" url="<?php echo site_url('www/user/ajax_list?tag_name=change_into') ?>" leetype="quote">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="out_examine">
				<?php echo $labels["order_out_examine"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
<input type="text" name="order[out_examine]" id="out_examine" value_id ="" value="">
<script type="text/javascript">
$(document).ready(function () {
	$('#out_examine').leeQuote({
		url:'<?php echo site_url('www/user/ajax_list?tag_name=out_examine');?>',
		title:'选择主管人员',
		data:{
			'rel_role_id':2,
		}
	});
});
</script>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="order_relation_order_id">
				<?php echo $labels["order_relation_order_id"];?>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="order[relation_order_id]" id="order_relation_order_id" value_id ="" value="" url="<?php echo site_url('www/order/ajax_list?type_id=6&tag_name=order_relation_order_id') ?>" leetype="quote">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="order_change_into_money">
				<?php echo $labels["order_change_into_money"];?>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="order[change_into_money]" value="" id="order_change_into_money">
			</div>
		</div>


		<!--
		<div class="control-group">
			<label class="control-label required" for="order_transfer_date">
				<?php echo $labels["order_transfer_date"];?>
			</label>
			<div class="controls">
				<div class="input-append date form_date" id="order_transfer_date">
					<input size="16" type="text" name="order[transfer_date]" value="" style="width: 154px;">
					<span class="add-on"><i class="icon-close"></i></span>
					<span class="add-on"><i class="icon-clock"></i></span>
				</div>
				<script type="text/javascript">
				$(document).ready(function () {
					$('#order_transfer_date').datetimepicker({
						language: 'zh-CN',
						weekStart: 1,
						todayBtn:  1,
						autoclose: 1,
						todayHighlight: 1,
						startView: 2,
						minView: 2,
						forceParse: 0,
						format:'yyyy-mm-dd'
					});
				});
				</script>
			</div>
		</div>
		-->
		<div class="control-group">
			<label class="control-label required" for="order_transfer_reason">
				<?php echo $labels["order_transfer_reason"];?>
			</label>
			<div class="controls">
				<textarea id="order_transfer_reason" rows="6" style="width:400px" name="order[transfer_reason]"></textarea>
			</div>
		</div>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>