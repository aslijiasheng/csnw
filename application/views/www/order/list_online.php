<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						<?php
						if (isset($_GET['type_id'])) {
							if (isset($type_arr[$_GET['type_id']]['type_name'])) {
								echo $type_arr[$_GET['type_id']]['type_name'];
							} else {
								echo '订单';
							}
						} else {
							echo '订单';
						}
						?>
						<span class="mini-title">
							<?php echo $type_arr[$_GET['type_id']]['type_label']; ?> Order
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
							<!--lee 按钮部分 start-->
							<?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (in_array('online_dr', $user_auth['activity_auth_arr'])) {
								?>
								<form action="<?php echo site_url('www/introduction/execlFile?obj=order_online') ?>" method="post" enctype="multipart/form-data">
									<a id="Admit_it_into" class="btn btn-primary" leetype='seniorquery'>
										<i class="icon-download-2"></i>
										订单导入
										<input id="fileupload" name="file" type="file" leetype='file'>
									</a>
								</form>
							<?php } ?>
							<script type="text/javascript">
								$("[leetype='file']").each(function() {
									//透明度改成0
									$(this).attr('style', 'opacity:0;position:absolute;');
									//获取父对象的宽度以及高度、还有坐标
									$closest = $(this).closest('a');
									//alert($closest.width());
									$(this).width($closest.width() + 25);
									$(this).height($closest.height() + 10);
									$(this).offset($closest.offset());
									$(this).change(function() {
										$(this).closest('form').submit();
									});
								});
							</script>


							<?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (in_array('online_pldz', $user_auth['activity_auth_arr'])) {
								?>
								<div class="btn-group">
									<a id="recon" class="btn btn-primary">
										<i class="icon-file"></i>
										批量对账
									</a>
								</div>
							<?php } ?>
							<?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (in_array('online_gjss', $user_auth['activity_auth_arr'])) {
								?>
								<a id="order_online_seniorquery" class="btn btn-primary">
									<i class="icon-search"></i>
									高级查询
								</a>
								<script type="text/javascript">
									//高级查询
									$(document).ready(function() {
										$seniorquery_attr = <?php echo json_encode($seniorquery_attr); ?>;
										$("#order_online_seniorquery").seniorquery({
											selectAttr: $seniorquery_attr,
											list_id: 'order_list',
											url: "<?php echo site_url('www/order/ajax_select_online'); ?>", //ajax查询的地址
											perNumber: 20 //每页显示多少条数据(因为暂时没想通这里要怎么获取！所以直接赋值先)
										});
									});
								</script>
							<?php } ?>
							<?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (in_array('online_pldc', $user_auth['activity_auth_arr'])) {
								?>
								<a id="order_online_export" class="btn btn-primary">
									<i class="icon-upload-2"></i>
									批量导出
								</a>
								<div id='order_online_export_div' style='display:none;'></div>
								<script type="text/javascript">
									//导出
									$(document).ready(function() {
										$("#order_online_export").click(function() {
											$url = '<?php echo site_url('www/export/execlExport'); ?>';
											$order_online_sel_json = $("#order_online_sel_json").html();
											$formdata = {'sel_json': $order_online_sel_json};
											$('#order_online_export_div').ajaxHtml({
												url: $url,
												data: $formdata,
											});
											$("#order_online_export_div").dialog({
												title: "批量导出",
												modal: true,
												width: 700,
												height: 500,
												buttons: [{
														text: "导出",
														Class: "btn bottom-margin btn-primary",
														click: function() {
															//获取所有选择的属性名称
															$formdata2 = {'sel_json': $order_online_sel_json, 'form': $("#export-form").serializeArray()};
															$.ajax({
																'type': 'post',
																'data': $formdata2,
																'success': function(data) {
																	if (data == "" || data == 0) {
																		alert('导出失败，是否没有填写参数');
																	} else {
																		window.open(data);
																	}
																	//alert(data);
																},
																'url': '<?php echo site_url('www/export/execlDownload'); ?>',
																'async': false
															});
														}
													}, {
														text: "取消",
														Class: "btn bottom-margin btn-danger ",
														click: function() {
															alertify.error("你取消了批量导出");
															$(this).dialog("close");
														}
													}]
											});
											$("#order_online_export_div").dialog('open');
										});
									});
								</script>
<?php } ?>
							<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">

							<div class="grid-view">
								<div id="order_list"></div>
								<div>
									<script type="text/javascript">
										$(document).ready(function() {
											$selectAttr = [
												{'value': 'number', 'txt': '订单编号'},
												{'value': 'owner.name', 'txt': '销售人员'},
												{'value': 'account.name', 'txt': '客户名称'},
												{'value': 'department.name', 'txt': '所属部门'},
												{'value': 'account.account_shopex_id', 'txt': '客户商业ID'},
												{'value': 'review.name', 'txt': '业务主管'},
												{'value': 'finance.name', 'txt': '财务人员'},
												{'value': 'if_renew.name', 'txt': '否续费订单'},
												{'value': 'create_user_id.name', 'txt': '订单创建者'}
											];

											//查询条件
											where = [{
													'owner.name': 's1001'
												}];
											$("#order_list").leeDataTable({
												selectAttr: $selectAttr, //简单查询的查询属性
												url: "<?php echo site_url('www/order/ajax_select'); ?>", //ajax查询的地址
												perNumber: 20, //每页显示多少条数据
												obj: 'order',
												where:<?php echo $data_auth['where']; ?>,
												where_rel: '<?php echo $data_auth['where_rel']; ?>',
											});

											$('#order_list_select_attr').change(function() {
												$('#order_list_select_attr').parent().next().empty();
												if ($(this).val() == 'if_renew.name') {
													var html = '<select name="order_list_select_value_name"><option value="是">是</option><option value="否">否</option></select>';
												} else {
													var html = '<input type="text" name="order_list_select_value_name">';
												}
												$('#order_list_select_attr').parent().next().append(html);
											});

										});

									</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="orders_id" id="orders_id" value="" />
		<script>
			$(function() {
				$('#recon').click(function() {
					$.ajax({
						'type': 'get',
						'success': function(data) {
							$('#operation_dialog').html(data);
						},
						'url': '<?php echo site_url('www/order/recon'); ?>',
						'cache': false
					});
					$('#operation_dialog').dialog({
						title: "批量对账",
						modal: true,
						width: 600,
						height: 300,
						buttons: [{
								text: "对账",
								Class: "btn bottom-margin btn-primary",
								click: function() {
									orders_id = $("#orders_id").val();
									sp_date = $("#sp_date").val();
									//新增对账选择判断
									$q = $(":radio[name='q']:checked").val();
									if ($q == "all") {
										orders_id = $("#selectAll").attr("value");
									}
									if ($q == "check" && (orders_id=="")) {
										alert("未选中记录");
										return false;
									}
									//新增对账选择判断
									formdata = {"orders_id": orders_id, 'sp_date': sp_date};
									$.ajax({
										'type': 'post',
										'data': formdata,
										'success': function(data) {
											if (data == 'error') {
												$('#returnInfo').html(data);
												// alertify.error("失败订单号");
											} else if (data == '没有关联关系，请检查订单') {
												$('#returnInfo').html(data);
												// alertify.error("没有关联关系，请检查订单");
											} else {
												$('#returnInfo').html(data);
												// alertify.success(data);
											}
										},
										'url': '<?php echo site_url('www/order/recon'); ?>',
										'async': false
									});
								}
							}, {
								text: "关闭",
								Class: "btn bottom-margin btn-danger ",
								click: function() {
									$(this).dialog("close");
								}
							}]
					});
					$('#operation_dialog').dialog('open');
				});
			})
		</script>
		<div id="operation_dialog"></div>