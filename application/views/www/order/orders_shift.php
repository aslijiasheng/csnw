<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<form class="form-horizontal" id="orders-shift-form" action="" method="post">
				<div class="controls" style="margin-bottom:5px;">总用 <span style="color:red"><?php echo isset($order_number) ? $order_number : 0; ?></span> 条订单</div>
				<div class="control-group">
					<label class="control-label required" for="shift_title">
						任务标题
						<span class="required"></span>
					</label>
					<div class="controls">
						<input type="text" maxlength="128" name="title" class="isrequired" required="required" id="shift_title" value_id ="" value="" url=""  leetype="quote">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label required" for="order_owner">
						销售人员
						<span class="required"></span>
					</label>
					<div class="controls">
						<input type="text" name="order[order_owner]" id="order_owner" value_id ="" value="">
						<script type="text/javascript">
							$(document).ready(function() {
								$('#order_owner').leeQuote({
									url: '<?php echo site_url('www/user/ajax_list?tag_name=order_owner'); ?>',
									title: '选择销售'
								});
							});
						</script>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label required" for="order_review">
						业务主管
						<span class="required"></span>
					</label>
					<div class="controls">
						<input type="text" name="order[order_review]" id="order_review" value_id ="" value="">
						<script type="text/javascript">
							$(document).ready(function() {
								$('#order_review').leeQuote({
									url: '<?php echo site_url('www/user/ajax_list?tag_name=order_review'); ?>',
									title: '选择业务主管'
								});
							});
						</script>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label required" for="order_finance">
						财务
						<span class="required"></span>
					</label>
					<div class="controls">
						<input type="text" name="order[order_finance]" id="order_finance" value_id ="" value="">
						<script type="text/javascript">
							$(document).ready(function() {
								$('#order_finance').leeQuote({
									url: '<?php echo site_url('www/user/ajax_list?tag_name=order_finance'); ?>',
									title: '选择财务'
								});
							});
						</script>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label required" for="order_typein">
						录入人
						<span class="required"></span>
					</label>
					<div class="controls">
						<input type="text" name="order[order_typein]" id="order_typein" value_id ="" value="">
						<script type="text/javascript">
							$(document).ready(function() {
								$('#order_typein').leeQuote({
									url: '<?php echo site_url('www/user/ajax_list?tag_name=order_typein'); ?>',
									title: '选择录入人'
								});
							});
						</script>
					</div>
				</div>
			</form>
		</div>
	</div>