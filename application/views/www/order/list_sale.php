<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						<?php
						if (isset($_GET['type_id'])) {
							if (isset($type_arr[$_GET['type_id']]['type_name'])) {
								echo $type_arr[$_GET['type_id']]['type_name'];
							} else {
								echo '订单';
							}
						} else {
							echo '订单';
						}
						?>
						<span class="mini-title">
							<?php echo $type_arr[$_GET['type_id']]['type_label']; ?> order
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
							<!--lee 按钮部分 start-->
							<!--
							<div class="btn-group">
							<a id="neworder" class="btn btn-primary" href="<?php // echo site_url('www/order/add')                  ?>" data-toggle="dropdown" <?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (!in_array('order_add', $user_auth['activity_auth_arr'])) {
								echo 'style="display:none;"';
							}
							?>>
							<i class="icon-file"></i>
							新增订单
							</a>
							<ul class="dropdown-menu" role="menu">
							<?php foreach ($type_arr as $k => $v) { ?>
																						<li <?php
								if (!in_array($v['type_label'] . '_add', $user_auth['activity_auth_arr'])) {
									echo 'style="display:none;"';
								}
								?>><a href="<?php echo site_url('www/order/add?type_id=' . $v['type_id']); ?>">新增<?php echo $v['type_name']; ?></a></li>
							<?php } ?>
							</ul>
							</div>
							-->
							<?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (in_array('sale_rzdr', $user_auth['activity_auth_arr'])) {
							?>
								<form action="<?php echo site_url('www/introduction/execlFile?obj=order_sale') ?>" method="post" enctype="multipart/form-data">
									<a id="Admit_it_into" class="btn btn-primary" leetype='seniorquery'>
										<i class="icon-download-2"></i>
										认账导入
										<input id="fileupload" name="file" type="file" leetype='file'>
									</a>
								</form>
							<?php } ?>
							<?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (in_array('sale_plkp', $user_auth['activity_auth_arr'])) {
								?>
								<form action="<?php echo site_url('www/introduction/BulkBilling') ?>" method="post" enctype="multipart/form-data">
									<a id="BulkBilling" class="btn btn-primary" leetype='seniorquery'>
										<i class="icon-download-2"></i>
										批量开票
										<input id="fileupload" name="file" type="file" leetype='file'>
									</a>
								</form>
							<?php } ?>
							<script type="text/javascript">
								$("[leetype='file']").each(function() {
									//透明度改成0
									$(this).attr('style', 'opacity:0;position:absolute;');
									//获取父对象的宽度以及高度、还有坐标
									$closest = $(this).closest('a');
									//alert($closest.width());
									$(this).width($closest.width() + 25);
									$(this).height($closest.height() + 10);
									$(this).offset($closest.offset());
									$(this).change(function() {
										$(this).closest('form').submit();
									});
								});
							</script>

							<?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (in_array('sale_gjss', $user_auth['activity_auth_arr'])) {
								?>
								<a id="order_sale_seniorquery" class="btn btn-primary">
									<i class="icon-search"></i>
									高级查询
								</a>
								<script type="text/javascript">
									//高级查询
									$(document).ready(function() {
										$seniorquery_attr = <?php echo json_encode($seniorquery_attr); ?>;
										$("#order_sale_seniorquery").seniorquery({
											selectAttr: $seniorquery_attr,
											list_id: 'order_list',
											url: "<?php echo site_url('www/order/ajax_select_sale'); ?>", //ajax查询的地址
											perNumber: 20 //每页显示多少条数据(因为暂时没想通这里要怎么获取！所以直接赋值先)
										});
									});
								</script>
							<?php } ?>

							<?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (in_array('sale_pldc', $user_auth['activity_auth_arr'])) {
								?>
								<a id="order_sale_export" class="btn btn-primary">
									<i class="icon-upload-2"></i>
									批量导出
								</a>
								<div id='order_sale_export_div' style='display:none;'></div>
								<script type="text/javascript">
									//导出
									$(document).ready(function() {
										$("#order_sale_export").click(function() {
											$url = '<?php echo site_url('www/export/execlExport'); ?>';
											$order_sale_sel_json = $("#order_sale_sel_json").html();
											$formdata = {'sel_json': $order_sale_sel_json};
											$('#order_sale_export_div').ajaxHtml({
												url: $url,
												data: $formdata,
											});
											$("#order_sale_export_div").dialog({
												title: "批量导出",
												modal: true,
												width: 700,
												height: 500,
												buttons: [{
														text: "导出",
														Class: "btn bottom-margin btn-primary",
														click: function() {
															//获取所有选择的属性名称
															$formdata2 = {'sel_json': $order_sale_sel_json, 'form': $("#export-form").serializeArray()};
															$.ajax({
																'type': 'post',
																'data': $formdata2,
																'success': function(data) {
																	if (data == "" || data == 0) {
																		alert('导出失败，是否没有填写参数');
																	} else {
																		window.open(data);
																	}
																	//alert(data);
																},
																'url': '<?php echo site_url('www/export/execlDownload'); ?>',
																'async': false
															});
														}
													}, {
														text: "取消",
														Class: "btn bottom-margin btn-danger ",
														click: function() {
															alertify.error("你取消了批量导出");
															$(this).dialog("close");
														}
													}]
											});
											$("#order_sale_export_div").dialog('open');
										});
									});
								</script>
							<?php } ?>

							<?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (in_array('order_shift', $user_auth['activity_auth_arr'])) {
							?>
							<a id="orders_shift" class="btn btn-primary">
								<i class="icon-indent-increase"></i>
								批量订单转移
							</a>
							<div id="orders_shift_div" style='display:none;'></div>
							<script>
								$(function() {
									$('#orders_shift').click(function() {
										order_sale_sel_json = $("#order_sale_sel_json").html();
										formdata = {'sel_json': order_sale_sel_json};

										$.ajax({
											'type': 'post',
											'data': formdata,
											'success': function(data) {
												$('#orders_shift_div').html(data);
											},
											'url': '<?php echo site_url('www/order/orders_shift'); ?>',
											'cache': false
										});

										$("#orders_shift_div").dialog({
											title: "批量订单转移",
											modal: true,
											width: 600,
											height: 420,
											buttons: [{
													text: "转移",
													Class: "btn bottom-margin btn-primary",
													click: function() {
														formdata = $("#orders-shift-form").serializeArray();
														var status = leeConditionJudgment(formdata);
														if (status == false)
															return;

														formdata[formdata.length] = {"name": "sel_json", "value": order_sale_sel_json};
														$.ajax({
															'type': 'post',
															'data': formdata,
															'success': function(data) {
																if (data == 1) {
																	alertify.success("修改成功");
																	$("#orders_shift_div").dialog("close");
																	$("#order_list").ajaxHtml({
																		url: '<?php echo site_url('www/order/new_ajax_select?type_id=6');   ?>',
																		data: JSON.parse(order_sale_sel_json)
																	});
																} else {
																	alert(data);
																}
															},
															'url': '<?php echo site_url('www/order/ajax_orders_shift'); ?>',
															'async': false
														});
													}
												}, {
													text: "取消",
													Class: "btn bottom-margin btn-danger ",
													click: function() {
//														alertify.error("你取消了批量订单转移");
														$(this).dialog("close");
													}
												}]
										});
										$("#orders_shift_div").dialog('open');
									});
								});
							</script>
							<?php } ?>
						</div>
						<div id="divmessagelist">
							<div class="grid-view">
								<div id="order_list"></div>
								<div>
									<script type="text/javascript">
										$(document).ready(function() {
											$selectAttr = [
												{'value': 'number', 'txt': '订单编号'},
												{'value': 'owner.name', 'txt': '销售人员'},
												{'value': 'account.name', 'txt': '客户名称'},
												{'value': 'department.name', 'txt': '所属部门'},
												{'value': 'account.account_shopex_id', 'txt': '客户商业ID'},
												{'value': 'agent.shopex_id', 'txt': '代理商ID'},
												{'value': 'agreement_no', 'txt': '合同编号'},
												{'value': 'review.name', 'txt': '业务主管'},
												{'value': 'finance.name', 'txt': '财务人员'},
												{'value': 'if_renew.name', 'txt': '否续费订单'}
											];
											$("#order_list").leeDataTable({
												selectAttr: $selectAttr, //简单查询的查询属性
												url: "<?php echo site_url('www/order/ajax_select'); ?>", //ajax查询的地址
												perNumber: 20, //每页显示多少条数据
												obj: 'order',
												where:<?php echo $data_auth['where']; ?>,
												where_rel: '<?php echo $data_auth['where_rel']; ?>',
											});
											$('#order_list_select_attr').change(function() {
												$('#order_list_select_attr').parent().next().empty();
												if ($(this).val() == 'if_renew.name') {
													var html = '<select name="order_list_select_value_name"><option value="是">是</option><option value="否">否</option></select>';
												} else {
													var html = '<input type="text" name="order_list_select_value_name">';
												}
												$('#order_list_select_attr').parent().next().append(html);
											});
										});
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="operation_dialog"></div>