<div  id='order_online_sel_json' style='display:none;'><?php echo json_encode($sel_data);?></div>
<table class="table table-striped table-bordered table-hover">
	<thead>
	<tr>
		<th width="10" style="height:20px; display:block;"><input type="checkbox" id="select" value=""/>

			<div style="display:none;position:relative;top:-50px;width:150px;cursor:pointer;"><a id="selectAll" value="<?php echo isset($allOrdersId) ? $allOrdersId :'' ; ?>">
					<strog>点击选中所有订单 共<?php echo $totalNumber; ?>条</strog>
				</a></div>
		</th>
		<th width="75">操作</th>
		<th>订单编号</th>
		<th>订单总金额</th>
		<th>已到帐金额</th>
		<th>客户名称</th>
		<th>销售人员</th>
		<th>所属部门</th>
		<th>财务人员</th>
		<th>创建时间</th>
	</tr>
	</thead>
	<tbody id="onlineOrder">
	<?php foreach ($listData as $listData_value) { ?>
		<tr>
			<td><input type="checkbox" value="<?php echo $listData_value['order_id']; ?>"/></td>
			<td style="text-align:left">
				<div class="btn-toolbar">
					<div class="btn-group">
						<a class="btn btn-small btn-primary"
						   href="<?php echo site_url('www/order/view') . '?order_id=' . $listData_value['order_id']; ?>"<?php
						$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
						if (!in_array('Sale_view', $user_auth['activity_auth_arr'])) {
							echo 'style="display:none;"';
						}
						?> target="_black">查看</a>
						<button class="btn btn-small dropdown-toggle  btn-primary" data-toggle="dropdown"<?php
						if (!in_array('Sale_view', $user_auth['activity_auth_arr'])) {
							echo 'style="display:none;"';
						}
						?>>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li <?php
							if (!in_array('Sale_deal_salespay', $user_auth['activity_auth_arr'])) {
								echo 'style="display:none;"';
							}
							?>>
								<a value="<?php echo $listData_value['order_id']; ?>">对账</a>
							</li>
						</ul>
						>
					</div>
				</div>
			</td>
			<td><?php echo $listData_value['order_number']; ?></td>
			<td><?php echo $listData_value['order_amount']; ?></td>
			<td><?php echo isset($listData_value['ydz_amount']) ? $listData_value['ydz_amount'] : 0; ?></td>
			<td><?php echo ($listData_value['order_account'] != 0 and $listData_value['order_account'] != "") ? $listData_value['order_account_arr']['account_name'] : "&nbsp"; ?></td>
			<td><?php echo ($listData_value['order_owner'] != 0 and $listData_value['order_owner'] != "") ? $listData_value['order_owner_arr']['user_name'] : "&nbsp"; ?></td>
			<td><?php echo ($listData_value['order_department'] != 0 and $listData_value['order_department'] != "") ? $listData_value['order_department_arr']['department_name'] : "&nbsp"; ?></td>
			<td><?php echo ($listData_value['order_finance'] != 0 and $listData_value['order_finance'] != "") ? $listData_value['order_finance_arr']['user_name'] : "&nbsp"; ?>
			<td><?php echo $listData_value['order_create_time']; ?></td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber; ?>条</div>
<input type='hidden' id='totalNumber' value="<?php echo $totalNumber; ?>">
</div>
<script>
	$(function () {
		//刷新分页
		$("#order_list").RefreshPage({
			page:<?php if(isset($page)){echo $page;}else{echo 1;}?>, //当前页码
			perNumber:20, //每页显示多少条数据
			totalNumber:<?php echo $totalNumber;?>, //数据总数
			sel_data:<?php echo json_encode($sel_data)?>,
			listUrl:'<?php echo site_url('www/order/ajax_select_online'); ?>',
		});

		//选中已勾选的订单
		$("#select").attr("checked", true);
		$('#onlineOrder tr').each(function () {
			var this_order_id = ',' + $(this).children('td:first').children('input:checkbox').val() + ',';
			var orders = $("#orders_id").val();
			orders = ',' + orders + ',';
			if (orders.indexOf(this_order_id) > -1) {
				$(this).children('td:first').children('input:checkbox').attr('checked', true);
			} else {
				$("#select").attr("checked", false);
			}
			$("#orders_id").val(strTrim(orders));
		});

		$("#onlineOrder tr td input").click(function () {
			if ($(this).is(':checked')) {
				s = $("#orders_id").val() + ',' + $(this).val();
				$("#orders_id").val(strTrim(s));
			} else {
				s = ',' + $("#orders_id").val() + ',';
				s = s.replace(',' + $(this).val() + ',', ',');
				$("#orders_id").val(strTrim(s));
			}
		});

		$("#onlineOrder tr td").click(function () {
//			var thisCheckBox = $(this).siblings("td:first").children("input:checkbox");
			var thisCheckBox = $(this).parent().children().eq(0).children("input");
			if ($(this).index() != 1 && $(this).index() != 0) { //排除操作选项
				if (thisCheckBox.is(":checked") == false) {
					thisCheckBox.attr("checked", true);
					s = $("#orders_id").val() + ',' + thisCheckBox.val();
					$("#orders_id").val(strTrim(s));
				} else {
					thisCheckBox.attr("checked", false);
					$("#select").attr("checked", false);
					$("#select").children("div").hide();
					$("#select").siblings("div").hide();
					s = ',' + $("#orders_id").val() + ',';
					s = s.replace(',' + thisCheckBox.val() + ',', ',');
					$("#orders_id").val(strTrim(s));
				}
			}
		});

		$("#select").click(function () {
			if ($(this).is(':checked') == true) {
				$(this).siblings("div").show();
				$(':checkbox').attr('checked', true);
				var s = [];
				$('input:checkbox:checked').each(function () {
					s.push($(this).val());
				});
				$("#orders_id").val(strTrim(s));
				$("#select").siblings("div").delay(3000).hide(0);
			} else {
				$(this).siblings("div").hide();
				$(":checkbox").attr("checked", false);
				$("#orders_id").val("");
			}
		});

		$("#selectAll").click(function () {
			s = $(this).attr("value")
			$("#orders_id").val(s);
			$("#select").siblings("div").hide();
		});

		function strTrim(str) {
			str = str.toString();
			if (str.substr(0, 1) == ',')
				str = str.substr(1);
			if (str.substr(str.length - 1, 1) == ',')
				str = str.substr(0, str.length - 1);
			return str;
		}

		$(".dropdown-menu a").click(function () {
			formdata = {"orders_id": $(this).attr("value")};
			$.ajax({
				'type': 'post',
				'data': formdata,
				'success': function (data) {
					if (data == 'error') {
						alertify.error("失败订单号");
					} else if (data == '没有关联关系，请检查订单') {
						alertify.error("没有关联关系，请检查订单");
					} else {
						alertify.success(data);
					}
				},
				'url': '<?php echo site_url('www/order/recon'); ?>',
				'async': false
			});
		});
	});
</script>