<div  id='order_deposit_sel_json' style='display:none;'><?php echo json_encode($sel_data);?></div>
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="75">操作</th>
			<th>订单编号</th>
			<th>订单总金额</th>
			<th>已到帐金额</th>
			<th>款项类型</th>
			<th>客户名称</th>
			<th>经销商名称</th>
			<th>销售人员</th>
			<th>所属部门</th>
			<th>财务人员</th>
			<th>创建时间</th>
		</tr>
	</thead>

	<tbody>
		<?php foreach ($listData as $listData_value) { ?>
			<tr>
				<td style="text-align:left">
					<div class="btn-toolbar">
						<div class="btn-group">
							<a class="btn btn-small btn-primary" href="<?php echo site_url('www/order/view') . '?order_id=' . $listData_value['order_id']; ?>"<?php
							$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
							if (!in_array('Sale_view', $user_auth['activity_auth_arr'])) {
								echo 'style="display:none;"';
							}
							?> target="_black">查看</a>
							<button class="btn btn-small dropdown-toggle  btn-primary" data-toggle="dropdown"<?php
							if (!in_array('Sale_view', $user_auth['activity_auth_arr'])) {
								echo 'style="display:none;"';
							}
							?>>
								<span class="caret"></span>
							</button>
							<!--
							<ul class="dropdown-menu">
								<li <?php
								if (!in_array('Sale_deal_salespay', $user_auth['activity_auth_arr'])) {
									echo 'style="display:none;"';
								}
								?>>
									<a id="<?php echo $listData_value['order_id']; ?>" class="confirm_deposit">确认返点</a>
								</li>
								<li <?php if(!in_array('Deposit_apply_invoice',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="apply_invoice">申请开票</a>
								</li>
							</ul>
							-->
						</div>
					</div>
				</td>
				<td>
					<?php echo $listData_value['order_number']; ?>
					<?php echo $listData_value['order_is_cancel'] != '1001' ? '('.$listData_value['order_is_cancel_arr']['enum_name'].')' : '' ?>
				</td>

				<td>
					<?php echo $listData_value['order_amount']; ?>
				</td>

				<td>
					<?php if (isset($listData_value['ydz_amount'])) {
						echo $listData_value['ydz_amount'];
					} else {
						echo 0;
					} ?>
				</td>
				<td>
					<?php if (isset($listData_value['order_deposit_paytype_arr'])) {
						echo $listData_value['order_deposit_paytype_arr']['enum_name'];
					} else {
						echo 0;
					} ?>
				</td>
				<td>
					<?php
					if ($listData_value['order_account'] != 0 and $listData_value['order_account'] != "") {
						echo $listData_value['order_account_arr']['account_name'];
					} else {
						echo "&nbsp";
					}
					?>
				</td>

				<td>
					<?php
					if ($listData_value['order_agent'] != 0 and $listData_value['order_agent'] != "") {
						echo $listData_value['order_agent_arr']['partner_name'];
					} else {
						echo "&nbsp";
					}
					?>
				</td>

				<td>
					<?php
					if ($listData_value['order_owner'] != 0 and $listData_value['order_owner'] != "") {
						echo $listData_value['order_owner_arr']['user_name'];
					} else {
						echo "&nbsp";
					}
					?>
				</td>

				<td>
					<?php
					if ($listData_value['order_department'] != 0 and $listData_value['order_department'] != "") {
						echo $listData_value['order_department_arr']['department_name'];
					} else {
						echo "&nbsp";
					}
					?>
				</td>

				<td>
					<?php
					if ($listData_value['order_finance'] != 0 and $listData_value['order_finance'] != "") {
						echo $listData_value['order_finance_arr']['user_name'];
					} else {
						echo "&nbsp";
					}
					?>
				</td>

				<td>
	<?php echo $listData_value['order_create_time']; ?>
				</td>
			</tr>
<?php } ?>
	</tbody>
</table>
<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber; ?>条</div>
<input type='hidden' id='totalNumber' value="<?php echo $totalNumber; ?>">
</div>
<script>
$(function() {
	//刷新分页
	$("#order_list").RefreshPage({
		page:<?php if(isset($page)){echo $page;}else{echo 1;}?>, //当前页码
		perNumber:20, //每页显示多少条数据
		totalNumber:<?php echo $totalNumber;?>, //数据总数
		sel_data:<?php echo json_encode($sel_data)?>,
		listUrl:'<?php echo site_url('www/order/ajax_select_deposit'); ?>',
	});

		//	申请开票
   $(".apply_invoice").click(function(){

   	id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'order_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/invoice/ajax_add?type=1001&order_type=10'); ?>',
			'cache':false
		});
        $('#operation_dialog').dialog({
			title:'申请开票',
			modal: true,
			width:1000,
			height:450,
			buttons: [{
				text:"提交",
				Class:"btn btn-primary",
				click: function(){
					 $this = $(this);
					 formdata = $("#invoice-form").serializeArray();
					 var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					// //本次入账金额
					// $pay_amount = $('[name="salespay[salespay_pay_amount]"]').val();
					// //可入账金额
					// $krz_amount = $('#kdz_amount').html();
					// if(($krz_amount-$pay_amount)<0){
					// 	alertify.alert("【入账金额】不得大于【可入账金额】");
					// 	return false;
					// }
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("成功");
								 $('#operation_dialog').dialog("close");
							}else if(data==2){
								alertify.alert("开票额度超出");
							 }else if(data=='失败'){
							 	alertify.alert("错了");
							 }else{
							 	alert(data);
							 }
						},
						'url':'<?php echo site_url('www/invoice/ajax_add_post?type=1'); ?>',
						'async':false
					});
				}
			}]
		});
		  $('#operation_dialog').dialog('open');

   })

		//确认返点
		$(".confirm_deposit").click(function() {
			id = $(this).attr('id');
			$.ajax({
				'type': 'get',
				'data': 'order_id=' + id,
				'success': function(data) {
					$('#operation_dialog').html(data);
				},
				'url': '<?php echo site_url('www/rebate/ajax_depositOrder'); ?>',
				'cache': false
			});
			$('#operation_dialog').dialog({
				modal: true,
				title: '执行返点',
				width: 800,
				height: 450,
				buttons: [{
						text: "执行返点",
						Class: "btn btn-primary",
						click: function() {
							formdata = $("#depositOrder_form").serializeArray();
							//alert(formdata);
							$.ajax({
								'type': 'post',
								'data': formdata,
								'success': function(data) {
									if (data == 1) {
										alertify.success("已经执行返点");
//										$('#rebate').ajaxHtml(); //再次加载1次页面
										$('#operation_dialog').dialog("close");
									} else {
										alertify.error(data);
									}
								},
								'url': '<?php echo site_url('www/rebate/ajax_confirm_depositOrder'); ?>',
								'async': false
							});
						}
					}, {
						text: "取消",
						Class: "btn bottom-margin",
						click: function() {
							alertify.error("取消返点操作");
							$(this).dialog("close");
						}
					}]

			});
			$('#operation_dialog').dialog('open');
		});
	})
</script>