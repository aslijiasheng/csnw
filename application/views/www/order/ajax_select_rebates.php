<div  id='order_rebates_sel_json' style='display:none;'><?php echo json_encode($sel_data);?></div>
<table class="table table-striped table-bordered table-hover">
	<thead>
	<tr>
		<th width="75">操作</th>
		<th>订单编号</th>
		<th>代理商名称</th>
		<th>返点类型</th>
		<th>渠道经理</th>
		<th>业务主管</th>
		<th>财务</th>
		<th>返点金额</th>
		<th>返点状态</th>
		<th>创建时间</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($listData as $listData_value) { ?>
		<tr>
			<td style="text-align:left">
				<div class="btn-toolbar">
					<div class="btn-group">
						<a class="btn btn-small btn-primary"
						   href="<?php echo site_url('www/order/view') . '?order_id=' . $listData_value['order_id']; ?>"<?php
						$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
						if (!in_array('Sale_view', $user_auth['activity_auth_arr'])) {
							echo 'style="display:none;"';
						}
						?> target="_black">查看</a>
						<button class="btn btn-small dropdown-toggle  btn-primary" data-toggle="dropdown"<?php
						if (!in_array('Sale_view', $user_auth['activity_auth_arr'])) {
							echo 'style="display:none;"';
						}
						?>>
							<span class="caret"></span>
						</button>
						<!--
						<ul class="dropdown-menu">
							<li <?php
							if (!in_array('Sale_deal_salespay', $user_auth['activity_auth_arr'])) {
								echo 'style="display:none;"';
							}
							?>>
								<a id="<?php echo $listData_value['order_id']; ?>" class="confirm_rebate">确认返点</a>
							</li>
						</ul>
						-->
					</div>
				</div>
			</td>
			<td>
				<?php echo $listData_value['order_number']; ?>&nbsp
			</td>
			<td>
				<?php if(isset($listData_value['order_agent_arr']['partner_name'])){echo $listData_value['order_agent_arr']['partner_name'];}?>&nbsp
			</td>
			<td>
				<?php if(isset($listData_value['order_rebates_type_arr']['enum_name'])){echo $listData_value['order_rebates_type_arr']['enum_name'];}?>&nbsp
			</td>
			<td>
				<?php if(isset($listData_value['order_owner_arr']['user_name'])){echo $listData_value['order_owner_arr']['user_name'];}?>&nbsp
			</td>
			<td>
				<?php if(isset($listData_value['order_review_arr']['user_name'])){echo $listData_value['order_review_arr']['user_name'];} ?>&nbsp
			</td>
			<td>
				<?php if(isset($listData_value['order_finance_arr']['user_name'])){echo $listData_value['order_finance_arr']['user_name'];} ?>&nbsp
			</td>
			<td>
				<?php echo $listData_value['order_amount']; ?>&nbsp
			</td>
			<td>
				<?php if(isset($listData_value['order_rebates_state_arr']['enum_name'])){echo $listData_value['order_rebates_state_arr']['enum_name'];} ?>&nbsp
			</td>
			<td>
				<?php echo $listData_value['order_create_time']; ?>&nbsp
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber; ?>条</div>
<input type='hidden' id='totalNumber' value="<?php echo $totalNumber; ?>">
</div>
<script>
	$(function () {
		//刷新分页
		$("#order_list").RefreshPage({
			page:<?php if(isset($page)){echo $page;}else{echo 1;}?>, //当前页码
			perNumber:20, //每页显示多少条数据
			totalNumber:<?php echo $totalNumber;?>, //数据总数
			sel_data:<?php echo json_encode($sel_data)?>,
			listUrl:'<?php echo site_url('www/order/ajax_select_rebates'); ?>',
		});

		//确认返点
		$(".confirm_rebate").click(function () {
			id = $(this).attr('id');
			$.ajax({
				'type': 'get',
				'data': 'order_id=' + id,
				'success': function (data) {
					$('#operation_dialog').html(data);
				},
				'url': '<?php echo site_url('www/rebate/ajax_rebateOrder'); ?>',
				'cache': false
			});
			$('#operation_dialog').dialog({
				modal: true,
				title: '执行返点',
				width: 800,
				height: 450,
				buttons: [
					{
						text: "执行返点",
						Class: "btn btn-primary",
						click: function () {
							formdata = $("#rebateOrder_form").serializeArray();
							formdata[formdata.length] = {"name": "rebate[rebate_status]", "value": "1004"}; //添加1个参数
							//alert(formdata);
							$.ajax({
								'type': 'post',
								'data': formdata,
								'success': function (data) {
									if (data == 1) {
										alertify.success("已经执行返点");
										$('#rebate').ajaxHtml(); //再次加载1次页面
										$('#operation_dialog').dialog("close");
									} else {
										alertify.error(data);
									}
								},
								'url': '<?php echo site_url('www/rebate/ajax_confirm_rebateOrder'); ?>',
								'async': false
							});
						}
					},
					{
						text: "取消",
						Class: "btn bottom-margin",
						click: function () {
							alertify.error("取消返点操作");
							$(this).dialog("close");
						}
					}
				]

			});
			$('#operation_dialog').dialog('open');
		});
	})
</script>