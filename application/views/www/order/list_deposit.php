<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						<?php if(isset($_GET['type_id'])){
							if(isset($type_arr[$_GET['type_id']]['type_name'])){
								echo $type_arr[$_GET['type_id']]['type_name'];
							}else{
								echo '订单';
							}
						}else{
							echo '订单';
						}
						?>
						<span class="mini-title">
						<?php echo $type_arr[$_GET['type_id']]['type_label'];?> Order
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<?php $user_auth = $this->user->user_auth($this->session->userdata('user_id'));if(in_array('Deposit_gjss', $user_auth['activity_auth_arr'])){?>
<a id="order_deposit_seniorquery" class="btn btn-primary">
<i class="icon-search"></i>
高级查询
</a>
<script type="text/javascript">
//高级查询
$(document).ready(function () {
	$seniorquery_attr = <?php echo json_encode($seniorquery_attr);?>;
	$("#order_deposit_seniorquery").seniorquery({
		selectAttr:$seniorquery_attr,
		list_id:'order_list',
		url:"<?php echo site_url('www/order/ajax_select_deposit'); ?>", //ajax查询的地址
		perNumber:20 //每页显示多少条数据(因为暂时没想通这里要怎么获取！所以直接赋值先)
	});
});
</script>
<?php }?>
<?php $user_auth = $this->user->user_auth($this->session->userdata('user_id'));if(in_array('Deposit_pldc', $user_auth['activity_auth_arr'])){?>
<a id="order_deposit_export" class="btn btn-primary">
<i class="icon-upload-2"></i>
批量导出
</a>
<div id='order_deposit_export_div' style='display:none;'></div>
<script type="text/javascript">
//导出
$(document).ready(function () {
	$("#order_deposit_export").click(function(){
		$url = '<?php echo site_url('www/export/execlExport');?>';
		$order_deposit_sel_json = $("#order_deposit_sel_json").html();
		//alert($order_sale_sel_json);
		$formdata = {'sel_json':$order_deposit_sel_json};
		$('#order_deposit_export_div').ajaxHtml({
			url:$url,
			data:$formdata,
		});
		$("#order_deposit_export_div").dialog({
			title:"批量导出",
			modal: true,
			width:700,
			height:500,
			buttons: [{
				text: "导出",
				Class: "btn bottom-margin btn-primary",
				click: function() {
					//获取所有选择的属性名称
					$formdata2 = {'sel_json':$order_deposit_sel_json,'form':$("#export-form").serializeArray()};
					$.ajax({
						'type':'post',
						'data':$formdata2,
						'success':function(data){
							if(data=="" || data==0){
								alert('导出失败，是否没有填写参数');
							}else{
								window.open(data);
							}
							//alert(data);
						},
						'url':'<?php echo site_url('www/export/execlDownload');?>',
						'async':false
					});
				}
			},{
				text: "取消",
				Class: "btn bottom-margin btn-danger ",
				click: function() {
					alertify.error("你取消了批量导出");
					$(this).dialog("close");
				}
			}]
		});
		$("#order_deposit_export_div").dialog('open');
	});
});
</script>
<?php }?>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">

<div class="grid-view">
	<div id="order_list"></div>
<div>


<script type="text/javascript">
$(document).ready(function () {
	$selectAttr=[
		{'value':'number','txt':'订单编号'},
		{'value':'owner.name','txt':'销售人员'},
		{'value':'agent.name','txt':'所属代理商'},
		{'value':'account.name','txt':'客户名称'},
		{'value':'department.name','txt':'所属部门'},
		{'value':'account.account_shopex_id','txt':'客户商业ID'},
		{'value':'review.name','txt':'业务主管'},
		{'value':'finance.name','txt':'财务人员'},
		{'value':'source.name','txt':'订单来源'}
	];

	$("#order_list").leeDataTable({
		selectAttr:$selectAttr, //简单查询的查询属性
		url:"<?php echo site_url('www/order/ajax_select'); ?>", //ajax查询的地址
		perNumber:20, //每页显示多少条数据
		obj:'order',

		where:<?php echo $data_auth['where'];?>,
		where_rel:'<?php echo $data_auth['where_rel'];?>',
	});

	$('#order_list_select_attr').change(function() {
		$('#order_list_select_attr').parent().next().empty();
		if ($(this).val() == 'source.name') {
			var html = '<select name="order_list_select_value_name"><option value="云猫">云猫</option><option value="云生意">云生意</option><option value="渠道">渠道</option></select>';
		} else {
			var html = '<input type="text" name="order_list_select_value_name">';
		}
		$('#order_list_select_attr').parent().next().append(html);
	});

});

</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div id="operation_dialog"></div>