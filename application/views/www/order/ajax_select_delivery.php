<div id='order_delivery_sel_json' style='display:none;'><?php echo json_encode($sel_data);?></div>

	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="75">操作</th>
				<th>订单编号</th>
				<th>订单总金额</th>
				<th>客户名称</th>
				<th>经销商名称</th>
				<th>销售人员</th>
				<th>所属部门</th>
				<th>财务人员</th>
				<th>创建时间</th>
			</tr>
		</thead>

		<tbody>

		<?php foreach($listData as $listData_value){?>
			<?php  $user_auth = $this->user->user_auth($this->session->userdata('user_id')); ?>
			<tr>
				<td style="text-align:left">
					<div class="btn-toolbar">
						<div class="btn-group">
							<a class="btn btn-small btn-primary" href="<?php echo site_url('www/order/view').'?order_id='.$listData_value['order_id']; ?>"<?php $user_auth = $this->user->user_auth($this->session->userdata('user_id'));if(!in_array('Sale_view',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?> target="_black">查看</a>
							<button class="btn btn-small dropdown-toggle  btn-primary" data-toggle="dropdown"<?php if(!in_array('Sale_view',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">




										<li <?php if(!in_array('Sale_deal_invoice',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
											<a href="<?php echo site_url('www/order/view').'?order_id='.$listData_value['order_id']."&active=invoice"; ?>">处理票据</a>
										</li>

										<li <?php if(!in_array('Sale_deal_refund',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
											<a href="<?php echo site_url('www/order/view').'?order_id='.$listData_value['order_id']."&active=active=salespay_out"; ?>">处理退款</a>
										</li>

								<li <?php if(!in_array('Sale_add_refund',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="add_refund_salespay">申请退款项</a>
								</li>

								<li <?php if(!in_array('Sale_apply_invoice_2',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="apply_invoice_2">申请开票</a>
								</li>



								<li <?php if(!in_array('Sale_add_refund',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="add_refund">申请退款</a>
								</li>

                                </ul>
						</div>
					</div>
				</td>
				<td>
				<?php  echo $listData_value['order_number'];?>
				<?php echo $listData_value['order_is_cancel'] != '1001' ? '('.$listData_value['order_is_cancel_arr']['enum_name'].')' : '' ?>
				</td>

				<td>
				<?php  echo $listData_value['order_amount'];?>
				</td>

				<td>
					<?php
						if ($listData_value['order_account']!=0 and $listData_value['order_account']!=""){
							echo $listData_value['order_account_arr']['account_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>

				<td>
					<?php
					if ($listData_value['order_agent'] != 0 and $listData_value['order_agent'] != "") {
						echo $listData_value['order_agent_arr']['partner_name'];
					} else {
						echo "&nbsp";
					}
					?>
				</td>

				<td>
					<?php
						if ($listData_value['order_owner']!=0 and $listData_value['order_owner']!=""){
							echo $listData_value['order_owner_arr']['user_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>

				<td>
					<?php
						if ($listData_value['order_department']!=0 and $listData_value['order_department']!=""){
							echo $listData_value['order_department_arr']['department_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>

				<td>
					<?php
						if ($listData_value['order_finance']!=0 and $listData_value['order_finance']!=""){
							echo $listData_value['order_finance_arr']['user_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>

				<td>
				<?php  echo $listData_value['order_create_time'];?>
				</td>
			</tr>
		<?php }?>
		</tbody>
	</table>
	<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber;?>条</div>
	<input type='hidden' id='totalNumber' value="<?php echo $totalNumber;?>">
	</div>


<script type="text/javascript">
$(document).ready(function () {
	//刷新分页
	$("#order_list").RefreshPage({
		page:<?php if(isset($page)){echo $page;}else{echo 1;}?>, //当前页码
		perNumber:20, //每页显示多少条数据
		totalNumber:<?php echo $totalNumber;?>, //数据总数
		sel_data:<?php echo json_encode($sel_data)?>,
		listUrl:'<?php echo site_url('www/order/ajax_select_delivery'); ?>',
	});

	//申请入款项




	//申请退款
	$('.add_refund').click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'order_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/refund/ajax_add'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			title:'申请退款',
			modal: true,
			width:800,
			height:450,
			buttons: [{
				text:"提交",
				Class:"btn btn-primary",
				click: function(){
					$this = $(this);
					formdata = $("#refund-form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//这里需要通过产品单个金额汇总【本次退款金额】
					var $refund_amount = 0; //本次退款金额
					$('.goods_refund_amount').each(function(){
						$goods_amount = $(this).closest('td').siblings('.goods_amount').html(); //这个商品的折后价
						$goods_refund_amount = $(this).val(); //这个商品的返款金额
						if($goods_refund_amount==""){
							$goods_refund_amount=0;
						}
						if(($goods_amount-$goods_refund_amount)<0){
							alert($goods_amount+"-"+$goods_refund_amount);
							alertify.alert('商品的【退款金额】不得大于商品【折后价】');
							return false;
						}
						$refund_amount += parseInt($goods_refund_amount);
					});
					//可退款金额
					$ktk_amount = $('#ktk_amount').html();
					if(($ktk_amount-$refund_amount)<0){
						alertify.alert("【本次退款金额】不得大于【可退款金额】");
						return false;
					}
					//return false;
					//将本次退款金额添加到formdata里
					formdata[formdata.length]={"name":"refund[refund_amount]","value":$refund_amount};
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("申请退款创建成功");
								$this.dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/refund/ajax_add_post'); ?>',
						'async':false
					});
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});



   //申请开票
   $(".apply_invoice_2").click(function(){

   	id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'order_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/invoice/ajax_add?type=1001&order_type=13'); ?>',
			'cache':false
		});
        $('#operation_dialog').dialog({
			title:'申请开票',
			modal: true,
			width:1000,
			height:450,
			buttons: [{
				text:"提交",
				Class:"btn btn-primary",
				click: function(){
					 $this = $(this);
					 formdata = $("#invoice-form").serializeArray();
					 var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					// //本次入账金额
					// $pay_amount = $('[name="salespay[salespay_pay_amount]"]').val();
					// //可入账金额
					// $krz_amount = $('#kdz_amount').html();
					// if(($krz_amount-$pay_amount)<0){
					// 	alertify.alert("【入账金额】不得大于【可入账金额】");
					// 	return false;
					// }
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("操作成功");
								//成功后关闭当前弹框

								 $('#operation_dialog').dialog("close");
							}else if(data==2){
								alertify.alert("开票额度超出");
							 }else if(data=='失败'){
							 	alertify.alert("错了");
							 }else{
							 	alert(data);
							 }
						},
						'url':'<?php echo site_url('www/invoice/ajax_add_post?type=1001'); ?>',
						'async':false
					});
				}
			}]
		});
		  $('#operation_dialog').dialog('open');

   })

   //申请退款项
	$('.add_refund_salespay').click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':{
				'order_id':id,
				'money_type_name':'1003' //【款项类型名称】为【退款项】
			},
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/salespay/ajax_add_out'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			title:'申请退款项',
			modal: true,
			width:800,
			height:450,
			buttons: [{
				text:"提交",
				Class:"btn btn-primary",
				click: function(){
					$this = $(this);
					formdata = $("#salespay-form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//本次返点金额
					//$salespay_pay_amount = $('[name="salespay[salespay_pay_amount]"]').val();
					//这里需要通过产品单个金额汇总【本次退款金额】
					var $salespay_pay_amount = 0; //本次退款金额
					$('.goods_refund_amount').each(function(){
						$goods_amount = $(this).closest('td').siblings('.goods_amount').html(); //这个商品的折后价
						$goods_refund_amount = $(this).val(); //这个商品的返款金额
						if($goods_refund_amount==""){
							$goods_refund_amount=0;
						}
						if(($goods_amount-$goods_refund_amount)<0){
							alert($goods_amount+"-"+$goods_refund_amount);
							alertify.alert('商品的【退款金额】不得大于商品【折后价】');
							return false;
						}
						$salespay_pay_amount += parseInt($goods_refund_amount);
					});
					//可退款金额
					$ktk_amount = $('#ktk_amount').html();
					if(($ktk_amount-$salespay_pay_amount)<0){
						alertify.alert("【本次退款金额】不得大于【可退款金额】");
						return false;
					}
					//将本次退款金额添加到formdata里
					formdata[formdata.length]={"name":"salespay[salespay_pay_amount]","value":$salespay_pay_amount};

					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("申请成功");
								$this.dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/salespay/ajax_add_post'); ?>',
						'async':false
					});
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});






});
