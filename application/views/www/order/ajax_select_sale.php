	<div  id='order_sale_sel_json' style='display:none;'><?php echo json_encode($sel_data);?></div>

	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="75">操作</th>
				<th>订单编号</th>
				<th>订单总金额</th>
				<th>已到帐金额</th>
				<th>已到帐金额(测试)</th>
				<th>客户名称</th>
				<th>销售人员</th>
				<th>所属部门</th>
				<th>财务人员</th>
				<th>创建时间</th>
			</tr>
		</thead>

		<tbody>

		<?php foreach($listData as $listData_value){?>
			<?php  $user_auth = $this->user->user_auth($this->session->userdata('user_id')); ?>
			<tr>
				<td style="text-align:left">
					<div class="btn-toolbar">
						<div class="btn-group">
							<a class="btn btn-small btn-primary" href="<?php echo site_url('www/order/view').'?order_id='.$listData_value['order_id']; ?>"<?php $user_auth = $this->user->user_auth($this->session->userdata('user_id'));if(!in_array('Sale_view',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?> target="_black">查看</a>
							<button class="btn btn-small dropdown-toggle  btn-primary" data-toggle="dropdown"<?php if(!in_array('Sale_view',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">



										<li <?php if(!in_array('Sale_deal_salespay',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
											<a href="<?php echo site_url('www/order/view').'?order_id='.$listData_value['order_id']."&active=salespay_into"; ?>">处理入款项</a>
										</li>
										<li <?php if(!in_array('Sale_deal_invoice',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
											<a href="<?php echo site_url('www/order/view').'?order_id='.$listData_value['order_id']."&active=invoice"; ?>">处理票据</a>
										</li>
										<li <?php if(!in_array('Sale_deal_rebate',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
											<a href="<?php echo site_url('www/order/view').'?order_id='.$listData_value['order_id']."&active=salespay_out"; ?>">处理返点</a>
										</li>
										<li <?php if(!in_array('Sale_deal_refund',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
											<a href="<?php echo site_url('www/order/view').'?order_id='.$listData_value['order_id']."&active=salespay_out"; ?>">处理退款</a>
										</li>



								<li <?php if(!in_array('Sale_add_salespay',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="add_salespay_into">新增入款项</a>
								</li>
								<li <?php if(!in_array('Sale_apply_invoice_1',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="apply_invoice_1">申请开票</a>
								</li>
								<li <?php if(!in_array('Sale_apply_invoice_2',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="apply_invoice_2">申请预开票</a>
								</li>
								<li <?php if(!in_array('Sale_apply_invoice_3',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="financial_invoice">财务人员自主开票</a>
								</li>
								<!--
								<li <?php if(!in_array('Sale_add_rebate',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="add_rebate">申请返点</a>
								</li>
								-->
								<li <?php if(!in_array('Sale_add_rebate',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="add_rebate_salespay">申请返点款项</a>
								</li>
								<!--
								<li <?php if(!in_array('Sale_add_refund',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="add_refund">申请退款</a>
								</li>
								-->
								<li <?php if(!in_array('Sale_add_refund',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>>
									<a id="<?php echo $listData_value['order_id'];?>" class="add_refund_salespay">申请退款项</a>
								</li>

                                <li <?php if (!in_array('order_cancel', $user_auth['activity_auth_arr'])) { echo 'style="display:none;"';} ?>>
                                	<a id="<?php echo $listData_value['order_id'] ?>" cancel="<?php echo $listData_value['order_is_cancel'] ?>" class="order_cancel">订单作废</a>
                                </li>

                                <li <?php if (!in_array('cancel_check', $user_auth['activity_auth_arr'])) { echo 'style="display:none;"'; } ?>>
                                	<a id="<?php echo $listData_value['order_id'] ?>" cancel="<?php echo $listData_value['order_is_cancel'] ?>" class="cancel_check">审核作废</a>
                                </li>

                                <li <?php if (!in_array('cancel_affirm', $user_auth['activity_auth_arr'])) { echo 'style="display:none;"'; } ?>>
                                	<a id="<?php echo $listData_value['order_id'] ?>" cancel="<?php echo $listData_value['order_is_cancel'] ?>" class="cancel_affirm">确认作废</a>
                                </li>

								<li <?php if (!in_array('order_shift', $user_auth['activity_auth_arr'])) { echo 'style="display:none;"'; } ?>>
									<a id="<?php echo $listData_value['order_id'] ?>" class="order_shift">订单转移</a>
								</li>

								<li <?php if (!in_array('order_adjunct', $user_auth['activity_auth_arr'])) { echo 'style="display:none;"'; } ?>>
									<a number="<?php echo $listData_value['order_number'] ?>" id="<?php echo $listData_value['order_id'] ?>" class="order_adjunct">上传附件</a>
								</li>

                            </ul>
						</div>
					</div>
				</td>
				<td>
				<?php echo $listData_value['order_number'];?>
				<?php echo $listData_value['order_is_cancel'] != '1001' ? '('.$listData_value['order_is_cancel_arr']['enum_name'].')' : '' ?>
				</td>

				<td>
				<?php  echo $listData_value['order_amount'];?>
				</td>

				<td>
				<?php  echo $listData_value['ydz_amount'];
					//echo $listData_value['order_having_account'];
				?>
				</td>
				
				<td>
				<?php  
					echo $listData_value['order_having_account'];
				?>
				</td>				

				<td>
					<?php
						if ($listData_value['order_account']!=0 and $listData_value['order_account']!=""){
							echo $listData_value['order_account_arr']['account_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>

				<td>
					<?php
						if ($listData_value['order_owner']!=0 and $listData_value['order_owner']!=""){
							echo $listData_value['order_owner_arr']['user_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>

				<td>
					<?php
						if ($listData_value['order_department']!=0 and $listData_value['order_department']!=""){
							echo $listData_value['order_department_arr']['department_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>

				<td>
					<?php
						if ($listData_value['order_finance']!=0 and $listData_value['order_finance']!=""){
							echo $listData_value['order_finance_arr']['user_name'];
						}else{
							echo "&nbsp";
						}
					?>
				</td>

				<td>
				<?php  echo $listData_value['order_create_time'];?>
				</td>
			</tr>
		<?php }?>
		</tbody>
	</table>
	<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber;?>条</div>
	<input type='hidden' id='totalNumber' value="<?php echo $totalNumber;?>">
	</div>
<?php //p(json_encode($sel_data));?>
<script type="text/javascript">
$(document).ready(function () {
	//刷新分页
	$("#order_list").RefreshPage({
		page:<?php if(isset($page)){echo $page;}else{echo 1;}?>, //当前页码
		perNumber:20, //每页显示多少条数据
		totalNumber:<?php echo $totalNumber;?>, //数据总数
		sel_data:<?php echo json_encode($sel_data)?>,
		listUrl:'<?php echo site_url('www/order/ajax_select_sale'); ?>',
	});


	//申请入款项
	$('.add_salespay_into').click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'order_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/salespay/ajax_add_into'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			title:'新增入款项',
			modal: true,
			width:800,
			height:450,
			buttons: [{
				text:"提交",
				Class:"btn btn-primary",
				click: function(){
					$this = $(this);
					formdata = $("#salespay-form").serializeArray();
					$is=leeConditionJudgment(formdata)
					if($is==false){
						return false;
					};

					//本次入账金额
					$pay_amount = $('[name="salespay[salespay_pay_amount]"]').val();
					//可入账金额
					$krz_amount = $('#kdz_amount').html();
					if(($krz_amount-$pay_amount)<0){
						alertify.alert("【入账金额】不得大于【可入账金额】");
						return false;
					}
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data == 'amount'){
								alert('申请金额大于订单金额');
							}else if(data==1){
								alertify.alert("创建入款项成功");
								$this.dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/salespay/ajax_add_post'); ?>',
						'async':false
					});
				}
				},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了此次操作");
					$(this).dialog("close");
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

	//申请返点
	$('.add_rebate').click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'order_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/rebate/ajax_add'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			title:'申请返点',
			modal: true,
			width:800,
			height:450,
			buttons: [{
				text:"提交",
				Class:"btn btn-primary",
				click: function(){
					$this = $(this);
					formdata = $("#rebate-form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//本次返点金额
					$rebate_amount = $('[name="rebate[rebate_amount]"]').val();
					//可返点金额
					$kfd_amount = $('#kfd_amount').html();
					if(($kfd_amount-$rebate_amount)<0){
						alertify.alert("【本次返点金额】不得大于【可返点金额】");
						return false;
					}
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("申请成功");
								$this.dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/rebate/ajax_add_post'); ?>',
						'async':false
					});
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

	//申请返点款项
	$('.add_rebate_salespay').click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':{
				'order_id':id,
				'money_type_name':'1002' //【款项类型名称】为【返点款项】
			},
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/salespay/ajax_add_out'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			title:'申请返点款项',
			modal: true,
			width:800,
			height:450,
			buttons: [{
				text:"提交",
				Class:"btn btn-primary",
				click: function(){
					$this = $(this);
					formdata = $("#salespay-form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//本次返点金额
					$salespay_pay_amount = $('[name="salespay[salespay_pay_amount]"]').val();
					//alert($salespay_pay_amount);
					//可返点金额
					$kfd_amount = $('#kfd_amount').html();
					if(($kfd_amount-$salespay_pay_amount)<0){
						alertify.alert("【本次返点金额】不得大于【可返点金额】");
						return false;
					}
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("申请成功");
								$this.dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/salespay/ajax_add_post'); ?>',
						'async':false
					});
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

	//申请退款项
	$('.add_refund_salespay').click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':{
				'order_id':id,
				'money_type_name':'1003' //【款项类型名称】为【退款项】
			},
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/salespay/ajax_add_out'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			title:'申请退款项',
			modal: true,
			width:800,
			height:450,
			buttons: [{
				text:"提交",
				Class:"btn btn-primary",
				click: function(){
					$this = $(this);
					formdata = $("#salespay-form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//本次返点金额
					//$salespay_pay_amount = $('[name="salespay[salespay_pay_amount]"]').val();
					//这里需要通过产品单个金额汇总【本次退款金额】
					var $salespay_pay_amount = 0; //本次退款金额
					$('.goods_refund_amount').each(function(){
						$goods_amount = $(this).closest('td').siblings('.goods_amount').html(); //这个商品的折后价
						$goods_refund_amount = $(this).val(); //这个商品的返款金额
						if($goods_refund_amount==""){
							$goods_refund_amount=0;
						}
						if(($goods_amount-$goods_refund_amount)<0){
							alert($goods_amount+"-"+$goods_refund_amount);
							alertify.alert('商品的【退款金额】不得大于商品【折后价】');
							return false;
						}
						$salespay_pay_amount += parseInt($goods_refund_amount);
					});
					//可退款金额
					$ktk_amount = $('#ktk_amount').html();
					if(($ktk_amount-$salespay_pay_amount)<0){
						alertify.alert("【本次退款金额】不得大于【可退款金额】");
						return false;
					}
					//将本次退款金额添加到formdata里
					formdata[formdata.length]={"name":"salespay[salespay_pay_amount]","value":$salespay_pay_amount};

					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("申请成功");
								$this.dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/salespay/ajax_add_post'); ?>',
						'async':false
					});
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});


	//申请退款
	$('.add_refund').click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'order_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/refund/ajax_add'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			title:'申请退款',
			modal: true,
			width:800,
			height:450,
			buttons: [{
				text:"提交",
				Class:"btn btn-primary",
				click: function(){
					$this = $(this);
					formdata = $("#refund-form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//这里需要通过产品单个金额汇总【本次退款金额】
					var $refund_amount = 0; //本次退款金额
					$('.goods_refund_amount').each(function(){
						$goods_amount = $(this).closest('td').siblings('.goods_amount').html(); //这个商品的折后价
						$goods_refund_amount = $(this).val(); //这个商品的返款金额
						if($goods_refund_amount==""){
							$goods_refund_amount=0;
						}
						if(($goods_amount-$goods_refund_amount)<0){
							alert($goods_amount+"-"+$goods_refund_amount);
							alertify.alert('商品的【退款金额】不得大于商品【折后价】');
							return false;
						}
						$refund_amount += parseInt($goods_refund_amount);
					});
					//可退款金额
					$ktk_amount = $('#ktk_amount').html();
					if(($ktk_amount-$refund_amount)<0){
						alertify.alert("【本次退款金额】不得大于【可退款金额】");
						return false;
					}
					//return false;
					//将本次退款金额添加到formdata里
					formdata[formdata.length]={"name":"refund[refund_amount]","value":$refund_amount};
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("申请退款创建成功");
								$this.dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/refund/ajax_add_post'); ?>',
						'async':false
					});
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

//	申请开票
   $(".apply_invoice_1").click(function(){

   	id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'order_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/invoice/ajax_add?type=1001&order_type=6'); ?>',
			'cache':false
		});
        $('#operation_dialog').dialog({
			title:'申请开票',
			modal: true,
			width:1000,
			height:450,
			buttons: [{
				text:"提交",
				Class:"btn btn-primary",
				click: function(){
					 $this = $(this);
					 formdata = $("#invoice-form").serializeArray();
					 var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					// //本次入账金额
					// $pay_amount = $('[name="salespay[salespay_pay_amount]"]').val();
					// //可入账金额
					// $krz_amount = $('#kdz_amount').html();
					// if(($krz_amount-$pay_amount)<0){
					// 	alertify.alert("【入账金额】不得大于【可入账金额】");
					// 	return false;
					// }
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data == 'amount'){
								alert('检查开票金额');
							}else  if(data==1){
								alertify.alert("成功");
								 $('#operation_dialog').dialog("close");
							}else if(data==2){
								alertify.alert("开票额度超出");
							 }else if(data=='失败'){
							 	alertify.alert("错了");
							 }else{
							 	alert(data);
							 }
						},
						'url':'<?php echo site_url('www/invoice/ajax_add_post?type=1001'); ?>',
						'async':false
					});
				}
				},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了此次操作");
					$(this).dialog("close");
				}
			}]
		});
		  $('#operation_dialog').dialog('open');

   })

   //申请预开票
   $(".apply_invoice_2").click(function(){

   	id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'order_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/invoice/ajax_add?type=1002&order_type=6'); ?>',
			'cache':false
		});
        $('#operation_dialog').dialog({
			title:'申请预开票',
			modal: true,
			width:1000,
			height:450,
			buttons: [{
				text:"提交",
				Class:"btn btn-primary",
				click: function(){
					 $this = $(this);
					 formdata = $("#invoice-form").serializeArray();
					 var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					// //本次入账金额
					// $pay_amount = $('[name="salespay[salespay_pay_amount]"]').val();
					// //可入账金额
					// $krz_amount = $('#kdz_amount').html();
					// if(($krz_amount-$pay_amount)<0){
					// 	alertify.alert("【入账金额】不得大于【可入账金额】");
					// 	return false;
					// }
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("操作成功");
								//成功后关闭当前弹框

								 $('#operation_dialog').dialog("close");
							}else if(data==2){
								alertify.alert("开票额度超出");
							 }else if(data=='失败'){
							 	alertify.alert("错了");
							 }else{
							 	alert(data);
							 }
						},
						'url':'<?php echo site_url('www/invoice/ajax_add_post?type=1002'); ?>',
						'async':false
					});
				}
				},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了此次操作");
					$(this).dialog("close");
				}
			}]
		});
		  $('#operation_dialog').dialog('open');

   })


	//财务人员自主开票
	 $(".financial_invoice").click(function(){
   	 id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'order_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/invoice/ajax_add?type=1003&order_type=6'); ?>',
			'cache':false
		});
	 $('#operation_dialog').dialog({
			title:'财务人员自主开票',
			modal: true,
			width:1000,
			height:450,
			buttons: [{
				text:"提交",
				Class:"btn btn-primary",
				click: function(){
					 $this = $(this);
					 formdata = $("#invoice-form").serializeArray();
					 var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					// //本次入账金额
					// $pay_amount = $('[name="salespay[salespay_pay_amount]"]').val();
					// //可入账金额
					// $krz_amount = $('#kdz_amount').html();
					// if(($krz_amount-$pay_amount)<0){
					// 	alertify.alert("【入账金额】不得大于【可入账金额】");
					// 	return false;
					// }
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("操作成功");
								//成功后关闭当前弹框



								 $('#operation_dialog').dialog("close");
							}else if(data==2){
								alertify.alert("开票额度超出");
							 }else if(data=='失败'){
							 	alertify.alert("错了");
							 }else{
							 	alert(data);
							 }
						},
						'url':'<?php echo site_url('www/invoice/ajax_add_post?type=1003'); ?>',
						'async':false
					});
				}
				},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了此次操作");
					$(this).dialog("close");
				}
			}]
		});
		  $('#operation_dialog').dialog('open');
   })

	//申请作废&变更
	$('.order_cancel').click(function(){
		id = $(this).attr('id');
		cancel = $(this).attr('cancel');
		$.ajax({
			'type': 'get',
			'data': 'order_id=' + id + '&cancel=' + cancel,
			'success': function(data) {
				var pattern = /^[1-9]\d*|0$/;
				if (pattern.test(data) && (data>0)) {
					alertify.error("请先作废开票数据");
					$('#operation_dialog').dialog("close");
					return false;
				}
				$('#operation_dialog').html(data);
			},
			'url': '<?php echo site_url('www/cancel/order_cancel'); ?>',
			'cache': false
		});
		$('#operation_dialog').dialog({
			title: '申请作废&变更',
			modal: true,
			width: 980,
			height: 600,
			buttons: [{
				text: "提交",
				Class: "btn btn-primary",
				click: function() {
					$this = $(this);
					formdata = $("#cancel-form").serializeArray();
					$.ajax({
						'type': 'post',
						'data': formdata,
						'success': function(data) {
							if (data == 1) {
								alertify.success("已申请，等待审核");
								$this.dialog("close");
								$("#order_list").ajaxHtml({
									url:"<?php echo site_url('www/order/new_ajax_select?type_id=6'); ?>",
									data:<?php echo json_encode($sel_data);?>,
								});
							} else if(data == 'remark error'){
								alertify.error("需要填写备注内容");
								$('#remark').focus();
							} else if(data == 'order error'){
								alertify.error("订单存在关联，无法申请作废&变更");
							} else if(data == 'none order'){
								alertify.error("变更订单不存在");
							} else {
								alertify.error( '失败 ' + data );
							}
						},
						'url': '<?php echo site_url('www/cancel/ajax_order_cancel'); ?>',
						'async': false
					});
				}
			},{
				text: "关闭",
				Class: "btn",
				click: function() {
					$this = $(this);
					$this.dialog("close");
					$("#order_list").ajaxHtml({
						url:"<?php echo site_url('www/order/new_ajax_select?type_id=6'); ?>",
						data:<?php echo json_encode($sel_data);?>,
					});
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

	//主管审核作废
	$('.cancel_check').click(function() {
		id = $(this).attr('id');
		cancel = $(this).attr('cancel');
		$.ajax({
			'type': 'get',
			'data': 'order_id=' + id + '&cancel=' + cancel,
			'success': function(data) {
			$('#operation_dialog').html(data);
		},
		'url': '<?php echo site_url('www/cancel/cancel_check'); ?>',
		'cache': false
		});
	$('#operation_dialog').dialog({
			title: '审核',
			modal: true,
			width: 980,
			height: 550,
			buttons: [{
				text: "通过审核",
				Class: "btn btn-primary",
				click: function() {
					$this = $(this);
					formdata = $("#cancel-form").serializeArray();

					$.ajax({
						'type': 'post',
						'data': formdata,
						'success': function(data) {
							if (data == 1) {
								alertify.success("通过审核，待财务确认");
								$this.dialog("close");
								$("#order_list").ajaxHtml({
									url:"<?php echo site_url('www/order/new_ajax_select?type_id=6'); ?>",
									data:<?php echo json_encode($sel_data);?>,
								});
							} else if(data == 'remark error'){
								alertify.error("需要填写备注内容");
								$('#remark').focus();
							} else {
								alertify.error( '失败 ' + data );
							}
						},
						'url': '<?php echo site_url('www/cancel/ajax_cancel_check'); ?>',
						'async': false
					});
				}
			},{
				text: "拒绝",
				Class: "btn btn-primary",
				click: function() {
					$this = $(this);
					formdata = $("#cancel-form").serializeArray();
					formdata[formdata.length] = {"name": "cancel[refuse]", "value": 1};
					$.ajax({
					'type': 'post',
					'data': formdata,
					'success': function(data) {
						if (data == 1) {
							alertify.error("已拒绝");
							$this.dialog("close");
							$("#order_list").ajaxHtml({
								url:"<?php echo site_url('www/order/new_ajax_select?type_id=6'); ?>",
								data:<?php echo json_encode($sel_data);?>,
							});
						} else if(data == 'remark error'){
							alertify.error("需要填写备注内容");
							$('#remark').focus();
						} else {
							alertify.error( '失败 ' + data );
						}
					},
					'url': '<?php echo site_url('www/cancel/ajax_cancel_check'); ?>',
					'async': false
					});
				}
			},{
				text: "关闭",
				Class: "btn",
				click: function() {
					$this = $(this);
					$this.dialog("close");
					$("#order_list").ajaxHtml({
						url:"<?php echo site_url('www/order/new_ajax_select?type_id=6'); ?>",
						data:<?php echo json_encode($sel_data);?>,
					});
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

	//财务确认作废
	$('.cancel_affirm').click(function() {
		id = $(this).attr('id');
		cancel = $(this).attr('cancel');
		$.ajax({
			'type': 'get',
			'data': 'order_id=' + id + '&cancel=' + cancel,
			'success': function(data) {
			$('#operation_dialog').html(data);
		},
		'url': '<?php echo site_url('www/cancel/cancel_affirm'); ?>',
		'cache': false
		});
	$('#operation_dialog').dialog({
			title: '审核',
			modal: true,
			width: 980,
			height: 550,
			buttons: [{
				text: "通过",
				Class: "btn btn-primary",
				click: function() {
					$this = $(this);
					formdata = $("#cancel-form").serializeArray();
					$.ajax({
						'type': 'post',
						'data': formdata,
						'success': function(data) {
							if (data == 1) {
								alertify.success("已处理");
								$this.dialog("close");
								$("#order_list").ajaxHtml({
									url:"<?php echo site_url('www/order/new_ajax_select?type_id=6'); ?>",
									data:<?php echo json_encode($sel_data);?>,
								});
							} else if(data == 'remark error'){
								alertify.error("需要填写备注内容");
								$('#remark').focus();
							} else {
								alertify.error( '失败 ' + data );
							}
						},
						'url': '<?php echo site_url('www/cancel/ajax_cancel_affirm'); ?>',
						'async': false
					});
				}
			},{
				text: "拒绝",
				Class: "btn btn-primary",
				click: function() {
					$this = $(this);
					formdata = $("#cancel-form").serializeArray();
					formdata[formdata.length] = {"name": "cancel[refuse]", "value": 1};
					$.ajax({
					'type': 'post',
					'data': formdata,
					'success': function(data) {
						if (data == 1) {
							alertify.error("已拒绝");
							$this.dialog("close");
							$("#order_list").ajaxHtml({
								url:"<?php echo site_url('www/order/new_ajax_select?type_id=6'); ?>",
								data:<?php echo json_encode($sel_data);?>,
							});
						} else if(data == 'remark error'){
							alertify.error("需要填写备注内容");
							$('#remark').focus();
						} else {
							alertify.error( '失败 ' + data );
						}
					},
					'url': '<?php echo site_url('www/cancel/ajax_cancel_affirm'); ?>',
					'async': false
					});
				}
			},{
				text: "关闭",
				Class: "btn",
				click: function() {
					$this = $(this);
					$this.dialog("close");
					$("#order_list").ajaxHtml({
						url:"<?php echo site_url('www/order/new_ajax_select?type_id=6'); ?>",
						data:<?php echo json_encode($sel_data);?>,
					});
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

	// 订单转移
	$('.order_shift').click(function() {
		id = $(this).attr('id');
		$.ajax({
			'type': 'get',
			'data': 'order_id=' + id,
			'success': function(data) {
			$('#operation_dialog').html(data);
		},
		'url': '<?php echo site_url('www/order/order_shift'); ?>',
		'cache': false
		});
	$('#operation_dialog').dialog({
			title: '订单转移',
			modal: true,
			width: 980,
			height: 500,
			buttons: [{
				text: "转移",
				Class: "btn btn-primary",
				click: function() {
					$this = $(this);
					formdata = $("#shift-form").serializeArray();
					$.ajax({
						'type': 'post',
						'data': formdata,
						'success': function(data) {
							if (data == 1) {
								alertify.success("已处理");
								$this.dialog("close");
								$("#order_list").ajaxHtml({
									url:"<?php echo site_url('www/order/new_ajax_select?type_id=6'); ?>",
									data:<?php echo json_encode($sel_data);?>,
								});
							} else {
								alertify.error( '失败 ' + data );
							}
						},
						'url': '<?php echo site_url('www/order/ajax_order_shift'); ?>',
						'async': false
					});
				}
			},{
				text: "关闭",
				Class: "btn",
				click: function() {
					$this = $(this);
					$this.dialog("close");
					$("#order_list").ajaxHtml({
						url:"<?php echo site_url('www/order/new_ajax_select?type_id=6'); ?>",
						data:<?php echo json_encode($sel_data);?>,
					});
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

	// 上传附件
	$('.order_adjunct').click(function() {
		id = $(this).attr('id');
		number = $(this).attr('number');
		$.ajax({
			'type': 'get',
			'data': 'order_id=' + id,
			'success': function(data) {
			$('#operation_dialog').html(data);
		},
		'url': '<?php echo site_url('www/order/order_adjunct'); ?>',
		'cache': false
		});
		$('#operation_dialog').dialog({
			title: '订单附件<span style="padding-left:10px;font-size:12px;">编号'+number+'</span>',
			modal: true,
			width: 700,
			height: 475,
			buttons: [{
				text: "关闭",
				Class: "btn",
				click: function() {
					$this = $(this);
					$this.dialog("close");
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

});
</script>