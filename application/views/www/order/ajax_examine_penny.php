<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="examine_penny_form">
		<!-- 用一个隐藏文本框来存order_id -->
		<input size="16" type="hidden" name="order[order_id]" value='<?php echo $order_data['order_id'];?>'>
		<input type="hidden" name='penny' value='1'>

<table class="leetable table table-bordered">
	<tbody>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_transfer_name'];?>
			</td>
			<td class="TbRight" colspan='3'>
				<?php echo $order_data['order_transfer_name'];?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_number'];?>
			</td>
			<td class="TbRight">
				<?php echo $order_data['order_number'];?>
			</td>
			<td class="TbLeft">
				<?php echo $labels['order_transfer_date'];?>
			</td>
			<td class="TbRight">
				<?php
					if($order_data['order_transfer_date']!="" and $order_data['order_transfer_date']!="0000-00-00"){
						echo date("Y-m-d",strtotime($order_data['order_transfer_date']));
					}else{
						echo "&nbsp";
					}
				?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_change_into'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($order_data['order_change_into']!=0 and $order_data['order_change_into']!=""){
					echo $order_data['order_change_into_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft">
				<?php echo  $labels['order_change_out'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($order_data['order_change_out']!=0 and $order_data['order_change_out']!=""){
					echo $order_data['order_change_out_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_out_examine'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($order_data['order_out_examine']!=0 and $order_data['order_out_examine']!=""){
					echo $order_data['order_out_examine_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft">
				<?php echo $labels['order_transfer_state'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($order_data['order_transfer_state']!=0 and $order_data['order_transfer_state']!=""){
					echo $order_data['order_transfer_state_arr']['enum_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_relation_order_id'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($order_data['order_relation_order_id']!=0 and $order_data['order_relation_order_id']!=""){
					echo $order_data['order_relation_order_id_arr']['order_number'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft">
				<?php echo  $labels['order_change_into_money'];?>
			</td>
			<td class="TbRight">
				<?php echo $order_data['order_change_into_money'];?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo  $labels['order_transfer_reason'];?>
			</td>
			<td class="TbRight" colspan='3'>
				<pre><?php echo $order_data['order_transfer_reason'];?></pre>
			</td>
		</tr>
	</tbody>
</table>
	</form>
</div>
<!--lee 内容部分 end-->