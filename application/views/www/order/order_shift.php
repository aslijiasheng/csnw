<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<form id="shift-form" action="" method="post" >
				<input type="hidden" name="order_id" value="<?php echo $data['order_id']; ?>">
				<table class="leetable table table-bordered">
					<tbody>
						<tr>
							<td class="TbLeft">
								<?php echo $labels['order_number']; ?>
							</td>
							<td class="TbRight" colspan='3'>
								<?php echo $data['order_number']; ?>
							</td>
						</tr>
						<tr>
							<td class="TbLeft">
								<?php echo $labels['order_typein']; ?>
							</td>
							<td class="TbRight">
								<?php
								if ($data['order_typein'] != 0 and $data['order_typein'] != "") {
									echo $data['order_typein_arr']['user_name'];
								} else {
									echo "&nbsp";
								}
								?>
								<div style="float:right;margin-right:25px;">
									<input type="text" name="order[order_typein]" id="order_typein" value_id ="" value="">
									<script type="text/javascript">
										$(document).ready(function() {
											$('#order_typein').leeQuote({
												url: '<?php echo site_url('www/user/ajax_list?tag_name=order_typein'); ?>',
												title: '选择录入人'
											});
										});
									</script>
								</div>
							</td>
							<td class="TbLeft">
								<?php echo $labels['order_create_time']; ?>
							</td>
							<td class="TbRight">
								<?php echo $data['order_create_time']; ?>
							</td>
						</tr>
						<tr>
							<td class="TbLeft">
								销售人员
							</td>
							<td class="TbRight">
								<?php
								if ($data['order_owner'] != 0 and $data['order_owner'] != "") {
									echo $data['order_owner_arr']['user_name'];
								} else {
									echo "&nbsp";
								}
								?>
								<div style="float:right;margin-right:25px;">
									<input type="text" name="order[order_owner]" id="order_owner" value_id ="" value="">
									<script type="text/javascript">
										$(document).ready(function() {
											$('#order_owner').leeQuote({
												url: '<?php echo site_url('www/user/ajax_list?tag_name=order_owner'); ?>',
												title: '选择销售'
											});
										});
									</script>
								</div>
							</td>
							<td class="TbLeft">
								<?php echo $labels['order_department']; ?>
							</td>
							<td class="TbRight">
								<?php
								if ($data['order_department'] != 0 and $data['order_department'] != "") {
									//直接判断，不使用递归
									$d1 = $d2 = $d3 = $d4 = $d5 = '';
									$d1 = $data['order_department_arr']['department_name'];
									if (isset($data['order_department_arr']['department_uid_arr'])) {
										$d2 = $data['order_department_arr']['department_uid_arr']['department_name'];
										if (isset($data['order_department_arr']['department_uid_arr']['department_uid_arr'])) {
											$d3 = $data['order_department_arr']['department_uid_arr']['department_uid_arr']['department_name'];
											if (isset($data['order_department_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr'])) {
												$d4 = $data['order_department_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr']['department_name'];
												if (isset($data['order_department_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr'])) {
													$d5 = $data['order_department_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr']['department_name'];
												}
											}
										}
									}
									$s = trim($d5 . ' - ' . $d4 . ' - ' . $d3 . ' - ' . $d2 . ' - ' . $d1, ' - ');
									echo $s;
								} else {
									echo "&nbsp";
								}
								?>
							</td>
						</tr>
						<tr>
							<td class="TbLeft">
								<?php echo $labels['order_agreement_name']; ?>
							</td>
							<td class="TbRight">
								<?php echo $data['order_agreement_name']; ?>
							</td>
							<td class="TbLeft">
								<?php echo $labels['order_agreement_no']; ?>
							</td>
							<td class="TbRight">
								<?php echo $data['order_agreement_no']; ?>
							</td>
						</tr>
						<tr>
							<td class="TbLeft">
								客户名称
							</td>
							<td class="TbRight">
								<?php
								if ($data['order_account'] != 0 and $data['order_account'] != "") {
									echo $data['order_account_arr']['account_name'];
								} else {
									echo "&nbsp";
								}
								?>
							</td>
							<td class="TbLeft">
								客户商业ID
							</td>
							<td class="TbRight">
								<?php
								if ($data['order_account'] != 0 and $data['order_account'] != "") {
									echo $data['order_account_arr']['account_account_shopexid'];
								} else {
									echo "&nbsp";
								}
								?>
							</td>
						</tr>
						<tr>
							<td class="TbLeft">
								<?php echo $labels['order_agent']; ?>
							</td>
							<td class="TbRight">
								<?php
								if ($data['order_agent'] != 0 and $data['order_agent'] != "") {
									echo $data['order_agent_arr']['partner_name'];
								} else {
									echo "&nbsp";
								}
								?>
							</td>
							<td class="TbLeft">
								邮箱
							</td>
							<td class="TbRight">
								<?php
								if ($data['order_account'] != 0 and $data['order_account'] != "") {
									echo $data['order_account_arr']['account_email'];
								} else {
									echo "&nbsp";
								}
								?>
							</td>
						</tr>
						<tr>
							<td class="TbLeft">
								<?php echo $labels['order_finance']; ?>
							</td>
							<td class="TbRight">
								<?php
								if ($data['order_finance'] != 0 and $data['order_finance'] != "") {
									echo $data['order_finance_arr']['user_name'];
								} else {
									echo "&nbsp";
								}
								?>
								<div style="float:right;margin-right:25px;">
									<input type="text" name="order[order_finance]" id="order_finance" value_id ="" value="">
									<script type="text/javascript">
										$(document).ready(function() {
											$('#order_finance').leeQuote({
												url: '<?php echo site_url('www/user/ajax_list?tag_name=order_finance'); ?>',
												title: '选择财务'
											});
										});
									</script>
								</div>
							</td>
							<td class="TbLeft">
								<?php echo $labels['order_review']; ?>
							</td>
							<td class="TbRight">
								<?php
								if ($data['order_review'] != 0 and $data['order_review'] != "") {
									echo $data['order_review_arr']['user_name'];
								} else {
									echo "&nbsp";
								}
								?>
								<div style="float:right;margin-right:25px;">
									<input type="text" name="order[order_review]" id="order_review" value_id ="" value="">
									<script type="text/javascript">
										$(document).ready(function() {
											$('#order_review').leeQuote({
												url: '<?php echo site_url('www/user/ajax_list?tag_name=order_review'); ?>',
												title: '选择业务主管'
											});
										});
									</script>
								</div>
							</td>
						</tr>
						<tr>
							<td class="TbLeft">
								<?php echo $labels['order_nreview']; ?>
							</td>
							<td class="TbRight">
								<?php
								if ($data['order_nreview'] != 0 and $data['order_nreview'] != "") {
									echo $data['order_nreview_arr']['user_name'];
								} else {
									echo "&nbsp";
								}
								?>
							</td>
							<td class="TbLeft">
								到款备注
							</td>
							<td class="TbRight">

							</td>
						</tr>
						<?php if (isset($data['order_is_cancel']) && $data['order_is_cancel'] == '1002'): ?>
							<tr>
								<td class="TbLeft">
									作废关联订单
								</td>
								<td class="TbRight" colspan='3'>
									<?php
									if ($data['order_is_cancel'] != 0 and $data['order_is_cancel'] != "") {
										echo '<a href="?order_id=' . $data['order_cancel_order_id_arr']['order_id'] . '">' . $data['order_cancel_order_id_arr']['order_number'] . '</a>';
									} else {
										echo "&nbsp";
									}
									?>
								</td>
								</td>
							</tr>
						<?php endif; ?>
					</tbody>
				</table>
			</form>
		</div>
	</div>
	<script>
		$(document).ready(function() {
		})
	</script>