<script src="<?php echo base_url(); ?>style/admin/js/ajaxfileupload.js"></script>
<script type="text/javascript">
	function ajaxFileUpload() {
		$("#loading").ajaxStart(function() {
			$(this).show();
		}).ajaxComplete(function() {
			$(this).hide();
		});

		$.ajaxFileUpload({
			url: '<?php echo site_url("www/order/ajax_upload_order_adjunct"); ?>',
			secureuri: false,
			fileElementId: 'fileToUpload',
			dataType: 'json',
			data: {order_id: '<?php echo $_GET['order_id'] ?>'},
			success: function(data, status) {
				if (typeof (data.res) != 'undefined') {
					if (data.res == 'fail') {
						alert(data.msg);
					} else if (data.res == 'succ') {
						// alert(data.msg);
						$(".content").append(data.info);
					}
				}
			}, error: function(data, status, e) {
				alert(e);
			}
		})
		return false;
	}

	function delete_order_adjunct(e) {
		$("#loading").ajaxStart(function() {
			$(this).show();
		}).ajaxComplete(function() {
			$(this).hide();
		});

		var id = $(e).attr("id");
		$.ajax({
			'type': 'get',
			'data': 'adjunct_id=' + id,
			'success': function(data) {
				if (data == 1) {
					// alert('已删除');
					$(e).parent().remove();
				}
			},
			'url': '<?php echo site_url('www/order/ajax_delete_order_adjunct'); ?>',
			'cache': false,
			'async': false
		});
	}
</script>
<style>
	.content{border:1px solid #ccc;}
	.adjunct_box img{width:40px; height:40px;}
	.adjunct_box a span{margin-left:15px;}
</style>
<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<h5>已上传的文件</h5>
			<div class="content">
				<?php foreach ($files as $k => $v) : ?>
					<div class="adjunct_box">
						<img src="<?php echo $v['type']; ?>"/>
						<a href="<?php echo base_url() . $v['order_adjunct_url']; ?>" target="_blank"><?php echo $v['order_adjunct_name']; ?></a>
						<a id="<?php echo $v['order_adjunct_id']; ?>" onclick="delete_order_adjunct(this)"><span class="icon-close" style="font-size: 16px;"></span></a>
					</div>
				<?php endforeach; ?>
			</div>
			<form name="form" action="" method="POST" enctype="multipart/form-data" style="margin-top:20px;">

				<thead>
					<tr>
						<td><h5>选择需要上传的文件</h5></td>
						<td><input id="fileToUpload" type="file" size="45" name="fileToUpload" class="input"></td>
						<td><button class="button" id="buttonUpload" onclick="return ajaxFileUpload();">上传</button></td>
					</tr>
				<img id="loading" src="<?php echo base_url(); ?>style/admin/images/loading.gif" style="display:none;">
				</table>
			</form>
		</div>
	</div>