<div class="all-sidebar">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        商品
                        <span class="mini-title">
                            goods
                        </span>
                    </div>
                    <span class="tools">
                        <!-- 窗口按钮部门
                        <a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
                        -->
                    </span>
                </div>
                <div class="widget-body">
                    <div class="row-fluid">
                        <div class="leebutton">
                            <!--lee 按钮部分 start-->
                            <a id="newgoods" class="btn btn-primary" href="<?php
                            if (!isset($_GET['type_id'])) {
                                echo site_url('www/goods');
                            } else {
                                echo site_url('www/goods?type_id=' . $_GET['type_id']);
                            }
                            ?>">
                                <i class="icon-file"></i>
                                返回
                            </a>
                            <!--lee 按钮部分 end-->
                        </div>
                        <div id="divmessagelist">
                            <div>
                                <div id="objlist" class="grid-view">
                                    <table class="leetable table table-bordered">
                                        <tbody>
                                            <?PHP $listData_value = $listLayout; ?>

                                            <?php if (0 == 1) { ?>
                                                <?php if (!isset($_GET['type_id'])) { ?>

                                                    <tr>
                                                        <td>
                                                            <?php echo $labels['goods_name']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $listData_value['goods_name']; ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <?php echo $labels['goods_type']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $listData_value['goods_type_arr']['enum_name']; ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <?php echo $labels['goods_check_pay']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $listData_value['goods_check_pay_arr']['enum_name']; ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <?php echo $labels['goods_trail_days']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $listData_value['goods_trail_days']; ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <?php echo $labels['goods_code']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $listData_value['goods_code']; ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <?php echo $labels['goods_is_new']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $listData_value['goods_is_new']; ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <?php echo $labels['goods_visable']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $listData_value['goods_visable_arr']['enum_name']; ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <?php echo $labels['goods_sale_type']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo!empty($listData_value['goods_sale_type_arr']) ? $listData_value['goods_sale_type_arr']['enum_name'] : ''; ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <?php echo $labels['goods_is_trail']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $listData_value['goods_is_trail_arr']['enum_name']; ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <?php echo $labels['goods_desc']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $listData_value['goods_desc']; ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <?php echo $labels['goods_is_sale']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $listData_value['goods_is_sale_arr']['enum_name']; ?>
                                                        </td>
                                                    </tr>

                                                <?php } else { ?>
                                                    <?php
                                                    switch ($_GET["type_id"]) {
                                                        case -1:
                                                            break;
                                                            ?>

                                                        <?php default: ?>

                                                            <tr>


                                                                <td>
                                                                    <?php echo $labels['goods_name']; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $listData_value['goods_name']; ?>
                                                                </td>

                                                            </tr>

                                                            <tr>


                                                                <td>
                                                                    <?php echo $labels['goods_type']; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $listData_value['goods_type_arr']['enum_name']; ?>
                                                                </td>

                                                            </tr>

                                                            <tr>


                                                                <td>
                                                                    <?php echo $labels['goods_check_pay']; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $listData_value['goods_check_pay_arr']['enum_name']; ?>
                                                                </td>

                                                            </tr>

                                                            <tr>


                                                                <td>
                                                                    <?php echo $labels['goods_trail_days']; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $listData_value['goods_trail_days']; ?>
                                                                </td>

                                                            </tr>

                                                            <tr>


                                                                <td>
                                                                    <?php echo $labels['goods_code']; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $listData_value['goods_code']; ?>
                                                                </td>

                                                            </tr>

                                                            <tr>


                                                                <td>
                                                                    <?php echo $labels['goods_is_new']; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $listData_value['goods_is_new']; ?>
                                                                </td>

                                                            </tr>

                                                            <tr>


                                                                <td>
                                                                    <?php echo $labels['goods_visable']; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $listData_value['goods_visable_arr']['enum_name']; ?>
                                                                </td>

                                                            </tr>

                                                            <tr>


                                                                <td>
                                                                    <?php echo $labels['goods_sale_type']; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo!empty($listData_value['goods_sale_type_arr']) ? $listData_value['goods_sale_type_arr']['enum_name'] : ''; ?>
                                                                </td>

                                                            </tr>

                                                            <tr>


                                                                <td>
                                                                    <?php echo $labels['goods_is_trail']; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $listData_value['goods_is_trail_arr']['enum_name']; ?>
                                                                </td>

                                                            </tr>

                                                            <tr>


                                                                <td>
                                                                    <?php echo $labels['goods_desc']; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $listData_value['goods_desc']; ?>
                                                                </td>

                                                            </tr>

                                                            <tr>


                                                                <td>
                                                                    <?php echo $labels['goods_is_sale']; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $listData_value['goods_is_sale_arr']['enum_name']; ?>
                                                                </td>

                                                            </tr>
                                                            <tr>
                                                                <?php
                                                                foreach ($products as $v) {
                                                                    echo $v['procut_name'];
                                                                }
                                                                ?>
                                                            </tr>
                                                    <?php } ?>
                                                    <?php
                                                }
                                            } else {
                                                ?>

                                                <tr>


                                                    <td>
                                                        <?php echo $labels['goods_name']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $listData_value['goods_name']; ?>
                                                    </td>

                                                </tr>

                                                <tr>


                                                    <td>
                                                        <?php echo $labels['goods_type']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $listData_value['goods_type_arr']['enum_name']; ?>
                                                    </td>

                                                </tr>

                                                <tr>


                                                    <td>
                                                        <?php echo $labels['goods_check_pay']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $listData_value['goods_check_pay_arr']['enum_name']; ?>
                                                    </td>

                                                </tr>

                                                <tr>


                                                    <td>
                                                        <?php echo $labels['goods_trail_days']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $listData_value['goods_trail_days']; ?>
                                                    </td>

                                                </tr>

                                                <tr>


                                                    <td>
                                                        <?php echo $labels['goods_code']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $listData_value['goods_code']; ?>
                                                    </td>

                                                </tr>

                                                <tr>


                                                    <td>
                                                        <?php echo $labels['goods_is_new']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $listData_value['goods_is_new']; ?>
                                                    </td>

                                                </tr>

                                                <tr>


                                                    <td>
                                                        <?php echo $labels['goods_visable']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $listData_value['goods_visable_arr']['enum_name']; ?>
                                                    </td>

                                                </tr>

                                                <tr>


                                                    <td>
                                                        <?php echo $labels['goods_sale_type']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo!empty($listData_value['goods_sale_type_arr']['enum_name']) ? $listData_value['goods_sale_type_arr']['enum_name'] : ''; ?>
                                                    </td>

                                                </tr>

                                                <tr>


                                                    <td>
                                                        <?php echo $labels['goods_is_trail']; ?>
                                                    </td>
                                                    <td><?php echo $listData_value['goods_is_trail_arr']['enum_name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $labels['goods_desc']; ?></td>
                                                    <td><?php echo $listData_value['goods_desc']; ?></td>
                                                </tr>

                                                <tr>


                                                    <td>
                                                        <?php echo $labels['goods_is_sale']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $listData_value['goods_is_sale_arr']['enum_name']; ?>
                                                    </td>

                                                </tr>
                                                <tr>
                                            <table class="leetable table table-bordered">
                                                <th>
                                                    关联基础产品
                                                </th>
                                                <tr>
                                                    <td>基础产品编码</td>
                                                    <td>基础产品名称</td>
                                                    <td>分配比</td>
                                                </tr>
                                                <?php foreach ($products as $v) { ?>
                                                    <tr>
                                                        <td><?php echo $v['product_basic_code']; ?></td>
                                                        <td><?php echo $v['product_basic_name']; ?></td>
                                                        <td><?php echo $v['product_basic_name']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                            <table class="leetable table table-bordered">
                                                <th>
                                                    价格信息
                                                </th>
                                                <tr>
                                                    <td>价格编号</td>
                                                    <td>一次性价格</td>
                                                    <td>有效时间</td>
                                                </tr>
                                                <?php foreach ($goods_price as $v) { ?>
                                                    <tr>
                                                        <td><?php echo $v['goods_price_code'] ?></td>
                                                        <td><?php echo $v['goods_price_cycle_price'] ?></td>
                                                        <td><?php echo $v['goods_price_effective_days'] ?> 天</td>
                                                    </tr>

                                                    <?php } ?>


                                                </table>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>