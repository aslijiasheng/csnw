<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="30">操作</th>
            <th width="100"><?php echo $labels['goods_code']; ?></th>
            <th width="55"><?php echo $labels['goods_type']; ?></th>
            <th width=""><?php echo $labels['goods_name']; ?></th>
            <th width="55"><?php echo $labels['goods_check_pay']; ?></th>
            <th width="55"><?php echo $labels['goods_visable']; ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listData as $listData_value) { ?>
            <tr>
                <td style="font-size:16px;">
                    <a href="<?php echo site_url('www/goods/view') . '?goods_id=' . $listData_value['goods_id'] . '&type_id=' . $_GET['type_id'] ?>" rel="tooltip" title="查看"><span class="icon-eye"></span></a>
                </td>
                <td>
                    <?php echo $listData_value['goods_code']; ?>
                </td>
                <td>
                    <?php echo $listData_value['goods_type_arr']['enum_name']; ?>
                </td>
                <td>
                    <?php echo $listData_value['goods_name']; ?>
                </td>
                <td>
                    <?php echo !empty($listData_value['goods_check_pay_arr']['enum_name']) ? $listData_value['goods_check_pay_arr']['enum_name']: ''; ?>
                </td>
                <td>
                    <?php echo $listData_value['goods_visable_arr']['enum_name']; ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber; ?>条</div>
<input type='hidden' id='totalNumber' value="<?php echo $totalNumber; ?>">
</div>