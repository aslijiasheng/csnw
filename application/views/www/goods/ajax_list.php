<div class="all-sidebar">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <?php
                        if (isset($_GET['type_id'])) {
                            if (isset($type_arr[$_GET['type_id']]['type_name'])) {
                                echo $type_arr[$_GET['type_id']]['type_name'];
                            } else {
                                echo '商品';
                            }
                        } else {
                            echo '商品';
                        }
                        ?>
                        <span class="mini-title">
                            goods
                        </span>
                    </div>
                    <span class="tools">
                        <!-- 窗口按钮部门
                        <a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
                        -->
                    </span>
                </div>
                <div class="widget-body">
                    <div class="row-fluid">
                        <div class="leebutton">

                        </div>
                        <div id="divmessagelist">

                            <div class="grid-view">
                                <div id="goods_list"></div>
                                <div>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            $selectAttr = [
                                                {'value': 'name', 'txt': '商品名称'},
                                                {'value': 'utime', 'txt': '商品信息更新时间'},
                                                {'value': 'type', 'txt': '是标准商品或者捆绑商品'},
                                                {'value': 'check_pay', 'txt': '计费方式'},
                                                {'value': 'trail_days', 'txt': '试用天数（天）'},
                                                {'value': 'code', 'txt': '商品编码'},
                                                {'value': 'is_new', 'txt': ''},
                                                {'value': 'visable', 'txt': '是否启用'},
                                                {'value': 'sale_type', 'txt': '售卖方分类'},
                                                {'value': 'is_trail', 'txt': '是否可试用'},
                                                {'value': 'desc', 'txt': '商品简介'},
                                                {'value': 'is_sale', 'txt': '是否允许单独售卖'},
                                            ];

                                            $("#goods_list").leeDataTable({
                                                selectAttr: $selectAttr, //简单查询的查询属性
                                                url: "<?php echo site_url('www/goods/ajax_select_quote?hidden_v=' . $_GET['hidden_v']); ?>", //ajax查询的地址
                                                perNumber: 20, //每页显示多少条数据

                                            });
                                        });

                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

