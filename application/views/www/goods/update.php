<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						商品新建页面
						<span class="mini-title">
						goods
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/goods/')?>">
<i class="icon-undo"></i>
返回商品列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php if(!isset($_GET['type_id'])){echo site_url('www/goods/update?goods_id='.$_GET['goods_id']);}else{echo site_url('www/goods/update?goods_id='.$_GET['goods_id'].'&type_id='.$_GET['type_id']);}?>" method="post">
		<?php if(0==1){?>
		 	<?php if(!isset($_GET['type_id'])){ ?>
		 	  
						<div class="control-group">
							<label class="control-label required" for="goods_code">
								<?php echo $labels["goods_code"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_code]" value="<?php echo $id_aData['goods_code'] ?>" id="goods_code">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_code]" id="goods_code">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_name">
								<?php echo $labels["goods_name"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_name]" value="<?php echo $id_aData['goods_name'] ?>" id="goods_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_name]" id="goods_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_type">
								<?php echo $labels["goods_type"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_type_enum as $type_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $type_v['enum_key']==$id_aData['goods_type_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $type_v['enum_key']?>" name="goods[goods_type]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $type_v['enum_key']?>" name="goods[goods_type]">
<?php }?>
<?php echo $type_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_visable">
								<?php echo $labels["goods_visable"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_visable_enum as $visable_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $visable_v['enum_key']==$id_aData['goods_visable_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $visable_v['enum_key']?>" name="goods[goods_visable]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $visable_v['enum_key']?>" name="goods[goods_visable]">
<?php }?>
<?php echo $visable_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_is_sale">
								<?php echo $labels["goods_is_sale"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_is_sale_enum as $is_sale_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $is_sale_v['enum_key']==$id_aData['goods_is_sale_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $is_sale_v['enum_key']?>" name="goods[goods_is_sale]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $is_sale_v['enum_key']?>" name="goods[goods_is_sale]">
<?php }?>
<?php echo $is_sale_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_is_trail">
								<?php echo $labels["goods_is_trail"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_is_trail_enum as $is_trail_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $is_trail_v['enum_key']==$id_aData['goods_is_trail_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $is_trail_v['enum_key']?>" name="goods[goods_is_trail]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $is_trail_v['enum_key']?>" name="goods[goods_is_trail]">
<?php }?>
<?php echo $is_trail_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_trail_days">
								<?php echo $labels["goods_trail_days"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_trail_days]" value="<?php echo $id_aData['goods_trail_days'] ?>" id="goods_trail_days">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_trail_days]" id="goods_trail_days">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_sale_type">
								<?php echo $labels["goods_sale_type"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="goods[goods_sale_type]">
<?php foreach($goods_sale_type_enum as $sale_type_v){ ?>
   <?php if(isset($id_aData) && $sale_type_v['enum_key']==$id_aData['goods_sale_type_arr']['enum_key'] ){ ?>
     <option value="<?php echo $sale_type_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $sale_type_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $sale_type_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_check_pay">
								<?php echo $labels["goods_check_pay"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="goods[goods_check_pay]">
<?php foreach($goods_check_pay_enum as $check_pay_v){ ?>
   <?php if(isset($id_aData) && $check_pay_v['enum_key']==$id_aData['goods_check_pay_arr']['enum_key'] ){ ?>
     <option value="<?php echo $check_pay_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $check_pay_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $check_pay_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_utime">
								<?php echo $labels["goods_utime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="goods_utime">
	<input size="16" type="text" name="goods[goods_utime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['goods_utime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['goods_utime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#goods_utime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_desc">
								<?php echo $labels["goods_desc"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_desc]" value="<?php echo $id_aData['goods_desc'] ?>" id="goods_desc">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_desc]" id="goods_desc">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_is_new">
								<?php echo $labels["goods_is_new"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_is_new]" value="<?php echo $id_aData['goods_is_new'] ?>" id="goods_is_new">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_is_new]" id="goods_is_new">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
		 	<?php }else{ ?>
		    <?php switch ($_GET["type_id"]){ 
							case -1:
							break;
						?>
						
						<?php default: ?>
						
						<div class="control-group">
							<label class="control-label required" for="goods_code">
								<?php echo $labels["goods_code"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_code]" value="<?php echo $id_aData['goods_code'] ?>" id="goods_code">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_code]" id="goods_code">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_name">
								<?php echo $labels["goods_name"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_name]" value="<?php echo $id_aData['goods_name'] ?>" id="goods_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_name]" id="goods_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_type">
								<?php echo $labels["goods_type"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_type_enum as $type_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $type_v['enum_key']==$id_aData['goods_type_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $type_v['enum_key']?>" name="goods[goods_type]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $type_v['enum_key']?>" name="goods[goods_type]">
<?php }?>
<?php echo $type_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_visable">
								<?php echo $labels["goods_visable"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_visable_enum as $visable_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $visable_v['enum_key']==$id_aData['goods_visable_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $visable_v['enum_key']?>" name="goods[goods_visable]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $visable_v['enum_key']?>" name="goods[goods_visable]">
<?php }?>
<?php echo $visable_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_is_sale">
								<?php echo $labels["goods_is_sale"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_is_sale_enum as $is_sale_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $is_sale_v['enum_key']==$id_aData['goods_is_sale_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $is_sale_v['enum_key']?>" name="goods[goods_is_sale]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $is_sale_v['enum_key']?>" name="goods[goods_is_sale]">
<?php }?>
<?php echo $is_sale_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_is_trail">
								<?php echo $labels["goods_is_trail"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_is_trail_enum as $is_trail_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $is_trail_v['enum_key']==$id_aData['goods_is_trail_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $is_trail_v['enum_key']?>" name="goods[goods_is_trail]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $is_trail_v['enum_key']?>" name="goods[goods_is_trail]">
<?php }?>
<?php echo $is_trail_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_trail_days">
								<?php echo $labels["goods_trail_days"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_trail_days]" value="<?php echo $id_aData['goods_trail_days'] ?>" id="goods_trail_days">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_trail_days]" id="goods_trail_days">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_sale_type">
								<?php echo $labels["goods_sale_type"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="goods[goods_sale_type]">
<?php foreach($goods_sale_type_enum as $sale_type_v){ ?>
   <?php if(isset($id_aData) && $sale_type_v['enum_key']==$id_aData['goods_sale_type_arr']['enum_key'] ){ ?>
     <option value="<?php echo $sale_type_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $sale_type_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $sale_type_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_check_pay">
								<?php echo $labels["goods_check_pay"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="goods[goods_check_pay]">
<?php foreach($goods_check_pay_enum as $check_pay_v){ ?>
   <?php if(isset($id_aData) && $check_pay_v['enum_key']==$id_aData['goods_check_pay_arr']['enum_key'] ){ ?>
     <option value="<?php echo $check_pay_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $check_pay_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $check_pay_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_utime">
								<?php echo $labels["goods_utime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="goods_utime">
	<input size="16" type="text" name="goods[goods_utime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['goods_utime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['goods_utime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#goods_utime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_desc">
								<?php echo $labels["goods_desc"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_desc]" value="<?php echo $id_aData['goods_desc'] ?>" id="goods_desc">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_desc]" id="goods_desc">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_is_new">
								<?php echo $labels["goods_is_new"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_is_new]" value="<?php echo $id_aData['goods_is_new'] ?>" id="goods_is_new">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_is_new]" id="goods_is_new">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
					<?php }?>
                <?php }}else{ ?>
                  
						<div class="control-group">
							<label class="control-label required" for="goods_code">
								<?php echo $labels["goods_code"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_code]" value="<?php echo $id_aData['goods_code'] ?>" id="goods_code">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_code]" id="goods_code">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_name">
								<?php echo $labels["goods_name"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_name]" value="<?php echo $id_aData['goods_name'] ?>" id="goods_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_name]" id="goods_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_type">
								<?php echo $labels["goods_type"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_type_enum as $type_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $type_v['enum_key']==$id_aData['goods_type_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $type_v['enum_key']?>" name="goods[goods_type]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $type_v['enum_key']?>" name="goods[goods_type]">
<?php }?>
<?php echo $type_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_visable">
								<?php echo $labels["goods_visable"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_visable_enum as $visable_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $visable_v['enum_key']==$id_aData['goods_visable_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $visable_v['enum_key']?>" name="goods[goods_visable]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $visable_v['enum_key']?>" name="goods[goods_visable]">
<?php }?>
<?php echo $visable_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_is_sale">
								<?php echo $labels["goods_is_sale"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_is_sale_enum as $is_sale_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $is_sale_v['enum_key']==$id_aData['goods_is_sale_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $is_sale_v['enum_key']?>" name="goods[goods_is_sale]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $is_sale_v['enum_key']?>" name="goods[goods_is_sale]">
<?php }?>
<?php echo $is_sale_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_is_trail">
								<?php echo $labels["goods_is_trail"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($goods_is_trail_enum as $is_trail_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $is_trail_v['enum_key']==$id_aData['goods_is_trail_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $is_trail_v['enum_key']?>" name="goods[goods_is_trail]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $is_trail_v['enum_key']?>" name="goods[goods_is_trail]">
<?php }?>
<?php echo $is_trail_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_trail_days">
								<?php echo $labels["goods_trail_days"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_trail_days]" value="<?php echo $id_aData['goods_trail_days'] ?>" id="goods_trail_days">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_trail_days]" id="goods_trail_days">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_sale_type">
								<?php echo $labels["goods_sale_type"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="goods[goods_sale_type]">
<?php foreach($goods_sale_type_enum as $sale_type_v){ ?>
   <?php if(isset($id_aData) && $sale_type_v['enum_key']==$id_aData['goods_sale_type_arr']['enum_key'] ){ ?>
     <option value="<?php echo $sale_type_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $sale_type_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $sale_type_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_check_pay">
								<?php echo $labels["goods_check_pay"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="goods[goods_check_pay]">
<?php foreach($goods_check_pay_enum as $check_pay_v){ ?>
   <?php if(isset($id_aData) && $check_pay_v['enum_key']==$id_aData['goods_check_pay_arr']['enum_key'] ){ ?>
     <option value="<?php echo $check_pay_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $check_pay_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $check_pay_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_utime">
								<?php echo $labels["goods_utime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="goods_utime">
	<input size="16" type="text" name="goods[goods_utime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['goods_utime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['goods_utime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#goods_utime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_desc">
								<?php echo $labels["goods_desc"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_desc]" value="<?php echo $id_aData['goods_desc'] ?>" id="goods_desc">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_desc]" id="goods_desc">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="goods_is_new">
								<?php echo $labels["goods_is_new"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="goods[goods_is_new]" value="<?php echo $id_aData['goods_is_new'] ?>" id="goods_is_new">
<?php }else{ ?>
<input type="text" maxlength="128" name="goods[goods_is_new]" id="goods_is_new">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
                 <?php }?>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>