<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						<?php if(isset($_GET['type_id'])){
							if(isset($type_arr[$_GET['type_id']]['type_name'])){
								echo $type_arr[$_GET['type_id']]['type_name'];
							}else{
								echo '客户';
							}
						}else{
							echo '客户';
						}
						?>
						<span class="mini-title">
						account
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<div class="btn-group">
<a id="newaccount" class="btn btn-primary" href="<?php echo site_url('www/account/add')?>" data-toggle="dropdown">
<i class="icon-file"></i>
新增客户
</a>
<ul class="dropdown-menu" role="menu">
	<?php foreach ($type_arr as $k=>$v){ ?>
	<li>
	<a href="<?php echo site_url('www/account/add?type_id='.$v['type_id']);?>">新增<?php echo $v['type_name'];?></a>
	</li>
	<?php }?>
</ul>
</div>
<a id="account_seniorquery" class="btn btn-primary" url="<?php echo site_url('www/account/seniorquery')?>">
<i class="icon-search"></i>
高级查询
</a>
<input id="select_json" type="hidden">
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">

<div class="grid-view">
	<div id="account_list"></div>
<div>
<script type="text/javascript">
$(document).ready(function () {
	$selectAttr=[
		{'value':'name','txt':'名称'},
		{'value':'leader','txt':'负责人'},
		{'value':'account_shopexid','txt':'商业id'},
		{'value':'telephone','txt':'电话'},
		{'value':'email','txt':'客户邮箱'},
		{'value':'department','txt':'所属部门'},
	];
	
	$("#account_list").leeDataTable({
		selectAttr:$selectAttr, //简单查询的查询属性
		url:"<?php echo site_url('www/account/ajax_select'); ?>", //ajax查询的地址
		perNumber:10 //每页显示多少条数据
	});


	//高级查询
	$seniorquery_attr = <?php echo json_encode($seniorquery_attr);?>;
	$("#account_seniorquery").seniorquery({
		selectAttr:$seniorquery_attr,
		list_id:'account_list',
		url:"<?php echo site_url('www/account/ajax_select'); ?>", //ajax查询的地址
		perNumber:10 //每页显示多少条数据(因为暂时没想通这里要怎么获取！所以直接赋值先)
	});
});

</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

