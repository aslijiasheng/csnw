<div style="padding:20px;">
	<ul>
		<li>在下面的行中设定属性的限制条件</li>
		<li>查询条件会在下面的文本区域中组合</li>
		<li>你可以改变条件的组合方式(如 1 or (2 and 3))</li>
	</ul>
	<br>
	<?php p(json_encode($seniorquery_attr));?>
	<form class="form-horizontal no-margin">
		<div id='conditions' style="padding:5px 0;">
			<div style="padding:5px 0;">
				<select id="aaa_1" class="span2">
					<option value="1"> --选择属性-- </option>
					<?php foreach ($seniorquery_attr as $v){?>
						<option value="<?php echo $v['name'];?>"> <?php echo $v['label'];?> </option>
					<?php }?>
				</select>
				<select id="DateOfBirthMonth"  class="span2 input-left-top-margins" >
					<option value="1"> 包含 </option>
					<option value="2"> 不包含 </option>
					<option value="3"> = </option>
					<option value="4"> 为空 </option>
					<option value="5"> 不为空 </option>
				</select>
				<input id="aaa" type="text" class="span4 input-left-top-margins" >
			</div>

			<div style="padding:5px 0;">
				<select id="aaa_1" class="span2">
					<option value="1"> 查询属性 </option>
					<?php foreach ($seniorquery_attr as $v){?>
						<option value="<?php echo $v['name'];?>"> <?php echo $v['label'];?> </option>
					<?php }?>
				</select>
				<select id="DateOfBirthMonth"  class="span2 input-left-top-margins" >
					<option value="1"> 包含 </option>
					<option value="2"> 不包含 </option>
					<option value="3"> = </option>
					<option value="4"> 为空 </option>
					<option value="5"> 不为空 </option>
				</select>
				<input id="aaa" type="text" class="span4 input-left-top-margins" >
			</div>
		</div>
		<div style="padding:5px 0;"><a class="add_seniorquery_line">添加行</a> <a class="del_seniorquery_line">删除行</a></div>
		<textarea id="description3" class="input-block-level span8" placeholder="Description" name="description3" rows="3"> </textarea>
	</form>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$seniorquery_attr = <?php echo json_encode($seniorquery_attr);?>;
		
		$line = 0;
		//首先要写个方法，专门用类添加行！
		function add_seniorquery_line($line){
			$("#conditions").append(''+
				'<div id="line_'+$line+'" style="padding:5px 0;">'+
					'<select id="select_'+$line+'" class="span2">'+
						'<option value="1"> --选择属性-- </option>'+
					'</select>'+
					'<select id="DateOfBirthMonth"  class="span2 input-left-top-margins">'+
						'<option value="1"> 包含 </option>'+
						'<option value="2"> 不包含 </option>'+
						'<option value="3"> = </option>'+
						'<option value="4"> 为空 </option>'+
						'<option value="5"> 不为空 </option>'+
					'</select>'+
					'<input id="aaa" type="text" class="span4 input-left-top-margins">'+
				'</div>'+
			'');
			//循环出所有属性
			$.each($seniorquery_attr,function(i,val){
				$("#select_"+$line).append('<option value="'+val.name+'"> '+val.label+' </option>');
			});
		};
		function del_seniorquery_line($line){
			$("#line_"+$line).remove();
		};
		$(".add_seniorquery_line").click(function(){
			$line++;
			add_seniorquery_line($line);
		});
		$(".del_seniorquery_line").click(function(){
			del_seniorquery_line($line);
			$line--;
		});
	});
</script>