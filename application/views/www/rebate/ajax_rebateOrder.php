<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="rebateOrder_form">
		<!-- 用一个隐藏文本框来存rebate_id -->
		<input size="16" type="hidden" name="order_number" value='<?php echo $data['order_number']; ?>'>
		<input size="16" type="hidden" name="order_id" value='<?php echo $data['order_id']; ?>'>
		<div class="control-group">
			<label class="control-label required" for="rebate_account_id">
				订单编号
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $data['order_number']; ?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<table class="table table-bordered">
						<tbody>
							</tr>
						<td>返点类型</td>
						<td><?php echo!empty($data['order_rebates_type']) ? $data['order_rebates_type']['enum_name'] : ''; ?></td>
						<td>返点金额</td>
						<td><?php echo!empty($data['order_amount']) ? $data['order_amount'] : ''; ?></td>
						</tr>
						</tr>
						<td>代理商名称</td>
						<td><?php echo!empty($data['order_agent']) ? $data['order_agent_arr']['partner_name'] : ''; ?></td>
						<td>代理商商业ID</td>
						<td><?php echo!empty($data['order_agent']) ? $data['order_agent_arr']['partner_shopex_id'] : ''; ?></td>
						</tr>
						</tr>
						<td>关联销售订单</td>
						<td><?php
							if (!empty($data['order_relation_order_id'])) {
								$money = 0;
								foreach ($data['order_relation_order_id_arr'] as $order) {
									echo $order['order_number'] . '<br/>';
									$money += $order['order_amount'];
								}
							}
							?></td>
						<td>订单金额合计</td>
						<td><?php echo!empty($money) ? $money : 0; ?></td>
						</tr>
						</tr>
						<td>经手人</td>
						<td><?php echo!empty($data['order_agent']) ? $data['order_agent_arr']['partner_name'] : ''; ?></td>
						<td>业务主管</td>
						<td><?php echo!empty($data['order_review']) ? $data['order_review_arr']['user_name'] : ''; ?></td>
						</tr>
						</tr>
						<td>返点订单状态</td>
						<td><?php echo!empty($data['order_rebates_state']) ? $data['order_rebates_state']['enum_name'] : ''; ?></td>
						<td>财务人员</td>
						<td><?php echo!empty($data['order_finance']) ? $data['order_finance_arr']['user_name'] : ''; ?></td>
						</tr>
						</tr>
						<td>支付方式</td>
						<td colspan="3"></td>
						</tr>
						</tr>
						<td>返点备注</td>
						<td colspan="3"><?php echo!empty($data['order_rebates_remark']) ? $data['order_rebates_remark'] : ''; ?></td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="rebate_account_id">
				关联日记账
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<input type="text" maxlength="128" required="required" title="关联日记账" name="books_id" id="books_id" value_id ="" value="" url="<?php echo site_url('www/books/ajax_list') ?>" title='关联日记账' leetype="quote" dialogwidth="1099" readonly>
					<script type="text/javascript">
						$(document).ready(function() {
							//这里的引用需要触发一下
							$('#books_id').leeQuote();
						});
					</script>
				</div>
			</div>
		</div>
	</form>
</div>
<!--lee 内容部分 end-->