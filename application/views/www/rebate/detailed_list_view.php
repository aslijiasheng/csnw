	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="75">操作</th>
				<th><?php echo $labels['rebate_amount'];?></th>
				<th><?php echo $labels['rebate_account_id'];?></th>
				<th><?php echo $labels['rebate_pay_method'];?></th>
				<th><?php echo $labels['rebate_status'];?></th>
				<th><?php echo $labels['rebate_person'];?></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($listData as $listData_value){?>
		<?php  $user_auth = $this->user->user_auth($this->session->userdata('user_id')); ?>
			<tr>
				<td>
					<div class="btn-toolbar">
						<div class="btn-group">
								<a class="btn btn-small btn-primary rebate_view" id="<?php  echo $listData_value['rebate_id'];?>" <?php if(!in_array('rebate_view', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>查看</a>
								<button class="btn btn-small dropdown-toggle btn-primary" data-toggle="dropdown" <?php if(!in_array('rebate_view', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
									<span class="caret"></span>
								</button>
							<ul class="dropdown-menu">
								<li <?php if(!in_array('examine_rebate', $user_auth['activity_auth_arr']) or $listData_value['rebate_status'] != 1001){ echo 'style="display:none;"';} ?>>
									<a id="<?php  echo $listData_value['rebate_id'];?>" class="examine_rebate">审批返点</a>
								</li>
								<li <?php if(!in_array('confirm_rebate', $user_auth['activity_auth_arr']) or $listData_value['rebate_status'] != 1002){ echo 'style="display:none;"';} ?>>
									<a id="<?php  echo $listData_value['rebate_id'];?>" class="confirm_rebate">确认返点</a>
								</li>
								<li <?php if(!in_array('rebate_edit', $user_auth['activity_auth_arr']) or !in_array($listData_value['rebate_status'],array(1003,1005))){ echo 'style="display:none;"';} ?>>
									<a id="<?php  echo $listData_value['rebate_id'];?>" class="edit_rebate">编辑</a>
								</li>
								<li <?php if(!in_array('rebate_del', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
									<a id="<?php  echo $listData_value['rebate_id'];?>" class="del_rebate">删除</a>
								</li>
							</ul>
						</div>
					</div>
				</td>






					<td>
					<?php  echo $listData_value['rebate_amount'];?>
					</td>

					<td>
							<?php
			if ($listData_value['rebate_account_id']!=0 and $listData_value['rebate_account_id']!=""){
				echo $listData_value['rebate_account_id_arr']['account_name'];
			}else{
				echo "&nbsp";
			}
		?>
					</td>

					<td>
					<?php echo $listData_value['rebate_pay_method_arr']['enum_name'];?>
					</td>

					<td>
					<?php echo $listData_value['rebate_status_arr']['enum_name'];?>
					</td>

					<td>
					<?php  echo $listData_value['rebate_person'];?>
					</td>
			</tr>
		<?php }?>
		</tbody>

	</table>

<script type="text/javascript">
$(document).ready(function() {
	$(".rebate_view").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'rebate_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/rebate/ajax_view'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'返点详细信息',
			width:800,
			height:450,
			buttons:''
		});
		$('#operation_dialog').dialog('open');
	});

	$(".examine_rebate").click(function(){
		//alert('审批返点');
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'rebate_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/rebate/ajax_examine_rebate'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'审批返点',
			width:800,
			height:450,
			buttons: [{
				text:"审批通过",
				Class:"btn btn-primary",
				click: function(){
					formdata = $("#examine_rebate_form").serializeArray();
					formdata[formdata.length]={"name":"rebate[rebate_status]","value":"1002"}; //添加1个参数
					//alert(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("审批通过");
								$('#rebate').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");

							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/rebate/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"驳回审批",
				Class:"btn btn-success",
				click: function(){
					formdata = $("#examine_rebate_form").serializeArray();
					formdata[formdata.length]={"name":"rebate[rebate_status]","value":"1003"}; //添加1个参数
					//alert(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("驳回审批");
								$('#rebate').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/rebate/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了审批返点");
					$(this).dialog("close");
				}
			}]

		});
		$('#operation_dialog').dialog('open');
	});

	//确认返点
	$(".confirm_rebate").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'rebate_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/rebate/ajax_confirm_rebate'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'已经执行返点',
			width:800,
			height:450,
			buttons: [{
				text:"已经执行返点",
				Class:"btn btn-primary",
				click: function(){
					formdata = $("#examine_rebate_form").serializeArray();
					formdata[formdata.length]={"name":"rebate[rebate_status]","value":"1004"}; //添加1个参数
					//alert(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("已经执行返点");
								$('#rebate').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");

							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/rebate/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"执行不成功",
				Class:"btn btn-success",
				click: function(){
					formdata = $("#examine_rebate_form").serializeArray();
					formdata[formdata.length]={"name":"rebate[rebate_status]","value":"1005"}; //添加1个参数
					//alert(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("执行不成功");
								$('#rebate').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/rebate/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了审批返点");
					$(this).dialog("close");
				}
			}]

		});
		$('#operation_dialog').dialog('open');
	});

	$(".edit_rebate").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'rebate_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/rebate/ajax_edit'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'修改返点',
			width:800,
			height:450,
			buttons: [{
				text:"保存",
				Class:"btn btn-primary",
				click: function(){
					formdata = $("#rebate-form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//本次返点金额
					$rebate_amount = $('[name="rebate[rebate_amount]"]').val();
					//可返点金额
					$kfd_amount = $('#kfd_amount').html();
					if(($kfd_amount-$rebate_amount)<0){
						alertify.alert("【本次返点金额】不得大于【可返点金额】");
						return false;
					}
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("修改返点成功");
								$('#rebate').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
								$('#operation_dialog').dialog("close");
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/rebate/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了修改入款项");
					$(this).dialog("close");
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

	$(".del_rebate").click(function(){
		id=$(this).attr('id');
		alertify.confirm("是否删除这个返点记录", function (e) {
			if (e) {
				$.ajax({
					'type':'post',
					'data':'rebate_id='+id+'&order_id='+<?php echo $_GET['order_id'] ?>,
					'success':function(data){
						if(data==1){
							alertify.success("删除成功");
							$('#rebate').ajaxHtml(); //再次加载1次页面
							$('#operation_log').ajaxHtml();
						}else{
							alert('失败'+data);
						}
					},
					'url':'<?php echo site_url('www/rebate/ajax_del_post'); ?>',
					'async':false
				});
			} else {
				alertify.error("你放弃了删除");
			}
		});
	});


});

</script>