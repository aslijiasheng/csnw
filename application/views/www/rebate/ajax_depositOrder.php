<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="depositOrder_form">
		<!-- 用一个隐藏文本框来存rebate_id -->
		<input size="16" type="hidden" name="order_number" value='<?php echo $data['order_number']; ?>'>
		<input size="16" type="hidden" name="order_id" value='<?php echo $data['order_id']; ?>'>
		<div class="control-group">
			<label class="control-label required" for="rebate_account_id">
				订单编号
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $data['order_number']; ?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<table class="table table-bordered">
						<tbody>
							</tr>
						<td>代理商名称</td>
						<td><?php echo !empty($data['order_agent']) ? $data['order_agent_arr']['partner_name'] : ''; ?></td>
						<td>客户名称</td>
						<td><?php echo !empty($data['order_account']) ? $data['order_account_arr']['account_name'] : ''; ?></td>
						</tr>
						</tr>
						<td>订单金额</td>
						<td><?php echo !empty($data['order_amount']) ? $data['order_amount'] : ''; ?></td>
						<td>创建时间</td>
						<td><?php echo !empty($data['order_create_time']) ? $data['order_create_time'] : ''; ?></td>
						</tr>
						</tr>
						<td>所属部门</td>
						<td><?php echo !empty($data['order_department']) ? $data['order_department_arr']['department_name'] : ''; ?></td>
						<td>所属销售</td>
						<td><?php echo !empty($data['order_finance']) ? $data['order_finance_arr']['user_name'] : ''; ?></td>
						</tr>
						</tr>
						<td>款项类型</td>
						<td><?php echo !empty($data['order_deposit_paytype']) ? $data['order_deposit_paytype_arr']['enum_name']: ''; ?></td>
						<td>订单来源</td>
						<td><?php echo !empty($data['order_source']) ? $data['order_source_arr']['enum_name'] : ''; ?></td>
						</tr>
						</tr>
						<td>财务</td>
						<td><?php echo !empty($data['order_finance']) ? $data['order_finance_arr']['user_name'] : ''; ?></td>
						<td>订单状态</td>
						<td><?php echo !empty($data['order_state']) ? $data['order_state'] : ''; ?></td>
						</tr>
						</tr>
						<td>订单备注</td>
						<td colspan="3"><?php echo !empty($data['order_deposit_remark']) ? $data['order_deposit_remark'] : ''; ?></td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="rebate_account_id">
				关联日记账
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<input type="text" maxlength="128" required="required" title="关联日记账" name="books_id" id="books_id" value_id ="" value="" url="<?php echo site_url('www/books/ajax_list') ?>" title='关联日记账' leetype="quote" dialogwidth="1099" readonly>
					<script type="text/javascript">
						$(document).ready(function() {
							//这里的引用需要触发一下
							$('#books_id').leeQuote();
						});
					</script>
				</div>
			</div>
		</div>
	</form>
</div>
<!--lee 内容部分 end-->