<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="rebate-form" method="post">
		<!-- 用一个隐藏文本框来存rebate_id -->
		<input size="16" type="hidden" name="rebate[rebate_id]" value='<?php echo $rebate_data['rebate_id'];?>'>
		<input size="16" type="hidden" name="rebate[rebate_order_id]" value='<?php echo $rebate_data['rebate_order_id'];?>'>
		<input type="hidden" name="order_finance" value="<?php echo $rebate_data['rebate_pay_info_arr']['reviewer'] ?>">
		<input type="hidden" name="order_finance_department" value="<?php echo $rebate_data['rebate_pay_info_arr']['department'] ?>">
		<div class="control-group">
			<label class="control-label required" for="rebate_create_time">
				订单信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td width="10%">订单总金额</td>
								<td width="10%"><?php echo $rebate_data['rebate_order_id_arr']['order_amount'];?></td>
								<td width="10%">返款总额</td>
								<td width="10%"><?php echo $rebate_data['rebate_order_id_arr']['order_rebate_amount'];?></td>
							<tr>
							</tr>
								<td>已确认返点</td>
								<td><?php echo $yfd_amount;?></td>
								<td>未确认返点</td>
								<td><?php echo $wfd_amount;?></td>
							</tr>
							</tr>
								<td>可返款金额</td>
								<td colspan="3" id="kfd_amount"><?php echo $kfd_amount;?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="invoice_reviewer">
				业务负责人

			</label>
			<div class="controls">
<input type="text" name="rebate[rebate_reviewer]" id="rebate_reviewer"  required="required" title="业务负责人" value_id ="<?php echo $rebate_data['rebate_reviewer_arr']['user_id'];?>" value="<?php echo $rebate_data['rebate_reviewer_arr']['user_name'];?>">
<script type="text/javascript">
$(document).ready(function () {
	$('#rebate_reviewer').leeQuote({
		url:'<?php echo site_url('www/user/ajax_list?tag_name=rebate_reviewer');?>',
		title:'选择主管人员',
		data:{
			'rel_role_id':10,
		}
	});
});
</script>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="rebate_account_id">
				选择返点的客户
				<span class="required"></span>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="rebate[rebate_account_id]" required="required" id="rebate_account_id" value_id ="<?php echo $rebate_data['rebate_account_id_arr']['account_id'];?>" value="<?php echo $rebate_data['rebate_account_id_arr']['account_name'];?>" url="<?php echo site_url('www/account/ajax_list') ?>">
			</div>
<script type="text/javascript">
$(document).ready(function () {
	//这里的引用需要触发一下
	$('#rebate_account_id').leeQuote();
});
</script>

		</div>
		<div class="control-group">
			<label class="control-label required" for="rebate_amount">
				本次返点金额
				<span class="required"></span>
			</label>
			<div class="controls">
				<input size="16" type="text" name="rebate[rebate_amount]" id="rebate_amount" required="required" value='<?php echo $rebate_data['rebate_amount'];?>'>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="rebate_pay_method">
				返点付款方式
				<span class="required"></span>
			</label>
			<div class="controls">
				<select id="rebate_pay_method" name="rebate[rebate_pay_method]">
					<?php foreach($rebate_pay_method_enum as $pay_method_v){ ?>
					<option value="<?php echo $pay_method_v['enum_key'];?>" <?php
						if($rebate_data['rebate_pay_method']==$pay_method_v['enum_key']){echo 'selected';}
					?>>
						<?php echo $pay_method_v['enum_name'];?>
					</option>
					<?php } ?>
				</select>
			</div>
		</div>

						<div class="control-group">
							<div class="controls">
<div id="remit_info" class="well payinfo" <?php
	if ($rebate_data['rebate_pay_method']!=1001){
		echo 'style="display:none;"';
	}
?>>
	<div class="form-horizontal">
		<div class="control-group">
			<label class="control-label" style="width: 120px;">账户类型：</label>
			<div class="controls" style="margin-left: 120px;">
				<label class="radio inline">
					<input type="radio" name="payinfo[bank_type]" value="1001" <?php
						if($rebate_data['rebate_pay_info_arr']['bank_type']==1001){echo 'checked';}
					?>>
					企业
				</label>
				<label class="radio inline">
					<input type="radio" name="payinfo[bank_type]" value="1002" <?php
						if($rebate_data['rebate_pay_info_arr']['bank_type']==1002){echo 'checked';}
					?>>
					个人
				</label>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;" for="remit_bank">银行账户：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="payinfo[remit_bank]" id="remit_bank" required="required" class="isrequired" value='<?php echo $rebate_data['rebate_pay_info_arr']['remit_bank']?>'>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;" for="remit_person">账户名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="payinfo[remit_person]" id="remit_person" required="required" class="isrequired" value='<?php echo $rebate_data['rebate_pay_info_arr']['remit_person']?>'>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">开户银行名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="payinfo[remit_account]" value='<?php echo $rebate_data['rebate_pay_info_arr']['remit_account']?>'>
			</div>
		</div>
	</div>
</div>
<div id="alipay_info" class="well payinfo" <?php
	if ($rebate_data['rebate_pay_method']!=1002){
		echo 'style="display:none;"';
	}
?>>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="alipay_order">交易号：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="payinfo[alipay_order]" id="alipay_order" required="required" class="isrequired" value='<?php echo $rebate_data['rebate_pay_info_arr']['alipay_order']?>'>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="alipay_account">支付宝账号：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="payinfo[alipay_account]" id="alipay_account" required="required" class="isrequired" value='<?php echo $rebate_data['rebate_pay_info_arr']['alipay_account']?>'>
		</div>
	</div>
</div>
<div id="cash_info" class="payinfo" <?php
	if ($rebate_data['rebate_pay_method']!=1003){
		echo 'style="display:none;"';
	}
?>> </div>

							</div>
						</div>
<script type="text/javascript">
$(document).ready(function () {
	var rebate_pay_method = '<?php echo $rebate_data['rebate_pay_method']; ?>';
		if(rebate_pay_method == 1001){
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');

		}else if(rebate_pay_method == 1002){
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}else {
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#remit_info").find('.isrequired').removeAttr('required');
		}
	$('#rebate_pay_method').change(function(){
		//获取选择的value用作判断
		enumkey = $(this).val();
		if(enumkey==1001){
			$(".payinfo").hide();
			$("#remit_info").show();
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#remit_info").find('.isrequired').attr('required','required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1002){
			$(".payinfo").hide();
			$("#alipay_info").show();
			$("#alipay_info").find('.isrequired').attr('required','required');
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1003){
			$(".payinfo").hide();
			$("#cash_info").show();
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').attr('required','required');
		}
	});
});
</script>
						<div class="control-group">
							<label class="control-label required" for="rebate_person">
								返点经手人姓名
							</label>
							<div class="controls">
								<input size="16" type="text" name="rebate[rebate_person]" value='<?php echo $rebate_data['rebate_person']?>'>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label required" for="rebate_note">
								返点备注
							</label>
							<div class="controls">
								<textarea name="rebate[rebate_note]" id="rebate_note" style="width:400px" rows="6"><?php echo $rebate_data['rebate_note']?></textarea>
							</div>
						</div>
	</form>
</div>
<!--lee 内容部分 end-->