<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal">
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td width="10%">订单总金额</td>
								<td width="10%"><?php echo $rebate_data['rebate_order_id_arr']['order_amount'];?></td>
								<td width="10%">返款总额</td>
								<td width="10%"><?php echo $rebate_data['rebate_order_id_arr']['order_rebate_amount'];?></td>
							<tr>
							</tr>
								<td>已确认返点</td>
								<td><?php echo $yfd_amount;?></td>
								<td>未确认返点</td>
								<td><?php echo $wfd_amount;?></td>
							</tr>
							</tr>
								<td>可返款金额</td>
								<td colspan="3" id="kfd_amount"><?php echo $kfd_amount;?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="rebate_account_id">
				选择返点的客户
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $rebate_data['rebate_account_id_arr']['account_name'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="rebate_amount">
				本次返点金额
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $rebate_data['rebate_amount'];?>
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label required" for="rebate_pay_method">
				返点付款方式
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $rebate_data['rebate_pay_method_arr']['enum_name'];?>
				</div>
			</div>
		</div>
						
						<div class="control-group">
							<div class="controls">
<div id="remit_info" class="well payinfo">
	<div class="form-horizontal">
		<div class="control-group">
			<label class="control-label" style="width: 120px;">账户类型：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php if($rebate_data['rebate_pay_info_arr']['bank_type']==1001){echo '企业';}?>
					<?php if($rebate_data['rebate_pay_info_arr']['bank_type']==1002){echo '个人';}?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">银行账户：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $rebate_data['rebate_pay_info_arr']['remit_bank']?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">账户名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $rebate_data['rebate_pay_info_arr']['remit_person']?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">开户银行名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $rebate_data['rebate_pay_info_arr']['remit_account']?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="alipay_info" class="well payinfo"  style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;">交易号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $rebate_data['rebate_pay_info_arr']['alipay_order']?>
			</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;">支付宝账号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $rebate_data['rebate_pay_info_arr']['alipay_account']?>
			</div>
		</div>
	</div>
</div>
<div id="cash_info" class="payinfo" style="display:none;"> </div>
								
							</div>
						</div>
<script type="text/javascript">
$(document).ready(function () {
	if(<?php echo $rebate_data['rebate_pay_method'];?>==1001){
		$(".payinfo").hide();
		$("#remit_info").show();
	}
	if(<?php echo $rebate_data['rebate_pay_method'];?>==1002){
		$(".payinfo").hide();
		$("#alipay_info").show();
	}
	if(<?php echo $rebate_data['rebate_pay_method'];?>==1003){
		$(".payinfo").hide();
		$("#cash_info").show();
	}
});
</script>

<script type="text/javascript">
$(document).ready(function () {
	$('#rebate_pay_method').change(function(){
		//获取选择的value用作判断
		enumkey = $(this).val();
		if(enumkey==1001){
			$(".payinfo").hide();
			$("#remit_info").show();
		}
		if(enumkey==1002){
			$(".payinfo").hide();
			$("#alipay_info").show();
		}
		if(enumkey==1003){
			$(".payinfo").hide();
			$("#cash_info").show();
		}
	});
});
</script>
		<div class="control-group">
			<label class="control-label required" for="rebate_person">
				返点经手人姓名
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $rebate_data['rebate_person'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="rebate_note">
				返点备注
			</label>
			<div class="controls">
				<div class="group-text">
					<pre><?php echo $rebate_data['rebate_note'];?></pre>
				</div>
			</div>
		</div>
	</form>
</div>
<!--lee 内容部分 end-->