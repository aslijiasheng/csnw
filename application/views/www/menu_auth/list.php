<div class="all-sidebar">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        菜单权限选择
                        <span class="mini-title">
                            Menu Manager
                        </span>
                    </div>

                    <span class="tools">
                    </span>
                </div>

                <div class="widget-body">
                    <div class="row-fluid">
                       <div class="leebutton">
                        <!--lee 按钮部分 start-->
                        <a href="<?php echo site_url('www/role') ?>" class="btn btn-primary" id="yw0">
                        <i class="icon-undo"></i>
                        返回角色列表
                        </a>
                        <!--lee 按钮部分 end-->
                        </div>
                        </div>
                        <div id="divmessagelist">
                            <div>
                                <div id="objlist" class="grid-view">
                                    <div id="dt_example" class="example_alt_pagination">
                                    <form action="<?php echo site_url('www/menu_auth/add?role_id='.$_GET['role_id']) ?>" method="post" id="ff" name="a_form">
                                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
                                            <thead>
                                                <tr>
                                                    
                                                    <th width="">菜单名称</th>
                                                    <td width="30%">权限选择</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($list as $v): ?>
                                                    <tr>
                                                        <td><?php
                                                            for ($i = 1; $i < $v['menu_order']; $i++):
                                                                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                            endfor;
															if ($v['menu_url']==""){
																echo $v['menu_name'];
															}else{
																echo "<a href='".site_url($v['menu_url'])."'>".$v['menu_name']."</a>";
															}
                                                            ?></td>
                                                       <td>
                                                        
                                                       <?php if(!empty($menu_auth_check)){if(in_array($v['menu_label'], $menu_auth_check)){ ?>
                                                          <input type="checkbox" name="<?php echo $v['menu_label'] ?>" id="" value="1" checked="checked">
                                                            <?php }else{ ?>
															<input type="checkbox" name="<?php echo $v['menu_label'] ?>" id="" value="1">
															<?php }}else{ ?>
															<input type="checkbox" name="<?php echo $v['menu_label'] ?>" id="" value="1">
														<?php } ?> 
                                                        
                                                        </td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>  
                                            <div class="leebutton">
                                            <button class="btn btn-primary" type="submit">保存</button>
                                            </div>                               
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>style/admin/js/jquery.dataTables.js">
</script>
<script type="text/javascript">
    //Data Tables
    $(document).ready(function() {
        // $("#submit").click(function(){
        //     //alert(11);
        //     $("#ff").submit();
        // })
    });
</script>