<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						基础产品新建页面
						<span class="mini-title">
						product_basic
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/product_basic/')?>">
<i class="icon-undo"></i>
返回基础产品列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php if(!isset($_GET['type_id'])){echo site_url('www/product_basic/add');}else{echo site_url('www/product_basic/add?type_id='.$_GET['type_id']);}?>" method="post">
		 	<?php if(0==1){?>
		 	<?php if(!isset($_GET['type_id'])){ ?>
		 	  
						<div class="control-group">
							<label class="control-label required" for="product_basic_code">
								<?php echo $labels["product_basic_code"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_code]" value="<?php echo $id_aData['product_basic_code'] ?>" id="product_basic_code">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_code]" id="product_basic_code">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_name">
								<?php echo $labels["product_basic_name"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_name]" value="<?php echo $id_aData['product_basic_name'] ?>" id="product_basic_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_name]" id="product_basic_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_category">
								<?php echo $labels["product_basic_category"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_category]">
<?php foreach($product_basic_category_enum as $category_v){ ?>
   <?php if(isset($id_aData) && $category_v['enum_key']==$id_aData['product_basic_category_arr']['enum_key'] ){ ?>
     <option value="<?php echo $category_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $category_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $category_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_sub_category">
								<?php echo $labels["product_basic_sub_category"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_sub_category]">
<?php foreach($product_basic_sub_category_enum as $sub_category_v){ ?>
   <?php if(isset($id_aData) && $sub_category_v['enum_key']==$id_aData['product_basic_sub_category_arr']['enum_key'] ){ ?>
     <option value="<?php echo $sub_category_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $sub_category_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $sub_category_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_cost">
								<?php echo $labels["product_basic_cost"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_cost]" value="<?php echo $id_aData['product_basic_cost'] ?>" id="product_basic_cost">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_cost]" id="product_basic_cost">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_price">
								<?php echo $labels["product_basic_price"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_price]" value="<?php echo $id_aData['product_basic_price'] ?>" id="product_basic_price">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_price]" id="product_basic_price">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_sales_type">
								<?php echo $labels["product_basic_sales_type"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_sales_type]">
<?php foreach($product_basic_sales_type_enum as $sales_type_v){ ?>
   <?php if(isset($id_aData) && $sales_type_v['enum_key']==$id_aData['product_basic_sales_type_arr']['enum_key'] ){ ?>
     <option value="<?php echo $sales_type_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $sales_type_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $sales_type_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_charging_type">
								<?php echo $labels["product_basic_charging_type"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_charging_type]">
<?php foreach($product_basic_charging_type_enum as $charging_type_v){ ?>
   <?php if(isset($id_aData) && $charging_type_v['enum_key']==$id_aData['product_basic_charging_type_arr']['enum_key'] ){ ?>
     <option value="<?php echo $charging_type_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $charging_type_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $charging_type_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_calc_unit">
								<?php echo $labels["product_basic_calc_unit"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_calc_unit]">
<?php foreach($product_basic_calc_unit_enum as $calc_unit_v){ ?>
   <?php if(isset($id_aData) && $calc_unit_v['enum_key']==$id_aData['product_basic_calc_unit_arr']['enum_key'] ){ ?>
     <option value="<?php echo $calc_unit_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $calc_unit_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $calc_unit_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_service_unit">
								<?php echo $labels["product_basic_service_unit"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_service_unit]">
<?php foreach($product_basic_service_unit_enum as $service_unit_v){ ?>
   <?php if(isset($id_aData) && $service_unit_v['enum_key']==$id_aData['product_basic_service_unit_arr']['enum_key'] ){ ?>
     <option value="<?php echo $service_unit_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $service_unit_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $service_unit_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_can_renewal">
								<?php echo $labels["product_basic_can_renewal"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($product_basic_can_renewal_enum as $can_renewal_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $can_renewal_v['enum_key']==$id_aData['product_basic_can_renewal_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $can_renewal_v['enum_key']?>" name="product_basic[product_basic_can_renewal]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $can_renewal_v['enum_key']?>" name="product_basic[product_basic_can_renewal]">
<?php }?>
<?php echo $can_renewal_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_need_host">
								<?php echo $labels["product_basic_need_host"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($product_basic_need_host_enum as $need_host_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $need_host_v['enum_key']==$id_aData['product_basic_need_host_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $need_host_v['enum_key']?>" name="product_basic[product_basic_need_host]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $need_host_v['enum_key']?>" name="product_basic[product_basic_need_host]">
<?php }?>
<?php echo $need_host_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_need_open">
								<?php echo $labels["product_basic_need_open"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($product_basic_need_open_enum as $need_open_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $need_open_v['enum_key']==$id_aData['product_basic_need_open_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $need_open_v['enum_key']?>" name="product_basic[product_basic_need_open]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $need_open_v['enum_key']?>" name="product_basic[product_basic_need_open]">
<?php }?>
<?php echo $need_open_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_has_expired">
								<?php echo $labels["product_basic_has_expired"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($product_basic_has_expired_enum as $has_expired_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $has_expired_v['enum_key']==$id_aData['product_basic_has_expired_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $has_expired_v['enum_key']?>" name="product_basic[product_basic_has_expired]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $has_expired_v['enum_key']?>" name="product_basic[product_basic_has_expired]">
<?php }?>
<?php echo $has_expired_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_utime">
								<?php echo $labels["product_basic_utime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="product_basic_utime">
	<input size="16" type="text" name="product_basic[product_basic_utime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['product_basic_utime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['product_basic_utime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#product_basic_utime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_domain_cost">
								<?php echo $labels["product_basic_domain_cost"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_domain_cost]" value="<?php echo $id_aData['product_basic_domain_cost'] ?>" id="product_basic_domain_cost">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_domain_cost]" id="product_basic_domain_cost">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_sms_cost">
								<?php echo $labels["product_basic_sms_cost"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_sms_cost]" value="<?php echo $id_aData['product_basic_sms_cost'] ?>" id="product_basic_sms_cost">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_sms_cost]" id="product_basic_sms_cost">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_confirm_method">
								<?php echo $labels["product_basic_confirm_method"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_confirm_method]" value="<?php echo $id_aData['product_basic_confirm_method'] ?>" id="product_basic_confirm_method">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_confirm_method]" id="product_basic_confirm_method">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_idc_cost">
								<?php echo $labels["product_basic_idc_cost"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_idc_cost]" value="<?php echo $id_aData['product_basic_idc_cost'] ?>" id="product_basic_idc_cost">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_idc_cost]" id="product_basic_idc_cost">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_atime">
								<?php echo $labels["product_basic_atime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="product_basic_atime">
	<input size="16" type="text" name="product_basic[product_basic_atime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['product_basic_atime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['product_basic_atime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#product_basic_atime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
		 	<?php }else{ ?>
		    <?php switch ($_GET["type_id"]){ 
							case -1:
							break;
						?>
						
						<?php default: ?>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_code">
								<?php echo $labels["product_basic_code"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_code]" value="<?php echo $id_aData['product_basic_code'] ?>" id="product_basic_code">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_code]" id="product_basic_code">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_name">
								<?php echo $labels["product_basic_name"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_name]" value="<?php echo $id_aData['product_basic_name'] ?>" id="product_basic_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_name]" id="product_basic_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_category">
								<?php echo $labels["product_basic_category"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_category]">
<?php foreach($product_basic_category_enum as $category_v){ ?>
   <?php if(isset($id_aData) && $category_v['enum_key']==$id_aData['product_basic_category_arr']['enum_key'] ){ ?>
     <option value="<?php echo $category_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $category_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $category_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_sub_category">
								<?php echo $labels["product_basic_sub_category"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_sub_category]">
<?php foreach($product_basic_sub_category_enum as $sub_category_v){ ?>
   <?php if(isset($id_aData) && $sub_category_v['enum_key']==$id_aData['product_basic_sub_category_arr']['enum_key'] ){ ?>
     <option value="<?php echo $sub_category_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $sub_category_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $sub_category_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_cost">
								<?php echo $labels["product_basic_cost"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_cost]" value="<?php echo $id_aData['product_basic_cost'] ?>" id="product_basic_cost">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_cost]" id="product_basic_cost">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_price">
								<?php echo $labels["product_basic_price"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_price]" value="<?php echo $id_aData['product_basic_price'] ?>" id="product_basic_price">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_price]" id="product_basic_price">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_sales_type">
								<?php echo $labels["product_basic_sales_type"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_sales_type]">
<?php foreach($product_basic_sales_type_enum as $sales_type_v){ ?>
   <?php if(isset($id_aData) && $sales_type_v['enum_key']==$id_aData['product_basic_sales_type_arr']['enum_key'] ){ ?>
     <option value="<?php echo $sales_type_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $sales_type_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $sales_type_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_charging_type">
								<?php echo $labels["product_basic_charging_type"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_charging_type]">
<?php foreach($product_basic_charging_type_enum as $charging_type_v){ ?>
   <?php if(isset($id_aData) && $charging_type_v['enum_key']==$id_aData['product_basic_charging_type_arr']['enum_key'] ){ ?>
     <option value="<?php echo $charging_type_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $charging_type_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $charging_type_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_calc_unit">
								<?php echo $labels["product_basic_calc_unit"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_calc_unit]">
<?php foreach($product_basic_calc_unit_enum as $calc_unit_v){ ?>
   <?php if(isset($id_aData) && $calc_unit_v['enum_key']==$id_aData['product_basic_calc_unit_arr']['enum_key'] ){ ?>
     <option value="<?php echo $calc_unit_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $calc_unit_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $calc_unit_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_service_unit">
								<?php echo $labels["product_basic_service_unit"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_service_unit]">
<?php foreach($product_basic_service_unit_enum as $service_unit_v){ ?>
   <?php if(isset($id_aData) && $service_unit_v['enum_key']==$id_aData['product_basic_service_unit_arr']['enum_key'] ){ ?>
     <option value="<?php echo $service_unit_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $service_unit_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $service_unit_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_can_renewal">
								<?php echo $labels["product_basic_can_renewal"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($product_basic_can_renewal_enum as $can_renewal_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $can_renewal_v['enum_key']==$id_aData['product_basic_can_renewal_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $can_renewal_v['enum_key']?>" name="product_basic[product_basic_can_renewal]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $can_renewal_v['enum_key']?>" name="product_basic[product_basic_can_renewal]">
<?php }?>
<?php echo $can_renewal_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_need_host">
								<?php echo $labels["product_basic_need_host"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($product_basic_need_host_enum as $need_host_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $need_host_v['enum_key']==$id_aData['product_basic_need_host_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $need_host_v['enum_key']?>" name="product_basic[product_basic_need_host]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $need_host_v['enum_key']?>" name="product_basic[product_basic_need_host]">
<?php }?>
<?php echo $need_host_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_need_open">
								<?php echo $labels["product_basic_need_open"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($product_basic_need_open_enum as $need_open_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $need_open_v['enum_key']==$id_aData['product_basic_need_open_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $need_open_v['enum_key']?>" name="product_basic[product_basic_need_open]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $need_open_v['enum_key']?>" name="product_basic[product_basic_need_open]">
<?php }?>
<?php echo $need_open_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_has_expired">
								<?php echo $labels["product_basic_has_expired"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($product_basic_has_expired_enum as $has_expired_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $has_expired_v['enum_key']==$id_aData['product_basic_has_expired_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $has_expired_v['enum_key']?>" name="product_basic[product_basic_has_expired]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $has_expired_v['enum_key']?>" name="product_basic[product_basic_has_expired]">
<?php }?>
<?php echo $has_expired_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_utime">
								<?php echo $labels["product_basic_utime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="product_basic_utime">
	<input size="16" type="text" name="product_basic[product_basic_utime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['product_basic_utime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['product_basic_utime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#product_basic_utime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_domain_cost">
								<?php echo $labels["product_basic_domain_cost"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_domain_cost]" value="<?php echo $id_aData['product_basic_domain_cost'] ?>" id="product_basic_domain_cost">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_domain_cost]" id="product_basic_domain_cost">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_sms_cost">
								<?php echo $labels["product_basic_sms_cost"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_sms_cost]" value="<?php echo $id_aData['product_basic_sms_cost'] ?>" id="product_basic_sms_cost">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_sms_cost]" id="product_basic_sms_cost">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_confirm_method">
								<?php echo $labels["product_basic_confirm_method"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_confirm_method]" value="<?php echo $id_aData['product_basic_confirm_method'] ?>" id="product_basic_confirm_method">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_confirm_method]" id="product_basic_confirm_method">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_idc_cost">
								<?php echo $labels["product_basic_idc_cost"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_idc_cost]" value="<?php echo $id_aData['product_basic_idc_cost'] ?>" id="product_basic_idc_cost">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_idc_cost]" id="product_basic_idc_cost">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_atime">
								<?php echo $labels["product_basic_atime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="product_basic_atime">
	<input size="16" type="text" name="product_basic[product_basic_atime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['product_basic_atime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['product_basic_atime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#product_basic_atime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
					<?php }?>
                <?php }}else{ ?>
                  
						<div class="control-group">
							<label class="control-label required" for="product_basic_code">
								<?php echo $labels["product_basic_code"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_code]" value="<?php echo $id_aData['product_basic_code'] ?>" id="product_basic_code">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_code]" id="product_basic_code">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_name">
								<?php echo $labels["product_basic_name"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_name]" value="<?php echo $id_aData['product_basic_name'] ?>" id="product_basic_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_name]" id="product_basic_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_category">
								<?php echo $labels["product_basic_category"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_category]">
<?php foreach($product_basic_category_enum as $category_v){ ?>
   <?php if(isset($id_aData) && $category_v['enum_key']==$id_aData['product_basic_category_arr']['enum_key'] ){ ?>
     <option value="<?php echo $category_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $category_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $category_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_sub_category">
								<?php echo $labels["product_basic_sub_category"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_sub_category]">
<?php foreach($product_basic_sub_category_enum as $sub_category_v){ ?>
   <?php if(isset($id_aData) && $sub_category_v['enum_key']==$id_aData['product_basic_sub_category_arr']['enum_key'] ){ ?>
     <option value="<?php echo $sub_category_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $sub_category_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $sub_category_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_cost">
								<?php echo $labels["product_basic_cost"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_cost]" value="<?php echo $id_aData['product_basic_cost'] ?>" id="product_basic_cost">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_cost]" id="product_basic_cost">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_price">
								<?php echo $labels["product_basic_price"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_price]" value="<?php echo $id_aData['product_basic_price'] ?>" id="product_basic_price">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_price]" id="product_basic_price">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_sales_type">
								<?php echo $labels["product_basic_sales_type"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_sales_type]">
<?php foreach($product_basic_sales_type_enum as $sales_type_v){ ?>
   <?php if(isset($id_aData) && $sales_type_v['enum_key']==$id_aData['product_basic_sales_type_arr']['enum_key'] ){ ?>
     <option value="<?php echo $sales_type_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $sales_type_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $sales_type_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_charging_type">
								<?php echo $labels["product_basic_charging_type"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_charging_type]">
<?php foreach($product_basic_charging_type_enum as $charging_type_v){ ?>
   <?php if(isset($id_aData) && $charging_type_v['enum_key']==$id_aData['product_basic_charging_type_arr']['enum_key'] ){ ?>
     <option value="<?php echo $charging_type_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $charging_type_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $charging_type_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_calc_unit">
								<?php echo $labels["product_basic_calc_unit"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_calc_unit]">
<?php foreach($product_basic_calc_unit_enum as $calc_unit_v){ ?>
   <?php if(isset($id_aData) && $calc_unit_v['enum_key']==$id_aData['product_basic_calc_unit_arr']['enum_key'] ){ ?>
     <option value="<?php echo $calc_unit_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $calc_unit_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $calc_unit_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_service_unit">
								<?php echo $labels["product_basic_service_unit"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="product_basic[product_basic_service_unit]">
<?php foreach($product_basic_service_unit_enum as $service_unit_v){ ?>
   <?php if(isset($id_aData) && $service_unit_v['enum_key']==$id_aData['product_basic_service_unit_arr']['enum_key'] ){ ?>
     <option value="<?php echo $service_unit_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $service_unit_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $service_unit_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_can_renewal">
								<?php echo $labels["product_basic_can_renewal"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($product_basic_can_renewal_enum as $can_renewal_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $can_renewal_v['enum_key']==$id_aData['product_basic_can_renewal_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $can_renewal_v['enum_key']?>" name="product_basic[product_basic_can_renewal]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $can_renewal_v['enum_key']?>" name="product_basic[product_basic_can_renewal]">
<?php }?>
<?php echo $can_renewal_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_need_host">
								<?php echo $labels["product_basic_need_host"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($product_basic_need_host_enum as $need_host_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $need_host_v['enum_key']==$id_aData['product_basic_need_host_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $need_host_v['enum_key']?>" name="product_basic[product_basic_need_host]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $need_host_v['enum_key']?>" name="product_basic[product_basic_need_host]">
<?php }?>
<?php echo $need_host_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_need_open">
								<?php echo $labels["product_basic_need_open"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($product_basic_need_open_enum as $need_open_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $need_open_v['enum_key']==$id_aData['product_basic_need_open_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $need_open_v['enum_key']?>" name="product_basic[product_basic_need_open]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $need_open_v['enum_key']?>" name="product_basic[product_basic_need_open]">
<?php }?>
<?php echo $need_open_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_has_expired">
								<?php echo $labels["product_basic_has_expired"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($product_basic_has_expired_enum as $has_expired_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $has_expired_v['enum_key']==$id_aData['product_basic_has_expired_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $has_expired_v['enum_key']?>" name="product_basic[product_basic_has_expired]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $has_expired_v['enum_key']?>" name="product_basic[product_basic_has_expired]">
<?php }?>
<?php echo $has_expired_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_utime">
								<?php echo $labels["product_basic_utime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="product_basic_utime">
	<input size="16" type="text" name="product_basic[product_basic_utime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['product_basic_utime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['product_basic_utime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#product_basic_utime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_domain_cost">
								<?php echo $labels["product_basic_domain_cost"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_domain_cost]" value="<?php echo $id_aData['product_basic_domain_cost'] ?>" id="product_basic_domain_cost">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_domain_cost]" id="product_basic_domain_cost">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_sms_cost">
								<?php echo $labels["product_basic_sms_cost"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_sms_cost]" value="<?php echo $id_aData['product_basic_sms_cost'] ?>" id="product_basic_sms_cost">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_sms_cost]" id="product_basic_sms_cost">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_confirm_method">
								<?php echo $labels["product_basic_confirm_method"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_confirm_method]" value="<?php echo $id_aData['product_basic_confirm_method'] ?>" id="product_basic_confirm_method">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_confirm_method]" id="product_basic_confirm_method">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_idc_cost">
								<?php echo $labels["product_basic_idc_cost"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_idc_cost]" value="<?php echo $id_aData['product_basic_idc_cost'] ?>" id="product_basic_idc_cost">
<?php }else{ ?>
<input type="text" maxlength="128" name="product_basic[product_basic_idc_cost]" id="product_basic_idc_cost">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="product_basic_atime">
								<?php echo $labels["product_basic_atime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="product_basic_atime">
	<input size="16" type="text" name="product_basic[product_basic_atime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['product_basic_atime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['product_basic_atime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#product_basic_atime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
                 <?php }?>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>