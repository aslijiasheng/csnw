<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="100"><?php echo $labels['product_basic_code']; ?></th>
            <th width="350"><?php echo $labels['product_basic_name']; ?></th>
            <th width="100"><?php echo $labels['product_basic_category']; ?></th>
            <th width="100"><?php echo $labels['product_basic_sub_category']; ?></th>
            <th width="100"><?php echo $labels['product_basic_sales_type']; ?></th>
            <th width="100"><?php echo $labels['product_basic_domain_cost']; ?></th>
            <th width="100"><?php echo $labels['product_basic_sms_cost']; ?></th>
            <th width="100"><?php echo $labels['product_basic_idc_cost']; ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listData as $listData_value) { ?>
            <tr>
                <td>
                    <?php echo $listData_value['product_basic_code']; ?>
                </td>
                <td>
                    <?php echo $listData_value['product_basic_name']; ?>
                </td>
                <td>
                    <?php echo $listData_value['product_basic_category_arr']['enum_name']; ?>
                </td>
                <td>
                    <?php echo $listData_value['product_basic_sub_category_arr']['enum_name']; ?>
                </td>
                <td>
                    <?php echo $listData_value['product_basic_sales_type_arr']['enum_name']; ?>
                </td>
                <td>
                    <?php echo $listData_value['product_basic_domain_cost']; ?>
                </td>
                <td>
                    <?php echo $listData_value['product_basic_sms_cost']; ?>
                </td>
                <td>
                    <?php echo $listData_value['product_basic_idc_cost']; ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber; ?>条</div>
<input type='hidden' id='totalNumber' value="<?php echo $totalNumber; ?>">
</div>