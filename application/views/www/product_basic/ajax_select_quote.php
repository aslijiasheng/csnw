<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="100">操作</th>
            <th><?php echo $labels['product_basic_code']; ?></th>
            <th><?php echo $labels['product_basic_name']; ?></th>
            <th><?php echo $labels['product_basic_category']; ?></th>
            <th><?php echo $labels['product_basic_sub_category']; ?></th>
            <th><?php echo $labels['product_basic_sales_type']; ?></th>
            <th><?php echo $labels['product_basic_charging_type']; ?></th>
            <th><?php echo $labels['product_basic_calc_unit']; ?></th>
            <th><?php echo $labels['product_basic_service_unit']; ?></th>
            <th><?php echo $labels['product_basic_can_renewal']; ?></th>
            <th><?php echo $labels['product_basic_need_host']; ?></th>
            <th><?php echo $labels['product_basic_need_open']; ?></th>
            <th><?php echo $labels['product_basic_has_expired']; ?></th>
            <th><?php echo $labels['product_basic_domain_cost']; ?></th>
            <th><?php echo $labels['product_basic_sms_cost']; ?></th>
            <th><?php echo $labels['product_basic_idc_cost']; ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listData as $listData_value) { ?>
            <tr>
                <td style="font-size:16px;">
                    <input type="radio" name="optionsRadios" value="<?php echo $listData_value['product_basic_id']; ?>" name_val="<?php echo $listData_value['product_basic_name']; ?>"<?php if ($_GET["hidden_v"] != '') {
            $hidden_id = explode('?', $_GET["hidden_v"]);
            $hidden_id = $hidden_id[0];
            if ($hidden_id == $listData_value['product_basic_id']) {
                echo "checked='checked'";
            }
        } ?> >
                </td>
                <td>
    <?php echo $listData_value['product_basic_code']; ?>
                </td>
                <td>
    <?php echo $listData_value['product_basic_name']; ?>
                </td>
                <td>
    <?php echo $listData_value['product_basic_category_arr']['enum_name']; ?>
                </td>
                <td>
    <?php echo $listData_value['product_basic_sub_category_arr']['enum_name']; ?>
                </td>
                <td>
    <?php echo $listData_value['product_basic_sales_type_arr']['enum_name']; ?>
                </td>

                <td>
    <?php echo $listData_value['product_basic_charging_type_arr']['enum_name']; ?>
                </td>

                <td>
    <?php echo $listData_value['product_basic_calc_unit_arr']['enum_name']; ?>
                </td>

                <td>
    <?php echo $listData_value['product_basic_service_unit_arr']['enum_name']; ?>
                </td>

                <td>
    <?php echo $listData_value['product_basic_can_renewal_arr']['enum_name']; ?>
                </td>

                <td>
    <?php echo $listData_value['product_basic_need_host_arr']['enum_name']; ?>
                </td>

                <td>
    <?php echo $listData_value['product_basic_need_open_arr']['enum_name']; ?>
                </td>

                <td>
    <?php echo $listData_value['product_basic_has_expired_arr']['enum_name']; ?>
                </td>
                <td>
    <?php echo $listData_value['product_basic_domain_cost']; ?>
                </td>
                <td>
    <?php echo $listData_value['product_basic_sms_cost']; ?>
                </td>
                <td>
    <?php echo $listData_value['product_basic_idc_cost']; ?>
                </td>
            </tr>
<?php } ?>
    </tbody>

</table>
<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber; ?>条</div>
<input type='hidden' id='totalNumber' value="<?php echo $totalNumber; ?>">
</div>

