<div class="all-sidebar">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <?php
                        if (isset($_GET['type_id'])) {
                            if (isset($type_arr[$_GET['type_id']]['type_name'])) {
                                echo $type_arr[$_GET['type_id']]['type_name'];
                            } else {
                                echo '基础产品';
                            }
                        } else {
                            echo '基础产品';
                        }
                        ?>
                        <span class="mini-title">
                            product_basic
                        </span>
                    </div>
                    <span class="tools">
                        <!-- 窗口按钮部门
                        <a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
                        -->
                    </span>
                </div>
                <div class="widget-body">
                    <div class="row-fluid">
                        <div class="leebutton">

                        </div>
                        <div id="divmessagelist">

                            <div class="grid-view">
                                <div id="product_basic_list"></div>
                                <div>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            $selectAttr = [
                                                {'value': 'code', 'txt': '产品编号'},
                                                {'value': 'name', 'txt': '产品名称'},
                                                {'value': 'category', 'txt': '一级分类'},
                                                {'value': 'sub_category', 'txt': '二级分类'},
                                                {'value': 'cost', 'txt': '成本价'},
                                                {'value': 'price', 'txt': '市场价'},
                                                {'value': 'sales_type', 'txt': '销售模式'},
                                                {'value': 'charging_type', 'txt': '收费方式'},
                                                {'value': 'calc_unit', 'txt': '计量单位'},
                                                {'value': 'service_unit', 'txt': '服务周期单位'},
                                                {'value': 'can_renewal', 'txt': '是否可续费'},
                                                {'value': 'need_host', 'txt': '是否需要主机'},
                                                {'value': 'need_open', 'txt': '是否需要开通'},
                                                {'value': 'has_expired', 'txt': '是否有有效期'},
                                                {'value': 'utime', 'txt': '变更信息时间'},
                                                {'value': 'domain_cost', 'txt': '域名／备案成本'},
                                                {'value': 'sms_cost', 'txt': '短信成本'},
                                                {'value': 'confirm_method', 'txt': '确认类型'},
                                                {'value': 'idc_cost', 'txt': 'IDC成本'},
                                                {'value': 'atime', 'txt': '创建时间'},
                                            ];

                                            $("#product_basic_list").leeDataTable({
                                                selectAttr: $selectAttr, //简单查询的查询属性
                                                url: "<?php echo site_url('www/product_basic/ajax_select_quote?hidden_v=' . $_GET['hidden_v']); ?>", //ajax查询的地址
                                                perNumber: 20, //每页显示多少条数据

                                            });
                                        });

                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

