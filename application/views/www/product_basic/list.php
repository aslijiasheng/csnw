<div class="all-sidebar">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        基础产品
                        <span class="mini-title">
                            product_basic
                        </span>
                    </div>
                    <span class="tools">
                    </span>
                </div>
                <div class="widget-body">
                    <div class="row-fluid">
                        <div id="divmessagelist">
                            <div class="grid-view">
                                <div id="product_basic_list"></div>
                                <div>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            $selectAttr = [
                                                {'value': 'code', 'txt': '产品编号'},
                                                {'value': 'name', 'txt': '产品名称'},
                                            ];
                                            $("#product_basic_list").leeDataTable({
                                                selectAttr: $selectAttr, //简单查询的查询属性
                                                url: "<?php echo site_url('www/product_basic/ajax_select'); ?>", //ajax查询的地址
                                                perNumber: 20 //每页显示多少条数据
                                            });
                                        });

                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

