<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						人员新建页面
						<span class="mini-title">
						user
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/user/')?>">
<i class="icon-undo"></i>
返回人员列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php echo site_url('www/user/do_add') ?>" method="post" enctype="multipart/form-data">
		 	<div class="control-group">
		 		<label for="user_name" class="control-label">
		 			用户名
		 		</label>
		 		<div class="controls">
		 			<input type="text"  id="user_name" readonly="readonly" value="<?php echo $user_name; ?>">
		 			<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
		 		</div>
		 	</div>
	 		<div class="control-group">
		 		<label for="user_name" class="control-label">
		 			用户头像
		 		</label>
		 		<div class="controls">
		 			<input type="file"  name="thumb" id="pic_path" readonly="readonly">
		 		</div>
		 	</div>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>