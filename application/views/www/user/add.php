<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						人员新建页面
						<span class="mini-title">
						user
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/user/')?>">
<i class="icon-undo"></i>
返回人员列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php if(!isset($_GET['type_id'])){echo site_url('www/user/add');}else{echo site_url('www/user/add?type_id='.$_GET['type_id']);}?>" method="post">
		 	<?php if(0==1){?>
		 	<?php if(!isset($_GET['type_id'])){ ?>
		 	  
						<div class="control-group">
							<label class="control-label required" for="user_name">
								<?php echo $labels["user_name"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="user[user_name]" value="<?php echo $id_aData['user_name'] ?>" id="user_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_name]" id="user_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_sex">
								<?php echo $labels["user_sex"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php foreach($user_sex_enum as $sex_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $sex_v['enum_key']==$id_aData['user_sex_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $sex_v['enum_key']?>" name="user[user_sex]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $sex_v['enum_key']?>" name="user[user_sex]">
<?php }?>
<?php echo $sex_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_password">
								<?php echo $labels["user_password"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="password" maxlength="128" name="user[user_password]" value="<?php echo $id_aData['user_password'] ?>" id="user_password">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_password]" id="user_password">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_department">
								<?php echo $labels["user_department"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="user[user_department]" id="user_department" value_id ="<?php if(isset($id_aData['user_department_arr']['department_id'])){echo $id_aData['user_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['user_department_arr']['department_name'])){echo $id_aData['user_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_email">
								<?php echo $labels["user_email"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="user[user_email]" value="<?php echo $id_aData['user_email'] ?>" id="user_email">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_email]" id="user_email">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_gonghao">
								<?php echo $labels["user_gonghao"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="user[user_gonghao]" value="<?php echo $id_aData['user_gonghao'] ?>" id="user_gonghao">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_gonghao]" id="user_gonghao">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
					
						<div class="control-group">
							<label class="control-label required" for="user_status">
								<?php echo $labels["user_status"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php foreach($user_status_enum as $status_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $status_v['enum_key']==$id_aData['user_status_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $status_v['enum_key']?>" name="user[user_status]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $status_v['enum_key']?>" name="user[user_status]">
<?php }?>
<?php echo $status_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_login_name">
								<?php echo $labels["user_login_name"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="user[user_login_name]" value="<?php echo $id_aData['user_login_name'] ?>" id="user_login_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_login_name]" id="user_login_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
		 	<?php }else{ ?>
		    <?php switch ($_GET["type_id"]){ 
							case -1:
							break;
						?>
						
						<?php default: ?>
						
						<div class="control-group">
							<label class="control-label required" for="user_name">
								<?php echo $labels["user_name"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="user[user_name]" value="<?php echo $id_aData['user_name'] ?>" id="user_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_name]" id="user_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_sex">
								<?php echo $labels["user_sex"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php foreach($user_sex_enum as $sex_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $sex_v['enum_key']==$id_aData['user_sex_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $sex_v['enum_key']?>" name="user[user_sex]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $sex_v['enum_key']?>" name="user[user_sex]">
<?php }?>
<?php echo $sex_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_password">
								<?php echo $labels["user_password"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="password" maxlength="128" name="user[user_password]" value="<?php echo $id_aData['user_password'] ?>" id="user_password">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_password]" id="user_password">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_department">
								<?php echo $labels["user_department"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="user[user_department]" id="user_department" value_id ="<?php if(isset($id_aData['user_department_arr']['department_id'])){echo $id_aData['user_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['user_department_arr']['department_name'])){echo $id_aData['user_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_email">
								<?php echo $labels["user_email"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="user[user_email]" value="<?php echo $id_aData['user_email'] ?>" id="user_email">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_email]" id="user_email">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_gonghao">
								<?php echo $labels["user_gonghao"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="user[user_gonghao]" value="<?php echo $id_aData['user_gonghao'] ?>" id="user_gonghao">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_gonghao]" id="user_gonghao">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						
						
						<div class="control-group">
							<label class="control-label required" for="user_status">
								<?php echo $labels["user_status"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php foreach($user_status_enum as $status_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $status_v['enum_key']==$id_aData['user_status_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $status_v['enum_key']?>" name="user[user_status]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $status_v['enum_key']?>" name="user[user_status]">
<?php }?>
<?php echo $status_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_login_name">
								<?php echo $labels["user_login_name"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="user[user_login_name]" value="<?php echo $id_aData['user_login_name'] ?>" id="user_login_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_login_name]" id="user_login_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
					<?php }?>
                <?php }}else{ ?>
                  
						<div class="control-group">
							<label class="control-label required" for="user_name">
								<?php echo $labels["user_name"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="user[user_name]" value="<?php echo $id_aData['user_name'] ?>" id="user_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_name]" id="user_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_sex">
								<?php echo $labels["user_sex"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php foreach($user_sex_enum as $sex_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $sex_v['enum_key']==$id_aData['user_sex_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $sex_v['enum_key']?>" name="user[user_sex]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $sex_v['enum_key']?>" name="user[user_sex]">
<?php }?>
<?php echo $sex_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_password">
								<?php echo $labels["user_password"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="password" maxlength="128" name="user[user_password]" value="<?php echo $id_aData['user_password'] ?>" id="user_password">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_password]" id="user_password">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_department">
								<?php echo $labels["user_department"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="user[user_department]" id="user_department" value_id ="<?php if(isset($id_aData['user_department_arr']['department_id'])){echo $id_aData['user_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['user_department_arr']['department_name'])){echo $id_aData['user_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_email">
								<?php echo $labels["user_email"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="user[user_email]" value="<?php echo $id_aData['user_email'] ?>" id="user_email">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_email]" id="user_email">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_gonghao">
								<?php echo $labels["user_gonghao"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="user[user_gonghao]" value="<?php echo $id_aData['user_gonghao'] ?>" id="user_gonghao">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_gonghao]" id="user_gonghao">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
					
						
						<div class="control-group">
							<label class="control-label required" for="user_status">
								<?php echo $labels["user_status"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php foreach($user_status_enum as $status_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $status_v['enum_key']==$id_aData['user_status_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $status_v['enum_key']?>" name="user[user_status]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $status_v['enum_key']?>" name="user[user_status]">
<?php }?>
<?php echo $status_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="user_login_name">
								<?php echo $labels["user_login_name"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="user[user_login_name]" value="<?php echo $id_aData['user_login_name'] ?>" id="user_login_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="user[user_login_name]" id="user_login_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
                 <?php }?>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>