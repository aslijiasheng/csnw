	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="100">操作</th>
					<th><?php echo $labels['user_name'];?></th>
					<th><?php echo $labels['user_sex'];?></th>
					<th><?php echo $labels['user_email'];?></th>
					<th><?php echo $labels['user_department'];?></th>
					<th><?php echo $labels['user_gonghao'];?></th>
					<th><?php echo $labels['user_status'];?></th>
					<th><?php echo $labels['user_login_name'];?></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ( $listData as $listData_value ) {?>
			<tr>
				<td style="font-size:16px;">
					<a href="<?php echo site_url( 'www/user/view' ).'?user_id='.$listData_value['user_id'].'&type_id='.$_GET['type_id'] ?>" rel="tooltip" title="查看"><span class="icon-eye"></span></a>
				    <a href="<?php echo site_url( 'www/user/add_other_info' ).'?user_id='.$listData_value['user_id'].'&type_id='.$_GET['type_id'].'&user_name='.$listData_value['user_name'] ?>" rel="tooltip" title="配置额外信息"><span class="icon-attachment"></span></a>
				    <a href="<?php echo site_url( 'www/user/update' ).'?user_id='.$listData_value['user_id'].'&type_id='.$_GET['type_id'] ?>" rel="tooltip" title="更新"><span class="icon-pencil"></span></a>
				    <a href="<?php echo site_url( 'www/user/del' ).'?user_id='.$listData_value['user_id'].'&type_id='.$_GET['type_id'] ?>" rel="tooltip" title="删除"><span class="icon-remove-2"></span></a>
				</td>



					<td>
					<?php  echo $listData_value['user_name'];?>
					</td>

					<td>
					<?php echo !empty($listData_value['user_sex_arr']['enum_name']) ? $listData_value['user_sex_arr']['enum_name'] : '';?>
					</td>

					<td>
					<?php  echo $listData_value['user_email'];?>
					</td>

					<td>
							<?php
	if ( $listData_value['user_department']!=0 and $listData_value['user_department']!="" ) {
		echo !empty($listData_value['user_department_arr']['department_name']) ? $listData_value['user_department_arr']['department_name'] : '';
	}else {
		echo "&nbsp";
	}
?>
					</td>

					<td>
					<?php  echo !empty($listData_value['user_gonghao']) ? $listData_value['user_gonghao'] : '';?>
					</td>

					<td>
					<?php echo !empty($listData_value['user_status_arr']['enum_name']) ? $listData_value['user_status_arr']['enum_name'] : '';?>
					</td>



					<td>
					<?php  echo $listData_value['user_login_name'];?>
					</td>




			</tr>
		<?php }?>
		</tbody>

	</table>
	<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber;?>条</div>
	<input type='hidden' id='totalNumber' value="<?php echo $totalNumber;?>">
	</div>
