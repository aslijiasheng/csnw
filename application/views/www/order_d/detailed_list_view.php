<?php
$user_auth = $this->user->user_auth($this->session->userdata('user_id'));
//p($user_auth['activity_auth_arr']);
?>
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th width="20" style='text-align:center;'><span id='contraction_btn' class="icon-contract"></span></th>
			<th><?php echo $labels['order_d_order_product']; ?></th>
			<th width="200"><?php echo $labels['order_d_order_productcode']; ?></th>
			<th width="200">商品原价</th>
			<th width="200">商品折后价</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($listData as $k => $listData_value) { ?>
			<tr>
				<td style='text-align:center;' leetype='contraction' leefor="#contraction_tr_<?php echo $k; ?>" class='contraction_icon'><span class="icon-minus"></span></td>
				<td><?php echo $listData_value['order_d_goods_name']; ?></td>
				<td><?php echo $listData_value['order_d_goods_code']; ?></td>
				<td><?php echo $listData_value['order_d_order_primecost']; ?></td>
				<td><?php echo $listData_value['order_d_order_disc']; ?></td>
			</tr>
			<tr id='contraction_tr_<?php echo $k; ?>' class='contraction_tr'>
				<td style="font-size:16px;"></td>
				<td colspan="4">
					<table class="table table-bordered ">
						<thead>
							<tr>
								<th width="200">产品名称</th>
								<th width="200">产品编码</th>
								<th width="200"><?php echo $labels['order_d_order_primecost']; ?></th>
								<th width="200"><?php echo $labels['order_d_order_disc']; ?></th>
								<th width="200">域名／备案成本</th>
								<th width="200">短信成本</th>
								<th width="200">IDC成本</th>
								<th width="200">服务周期</th>
								<th width="200">开通状态</th>
								<th width="200">开通开始时间</th>
								<th width="200">开通结束时间</th>
								<th width="75">操作</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($listData_value['product_arr'] as $kk => $vv) { ?>
								<?php //p($vv);?>
								<tr>
									<td><?php echo $vv['order_d_product_basic_name']; ?></td>
									<td><?php echo $vv['order_d_product_basic_code']; ?></td>
									<td><?php echo $vv['order_d_product_basic_primecost']; ?></td>
									<td><?php echo $vv['order_d_product_basic_disc']; ?></td>
									<td><?php echo empty($vv['order_d_domain_cost']) ? '0.00' : $vv['order_d_domain_cost'] ; ?></td>
									<td><?php echo empty($vv['order_d_sms_cost']) ? '0.00' : $vv['order_d_sms_cost'] ; ?></td>
									<td><?php echo empty($vv['order_d_idc_cost']) ? '0.00' : $vv['order_d_idc_cost'] ; ?></td>
									<td><?php echo isset($vv['cycle']) ? $vv['cycle'] : ''; ?></td>
									<td><?php
										if ($vv['order_d_product_basic_style'] != "" or $vv['order_d_product_basic_style'] != 0) {
											echo $vv['order_d_product_basic_style_arr']['enum_name'];
										}
										?></td>
									<td>
										<?php
											if($vv['order_d_open_startdate']){
												echo substr($vv['order_d_open_startdate'],0,10);
											}
										?>
									</td>
									<td>
										<?php
											if($vv['order_d_open_enddate']){
												echo substr($vv['order_d_open_enddate'],0,10);
											}
										?>
									</td>
									<td>
										<?php if ($ydz_amount > 0 or $orderData['order_amount'] == 0) { ?>
											<?php if ($vv['order_d_product_basic_style'] == 1001 or $vv['order_d_product_basic_style'] == "" or $vv['order_d_product_basic_style'] == 0) { ?>
												<a class="badge badge-info ApplyOpen" data-id='<?php echo $vv['order_d_id']; ?>' <?php
												if (!in_array('apply_open', $user_auth['activity_auth_arr'])) {
													echo 'style="display:none;"';
												}
												?>> 申请开通 </a>
												<br>
											<?php } ?>
											<?php
											//这里的确认开通需要根据工单类型来做判断，是否显示
											if (isset($vv['params_arr']['open']['issue_type']['params_value'])) {
												$issue_type = $vv['params_arr']['open']['issue_type']['params_value'];
											} else {
												$issue_type = "";
											}
											$is_type_arr = array(
												'saas',
												'taoex',
												'sms',
												'oaekd',
												'oadrp',
												'oabqkz',
												'oawxd',
											);
											if (in_array($issue_type, $is_type_arr)) {
												?>
												<?php if ($vv['order_d_product_basic_style'] == 1002) { ?>
													<a class="badge badge-info ConfirmOpen" data-id='<?php echo $vv['order_d_id']; ?>' <?php
													if (!in_array('confirm_open', $user_auth['activity_auth_arr'])) {
														echo 'style="display:none;"';
													}
													?>> 确认开通 </a>
													<br>
												<?php } ?>
											<?php } ?>
											<?php if ($vv['order_d_product_basic_style'] == 1002) { ?>
												<a class="badge badge-info BelowLineOpen" data-id='<?php echo $vv['order_d_id']; ?>' <?php
												if (!in_array('below_line_open', $user_auth['activity_auth_arr'])) {
													echo 'style="display:none;"';
												}
												?>> 线下开通 </a>
												<br>
											   <?php } ?>
											<a class="badge badge-info SeeParams" data-id='<?php echo $vv['order_d_id']; ?>' <?php
											   if (!in_array('see_params', $user_auth['activity_auth_arr'])) {
												   echo 'style="display:none;"';
											   }
											   ?>> 查看参数 </a>
		<?php } ?>
									</td>
								</tr>
	<?php } ?>
						</tbody>
					</table>
				</td>
			</tr>
<?php } ?>
	</tbody>
</table>
<script type="text/javascript">
	$(document).ready(function() {
		$('[leetype="contraction"]').click(function() {
			leefor = $(this).attr('leefor');
			if ($(leefor).is(":hidden")) {
				//隐藏了则将它显示出来
				$(leefor).show(500);
				$(this).html('<span class="icon-minus"></span>');
			} else {
				//没隐藏则将它隐藏起来
				$(leefor).hide(500);
				$(this).html('<span class="icon-plus"></span>');
			}
		});
		$('#contraction_btn').click(function() {
			btncss = $(this).attr('class');
			if (btncss == 'icon-contract') {
				$(this).attr('class', 'icon-expand');
				$('.contraction_tr').hide(500);
				$('.contraction_icon').html('<span class="icon-plus"></span>');
			}
			if (btncss == 'icon-expand') {
				$(this).attr('class', 'icon-contract');
				$('.contraction_tr').show(500);
				$('.contraction_icon').html('<span class="icon-minus"></span>');
			}
		});

		//申请开通
		$(".ApplyOpen").click(function() {
			//alert('申请开通');
			id = $(this).attr('data-id');
			$.ajax({
				'type': 'get',
				'data': 'id=' + id,
				'success': function(data) {
					$('#operation_dialog').html(data);
				},
				'url': '<?php echo site_url('www/applyopen/ajax_apply_open'); ?>',
				'cache': false
			});
			$('#operation_dialog').dialog({
				modal: true,
				title: '申请开通',
				width: 800,
				height: 450,
				buttons: [{
						text: "申请开通",
						Class: "btn btn-primary",
						click: function() {
							$this = $("#operation_dialog");
							formdata = $("#applyopen-form").serializeArray();
							$.ajax({
								'type': 'post',
								'data': formdata,
								'success': function(data) {
									if (data == 1) {
										alertify.success("已申请，等待确认");
										$this.dialog("close");
										$("#home").ajaxHtml();
									} else {
										alert('失败' + data);
									}
								},
								'url': '<?php echo site_url('www/applyopen/ajax_add_post'); ?>',
								'async': false
							});
						}
					}, {
						text: "取消",
						Class: "btn bottom-margin",
						click: function() {
							alertify.error("你取消了开通申请");
							$(this).dialog("close");
						}
					}]

			});
			$('#operation_dialog').dialog('open');
		});
		//确认开通
		$(".ConfirmOpen").click(function() {
			//alert('确认开通');
			id = $(this).attr('data-id');
			$.ajax({
				'type': 'get',
				'data': 'id=' + id,
				'success': function(data) {
					$('#operation_dialog').html(data);
				},
				'url': '<?php echo site_url('www/applyopen/ajax_confirm_open'); ?>',
				'cache': false
			});
			$('#operation_dialog').dialog({
				modal: true,
				title: '确认开通',
				width: 800,
				height: 450,
				buttons: [{
						text: "确认开通",
						Class: "btn btn-primary",
						click: function() {
							$this = $("#operation_dialog");
							formdata = $("#ConfirmOpen-form").serializeArray();
							$.ajax({
								'type': 'post',
								'data': formdata,
								'success': function(data) {
									if (data == 1) {
										alertify.success("提交开通成功");
										$this.dialog("close");
										$("#home").ajaxHtml();
									} else {
										alert('失败' + data);
									}
								},
								'url': '<?php echo site_url('www/applyopen/ajax_confirm_post'); ?>',
								'async': false
							});
						}
					}, {
						text: "取消",
						Class: "btn bottom-margin",
						click: function() {
							alertify.error("你取消了确认开通");
							$(this).dialog("close");
						}
					}]
			});
			$('#operation_dialog').dialog('open');
		});
		//查看参数
		$(".SeeParams").click(function() {
			//alert('查看参数');
			id = $(this).attr('data-id');
			$.ajax({
				'type': 'get',
				'data': 'id=' + id,
				'success': function(data) {
					$('#operation_dialog').html(data);
				},
				'url': '<?php echo site_url('www/applyopen/ajax_see_params'); ?>',
				'cache': false
			});
			$('#operation_dialog').dialog({
				modal: true,
				title: '查看参数',
				width: 800,
				height: 450,
				buttons: [{
						text: "关闭",
						Class: "btn bottom-margin",
						click: function() {
							alertify.error("你关闭了查看参数");
							$(this).dialog("close");
						}
					}]
			});
			$('#operation_dialog').dialog('open');
		});
		//线下开通
		$(".BelowLineOpen").click(function() {
			//alert('线下开通');
			id = $(this).attr('data-id');
			$.ajax({
				'type': 'get',
				'data': 'id=' + id,
				'success': function(data) {
					$('#operation_dialog').html(data);
				},
				'url': '<?php echo site_url('www/applyopen/ajax_belowline_open'); ?>',
				'cache': false
			});
			$('#operation_dialog').dialog({
				modal: true,
				title: '线下确认开通',
				width: 800,
				height: 450,
				buttons: [{
						text: "线下确认开通",
						Class: "btn btn-primary",
						click: function() {
							//alert(111);
							$this = $("#operation_dialog");
							formdata = $("#belowline-form").serializeArray();
							$.ajax({
								'type': 'post',
								'data': formdata,
								'success': function(data) {
									if (data == 1) {
										alertify.success("线下开通成功");
										$this.dialog("close");
										$("#home").ajaxHtml();
									} else {
										alert('失败' + data);
									}
								},
								'url': '<?php echo site_url('www/applyopen/ajax_update_post'); ?>',
								'async': false
							});
						}
					}, {
						text: "取消",
						Class: "btn bottom-margin",
						click: function() {
							alertify.error("你取消了开通申请");
							$(this).dialog("close");
						}
					}]
			});
			$('#operation_dialog').dialog('open');
		});
	});
</script>