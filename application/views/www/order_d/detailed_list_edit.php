<div id='order_d_list_edit' style="overflow-x:auto;">
<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th width="60">
				<a id="order_d_detailed_add" title="添加明细"><span class="icon-plus"></span></a>
			</th>
			
			<th><?php echo $labels['order_d_order_id'];?></th>
			
			<th><?php echo $labels['order_d_order_product'];?></th>
			
			<th><?php echo $labels['order_d_order_productcode'];?></th>
			
			<th><?php echo $labels['order_d_order_basicproduct'];?></th>
			
			<th><?php echo $labels['order_d_order_basiccode'];?></th>
			
			<th><?php echo $labels['order_d_order_servicecycle'];?></th>
			
		</tr>
	</thead>
	<tbody>

	</tbody>
</table>
</div>
<script type="text/javascript">
//Data Tables
$(document).ready(function () {
	//把列表的内容信息全部组成数组
	$attrArr = [
	
	{
		"attr_name": "order_id",
		"attr_label": "所属订单",
		"attr_field_name": "order_d_order_id",
		"attr_type": "19",
		"attr_type_attr":{
			
			"quote_id":"订单",
			"url":"<?php echo site_url('www/order/ajax_list'); ?>",
			
		}
	},
	
	{
		"attr_name": "order_product",
		"attr_label": "商品名称",
		"attr_field_name": "order_d_order_product",
		"attr_type": "8",
		"attr_type_attr":{
			
		}
	},
	
	{
		"attr_name": "order_productcode",
		"attr_label": "商品编码",
		"attr_field_name": "order_d_order_productcode",
		"attr_type": "8",
		"attr_type_attr":{
			
		}
	},
	
	{
		"attr_name": "order_basicproduct",
		"attr_label": "基础产品名称",
		"attr_field_name": "order_d_order_basicproduct",
		"attr_type": "8",
		"attr_type_attr":{
			
		}
	},
	
	{
		"attr_name": "order_basiccode",
		"attr_label": "基础产品编码",
		"attr_field_name": "order_d_order_basiccode",
		"attr_type": "8",
		"attr_type_attr":{
			
		}
	},
	
	{
		"attr_name": "order_servicecycle",
		"attr_label": "服务周期",
		"attr_field_name": "order_d_order_servicecycle",
		"attr_type": "8",
		"attr_type_attr":{
			
		}
	},
	
	];
	$('#order_d_list_edit').leeDetailedEdit({
		"addId": "order_d_detailed_add",
		"attrArr":$attrArr
	});
});
</script>

