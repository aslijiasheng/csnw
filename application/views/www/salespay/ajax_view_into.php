<!--lee 内容部分 start-->
<?php //p($salespay_data);?>
<div class="grid-view">
	<form class="form-horizontal">
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单总金额
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_order_id_arr']['order_amount'];?>
				</div>
			</div>
		</div>
		<!--所属子公司 2014/8/25-->
		<div class="control-group">
			<label class="control-label required" for="order_subsidiary">
				所属子公司
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_subsidiary_arr']['enum_name'];?>
				</div>
			</div>
		</div>
		<!--所属子公司 2014/8/25-->
		<div class="control-group">
			<label class="control-label required" for="salespay_pay_amount">
				本次入账金额
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_pay_amount'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="salespay_pay_date">
				入账日期
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_pay_date'];?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_sp_date">
				确认到帐时间
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_sp_date'];?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_sp_real_date">
				实际到帐时间(U8)
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_sp_real_date'];?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_pay_method">
				<?php echo $labels["salespay_pay_method"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_pay_method_arr']['enum_name'];?>
				</div>
			</div>
		</div>

						<div class="control-group">
							<div class="controls">
<div id="remit_info" class="well payinfo">
	<div class="form-horizontal">
		<div class="control-group">
			<label class="control-label" style="width: 120px;">汇款账户：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php if(isset($salespay_data['salespay_my_bank_account_arr'])){echo $salespay_data['salespay_my_bank_account_arr']['enum_name'];}?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">汇款人姓名：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $salespay_data['salespay_account_bank_account_name'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">汇款人银行账号：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $salespay_data['salespay_account_bank_account'];?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="alipay_info" class="well payinfo" style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;">入帐支付宝帐号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php
					if(isset($salespay_data['salespay_my_alipay_account_arr']['enum_name'])){
						echo $salespay_data['salespay_my_alipay_account_arr']['enum_name'];
					}
				?>
			</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;">交易号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $salespay_data['salespay_alipay_order'];?>
			</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;">支付宝账号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $salespay_data['salespay_account_alipay_account'];?>
			</div>
		</div>
	</div>
</div>
<div id="check_info" class="well payinfo" style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;">支票名称：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $salespay_data['salespay_check_name'];?>
			</div>
		</div>
	</div>
</div>
<div id="cash_info" class="payinfo" style="display:none;"> </div>

							</div>
						</div>
<script type="text/javascript">
$(document).ready(function () {
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1001){
		$(".payinfo").hide();
		$("#remit_info").show();
	}
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1002){
		$(".payinfo").hide();
		$("#alipay_info").show();
	}
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1003){
		$(".payinfo").hide();
		$("#check_info").show();
	}
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1004){
		$(".payinfo").hide();
		$("#cash_info").show();
	}
});
</script>
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_note">
								<?php echo $labels["salespay_pay_note"];?>
							</label>
							<div class="controls">
								<pre><?php echo $salespay_data['salespay_pay_note'];?></pre>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="salespay_examine_note">
								<?php echo $labels["salespay_examine_note"];?>
							</label>
							<div class="controls">
								<pre><?php echo $salespay_data['salespay_examine_note'];?></pre>
							</div>
						</div>
	</form>
</div>
<!--lee 内容部分 end-->