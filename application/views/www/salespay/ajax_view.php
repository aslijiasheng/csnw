<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal">
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单总金额
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_order_id_arr']['order_amount'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="salespay_pay_amount">
				本次入账金额
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_pay_amount'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="salespay_pay_date">
				<?php echo $labels["salespay_pay_date"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_pay_date'];?>
				</div>
			</div>
		</div>
						
		<div class="control-group">
			<label class="control-label required" for="salespay_pay_method">
				<?php echo $labels["salespay_pay_method"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_pay_method_arr']['enum_name'];?>
				</div>
			</div>
		</div>
						
						<div class="control-group">
							<div class="controls">
<div id="remit_info" class="well payinfo">
	<div class="form-horizontal">
		<div class="control-group">
			<label class="control-label" style="width: 120px;">汇款账户：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php 
						if(isset($salespay_data['salespay_pay_info_arr']['remit_bank_arr']['enum_name'])){
							echo $salespay_data['salespay_pay_info_arr']['remit_bank_arr']['enum_name'];
						}
					?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">汇款人姓名：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $salespay_data['salespay_pay_info_arr']['remit_person'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">汇款人银行账号：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $salespay_data['salespay_pay_info_arr']['remit_account'];?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="alipay_info" class="well payinfo" style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;">入帐支付宝帐号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php 
					if(isset($salespay_data['salespay_pay_info_arr']['alipay_bank_arr']['enum_name'])){
						echo $salespay_data['salespay_pay_info_arr']['alipay_bank_arr']['enum_name'];
					}
				?>
			</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;">交易号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $salespay_data['salespay_pay_info_arr']['alipay_order'];?>
			</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;">支付宝账号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $salespay_data['salespay_pay_info_arr']['alipay_account'];?>
			</div>
		</div>
	</div>
</div>
<div id="check_info" class="well payinfo" style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;">支票名称：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $salespay_data['salespay_pay_info_arr']['check_name'];?>
			</div>
		</div>
	</div>
</div>
<div id="cash_info" class="payinfo" style="display:none;"> </div>
								
							</div>
						</div>
<script type="text/javascript">
$(document).ready(function () {
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1001){
		$(".payinfo").hide();
		$("#remit_info").show();
	}
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1002){
		$(".payinfo").hide();
		$("#alipay_info").show();
	}
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1003){
		$(".payinfo").hide();
		$("#check_info").show();
	}
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1004){
		$(".payinfo").hide();
		$("#cash_info").show();
	}
});
</script>
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_note">
								<?php echo $labels["salespay_pay_note"];?>
							</label>
							<div class="controls">
								<pre><?php echo $salespay_data['salespay_pay_note'];?></pre>
							</div>
						</div>
	</form>
</div>
<!--lee 内容部分 end-->