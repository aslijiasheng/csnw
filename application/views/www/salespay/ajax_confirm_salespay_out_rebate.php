<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="confirm-form">
		<!-- 用一个隐藏文本框来存order_id -->
		<input size="16" type="hidden" name="salespay[salespay_id]" value='<?php echo $salespay_data['salespay_id'];?>'>
		<input size="16" type="hidden" name="salespay[salespay_order_id]" value='<?php echo $salespay_data['salespay_order_id'];?>'>
		<input size="16" type="hidden" name="order_finance_department" value='<?php echo $order_data['order_finance_arr']['user_department'];?>'>
		<input size="16" type="hidden" name="order_finance" value='<?php echo $order_data['order_finance_arr']['user_id'];?>'>
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单总金额
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_order_id_arr']['order_amount'];?>
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label required">
				款项类型名称
			</label>
			<div class="controls">
				<div class="group-text">
					返点款项
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_reviewer">
				业务负责人
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_reviewer_arr']['user_name'];?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required">
				选择返点的客户
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_rebate_account_arr']['account_name'];?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_pay_amount">
				本次返点金额
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_pay_amount'];?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_book_id">
				<?php echo $labels["salespay_book_id"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" required="required" title="<?php echo $labels["salespay_book_id"];?>" name="salespay[salespay_book_id]" id="salespay_book_id" value_id ="" value="" url="<?php echo site_url('www/books/ajax_list') ?>" title='关联日记账' leetype="quote" dialogwidth="1099" readonly>
			</div>
		</div>
<script type="text/javascript">
$(document).ready(function () {
	//这里的引用需要触发一下
	$('#salespay_book_id').leeQuote();
});
</script>


		<div class="control-group">
			<label class="control-label required" for="salespay_sp_date">
				确认出账日期
				<span class="required"></span>
			</label>
			<div class="controls">
<div class="input-append date form_date" id="salespay_sp_date">
	<input size="16" type="text" name="salespay[salespay_sp_date]" style="width: 154px;" readonly id="salespay_sp_date" required="required" title="<?php echo $labels["salespay_sp_date"];?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-calendar"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#salespay_sp_date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		format:'yyyy-mm-dd'
    });
});
</script>
			</div>
		</div>


		<div class="control-group">
			<label class="control-label required" for="salespay_sp_real_date">
				实际出账日期（用于U8）
				<span class="required"></span>
			</label>
			<div class="controls">
<div class="input-append date form_date" id="salespay_sp_real_date">
	<input size="16" type="text" name="salespay[salespay_sp_real_date]" style="width: 154px;" readonly id="salespay_sp_real_date" required="required" title="<?php echo $labels["salespay_sp_real_date"];?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-calendar"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#salespay_sp_real_date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		format:'yyyy-mm-dd'
    });
});
</script>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_pay_method">
				<?php echo $labels["salespay_pay_method"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_pay_method_arr']['enum_name'];?>
				</div>
			</div>
		</div>

						<div class="control-group">
							<div class="controls">
<div id="remit_info" class="well payinfo">
	<div class="form-horizontal">
		<div class="control-group">
			<label class="control-label" style="width: 120px;">客户账号类型：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php if(isset($salespay_data['salespay_account_bank_type_arr']['enum_name']))echo $salespay_data['salespay_account_bank_type_arr']['enum_name'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">客户银行账户：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $salespay_data['salespay_account_bank_account'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">客户账户名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $salespay_data['salespay_account_bank_account_name'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">开户银行名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $salespay_data['salespay_account_bank_name'];?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="alipay_info" class="well payinfo" style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;">公司支付宝帐号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php 
					if(isset($salespay_data['salespay_my_alipay_bank_arr']['enum_name'])){
						echo $salespay_data['salespay_my_alipay_bank_arr']['enum_name'];
					}
				?>
			</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;">交易号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $salespay_data['salespay_alipay_order'];?>
			</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;">客户支付宝账号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $salespay_data['salespay_account_alipay_account'];?>
			</div>
		</div>
	</div>
</div>
<div id="check_info" class="well payinfo" style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;">支票名称：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $salespay_data['salespay_check_name'];?>
			</div>
		</div>
	</div>
</div>
<div id="cash_info" class="payinfo" style="display:none;"> </div>

							</div>
						</div>
<script type="text/javascript">
$(document).ready(function () {
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1001){
		$(".payinfo").hide();
		$("#remit_info").show();
	}
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1002){
		$(".payinfo").hide();
		$("#alipay_info").show();
	}
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1003){
		$(".payinfo").hide();
		$("#check_info").show();
	}
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1004){
		$(".payinfo").hide();
		$("#cash_info").show();
	}
});
</script>
		<div class="control-group">
			<label class="control-label required" for="salespay_rebate_person">
				返点经手人姓名
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_rebate_person'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="salespay_pay_note">
				返点备注
			</label>
			<div class="controls">
				<pre><?php echo $salespay_data['salespay_pay_note'];?></pre>
			</div>
		</div>
	</form>
</div>
<!--lee 内容部分 end-->