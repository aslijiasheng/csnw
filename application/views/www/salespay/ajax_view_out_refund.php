<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal">
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单总金额
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_order_id_arr']['order_amount'];?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单所属客户
			</label>
			<div class="controls">
				<div class="group-text">
					<?php if(isset($salespay_data['salespay_order_id_arr']['order_account'])){echo $salespay_data['salespay_order_id_arr']['order_account_arr']['account_name'];}?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required">
				款项类型名称
			</label>
			<div class="controls">
				<div class="group-text">
					退款项
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_reviewer">
				业务负责人
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_reviewer_arr']['user_name'];?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required">
				退款商品信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<pre><?php echo $salespay_data['salespay_goods_info_txt'];?></pre>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_pay_amount">
				本次退款总金额
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_pay_amount'];?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_book_id">
				关联日记账
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php if(isset($salespay_data['salespay_book_id_arr'])){echo $salespay_data['salespay_book_id_arr']['books_number'];}?>
				</div>
			</div>
		</div>


		<div class="control-group">
			<label class="control-label required" for="salespay_sp_date">
				确认退款时间
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php if($salespay_data['salespay_sp_date']!="" and $salespay_data['salespay_sp_date']!=0){echo date('Y-m-d',strtotime($salespay_data['salespay_sp_date']));}?>
				</div>
			</div>
		</div>


		<div class="control-group">
			<label class="control-label required" for="salespay_sp_real_date">
				实际退款时间（用于U8）
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php if($salespay_data['salespay_sp_real_date']!="" and $salespay_data['salespay_sp_real_date']!=0){echo date('Y-m-d',strtotime($salespay_data['salespay_sp_real_date']));}?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_pay_method">
				<?php echo $labels["salespay_pay_method"];?>
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $salespay_data['salespay_pay_method_arr']['enum_name'];?>
				</div>
			</div>
		</div>

						<div class="control-group">
							<div class="controls">
<div id="remit_info" class="well payinfo">
	<div class="form-horizontal">
		<div class="control-group">
			<label class="control-label" style="width: 120px;">客户账号类型：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php if(isset($salespay_data['salespay_account_bank_type_arr']['enum_name']))echo $salespay_data['salespay_account_bank_type_arr']['enum_name'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">客户银行账户：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $salespay_data['salespay_account_bank_account'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">客户账户名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $salespay_data['salespay_account_bank_account_name'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">客户开户行名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<div class="group-text">
					<?php echo $salespay_data['salespay_account_bank_name'];?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="alipay_info" class="well payinfo" style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;">公司支付宝帐号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php 
					if(isset($salespay_data['salespay_my_alipay_account_arr']['enum_name'])){
						echo $salespay_data['salespay_my_alipay_account_arr']['enum_name'];
					}
				?>
			</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;">交易号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $salespay_data['salespay_alipay_order'];?>
			</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;">客户支付宝账号：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $salespay_data['salespay_account_alipay_account'];?>
			</div>
		</div>
	</div>
</div>
<div id="check_info" class="well payinfo" style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;">支票名称：</label>
		<div class="controls" style="margin-left: 120px;">
			<div class="group-text">
				<?php echo $salespay_data['salespay_check_name'];?>
			</div>
		</div>
	</div>
</div>
<div id="cash_info" class="payinfo" style="display:none;"> </div>

							</div>
						</div>
<script type="text/javascript">
$(document).ready(function () {
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1001){
		$(".payinfo").hide();
		$("#remit_info").show();
	}
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1002){
		$(".payinfo").hide();
		$("#alipay_info").show();
	}
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1003){
		$(".payinfo").hide();
		$("#check_info").show();
	}
	if(<?php echo $salespay_data['salespay_pay_method'];?>==1004){
		$(".payinfo").hide();
		$("#cash_info").show();
	}
});
</script>
		<div class="control-group">
			<label class="control-label required" for="salespay_pay_note">
				 退款备注
			</label>
			<div class="controls">
				<pre><?php echo $salespay_data['salespay_pay_note'];?></pre>
			</div>
		</div>
	</form>
</div>
<!--lee 内容部分 end-->