<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id='salespay-form'>
		<!-- 用一个隐藏文本框来存order_id -->
		<input size="16" type="hidden" name="salespay[salespay_id]" value='<?php echo $salespay_data['salespay_id'];?>'>
		<input size="16" type="hidden" name="salespay[salespay_order_id]" value='<?php echo $salespay_data['salespay_order_id'];?>'>
		<input type="hidden" name="order_finance" value="<?php echo $order_data['order_finance_arr']['user_id']; ?>">
		<input type="hidden" name="order_finance_department" value="<?php echo $order_data['order_finance_arr']['user_department']; ?>">
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td width="10%">订单总金额</td>
								<td width="10%"><?php echo $order_data['order_amount'];?></td>
								<td width="10%">返点总额</td>
								<td width="10%"><?php echo $order_data['order_rebate_amount'];?></td>
							<tr>
							</tr>
								<td>已确认返点</td>
								<td><?php echo $yfd_amount;?></td>
								<td>未确认返点</td>
								<td><?php echo $wfd_amount;?></td>
							</tr>
							</tr>
								<td>可返款金额</td>
								<td colspan="3" id="kfd_amount"><?php echo $kfd_amount;?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required">
				款项类型名称
			</label>
			<div class="controls">
				<input type="hidden" name="salespay[salespay_money_type]" id='salespay_money_type' value="1002">
				<input type="hidden" name="salespay[salespay_money_type_name]" id='salespay_money_type_name' value="1002">
				<input type="text" value="返点款项" disabled>
			</div>
		</div>
		<!--新增所属子公司 2014/8/25-->
			<div class="control-group">
				<label class="control-label required" for="salespay_subsidiary">
					<?php echo $labels["salespay_subsidiary"]; ?>
					<span class="required"></span>
				</label>
				<div class="controls">
					<?php foreach ($salespay_subsidiary_enum as $company_v) { ?>
						<label class="radio inline">
							<?php if (isset($salespay_data) && $company_v['enum_key'] == $salespay_data['salespay_subsidiary_arr']['enum_key']) { ?>
								<input id="inlineRadioB" type="radio" value="<?php echo $company_v['enum_key'] ?>" name="salespay_data[salespay_subsidiary]" checked>
							<?php } else { ?>
								<input id="inlineRadioB" type="radio"  value="<?php echo $company_v['enum_key'] ?>" name="salespay_data[salespay_subsidiary]">
							<?php } ?>
							<?php echo $company_v['enum_name']; ?>
						</label>
					<?php } ?>
					<span class="help-inline"></span>
				</div>
			</div>
		<!--新增所属子公司 2014/8/25-->
		<div class="control-group">
			<label class="control-label required" for="salespay_reviewer">
				业务负责人
			</label>
			<div class="controls">
<input type="text" name="salespay[salespay_reviewer]" id="salespay_reviewer" value_id ="<?php echo $salespay_data['salespay_reviewer'];?>" value="<?php if(isset($salespay_data['salespay_reviewer_arr'])){echo $salespay_data['salespay_reviewer_arr']['user_name'];}?>" required="required" title="业务负责人">
<script type="text/javascript">
$(document).ready(function () {
	$('#salespay_reviewer').leeQuote({
		url:'<?php echo site_url('www/user/ajax_list?tag_name=salespay_reviewer');?>',
		title:'选择主管人员',
		data:{
			'rel_role_id':10,
		}
	});
});
</script>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_rebate_account">
				选择返点的客户
				<span class="required"></span>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="salespay[salespay_rebate_account]" required="required" id="salespay_rebate_account" value_id ="<?php echo $salespay_data['salespay_rebate_account'];?>" value="<?php if(isset($salespay_data['salespay_rebate_account_arr'])){echo $salespay_data['salespay_rebate_account_arr']['account_name'];}?>" url="<?php echo site_url('www/account/ajax_list') ?>">
			</div>
<script type="text/javascript">
$(document).ready(function () {
	//这里的引用需要触发一下
	$('#salespay_rebate_account').leeQuote({
		url:'<?php echo site_url('www/account/ajax_list?tag_name=salespay_rebate_account');?>',
		title:'选择返点客户',
	});
});
</script>
		</div>

		<div class="control-group">
			<label class="control-label required"  for="salespay_pay_amount">
				本次返点金额
			</label>
			<div class="controls">
				<input size="16" id="salespay_pay_amount" type="text" name="salespay[salespay_pay_amount]" required="required" value="<?php echo $salespay_data['salespay_pay_amount'];?>">
				<span class="text-error">(必填)</span>
			</div>
		</div>


						<div class="control-group">
							<label class="control-label required" for="salespay_pay_method">
								<?php echo $labels["salespay_pay_method"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="salespay_pay_method" name="salespay[salespay_pay_method]">
									<?php foreach($salespay_pay_method_enum as $pay_method_v){ ?>
									<option value="<?php echo $pay_method_v['enum_key'];?>" <?php
										if ($salespay_data['salespay_pay_method']==$pay_method_v['enum_key']){
											echo 'selected';
										}
									?>>
										<?php echo $pay_method_v['enum_name'];?>
									</option>
									<?php } ?>
								</select>
							</div>
						</div>


						<div class="control-group">
							<div class="controls">
<div id="remit_info" class="well payinfo" <?php
	if ($salespay_data['salespay_pay_method']!=1001){
		echo 'style="display:none;"';
	}
?>>
	<div class="form-horizontal">

		<div class="control-group">
			<label class="control-label" style="width: 120px;">账户类型：</label>
			<div class="controls" style="margin-left: 120px;">
				<?php foreach($account_bank_type_enum as $salespay_bank_type_v){ ?>
				<label class="radio inline">
					<input type="radio" name="salespay[salespay_account_bank_type]" value="<?php echo $salespay_bank_type_v['enum_key'];?>" <?php
					if ($salespay_data['salespay_account_bank_type']==$salespay_bank_type_v['enum_key']){
						echo 'checked';
					}
					?>>
					<?php echo $salespay_bank_type_v['enum_name'];?>
				</label>
				<?php } ?>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" style="width: 120px;" for="remit_bank">客户银行账户：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="salespay[salespay_account_bank_account]" id="remit_bank" required="required" class="isrequired" value='<?php echo $salespay_data["salespay_account_bank_account"];?>'>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;" for="remit_person">账户姓名：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="salespay[salespay_account_bank_account_name]" id="remit_person" required="required" class="isrequired" value='<?php echo $salespay_data["salespay_account_bank_account_name"];?>'>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;">开户银行名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="salespay[salespay_account_bank_name]" value='<?php echo $salespay_data["salespay_account_bank_name"];?>'>
			</div>
		</div>
	</div>
</div>
<div id="alipay_info" class="well payinfo" <?php
	if ($salespay_data['salespay_pay_method']!=1002){
		echo 'style="display:none;"';
	}
?>>
	<div class="control-group">
		<label class="control-label" style="width: 120px;">入帐支付宝帐号：</label>
		<div class="controls" style="margin-left: 120px;">
			<select id="country" name="salespay[salespay_my_alipay_account]">
				<?php foreach($my_alipay_account_enum as $alipay_bank_v){ ?>
				<option value="<?php echo $alipay_bank_v['enum_key'];?>" <?php
					if ($salespay_data["salespay_my_alipay_account"]==$alipay_bank_v['enum_key']){
						echo 'selected';
					}
				?>>
					<?php echo $alipay_bank_v['enum_name'];?>
				</option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="alipay_order">交易号：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="salespay[salespay_alipay_order]" id="alipay_order"  required="required" class="isrequired" value="<?php echo $salespay_data["salespay_alipay_order"];?>">
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="alipay_account">支付宝账号：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="salespay[salespay_account_alipay_account]" id="alipay_account" required="required"  class="isrequired" value="<?php echo $salespay_data["salespay_account_alipay_account"];?>">
		</div>
	</div>
</div>
<div id="check_info" class="well payinfo" <?php
	if ($salespay_data['salespay_pay_method']!=1003){
		echo 'style="display:none;"';
	}
?>>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="check_name">支票名称：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="salespay[salespay_check_name]" id="check_name" required="required" class="isrequired" value='<?php echo $salespay_data['salespay_check_name'];?>'>
		</div>
	</div>
</div>
<div id="cash_info" class="payinfo" <?php
	if ($salespay_data['salespay_pay_method']!=1004){
		echo 'style="display:none;"';
	}
?>></div>

							</div>
						</div>
<script type="text/javascript">
$(document).ready(function () {
	var salespay_pay_method = '<?php echo $salespay_data['salespay_pay_method']; ?>';

	if(salespay_pay_method==1001){

		$("#alipay_info").find('.isrequired').removeAttr('required');
		$("#check_info").find('.isrequired').removeAttr('required');
		$("#cash_info").find('.isrequired').removeAttr('required');
	}else if(salespay_pay_method==1002){
		$("#remit_info").find('.isrequired').removeAttr('required');

		$("#check_info").find('.isrequired').removeAttr('required');
		$("#cash_info").find('.isrequired').removeAttr('required');
	}else if(salespay_pay_method==1003){
		$("#remit_info").find('.isrequired').removeAttr('required');
		$("#alipay_info").find('.isrequired').removeAttr('required');

		$("#cash_info").find('.isrequired').removeAttr('required');
	}else{
		$("#remit_info").find('.isrequired').removeAttr('required');
		$("#alipay_info").find('.isrequired').removeAttr('required');
		$("#check_info").find('.isrequired').removeAttr('required');

	}
	$('#salespay_pay_method').change(function(){
		//获取选择的value用作判断
		enumkey = $(this).val();
		if(enumkey==1001){
			$(".payinfo").hide();
			$("#remit_info").show();
			$("#remit_info").find('.isrequired').attr('required','required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1002){
			$(".payinfo").hide();
			$("#alipay_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').attr('required','required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1003){
			$(".payinfo").hide();
			$("#check_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').attr('required','required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1004){
			$(".payinfo").hide();
			$("#cash_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').attr('required','required');
		}
	});
});
</script>
		<div class="control-group">
			<label class="control-label required" for="salespay_rebate_person">
				返点经手人姓名
			</label>
			<div class="controls">
				<input size="16" type="text" name="salespay[salespay_rebate_person]" value="<?php echo $salespay_data['salespay_rebate_person'];?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="salespay_pay_note">
				返点备注
			</label>
			<div class="controls">
				<textarea name="salespay[salespay_pay_note]" id="salespay_pay_note" style="width:400px" rows="6"><?php if(isset($salespay_data["salespay_pay_note"])){echo $salespay_data["salespay_pay_note"];}?></textarea>
			</div>
		</div>
	</form>
</div>
<!--lee 内容部分 end-->