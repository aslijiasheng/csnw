<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						入款项
						<span class="mini-title">
						salespay
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">

						</div>
						<div id="divmessagelist">

<div class="grid-view">
	<div id="salespay_list"></div>
<div>
<script type="text/javascript">
$(document).ready(function () {
	$selectAttr=[

		{'value':'pay_date','txt':'入账日期'},

		{'value':'order_id.number','txt':'订单编号'},

		{'value':'pay_method.name','txt':'支付方式'},

		{'value':'pay_info','txt':'支付信息'},

		{'value':'pay_amount','txt':'入账金额'},

		{'value':'create_user.name','txt':'创建人'},

		{'value':'create_time','txt':'创建时间'},

	];
	$("#salespay_list").leeDataTable({
		selectAttr:$selectAttr, //简单查询的查询属性
		url:"<?php echo site_url('www/salespay/ajax_select'); ?>", //ajax查询的地址
		perNumber:10 //每页显示多少条数据
	});
});

</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="operation_dialog"></div>

