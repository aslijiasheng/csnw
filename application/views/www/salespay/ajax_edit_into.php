<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id='salespay-form'>
		<!-- 用一个隐藏文本框来存order_id -->
		<input size="16" type="hidden" name="salespay[salespay_id]" value='<?php echo $salespay_data['salespay_id'];?>'>
		<input size="16" type="hidden" name="salespay[salespay_order_id]" value='<?php echo $salespay_data['salespay_order_id'];?>'>
		<input type="hidden" name="order_finance" value="<?php echo $order_data['order_finance_arr']['user_id']; ?>">
		<input type="hidden" name="order_finance_department" value="<?php echo $order_data['order_finance_arr']['user_department']; ?>">
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td width="10%">订单总金额</td>
								<td width="10%"><?php echo $salespay_data['salespay_order_id_arr']['order_amount'];?></td>
								<td width="10%">可到帐金额</td>
								<td width="10%" id='kdz_amount'><?php echo $kdz_amount;?></td>
							<tr>
							</tr>
								<td width="10%">已确认到帐</td>
								<td width="10%"><?php echo $ydz_amount;?></td>
								<td width="10%">未确认到帐</td>
								<td width="10%"><?php echo $wdz_amount;?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="salespay_pay_amount">
				本次入账金额
				<span class="required"></span>
			</label>
			<div class="controls">
				<input size="16" type="text" name="salespay[salespay_pay_amount]" required="required" value='<?php if(isset($salespay_data['salespay_pay_amount'])){echo $salespay_data['salespay_pay_amount'];}?>'>
			</div>
		</div>
		<!--新增所属子公司 2014/8/25-->
			<div class="control-group">
				<label class="control-label required" for="salespay_subsidiary">
					<?php echo $labels["salespay_subsidiary"]; ?>
					<span class="required"></span>
				</label>
				<div class="controls">
					<?php foreach ($salespay_subsidiary_enum as $company_v) { ?>
						<label class="radio inline">
							<?php if (isset($salespay_data) && $company_v['enum_key'] == $salespay_data['salespay_subsidiary_arr']['enum_key']) { ?>
								<input id="inlineRadioB" type="radio" value="<?php echo $company_v['enum_key'] ?>" name="salespay[salespay_subsidiary]" checked>
							<?php } else { ?>
								<input id="inlineRadioB" type="radio"  value="<?php echo $company_v['enum_key'] ?>" name="salespay[salespay_subsidiary]">
							<?php } ?>
							<?php echo $company_v['enum_name']; ?>
						</label>
					<?php } ?>
					<span class="help-inline"></span>
				</div>
			</div>
		<!--新增所属子公司 2014/8/25-->


						<div class="control-group">
							<label class="control-label required" for="salespay_pay_date">
								<?php echo $labels["salespay_pay_date"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="salespay_pay_date">
									<input size="16" type="text" name="salespay[salespay_pay_date]" required="required" style="width: 154px;" value='<?php if(isset($salespay_data['salespay_pay_date'])){echo date("Y-m-d",strtotime($salespay_data['salespay_pay_date']));}?>'>
									<span class="add-on"><i class="icon-close"></i></span>
									<span class="add-on"><i class="icon-clock"></i></span>
								</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#salespay_pay_date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		format:'yyyy-mm-dd'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="salespay_pay_method">
								<?php echo $labels["salespay_pay_method"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="salespay_pay_method" name="salespay[salespay_pay_method]">
									<?php foreach($salespay_pay_method_enum as $pay_method_v){ ?>
									<option value="<?php echo $pay_method_v['enum_key'];?>" <?php
										if ($salespay_data['salespay_pay_method']==$pay_method_v['enum_key']){
											echo 'selected';
										}
									?>>
										<?php echo $pay_method_v['enum_name'];?>
									</option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
<div id="remit_info" class="well payinfo" <?php
	if ($salespay_data['salespay_pay_method']!=1001){
		echo 'style="display:none;"';
	}
?>>
<!--2014、09、16修改编辑页面 汇款子项内容-->
		<div class="form-horizontal">
			<div class="control-group">
				<label class="control-label" style="width: 120px;">到帐银行账户：</label>
				<div class="controls" style="margin-left: 120px;">
					<select id="country" name="salespay[salespay_my_bank_account]">
						<?php foreach($my_bank_account_enum as $my_bank_account_v){ ?>
						<option value="<?php echo $my_bank_account_v['enum_key'];?>" <?php if($salespay_data['salespay_my_bank_account']==$my_bank_account_v['enum_key']) echo "checked=checked";?>>
							<?php echo $my_bank_account_v['enum_name'];?>
						</option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="account_bank_account_name" style="width: 120px;">汇款人姓名：</label>
				<div class="controls"  style="margin-left: 120px;">
					<input type="text" name="salespay[salespay_account_bank_account_name]" id="account_bank_account_name" value="<?php echo $salespay_data['salespay_account_bank_account_name'];?>"  required="required" class="isrequired">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" style="width: 120px;" for="account_bank_account">汇款人银行账号：</label>
				<div class="controls" style="margin-left: 120px;">
					<input type="text" name="salespay[salespay_account_bank_account]" id="account_bank_account"  required="required" value="<?php echo $salespay_data['salespay_account_bank_account'];?>" class="isrequired">
				</div>
			</div>
		</div>
<!--2014、09、16修改编辑页面 汇款子项内容-->
</div>
<div id="alipay_info" class="well payinfo" <?php
	if ($salespay_data['salespay_pay_method']!=1002){
		echo 'style="display:none;"';
	}
?>>
	<div class="control-group">
		<label class="control-label" style="width: 120px;">入帐支付宝帐号：</label>
		<div class="controls" style="margin-left: 120px;">
			<select id="country" name="salespay[salespay_my_alipay_account]">
				<?php foreach($my_alipay_account_enum as $alipay_bank_v){ ?>
				<option value="<?php echo $alipay_bank_v['enum_key'];?>" <?php
					if ($salespay_data["salespay_my_alipay_account"]==$alipay_bank_v['enum_key']){
						echo 'selected';
					}
				?>>
					<?php echo $alipay_bank_v['enum_name'];?>
				</option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="alipay_order">交易号：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="salespay[salespay_alipay_order]" value="<?php echo isset($salespay_data['salespay_alipay_order']) ? $salespay_data['salespay_alipay_order'] : '';?>" id="alipay_order"  required="required" class="isrequired">
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="alipay_account">支付宝账号：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="salespay[salespay_account_alipay_account]" value="<?php echo isset($salespay_data['salespay_account_alipay_account']) ? $salespay_data['salespay_account_alipay_account'] : '';?>" id="alipay_account" required="required"  class="isrequired">
		</div>
	</div>
</div>
<div id="check_info" class="well payinfo" <?php
	if ($salespay_data['salespay_pay_method']!=1003){
		echo 'style="display:none;"';
	}
?>>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="check_name">支票名称：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="salespay[salespay_check_name]" id="check_name" required="required" class="isrequired" value="<?php echo isset($salespay_data['salespay_check_name']) ? $salespay_data['salespay_check_name'] : '';?>">
		</div>
	</div>
</div>
<div id="cash_info" class="payinfo" <?php
	if ($salespay_data['salespay_pay_method']!=1004){
		echo 'style="display:none;"';
	}
?>> </div>

							</div>
						</div>
<script type="text/javascript">
$(document).ready(function () {
	var salespay_pay_method = '<?php echo $salespay_data['salespay_pay_method']; ?>';

	if(salespay_pay_method==1001){

		$("#alipay_info").find('.isrequired').removeAttr('required');
		$("#check_info").find('.isrequired').removeAttr('required');
		$("#cash_info").find('.isrequired').removeAttr('required');
	}else if(salespay_pay_method==1002){
		$("#remit_info").find('.isrequired').removeAttr('required');

		$("#check_info").find('.isrequired').removeAttr('required');
		$("#cash_info").find('.isrequired').removeAttr('required');
	}else if(salespay_pay_method==1003){
		$("#remit_info").find('.isrequired').removeAttr('required');
		$("#alipay_info").find('.isrequired').removeAttr('required');

		$("#cash_info").find('.isrequired').removeAttr('required');
	}else{
		$("#remit_info").find('.isrequired').removeAttr('required');
		$("#alipay_info").find('.isrequired').removeAttr('required');
		$("#check_info").find('.isrequired').removeAttr('required');

	}
	$('#salespay_pay_method').change(function(){
		//获取选择的value用作判断
		enumkey = $(this).val();
		if(enumkey==1001){
			$(".payinfo").hide();
			$("#remit_info").show();
			$("#remit_info").find('.isrequired').attr('required','required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1002){
			$(".payinfo").hide();
			$("#alipay_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').attr('required','required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1003){
			$(".payinfo").hide();
			$("#check_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').attr('required','required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1004){
			$(".payinfo").hide();
			$("#cash_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').attr('required','required');
		}
	});
});
</script>
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_note">
								<?php echo $labels["salespay_pay_note"];?>
							</label>
							<div class="controls">
								<textarea name="salespay[salespay_pay_note]" id="salespay_pay_note" style="width:400px" rows="6"><?php if(isset($salespay_data["salespay_pay_note"])){echo $salespay_data["salespay_pay_note"];}?></textarea>
							</div>
						</div>


		<!-- 按钮区域 start -->
		<!--
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		-->
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->