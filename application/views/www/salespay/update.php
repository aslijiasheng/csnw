<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						入款项新建页面
						<span class="mini-title">
						salespay
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/salespay/')?>">
<i class="icon-undo"></i>
返回入款项列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php if(!isset($_GET['type_id'])){echo site_url('www/salespay/update?salespay_id='.$_GET['salespay_id']);}else{echo site_url('www/salespay/update?salespay_id='.$_GET['salespay_id'].'&type_id='.$_GET['type_id']);}?>" method="post">
		<?php if(0==1){?>
		 	<?php if(!isset($_GET['type_id'])){ ?>
		 	  
						<div class="control-group">
							<label class="control-label required" for="salespay_create_time">
								<?php echo $labels["salespay_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="salespay_create_time">
	<input size="16" type="text" name="salespay[salespay_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['salespay_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['salespay_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#salespay_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_order_id">
								<?php echo $labels["salespay_order_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="salespay[salespay_order_id]" id="salespay_order_id" value_id ="<?php if(isset($id_aData['salespay_order_id_arr']['order_id'])){echo $id_aData['salespay_order_id_arr']['order_id']; } ?>" value="<?php if(isset($id_aData['salespay_order_id_arr']['order_name'])){echo $id_aData['salespay_order_id_arr']['order_name'];} ?>" url="<?php echo site_url('www/order/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_create_user">
								<?php echo $labels["salespay_create_user"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="salespay[salespay_create_user]" id="salespay_create_user" value_id ="<?php if(isset($id_aData['salespay_create_user_arr']['user_id'])){echo $id_aData['salespay_create_user_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['salespay_create_user_arr']['user_name'])){echo $id_aData['salespay_create_user_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_amount">
								<?php echo $labels["salespay_pay_amount"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="salespay[salespay_pay_amount]" value="<?php echo $id_aData['salespay_pay_amount'] ?>" id="salespay_pay_amount">
<?php }else{ ?>
<input type="text" maxlength="128" name="salespay[salespay_pay_amount]" id="salespay_pay_amount">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_date">
								<?php echo $labels["salespay_pay_date"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="salespay_pay_date">
	<input size="16" type="text" name="salespay[salespay_pay_date]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['salespay_pay_date_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['salespay_pay_date_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#salespay_pay_date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_method">
								<?php echo $labels["salespay_pay_method"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="salespay[salespay_pay_method]">
<?php foreach($salespay_pay_method_enum as $pay_method_v){ ?>
   <?php if(isset($id_aData) && $pay_method_v['enum_key']==$id_aData['salespay_pay_method_arr']['enum_key'] ){ ?>
     <option value="<?php echo $pay_method_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $pay_method_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $pay_method_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_info">
								<?php echo $labels["salespay_pay_info"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="salespay[salespay_pay_info]" value="<?php echo $id_aData['salespay_pay_info'] ?>" id="salespay_pay_info">
<?php }else{ ?>
<input type="text" maxlength="128" name="salespay[salespay_pay_info]" id="salespay_pay_info">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
		 	<?php }else{ ?>
		    <?php switch ($_GET["type_id"]){ 
							case -1:
							break;
						?>
						
						<?php default: ?>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_create_time">
								<?php echo $labels["salespay_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="salespay_create_time">
	<input size="16" type="text" name="salespay[salespay_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['salespay_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['salespay_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#salespay_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_order_id">
								<?php echo $labels["salespay_order_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="salespay[salespay_order_id]" id="salespay_order_id" value_id ="<?php if(isset($id_aData['salespay_order_id_arr']['order_id'])){echo $id_aData['salespay_order_id_arr']['order_id']; } ?>" value="<?php if(isset($id_aData['salespay_order_id_arr']['order_name'])){echo $id_aData['salespay_order_id_arr']['order_name'];} ?>" url="<?php echo site_url('www/order/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_create_user">
								<?php echo $labels["salespay_create_user"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="salespay[salespay_create_user]" id="salespay_create_user" value_id ="<?php if(isset($id_aData['salespay_create_user_arr']['user_id'])){echo $id_aData['salespay_create_user_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['salespay_create_user_arr']['user_name'])){echo $id_aData['salespay_create_user_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_amount">
								<?php echo $labels["salespay_pay_amount"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="salespay[salespay_pay_amount]" value="<?php echo $id_aData['salespay_pay_amount'] ?>" id="salespay_pay_amount">
<?php }else{ ?>
<input type="text" maxlength="128" name="salespay[salespay_pay_amount]" id="salespay_pay_amount">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_date">
								<?php echo $labels["salespay_pay_date"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="salespay_pay_date">
	<input size="16" type="text" name="salespay[salespay_pay_date]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['salespay_pay_date_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['salespay_pay_date_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#salespay_pay_date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_method">
								<?php echo $labels["salespay_pay_method"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="salespay[salespay_pay_method]">
<?php foreach($salespay_pay_method_enum as $pay_method_v){ ?>
   <?php if(isset($id_aData) && $pay_method_v['enum_key']==$id_aData['salespay_pay_method_arr']['enum_key'] ){ ?>
     <option value="<?php echo $pay_method_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $pay_method_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $pay_method_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_info">
								<?php echo $labels["salespay_pay_info"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="salespay[salespay_pay_info]" value="<?php echo $id_aData['salespay_pay_info'] ?>" id="salespay_pay_info">
<?php }else{ ?>
<input type="text" maxlength="128" name="salespay[salespay_pay_info]" id="salespay_pay_info">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
					<?php }?>
                <?php }}else{ ?>
                  
						<div class="control-group">
							<label class="control-label required" for="salespay_create_time">
								<?php echo $labels["salespay_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="salespay_create_time">
	<input size="16" type="text" name="salespay[salespay_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['salespay_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['salespay_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#salespay_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_order_id">
								<?php echo $labels["salespay_order_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="salespay[salespay_order_id]" id="salespay_order_id" value_id ="<?php if(isset($id_aData['salespay_order_id_arr']['order_id'])){echo $id_aData['salespay_order_id_arr']['order_id']; } ?>" value="<?php if(isset($id_aData['salespay_order_id_arr']['order_name'])){echo $id_aData['salespay_order_id_arr']['order_name'];} ?>" url="<?php echo site_url('www/order/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_create_user">
								<?php echo $labels["salespay_create_user"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="salespay[salespay_create_user]" id="salespay_create_user" value_id ="<?php if(isset($id_aData['salespay_create_user_arr']['user_id'])){echo $id_aData['salespay_create_user_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['salespay_create_user_arr']['user_name'])){echo $id_aData['salespay_create_user_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_amount">
								<?php echo $labels["salespay_pay_amount"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="salespay[salespay_pay_amount]" value="<?php echo $id_aData['salespay_pay_amount'] ?>" id="salespay_pay_amount">
<?php }else{ ?>
<input type="text" maxlength="128" name="salespay[salespay_pay_amount]" id="salespay_pay_amount">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_date">
								<?php echo $labels["salespay_pay_date"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="salespay_pay_date">
	<input size="16" type="text" name="salespay[salespay_pay_date]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['salespay_pay_date_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['salespay_pay_date_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#salespay_pay_date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_method">
								<?php echo $labels["salespay_pay_method"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="salespay[salespay_pay_method]">
<?php foreach($salespay_pay_method_enum as $pay_method_v){ ?>
   <?php if(isset($id_aData) && $pay_method_v['enum_key']==$id_aData['salespay_pay_method_arr']['enum_key'] ){ ?>
     <option value="<?php echo $pay_method_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $pay_method_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $pay_method_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_info">
								<?php echo $labels["salespay_pay_info"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="salespay[salespay_pay_info]" value="<?php echo $id_aData['salespay_pay_info'] ?>" id="salespay_pay_info">
<?php }else{ ?>
<input type="text" maxlength="128" name="salespay[salespay_pay_info]" id="salespay_pay_info">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
                 <?php }?>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>