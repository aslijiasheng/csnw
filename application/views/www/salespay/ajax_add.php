<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="salespay-form" action="<?php echo site_url('www/salespay/add');?>" method="post">
		<!-- 用一个隐藏文本框来存order_id -->
		<input size="16" type="hidden" name="salespay[salespay_order_id]" value='<?php echo $order_data['order_id'];?>'>
		<input size="16" type="hidden" name="order_finance" value='<?php echo $order_data['order_finance'];?>'>
		<input type="hidden" name="order_finance_department" value="<?php echo $order_data['order_finance_arr']['user_department']; ?>">
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td width="10%">订单总金额</td>
								<td width="10%"><?php echo $order_data['order_amount'];?></td>
								<td width="10%">可到帐金额</td>
								<td width="10%" id='kdz_amount'><?php echo $kdz_amount;?></td>
							<tr>
							</tr>
								<td width="10%">已确认到帐</td>
								<td width="10%"><?php echo $ydz_amount;?></td>
								<td width="10%">未确认到帐</td>
								<td width="10%"><?php echo $wdz_amount;?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<!--
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单总金额
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $order_data['order_amount'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				已确认到帐
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $order_data['order_amount'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				未确认到帐
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $order_data['order_amount'];?>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				可到帐金额
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<?php echo $order_data['order_amount'];?>
				</div>
			</div>
		</div>
		-->
		<div class="control-group">
			<label class="control-label required"  for="salespay_pay_amount">
				本次入账金额
			</label>
			<div class="controls">
				<input size="16" id="salespay_pay_amount" type="text" name="salespay[salespay_pay_amount]" required="required">
				<span class="text-error">(必填)</span>
			</div>
		</div>



						<div class="control-group">
							<label class="control-label required" for="salespay_pay_date">
								<?php echo $labels["salespay_pay_date"];?>
							</label>
							<div class="controls">
								<div class="input-append date form_date">
									<input size="16" type="text" name="salespay[salespay_pay_date]" style="width: 180px;" value="" required="required" id="salespay_pay_date">
									<span class="add-on"><i class="icon-clock"></i></span>
								</div>
								<span class="text-error">(必填)</span>
<script type="text/javascript">
$(document).ready(function () {
	$('#salespay_pay_date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		format:'yyyy-mm-dd'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="salespay_pay_method">
								<?php echo $labels["salespay_pay_method"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="salespay_pay_method" name="salespay[salespay_pay_method]">
									<?php foreach($salespay_pay_method_enum as $pay_method_v){ ?>
									<option value="<?php echo $pay_method_v['enum_key'];?>">
										<?php echo $pay_method_v['enum_name'];?>
									</option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
<div id="remit_info" class="well payinfo">
	<div class="form-horizontal">
		<div class="control-group">
			<label class="control-label" style="width: 120px;">汇款账户：</label>
			<div class="controls" style="margin-left: 120px;">
				<select id="country" name="payinfo[remit_bank]">
					<?php foreach($remit_bank_enum as $remit_bank_v){ ?>
					<option value="<?php echo $remit_bank_v['enum_key'];?>">
						<?php echo $remit_bank_v['enum_name'];?>
					</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="remit_person" style="width: 120px;">汇款人姓名：</label>
			<div class="controls"  style="margin-left: 120px;">
				<input type="text" name="payinfo[remit_person]" id="remit_person"  required="required" class="isrequired">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;" for="remit_account">汇款人银行账号：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="payinfo[remit_account]" id="remit_account"  required="required" class="isrequired">
			</div>
		</div>
	</div>
</div>
<div id="alipay_info" class="well payinfo"  style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;">入帐支付宝帐号：</label>
		<div class="controls" style="margin-left: 120px;">
			<select id="country" name="payinfo[alipay_bank]">
				<?php foreach($alipay_bank_enum as $alipay_bank_v){ ?>
				<option value="<?php echo $alipay_bank_v['enum_key'];?>">
					<?php echo $alipay_bank_v['enum_name'];?>
				</option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="alipay_order">交易号：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="payinfo[alipay_order]" id="alipay_order"  required="required" class="isrequired">
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="alipay_account">支付宝账号：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="payinfo[alipay_account]" id="alipay_account" required="required"  class="isrequired">
		</div>
	</div>
</div>
<div id="check_info" class="well payinfo" style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="check_name">支票名称：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="payinfo[check_name]" id="check_name" required="required"  class="isrequired">
		</div>
	</div>
</div>
<div id="cash_info" class="payinfo" style="display:none;"> </div>

							</div>
						</div>
<script type="text/javascript">
$(document).ready(function () {
		$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');
	$('#salespay_pay_method').change(function(){
		//获取选择的value用作判断
		enumkey = $(this).val();
		if(enumkey==1001){

			$(".payinfo").hide();
			$("#remit_info").show();
			$("#remit_info").find('.isrequired').attr('required','required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1002){
			$(".payinfo").hide();
			$("#alipay_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').attr('required','required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1003){
			$(".payinfo").hide();
			$("#check_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').attr('required','required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1004){
			$(".payinfo").hide();
			$("#cash_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').attr('required','required');
		}
	});
});
</script>
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_note">
								<?php echo $labels["salespay_pay_note"];?>
							</label>
							<div class="controls">
								<textarea name="salespay[salespay_pay_note]" id="salespay_pay_note" style="width:400px" rows="6"></textarea>
							</div>
						</div>


		<!-- 按钮区域 start -->
		<!--
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		-->
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->

<script type="text/javascript">

</script>