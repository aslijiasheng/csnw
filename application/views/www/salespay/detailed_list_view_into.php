	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="75">操作</th>
				<?php if($orderData['type_id']==10 or $orderData['type_id']==8){?>
				<th>款项类型</th>
				<?php }?>
				<th>款项类型名称</th>
				<th>入账日期</th>
				<th>确认到帐日期</th>

				<th><?php echo $labels['salespay_pay_method'];?></th>
				
				<th><?php
					if($orderData['type_id']==10 or $orderData['type_id']==8){
						if($orderData['order_deposit_paytype']==1001 or $orderData['order_deposit_paytype']==1002 or $orderData['order_deposit_paytype']==1003){
							echo '入账金额';
						}
						if($orderData['order_deposit_paytype']==1004 or $orderData['order_deposit_paytype']==1005  or $orderData['type_id']==8){
							echo '出账金额';
						}
					}else{
						echo $labels['salespay_pay_amount'];
					}
				?>

				<th><?php echo $labels['salespay_status'];?></th>

				<th><?php echo $labels['salespay_create_user'];?></th>

				<th><?php echo $labels['salespay_create_time'];?></th>

			</tr>
		</thead>
		<tbody>
		<?php foreach($listData as $listData_value){?>
			<tr>
				<td style="font-size:16px;">
					<div class="btn-toolbar">
						<div class="btn-group">
						<?php  $user_auth = $this->user->user_auth($this->session->userdata('user_id')); ?>
						    	<a class="btn btn-small btn-primary salespay_view" id="<?php  echo $listData_value['salespay_id'];?>" <?php if(!in_array('salespay_view',$user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>查看</a>
								<button class="btn btn-small dropdown-toggle btn-primary" data-toggle="dropdown" <?php if(!in_array('salespay_view', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
									<span class="caret"></span>
								</button>
							<ul class="dropdown-menu">
								<li <?php if(!in_array('confirm_salespay',$user_auth['activity_auth_arr']) or $listData_value['salespay_status'] !=1001){ echo 'style="display:none;"';} ?>>
									<a id="<?php  echo $listData_value['salespay_id'];?>" class="confirm_salespay">确认到帐</a>
								</li>
								<li <?php if(!in_array('salespay_edit', $user_auth['activity_auth_arr']) or $listData_value['salespay_status'] !=1003){ echo 'style="display:none;"';} ?> >
									<a id="<?php  echo $listData_value['salespay_id'];?>" class="edit_salespay">编辑</a>
								</li>
								<li <?php if(!in_array('salespay_del', $user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
									<a id="<?php  echo $listData_value['salespay_id'];?>" class="del_salespay">删除</a>
								</li>
							</ul>
						</div>
					</div>
				</td>
					<?php if($orderData['type_id']==10 or $orderData['type_id']==8){?>
					<td>
						<?php 
							if ($listData_value['salespay_money_type']!=0 and $listData_value['salespay_money_type']!=""){
								echo $listData_value['salespay_money_type_arr']['enum_name'];
							}else{
								echo "&nbsp";
							}
						?>
					</td>
					<?php }?>
					<td>
						<?php 
							if ($listData_value['salespay_money_type_name']!=0 and $listData_value['salespay_money_type_name']!=""){
								echo $listData_value['salespay_money_type_name_arr']['enum_name'];
							}else{
								echo "&nbsp";
							}
						?>
					</td>
					<td>
					<?php if($listData_value['salespay_pay_date']!="" and $listData_value['salespay_pay_date']!=0){echo date('Y-m-d',strtotime($listData_value['salespay_pay_date']));}?>
					</td>

					<td>
					<?php if($listData_value['salespay_sp_date']!="" and $listData_value['salespay_sp_date']!=0){echo date('Y-m-d',strtotime($listData_value['salespay_sp_date']));}?>
					</td>

					<td>
						<?php 
							if ($listData_value['salespay_pay_method']!=0 and $listData_value['salespay_pay_method']!=""){
								echo $listData_value['salespay_pay_method_arr']['enum_name'];
							}else{
								echo "&nbsp";
							}
						?>
					</td>

					<td>
					<?php  echo $listData_value['salespay_pay_amount'];?>
					</td>

					<td>
						<?php
							if ($listData_value['salespay_status']!=0 and $listData_value['salespay_status']!=""){
								echo $listData_value['salespay_status_arr']['enum_name'];
							}else{
								echo "&nbsp";
							}
						?>
					</td>

					<td>
							<?php
			if ($listData_value['salespay_create_user']!=0 and $listData_value['salespay_create_user']!=""){
				echo $listData_value['salespay_create_user_arr']['user_name'];
			}else{
				echo "&nbsp";
			}
		?>
					</td>

					<td>
					<?php  echo $listData_value['salespay_create_time'];?>
					</td>

			</tr>
		<?php }?>
		</tbody>
	</table>


<script type="text/javascript">
$(document).ready(function() {
	//查看
	$(".salespay_view").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'salespay_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/salespay/ajax_view'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'入款项详细信息',
			width:800,
			height:450,
			buttons:''
		});
		$('#operation_dialog').dialog('open');
	});

	//确认到帐
	$(".confirm_salespay").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'salespay_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/salespay/ajax_confirm_salespay'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'确认款项',
			width:800,
			height:450,
			buttons: [{
				text:"确认款项",
				Class:"btn btn-primary",
				click: function(){
					//alert('确认到帐');
					$this = $(this);
					formdata = $("#confirm-form").serializeArray();
					formdata[formdata.length]={"name":"salespay[salespay_status]","value":"1002"}; //添加1个参数
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//dump_obj(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("已确认款项");
								$this.dialog("close");
								$('#salespay_into').ajaxHtml(); //再次加载1次页面
								$('#salespay_out').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/salespay/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"款项不成功",
				Class:"btn btn-success",
				click: function(){
					//alert('到帐不成功');
					$this = $(this);
					formdata = $("#confirm-form").serializeArray();
					formdata[formdata.length]={"name":"salespay[salespay_status]","value":"1003"}; //添加1个参数
					//alert(formdata);
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("款项不成功");
								$this.dialog("close");
								$('#salespay_into').ajaxHtml(); //再次加载1次页面
								$('#salespay_out').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/salespay/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了确认款项");
					$(this).dialog("close");
				}
			}]

		});
		$('#operation_dialog').dialog('open');
	});

	$(".edit_salespay").click(function(){
		id=$(this).attr('id');
		$.ajax({
			'type':'get',
			'data':'salespay_id='+id,
			'success':function(data){
				$('#operation_dialog').html(data);
			},
			'url':'<?php echo site_url('www/salespay/ajax_edit'); ?>',
			'cache':false
		});
		$('#operation_dialog').dialog({
			modal: true,
			title:'修改入款项',
			width:800,
			height:450,
			buttons: [{
				text:"保存",
				Class:"btn btn-primary",
				click: function(){
					$this = $(this);
					formdata = $("#salespay-form").serializeArray();
					var status = leeConditionJudgment(formdata);
					 if(status == false) return;
					//本次入账金额
					$pay_amount = $('[name="salespay[salespay_pay_amount]"]').val();
					//可入账金额
					$krz_amount = $('#kdz_amount').html();
					$.ajax({
						'type':'post',
						'data':formdata,
						'success':function(data){
							if(data==1){
								alertify.alert("修改成功");
								$this.dialog("close");
								$('#salespay_into').ajaxHtml(); //再次加载1次页面
								$('#salespay_out').ajaxHtml(); //再次加载1次页面
								$('#operation_log').ajaxHtml();
							}else{
								alert('失败'+data);
							}
						},
						'url':'<?php echo site_url('www/salespay/ajax_update_post'); ?>',
						'async':false
					});
				}
			},{
				text:"取消",
				Class:"btn bottom-margin",
				click: function(){
					alertify.error("你取消了修改入款项");
					$(this).dialog("close");
				}
			}]
		});
		$('#operation_dialog').dialog('open');
	});

	$(".del_salespay").click(function(){
		id=$(this).attr('id');
		alertify.confirm("是否删除这个入款项", function (e) {
			if (e) {
				$.ajax({
					'type':'post',
					'data':'salespay_id='+id+'&order_id='+<?php echo $_GET['order_id'] ?>,
					'success':function(data){
						if(data==1){
							alertify.success("删除成功");
							$('#salespay_into').ajaxHtml(); //再次加载1次页面
							$('#salespay_out').ajaxHtml(); //再次加载1次页面
							$('#operation_log').ajaxHtml();

						}else{
							alert('失败'+data);
						}
					},
					'url':'<?php echo site_url('www/salespay/ajax_del_post'); ?>',
					'async':false
				});
			} else {
				alertify.error("你放弃了删除");
			}
		});
		//return false;
	});


});

</script>