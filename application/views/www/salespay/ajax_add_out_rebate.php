<!--lee 内容部分 start-->
<!--这个是专门给新建返点款项的页面-->
<div class="grid-view">
	<form class="form-horizontal" id="salespay-form" method="post">
		<!-- 用一个隐藏文本框来存order_id -->
		<input size="16" type="hidden" name="salespay[salespay_order_id]" value='<?php echo $order_data['order_id'];?>'>
		<input size="16" type="hidden" name="order_finance" value='<?php echo $order_data['order_finance'];?>'>
		<input type="hidden" name="order_finance_department" value="<?php echo $order_data['order_finance_arr']['user_department']; ?>">
		<div class="control-group">
			<label class="control-label required" for="salespay_create_time">
				订单信息
				<span class="required"></span>
			</label>
			<div class="controls">
				<div class="group-text">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td width="10%">订单总金额</td>
								<td width="10%"><?php echo $order_data['order_amount'];?></td>
								<td width="10%">返点总额</td>
								<td width="10%"><?php echo $order_data['order_rebate_amount'];?></td>
							<tr>
							</tr>
								<td>已确认返点</td>
								<td><?php echo $yfd_amount;?></td>
								<td>未确认返点</td>
								<td><?php echo $wfd_amount;?></td>
							</tr>
							</tr>
								<td>可返款金额</td>
								<td colspan="3" id="kfd_amount"><?php echo $kfd_amount;?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required">
				款项类型名称
			</label>
			<div class="controls">
				<input type="hidden" name="salespay[salespay_money_type]" id='salespay_money_type' value="1002">
				<input type="hidden" name="salespay[salespay_money_type_name]" id='salespay_money_type_name' value="1002">
				<input type="text" value="返点款项" disabled>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_reviewer">
				业务负责人
			</label>
			<div class="controls">
<input type="text" name="salespay[salespay_reviewer]" id="salespay_reviewer" value_id ="" value="" required="required" title="业务负责人">
<script type="text/javascript">
$(document).ready(function () {
	$('#salespay_reviewer').leeQuote({
		url:'<?php echo site_url('www/user/ajax_list?tag_name=salespay_reviewer');?>',
		title:'选择主管人员',
		data:{
			'rel_role_id':2,
		}
	});
});
</script>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label required" for="salespay_rebate_account">
				选择返点的客户
				<span class="required"></span>
			</label>
			<div class="controls">
				<input type="text" maxlength="128" name="salespay[salespay_rebate_account]" required="required" id="salespay_rebate_account" value_id ="" value="" url="<?php echo site_url('www/account/ajax_list') ?>">
			</div>
<script type="text/javascript">
$(document).ready(function () {
	//这里的引用需要触发一下
	$('#salespay_rebate_account').leeQuote({
		url:'<?php echo site_url('www/account/ajax_list?tag_name=salespay_rebate_account');?>',
		title:'选择返点客户',
	});
});
</script>
		</div>

		<div class="control-group">
			<label class="control-label required"  for="salespay_pay_amount">
				本次返点金额
			</label>
			<div class="controls">
				<input size="16" id="salespay_pay_amount" type="text" name="salespay[salespay_pay_amount]" required="required">
				<span class="text-error">(必填)</span>
			</div>
		</div>

						<div class="control-group">
							<label class="control-label required" for="salespay_pay_method">
								支付方式
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="salespay_pay_method" name="salespay[salespay_pay_method]">
									<?php foreach($salespay_pay_method_enum as $pay_method_v){ ?>
									<option value="<?php echo $pay_method_v['enum_key'];?>">
										<?php echo $pay_method_v['enum_name'];?>
									</option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
<div id="remit_info" class="well payinfo">
	<div class="form-horizontal">
		<div class="control-group">
			<label class="control-label" style="width: 120px;" for='salespay_account_bank_type'>账户类型：</label>
			<div class="controls" style="margin-left: 120px;">
				<label class="radio inline">
					<input type="radio" name="salespay[salespay_account_bank_type]" id='salespay_account_bank_type' value="1001" checked>
					企业
				</label>
				<label class="radio inline">
					<input type="radio" name="salespay[salespay_account_bank_type]" id='salespay_account_bank_type' value="1002">
					个人
				</label>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;" for="salespay_account_bank_account">客户银行账户：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="salespay[salespay_account_bank_account]" id="salespay_account_bank_account" required="required" class="isrequired">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;" for="salespay_account_bank_account_name">账户姓名：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="salespay[salespay_account_bank_account_name]" id="salespay_account_bank_account_name" required="required" class="isrequired">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width: 120px;" for="salespay_account_bank_name">开户银行名称：</label>
			<div class="controls" style="margin-left: 120px;">
				<input type="text" name="salespay[salespay_account_bank_name]" id='salespay_account_bank_name'>
			</div>
		</div>
	</div>
</div>
<div id="alipay_info" class="well payinfo"  style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="salespay_alipay_order">交易号：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="salespay[salespay_alipay_order]" id="salespay_alipay_order"  required="required" class="isrequired">
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="salespay_account_alipay_account">支付宝账号：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="salespay[salespay_account_alipay_account]" id="salespay_account_alipay_account" required="required"  class="isrequired">
		</div>
	</div>
</div>
<div id="check_info" class="well payinfo" style="display:none;">
	<div class="control-group">
		<label class="control-label" style="width: 120px;" for="salespay_check_name">支票名称：</label>
		<div class="controls" style="margin-left: 120px;">
			<input type="text" name="salespay[salespay_check_name]" id="salespay_check_name" required="required"  class="isrequired">
		</div>
	</div>
</div>
<div id="cash_info" class="payinfo" style="display:none;"> </div>

							</div>
						</div>
<script type="text/javascript">
$(document).ready(function () {
		$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');
	$('#salespay_pay_method').change(function(){
		//获取选择的value用作判断
		enumkey = $(this).val();
		if(enumkey==1001){

			$(".payinfo").hide();
			$("#remit_info").show();
			$("#remit_info").find('.isrequired').attr('required','required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1002){
			$(".payinfo").hide();
			$("#alipay_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').attr('required','required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1003){
			$(".payinfo").hide();
			$("#check_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').attr('required','required');
			$("#cash_info").find('.isrequired').removeAttr('required');
		}
		if(enumkey==1004){
			$(".payinfo").hide();
			$("#cash_info").show();
			$("#remit_info").find('.isrequired').removeAttr('required');
			$("#alipay_info").find('.isrequired').removeAttr('required');
			$("#check_info").find('.isrequired').removeAttr('required');
			$("#cash_info").find('.isrequired').attr('required','required');
		}
	});
});
</script>
						<div class="control-group">
							<label class="control-label required" for="salespay_rebate_person">
								返点经手人姓名
							</label>
							<div class="controls">
								<input size="16" type="text" name="salespay[salespay_rebate_person]">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label required" for="salespay_pay_note">
								返点备注
							</label>
							<div class="controls">
								<textarea name="salespay[salespay_pay_note]" id="salespay_pay_note" style="width:400px" rows="6"></textarea>
							</div>
						</div>


		<!-- 按钮区域 start -->
		<!--
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		-->
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->

<script type="text/javascript">

</script>