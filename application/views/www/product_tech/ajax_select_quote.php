<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th><?php echo $labels['product_tech_name']; ?></th>
            <th><?php echo $labels['product_tech_code']; ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listData as $listData_value) { ?>
        <tr>
                <td style="font-size:16px;">
                    <input type="radio" name="optionsRadios" value="<?php echo $listData_value['product_tech_id']; ?>" name_val="<?php echo $listData_value['product_tech_name']; ?>"<?php
                           if ($_GET["hidden_v"] != '') {
                               $hidden_id = explode('?', $_GET["hidden_v"]);
                        $hidden_id = $hidden_id[0];
                        if ($hidden_id == $listData_value['product_tech_id']) {
                            echo "checked='checked'";
                        }
                    }
                    ?> >
                        </td>
                    <td>
                        <?php echo $listData_value['product_tech_name']; ?>
                    </td>
                    <td>
                        <?php echo $listData_value['product_tech_code']; ?>
                    </td>
                    <td>
                        <?php echo $listData_value['product_tech_desc']; ?>
                    </td>
                </tr>
            <?php } ?>
    </tbody>
</table>
<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber; ?>条</div>
<input type='hidden' id='totalNumber' value="<?php echo $totalNumber; ?>">
</div>