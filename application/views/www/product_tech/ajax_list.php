<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						<?php if(isset($_GET['type_id'])){
							if(isset($type_arr[$_GET['type_id']]['type_name'])){
								echo $type_arr[$_GET['type_id']]['type_name'];
							}else{
								echo '技术产品';
							}
						}else{
							echo '技术产品';
						}
						?>
						<span class="mini-title">
						product_tech
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">

						</div>
						<div id="divmessagelist">

<div class="grid-view">
	<div id="product_tech_list"></div>
<div>
<script type="text/javascript">
$(document).ready(function () {
	$selectAttr=[

		{'value':'name','txt':'产品名称'},

		{'value':'code','txt':'产品编号'},

		{'value':'desc','txt':'产品描述'},

	];

	$("#product_tech_list").leeDataTable({
		selectAttr:$selectAttr, //简单查询的查询属性
		url:"<?php echo site_url('www/product_tech/ajax_select_quote?hidden_v='.$_GET['hidden_v']); ?>", //ajax查询的地址
		perNumber:20, //每页显示多少条数据

	});
});

</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

