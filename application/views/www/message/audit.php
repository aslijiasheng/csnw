<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						待查账
						<span class="mini-title">
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">

						</div>
						<div id="divmessagelist">

							<div class="grid-view">
								<div id="message_list">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>订单编号</th>
												<th>订单总金额</th>
												<th>客户名称</th>
												<th>销售人员</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($orders as $v): ?>
												<tr>
													<td><a href="<?php echo site_url("www/order/view?order_id=" . $v['order_id'] . "&active=salespay_into"); ?>" target="_blank"><?php echo $v['order_number']; ?></a></td>
													<td><?php echo $v['order_amount']; ?></td>
													<td><?php echo $v['order_account_arr']['account_name']; ?></td>
													<td><?php echo $v['order_owner_arr']['user_name']; ?></td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>

								</div>
								<div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
