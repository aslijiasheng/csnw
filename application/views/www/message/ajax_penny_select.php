<script src="<?php echo base_url() . 'style/admin/js/message_operation.js' ?>"></script>
<?php
if (isset($_GET['module'])) {
	$tem = $_GET['module'];
	$module = explode('?', $tem);
	$module = $module[0];
} else {
	$module = '';
} if ($module != 'applyopen') {
	?>
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>操作</th>
				<th>订单编号</th>
				<th>款项名称</th>
				<th>转入方</th>
				<th>转出方</th>
				<th>转入金额</th>
				<th>所属子公司</th>
				<th>待办类型</th>
				<th>待办状态</th>
			</tr>
		</thead>
		<tbody>

			<?php foreach ($listData as $listData_value) { ?>
				<tr>
					<td>
						<?php if ($listData_value['message_status'] == 1001) { ?>
							<a id="<?php echo $listData_value['message_module_id']; ?>"  onclick='javascript:<?php echo $listData_value['type_name'] ?>(this.id, "<?php echo site_url($listData_value['message_url']) ?>",<?php echo json_encode($sel_data) ?>)' class="badge badge-info" >执行</a>
						<?php } else { ?>
						<?php } ?>
					</td>

					</td>

					<td>
						<?php echo $listData_value['order_number'] ?>
					</td>

					<td>
						<?php echo $listData_value['order_transfer_name'] ?>
					</td>

					<td>
						<?php echo $listData_value['order_change_into']; ?>
					</td>

					<td>
						<?php echo $listData_value['order_change_out']; ?>
					</td>

					<td>
						<?php echo $listData_value['order_change_into_money']; ?>
					</td>
					
					<td>
						<?php echo $listData_value['order_subsidiary'];?>
					</td>

					<td>
						<?php echo $listData_value['message_type_arr']['enum_name']; ?>
					</td>

					<td>
						<?php echo $listData_value['message_status_arr']['enum_name']; ?>
					</td>



				</tr>
			<?php } ?>
		</tbody>

	</table>
<?php } else { ?>
	<table class="table table-bordered ">
		<thead>
			<tr>
				<th width="100">关联订单ID</th>
				<th width="200">产品名称</th>
				<th width="200">产品编码</th>
				<th width="200">产品原价</th>
				<th width="200">折后价</th>
				<th width="200">开通状态</th>

				<th width="75">操作</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($apply_data as $kk => $vv) { ?>
				<tr>
					<td><a href="<?php echo site_url('www/order/view?order_id=' . $vv['order_d_order_id']) ?>"><?php echo $vv['order_d_order_id']; ?></a></td>
					<td><?php echo $vv['order_d_product_basic_name']; ?></td>
					<td><?php echo $vv['order_d_product_basic_code']; ?></td>
					<td><?php echo $vv['order_d_product_basic_primecost']; ?></td>
					<td><?php echo $vv['order_d_product_basic_disc']; ?></td>
					<td><?php
						if ($vv['order_d_product_basic_style'] == 1002) {
							echo '已申请';
						} else if ($vv['order_d_product_basic_style'] == 1003) {
							echo '已开通';
						}
						?></td>
					<td>
						<?php if ($vv['order_d_product_basic_style'] == 1001 or $vv['order_d_product_basic_style'] == "" or $vv['order_d_product_basic_style'] == 0) { ?>
							<a class="badge badge-info ApplyOpen" data-id='<?php echo $vv['order_d_id']; ?>' > 申请开通 </a>
							<br>
						<?php } ?>
						<?php if ($vv['order_d_product_basic_style'] == 1002) { ?>
							<a class="badge badge-info ConfirmOpen" data-id='<?php echo $vv['order_d_id']; ?>'  > 确认开通 </a>
							<br>
						<?php } ?>
						<?php if ($vv['order_d_product_basic_style'] == 1002) { ?>
							<a class="badge badge-info BelowLineOpen" data-id='<?php echo $vv['order_d_id']; ?>' onclick='javascript:BelowLineOpen("<?php echo $vv['order_d_id']; ?>",<?php echo json_encode($sel_data) ?>)'> 线下开通 </a>
							<br>
						<?php } ?>
						<a class="badge badge-info SeeParams" data-id='<?php echo $vv['order_d_id']; ?>'  > 查看参数 </a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<script type="text/javascript">
		$(document).ready(function() {
	//申请开通

	//确认开通
			$(".ConfirmOpen").click(function() {
	//alert('确认开通');
				id = $(this).attr('data-id');
				$.ajax({
					'type': 'get',
					'data': 'id=' + id,
					'success': function(data) {
						$('#operation_dialog').html(data);
					},
					'url': '<?php echo site_url('www/applyopen/ajax_confirm_open'); ?>',
					'cache': false
				});
				$('#operation_dialog').dialog({
					modal: true,
					title: '确认开通',
					width: 800,
					height: 450,
					buttons: [{
							text: "确认开通",
							Class: "btn btn-primary",
							click: function() {
								$this = $("#operation_dialog");
								formdata = $("#ConfirmOpen-form").serializeArray();
								$.ajax({
									'type': 'post',
									'data': formdata,
									'success': function(data) {
										if (data == 1) {
											alertify.success("提交开通成功");
											$this.dialog("close");
											$("#home").ajaxHtml();
										} else {
											alert('失败' + data);
										}
									},
									'url': '<?php echo site_url('www/applyopen/ajax_confirm_post'); ?>',
									'async': false
								});
							}
						}, {
							text: "取消",
							Class: "btn bottom-margin",
							click: function() {
								alertify.error("你取消了确认开通");
								$(this).dialog("close");
							}
						}]
				});
				$('#operation_dialog').dialog('open');
			});
	//查看参数
			$(".SeeParams").click(function() {
	//alert('查看参数');
				id = $(this).attr('data-id');
				$.ajax({
					'type': 'get',
					'data': 'id=' + id,
					'success': function(data) {
						$('#operation_dialog').html(data);
					},
					'url': '<?php echo site_url('www/applyopen/ajax_see_params'); ?>',
					'cache': false
				});
				$('#operation_dialog').dialog({
					modal: true,
					title: '查看参数',
					width: 800,
					height: 450,
					buttons: [{
							text: "关闭",
							Class: "btn bottom-margin",
							click: function() {
								alertify.error("你关闭了查看参数");
								$(this).dialog("close");
							}
						}]
				});
				$('#operation_dialog').dialog('open');
			});

		});
	</script>
<?php } ?>
<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber; ?>条</div>
<input type='hidden' id='totalNumber' value="<?php echo $totalNumber; ?>">
</div>
<script>
	// $(document).ready(function(){
	// 	$('a').click(function(){
	// 		salespay_view($(this).attr('id'));

	// 	})
	// })
</script>
