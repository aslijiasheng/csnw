<div class="grid-view" style="font-size:12px;">
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th width="20" style='text-align:center;'><span id='contraction_btn' class="icon-contract"></span></th>
				<th><?php echo $labels['order_d_order_product'];?></th>
				<th width="120"><?php echo $labels['order_d_order_productcode'];?></th>
				<th width="120">商品原价</th>
				<th width="120">商品折后价</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($listData as $k=>$listData_value){?>
			<tr>
				<td style='text-align:center;' leetype='contraction' leefor="#contraction_tr_<?php echo $k;?>" class='contraction_icon'><span class="icon-minus"></span></td>
				<td><?php echo $listData_value['order_d_goods_name'];?></td>
				<td><?php echo $listData_value['order_d_goods_code'];?></td>
				<td><?php echo $listData_value['order_d_order_primecost'];?></td>
				<td><?php echo $listData_value['order_d_order_disc'];?></td>
			</tr>
			<tr id='contraction_tr_<?php echo $k;?>' class='contraction_tr'>
				<td style="font-size:16px;"></td>
				<td colspan="4">
					<table class="table table-bordered ">
						<thead>
							<tr>
								<th width=""><?php echo $labels['order_d_order_product'];?></th>
								<th width="120"><?php echo $labels['order_d_order_productcode'];?></th>
								<th width="120"><?php echo $labels['order_d_order_primecost'];?></th>
								<th width="120"><?php echo $labels['order_d_order_disc'];?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($listData_value['product_arr'] as $kk=>$vv){ ?>
							<tr>
								<td><?php echo $vv['order_d_product_basic_name'];?></td>
								<td><?php echo $vv['order_d_product_basic_code'];?></td>
								<td><?php echo $vv['order_d_product_basic_primecost'];?></td>
								<td><?php echo $vv['order_d_product_basic_disc'];?></td>
							</tr>
							<?php }?>
						</tbody>
					</table>
				</td>
			</tr>
		<?php }?>
		</tbody>
	</table>
	<form class="form-horizontal" id="cancel-form" action="<?php echo site_url('www/cancel/ajax_add_post'); ?>" style="margin-top:20px;" method="post">
		<input size="16" type="hidden" name="cancel[order_id]" value='<?php echo $order_id; ?>'>
		<input size="16" type="hidden" name="cancel[node]" value='<?php echo $node; ?>'>
		<div class="control-group">
			<label class="control-label">
				<?php echo $listData['order_id']?>
			</label>
			<div class="controls">
				<textarea name="cancel[remark]" style="margin-left:40px;width:400px;height:60px;"></textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">
				备注
			</label>
			<div class="controls">
				<textarea name="cancel[remark]" style="margin-left:40px;width:400px;height:60px;"></textarea>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('[leetype="contraction"]').click(function(){
		leefor = $(this).attr('leefor');
		if($(leefor).is(":hidden")){
			//隐藏了则将它显示出来
			$(leefor).show(500);
			$(this).html('<span class="icon-minus"></span>');
		}else{
			//没隐藏则将它隐藏起来
			$(leefor).hide(500);
			$(this).html('<span class="icon-plus"></span>');
		}
	});
	$('#contraction_btn').click(function(){
		btncss=$(this).attr('class');
		if(btncss=='icon-contract'){
			$(this).attr('class','icon-expand');
			$('.contraction_tr').hide(500);
			$('.contraction_icon').html('<span class="icon-plus"></span>');
		}
		if(btncss=='icon-expand'){
			$(this).attr('class','icon-contract');
			$('.contraction_tr').show(500);
			$('.contraction_icon').html('<span class="icon-minus"></span>');
		}
	});
});
</script>