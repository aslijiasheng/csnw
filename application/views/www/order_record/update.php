<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						订单记录新建页面
						<span class="mini-title">
						order_record
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/order_record/')?>">
<i class="icon-undo"></i>
返回订单记录列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php if(!isset($_GET['type_id'])){echo site_url('www/order_record/update?order_record_id='.$_GET['order_record_id']);}else{echo site_url('www/order_record/update?order_record_id='.$_GET['order_record_id'].'&type_id='.$_GET['type_id']);}?>" method="post">
		<?php if(0==1){?>
		 	<?php if(!isset($_GET['type_id'])){ ?>
		 	  
						<div class="control-group">
							<label class="control-label required" for="order_record_order_id">
								<?php echo $labels["cancel_order_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order_record[cancel_order_id]" id="order_record_order_id" value_id ="<?php if(isset($id_aData['order_record_order_id_arr']['order_id'])){echo $id_aData['order_record_order_id_arr']['order_id']; } ?>" value="<?php if(isset($id_aData['order_record_order_id_arr']['order_name'])){echo $id_aData['order_record_order_id_arr']['order_name'];} ?>" url="<?php echo site_url('www/order/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_create_time">
								<?php echo $labels["cancel_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="order_record_create_time">
	<input size="16" type="text" name="order_record[cancel_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['order_record_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['order_record_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#order_record_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_operation">
								<?php echo $labels["order_record_operation"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($order_record_operation_enum as $operation_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $operation_v['enum_key']==$id_aData['order_record_operation_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $operation_v['enum_key']?>" name="order_record[order_record_operation]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $operation_v['enum_key']?>" name="order_record[order_record_operation]">
<?php }?>
<?php echo $operation_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_remark">
								<?php echo $labels["cancel_remark"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order_record[cancel_remark]" value="<?php echo $id_aData['cancel_remark'] ?>" id="order_record_remark">
<?php }else{ ?>
<input type="text" maxlength="128" name="order_record[cancel_remark]" id="order_record_remark">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_user_id">
								<?php echo $labels["cancel_user_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order_record[cancel_user_id]" id="order_record_user_id" value_id ="<?php if(isset($id_aData['order_record_user_id_arr']['user_id'])){echo $id_aData['order_record_user_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_record_user_id_arr']['user_name'])){echo $id_aData['order_record_user_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
		 	<?php }else{ ?>
		    <?php switch ($_GET["type_id"]){ 
							case -1:
							break;
						?>
						
						<?php default: ?>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_order_id">
								<?php echo $labels["cancel_order_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order_record[cancel_order_id]" id="order_record_order_id" value_id ="<?php if(isset($id_aData['order_record_order_id_arr']['order_id'])){echo $id_aData['order_record_order_id_arr']['order_id']; } ?>" value="<?php if(isset($id_aData['order_record_order_id_arr']['order_name'])){echo $id_aData['order_record_order_id_arr']['order_name'];} ?>" url="<?php echo site_url('www/order/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_create_time">
								<?php echo $labels["cancel_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="order_record_create_time">
	<input size="16" type="text" name="order_record[cancel_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['order_record_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['order_record_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#order_record_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_operation">
								<?php echo $labels["order_record_operation"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($order_record_operation_enum as $operation_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $operation_v['enum_key']==$id_aData['order_record_operation_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $operation_v['enum_key']?>" name="order_record[order_record_operation]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $operation_v['enum_key']?>" name="order_record[order_record_operation]">
<?php }?>
<?php echo $operation_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_remark">
								<?php echo $labels["cancel_remark"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order_record[cancel_remark]" value="<?php echo $id_aData['cancel_remark'] ?>" id="order_record_remark">
<?php }else{ ?>
<input type="text" maxlength="128" name="order_record[cancel_remark]" id="order_record_remark">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_user_id">
								<?php echo $labels["cancel_user_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order_record[cancel_user_id]" id="order_record_user_id" value_id ="<?php if(isset($id_aData['order_record_user_id_arr']['user_id'])){echo $id_aData['order_record_user_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_record_user_id_arr']['user_name'])){echo $id_aData['order_record_user_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
					<?php }?>
                <?php }}else{ ?>
                  
						<div class="control-group">
							<label class="control-label required" for="order_record_order_id">
								<?php echo $labels["cancel_order_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order_record[cancel_order_id]" id="order_record_order_id" value_id ="<?php if(isset($id_aData['order_record_order_id_arr']['order_id'])){echo $id_aData['order_record_order_id_arr']['order_id']; } ?>" value="<?php if(isset($id_aData['order_record_order_id_arr']['order_name'])){echo $id_aData['order_record_order_id_arr']['order_name'];} ?>" url="<?php echo site_url('www/order/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_create_time">
								<?php echo $labels["cancel_create_time"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="order_record_create_time">
	<input size="16" type="text" name="order_record[cancel_create_time]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['order_record_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['order_record_create_time_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#order_record_create_time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_operation">
								<?php echo $labels["order_record_operation"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($order_record_operation_enum as $operation_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $operation_v['enum_key']==$id_aData['order_record_operation_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $operation_v['enum_key']?>" name="order_record[order_record_operation]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $operation_v['enum_key']?>" name="order_record[order_record_operation]">
<?php }?>
<?php echo $operation_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_remark">
								<?php echo $labels["cancel_remark"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="order_record[cancel_remark]" value="<?php echo $id_aData['cancel_remark'] ?>" id="order_record_remark">
<?php }else{ ?>
<input type="text" maxlength="128" name="order_record[cancel_remark]" id="order_record_remark">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="order_record_user_id">
								<?php echo $labels["cancel_user_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="order_record[cancel_user_id]" id="order_record_user_id" value_id ="<?php if(isset($id_aData['order_record_user_id_arr']['user_id'])){echo $id_aData['order_record_user_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['order_record_user_id_arr']['user_name'])){echo $id_aData['order_record_user_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
                 <?php }?>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>