<?php if($orderData['type_id']==8){ //这个代表返点订单?>
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>订单编号</th>
				<th>代理商</th>
				<th>所属销售</th>
				<th>订单金额</th>
				<th>创建时间</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($listData as $listData_value){?>
			<tr>
				<td>
					<a href="<?php echo site_url('www/order/view?order_id='.$listData_value['rel_order_order_rel_order_arr']['order_id']); ?>"><?php echo $listData_value['rel_order_order_rel_order_arr']['order_number'];?></a>
				</td>
				<td>
					<?php 
						if(isset($listData_value['rel_order_order_rel_order_arr']['order_agent_arr']))echo $listData_value['rel_order_order_rel_order_arr']['order_agent_arr']['partner_name'];
					?>
				</td>
				<td>
					<?php 
						if(isset($listData_value['rel_order_order_rel_order_arr']['order_owner_arr']))echo $listData_value['rel_order_order_rel_order_arr']['order_owner_arr']['user_name'];
					?>
				</td>
				<td>
					<?php echo $listData_value['rel_order_order_rel_order_arr']['order_amount'];?>
				</td>
				<td>
					<?php echo $listData_value['rel_order_order_rel_order_arr']['order_create_time'];?>
				</td>
			</tr>
		<?php }?>
		</tbody>
	</table>
<?php }?>


<?php if($orderData['type_id']==13){ //这个代表返点订单?>
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>订单编号</th>
				<th>款项类型</th>
				<th>订单金额</th>
				<th>支付方式</th>
				<th>确认到帐时间</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($listData as $listData_value){?>
			<tr>
				<td>
					<a href="<?php echo site_url('www/order/view?order_id='.$listData_value['rel_order_order_rel_order_arr']['order_id']); ?>"><?php echo $listData_value['rel_order_order_rel_order_arr']['order_number'];?></a>
				</td>
				<td>
					<?php 
						if(isset($listData_value['rel_order_order_rel_order_arr']['order_deposit_paytype_arr']))echo $listData_value['rel_order_order_rel_order_arr']['order_deposit_paytype_arr']['enum_name'];
					?>
				</td>
				<td>
					<?php echo $listData_value['rel_order_order_rel_order_arr']['order_amount'];?>
				</td>
				<td>
					<?php 
						if(isset($listData_value['rel_order_order_rel_order_arr']['detailed']))echo $listData_value['rel_order_order_rel_order_arr']['detailed']['salespay_arr'][0]['salespay_pay_method_arr']['enum_name'];
					?>
				</td>
				<td>
					<?php 
						if(isset($listData_value['rel_order_order_rel_order_arr']['detailed']))echo $listData_value['rel_order_order_rel_order_arr']['detailed']['salespay_arr'][0]['salespay_sp_date'];
					?>
				</td>
			</tr>
		<?php }?>
		</tbody>
	</table>
<?php }?>