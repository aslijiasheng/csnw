	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="100">操作</th>
				
				
					
					<th><?php echo $labels['partner_name'];?></th>
					
					<th><?php echo $labels['partner_shopex_id'];?></th>
					
					<th><?php echo $labels['partner_department'];?></th>
					
					<th><?php echo $labels['partner_manager'];?></th>
					
					<th><?php echo $labels['partner_level'];?></th>
					
					<th><?php echo $labels['partner_disc'];?></th>
					
					<th><?php echo $labels['partner_CooperativeTime'];?></th>
					
					<th><?php echo $labels['partner_endingdate'];?></th>
					
					<th><?php echo $labels['partner_contacts'];?></th>
					
					<th><?php echo $labels['partner_email'];?></th>
					
					<th><?php echo $labels['partner_address'];?></th>
					
					<th><?php echo $labels['partner_fax'];?></th>
					
					<th><?php echo $labels['partner_region'];?></th>
					
					<th><?php echo $labels['partner_contract'];?></th>
					
				

			</tr>
		</thead>
		<tbody>
		<?php foreach($listData as $listData_value){?>
			<tr>
				<td style="font-size:16px;">
					<a href="<?php echo site_url('www/partner/view').'?partner_id='.$listData_value['partner_id'].'&type_id='.$_GET['type_id'] ?>" rel="tooltip" title="查看"><span class="icon-eye"></span></a>
				    <a href="<?php echo site_url('www/partner/update').'?partner_id='.$listData_value['partner_id'].'&type_id='.$_GET['type_id'] ?>" rel="tooltip" title="更新"><span class="icon-pencil"></span></a>
				    <a href="<?php echo site_url('www/partner/del').'?partner_id='.$listData_value['partner_id'].'&type_id='.$_GET['type_id'] ?>" rel="tooltip" title="删除"><span class="icon-remove-2"></span></a> 
				</td>
				
				
					
					<td>
					<?php  echo $listData_value['partner_name'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['partner_shopex_id'];?>
					</td>
					
					<td>
							<?php 
			if ($listData_value['partner_department']!=0 and $listData_value['partner_department']!=""){
				echo $listData_value['partner_department_arr']['department_name'];
			}else{
				echo "&nbsp";
			}
		?>
					</td>
					
					<td>
					<?php  echo $listData_value['partner_manager'];?>
					</td>
					
					<td>
					<?php echo $listData_value['partner_level_arr']['enum_name'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['partner_disc'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['partner_CooperativeTime'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['partner_endingdate'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['partner_contacts'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['partner_email'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['partner_address'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['partner_fax'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['partner_region'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['partner_contract'];?>
					</td>
					
				
				
				
			</tr>
		<?php }?>
		</tbody>
		
	</table>
	<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber;?>条</div>
	<input type='hidden' id='totalNumber' value="<?php echo $totalNumber;?>">
	</div>

