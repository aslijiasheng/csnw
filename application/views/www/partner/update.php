<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						合作伙伴新建页面
						<span class="mini-title">
						partner
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/partner/')?>">
<i class="icon-undo"></i>
返回合作伙伴列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php if(!isset($_GET['type_id'])){echo site_url('www/partner/update?partner_id='.$_GET['partner_id']);}else{echo site_url('www/partner/update?partner_id='.$_GET['partner_id'].'&type_id='.$_GET['type_id']);}?>" method="post">
		<?php if(0==1){?>
		 	<?php if(!isset($_GET['type_id'])){ ?>
		 	  
						<div class="control-group">
							<label class="control-label required" for="partner_partner_name">
								<?php echo $labels["partner_name"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_name]" value="<?php echo $id_aData['partner_name'] ?>" id="partner_partner_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_name]" id="partner_partner_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_shopex_id">
								<?php echo $labels["partner_shopex_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_shopex_id]" value="<?php echo $id_aData['partner_shopex_id'] ?>" id="partner_shopex_id">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_shopex_id]" id="partner_shopex_id">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_department">
								<?php echo $labels["partner_department"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="partner[partner_department]" id="partner_department" value_id ="<?php if(isset($id_aData['partner_department_arr']['department_id'])){echo $id_aData['partner_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['partner_department_arr']['department_name'])){echo $id_aData['partner_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_manager">
								<?php echo $labels["partner_manager"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_manager]" value="<?php echo $id_aData['partner_manager'] ?>" id="partner_manager">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_manager]" id="partner_manager">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_level">
								<?php echo $labels["partner_level"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="partner[partner_level]">
<?php foreach($partner_level_enum as $level_v){ ?>
   <?php if(isset($id_aData) && $level_v['enum_key']==$id_aData['partner_level_arr']['enum_key'] ){ ?>
     <option value="<?php echo $level_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $level_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $level_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_disc">
								<?php echo $labels["partner_disc"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_disc]" value="<?php echo $id_aData['partner_disc'] ?>" id="partner_disc">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_disc]" id="partner_disc">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_CooperativeTime">
								<?php echo $labels["partner_CooperativeTime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="partner_CooperativeTime">
	<input size="16" type="text" name="partner[partner_CooperativeTime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['partner_CooperativeTime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['partner_CooperativeTime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#partner_CooperativeTime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_endingdate">
								<?php echo $labels["partner_endingdate"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="partner_endingdate">
	<input size="16" type="text" name="partner[partner_endingdate]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['partner_endingdate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['partner_endingdate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#partner_endingdate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_contacts">
								<?php echo $labels["partner_contacts"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_contacts]" value="<?php echo $id_aData['partner_contacts'] ?>" id="partner_contacts">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_contacts]" id="partner_contacts">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_email">
								<?php echo $labels["partner_email"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_email]" value="<?php echo $id_aData['partner_email'] ?>" id="partner_email">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_email]" id="partner_email">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_address">
								<?php echo $labels["partner_address"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_address]" value="<?php echo $id_aData['partner_address'] ?>" id="partner_address">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_address]" id="partner_address">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_fax">
								<?php echo $labels["partner_fax"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_fax]" value="<?php echo $id_aData['partner_fax'] ?>" id="partner_fax">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_fax]" id="partner_fax">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_region">
								<?php echo $labels["partner_region"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_region]" value="<?php echo $id_aData['partner_region'] ?>" id="partner_region">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_region]" id="partner_region">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_contract">
								<?php echo $labels["partner_contract"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_contract]" value="<?php echo $id_aData['partner_contract'] ?>" id="partner_contract">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_contract]" id="partner_contract">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
		 	<?php }else{ ?>
		    <?php switch ($_GET["type_id"]){ 
							case -1:
							break;
						?>
						
						<?php default: ?>
						
						<div class="control-group">
							<label class="control-label required" for="partner_partner_name">
								<?php echo $labels["partner_name"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_name]" value="<?php echo $id_aData['partner_name'] ?>" id="partner_partner_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_name]" id="partner_partner_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_shopex_id">
								<?php echo $labels["partner_shopex_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_shopex_id]" value="<?php echo $id_aData['partner_shopex_id'] ?>" id="partner_shopex_id">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_shopex_id]" id="partner_shopex_id">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_department">
								<?php echo $labels["partner_department"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="partner[partner_department]" id="partner_department" value_id ="<?php if(isset($id_aData['partner_department_arr']['department_id'])){echo $id_aData['partner_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['partner_department_arr']['department_name'])){echo $id_aData['partner_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_manager">
								<?php echo $labels["partner_manager"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_manager]" value="<?php echo $id_aData['partner_manager'] ?>" id="partner_manager">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_manager]" id="partner_manager">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_level">
								<?php echo $labels["partner_level"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="partner[partner_level]">
<?php foreach($partner_level_enum as $level_v){ ?>
   <?php if(isset($id_aData) && $level_v['enum_key']==$id_aData['partner_level_arr']['enum_key'] ){ ?>
     <option value="<?php echo $level_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $level_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $level_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_disc">
								<?php echo $labels["partner_disc"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_disc]" value="<?php echo $id_aData['partner_disc'] ?>" id="partner_disc">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_disc]" id="partner_disc">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_CooperativeTime">
								<?php echo $labels["partner_CooperativeTime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="partner_CooperativeTime">
	<input size="16" type="text" name="partner[partner_CooperativeTime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['partner_CooperativeTime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['partner_CooperativeTime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#partner_CooperativeTime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_endingdate">
								<?php echo $labels["partner_endingdate"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="partner_endingdate">
	<input size="16" type="text" name="partner[partner_endingdate]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['partner_endingdate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['partner_endingdate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#partner_endingdate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_contacts">
								<?php echo $labels["partner_contacts"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_contacts]" value="<?php echo $id_aData['partner_contacts'] ?>" id="partner_contacts">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_contacts]" id="partner_contacts">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_email">
								<?php echo $labels["partner_email"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_email]" value="<?php echo $id_aData['partner_email'] ?>" id="partner_email">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_email]" id="partner_email">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_address">
								<?php echo $labels["partner_address"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_address]" value="<?php echo $id_aData['partner_address'] ?>" id="partner_address">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_address]" id="partner_address">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_fax">
								<?php echo $labels["partner_fax"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_fax]" value="<?php echo $id_aData['partner_fax'] ?>" id="partner_fax">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_fax]" id="partner_fax">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_region">
								<?php echo $labels["partner_region"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_region]" value="<?php echo $id_aData['partner_region'] ?>" id="partner_region">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_region]" id="partner_region">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_contract">
								<?php echo $labels["partner_contract"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_contract]" value="<?php echo $id_aData['partner_contract'] ?>" id="partner_contract">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_contract]" id="partner_contract">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
					<?php }?>
                <?php }}else{ ?>
                  
						<div class="control-group">
							<label class="control-label required" for="partner_partner_name">
								<?php echo $labels["partner_name"];?>
								<span class="required">*</span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_name]" value="<?php echo $id_aData['partner_name'] ?>" id="partner_partner_name">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_name]" id="partner_partner_name">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_shopex_id">
								<?php echo $labels["partner_shopex_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_shopex_id]" value="<?php echo $id_aData['partner_shopex_id'] ?>" id="partner_shopex_id">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_shopex_id]" id="partner_shopex_id">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_department">
								<?php echo $labels["partner_department"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="partner[partner_department]" id="partner_department" value_id ="<?php if(isset($id_aData['partner_department_arr']['department_id'])){echo $id_aData['partner_department_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['partner_department_arr']['department_name'])){echo $id_aData['partner_department_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_manager">
								<?php echo $labels["partner_manager"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_manager]" value="<?php echo $id_aData['partner_manager'] ?>" id="partner_manager">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_manager]" id="partner_manager">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_level">
								<?php echo $labels["partner_level"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="partner[partner_level]">
<?php foreach($partner_level_enum as $level_v){ ?>
   <?php if(isset($id_aData) && $level_v['enum_key']==$id_aData['partner_level_arr']['enum_key'] ){ ?>
     <option value="<?php echo $level_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $level_v['enum_key'];?>"> 
   <?php }?>   
        <?php echo $level_v['enum_name'];?>                       
     </option>
   
   
<?php } ?>
                            
</select>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_disc">
								<?php echo $labels["partner_disc"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_disc]" value="<?php echo $id_aData['partner_disc'] ?>" id="partner_disc">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_disc]" id="partner_disc">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_CooperativeTime">
								<?php echo $labels["partner_CooperativeTime"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="partner_CooperativeTime">
	<input size="16" type="text" name="partner[partner_CooperativeTime]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['partner_CooperativeTime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['partner_CooperativeTime_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#partner_CooperativeTime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_endingdate">
								<?php echo $labels["partner_endingdate"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="partner_endingdate">
	<input size="16" type="text" name="partner[partner_endingdate]" value="2012-01-01 00:00:00" style="width: 154px;" value="<?php if(isset($id_aData['partner_endingdate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['partner_endingdate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#partner_endingdate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 1,
		forceParse: 0,
		format:'yyyy-mm-dd hh:ii:ss'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_contacts">
								<?php echo $labels["partner_contacts"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_contacts]" value="<?php echo $id_aData['partner_contacts'] ?>" id="partner_contacts">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_contacts]" id="partner_contacts">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_email">
								<?php echo $labels["partner_email"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_email]" value="<?php echo $id_aData['partner_email'] ?>" id="partner_email">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_email]" id="partner_email">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_address">
								<?php echo $labels["partner_address"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_address]" value="<?php echo $id_aData['partner_address'] ?>" id="partner_address">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_address]" id="partner_address">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_fax">
								<?php echo $labels["partner_fax"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_fax]" value="<?php echo $id_aData['partner_fax'] ?>" id="partner_fax">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_fax]" id="partner_fax">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_region">
								<?php echo $labels["partner_region"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_region]" value="<?php echo $id_aData['partner_region'] ?>" id="partner_region">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_region]" id="partner_region">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="partner_contract">
								<?php echo $labels["partner_contract"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="partner[partner_contract]" value="<?php echo $id_aData['partner_contract'] ?>" id="partner_contract">
<?php }else{ ?>
<input type="text" maxlength="128" name="partner[partner_contract]" id="partner_contract">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>
						
                 <?php }?>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>