<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>客户化平台</title>
  <script src="<?php echo base_url(); ?>style/admin/js/html5.js"></script>
  <link href="<?php echo base_url();?>style/admin/css/bootstrap/css/bootstrap.css" rel="stylesheet"></link>
  <link href="<?php echo base_url();?>style/admin/css/jquery-ui-bootstrap.css" rel="stylesheet"></link>
  <link href="<?php echo base_url();?>style/admin/css/style.css" rel="stylesheet"></link>
  <link href="<?php echo base_url();?>style/admin/css/main.css" rel="stylesheet"></link>
  <link href="<?php echo base_url();?>style/admin/css/lee.css" rel="stylesheet"></link>
  <script src="<?php echo base_url();?>style/admin/js/jquery.js"></script>
  <script src="<?php echo base_url();?>style/admin/js/jui/js/jquery-ui.min.js"></script>
  <script src="<?php echo base_url();?>style/admin/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
	<header>
		<a data-original-title="" href="index.php" class="logo">新营收平台</a>
	</header>
	<div class="container-fluid">
		<div class="dashboard-container">
			<div class="sub-nav">
			</div>
		</div>
		<div class="dashboard-wrapper">

<div class="widget-body">
	<div class="span3"> </div>
	<div class="span6">
		<div class="sign-in-container">
			<form class="login-wrapper" method="post" action="<?php echo site_url('www/index') ?>">
			<div class="header">
				<div class="row-fluid">
					<div class="span12">
						<h3>
							用户登陆
							<img class="pull-right" alt="Logo" src="<?php echo base_url();?>/upload/logo.png">
						</h3>
						<p>欢迎使用新营收平台</p>
						<p style="color:red"><?php echo $error; ?></p>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="row-fluid">
					<div class="span12">
						<input class="input span12 email" type="text" value="<?php echo set_value('user[user_login_name]') ?>" name="user[user_login_name]" required="required" placeholder="账号" >
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<input class="input span12 password" type="password" name="user[user_password]" required="required" placeholder="密码">
					</div>
				</div>
			</div>
			<div class="actions">
				<input class="btn btn-danger" type="submit" value="登陆" name="Login">
				<a class="link" href="#" data-original-title="">忘记 密码?</a>
				<div class="clearfix"></div>
			</div>
			</form>
		</div>
	</div>
	<div class="span3"> </div>
	<div class="clearfix"></div>
</div>


		</div>
	</div>
	<footer>
	<p>
	© 模板样式 2013
	</p>
	</footer>

<div style="position: fixed; z-index: 2147483647;" title="Scroll to top" id="scrollUp">回到<br>顶部</div>

	<script type="text/javascript">
      var st2=document.documentElement.scrollTop;
	  if (st2>200) {
		$("#scrollUp").show();
	  }else{
		$("#scrollUp").hide();
	  }


	  $(window).scroll(function(){
		  var st2=document.documentElement.scrollTop;
		  if (st2>200) {

			  $("#scrollUp").fadeIn();
			  $("#scrollUp").fadeIn("slow");
			  $("#scrollUp").fadeIn(3000);
		  }else{
			  $("#scrollUp").fadeOut();
			  $("#scrollUp").fadeOut("slow");
			  $("#scrollUp").fadeOut(3000);
		  }
	  });
	  $("#scrollUp").click(function(){
		  $('html').animate({scrollTop:0},500);
	  });
	</script>

	<script src="<?php echo base_url();?>style/index/js/tiny-scrollbar.js"></script>
	<script type="text/javascript">

      //Animated Pie Charts
      function pie_chart() {

        $(function () {
          //create instance
          $('.chart5').easyPieChart({
            animate: 3000,
            barColor: '#F63131',
            trackColor: '#dddddd',
            scaleColor: '#F63131',
            size: 140,
            lineWidth: 6,
          });
          //update instance after 5 sec
          setTimeout(function () {
            $('.chart5').data('easyPieChart').update(30);
          }, 9000);
          setTimeout(function () {
            $('.chart5').data('easyPieChart').update(87);
          }, 19000);
          setTimeout(function () {
            $('.chart5').data('easyPieChart').update(28);
          }, 27000);
          setTimeout(function () {
            $('.chart5').data('easyPieChart').update(69);
          }, 39000);
          setTimeout(function () {
            $('.chart5').data('easyPieChart').update(99);
          }, 47000);
        });
      }
      //Tiny Scrollbar
      $('#scrollbar').tinyscrollbar();
      $('#scrollbar-one').tinyscrollbar();
      $('#scrollbar-two').tinyscrollbar();
      $('#scrollbar-three').tinyscrollbar();


    </script>
	<script src="<?php echo base_url();?>style/admin/js/lee/lee.js"></script>
	<script src="<?php echo base_url();?>style/admin/js/jquery.dataTables.js"></script>
</body>
</html>