<?php
if (isset($_GET['tag_name'])){
	$tag_name = $_GET['tag_name']."_";
}else{
	$tag_name="";
}
//p($_GET);
$postdata = json_encode($_POST);
?>
<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						<?php if(isset($_GET['type_id'])){
							if(isset($type_arr[$_GET['type_id']]['type_name'])){
								echo $type_arr[$_GET['type_id']]['type_name'];
							}else{
								echo '部门';
							}
						}else{
							echo '部门';
						}
						?>
						<span class="mini-title">
						department
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">

						</div>
						<div id="divmessagelist">

<div class="grid-view">
	<div id="<?php echo $tag_name;?>department_list"></div>
<div>
<script type="text/javascript">
$(document).ready(function () {
	$selectAttr=[
		{'value':'name','txt':'部门名称'},
		{'value':'uid','txt':'上级部门'},
		{'value':'code','txt':'部门编码'},
		{'value':'treepath','txt':'树路径'},
		{'value':'treelevel','txt':'级别'},
	];
	
	$("#<?php echo $tag_name;?>department_list").leeDataTable({
		selectAttr:$selectAttr, //简单查询的查询属性
		url:"<?php echo site_url('www/department/ajax_select_quote?hidden_v='.$_GET['hidden_v']); ?>", //ajax查询的地址
		perNumber:10, //每页显示多少条数据
		
	});
});

</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

