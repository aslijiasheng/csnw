
<div class='block'>
    <table class="table table-striped table-bordered table-hover">
      <caption><h3>部门信息</h3></caption>
       <thead><tr><th width="10%">&nbsp;&nbsp;选择部门</th><th width="*">部门名称</th><th width="10%">部门编号</th><th width="10%">负责人</th></tr></thead>
      <tbody>        
	      <tr><td><input type="radio" name="department" id="department" value="1-总部门"></td>
		     <td>总部门</td>
		     <td>1</td>
		      <td>李宗伟</td>
	       </tr>
          <tr><td><input type="radio" name="department" id="department" value="2-后勤部"></td>
	        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;后勤部</td>
	        <td>2</td>
	        <td>裴总</td>
            
          </tr>
          <tr><td><input type="radio" name="department" id="department" value="3-信息技术部"></td>
	        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;信息技术部</td>
	        <td>3</td>
	        <td>董伟</td>
	      </tr>
        </tbody> 
    </table>
    <div><input type="button" value="确定选择" class="btn btn-primary" id="select_department"></div>
</div>
<script>
  $(document).ready(function(){
  
      $("#select_department").click(function(){
          
          $("#department_name").val($("input[name=department]:radio:checked").val());
    	  
    	  $('#icon_dialog').dialog('close');
          })
	  })
</script>