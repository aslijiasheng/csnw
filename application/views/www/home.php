 <!-- 内容部分 -->
          <!--  <div class="block">
              <h2>欢迎登陆新营收平台</h2>
           </div> -->
		<div class="all-sidebar">
           <div class="row-fluid">
              <div class="span12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      代办事宜
                      <span class="mini-title">

                      </span>
                    </div>
                  </div>
                  <div class="widget-body">
                    <div class="row-fluid">
                      <div class="metro-nav">
                        <?php  $user_auth = $this->user->user_auth($this->session->userdata('user_id'));?>
                        <div class="metro-nav-block nav-block-yellow" <?php if(!in_array('mess_invoice',$user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
                          <a href="<?php if(isset($invoice_count) && $invoice_count){echo site_url('www/message?module=invoice');} ?>" data-original-title="">
                            <div class="fs1" aria-hidden="true" data-icon=""></div>
                            <div class="info"><?php if(isset($invoice_count)){echo $invoice_count;} ?></div>
                            <div class="brand">票据</div>
                          </a>
                        </div>

                        <div class="metro-nav-block nav-block-blue double" <?php if(!in_array('mess_salespay',$user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
                          <a href="<?php if(isset($salespay_count) && $salespay_count>0){echo site_url('www/message?module=salespay');}else{echo '#';} ?>" data-original-title="">
                            <div class="fs1" aria-hidden="true" data-icon=""></div>
                            <div class="info"><?php if(isset($salespay_count)){echo $salespay_count;} ?></div>
                            <div class="brand">入款项</div>
                          </a>
                        </div>

                        <div class="metro-nav-block nav-block-green" <?php if(!in_array('mess_rebate',$user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
                          <a href="<?php if(isset($rebate_count) && $rebate_count>0){echo site_url('www/message?module=rebate');}else{echo '#';} ?>" data-original-title="">
                              <div class="fs1" aria-hidden="true" data-icon=""></div>
                              <div class="info"><?php if(isset($rebate_count)){echo $rebate_count;} ?></div>
                            <div class="brand">返点</div>
                          </a>
                        </div>

                        <div class="metro-nav-block nav-block-red" <?php if(!in_array('mess_refund',$user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
                          <a href="<?php if(isset($refund_count) && $refund_count>0){echo site_url('www/message?module=refund');}else{echo '#';} ?>" data-original-title="">
                            <div class="fs1" aria-hidden="true" data-icon=""></div>
                            <div class="info"><?php if(isset($refund_count)){echo $refund_count;} ?></div>
                            <div class="brand">退款</div>
                          </a>
                        </div>

						<div class="metro-nav-block nav-block-orange" <?php if(!in_array('mess_pennyOrder',$user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
                          <a href="<?php if(isset($pennyOrder_count) && $pennyOrder_count>0){echo site_url('www/message?module=pennyOrder');}else{echo '#';} ?>" data-original-title="">
                            <div class="fs1" aria-hidden="true" data-icon=""></div>
                            <div class="info"><?php if(isset($pennyOrder_count)){echo $pennyOrder_count;} ?></div>
                            <div class="brand">内划订单</div>
                          </a>
                        </div>

                         <div class="metro-nav-block nav-block-yellow" <?php if(!in_array('mess_applyopen',$user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
                          <a href="<?php if(isset($applyopen_count) && $applyopen_count>0){echo site_url('www/message?module=applyopen');}else{echo '#';} ?>" data-original-title="">
                            <div class="fs1" aria-hidden="true" data-icon=""></div>
                            <div class="info"><?php if(isset($applyopen_count)){echo $applyopen_count;} ?></div>
                            <div class="brand">开通</div>
                          </a>
                        </div>

                        <div class="metro-nav-block nav-block-orange" <?php if(!in_array('cancel_check',$user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
                          <a href="<?php if(isset($cancel_check_count) && $cancel_check_count>0){echo site_url('www/message?module=cancel_check');}else{echo '#';} ?>" data-original-title="">
                            <div class="fs1" aria-hidden="true" data-icon=""></div>
                            <div class="info"><?php if(isset($cancel_check_count)){echo $cancel_check_count;} ?></div>
                            <div class="brand">审核订单作废</div>
                          </a>
                        </div>
						
						<div class="metro-nav-block nav-block-orange" <?php if(!in_array('cancel_affirm',$user_auth['activity_auth_arr'])){ echo 'style="display:none;"';} ?>>
                          <a href="<?php if(isset($cancel_affirm_count) && $cancel_affirm_count>0){echo site_url('www/message?module=cancel_affirm');}else{echo '#';} ?>" data-original-title="">
                            <div class="fs1" aria-hidden="true" data-icon=""></div>
                            <div class="info"><?php if(isset($cancel_affirm_count)){echo $cancel_affirm_count;} ?></div>
                            <div class="brand">确认订单作废</div>
                          </a>
                        </div>

                        <div class="metro-nav-block nav-block-yellow">
                          <a href="#" data-original-title="">
                            <div class="fs1" aria-hidden="true" data-icon=""></div>
                            <div class="info">0</div>
                            <div class="brand">其他代办</div>
                          </a>
                        </div>
						<!--
						2014-09-16 待查账造成慢查询 取消功能 代码已删除
						-->

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    <!-- 内容部分 -->

