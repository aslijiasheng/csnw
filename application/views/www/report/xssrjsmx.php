<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						销售收入结算明细
						<span class="mini-title">
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
<!--lee 按钮部分 start-->
<?php //p($_POST);?>
<form method="post" id="xssrjsmx-form">
时间范围：<div class="input-append date form_date" id="start_date">
	<input size="16" type="text" name="start_date" style="width: 154px;" value="<?php if(isset($start_date)){echo $start_date;}?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#start_date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		format:'yyyy-mm-dd'
    });
});
</script>
&nbsp至&nbsp
<div class="input-append date form_date" id="end_date">
	<input size="16" type="text" name="end_date" style="width: 154px;" value="<?php if(isset($end_date)){echo $end_date;}?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-clock"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#end_date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		format:'yyyy-mm-dd'
    });
});
</script>
&nbsp&nbsp
隶属于部门：<input type="text" name="department" id="department" value_id ="<?php if(isset($_POST['department'])){echo $_POST['department'];}?>" value="<?php if(isset($_POST['new_department'])){echo $_POST['new_department'];}?>">
<script type="text/javascript">
$(document).ready(function () {
	$('#department').leeQuote({
		url:'<?php echo site_url('www/department/ajax_list?tag_name=department');?>',
		title:'选择部门',
	});
});
</script>
<br>
所属销售：<input type="text" name="user" id="user" value_id ="<?php if(isset($_POST['user'])){echo $_POST['user'];}?>" value="<?php if(isset($_POST['new_user'])){echo $_POST['new_user'];}?>">
<script type="text/javascript">
$(document).ready(function () {
	$('#user').leeQuote({
		url:'<?php echo site_url('www/user/ajax_list?tag_name=user');?>',
		title:'选择销售',
	});
});
</script>
</br>
<button type="submit" class="btn">查看</button>
<?php
if (in_array('xssrjsmxDownload', $user_auth['activity_auth_arr'])) {
	echo '<button type="button" class="btn" id="xssrmxDownload">导出excel</button>';
}
?>
<script>
$(function(){
	$('#xssrmxDownload').click(function(){
		formdata = $('#xssrjsmx-form').serializeArray();
		$.ajax({
			'type': 'post',
			'data': formdata,
			'success': function(data) {
//				alert(data);
				window.location.href=data;
			},
			'url': '<?php echo site_url('www/report/xssrjsmxDownload'); ?>',
			'async': false
		});
	});
});
</script>
</br>
</form>
<!--lee 按钮部分 end-->
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<?php if(isset($data[0])){?>
<table class="leetable table table-bordered">
	<tbody>
		<tr>
			<th>
				销售净额
			</th>
			<td>
				<!--销售净额--><?php if(isset($hz['salespay_pay_amount'])){echo $hz['salespay_pay_amount'];}else{echo 0;}?>&nbsp
			</td>
			<th>
				销售金额
			</th>
			<td>
				<!--入账信息总金额--><?php if(isset($hz['rz_amount'])){echo $hz['rz_amount'];}else{echo 0;}?>&nbsp
			</td>
			<th>
				退款金额
			</th>
			<td>
				<!--退款项总金额--><?php if(isset($hz['tk_amount'])){echo $hz['tk_amount'];}else{echo 0;}?>&nbsp
			</td>
			<th>
				返点金额
			</th>
			<td>
				<!--返点款项总金额--><?php if(isset($hz['fd_amount'])){echo $hz['fd_amount'];}else{echo 0;}?>&nbsp
			</td>
		</tr>
	</tbody>
</table>

<div style="height:500px;overflow:auto;"><div style="width:3000px">
<table class="leetable table table-bordered">
	<tbody>
		<tr>
			<th>
				&nbsp
			</th>
			<th colspan='10'>
				收入信息
			</th>
			<th colspan='<?php echo 3+count($my_bank_account_arr);?>'>
				银行账户信息
			</th>
		</tr>
		<tr>
			<th>
				日期
			</th>
			<th>
				订单类型
			</th>
			<th>
				订单号
			</th>
			<th>
				所属子公司
			</th>
			<th>
				客户类型
			</th>
			<th>
				客户名称
			</th>
			<th>
				经销商名称
			</th>
			<th>
				购买商品
			</th>
			<th>
				所属销售
			</th>
			<th>
				所属部门
			</th>
			<th>
				款项类型名称
			</th>
			<th>
				款项金额
			</th>
			<th>
				支付宝
			</th>
			<th>
				现金
			</th>
			<?php //这里循环获取所有银行账号信息
				foreach($my_bank_account_arr as $k=>$v){
			?>
			<th>
				<?php echo $v;?>&nbsp
			</th>
			<?php
				}
			?>
			<th>
				合计
			</th>
		</tr>
		<?php foreach ($data as $v){ ?>
		<tr <?php
			if($v['salespay_money_type']==1001){echo 'class="warning"';}
			if($v['salespay_money_type']==1002){echo 'class="success"';}
		?>>
			<th>
				<!--日期--><?php if($v['salespay_sp_date']!=""){echo date('Y-m-d',strtotime($v['salespay_sp_date']));}?>&nbsp
			</th>
			<td>
				<!--订单类型--><?php if(isset($v['order_type'])){echo $order_type_arr[$v['order_type']];}?>&nbsp
			</td>
			<td>
				<!--订单号--><a href="<?php echo site_url('www/order/view?order_id='.$v['order_id'])?>"><?php echo $v['order_number'];?></a>&nbsp
			</td>
			<td>
				<!--所属子公司--><?php if(isset($v['salespay_subsidiary'])){echo $salespay_subsidiary_attr[$v['salespay_subsidiary']];}?>&nbsp
			</td>
			<td>
				<!--客户类型--><?php if(isset($v['account_tyoe'])){echo $account_type_arr[$v['account_tyoe']];}?>&nbsp
			</td>
			<td>
				<!--客户名称--><?php echo $v['account_name'];?>&nbsp
			</td>
			<td>
				<!--经销商名称--><?php echo $v['partner_name'];?>&nbsp
			</td>
			<td>
				<!--购买商品--><?php echo $v['goods_name'];?>&nbsp
			</td>
			<td>
				<!--销售人员--><?php echo $v['owner_name'];?>&nbsp
			</td>
			<td>
				<!--业务部门--><?php echo $v['department_name'];?>&nbsp
			</td>
			<td>
				<!--款项类型名称--><?php if(isset($v['salespay_money_type_name'])){echo $money_type_name_arr[$v['salespay_money_type_name']];}?>&nbsp
			</td>
			<td>
				<!--款项金额--><?php echo $v['salespay_pay_amount'];?>&nbsp
			</td>
			<td>
				<!--支付宝--><?php //echo $v['支付方式'].'--'.$pay_method_arr[$v['支付方式']];?>
				<?php if(isset($v['salespay_pay_method'])){if($pay_method_arr[$v['salespay_pay_method']]=='支付宝'){echo $v['salespay_pay_amount'];}}?>&nbsp
			</td>
			<td>
				<!--现金-->
				<?php
					if($v['salespay_money_type']==1001){
						if(isset($v['salespay_pay_method'])){if($pay_method_arr[$v['salespay_pay_method']]=='现金'){echo $v['salespay_pay_amount'];}}
					}
					if($v['salespay_money_type']==1002){
						if(isset($v['salespay_pay_method'])){if($pay_method_arr[$v['salespay_pay_method']]=='现金'){echo $v['salespay_pay_amount'];}}
					}
				?>&nbsp
			</td>
			<?php //这里循环获取所有银行账号信息
			foreach($my_bank_account_arr as $k2=>$v2){
			?>
			<td>
				<?php if(isset($v['salespay_pay_method'])){if($pay_method_arr[$v['salespay_pay_method']]=='汇款' and $v['salespay_my_bank_account']==$k2){echo $v['salespay_pay_amount'];}}?>
				&nbsp
			</td>
			<?php
			}
			?>
			<td>
				<!--合计--><?php echo $v['salespay_pay_amount'];?>&nbsp
			</td>
		</tr>
		<?php }?>
		<tr class="hz">
			<th>
				统计
			</th>
			<td>
				<!--订单号-->&nbsp
			</td>
			<td>
				<!--客户性质-->&nbsp
			</td>
			<td>
				<!--订单号-->&nbsp
			</td>
			<td>
				<!--客户名称-->&nbsp
			</td>
			<td>
				<!--经销商名称-->&nbsp
			</td>
			<td>
				<!--购买商品-->&nbsp
			</td>
			<td>
				<!--销售人员-->&nbsp
			</td>
			<td>
				<!--业务部门-->&nbsp
			</td>
			<td>
				<!--款项类型名称-->&nbsp
			</td>
			<td>
				<!--款项类型名称-->&nbsp
			</td>
			<td>
				<!--入账金额--><?php echo $hz['salespay_pay_amount'];?>&nbsp
			</td>
			<td>
				<!--支付宝--><?php echo $hz['zfb_amount'];?>&nbsp
			</td>
			<td>
				<!--现金--><?php echo $hz['xj_amount'];?>&nbsp
			</td>
			<?php //这里循环获取所有银行账号信息
			foreach($my_bank_account_arr as $k=>$v){
			?>
			<td>
				<?php echo $hz['hk_amount'][$k];?>&nbsp
			</td>
			<?php
			}
			?>
			<td>
				<!--合计--><?php echo $hz['rz_amount'];?>&nbsp
			</td>
		</tr>
	</tbody>
</table>
</div></div>
<?php }else{echo '没有查询到任何数据';}?>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>