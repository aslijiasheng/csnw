<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						<?php if(isset($_GET['type_id'])){
							if(isset($type_arr[$_GET['type_id']]['type_name'])){
								echo $type_arr[$_GET['type_id']]['type_name'];
							}else{
								echo '日记账';
							}
						}else{
							echo '日记账';
						}
						?>
						<span class="mini-title">
						books
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<a id="books_seniorquery" class="btn btn-primary">
<i class="icon-search"></i>
高级查询
</a>
<script type="text/javascript">
//高级查询
$(document).ready(function () {
	$seniorquery_attr = <?php echo json_encode($seniorquery_attr);?>;
	$("#books_seniorquery").seniorquery({
		selectAttr:$seniorquery_attr,
		list_id:'books_list',
		url:"<?php echo site_url('www/books/ajax_select_quote?hidden_v=books_ajax_select_quote'); ?>", //ajax查询的地址
		perNumber:20 //每页显示多少条数据(因为暂时没想通这里要怎么获取！所以直接赋值先)
	});

});
</script>
						</div>
						<div id="divmessagelist">

<div class="grid-view">
	<div id="books_list"></div>
<div>
<script type="text/javascript">
$(document).ready(function () {
	$selectAttr = [
		{'value': 'subsidiary.name', 'txt': '所属子公司'},
		{'value': 'bank_account.name', 'txt': '账户'},
		{'value': 'adate', 'txt': '实际到账/出账日期'},
		{'value': 'bdate', 'txt': '确认到账/出账日期'},
		{'value': 'trade_no', 'txt': '交易号'},
		{'value': 'attn_id.name', 'txt': '经办人'},
		{'value': 'department_id.name', 'txt': '部门'},
//		{'value': 'memo', 'txt': '备注'},
		{'value': 'customer_info', 'txt': '客户信息'},
		{'value': 'sale_no', 'txt': '销售记录号'},
		{'value': 'order_no', 'txt': '订单号'},
//		{'value': 'invoice_track', 'txt': '发票追踪'},
//		{'value': 'rate', 'txt': '汇率'},
//		{'value': 'rmb', 'txt': '折合RMB'},
		{'value': 'debit_amount', 'txt': '借方金额'},
		{'value': 'Credit_amount', 'txt': '贷方金额'},
		{'value': 'number', 'txt': '日记账编号'},
		{'value': 'Note ', 'txt': '摘要'}
	];

	$("#books_list").leeDataTable({
		selectAttr:$selectAttr, //简单查询的查询属性
		url:"<?php echo site_url('www/books/ajax_select_quote?hidden_v='.$_GET['hidden_v']); ?>", //ajax查询的地址
		perNumber:5, //每页显示多少条数据

	});
});

</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

