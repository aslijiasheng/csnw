<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						日记账
						<span class="mini-title">
						books
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="newbooks" class="btn btn-primary" href="<?php echo site_url('www/books');?>">
<i class="icon-file"></i>
返回
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<div id="objlist" class="grid-view">
<table class="leetable table table-bordered">
	<tbody>
		<tr>
			<td class="TbLeft">
				<?php echo $labels['books_number'];?>
			</td>
			<td class="TbRight" colspan='3'>
				<?php echo $data['books_number'];?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo $labels['books_adate'];?>
			</td>
			<td class="TbRight">
				<?php echo date("Y-m-d",strtotime($data['books_adate']));?>
			</td>
			<td class="TbLeft">
				<?php echo $labels['books_bdate'];?>
			</td>
			<td class="TbRight">
				<?php echo date("Y-m-d",strtotime($data['books_bdate']));?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo $labels['books_trade_no'];?>
			</td>
			<td class="TbRight">
				<?php echo $data['books_trade_no'];?>
			</td>
			<td class="TbLeft">
				<?php echo $labels['books_attn_id'];?>
			</td>
			<td class="TbRight">
				<?php
					if($data['books_attn_id']!="" and $data['books_attn_id']!=0){
						echo $data['books_attn_id_arr']['user_name'];
					}else{
						echo "&nbsp;";
					}
				?>
			</td>
		</tr>
		<tr>
		<!--增加所属子公司 2014/08/25-->
		<tr>
			<td class="TbLeft">
				<?php echo $labels['books_subsidiary'];?>
			</td>
			<td class="TbRight" colspan='3'>
				<?php
				if($data['books_subsidiary']!='' and $data['books_subsidiary']!=0){
					echo $data['books_subsidiary_arr']['enum_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>
		<!--增加所属子公司 2014/08/25-->
			<td class="TbLeft">
				<?php echo $labels['books_bank_account'];?>
			</td>
			<td class="TbRight">
				<?php
					if($data['books_bank_account']!="" and $data['books_bank_account']!=0){
						echo $data['books_bank_account_arr']['enum_name'];
					}else{
						echo "&nbsp;";
					}
				?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo $labels['books_customer_info'];?>
			</td>
			<td class="TbRight">
				<?php echo $data['books_customer_info'];?>
			</td>
			<td class="TbLeft">
				<?php echo $labels['books_order_no'];?>
			</td>
			<td class="TbRight">
				<?php echo $data['books_order_no'];?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo $labels['books_debit_amount'];?>
			</td>
			<td class="TbRight">
				<?php echo $data['books_debit_amount'];?>
			</td>
			<td class="TbLeft">
				<?php echo $labels['books_Credit_amount'];?>
			</td>
			<td class="TbRight">
				<?php echo $data['books_Credit_amount'];?>
			</td>
		</tr>
		<tr>
			<td class="TbLeft">
				<?php echo $labels['books_cashier'];?>
			</td>
			<td class="TbRight">
				<?php
				if($data['books_cashier']!='' and $data['books_cashier']!=0){
					echo $data['books_cashier_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
			<td class="TbLeft">
				<?php echo $labels['books_accounting'];?>
			</td>
			<td class="TbRight">
				<?php
				if($data['books_accounting']!='' and $data['books_accounting']!=0){
					echo $data['books_accounting_arr']['user_name'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>

		<tr>
			<td class="TbLeft">
				<?php echo $labels['books_sales_no'];?>
			</td>
			<td class="TbRight">
				<?php echo $data['books_sales_no'];?>
			</td>
			<td class="TbLeft">
				<?php echo $labels['books_department_id'];?>
			</td>
			<td class="TbRight">
				<?php
				if ($data['books_department_id']!=0 and $data['books_department_id']!=""){
				//直接判断，不使用递归
					$d1=$d2=$d3=$d4=$d5='';
					$d1 = $data['books_department_id_arr']['department_name'];
					if(isset($data['books_department_id_arr']['department_uid_arr'])){
						$d2 = $data['books_department_id_arr']['department_uid_arr']['department_name'];
						if(isset($data['books_department_id_arr']['department_uid_arr']['department_uid_arr'])){
							$d3 = $data['books_department_id_arr']['department_uid_arr']['department_uid_arr']['department_name'];
							if(isset($data['books_department_id_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr'])){
								$d4 = $data['books_department_id_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr']['department_name'];
								if(isset($data['books_department_id_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr'])){
									$d5 = $data['books_department_id_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr']['department_uid_arr']['department_name'];
								}
							}
						}
					}
					$s = trim($d5 .' - '.$d4.' - '.$d3.' - '.$d2.' - '.$d1 , ' - ');
					echo $s;
				}
				?>
			</td>
		</tr>

		<tr>
			<td class="TbLeft">
				<?php echo $labels['books_Note'];?>
			</td>
			<td class="TbRight" colspan='3'>
				<?php
				if($data['books_Note']!='' and $data['books_Note']!=0){
					echo $data['books_Note'];
				}else{
					echo "&nbsp";
				}
				?>
			</td>
		</tr>

	</tbody>
</table>


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>