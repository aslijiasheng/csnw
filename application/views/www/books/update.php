<div class="all-sidebar">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="title">
						日记账更新页面
						<span class="mini-title">
						books
						</span>
					</div>
					<span class="tools">
						<!-- 窗口按钮部门
						<a class="fs1" data-icon="&#xe090;" aria-hidden="true" data-original-title=""></a>
						-->
					</span>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="leebutton">
<!--lee 按钮部分 start-->
<a id="yw0" class="btn btn-primary" href="<?php echo site_url('www/books/')?>">
<i class="icon-undo"></i>
返回日记账列表
</a>
<!--lee 按钮部分 end-->
						</div>
						<div id="divmessagelist">
							<div>
<!--lee 内容部分 start-->
<div class="grid-view">
	<form class="form-horizontal" id="department-form" action="<?php if(!isset($_GET['type_id'])){echo site_url('www/books/update?books_id='.$_GET['books_id']);}else{echo site_url('www/books/update?books_id='.$_GET['books_id'].'&type_id='.$_GET['type_id']);}?>" method="post">
		<?php if(0==1){?>
		 	<?php if(!isset($_GET['type_id'])){ ?>

						<!--div class="control-group">
							<label class="control-label required" for="books_company">
								<?php //echo $labels["books_company"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php //foreach($books_company_enum as $company_v){ ?>
<label class="radio inline">
<?php// if(isset($id_aData) && $company_v['enum_key']==$id_aData['books_company_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php //echo $company_v['enum_key']?>" name="books[books_company]" checked>
<?php //}else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php// echo $company_v['enum_key']?>" name="books[books_company]">
<?php //}?>
<?php// echo $company_v['enum_name'];?>
</label>
<?php //} ?>
								<span class="help-inline"></span>
							</div>
						</div-->
<!--新增所属子公司 2014/8/25-->
										<div class="control-group">
											<label class="control-label required" for="books_subsidiary">
												<?php echo $labels["books_subsidiary"]; ?>
												<span class="required"></span>
											</label>
											<div class="controls">
												<?php foreach ($books_subsidiary_enum as $company_v) { ?>
													<label class="radio inline">
														<?php if (isset($id_aData) && $company_v['enum_key'] == $id_aData['books_subsidiary_arr']['enum_key']) { ?>
															<input id="inlineRadioB" type="radio" value="<?php echo $company_v['enum_key'] ?>" name="books[books_subsidiary]" checked>
														<?php } else { ?>
															<input id="inlineRadioB" type="radio"  value="<?php echo $company_v['enum_key'] ?>" name="books[books_subsidiary]">
														<?php } ?>
														<?php echo $company_v['enum_name']; ?>
													</label>
												<?php } ?>
												<span class="help-inline"></span>
											</div>
										</div>
										<!--新增所属子公司 2014/8/25-->
						<div class="control-group">
							<label class="control-label required" for="books_account_id">
								<?php echo $labels["books_bank_account"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="books[books_bank_account]">
<?php foreach($books_bank_account_enum as $bank_account_v){ ?>
   <?php if(isset($id_aData) && $bank_account_v['enum_key']==$id_aData['books_bank_account_arr']['enum_key'] ){ ?>
     <option value="<?php echo $bank_account_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $bank_account_v['enum_key'];?>">
   <?php }?>
        <?php echo $bank_account_v['enum_name'];?>
     </option>


<?php } ?>

</select>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_adate">
								<?php echo $labels["books_adate"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="books_adate">
	<input size="16" type="text" name="books[books_adate]" value="<?php echo empty($id_aData['books_adate']) ? '' : $id_aData['books_adate']; ?>" style="width: 154px;" value="<?php if(isset($id_aData['books_adate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['books_adate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-calendar"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#books_adate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		format:'yyyy-mm-dd'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_bdate">
								<?php echo $labels["books_bdate"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="books_bdate">
	<input size="16" type="text" name="books[books_bdate]" value="<?php echo empty($id_aData['books_bdate']) ? '' : $id_aData['books_bdate']; ?>" style="width: 154px;" value="<?php if(isset($id_aData['books_bdate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['books_bdate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-calendar"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#books_bdate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		format:'yyyy-mm-dd'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_trade_no">
								<?php echo $labels["books_trade_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_trade_no]" value="<?php echo $id_aData['books_trade_no'] ?>" id="books_trade_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_trade_no]" id="books_trade_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_attn_id">
								<?php echo $labels["books_attn_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="books[books_attn_id]" id="books_attn_id" value_id ="<?php if(isset($id_aData['books_attn_id_arr']['user_id'])){echo $id_aData['books_attn_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['books_attn_id_arr']['user_name'])){echo $id_aData['books_attn_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_department_id">
								<?php echo $labels["books_department_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="books[books_department_id]" id="books_department_id" value_id ="<?php if(isset($id_aData['books_department_id_arr']['department_id'])){echo $id_aData['books_department_id_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['books_department_id_arr']['department_name'])){echo $id_aData['books_department_id_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_memo">
								<?php echo $labels["books_memo"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_memo]" value="<?php echo $id_aData['books_memo'] ?>" id="books_memo">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_memo]" id="books_memo">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_customer_info">
								<?php echo $labels["books_customer_info"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_customer_info]" value="<?php echo $id_aData['books_customer_info'] ?>" id="books_customer_info">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_customer_info]" id="books_customer_info">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_sale_no">
								<?php echo $labels["books_sale_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_sale_no]" value="<?php echo $id_aData['books_sale_no'] ?>" id="books_sale_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_sale_no]" id="books_sale_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_order_no">
								<?php echo $labels["books_order_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_order_no]" value="<?php echo $id_aData['books_order_no'] ?>" id="books_order_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_order_no]" id="books_order_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_invoice_track">
								<?php echo $labels["books_invoice_track"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="books[books_invoice_track]">
<?php foreach($books_invoice_track_enum as $invoice_track_v){ ?>
   <?php if(isset($id_aData) && $invoice_track_v['enum_key']==$id_aData['books_invoice_track_arr']['enum_key'] ){ ?>
     <option value="<?php echo $invoice_track_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $invoice_track_v['enum_key'];?>">
   <?php }?>
        <?php echo $invoice_track_v['enum_name'];?>
     </option>


<?php } ?>

</select>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_rate">
								<?php echo $labels["books_rate"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_rate]" value="<?php echo $id_aData['books_rate'] ?>" id="books_rate">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_rate]" id="books_rate">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_rmb">
								<?php echo $labels["books_rmb"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_rmb]" value="<?php echo $id_aData['books_rmb'] ?>" id="books_rmb">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_rmb]" id="books_rmb">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_state">
								<?php echo $labels["books_state"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($books_state_enum as $state_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $state_v['enum_key']==$id_aData['books_state_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $state_v['enum_key']?>" name="books[books_state]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $state_v['enum_key']?>" name="books[books_state]">
<?php }?>
<?php echo $state_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

		 	<?php }else{ ?>
		    <?php switch ($_GET["type_id"]){
							case -1:
							break;
						?>

						<?php default: ?>

						
						<div class="control-group">
											<label class="control-label required" for="books_subsidiary">
												<?php echo $labels["books_subsidiary"]; ?>
												<span class="required"></span>
											</label>
											<div class="controls">
												<?php foreach ($books_subsidiary_enum as $company_v) { ?>
													<label class="radio inline">
														<?php if (isset($id_aData) && $company_v['enum_key'] == $id_aData['books_subsidiary_arr']['enum_key']) { ?>
															<input id="inlineRadioB" type="radio" value="<?php echo $company_v['enum_key'] ?>" name="books[books_subsidiary]" checked>
														<?php } else { ?>
															<input id="inlineRadioB" type="radio"  value="<?php echo $company_v['enum_key'] ?>" name="books[books_subsidiary]">
														<?php } ?>
														<?php echo $company_v['enum_name']; ?>
													</label>
												<?php } ?>
												<span class="help-inline"></span>
											</div>
										</div>
										<!--新增所属子公司 2014/8/25-->

						<div class="control-group">
							<label class="control-label required" for="books_account_id">
								<?php echo $labels["books_bank_account"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="books[books_bank_account]">
<?php foreach($books_bank_account_enum as $bank_account_v){ ?>
   <?php if(isset($id_aData) && $bank_account_v['enum_key']==$id_aData['books_bank_account_arr']['enum_key'] ){ ?>
     <option value="<?php echo $bank_account_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $bank_account_v['enum_key'];?>">
   <?php }?>
        <?php echo $bank_account_v['enum_name'];?>
     </option>


<?php } ?>

</select>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_adate">
								<?php echo $labels["books_adate"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="books_adate">
	<input size="16" type="text" name="books[books_adate]" value="<?php echo empty($id_aData['books_adate']) ? '' : $id_aData['books_adate']; ?>" style="width: 154px;" value="<?php if(isset($id_aData['books_adate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['books_adate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-calendar"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#books_adate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		format:'yyyy-mm-dd'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_bdate">
								<?php echo $labels["books_bdate"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="books_bdate">
	<input size="16" type="text" name="books[books_bdate]" value="<?php echo empty($id_aData['books_bdate']) ? '' : $id_aData['books_bdate']; ?>" style="width: 154px;" value="<?php if(isset($id_aData['books_bdate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'])){echo $id_aData['books_bdate_arr']['attr_id_arr.attr_quote_id_arr.obj_name_name'];} ?>">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-calendar"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#books_bdate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		format:'yyyy-mm-dd'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_trade_no">
								<?php echo $labels["books_trade_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_trade_no]" value="<?php echo $id_aData['books_trade_no'] ?>" id="books_trade_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_trade_no]" id="books_trade_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_attn_id">
								<?php echo $labels["books_attn_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="books[books_attn_id]" id="books_attn_id" value_id ="<?php if(isset($id_aData['books_attn_id_arr']['user_id'])){echo $id_aData['books_attn_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['books_attn_id_arr']['user_name'])){echo $id_aData['books_attn_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_department_id">
								<?php echo $labels["books_department_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="books[books_department_id]" id="books_department_id" value_id ="<?php if(isset($id_aData['books_department_id_arr']['department_id'])){echo $id_aData['books_department_id_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['books_department_id_arr']['department_name'])){echo $id_aData['books_department_id_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_memo">
								<?php echo $labels["books_memo"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_memo]" value="<?php echo $id_aData['books_memo'] ?>" id="books_memo">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_memo]" id="books_memo">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_customer_info">
								<?php echo $labels["books_customer_info"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_customer_info]" value="<?php echo $id_aData['books_customer_info'] ?>" id="books_customer_info">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_customer_info]" id="books_customer_info">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_sale_no">
								<?php echo $labels["books_sale_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_sale_no]" value="<?php echo $id_aData['books_sale_no'] ?>" id="books_sale_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_sale_no]" id="books_sale_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_order_no">
								<?php echo $labels["books_order_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_order_no]" value="<?php echo $id_aData['books_order_no'] ?>" id="books_order_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_order_no]" id="books_order_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_invoice_track">
								<?php echo $labels["books_invoice_track"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="books[books_invoice_track]">
<?php foreach($books_invoice_track_enum as $invoice_track_v){ ?>
   <?php if(isset($id_aData) && $invoice_track_v['enum_key']==$id_aData['books_invoice_track_arr']['enum_key'] ){ ?>
     <option value="<?php echo $invoice_track_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $invoice_track_v['enum_key'];?>">
   <?php }?>
        <?php echo $invoice_track_v['enum_name'];?>
     </option>


<?php } ?>

</select>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_rate">
								<?php echo $labels["books_rate"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_rate]" value="<?php echo $id_aData['books_rate'] ?>" id="books_rate">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_rate]" id="books_rate">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_rmb">
								<?php echo $labels["books_rmb"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_rmb]" value="<?php echo $id_aData['books_rmb'] ?>" id="books_rmb">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_rmb]" id="books_rmb">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_state">
								<?php echo $labels["books_state"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php foreach($books_state_enum as $state_v){ ?>
<label class="radio inline">
<?php if(isset($id_aData) && $state_v['enum_key']==$id_aData['books_state_arr']['enum_key'] ){ ?>
<input id="inlineRadioB" type="radio" value="<?php echo $state_v['enum_key']?>" name="books[books_state]" checked>
<?php }else{ ?>
	<input id="inlineRadioB" type="radio"  value="<?php echo $state_v['enum_key']?>" name="books[books_state]">
<?php }?>
<?php echo $state_v['enum_name'];?>
</label>
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

					<?php }?>
                <?php }}else{ ?>

						<div class="control-group">
											<label class="control-label required" for="books_subsidiary">
												<?php echo $labels["books_subsidiary"]; ?>
												<span class="required"></span>
											</label>
											<div class="controls">
												<?php foreach ($books_subsidiary_enum as $company_v) { ?>
													<label class="radio inline">
														<?php if (isset($id_aData) && $company_v['enum_key'] == $id_aData['books_subsidiary_arr']['enum_key']) { ?>
															<input id="inlineRadioB" type="radio" value="<?php echo $company_v['enum_key'] ?>" name="books[books_subsidiary]" checked>
														<?php } else { ?>
															<input id="inlineRadioB" type="radio"  value="<?php echo $company_v['enum_key'] ?>" name="books[books_subsidiary]">
														<?php } ?>
														<?php echo $company_v['enum_name']; ?>
													</label>
												<?php } ?>
												<span class="help-inline"></span>
											</div>
										</div>
										<!--新增所属子公司 2014/8/25-->

						<div class="control-group">
							<label class="control-label required" for="books_account_id">
								<?php echo $labels["books_bank_account"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="books[books_bank_account]">
<?php foreach($books_bank_account_enum as $bank_account_v){ ?>
   <?php if(isset($id_aData) && $bank_account_v['enum_key']==$id_aData['books_bank_account_arr']['enum_key'] ){ ?>
     <option value="<?php echo $bank_account_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $bank_account_v['enum_key'];?>">
   <?php }?>
        <?php echo $bank_account_v['enum_name'];?>
     </option>


<?php } ?>

</select>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_adate">
								<?php echo $labels["books_adate"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="books_adate">
	<input size="16" type="text" name="books[books_adate]" value="<?php echo empty($id_aData['books_adate']) ? '' : $id_aData['books_adate']; ?>" style="width: 154px;">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-calendar"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#books_adate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		format:'yyyy-mm-dd'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_bdate">
								<?php echo $labels["books_bdate"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<div class="input-append date form_date" id="books_bdate">
									<input size="16" type="text" name="books[books_bdate]" value="<?php echo empty($id_aData['books_bdate']) ? '' : $id_aData['books_bdate']; ?>" style="width: 154px;">
	<span class="add-on"><i class="icon-close"></i></span>
	<span class="add-on"><i class="icon-calendar"></i></span>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#books_bdate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		format:'yyyy-mm-dd'
    });
});
</script>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_trade_no">
								<?php echo $labels["books_trade_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_trade_no]" value="<?php echo $id_aData['books_trade_no'] ?>" id="books_trade_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_trade_no]" id="books_trade_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_attn_id">
								<?php echo $labels["books_attn_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="books[books_attn_id]" id="books_attn_id" value_id ="<?php if(isset($id_aData['books_attn_id_arr']['user_id'])){echo $id_aData['books_attn_id_arr']['user_id']; } ?>" value="<?php if(isset($id_aData['books_attn_id_arr']['user_name'])){echo $id_aData['books_attn_id_arr']['user_name'];} ?>" url="<?php echo site_url('www/user/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_department_id">
								<?php echo $labels["books_department_id"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" maxlength="128" name="books[books_department_id]" id="books_department_id" value_id ="<?php if(isset($id_aData['books_department_id_arr']['department_id'])){echo $id_aData['books_department_id_arr']['department_id']; } ?>" value="<?php if(isset($id_aData['books_department_id_arr']['department_name'])){echo $id_aData['books_department_id_arr']['department_name'];} ?>" url="<?php echo site_url('www/department/ajax_list') ?>"  leetype="quote">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_memo">
								<?php echo $labels["books_memo"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_memo]" value="<?php echo $id_aData['books_memo'] ?>" id="books_memo">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_memo]" id="books_memo">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_Note">
								<?php echo $labels["books_Note"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_Note]" value="<?php echo $id_aData['books_Note'] ?>" id="books_Note">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_Note]" id="books_Note">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_customer_info">
								<?php echo $labels["books_customer_info"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_customer_info]" value="<?php echo $id_aData['books_customer_info'] ?>" id="books_customer_info">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_customer_info]" id="books_customer_info">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_sale_no">
								<?php echo $labels["books_sale_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_sale_no]" value="<?php echo $id_aData['books_sale_no'] ?>" id="books_sale_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_sale_no]" id="books_sale_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_order_no">
								<?php echo $labels["books_order_no"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_order_no]" value="<?php echo $id_aData['books_order_no'] ?>" id="books_order_no">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_order_no]" id="books_order_no">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_invoice_track">
								<?php echo $labels["books_invoice_track"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<select id="country" name="books[books_invoice_track]">
<?php foreach($books_invoice_track_enum as $invoice_track_v){ ?>
   <?php if(isset($id_aData) && $invoice_track_v['enum_key']==$id_aData['books_invoice_track_arr']['enum_key'] ){ ?>
     <option value="<?php echo $invoice_track_v['enum_key'];?>" selected="selected">
   <?php }else{ ?>
      <option value="<?php echo $invoice_track_v['enum_key'];?>">
   <?php }?>
        <?php echo $invoice_track_v['enum_name'];?>
     </option>


<?php } ?>

</select>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_rate">
								<?php echo $labels["books_rate"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_rate]" value="<?php echo $id_aData['books_rate'] ?>" id="books_rate">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_rate]" id="books_rate">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_rmb">
								<?php echo $labels["books_rmb"];?>
								<span class="required"></span>
							</label>
							<div class="controls">
								<?php if(isset($id_aData)){ ?>
<input type="text" maxlength="128" name="books[books_rmb]" value="<?php echo $id_aData['books_rmb'] ?>" id="books_rmb">
<?php }else{ ?>
<input type="text" maxlength="128" name="books[books_rmb]" id="books_rmb">
<?php } ?>
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_debit_amount">
								借方金额
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" id="books_debit_amount" name="books[books_debit_amount]" onKeyPress="if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode != 46 || /\.\d\d$/.test(value))
											event.returnValue = false" value="<?php echo $id_aData['books_debit_amount'] ?>">
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label required" for="books_Credit_amount">
								贷方金额
								<span class="required"></span>
							</label>
							<div class="controls">
								<input type="text" id="books_Credit_amount" onKeyPress="if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode != 46 ||  /\.\d\d$/.test(value) ) 									event.returnValue = false"  name="books[books_Credit_amount]" value="<?php echo $id_aData['books_Credit_amount'] ?>">
								<span class="help-inline"></span>
							</div>
						</div>
				<?php }?>

		<!-- 按钮区域 start -->
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">保存</button>
		</div>
		<!-- 按钮区域 end -->
	</form>
</div>
<!--lee 内容部分 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>