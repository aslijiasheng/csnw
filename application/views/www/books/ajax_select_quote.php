	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="30">操作</th>
				<th width="30"><?php echo $labels['books_company'];?></th>
				<th width="80">日记账编号</th>
				<th width="60"><?php echo $labels['books_bank_account'];?></th>
				<th><?php echo $labels['books_adate'];?></th>
				<th width="80"><?php echo $labels['books_trade_no'];?></th>
				<th><?php echo $labels['books_Note'];?></th>
				<th width="80">借方金额</th>
				<th width="80">贷方金额</th>
				

			</tr>
		</thead>
		<tbody>
		<?php foreach($listData as $listData_value){?>
			<tr>
				<td style="font-size:16px;">
					<input type="radio" name="optionsRadios" value="<?php echo $listData_value['books_id']; ?>" name_val="<?php echo $listData_value['books_number']; ?>"<?php if($_GET["hidden_v"]!=''){$hidden_id=explode('?',$_GET["hidden_v"]);$hidden_id=$hidden_id[0]; if($hidden_id==$listData_value['books_id']){echo "checked='checked'";}}?> >
				</td>
				<td>
					<?php
						if($listData_value['books_company']!="" and $listData_value['books_company']!=0){
							echo $listData_value['books_company_arr']['enum_name'];
						}else{
							echo "&nbsp;";
						}
					?>
					</td>
					
					<td>
					<?php echo $listData_value['books_number'];?>
					</td>

					<td>
					<?php
						if($listData_value['books_bank_account']!="" and $listData_value['books_bank_account']!=0){
							echo $listData_value['books_bank_account_arr']['enum_name'];
						}else{
							echo "&nbsp;";
						}
					?>
					</td>
					
					<td>
					<?php echo date("Y-m-d",strtotime($listData_value['books_adate']));?>
					</td>
					
					<td>
					<?php  echo $listData_value['books_trade_no'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['books_Note'];?>
					</td>

					<td>
					<?php  echo $listData_value['books_debit_amount'];?>
					</td>
					
					<td>
					<?php  echo $listData_value['books_Credit_amount'];?>
					</td>
					
				
			</tr>
		<?php }?>
		</tbody>
		
	</table>
	<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber;?>条</div>
	<input type='hidden' id='totalNumber' value="<?php echo $totalNumber;?>">
	</div>

	<script type="text/javascript">
	$(document).ready(function () {
		//刷新分页
		$("#books_list").RefreshPage({
			page:<?php if(isset($page)){echo $page;}else{echo 1;}?>, //当前页码
			perNumber:20, //每页显示多少条数据
			totalNumber:<?php echo $totalNumber;?>, //数据总数
			sel_data:<?php echo json_encode($sel_data)?>,
			listUrl:'<?php echo site_url('www/books/ajax_select_quote?hidden_v=books_ajax_select_quote'); ?>',
		});
	});
	</script>

