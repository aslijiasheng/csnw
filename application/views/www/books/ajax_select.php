<div  id='books_json' style='display:none;'><?php echo json_encode($sel_data); ?></div>
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="6%">操作</th>
			<th width="4%"><?php echo $labels['books_subsidiary']; ?></th>
			<th width="5%">日记账编号</th>
			<th width="5%">部门</th>
			<th>客户信息</th>
			<th width="8%">订单编号</th>
			<th width="18%"><?php echo $labels['books_bank_account']; ?></th>
			<th width="7.5%"><?php echo $labels['books_adate']; ?></th>
			<th width="7.5%"><?php echo $labels['books_bdate']; ?></th>
			<th width="10%"><?php echo $labels['books_trade_no']; ?></th>
			<th width="10%"><?php echo $labels['books_Note']; ?></th>
			<th width="6%">借方金额</th>
			<th width="6%">贷方金额</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($listData as $listData_value) { ?>
			<tr>
				<td style="font-size:16px;">
					<a href="<?php echo site_url('www/books/view') . '?books_id=' . $listData_value['books_id'] ?>" rel="tooltip" title="查看"><span class="icon-eye"></span></a>
					<a href="<?php echo site_url('www/books/update') . '?books_id=' . $listData_value['books_id'] ?>" rel="tooltip" title="更新"><span class="icon-pencil"></span></a>
					<a class="delBooks" books_id="<?php echo $listData_value['books_id']; ?>" books_number="<?php echo $listData_value['books_number']; ?>" rel="tooltip" title="删除" <?php if(!in_array('books_del',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>><span class="icon-remove-2"></span></a>
					<a class="pressBooks" books_id="<?php echo $listData_value['books_id']; ?>" books_number="<?php echo $listData_value['books_number']; ?>" rel="tooltip" title="取消关联" <?php if(!in_array('books_press',$user_auth['activity_auth_arr'])){echo 'style="display:none;"';} ?>><span class="icon-scissors"></span></a>
				</td>
				<td>
					<?php
					if ($listData_value['books_subsidiary'] != "" and $listData_value['books_subsidiary'] != 0) {
						echo $listData_value['books_subsidiary_arr']['enum_name'];
					} else {
						echo "&nbsp;";
					}
					?>
				</td>

				<td>
					<?php echo $listData_value['books_number']; ?>
				</td>

				<td>
					<?php echo isset($listData_value['books_department_id_arr']['department_name']) ? $listData_value['books_department_id_arr']['department_name'] : ''; ?>
				</td>
				
				<td>
					<?php echo $listData_value['books_customer_info']; ?>
				</td>

				<td>
					<?php echo $listData_value['books_order_no']; ?>
				</td>

				<td>
					<?php
					if ($listData_value['books_bank_account'] != "" and $listData_value['books_bank_account'] != 0) {
						echo $listData_value['books_bank_account_arr']['enum_name'];
					} else {
						echo "&nbsp;";
					}
					?>
				</td>

				<td>
					<?php echo date("Y-m-d", strtotime($listData_value['books_adate'])); ?>&nbsp;
				</td>

				<td>
					<?php
					if (isset($listData_value['books_bdate'])) {
						echo date("Y-m-d", strtotime($listData_value['books_bdate']));
					}
					?>&nbsp;
				</td>

				<td>
					<?php echo $listData_value['books_trade_no']; ?>
				</td>

				<td>
					<?php echo $listData_value['books_Note']; ?>
				</td>

				<td style="text-align:right;">
					<?php echo $listData_value['books_debit_amount']; ?>
				</td>

				<td style="text-align:right;">
					<?php echo $listData_value['books_Credit_amount']; ?>
				</td>

			</tr>
		<?php } ?>
	</tbody>
</table>
<script>
	$(".delBooks").click(function() {
		books_number = $(this).attr('books_number');
		books_id = $(this).attr('books_id');
		alertify.confirm('是否删除日记账 ' + books_number, function(e) {
			if (e) {
//														orders_id = $("#orders_id").val();
				formdata = {"books_id": books_id};
				$.ajax({
					'type': 'post',
					'data': formdata,
					'success': function(data) {
						if (data == 'ok') {
							alertify.success('已删除');
							$("#books_list").ajaxHtml({
								url: "<?php echo site_url('www/books/ajax_select'); ?>",
								data:<?php echo json_encode($sel_data); ?>,
							});
						} else {
							alertify.error(data);
						}
					},
					'url': '<?php echo site_url('www/books/del'); ?>',
					'async': false
				});
			} else {
				alertify.error("已取消");
			}
		})
	});

	$(".pressBooks").click(function() {
		books_number = $(this).attr('books_number');
		books_id = $(this).attr('books_id');
		alertify.confirm('是否取消日记账关联 ' + books_number, function(e) {
			if (e) {
				formdata = {"books_id": books_id};
				$.ajax({
					'type': 'post',
					'data': formdata,
					'success': function(data) {
						if (data == 'ok') {
							alertify.success('已取消关联');
							$("#books_list").ajaxHtml({
								url: "<?php echo site_url('www/books/ajax_select'); ?>",
								data:<?php echo json_encode($sel_data); ?>,
							});
						} else {
							alertify.error(data);
						}
					},
					'url': '<?php echo site_url('www/books/press'); ?>',
					'async': false
				});
			} else {
				alertify.error("已取消操作");
			}
		})


		disBooks
	});
</script>
<div id="data-table_info" style="float:left">共查询到<?php echo $totalNumber; ?>条</div>
<input type='hidden' id='totalNumber' value="<?php echo $totalNumber; ?>">
</div>

<script type="text/javascript">
	$(document).ready(function() {
		//刷新分页
		$("#books_list").RefreshPage({
			page:<?php if (isset($page)) {
			echo $page;
		} else {
			echo 1;
		} ?>, //当前页码
			perNumber: 20, //每页显示多少条数据
			totalNumber:<?php echo $totalNumber; ?>, //数据总数
			sel_data:<?php echo json_encode($sel_data) ?>,
			listUrl: '<?php echo site_url('www/books/ajax_select?hidden_v=books_ajax_select'); ?>',
		});
	});
</script>