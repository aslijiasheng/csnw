<?php
class Open_apply_model extends CI_Model{

	//这里代表那些字段在列表上显示出来
	public function listLayout(){
		return array(
			
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout(){
		return array(
			
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout(){
		return array(
			
		);
	}

	//属性对应的中文标签
	public function attributeLabels(){
		return array(
			
			'open_apply_account' => '客户',
			
			'open_apply_order' => '订单',
			
			'open_apply_goods_acct_code' => '商品户号',
			
			'open_apply_goods_name' => '商品名称',
			
			'open_apply_goods_code' => '商品编码',
			
			'open_apply_product_basic_acct_code' => '基础产品户号',
			
			'open_apply_product_basic_name' => '基础产品名称',
			
			'open_apply_product_basic_code' => '基础产品编码',
			
			'open_apply_apply_status' => '开通状态',
			
			'open_apply_owner_user' => '所有者',
			
			'open_apply_department' => '所属部门',
			
			'open_apply_create_time' => '创建时间',
			
			'open_apply_modify_time' => '修改时间',
			
		);
	}

	//列表查询
	/*
		$page //获得当前的页面值
		$perNumber //每页显示的记录数
	*/
	public function listGetInfo($where,$page=1,$perNumber=10,$like=""){
		$limitStart = ($page-1)*$perNumber+1-1; //这里因为数据库是从0开始算的！所以要减1
		$limit=array($perNumber,$limitStart);
		//p($limit);
		$data = $this->GetInfo($where,$limit,$like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id){
		$where=array(
			'open_apply_id'=>$id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data,$id){
		$this->db->where('open_apply_id',$id)->update('tc_open_apply',$data);
	}

	//单个删除
	public function del($id){
		$this->db->where('open_apply_id',$id)->delete('tc_open_apply');
	}

	//单个新增
	public function add($data){
		$this->db->insert('tc_open_apply',$data);
		$insert_id=$this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where="",$like=""){
		$this->db->from('tc_open_apply');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}


	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where="",$limit="",$like=""){
		//首先查询出本身所有的内容
		$this->db->from('tc_open_apply');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($limit!=""){
			$this->db->limit($limit[0],$limit[1]);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$data=$this->db->get()->result_array();
		//然后循环查询出所有引用的内容
		
		foreach ($data as $k=>$v){
			//p($v);die;
			//所有属性循环
			
				 
			if($v['tc_open_apply_account']!="" and $v['tc_open_apply_account']!=0){
				$this->load->model('www/account_model','account');
				$data[$k]['tc_open_apply_account_arr'] = $this->account->id_aGetInfo($v['tc_open_apply_account']);
			}
				
			
				
			
				
			
			
				 
			if($v['tc_open_apply_order']!="" and $v['tc_open_apply_order']!=0){
				$this->load->model('www/order_model','order');
				$data[$k]['tc_open_apply_order_arr'] = $this->order->id_aGetInfo($v['tc_open_apply_order']);
			}
				
			
				
			
				
			
			
				
			
				
			
				
			
			
				
			
				
			
				
			
			
				
			
				
			
				
			
			
				
			
				
			
				
			
			
				
			
				
			
				
			
			
				
			
				
			
				
			
			
				
			
				
			
				 //当属性类型为下拉单选时
			if($v['open_apply_apply_status']!="" and $v['open_apply_apply_status']!=0){
				$this->load->model('admin/enum_model','enum');
				
				$data[$k]['open_apply_apply_status_arr']=$this->enum->getlist(649,$data[$k]['open_apply_apply_status']);
			}
				
			
			
				 
			if($v['open_apply_owner_user']!="" and $v['open_apply_owner_user']!=0){
				$this->load->model('www/user_model','user');
				$data[$k]['open_apply_owner_user_arr'] = $this->user->id_aGetInfo($v['open_apply_owner_user']);
			}
				
			
				
			
				
			
			
				 
			if($v['open_apply_department']!="" and $v['open_apply_department']!=0){
				$this->load->model('www/department_model','department');
				$data[$k]['open_apply_department_arr'] = $this->department->id_aGetInfo($v['open_apply_department']);
			}
				
			
				
			
				
			
			
				
			
				
			
				
			
			
				
			
				
			
				
			
			
		}
		return $data;
	}
}
?>
