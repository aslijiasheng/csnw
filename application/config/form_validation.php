<?php 
  $config = array(
     'login' => array(
         array(
            'field'=>'username',
            'label'=>'用户名',
            'rules'=>'required|min_length[4]'
         	),
         array(
            'field'=>'passwd',
            'label'=>'密码',
            'rules'=>'required|min_length[5]'
         	)
     	),
	'register' =>array(
	  array(
	    'field'=>'nickname',
		'label'=>'昵称',
		'rules'=>'required|min_length[4]|max_length[20]'
	  ),
	  array(
	    'field'=>'password',
		'label'=>'密码',
		'rules'=>'required|min_length[5]|max_length[15]'
	  ),
	  array(
	    'field'=>'email',
		'label'=>'邮箱',
		'rules'=>'required|valid_email'
	  ),
	  array(
	    'field'=>'code',
		'label'=>'验证码',
		'rules'=>'required'
	  ),
	),
  	);
 ?>