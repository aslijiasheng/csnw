<?php 
class Templates_model extends CI_Model{
  	public function tem_list($template_type){
  		return $this->db->get_where('dd_template',array('template_type'=>$template_type))->result_array();
  	}

  	public function del($id){
       $this->db->where('template_id',$id)->delete("dd_template");
  	}
  	public function add($data){
     $data['create_time']=date('Y-m-d H:i:s',time());
     $data['update_time']=date('Y-m-d H:i:s',time());
     $this->db->insert('dd_template',$data);
  	}

	//用ID单个查询
	public function id_aGetInfo($id){
		$where=array(
			'template_id'=>$id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}
	
	//根据模板ID IN出结果集
	public function selectIn($ids)
	{
		return $this->db->from('dd_template')->where_in('template_id',$ids)->get()->result_array();
	}


	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where=""){
		//首先查询出本身所有的内容
		if ($where==""){
			$data=$this->db->from('dd_template')->get()->result_array();
		}else{
			$data=$this->db->from('dd_template')->where($where)->get()->result_array();
		}
		return $data;
	}
}
?>