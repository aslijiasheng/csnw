<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Action_model extends CI_Model{
     public function getActions($obj_id){
        $data = $this->db->select('*')->from('dd_action')->where(array('dd_action.obj_id'=>$obj_id))->join('dd_object','dd_object.obj_id=dd_action.obj_id')->get()->result_array();
        for($i=0; $i<count($data);$i++){
        	$data[$i]['controller_template']=$this->db->get_where('dd_template',array('template_id'=>$data[$i]['controller_template_id']))->result_array();
        	$data[$i]['view_template']=$this->db->get_where('dd_template',array('template_id'=>$data[$i]['view_template_id']))->result_array();
        }
       return $data;
     }
      public function get_obj_name($obj_id){
       return $this->db->select('obj_name')->from('dd_object')->where('obj_id',$obj_id)->get()->result_array();
    }
     public function add_action($data){
        $data['create_time']=date('Y-m-d H:i:s',time());
        $this->db->insert('dd_action',$data);
     }
     public function del_action($act_id){
      $this->db->where('act_id',$act_id)->delete('dd_action');
     }
     public function search_info($act_id){
       //return $this->db->get_where('dd_action',array('act_id'=>$act_id))->join('dd_template','dd_template.template_id=dd_action.')->result_array();
         $data=$this->db->select("*")->from('dd_action')->where('act_id',$act_id)->get()->result_array();

         $controller_template_name=$this->db->select('template_name')->from('dd_template')->where('template_id',$data[0]['controller_template_id'])->get()->result_array();
         //
         //$data[0]['view_template_name']=$view_template_name;
         $data[0]['controller_template_name']=$controller_template_name[0]['template_name'];
         $view_template_name=$this->db->select('template_name')->from('dd_template')->where('template_id',$data[0]['view_template_id'])->get()->result_array();
         $data[0]['view_template_name']=$view_template_name[0]['template_name'];
        return $data;
     }
     public function edit($data,$act_id){
       $this->db->where('act_id',$act_id)->update('dd_action',$data);
     }


	//列表查询-根据obj_id查询
	public function listGetInfo($obj_id){
		$where=array(
			'obj_id'=>$obj_id
		);
		$data = $this->GetInfo($where);
		return $data;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where=""){
		//首先查询出本身所有的内容
		if ($where==""){
			$data=$this->db->from('dd_action')->get()->result_array();
		}else{
			$data=$this->db->from('dd_action')->where($where)->get()->result_array();
		}
		//p($data);exit;
		foreach ($data as $k=>$v){
			//controller_template_id引用模板
			if($v['controller_template_id']!="" and $v['controller_template_id']!=0){
				$this->load->model('admin/templates_model','templates');
				$data[$k]['controller_template_id_arr'] = $this->templates->id_aGetInfo($v['controller_template_id']);
			}
			//controller_template_id引用模板
			if($v['view_template_id']!="" and $v['view_template_id']!=0){
				$this->load->model('admin/templates_model','templates');
				$data[$k]['view_template_id_arr'] = $this->templates->id_aGetInfo($v['view_template_id']);
			}
		}
		return $data;
	}
}
?>