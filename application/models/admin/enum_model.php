<?php
  class Enum_model extends CI_Model{
  	public function getlist($attr_id='',$enum_key=""){
      $where=array(
        'attr_id'=>$attr_id
      );

      if($enum_key!=""){
        $where['enum_key']=$enum_key;
      }
		$data = $this->db->get_where("dd_enum",$where)->result_array();
      if($enum_key!=""){
        if(!isset($data[0])){
			//p($where);
		}
		$data=$data[0];
      }
      return $data;
  	}
  	public function add($attr_id,$arr,$start_key=1001){
  		for($i=0;$i<count($arr);$i++){
  			$data['attr_id']=$attr_id;
  			$data['enum_key']=$start_key+$i;
  			$data['enum_name']=$arr[$i];
  			$this->db->insert("dd_enum",$data);
  		}
  	}

  	public function edit($attr_id,$arr){
  		for($i=0;$i<count($arr);$i++){
  			$data['enum_name']=$arr[$i];
  			$this->db->where(array('attr_id'=>$attr_id,'enum_key'=>(1001+$i)))->update('dd_enum',$data);
  		}
  	}

  	public function del($attr_id,$enum_key){
  		$this->db->where(array('attr_id'=>$attr_id,'enum_key'=>$enum_key))->delete('dd_enum');
  		$arr=$this->db->get_where('dd_enum',array('attr_id'=>$attr_id))->result_array();

  		 for($i=0;$i<count($arr);$i++){
  		 	$data['enum_name']=$arr[$i]['enum_name'];
  		 	$data['enum_key']=1001+$i;
  		 	$this->db->where(array('attr_id'=>$attr_id,'enum_key'=>$arr[$i]['enum_key']))->update('dd_enum',$data);
  		 }
  	}

	public function getLists($attr_id = '', $enum_key = "") {
		if ($attr_id && $enum_key) {
			$enum_arr = $this->db->query('SELECT * FROM `dd_enum` WHERE `attr_id`="' . $attr_id . '" AND `enum_key` in (' . $enum_key . ')')->result_array();
		} else {
			return '';
		}

		if ($enum_arr) {
			$data['enum_name'] = '';
			foreach ($enum_arr as $k => $v) {
				$data['enum_name'] .= ',' . $v['enum_name'];
			}
			$data['enum_name'] = ltrim($data['enum_name'], ',');
		}
        // var_dump($attr_id);
        // var_dump($enum_key);
        return $data;
	}

	public function attr_id_GetInfo($id) {
		$where=array(
			'attr_id'=>$id,
		);
		$enum_arr = $this->db->get_where('dd_enum', $where)->result_array();
		if ($enum_arr) {
			foreach ($enum_arr as $k => $v) {
				$data[$v['enum_key']] = $v['enum_name'];
			}
		}
		return $data;
	}

	public function whereGetInfo($where) {
		if (!empty($where)) {
			$res = $this->db->get_where('dd_enum', $where)->row_array();
			if ($res) {
				return $res;
			} else
				return false;
		} return false;
	}

}
 ?>