<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Object_model extends CI_Model{
	var $title = '';

	//属性对应的中文标签
	public function attributeLabels()
	{
		return array(
			'obj_id' => '对象ID',
			'obj_uid' => '父级对象ID',
			'obj_name' => '对象名称',
			'obj_label' => '中文标签',
			'menu_label' => '主菜单标签',
			'obj_icon' => '对象图标',
			'is_obj_type' => '定义类型',
			'is_ref_obj' => '可以被引用',
			'is_detail' => '是否包含明细',
			'create_time' => '创建时间',
			'update_time' => '最后更新时间'
		);
	}

	//列表查询
	public function objlist() {
		$this->db->order_by('obj_id');
		$obj_arr = $this->db->get_where('dd_object',array('obj_uid'=>0))->result_array();
		foreach ($obj_arr as $key=>$value){
			if ($value['is_detail']==1 && $value['is_obj_type']==0){
                $this->db->select('dd_detail.detail_obj_id,dd_detail.obj_id,dd_object.obj_label');
                $this->db->from('dd_object');
                $this->db->join('dd_detail', 'dd_object.obj_id = dd_detail.detail_obj_id');
                $this->db->where('dd_detail.obj_id',$value['obj_id']);
                $obj_arr[$key]['detail_list'] = $this->db->get()->result_array();
			}
		}
		return $obj_arr;
	}

	public function add($data){
		$this->db->insert('dd_object',$data);
		$insert_id=$this->db->insert_id();
		return $insert_id;
	}

	//单个查询
	public function getinfo($obj_id){
		$getinfo = $this->db->get_where('dd_object',array('obj_id'=>$obj_id))->result_array();
		foreach ($getinfo as $key=>$value){
			if ($value['is_detail']==1){
                $this->db->select('dd_detail.detail_obj_id,dd_detail.obj_id,dd_object.obj_label');
                $this->db->from('dd_object');
                $this->db->join('dd_detail', 'dd_object.obj_id = dd_detail.detail_obj_id');
                $this->db->where('dd_detail.obj_id',$value['obj_id']);
                $obj_arr[$key]['detail_list'] = $this->db->get()->result_array();
//				$this->db->order_by('obj_id asc');
//				$getinfo[$key]['object'] = $this->db->get_where('dd_object',array('obj_uid'=>$value['obj_id']))->result_array();
			}
		}
		return $getinfo[0];
	}

	//单个修改
	public function update_obj($obj_id,$data){
		$this->db->where('obj_id',$obj_id)->update('dd_object',$data);
	}

	public function whereGetInfo($where) {
		return $this->db->get_where('dd_object', $where)->row_array();
	}
	
	//通过对象名获取单个对象的参数以及包含的属性
	public function nameGetInfo($obj_name){
		$where = array(
			'obj_name'=>$obj_name,
		);
		$data = $this->db->get_where('dd_object',$where)->row_array();
		//添加这个对象包含的属性
		$this->load->model("admin/attribute_model",'attr');
		$data['attr_arr'] = $this->attr->check($data['obj_id'], TRUE);
		return $data;
	}

}
 ?>