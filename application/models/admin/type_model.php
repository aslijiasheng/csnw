<?php
  class Type_model extends CI_Model{
  	public function getlist($obj_id){
  		$this->db->query("set names utf8");
  		$data = $this->db->get_where('dd_type',array('obj_id'=>$obj_id))->result_array();
        // 添加明细
		//p($data);
        foreach($data as $k=>$v){
            $sql = 'SELECT * FROM dd_object AS o WHERE o.obj_id IN (SELECT d.detail_obj_id FROM dd_detail AS d WHERE d.obj_id = '.$v['obj_id'].' AND d.type_id = '.$v['type_id'].')';
            $data[$k]['detaillist'] = $this->db->query($sql)->result_array();
        }
        return $data;
  	}
  	public function add($data){
      $this->db->insert('dd_type',$data);
      $insert_id=$this->db->insert_id();
      return $insert_id;
  	}

  	public function id_getinfo($id){
      return $this->db->get_where('dd_type',array('type_id'=>$id))->result_array();
  	}
  	public function edit($data,$type_id){
       $this->db->where('type_id',$type_id)->update('dd_type',$data);
  	}
  	public function del($type_id){
  		$this->db->where('type_id',$type_id)->delete("dd_type");
  	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where="",$limit="",$like=""){
		//首先查询出本身所有的内容
		$this->db->from('dd_type');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($limit!=""){
			$this->db->limit($limit[0],$limit[1]);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$data=$this->db->get()->result_array();

		foreach ($data as $k=>$v){
			//p($v['type_id']);
			//循环将查询到的布局信息写进每个类型
			$type_id =$v['type_id'];
			$type_name =$v['type_name'];
			$obj_id =$v['obj_id'];
			$this->load->model('admin/layout_model','layout');
			$where = array(
				'obj_id'=>$obj_id,
				'layout_type'=>'list',
				'type_id'=>$type_id,
			);
			$data[$k]['list_layout'] = $this->layout->arr_getinfo($where);
			$where = array(
				'obj_id'=>$obj_id,
				'layout_type'=>'view',
				'type_id'=>$type_id,
			);
			$data[$k]['view_layout'] = $this->layout->arr_getinfo($where);
			$where = array(
				'obj_id'=>$obj_id,
				'layout_type'=>'edit',
				'type_id'=>$type_id,
			);
			$data[$k]['edit_layout'] = $this->layout->arr_getinfo($where);

		}
		//exit;

		return $data;
	}

	public function whereGetInfo($where) {
		if (!empty($where)) {
			$res = $this->db->get_where('dd_type', $where)->row_array();
			if ($res) {
				return $res;
			} else
				return false;
		}return false;
	}

    public function rowGetInfo($where) {
		if (!empty($where)) {
			$res = $this->db->get_where('dd_type', $where)->row_array();
			if ($res) {
				return $res;
			} else
				return false;
		}return false;
	}

	public function obj_id_GetInfo($id) {
		$where=array(
			'obj_id'=>$id,
		);
		$type_arr = $this->db->get_where('dd_type', $where)->result_array();
		if ($type_arr) {
			foreach ($type_arr as $k => $v) {
				$data[$v['type_id']] = $v['type_name'];
			}
		}
		return $data;
	}
}
 ?>