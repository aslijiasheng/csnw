<?php 
class TemplateGroup_model extends CI_Model{
	
		
	public function libattrible()
	{
		return array("view" => "视图模板",
			 "controller" => "控制器模板",
			 "model" => "模型模板");
	}
	
  	public function del($id){
       $this->db->where('templateGroup_id',$id)->delete("dd_templateGroup");
       return $this->db->affected_rows();
  	}
	
  	public function add($data){
		$res=$this->db->insert("dd_templateGroup",$data);
		//echo $this->db->last_query();
		return $res ? array('res' => 'succ', "dd_templateGroup" . '_id' => $this->db->insert_id()) : array('res' => 'fail');
  	}
	
	public function t_del( $id )
	{
		$this->db->where('templateGroup_id',$id)->delete("dd_t_g");
		return $this->db->affected_rows();
	}
	
	public function t_add( $data )
	{
		$res = $this->db->insert("dd_t_g" , $data);
		return $res ? array('res' => 'succ', "dd_t_g" . '_id' => $this->db->insert_id()) : array('res' => 'fail');
	}
	
	public function update($id, $data)
	{
		return $this->db->where('templateGroup_id', $id)->update("dd_templateGroup" , $data);
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where=""){
		//首先查询出本身所有的内容
		if ($where==""){
			$data=$this->db->from("dd_templateGroup")->get()->result_array();
		}else{
			$data=$this->db->from("dd_templateGroup")->where($where)->get()->result_array();
			//echo $this->db->last_query();
		}
		return $data;
	}
	
	//获取执行的结果数
	public function getRowNumber($where="")
	{
		//echo "dd";
		if ($where==""){
			$this->db->from("dd_t_g");
		}else{
			$this->db->from("dd_t_g")->where($where);
		}
		//echo $this->db->last_query();
		return $this->db->count_all_results();
	}

	//根据条件查询关联表数据
	public function check( $where )
	{
		//$array =debug_backtrace();
		if( $where != "" )
		{
			//p($where);
			$res = $this->db->select('B.template_id,B.template_name,B.template_path,B.template_type,B.create_time,B.update_time')->from("dd_t_g as A")->join("dd_template as B", "A.template_id=B.template_id")->where($where)->get()->result_array();
			//echo $this->db->last_query()."<br/>";
		}
		return $res;
	}

	//获取关联模板信息
  	public function tglist_template( $id , $type="" )
  	{
		$where = empty($type)?array("templateGroup_id"=>$id):array("templateGroup_id" => $id,"template_type"=>$type);
  		$info = $this->check( $where );
		if(empty($type))
		{
			foreach($info as $key => $value)
			{
				$res[$key][$value['template_type']]=$value;
			}
			return $res;
		}
  		return $info;
  	}
	
	//模板关联插入
	public function _insert($data, $templategroup_id="")
	{
		$this->db->trans_begin();
		
		$where = array("templateGroup_name"=>$data["templategroup_name"],"templateGroup_desc"=>$data["templategroup_desc"]);
		unset($data["templategroup_name"]);
		unset($data["templategroup_desc"]);
		if( empty( $templategroup_id ) )
		{
			$res=$this->add($where);
			if( $res['res']=="succ" )
			{
				$templategroup_id=$res["dd_templateGroup_id"];
			}
			else
			{
			//echo "asdfasdf";
				$this->db->trans_rollback();
				$this->db->close();
				//exit;
				return false;
				die();
			}
		}
		
		$result = $this->parseData( $data );
		
		$result_values=array_values($data);
		
		$t_g_where=array("templateGroup_id"=>$templategroup_id);
		
		$rownumber=$this->getRowNumber($t_g_where);
		
		if( $rownumber!=0 )
		{
			if( !empty($result_values) )
			{
				$affected_rows = $this->t_del( $templategroup_id );
			}
			if( $affected_rows == 0 )
			{
				$this->db->trans_rollback();
				$this->db->close();
				return false;
				die();
			}
		}
		
		foreach($result as $key => $value)
		{
			foreach($value as $k => $v)
			{	
				if( !empty($v) )
				{
					$values["templateGroup_id"] = $templategroup_id;
					$values["template_id"] = $v;
					$res=$this->t_add($values);
					echo $this->db->last_query();
					if($res['res']=="fail")
					{
						$this->db->trans_rollback();
						$this->db->close();
						return false;
					}
				}
			}				
		}
		
		$this->update($templategroup_id, $where);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->db->close();
		} else {
			$this->db->trans_commit();
			$this->db->close();
			return true;
		}
	}
	
	//模板数组解析
	private function parseData( $res )
	{
		$result = array();
		$attribute = $this->libattrible();
		if(is_array($res))
		{
			foreach( $attribute as $key => $lvalue )
			{
				array_push($result, explode( ",", $res["text_".$key] ));
			}
		}
		return $result;
	}
	

}
?>