<?php

class Api_error_model extends CI_Model {

	public function add($object = '', $action = '', $data = '') {
		$error_data['atime'] = date('Y-m-d H:m:s', time());
		$error_data['object'] = $object;
		$error_data['action'] = $action;
		$error_data['data'] = json_encode($data);
		$res_id = $this->db->insert('tc_api_error', $error_data);
		if (!$res_id) {
			$error_data['atime'] = date('Y-m-d H:m:s', time());
			$error_data['object'] = $error_data['action'] = '';
			$error_data['data'] = '日志生成失败';
			$this->db->insert('tc_api_error', $error_data);
		}
	}

}

?>