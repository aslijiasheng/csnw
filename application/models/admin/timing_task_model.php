<?php
class Timing_task_model extends CI_Model{

	//这里代表那些字段在列表上显示出来
	public function listLayout(){
		return array(
			'timing_task_do_time',
			'timing_task_url',
			'timing_task_note',
			'timing_task_status',
			'timing_task_name',
			'timing_task_params',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout(){
		return array(
			'timing_task_do_time',
			'timing_task_url',
			'timing_task_note',
			'timing_task_status',
			'timing_task_name',
			'timing_task_params',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout(){
		return array(
			'timing_task_do_time',
			'timing_task_url',
			'timing_task_note',
			'timing_task_status',
			'timing_task_name',
			'timing_task_params',
		);
	}
	
	public function viewLayout_config()
	{
		return array(
			"config_id",
			"config_datetime",
			"config_status",
			"config_difftime",
		);
	}

	//属性对应的中文标签
	public function attributeLabels(){
		return array(
			'timing_task_next_time' => '下次执行时间',
			'timing_task_do_time' => '执行时间',
			'timing_task_url' => '链接',
			'timing_task_note' => '说明',
			'timing_task_status' => '状态',
			'timing_task_name' => '名称',
			'timing_task_params' => '参数',
		);
	}
	
	public function attributeLabels_config()
	{
		return array(
			"config_id" => "主键ID",
			"config_datetime" => "定时任务上次执行时间",
			"config_status" => "是否开启",
			"config_difftime" => "间隔时间",
		);
	}
	
	/**
	对配置表的查询 更新
	**/
	public function listconfigGetInfo()
	{
		$data=$this->db->from("dd_config")->get()->result_array();
		return $data['0'];
	}
	
	public function upconfig($data)
	{
		$this->db->update('dd_config',$data);
		return $this->db->affected_rows();
	}

	//列表查询
	/*
		$page //获得当前的页面值
		$perNumber //每页显示的记录数
	*/
	public function listGetInfo($where,$page=1,$perNumber=10,$like=""){
		$limitStart = ($page-1)*$perNumber+1-1; //这里因为数据库是从0开始算的！所以要减1
		$limit=array($perNumber,$limitStart);
		//p($limit);
		$data = $this->GetInfo($where,$limit,$like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id){
		$where=array(
			'timing_task_id'=>$id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data,$id){
		$this->db->where('timing_task_id',$id)->update('tc_timing_task',$data);
	}

	//单个删除
	public function del($id){
		$this->db->where('timing_task_id',$id)->delete('tc_timing_task');
	}

	//单个新增
	public function add($data){
		$this->db->insert('tc_timing_task',$data);
		$insert_id=$this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where="",$like=""){
		$this->db->from('tc_timing_task');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}


	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where="",$limit="",$like=""){
		//首先查询出本身所有的内容
		$this->db->from('tc_timing_task');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($limit!=""){
			$this->db->limit($limit[0],$limit[1]);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$data=$this->db->get()->result_array();
		//然后循环查询出所有引用的内容

		foreach ($data as $k=>$v){
			//p($v);die;
			//所有属性循环
			//当属性类型为单选时
			if($v['timing_task_status']!="" and $v['timing_task_status']!=0){
				$this->load->model('admin/enum_model','enum');
				$data[$k]['timing_task_status_arr']=$this->enum->getlist(607, $data[$k]['timing_task_status']);
			}
		}
		return $data;
	}
}
?>
