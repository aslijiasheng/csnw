<?php
class Layout_model extends CI_Model{
	public function add($data){
		$this->db->insert('dd_layout',$data);
	}
	public function update($data,$type_id,$obj_id,$layout_type){
		$arr=array(
			'layout_json'=>$data
		);
		$this->db->where(array('type_id'=>$type_id,'obj_id'=>$obj_id,'layout_type'=>$layout_type))->update('dd_layout',$arr);
	}
	public function attr_id($type_id,$layout_type,$obj_id){
		return $this->db->get_where('dd_layout',array('type_id'=>$type_id,'obj_id'=>$obj_id,'layout_type'=>$layout_type))->result_array();
	}
	public function check_choosed_attrs($obj_id,$choosed_attr_id = array()){
		if(empty($choosed_attr_id)){
		 return $this->db->get_where('dd_attribute',array('attr_obj_id'=>$obj_id))->result_array();
		}
		else{
		  return  $this->db->where(array('attr_obj_id'=>$obj_id))->where_not_in('attr_id',$choosed_attr_id)->get('dd_attribute')->result_array();
		}
	}

	//数组单个查询
	public function arr_getinfo($where){
		$getinfo = $this->db->get_where('dd_layout',$where)->result_array();
		//p($getinfo);
		$getinfo = $getinfo[0];
		//这里需要把查询出来的json数组转化成数组展示出来
		//echo $getinfo['layout_json'];
		$getinfo['layout_arr'] = json_decode($getinfo['layout_json'],true);
		//循环将里面的属性ID转换成属性arr
		foreach ($getinfo['layout_arr'] as $k=>$v){
			//attr_id引用属性
			if($v['attr_id']!="" and $v['attr_id']!=0){
				$this->load->model('admin/attribute_model','attr');
				$getinfo['layout_arr'][$k]['attr_id_arr'] = $this->attr->getinfo($v['attr_id']);
			}
		}
		return $getinfo;
	}
}
?>