<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu_model extends CI_Model {

    var $title = '';

    public function attributeLabels() {
        return array(
            'menu_id' => '菜单ID',
            'menu_name' => '菜单名称',
            'menu_uid' => '父级菜单ID',
            'menu_url' => '菜单路径',
            'menu_label' => '菜单标签',
            'menu_is_js' => '是否定义JS方法(0:否; 1:是)',
            'menu_js_content' => 'JS内容',
            'menu_icon' => '菜单图标',
            'menu_path' => '菜单层级',
            'menu_order' => '菜单排序',
        );
    }

    // false 返回空数组
    public function menulist() {
        $this->db->order_by('menu_path, menu_order');
        $rows = $this->db->get('dd_menu')->result_array();
        return $rows ? $rows : array();
    }

    public function add($data) {
        $this->db->insert('dd_menu', $data);
        return $this->db->insert_id();
    }

    public function update($data, $menu_id) {
        $this->db->where('menu_id', $menu_id)->update('dd_menu', $data);
    }

    public function getinfo($menu_id) {
        $rows = $this->db->get_where('dd_menu', array('menu_id' => $menu_id))->result_array();
        return $rows[0];
    }

    public function getuidinfo($menu_id){
        $this->db->order_by('menu_path, menu_order');
        return $rows = $this->db->get_where('dd_menu', array('menu_uid' => $menu_id))->result_array();
    }

    public function del($menu_id) {
        return $this->db->delete('dd_menu', array('menu_id' => $menu_id));
    }

    public function getorder($data) {
        $parr = explode(',', str_replace('-', ',', $data['menu_path']));
        return ltrim($parr[count($parr) - 2], '0');
    }

}

?>