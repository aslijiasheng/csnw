<?php 
class Layout_model extends CI_Model{
	public function add($data){
		$this->db->insert('dd_layout',$data);
	}

	public function select_list($obj_id,$type_id=""){
		$this->db->select('layout_json');
		$obj_arr = $this->db->get_where('dd_layout',array('obj_id'=>$obj_id,'layout_type'=>'list'))->result_array();
		return $obj_arr[0]['layout_json'];
	}
}
 ?>