<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Attrtype_model extends CI_Model {

    public function check() {
        return $this->db->get_where("dd_attrtype")->result_array();
    }

    public function add_type($data) {
        $this->db->insert("dd_attrtype", $data);
    }

    public function getinfo($attrtype_id) {
        $getinfo = $this->db->get_where("dd_attrtype", array('attrtype_id' => $attrtype_id))->result_array();
        return $getinfo[0];
    }

    public function edit($data, $attrtype_id) {
        $this->db->where('attrtype_id', $attrtype_id)->update("dd_attr_type", $data);
    }

    public function getList() {
        $this->db->select('attrtype_id,attrtype_field_type');
        $res = $this->db->get('dd_attrtype')->result_array();
        foreach ($res as $v) {
//            $v['attrtype_field_type'] = trim(preg_replace('/\(.*\)/', '', $v['attrtype_field_type']));
//            switch ($v['attrtype_field_type']) {
//                case 'int':
//                    $v['attrtype_field_type'] = 'integer';
//                    break;
//                case 'varchar':
//                    $v['attrtype_field_type'] = 'string';
//                    break;
//                case 'decimal':
//                    $v['attrtype_field_type'] = 'double';
//                    break;
//                default:
//                    $v['attrtype_field_type'] = 'string';
//                    break;
//            }
            $result[$v['attrtype_id']] = trim($v['attrtype_field_type']);
        }
        return $result;
    }

}

?>