<?php
class Icon_model extends CI_Model{
	var $title = '';
	//列表查询
	public function iconlist(){
		$icon_arr = $this->db->get_where('dd_icon')->result_array();
		return $icon_arr;
	}
	//属性对应的中文标签
	public function attributeLabels()
	{
		return array(
			'obj_id' => '对象ID',
			'obj_uid' => '父级对象ID',
			'obj_name' => '对象名称',
			'obj_label' => '中文标签',
			'obj_icon' => '对象图标',
			'is_obj_type' => '是否定义对象类型',
			'is_ref_obj' => '是否可以被引用',
			'is_detail' => '是否包含明细',
			'create_time' => '创建时间',
			'update_time' => '最后更新时间'
		);
	}

	//单个查询
	public function getinfo($obj_id){
		$getinfo = $this->db->get_where('dd_object',array('obj_id'=>$obj_id))->result_array();
		foreach ($getinfo as $key=>$value){
			if ($value['is_detail']==1){
				$getinfo[$key]['object'] = $this->db->get_where('dd_object',array('obj_uid'=>$value['obj_id']))->result_array();
			}
		}

		return $getinfo[0];
	}

	//单个修改
	public function update_obj($obj_id,$data){
		$this->db->where('obj_id',$obj_id)->update('obj',$data);
	}
}
?>