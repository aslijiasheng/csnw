<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Detail_model extends CI_Model {

	var $title = '';

	// 根据 obj_id、type_id 获取所有明细
	public function getList($obj_id, $type_id = 0) {
		$rows = $type_id ? $this->db->get_where('dd_detail', array('obj_id' => $obj_id, 'type_id' => $type_id))->result_array() : $rows = $this->db->get_where('dd_detail', array('obj_id' => $obj_id))->result_array();

		//获取当前对象内容
		$obj = $this->db->select('obj_id, obj_name, obj_label')->get_where('dd_object', array('obj_id' => $obj_id))->row_array();
		foreach ($rows as $k => $v) {
			if ($v['type_id']) {
				$sql = 'SELECT * FROM dd_attribute a, dd_object o, dd_type t '
						. 'WHERE a.attr_id=' . $v['detail_attr_id'] . ' AND o.obj_id=' . $v['detail_obj_id'] . ' AND t.type_id=' . $v['type_id'] . '';
			} else {
				$sql = 'SELECT * FROM dd_attribute a, dd_object o '
						. 'WHERE a.attr_id=' . $v['detail_attr_id'] . ' AND o.obj_id=' . $v['detail_obj_id'] . '';
			}
			$rows[$k]['obj'] = $obj;
			$rows[$k]['detailArr'] = $this->db->query($sql)->row_array();
		}
		return $rows;
	}

	// 添加明细
	public function addDetail($data) {
		if ($data['obj_id'] && $data['detail_obj_id']) {
			$this->db->insert('dd_detail', $data);
		}
	}

	// 更新明细
	public function updateDetail($id, $data) {
		return $this->db->where('id', $id)->update('dd_detail', $data);
	}

	// 删除明细记录
	public function delDetail($id) {
		return $this->db->delete('dd_detail', array('id' => $id));
	}

	// 获取字段
	public function columns_list($detail_obj_id) {
		return $this->db->get_where('dd_attribute', array('attr_obj_id' => $detail_obj_id, 'attr_type' => '19'))->result_array();
	}

	// 获取单个明细详细内容
	public function getDetailInfo($detail_id) {
		$row = $this->db->get_where('dd_detail', array('id' => $detail_id))->row_array();
		if ($row) {
			$result = $this->getList($row['obj_id']);
			return $result['0'];
		}
	}

}

?>