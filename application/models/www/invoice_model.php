<?php

class Invoice_model extends CI_Model {

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'invoice_sale_id',
			'invoice_order_id',
			'invoice_invoice_no',
			'invoice_customer_type',
			'invoice_title',
			'invoice_address',
			'invoice_tel',
			'invoice_bank',
			'invoice_account',
			'invoice_content',
			'invoice_amount',
			'invoice_addtime',
			'invoice_untime',
			'invoice_memo',
			'invoice_taxpayer',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'invoice_type',
			'invoice_customer_type',
			'invoice_title',
			'invoice_taxpayer',
			'invoice_invoice_no',
			'invoice_addtime',
			'invoice_untime',
			'invoice_bank',
			'invoice_tel',
			'invoice_address',
			'invoice_memo',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'invoice_sale_id' => '销售ID',
			'invoice_order_id' => '订单ID',
			'invoice_type' => '票据类型',
			'invoice_invoice_no' => '票据号码',
			'invoice_code' => '票据编码',
			'invoice_customer_type' => '客户类型',
			'invoice_title' => '票据抬头',
			'invoice_address' => '地址',
			'invoice_tel' => '电话',
			'invoice_bank' => '开户行',
			'invoice_content' => '票据内容',
			'invoice_amount' => '票据金额',
			'invoice_addtime' => '开票时间',
			'invoice_untime' => '票据作废时间',
			'invoice_apply_untime' => '申请作废时间',
			'invoice_memo' => '开票备注',
			'invoice_taxpayer' => '纳税人识别号',
			'invoice_uninvoice_status' => '作废状态',
			'invoice_process_status' => '审核状态',
			'invoice_do_status' => '开票状态',
			'invoice_status' => '票据状态',
			'invoice_apply_uninvoice_note' => '申请作废备注',
			'invoice_uninvoice_note' => '作废备注',
			'invoice_process_note' => '审核备注',
			'invoice_do_note' => '开票备注',
			'invoice_reviewer' => '业务负责人',
			'invoice_account' => '账号',
			'invoice_subsidiary' => '所属子公司',
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'invoice_id' => $id
		);
		$data = $this->GetInfo($where);
		if (!empty($data)) {
			$data = $data[0];
		}
		return $data;
	}

	//单个修改
	public function update($data, $id) {
		$this->db->where('invoice_id', $id)->update('tc_invoice', $data);
	}

	//单个删除
	public function del($id) {
		$this->db->where('invoice_id', $id)->delete('tc_invoice');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_invoice', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//只查当前表数据非关联
	public function GetResultInfo($fields = "*", $where = "", $orderby="") {
		if ( !empty($orderby) ) {
			list($title, $desc) = explode(",", $orderby);
		}
		$this->db->from("tc_invoice");
		$this->db->select($fields);
		if ($where != "") {
			$this->db->where($where);
		}
		if ($orderby != "") {
			$this->db->order_by($title, $desc);
		}
		$data = $this->db->get()->result_array();
		return $data;
	}

	/**
	* 例：查询汇总纪录
	* @params $data 要载入的数据条件
	* @return int   返回的汇总数据
	*/
	public function SelectSum($data) {
		extract($data,EXTR_OVERWRITE);
		$this->db->from('tc_invoice');
		$this->db->select_sum($fields, "sum");
		if ($where != "") {
			$this->db->where($where);
		}
		$data = $this->db->get()->result_array();
		if ($data[0]['sum'] == "") {
			$data[0]['sum'] = 0;
		}
		return $data[0]['sum'];
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_invoice');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_invoice');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		//p($where);
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容

		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环
			if ($v['invoice_order_id'] != "" and $v['invoice_order_id'] != 0) {
				$this->load->model('www/order_model', 'order');
				$data[$k]['invoice_order_id_arr'] = $this->order->id_aGetInfo($v['invoice_order_id']);
			}
			//当属性类型为单选时
			if ($v['invoice_type'] != "" and $v['invoice_type'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['invoice_type_arr'] = $this->enum->getlist(155, $data[$k]['invoice_type']);
			}
			
			//所属子公司
			if ($v['invoice_subsidiary'] != "" and $v['invoice_subsidiary'] != 0) {
				$this->load->model('admin/enum_model', 'enum');
				$data[$k]['invoice_subsidiary_arr'] = $this->enum->getlist(630, $data[$k]['invoice_subsidiary']);
			}


			//当属性类型为单选时
			if ($v['invoice_customer_type'] != "" and $v['invoice_customer_type'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['invoice_customer_type_arr'] = $this->enum->getlist(157, $data[$k]['invoice_customer_type']);
			}

			if ($v['invoice_uninvoice_status'] != "" and $v['invoice_uninvoice_status'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['invoice_uninvoice_status_arr'] = $this->enum->getlist(443, $data[$k]['invoice_uninvoice_status']);
			}

			//当属性类型为单选时
			if ($v['invoice_process_status'] != "" and $v['invoice_process_status'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['invoice_process_status_arr'] = $this->enum->getlist(444, $data[$k]['invoice_process_status']);
			}
			//当属性类型为单选时
			if ($v['invoice_do_status'] != "" and $v['invoice_do_status'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['invoice_do_status_arr'] = $this->enum->getlist(445, $data[$k]['invoice_do_status']);
			}

			if ($v['invoice_status'] != "" and $v['invoice_status'] != 0) {
				$this->load->model('admin/enum_model', 'enum');
				$data[$k]['invoice_status_arr'] = $this->enum->getlist(567, $data[$k]['invoice_status']);
			}
		}
		return $data;
	}

	public function info($data) {
		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环
			//当属性类型为单选时
			if ($v['invoice_type'] != "" and $v['invoice_type'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['invoice_type_arr'] = $this->enum->getlist(155, $data[$k]['invoice_type']);
			}
			//当属性类型为单选时
			if ($v['invoice_customer_type'] != "" and $v['invoice_customer_type'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['invoice_customer_type_arr'] = $this->enum->getlist(157, $data[$k]['invoice_customer_type']);
			}
			if ($v['invoice_uninvoice_status'] != "" and $v['invoice_uninvoice_status'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['invoice_uninvoice_status_arr'] = $this->enum->getlist(443, $data[$k]['invoice_uninvoice_status']);
			}
			//当属性类型为单选时
			if ($v['invoice_process_status'] != "" and $v['invoice_process_status'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['invoice_process_status_arr'] = $this->enum->getlist(444, $data[$k]['invoice_process_status']);
			}
			//当属性类型为单选时
			if ($v['invoice_do_status'] != "" and $v['invoice_do_status'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['invoice_do_status_arr'] = $this->enum->getlist(445, $data[$k]['invoice_do_status']);
			}
		}
		return $data;
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('invoice');
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $user['user_id'];
			unset($data['user']);
		}

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
					$json = $this->arrJson($v);
					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}
		
		$res = $this->db->insert('tc_invoice', $save_data);
		//echo "-------------1111--------";
		//var_dump($save_data['invoice_status']);
		//添加待办
		if ($save_data['invoice_status'] == 1001) {
			$invoice_id = $this->db->insert_id();
			$this->load->model('www/order_model', 'order');
			$order_data = $this->order->id_aGetInfo($save_data['invoice_order_id']);
			$this->load->model('www/message_model', 'message');
			$message_data = array(
				'message_owner' => $order_data['order_finance'],
				'message_module' => 'invoice',
				'message_module_id' => $invoice_id,
				'message_type' => 1002,
				'message_url' => 'www/invoice/confirm_invoice',
				'message_department' => $order_data['order_finance_department']
			);
			//echo "<hr/>";
			//var_dump($message_data);
			//echo "<hr/>";
			$this->message->add($message_data);
		}

		//结束
		if ($res) {
			return array('res' => 'succ', 'msg' => '');
		} else
			return array('res' => 'fail', 'msg' => 'error invoice save');
	}

	private function arrJson($v) {
		$json = '';
		foreach ($v as $kk => $vv) {
			if (strpos($kk, '.')) {
				list($fields, $attr) = explode('.', $kk);
				if ($fields == 'content_class') {
					$res_enum = $this->db->select('enum_key')->get_where('dd_enum', array('enum_name' => $vv, 'attr_id' => '428'))->row_array();
					if (!$res_enum)
						return array('res' => 'fail', 'msg' => 'error [' . $kk . ' : ' . $vv . ']');
					$json[$fields] = $res_enum['enum_key'];
				}
			} else {
				switch ($kk) {
					case 'service_select':
						if (strpos($vv['goods'], ',')) {
							$goods_arr = explode(',', $vv['goods']);
							foreach ($goods_arr as $gk => $gv) {
								$json['service_select'][$gv] = '1';
							}
						} else {
							$json['service_select'][$vv['goods']] = '1';
						}
						$json['other_price']['1001'] = $vv['other_price'];
						break;
					case 'net_service_select':
						if (strpos($vv['goods'], ',')) {
							$goods_arr = explode(',', $vv['goods']);
							foreach ($goods_arr as $gk => $gv) {
								$json['net_service_select'][$gv] = '1';
							}
						} else {
							$json['net_service_select'][$vv['goods']] = '1';
						}
						$json['other_price']['1002'] = $vv['other_price'];
						break;
					default:
						$json[$kk] = $vv;
						break;
				}
			}
		}
		return $json;
	}

	public function api_update($data, $where) {
		$update_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('invoice');
		$object = $objDetail['obj'];

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $object . '_' . $fields;
				if (!$this->db->field_exists($this_fields, 'tc_' . $object)) {
					return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
				}

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$update_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$update_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $object . '_' . $k;
				if (!$this->db->field_exists($this_fields, 'tc_' . $object)) {
					return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
				}

				//内容是数组，将内容组成 json
				if (is_array($v)) {
//					$json = '';
//					$update_data[$this_fields] = json_encode($json);
				} else
					$update_data[$this_fields] = $v;
			}
		}

		//添加待办
		if ($update_data['invoice_status'] == 1001) {
			//查询出对应的ID
			$order_data = $this->GetInfo($where);
			//p($order_data);exit;
			$invoice_id = $order_data[0]['invoice_id'];
			$this->load->model('www/order_model', 'order');
			$order_data = $this->order->id_aGetInfo($update_data['invoice_order_id']);
			//p($order_data['order_finance_department']);exit;
			$this->load->model('www/message_model', 'message');
			$message_data = array(
				'message_owner' => $order_data['order_finance'],
				'message_module' => 'invoice',
				'message_module_id' => $invoice_id,
				'message_type' => 1002,
				'message_url' => 'www/invoice/confirm_invoice',
				'message_department' => $order_data['order_finance_arr']['user_department']
			);
			//p($message_data);
			$this->message->add($message_data);
		}
		//结束

		$res = $this->db->update('tc_' . $object, $update_data, $where);
		if ($res)
			return array('res' => 'succ', 'msg' => 'update success');
		else
			return array('res' => 'fail', 'msg' => 'update failure');
	}

	// 开票金额验证
	public function checkAmount($data) {
		$r_order = $this->db->select('order_amount,order_rebate_amount')->get_where('tc_order', array('order_id' => $data['invoice_order_id']))->row_array();
		$sql = 'SELECT invoice_amount FROM tc_invoice WHERE invoice_order_id=' . $data['invoice_order_id'] . ' AND (invoice_status = 1001 or invoice_status = 1002)';
		$r_invoice = $this->db->query($sql)->result_array();
		//p($sql);
		//p($r_invoice);//exit;
		foreach ($r_invoice as $v) {
			//$r_order['order_amount'] -= $v['invoice_amount'];
			$r_order['order_amount'] = bcsub($r_order['order_amount'], $v['invoice_amount'], 2);
		}
		if (floatval($r_order['order_amount']) >= floatval($data['invoice_amount'])) {
			return true;
		} else {
			return false;
		}

		exit;
	}

	//2014-07-17 api start
	public function _addCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无开票数据');
		return $this->api->_return(__class__ . ' _addCheck succ', $data, 'succ');
	}

	public function _updateCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无开票数据');
		return $this->api->_return(__class__ . ' _updateCheck succ', $data, 'succ');
	}
	//2014-07-17 api end
	
	/**
	* @例:数据查询
	* @params $data 待查询的数据条件
	* @params return array 返回的数据集结果
	*/
	public function listGetInfoIn( $data ) {
		return $this->db->from("tc_invoice")->where_in( "invoice_order_id", $data )->get()->result_array();
	}
	
	/**
	* @例:数据删除
	* @params $invoice_order_id 待查询的数据条件
	* @retyrb 无
	*/
	public function _deleteInfo( $invoice_order_id ) {
		$this->db->where_in( "invoice_order_id", $invoice_order_id )->delete("tc_invoice");
	}
}

?>
