<?php
class User_model extends CI_Model{

	//这里代表那些字段在列表上显示出来
	public function listLayout(){
		return array(

			'user_{#attr_name#}',

			'user_{#attr_name#}',

			'user_{#attr_name#}',

			'user_{#attr_name#}',

			'user_{#attr_name#}',

			'user_{#attr_name#}',

			'user_{#attr_name#}',

			'user_{#attr_name#}',

		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout(){
		return array(

			'user_name',

			'user_password',

			'user_email',

			'user_sex',

			'user_department',

			'user_gonghao',

			'user_duty_name',

			'user_status',

			'user_login_name',

		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout(){
		return array(

			'user_name',

			'user_sex',

			'user_email',

			'user_department',

			'user_gonghao',

			'user_duty_name',

			'user_login_name',

			'user_status',

		);
	}

	//属性对应的中文标签
	public function attributeLabels(){
		return array(

			'user_name' => '用户名',

			'user_sex' => '性别',

			'user_password' => '密码',

			'user_email' => '邮箱',

			'user_department' => '所属部门',

			'user_gonghao' => '工号',

			'user_duty_name' => '职位名称',

			'user_status' => '状态',

			'user_login_name' => '登录名',

		);
	}

	//列表查询
	/*
		$page //获得当前的页面值
		$perNumber //每页显示的记录数
	*/
	public function listGetInfo($where,$page=1,$perNumber=10,$like=""){
		$limitStart = ($page-1)*$perNumber+1-1; //这里因为数据库是从0开始算的！所以要减1
		$limit=array($perNumber,$limitStart);
		//p($limit);
		$data = $this->GetInfo($where,$limit,$like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id){
		$where=array(
			'user_id'=>$id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data,$id){
		$this->db->where('user_id',$id)->update('tc_user',$data);
	}

	//单个删除
	public function del($id){
		$this->db->where('user_id',$id)->delete('tc_user');
	}

	//单个新增
	public function add($data){
		$this->db->insert('tc_user',$data);
		$insert_id=$this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where="",$like=""){
		$this->db->from('tc_user');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}


	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where="",$limit="",$like=""){
		//首先查询出本身所有的内容
		$this->db->from('tc_user');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($limit!=""){
			$this->db->limit($limit[0],$limit[1]);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$data=$this->db->get()->result_array();
		//然后循环查询出所有引用的内容
		
		foreach ($data as $k=>$v){
			//p($v);die;
			//所有属性循环
			
			//删除用户密码  2014 08 21	
			//if (isset($v['user_password'])) {
			//	unset($data[$k]['user_password']);
			//}
			//当属性类型为单选时
			if($v['user_sex']!="" and $v['user_sex']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['user_sex_arr']=$this->enum->getlist(64,$data[$k]['user_sex']);
			}


			if($v['user_department']!="" and $v['user_department']!=0){
				$this->load->model('www/department_model','department');
				$data[$k]['user_department_arr'] = $this->department->id_aGetInfo($v['user_department']);
			}

				 //当属性类型为下拉单选时
			if($v['user_duty_name']!="" and $v['user_duty_name']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['user_duty_name_arr']=$this->enum->getlist(69,$data[$k]['user_duty_name']);
			}

				 //当属性类型为单选时
			if($v['user_status']!="" and $v['user_status']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['user_status_arr']=$this->enum->getlist(70,$data[$k]['user_status']);
			}

		}
		// if(!empty($where)){
		// 	$role_info = $this->db->get_where('tc_role_user',$where)->result_array();
		// //$data['role_info'] = $role_info;
		// 	if(!empty($role_info)){
		// 					foreach($role_info as $arr){

		// 						$tem = $this->db->select('role_id,role_menu_auth,role_activity_auth,role_data_auth')->from('tc_role')->where('role_id',$arr['role_id'])->get()->result_array();
		// 						$auth_arr[] = $tem[0];

		// 					}
		// 	}
		// 	$data[0]['user_auth'] = $auth_arr;
		// 	///p($data);
		// 	// $menu_auth_arr = $this->menu_authorization($data[0]['user_auth']);
		// 	// $activity_auth_arr = $this->activity_authorization($data[0]['user_auth']);
		// 	// $data[0]['menu_auth_arr'] = $menu_auth_arr;
		// 	// $data[0]['activity_auth_arr'] = $activity_auth_arr;
		// }

		//p($data['auth_arr']);
		//$this->load->model('www/role_model','role');
		//$auth_arr = $this->role->
		//$data['']
		//p($data);
		return $data;
	}

	public function user_auth($id){
		 //echo "id:".$id;die;
			$role_info = $this->db->get_where('tc_role_user',array('user_id'=>$id))->result_array();
			//p($role_info);
			$menu_auth_arr = array();
			$activity_auth_arr = array();
			  $data_auth_arr = array();
			//p($role_info);
			//die;
			if(!empty($role_info)){
							foreach($role_info as $arr){

								$tem = $this->db->select('role_id,role_menu_auth,role_activity_auth,role_data_auth')->from('tc_role')->where('role_id',$arr['role_id'])->get()->result_array();
								$auth_arr[] = $tem[0];

							}
						$user_auth = $auth_arr;
					//p($user_auth);
				if (!empty($user_auth)) {

					foreach ($user_auth as $arr) {
						$menu_auth_arr = bingji($menu_auth_arr, json_decode($arr['role_menu_auth']));
					}
				}


			if (!empty($user_auth)) {
				foreach ($user_auth as $arr) {
					//p($user_auth);
					//p(object_array(json_decode($arr['role_activity_auth'])));
					//p(json_decode($arr['role_activity_auth']));
					$activity_auth_arr = bingji($activity_auth_arr, json_decode($arr['role_activity_auth']));
				}
			}



			if (!empty($user_auth)) {
				//p($user_auth);
				foreach ($user_auth as $arr) {
					$arr['role_data_auth'] = json_decode($arr['role_data_auth']);
					//p($arr['role_data_auth']);
					$data_auth_arr = bingji($data_auth_arr, $arr['role_data_auth']);
				}
			}

			}


		    $data['menu_auth_arr'] = $menu_auth_arr;
		    $data['activity_auth_arr'] = $activity_auth_arr;
		    $data['data_auth_arr'] = $data_auth_arr;
		    //p($data['data_auth_arr']);
		    return $data;

	}

	public function roleGetInfo($id,$like=""){
		//p($like);
		//将like转换成判断条件加上去
		$like_str = "";
		if($like!=""){
			foreach ($like as $k=>$v){
				$like_str .= 'and '.$k." like '%".$v."%'";
			}
		}
		$sql = "select * from tc_user where user_id in(SELECT user_id FROM `tc_role_user` WHERE role_id = ".$id.") ".$like_str;
		$data = $this->db->query($sql)->result_array();
		foreach ($data as $k=>$v){
			//p($v);die;
			//所有属性循环

				 //当属性类型为单选时
			if($v['user_sex']!="" and $v['user_sex']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['user_sex_arr']=$this->enum->getlist(64,$data[$k]['user_sex']);
			}

			if($v['user_department']!="" and $v['user_department']!=0){
				$this->load->model('www/department_model','department');
				$data[$k]['user_department_arr'] = $this->department->id_aGetInfo($v['user_department']);
			}

				 //当属性类型为下拉单选时
			if($v['user_duty_name']!="" and $v['user_duty_name']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['user_duty_name_arr']=$this->enum->getlist(69,$data[$k]['user_duty_name']);
			}
				 //当属性类型为单选时
			if($v['user_status']!="" and $v['user_status']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['user_status_arr']=$this->enum->getlist(70,$data[$k]['user_status']);
			}
		}
		return $data;
	}
	//该角色数据总数
	public function countRole($id,$like=""){
		//将like转换成判断条件加上去
		$like_str = "";
		if($like!=""){
			foreach ($like as $k=>$v){
				$like_str .= 'and '.$k." like '%".$v."%'";
			}
		}
		$sql = "select count(*) lee_count from tc_user where user_id in(SELECT user_id FROM `tc_role_user` WHERE role_id = ".$id.") ".$like_str;
		$data = $this->db->query($sql)->result_array();
		$count = $data[0]['lee_count'];
		return $count;
	}

}
?>
