
<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Sequence_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->db->query('set names utf8');
		date_default_timezone_set('PRC');
		$this->db->trans_strict(FALSE);
	}

	/**
	 * 接口数据更新
	 * @param array $data
	 * @param string $object
	 * @return array
	 */
	public function _update($data) {
        $where = $data['where'];
        $up_data = $data['data'];
		$res = $this->db->update('tc_object_sequence', $up_data, $where);
		return $res ? array('res' => 'succ', $object . '_id' => $this->db->affected_rows()) : array('res' => 'fail');
	}


    /**
     * get_sequence 
     * 获取sequence序号
     * @param mixed $sequence_name 序号名称
     * @access public
     * @return array
     */
    public function get_sequence($sequence_name = '') {
        //首先查询出本身所有的内容
        $this->db->from('tc_object_sequence');
        if (!empty($sequence_name)) {
            $this->db->where('object_sequence_sequence_name', $sequence_name);
        }
        $data = $this->db->get()->result_array();
        return $data;
    } 
}

?>
