<?php

class Partner_model extends CI_Model {

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'partner_name',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'partner_partner_name',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
			'partner_name',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'partner_name' => '伙伴名称',
			'partner_shopex_id' => '企业ID',
			'partner_department' => '所属部门',
			'partner_manager' => '渠道经理',
			'partner_level' => '合作级别',
			'partner_disc' => '对应折扣',
			'partner_CooperativeTime' => '合作日期',
			'partner_endingdate' => '到期时间',
			'partner_contacts' => '联系人',
			'partner_email' => '邮箱',
			'partner_address' => '联系地址',
			'partner_fax' => '传真',
			'partner_region' => '所属区域',
			'partner_contract' => '签约负责人',
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'partner_id' => $id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单条查询
	public function validation($data) {
		extract($data, EXTR_OVERWRITE);
		$fields = empty($fields)?"*":$fields;
		$this->db->select($fields);
		$this->db->from('tc_partner');
		$this->db->where($where);
		if ($where!="") {
			$this->db->where($where);
		}
		$get_obj = $this->db->get();
		$data    = $get_obj->result_array();
		return $data[0];
	}

	//单个修改
	public function update($data, $id) {
		$this->db->where('partner_id', $id)->update('tc_partner', $data);
	}

	//单个删除
	public function del($id) {
		$this->db->where('partner_id', $id)->delete('tc_partner');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_partner', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_partner');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_partner');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容

		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环
			if ($v['partner_department'] != "" and $v['partner_department'] != 0) {
				$this->load->model('www/department_model', 'department');
				$data[$k]['partner_department_arr'] = $this->department->id_aGetInfo($v['partner_department']);
			}

			//当属性类型为下拉单选时
			if ($v['partner_level'] != "" and $v['partner_level'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['partner_level_arr'] = $this->enum->getlist(102, $data[$k]['partner_level']);
			}
		}
		return $data;
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('partner');
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $user['user_id'];
			unset($data['user']);
		}

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

					switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
//					$json = $this->arrJson($v);
//					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}

		$res = $this->db->insert('tc_partner', $save_data);
		if ($res) {
			$res_id = $this->db->insert_id();
			return array('res' => 'succ', 'msg' => '', 'info' => array('partner_id' => $res_id));
		} else
			return array('res' => 'fail', 'msg' => 'error partner save');
	}

	//2014-07-17 api start
	public function _addCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无合作伙伴/经销商数据');
		return $this->api->_return(__class__ . ' _addCheck succ', $data, 'succ');
	}

	public function _updateCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无合作伙伴/经销商数据');
		return $this->api->_return(__class__ . ' _updateCheck succ', $data, 'succ');
	}
	//2014-07-17 api end

}

?>
