<?php

class Product_basic_model extends CI_Model {

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'product_basic_code',
			'product_basic_name',
			'product_basic_category',
			'product_basic_sub_category',
			'product_basic_cost',
			'product_basic_price',
			'product_basic_sales_type',
			'product_basic_charging_type',
			'product_basic_calc_unit',
			'product_basic_service_unit',
			'product_basic_can_renewal',
			'product_basic_need_host',
			'product_basic_need_open',
			'product_basic_has_expired',
			'product_basic_utime',
			'product_basic_domain_cost',
			'product_basic_sms_cost',
			'product_basic_confirm_method',
			'product_basic_idc_cost',
			'product_basic_atime',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'product_basic_code',
			'product_basic_name',
			'product_basic_category',
			'product_basic_sub_category',
			'product_basic_cost',
			'product_basic_price',
			'product_basic_sales_type',
			'product_basic_charging_type',
			'product_basic_calc_unit',
			'product_basic_service_unit',
			'product_basic_can_renewal',
			'product_basic_need_host',
			'product_basic_need_open',
			'product_basic_has_expired',
			'product_basic_utime',
			'product_basic_domain_cost',
			'product_basic_sms_cost',
			'product_basic_confirm_method',
			'product_basic_idc_cost',
			'product_basic_atime',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
			'product_basic_code',
			'product_basic_name',
			'product_basic_category',
			'product_basic_sub_category',
			'product_basic_cost',
			'product_basic_price',
			'product_basic_sales_type',
			'product_basic_charging_type',
			'product_basic_calc_unit',
			'product_basic_service_unit',
			'product_basic_can_renewal',
			'product_basic_need_host',
			'product_basic_need_open',
			'product_basic_has_expired',
			'product_basic_utime',
			'product_basic_domain_cost',
			'product_basic_sms_cost',
			'product_basic_confirm_method',
			'product_basic_idc_cost',
			'product_basic_atime',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'product_basic_code' => '产品编号',
			'product_basic_name' => '产品名称',
			'product_basic_category' => '一级分类',
			'product_basic_sub_category' => '二级分类',
			'product_basic_cost' => '成本价',
			'product_basic_price' => '市场价',
			'product_basic_sales_type' => '销售模式',
			'product_basic_charging_type' => '收费方式',
			'product_basic_calc_unit' => '计量单位',
			'product_basic_service_unit' => '服务周期单位',
			'product_basic_can_renewal' => '是否可续费',
			'product_basic_has_expired' => '是否有有效期',
			'product_basic_need_open' => '是否需要开通',
			'product_basic_need_host' => '是否需要主机',
			'product_basic_utime' => '变更信息时间',
			'product_basic_domain_cost' => '域名／备案成本',
			'product_basic_sms_cost' => '短信成本',
			'product_basic_idc_cost' => 'IDC成本',
			'product_basic_confirm_method' => '确认类型',
			'product_basic_atime' => '创建时间',
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'product_basic_id' => $id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data, $id) {
		$this->db->where('product_basic_id', $id)->update('tc_product_basic', $data);
	}

	//单个删除
	public function del($id) {
		$this->db->where('product_basic_id', $id)->delete('tc_product_basic');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_product_basic', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_product_basic');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_product_basic');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容

		$this->load->model('admin/enum_model', 'enum');
		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环
			//当属性类型为下拉单选时
			if ($v['product_basic_category'] != "" and $v['product_basic_category'] != 0) {
				$data[$k]['product_basic_category_arr'] = $this->enum->getlist(370, $data[$k]['product_basic_category']);
			}
			if ($v['product_basic_sub_category'] != "" and $v['product_basic_sub_category'] != 0) {
				$data[$k]['product_basic_sub_category_arr'] = $this->enum->getlist(371, $data[$k]['product_basic_sub_category']);
			}
			if ($v['product_basic_sales_type'] != "" and $v['product_basic_sales_type'] != 0) {
				$data[$k]['product_basic_sales_type_arr'] = $this->enum->getlist(374, $data[$k]['product_basic_sales_type']);
			}
			if ($v['product_basic_charging_type'] != "" and $v['product_basic_charging_type'] != 0) {
				$data[$k]['product_basic_charging_type_arr'] = $this->enum->getlist(375, $data[$k]['product_basic_charging_type']);
			}
			if ($v['product_basic_calc_unit'] != "" and $v['product_basic_calc_unit'] != 0) {
				$data[$k]['product_basic_calc_unit_arr'] = $this->enum->getlist(376, $data[$k]['product_basic_calc_unit']);
			}
			if ($v['product_basic_service_unit'] != "" and $v['product_basic_service_unit'] != 0) {
				$data[$k]['product_basic_service_unit_arr'] = $this->enum->getlist(377, $data[$k]['product_basic_service_unit']);
			}
			if ($v['product_basic_can_renewal'] != "" and $v['product_basic_can_renewal'] != 0) {
				$data[$k]['product_basic_can_renewal_arr'] = $this->enum->getlist(378, $data[$k]['product_basic_can_renewal']);
			}
			if ($v['product_basic_has_expired'] != "" and $v['product_basic_has_expired'] != 0) {
				$data[$k]['product_basic_has_expired_arr'] = $this->enum->getlist(379, $data[$k]['product_basic_has_expired']);
			}
			if ($v['product_basic_need_open'] != "" and $v['product_basic_need_open'] != 0) {
				$data[$k]['product_basic_need_open_arr'] = $this->enum->getlist(380, $data[$k]['product_basic_need_open']);
			}
			if ($v['product_basic_need_host'] != "" and $v['product_basic_need_host'] != 0) {
				$data[$k]['product_basic_need_host_arr'] = $this->enum->getlist(381, $data[$k]['product_basic_need_host']);
			}
		}
		return $data;
	}

	public function sync($starttime, $endtime) {
		$this->load->library('snoopy');
		$this->db->query('SET NAMES UTF8');
		$basic_arr = $this->getBasic($starttime, $endtime); // 抓取基础商品

		error_log(print_r(array('starttime' => $starttime, 'endtime' => $endtime, 'basic_arr' => $basic_arr), true) . "\n", 3, 'basic' . date("Ymd_h") . "log.log");
		if ($basic_arr) {
			foreach ($basic_arr as $bk => $bv) {
				$tech_arr = $this->getTech($bv['product_code']); // 抓取技术产品
				if ($tech_arr) {
					// 添加/保存技术产品 start
					$tech_data = array();
					$tech_data['product_tech_code'] = $tech_arr['product_code'];
					$tech_data['product_tech_name'] = $tech_arr['product_name'];

					$res_tech = $this->db->get_where('tc_product_tech', array(
								'product_tech_code' => $tech_data['product_tech_code'],
								'product_tech_name' => $tech_data['product_tech_name']
							))->row_array();

					// 获取技术商品id
					if ($res_tech) {
						$res_tech_id = $res_tech['product_tech_id'];
					} else {
						$res_tech_id = $this->db->insert('tc_product_tech', $tech_data);
					}

					if (!$res_tech_id) {
						$res['line'] = __LINE__;
						return $res;
					}
				} else {
					$res['line'] = __LINE__;
					return $res;
				} // 添加/保存技术产品 end
				// 添加/保存基础商品 start
				$basic_data = array();
				// 枚举类型存储
				$enum_category = array('1501' => '1001', '1502' => '1002', '1503' => '1003');
				$enum_sub_category = array('1601' => '1001', '1602' => '1002', '1603' => '1003', '1604' => '1004', '1605' => '1005', '1606' => '1006', '1607' => '1007');
				$enum_sales_type = array('1101' => '1001', '1102' => '1002', '1103' => '1003');
				$enum_arr = array('0' => '1002', '1' => '1001');

				$basic_data['product_basic_code'] = $bv['suite_code'];
				$basic_data['product_basic_name'] = $bv['suite_name'];
				$basic_data['product_basic_category'] = $enum_category[$bv['suite_category']];
				$basic_data['product_basic_sub_category'] = $enum_sub_category[$bv['suite_sub_category']];
				$basic_data['product_basic_sales_type'] = $enum_sales_type[$bv['suite_sales_type']];
				$basic_data['product_basic_confirm_method'] = $bv['suite_confirm_method'];
				$basic_data['product_basic_domain_cost'] = $bv['suite_domain_cost'];
				$basic_data['product_basic_sms_cost'] = $bv['suite_sms_cost'];
				$basic_data['product_basic_idc_cost'] = $bv['suite_idc_cost'];
				$basic_data['product_basic_atime'] = date('Y-m-d H:i:s', time());
				$basic_data['product_basic_utime'] = date('Y-m-d H:i:s', $bv['suite_utime']);
				// 暂时保留
//				$basic_data['product_basic_price'] = 0;
//				$basic_data['product_basic_service_cycle'] = '1001';
//				$basic_data['product_basic_review_status'] = '1001';
//				$basic_data['product_basic_domain'] = $bv['suite_domain_cost'];
//				$basic_data['product_basic_sms'] = $bv['suite_sms_cost'];
//				$basic_data['product_basic_idc'] = $bv['suite_idc_cost'];

				$res_basic = $this->db->get_where('tc_product_basic', array(
							'product_basic_code' => $basic_data['product_basic_code']
						))->row_array();

				if ($res_basic) {
					$this->db->update('tc_product_basic', $basic_data, array('product_basic_id' => $res_basic['product_basic_id']));
					$res_basic_id = $res_basic['product_basic_id'];
				} else {
					$res_basic_id = $this->db->insert('tc_product_basic', $basic_data);
				}

				if (!$res_basic_id) {
					$res['line'] = __LINE__;
				} // 添加/保存基础商品 end
				// 开通参数 start
				if (isset($bv['open_params'])) {
					foreach ($bv['open_params'] as $k => $v) {
						$params_data = array();
						$params_data['product_oparam_product_basic_id'] = $res_basic_id; // 基础商品id
						$params_data['product_oparam_name'] = $v['params_name'];
						$params_data['product_oparam_type'] = '1001'; //1001开通 1002返回
						$params_data['product_oparam_content'] = json_encode($bv['open_params'][$k]);
						$params_data['product_oparam_atime'] = date('Y-m-d H:i:s', $v['params_atime']);
						$params_data['product_oparam_utime'] = date('Y-m-d H:i:s', $v['params_utime']);

						$res_oparams = $this->db->get_where('tc_product_oparam', array(
									'product_oparam_product_basic_id' => $params_data['product_oparam_product_basic_id'],
									'product_oparam_content' => $params_data['product_oparam_content']
								))->row_array();
						if ($res_oparams) {
							$res_oparam_id = $res_oparams['product_oparam_id'];
						} else {
							$res_oparam_id = $this->db->insert('tc_product_oparam', $params_data);
						}
						if (!$res_oparam_id) {
							$res['line'] = __LINE__;
							return $res;
						}
					}
				} // 开通参数 end
				// 返回参数 start
				if (isset($bv['return_params'])) {
					foreach ($bv['return_params'] as $k => $v) {
						$params_data = array();
						$params_data['product_oparam_product_basic_id'] = $res_basic_id;
						$params_data['product_oparam_name'] = $v['params_name'];
						$params_data['product_oparam_type'] = '1002'; //1001开通 1002返回
						$params_data['product_oparam_content'] = json_encode($bv['return_params'][$k]);
						$params_data['product_oparam_atime'] = $v['params_atime'];
						$params_data['product_oparam_utime'] = $v['params_utime'];

						$res_rparams = $this->db->get_where('tc_product_oparam', array(
									'product_oparam_product_basic_id' => $params_data['product_oparam_product_basic_id'],
									'product_oparam_content' => $params_data['product_oparam_content']
								))->row_array();
						if ($res_rparams) {
							$res_oparam_id = $res_rparams['product_oparam_id'];
						} else {
							$res_oparam_id = $this->db->insert('tc_product_oparam', $params_data);
						}
						if (!$res_oparam_id) {
							$res['line'] = __LINE__;
							return $res;
						}
					}
				} // 返回参数 end
				// 基础产品和技术产品关系 start
				$techrel_data = array();
				$techrel_data['ptrelation_product_basic_id'] = $res_basic_id;
				$techrel_data['ptrelation_product_tech_id'] = $res_tech_id;

				$res_techrel = $this->db->get_where('tc_ptrelation', array(
							'ptrelation_product_basic_id' => $res_basic_id,
							'ptrelation_product_tech_id' => $res_tech_id
						))->row_array();

				if ($res_techrel) {
					$res_techrel_id = $res_techrel['ptrelation_id'];
				} else {
					$res_techrel_id = $this->db->insert('tc_ptrelation', $techrel_data);
				}
				if (!$res_techrel_id) {
					$res['line'] = __LINE__;
					return $res;
				}// 基础产品和技术产品关系 end
			}
			return true;
		}
	}

	// 抓取基础产品信息
	private function getBasic($starttime, $endtime) {
		$api_url = 'http://pro.shopex.cn/index.php/openapi/api/post';
		$params['method'] = 'product.getSuite';
		$params['app_type'] = 'post';
		$params['format'] = 'json';
		$params['version'] = '1.0';
		$params['call_times'] = 'time';
		$params['return'] = 'has';
		$get_prosuite_params = array(
			'starttime' => $starttime,
			'endtime' => $endtime,
		);
		$params = array_merge($params, $get_prosuite_params);
		$params['sign'] = $this->make_shopex_ac($params, 'procpbi');
		$this->snoopy->submit($api_url, $params);
		$res = $this->snoopy->results;
		$json_arr = json_decode($res, true);
		$basic_arr = $json_arr['info']['suite'];
		return $basic_arr;
	}

	// 抓取技术产品信息
	private function getTech($tech_code) {
		$api_url = 'http://pro.shopex.cn/index.php/openapi/api/post';
		$params['method'] = 'product.getProduct';
		$params['app_type'] = 'post';
		$params['format'] = 'json';
		$params['version'] = '1.0';
		$params['call_times'] = 'time';
		$params['return'] = 'has';
		$get_product_params = array(
			'product_code' => $tech_code,
		);
		$params = array_merge($params, $get_product_params);
		$params['sign'] = $this->make_shopex_ac($params, 'procpbi');
		$this->snoopy->submit($api_url, $params);
		$res = $this->snoopy->results;
		$json_arr = json_decode($res, true);
		$tech_arr = $json_arr['info']['product'];
		return $tech_arr;
	}

	// 平台验证
	private function make_shopex_ac($post_params, $token) {
		ksort($post_params);
		$str = '';
		foreach ($post_params as $key => $value) {
			if ($key != 'sign') {
				$str.=$value;
			}
		}
		return md5($str . $token);
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('product_basic');
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $user['user_id'];
			unset($data['user']);
		}

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
					$json = $this->arrJson($v);
					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}

		if ($bId = $this->db->select('product_basic_id')->get_where('tc_product_basic', array('product_basic_code' => $save_data['product_basic_code']))->row_array()) {
//			return array('res' => 'succ', 'msg' => '基础产品已存在', 'info' => array('product_basic_id' => $bId['product_basic_id'], 'old' => true));
			// p($save_data); exit;
			$this->db->update('tc_product_basic', $save_data, array('product_basic_id' => $bId['product_basic_id']));
			return array('res' => 'succ', 'msg' => 'update success', 'info' => array('product_basic_id' => $bId['product_basic_id']));
		}

		$res = $this->db->insert('tc_product_basic', $save_data);
		if ($res) {
			$res_id = $this->db->insert_id();
			return array('res' => 'succ', 'msg' => 'add success', 'info' => array('product_basic_id' => $res_id));
		} else
			return array('res' => 'fail', 'msg' => 'error product_basic save');
	}

	private function arrJson($v) {
		$json = '';
//		foreach ($v as $kk => $vv) {
//			if (strpos($kk, '.')) {
//				list($fields, $attr) = explode('.', $kk);
//				if ($fields == 'bank_type') {
//					$res_enum = $this->db->select('enum_key')->get_where('dd_enum', array('enum_name' => $vv, 'attr_id' => '560'))->row_array();
//					if (!$res_enum)
//						return array('res' => 'fail', 'msg' => 'error [' . $kk . ' : ' . $vv . ']');
//					$json[$fields] = $res_enum['enum_key'];
//				}
//			} else {
//				$json[$kk] = $vv;
//			}
//		}
		return $json;
	}

}

?>
