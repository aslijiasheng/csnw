<?php

class Goods_price_model extends CI_Model {

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'goods_price_effective_days',
			'goods_price_cycle_unit',
			'goods_price_cycle_price',
			'goods_price_start_price',
			'goods_price_cycle_days',
			'goods_price_code',
			'goods_price_visable',
			'goods_price_goods_id',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'goods_price_effective_days',
			'goods_price_cycle_unit',
			'goods_price_cycle_price',
			'goods_price_start_price',
			'goods_price_cycle_days',
			'goods_price_code',
			'goods_price_visable',
			'goods_price_goods_id',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
			'goods_price_effective_days',
			'goods_price_cycle_unit',
			'goods_price_cycle_price',
			'goods_price_start_price',
			'goods_price_cycle_days',
			'goods_price_code',
			'goods_price_visable',
			'goods_price_goods_id',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'goods_price_goods_id' => '商品ID',
			'goods_price_code' => '价格编号',
			'goods_price_visable' => '是否启用',
			'goods_price_start_price' => '一次性计费的价格',
			'goods_price_cycle_days' => '周期性计费的周期',
			'goods_price_cycle_unit' => '周期单位',
			'goods_price_cycle_price' => '周期性计费的价格',
			'goods_price_effective_days' => '有效期时间（天）',
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'goods_price_id' => $id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data, $id) {
		$this->db->where('goods_price_id', $id)->update('tc_goods_price', $data);
	}

	//单个删除
	public function del($id) {
		$this->db->where('goods_price_id', $id)->delete('tc_goods_price');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_goods_price', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_goods_price');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_goods_price');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容

		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环


			if ($v['goods_price_goods_id'] != "" and $v['goods_price_goods_id'] != 0) {
				$this->load->model('www/goods_model', 'goods');
				$data[$k]['goods_price_goods_id_arr'] = $this->goods->id_aGetInfo($v['goods_price_goods_id']);
			}
















			//当属性类型为单选时
			if ($v['goods_price_visable'] != "" and $v['goods_price_visable'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['goods_price_visable_arr'] = $this->enum->getlist(414, $data[$k]['goods_price_visable']);
			}























			//当属性类型为下拉单选时
			if ($v['goods_price_cycle_unit'] != "" and $v['goods_price_cycle_unit'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['goods_price_cycle_unit_arr'] = $this->enum->getlist(417, $data[$k]['goods_price_cycle_unit']);
			}
		}
		return $data;
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('goods_price');
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $user['user_id'];
			unset($data['user']);
		}

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
					$json = $this->arrJson($v);
					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}

		if ($gpId = $this->db->select('goods_price_id')->get_where('tc_goods_price', array('goods_price_code' => $save_data['goods_price_code']))->row_array()) {
			// return array('res' => 'succ', 'msg' => '价格编号已存在', 'info' => array('goods_price_id' => $gpId['goods_price_id']));
			$this->db->update('tc_goods_price', $save_data, array('goods_price_id' => $gpId['goods_price_id']));
			return array('res' => 'succ', 'msg' => 'update success', 'info' => array('goods_price_id' => $gpId['goods_price_id']));
		}

		$res = $this->db->insert('tc_goods_price', $save_data);
		if ($res) {
			$res_id = $this->db->insert_id();
			return array('res' => 'succ', 'msg' => 'add success', 'info' => array('goods_price_id' => $res_id));
		} else
			return array('res' => 'fail', 'msg' => 'error goods_price save');
	}

	private function arrJson($v) {
		$json = '';
//		foreach ($v as $kk => $vv) {
//			if (strpos($kk, '.')) {
//				list($fields, $attr) = explode('.', $kk);
//				if ($fields == 'bank_type') {
//					$res_enum = $this->db->select('enum_key')->get_where('dd_enum', array('enum_name' => $vv, 'attr_id' => '560'))->row_array();
//					if (!$res_enum)
//						return array('res' => 'fail', 'msg' => 'error [' . $kk . ' : ' . $vv . ']');
//					$json[$fields] = $res_enum['enum_key'];
//				}
//			} else {
//				$json[$kk] = $vv;
//			}
//		}
		return $json;
	}

}

?>
