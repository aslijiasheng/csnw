<?php

if ( !defined( 'BASEPATH' ) )
    exit( 'No direct script access allowed' );

class Cancel_model extends CI_Model {

    //判断订单是否可以作废
    public function isCancel( $order_id ) {
        $this->db->from( 'tc_salespay,tc_rebate,tc_refund' );
        $this->db->or_where( 'tc_salespay.salespay_order_id =', $order_id );
        $this->db->or_where( 'tc_rebate.rebate_order_id =', $order_id );
        $this->db->or_where( 'tc_refund.refund_order_id =', $order_id );
        $res = $this->db->get()->row_array();
        if ( $res )
            return $res;
        else
            return false;
    }

    //订单作废备注
    public function orderCancel( $order_id = '', $remark = '', $state = '' ) {
        // $relate = array('1001'=>'未作废', '1002'=>'已作废', '1003'=>'申请作废', '1004'=>'审核作废', '1005'=>'申请变更', '1006'=>'审核变更');
        date_default_timezone_set( 'PRC' );

        if ( !empty( $order_id ) ) {
            $this->load->library( 'session' );
            $this->load->model( 'www/order_record_model', 'record' );

            $session = $this->session->all_userdata();
            $log_data = array(
                'order_record_operation' => $state,
                'cancel_order_id' => $order_id,
                'cancel_remark' => $remark,
                'cancel_user_id' => $session['user_id'],
                'cancel_create_time' => date( 'Y-m-d H:i:s', time() )
            );
            $this->record->add( $log_data );
			
            $res = $this->db->update( 'tc_order', array( 'order_is_cancel' => $state ), array( 'order_id' => $order_id ) );
            if ( $res )
                return $res;
            else
                return false;
        } else
            return false;
    }
	
	//订单作废红冲
	public function orderRedCancel($order_id, $state) {
		//新增需求 加入订单作废并新增出账红冲 start
		if ($state == 1002) {
		
			$where = array(
				'salespay_order_id'  => $order_id,//订单ID
				'salespay_status'    => '1002',//已到账状态
				'salespay_money_type'=> '1001',//入款项
			);
			
			$res_data = $this->db->get_where('tc_salespay', $where)->result_array();
			
			$datenow = date('Y-m-d H:i:s');
			
			foreach ($res_data as $k=>$v) {
				
				$res_clone	 						   = $v;							//克隆新变量
				array_shift($res_clone);												//移除掉查询出来的ID
				
				$res_clone['salespay_order_id']        = $order_id;	//订单ID
				$res_clone['salespay_money_type_name'] = '1005';						//负红冲状态值
				$res_clone['salespay_money_type']      = '1002';						//出账记录
				$res_clone['salespay_sp_date']         = $datenow;						//确认到帐日期
				$res_clone['salespay_sp_real_date']    = $datenow;						//实际到帐日期(用于U8)
				$this->doinsert( 'tc_salespay', $res_clone);							//增加当前订单负出账红冲
				
			}
		}
		//新增需求 加入订单作废并新增出账红冲 end
	}

    //订单变更
    public function updateOrderModify( $post ) {
		//p($post);
        $res = $this->db->select('order_cancel_order_id')->get_where('tc_order', array('order_id'=>$post['cancel']['order_id']))->row_array();
		
        if ( $res ) {
            $this->load->model( 'www/order_model', 'order' );
			
			$datenow = date('Y-m-d H:i:s');
			
			$where = array(
				'salespay_order_id'  => $post['cancel']['order_id'],//订单ID
				'salespay_status'    => '1002',//已到账状态
			);
			
			$res_s = $this->db->get_where('tc_salespay', $where)->result_array();//取出订单有关的款项记录
			/*
            $res_s = $this->db->get_where( 'tc_salespay', array( 'salespay_order_id' => $post['cancel']['order_id'] ) )->row_array();//入款项
			//1001 入账记录 为正
			$where = array("salespay_order_id" => $post['cancel']['order_id'],
						   "salespay_money_type" => "1001",
						   "salespay_status" => "1002");
			$res_r_sum = $this->db->select_sum('salespay_pay_amount')->get_where( 'tc_salespay', $where )->row_array();
			
			//1002 出账记录 为负
			$where = array("salespay_order_id" => $post['cancel']['order_id'],
						   "salespay_money_type" => "1002",
						   "salespay_status" => "1002");
			$res_c_sum = $this->db->select_sum('salespay_pay_amount')->get_where( 'tc_salespay', $where )->row_array();
			
			//计算入账记录与出账记录差值
			$salespay_pay_amount = (float)$res_r_sum['salespay_pay_amount']-(float)$res_c_sum['salespay_pay_amount'];
			
			$res_s['salespay_order_id'] 		=   $res['order_cancel_order_id'];  //新订单ID
			$res_s['salespay_money_type_name']  =   '1004';							//正 红冲状态值
			$res_s['salespay_pay_date']         =   $datenow;						//到账/出账日期
			$res_s['salespay_sp_date']          =   $datenow;						//确认到帐日期
			$res_s['salespay_sp_real_date']     =   $datenow;						//实际到帐日期（用于U8）
			$res_s['salespay_pay_amount']       =   $salespay_pay_amount;			//差值
			$res_s['salespay_money_type']       =   '1001';							//入账记录
			//再增加一笔出账记录  1002 负 红冲状态值 1005
			
			array_shift($res_s);//移除掉查询出来的ID
			$this->db->trans_begin();
            if ( $res_s )
			{
				
				$res_s_r = $this->doinsert( 'tc_salespay', $res_s);
				//PC::debug($res_s);
				if($res_s_r['res'] != "succ")
				{
					$this->db->trans_rollback();
					$this->db->close();
					return false;
					die();
				}
				
				$res_s['salespay_order_id'] = $post['cancel']['order_id'];//还原回原单ID
				$res_s['salespay_money_type_name'] = '1005';//负 红冲状态值
				$res_s['salespay_money_type'] = '1002';//出账记录
				//PC::debug($res_s);
				$res_c_r = $this->doinsert( 'tc_salespay', $res_s);
				if($res_c_r['res'] != "succ")
				{
					$this->db->trans_rollback();
					$this->db->close();
					return false;
					die();
				}

			}
			*/
			$this->db->trans_begin();
			//1004正红冲 1005负红冲

			foreach ($res_s as $k=>$v) {
				//检测状态值为1001证明是入账记录,需要为订单增加一笔当前订单负红冲并且为关联订单增加一笔正红冲
				//检测状态值为1002证明是出账记录,需要为订单增加一笔当前订单正红冲并且为关联订单增加一笔负红冲
				if ($v['salespay_money_type'] == '1001') {//入账
					$res_clone	 						   = $v;							//克隆新变量
					array_shift($res_clone);												//移除掉查询出来的ID
					
					$res_clone['salespay_order_id']        = $post['cancel']['order_id'];	//订单ID
					$res_clone['salespay_money_type_name'] = '1005';						//负红冲状态值
					$res_clone['salespay_money_type']      = '1002';						//出账记录
					$res_clone['salespay_sp_date']         = $datenow;						//确认到帐日期
					$res_clone['salespay_sp_real_date']    = $datenow;						//实际到帐日期(用于U8)
					$this->doinsert( 'tc_salespay', $res_clone);							//增加当前订单负出账红冲
					
					$res_clone['salespay_order_id'] 	   = $res['order_cancel_order_id']; //新订单ID
					$res_clone['salespay_money_type_name'] = '1004';						//正红冲状态值
					$res_clone['salespay_money_type']      = '1001';						//入账记录
					$this->doinsert( 'tc_salespay', $res_clone);							//增加新订单正出账红冲
				} elseif ($v['salespay_money_type'] == '1002') {//出账
					$res_clone	 						   = $v;							//克隆新变量
					array_shift($res_clone);												//移除掉查询出来的ID
					
					$res_clone['salespay_order_id']        = $post['cancel']['order_id'];	//订单ID
					$res_clone['salespay_money_type_name'] = '1004';						//正红冲状态值
					$res_clone['salespay_money_type']      = '1001';						//入账记录
					$res_clone['salespay_sp_date']         = $datenow;						//确认到帐日期
					$res_clone['salespay_sp_real_date']    = $datenow;						//实际到帐日期(用于U8)
					$this->doinsert( 'tc_salespay', $res_clone);							//增加当前订单负出账红冲
					
					$res_clone['salespay_order_id'] 	   = $res['order_cancel_order_id']; //新订单ID
					$res_clone['salespay_money_type_name'] = '1005';						//负红冲状态值
					$res_clone['salespay_money_type']      = '1002';						//出账记录
					$this->doinsert( 'tc_salespay', $res_clone);							//增加新订单正出账红冲
				}
			}
			
            $res_f = $this->db->get_where( 'tc_refund', array( 'refund_order_id' => $post['cancel']['order_id'] ) )->row_array();
            if ( $res_f )
                $res_f_r = $this->db->update( 'tc_refund', array( 'refund_order_id' => $res['order_cancel_order_id'] ), array( 'refund_order_id' => $post['cancel']['order_id'] ) );
				
			$res_i = $this->db->get_where( 'tc_invoice', array( 'invoice_order_id' => $post['cancel']['order_id'] ) )->row_array();
			
			//修改红冲开票转移BUG 2014/08/29
            if ( $res_i ) {
				$res_i_r = $this->db->update( 'tc_invoice', array( 'invoice_order_id' => $res['order_cancel_order_id'] ), array( 'invoice_order_id' => $post['cancel']['order_id'] ) );
			}
            //修改红冲开票转移BUG 2014/08/29
			
            if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$this->db->close();
				return false;
			} else {
				$this->db->trans_commit();
				$this->db->close();
				return true;
			}
			
        } else
            return false;
    }
	
	private function doinsert( $table, $data )
	{
		$res=$this->db->insert($table,$data);
		//echo $this->db->last_query();
		return $res ? array('res' => 'succ', "$table" . '_id' => $this->db->insert_id()) : array('res' => 'fail');
	}

    public function saveOrderModify($post){
        $res_o = $this->db->select('order_id')->get_where('tc_order', array('order_number'=>$post['new_cancel']['cancel_order_number']))->row_array();
        if($res_o){
            $data = array('order_cancel_order_id'=>$res_o['order_id']);
            $this->db->where('order_id', $post['cancel']['order_id']);
            return $this->db->update('tc_order', $data);
        }else return false;
    }

    public function clearOrderModify($post){
        $data = array('order_cancel_order_id'=>'');
        $this->db->where('order_id', $post['cancel']['order_id']);
        return $this->db->update('tc_order', $data);
    }

}
