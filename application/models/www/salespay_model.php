<?php

class Salespay_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->model("www/order_model", "order");
	}

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'salespay_pay_date',
			'salespay_pay_method',
			'salespay_pay_info',
			'salespay_pay_amount',
			'salespay_create_user',
			'salespay_create_time',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'salespay_create_time',
			'salespay_order_id',
			'salespay_create_user',
			'salespay_pay_amount',
			'salespay_pay_date',
			'salespay_pay_method',
			'salespay_pay_info',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
			'salespay_create_time',
			'salespay_order_id',
			'salespay_create_user',
			'salespay_pay_amount',
			'salespay_pay_date',
			'salespay_pay_method',
			'salespay_pay_info',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'salespay_create_time' => '创建时间',
			'salespay_order_id' => '订单ID',
			'salespay_create_user' => '创建人',
			'salespay_pay_amount' => '入账金额',
			'salespay_pay_date' => '入账日期',
			'salespay_pay_method' => '支付方式',
			'salespay_pay_info' => '支付信息',
			'salespay_status' => '到账状态',
			'salespay_pay_note' => '入账备注',
			'salespay_finance' => '财务人员',
			'salespay_examine_note' => '审批备注',
			'salespay_remit_bank' => '汇款账户',
			'salespay_alipay_bank' => '支付宝账号',
			'salespay_book_id' => '选择对应日记账',
			'salespay_sp_date' => '确认到帐日期',
			'salespay_subsidiary' => '所属子公司',
			'salespay_sp_real_date' => '实际到帐日期（用于U8）',
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'salespay_id' => $id
		);
		$data = $this->GetInfo($where);
		if ($data != "") {
			$data = $data[0];
		}
		return $data;
	}

	//单个修改
	public function update($data, $id) {
		
		$this->db->where('salespay_id', $id)->update('tc_salespay', $data);
		
		$res = $this->id_aGetInfo($id);
		$res = $this->isParseData( $data );
		
		if( $res['status'] == "failed" ){
			return $res['description'];
		}
	}

	//单个删除
	public function del($id) {
		$this->db->where('salespay_id', $id)->delete('tc_salespay');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_salespay', $data);
		$insert_id = $this->db->insert_id();
		
		$res = $this->isParseData( $data );
		if( $res['status'] == "failed" ){
			return $res['description'];
		}
		return $insert_id;
	}
	/**
	* 说明：传递解析数据并进行处理
	* @params array $data 解析数据
	* @return string $res 状态值
	*/
	private function isParseData( $data )
	{
		//判断 到账状态是否为1002
		if( $data["salespay_status"]=="1002" ){
			$res = $this->order->parseData($data["salespay_order_id"]);
			//判断解析数据是否执行成功
			return $res;
		} else {
			return false;
		}
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_salespay');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}
	
	//只查当前表数据非关联
	public function GetResultInfo($fields = "*", $where = "", $orderby="") {
		if ( !empty($orderby) ) {
			list($title, $desc) = explode(",", $orderby);
		}
		$this->db->select($fields);
		$this->db->from("tc_salespay");
		if ($where != "") {
			$this->db->where($where);
		}
		if ($orderby != "") {
			$this->db->order_by($title, $desc);
		}
		$data = $this->db->get()->result_array();
		return $data;
	}
/**
	* 例：查询汇总纪录
	* @params $data 要载入的数据条件
	* @return int   返回的汇总数据
	*/
	public function SelectSum($data) {
		extract($data,EXTR_OVERWRITE);
		$this->db->from('tc_salespay');
		$this->db->select_sum($fields, "sum");
		if ($where != "") {
			$this->db->where($where);
		}
		$data = $this->db->get()->result_array();
		if ($data[0]['sum'] == "") {
			$data[0]['sum'] = 0;
		}
		return $data[0]['sum'];
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_salespay');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容

		$data = $this->info($data);
		return $data;
	}

	//salespay需要专门写个获取相关订单的未确认到帐、已确认到帐
	public function SumGetInfo($field_name, $where = "") {
		$this->db->from('tc_salespay');
		$this->db->select_sum($field_name, "sum");
		if ($where != "") {
			$this->db->where($where);
		}
		//p($this->db->Produces);
		$data = $this->db->get()->result_array();
		if ($data[0]['sum'] == "") {
			$data[0]['sum'] = 0;
		}
		return $data[0]['sum'];
	}

	public function info($data) {

		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环
			if ($v['salespay_order_id'] != "" and $v['salespay_order_id'] != 0) {
				$this->load->model('www/order_model', 'order');
				$data[$k]['salespay_order_id_arr'] = $this->order->id_aGetInfo_simple($v['salespay_order_id']);
			}
			
			if ($v['salespay_create_user'] != "" and $v['salespay_create_user'] != 0) {
				$this->load->model('www/user_model', 'user');
				$data[$k]['salespay_create_user_arr'] = $this->user->id_aGetInfo($v['salespay_create_user']);
			}
			if ($v['salespay_finance'] != "" and $v['salespay_finance'] != 0) {
				$this->load->model('www/user_model', 'user');
				$data[$k]['salespay_finance_arr'] = $this->user->id_aGetInfo($v['salespay_finance']);
			}


			//支付方式
			if ($v['salespay_pay_method'] != "" and $v['salespay_pay_method'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['salespay_pay_method_arr'] = $this->enum->getlist(144, $data[$k]['salespay_pay_method']);
			}
			
			//所属子公司 2014/08/25
			if ($v['salespay_subsidiary'] != "" and $v['salespay_subsidiary'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['salespay_subsidiary_arr'] = $this->enum->getlist(629, $data[$k]['salespay_subsidiary']);
			}
			//所属子公司 2014/08/25
			
			//到账状态
			if ($v['salespay_status'] != "" and $v['salespay_status'] != 0) {
				$this->load->model('admin/enum_model', 'enum');
				$data[$k]['salespay_status_arr'] = $this->enum->getlist(146, $data[$k]['salespay_status']);
			}

			//商派银行账号
			if ($v['salespay_my_bank_account'] != "" and $v['salespay_my_bank_account'] != 0) {
				$this->load->model('admin/enum_model', 'enum');
				$data[$k]['salespay_my_bank_account_arr'] = $this->enum->getlist(363, $data[$k]['salespay_my_bank_account']);
			}

			//商派支付宝账号
			if ($v['salespay_my_alipay_account'] != "" and $v['salespay_my_alipay_account'] != 0) {
				$this->load->model('admin/enum_model', 'enum');
				$data[$k]['salespay_my_alipay_account_arr'] = $this->enum->getlist(364, $data[$k]['salespay_my_alipay_account']);
			}

			//款项类型
			if ($v['salespay_money_type'] != "" and $v['salespay_money_type'] != 0) {
				$this->load->model('admin/enum_model', 'enum');
				$data[$k]['salespay_money_type_arr'] = $this->enum->getlist(545, $data[$k]['salespay_money_type']);
			}

			//款项类型名称
			if ($v['salespay_money_type_name'] != "" and $v['salespay_money_type_name'] != 0) {
				$this->load->model('admin/enum_model', 'enum');
				$data[$k]['salespay_money_type_name_arr'] = $this->enum->getlist(564, $data[$k]['salespay_money_type_name']);
			}
			//客户账号类型
			if ($v['salespay_account_bank_type'] != "" and $v['salespay_account_bank_type'] != 0) {
				$this->load->model('admin/enum_model', 'enum');
				$data[$k]['salespay_account_bank_type_arr'] = $this->enum->getlist(560, $data[$k]['salespay_account_bank_type']);
			}
			if ($v['salespay_book_id'] != "" and $v['salespay_book_id'] != 0) {
				$this->load->model('www/books_model', 'books');
				$data[$k]['salespay_book_id_arr'] = $this->books->id_aGetInfo($v['salespay_book_id']);
			}

			//业务主管，业务负责人
			if ($v['salespay_reviewer'] != "" and $v['salespay_reviewer'] != 0) {
				$this->load->model('www/user_model', 'user');
				$data[$k]['salespay_reviewer_arr'] = $this->user->id_aGetInfo($v['salespay_reviewer']);
			}

			//选择返点的客户
			if ($v['salespay_rebate_account'] != "" and $v['salespay_rebate_account'] != 0) {
				$this->load->model('www/account_model', 'account');
				$data[$k]['salespay_rebate_account_arr'] = $this->account->id_aGetInfo($v['salespay_rebate_account']);
			}
		}
		return $data;
	}

	public function api_update($data, $where) {
		$update_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('salespay');
		$object = $objDetail['obj'];

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $object . '_' . $fields;
				if (!$this->db->field_exists($this_fields, 'tc_' . $object)) {
					return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
				}

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$update_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$update_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $object . '_' . $k;
				if (!$this->db->field_exists($this_fields, 'tc_' . $object)) {
					return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
				}

				//内容是数组，将内容组成 json
				if (is_array($v)) {
					$json = $this->arrJson($v);
					$save_data[$this_fields] = json_encode($json);
				} else
					$update_data[$this_fields] = $v;
			}
		}

		//此处添加待办信息
		if ($update_data['salespay_status'] == 1001) {
			$salespay_data = $this->GetInfo($where);
			$salespay_data = $salespay_data[0];
			$salespay_id = $salespay_data['salespay_id'];
			//p($salespay_data);exit;
			//echo $salespay_id;die;
			$this->load->model('www/order_model', 'order');
			$order_data = $this->order->id_aGetInfo($salespay_data['salespay_order_id']);
			$this->load->model('www/message_model', 'message');
			//cus lee 待办需要分不同类型给不同待办 start
			switch ($update_data['salespay_money_type_name']) {
				case '1001':
					//日志
					$system_log_operation = '新增入款项';
					$message_owner = $order_data['order_finance'];
					$message_module = 'salespay';
					$message_type = '1001'; //确认到账
					$message_url = 'www/salespay/ajax_confirm_salespay';
					break;
				case '1002':
					//日志
					$system_log_operation = '新增返点款项';
					$message_owner = $salespay_data['salespay_reviewer'];
					$message_module = 'rebate';
					$message_type = '1020'; //出账审核
					$message_url = 'www/salespay/ajax_check_salespay';
					break;
				case '1003':
					if (isset($_POST['goods_info'])) {
						$salespay_data['salespay_refund_product_json'] = json_encode($_POST['goods_info']);
					}
					//日志
					$system_log_operation = '新增退款项';
					$message_owner = $salespay_data['salespay_reviewer'];
					$message_module = 'refund';
					$message_type = '1020'; //出账审核
					$message_url = 'www/salespay/ajax_check_salespay';
					break;
			}
			//cus lee 待办需要分不同类型给不同待办 end
			$message_data = array(
				'message_owner' => $message_owner,
				'message_module' => $message_module,
				'message_module_id' => $salespay_id,
				'message_type' => $message_type,
				'message_department' => $order_data['order_finance_arr']['user_department'],
				'message_url' => $message_url
			);

			$this->message->add($message_data);
		}
		//待办添加结束

		$res = $this->db->update('tc_' . $object, $update_data, $where);
		if ($res)
			return array('res' => 'succ', 'msg' => 'update success');
		else
			return array('res' => 'fail', 'msg' => 'update failure');
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('salespay');

		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $user['user_id'];
			unset($data['user']);
		}

		foreach ($data as $k => $v) {
			$v = trim($v);
			if (empty($v)) {
				unset($data[$k]);
				continue;
			}

			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
					$json = $this->arrJson($v);
					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}
		//p($save_data);die;
		$res = $this->db->insert('tc_salespay', $save_data);

		//此处添加待办信息
		if ($save_data['salespay_status'] == 1001) {
			$salespay_id = $this->db->insert_id();
			//echo $salespay_id;die;
			$this->load->model('www/order_model', 'order');
			$order_data = $this->order->id_aGetInfo($save_data['salespay_order_id']);
			$this->load->model('www/message_model', 'message');
			//cus lee 待办需要分不同类型给不同待办 start
			switch ($save_data['salespay_money_type_name']) {
				case '1001':
					//日志
					$system_log_operation = '新增入款项';
					$message_owner = $order_data['order_finance'];
					$message_module = 'salespay';
					$message_type = '1001'; //确认到账
					$message_url = 'www/salespay/ajax_confirm_salespay';
					break;
				case '1002':
					//日志
					$system_log_operation = '新增返点款项';
					$message_owner = $salespay_data['salespay_reviewer'];
					$message_module = 'rebate';
					$message_type = '1020'; //出账审核
					$message_url = 'www/salespay/ajax_check_salespay';
					break;
				case '1003':
					if (isset($_POST['goods_info'])) {
						$salespay_data['salespay_refund_product_json'] = json_encode($_POST['goods_info']);
					}
					//日志
					$system_log_operation = '新增退款项';
					$message_owner = $salespay_data['salespay_reviewer'];
					$message_module = 'refund';
					$message_type = '1020'; //出账审核
					$message_url = 'www/salespay/ajax_check_salespay';
					break;
			}
			//cus lee 待办需要分不同类型给不同待办 end
			$message_data = array(
				'message_owner' => $message_owner,
				'message_module' => $message_module,
				'message_module_id' => $salespay_id,
				'message_type' => $message_type,
				'message_department' => $order_data['order_finance_arr']['user_department'],
				'message_url' => $message_url
			);
			$this->message->add($message_data);
		}
		//待办添加结束

		if ($res) {
			return array('res' => 'succ', 'msg' => '');
		} else
			return array('res' => 'fail', 'msg' => 'error salespay save');
	}

	private function arrJson($v) {
		$json = '';
		foreach ($v as $kk => $vv) {
			if (strpos($kk, '.')) {
				list($fields, $attr) = explode('.', $kk);
				if ($fields == 'remit_bank') {
					$res_enum = $this->db->select('enum_key')->get_where('dd_enum', array('enum_name' => $vv, 'attr_id' => '363'))->row_array();
					if (!$res_enum)
						return array('res' => 'fail', 'msg' => 'error [' . $kk . ' : ' . $vv . ']');
					$json[$fields] = $res_enum['enum_key'];
				} elseif ($fields == 'alipay_bank') {
					$res_enum = $this->db->select('enum_key')->get_where('dd_enum', array('enum_name' => $vv, 'attr_id' => '364'))->row_array();
					if (!$res_enum)
						return array('res' => 'fail', 'msg' => 'error [' . $kk . ' : ' . $vv . ']');
					$json[$fields] = $res_enum['enum_key'];
				}
			} else {
				$json[$kk] = $vv;
			}
		}
		return $json;
	}

	// 检查金额，已到账金额
	public function checkPayAmount($salespay_data, $order_data) {
		if ($salespay_data['salespay_money_type'] == '1002')
			return true;
		$sql = 'SELECT salespay_pay_amount FROM tc_salespay WHERE salespay_order_id=' . $salespay_data['salespay_order_id'] . ' AND salespay_money_type="1001" AND (salespay_status = 1001 or salespay_status = 1002)';
		$r_salespay = $this->db->query($sql)->result_array();
		foreach ($r_salespay as $v) {
			$salespay_data['salespay_pay_amount'] += $v['salespay_pay_amount'];
		}

		return $order_data['order_amount'] >= $salespay_data['salespay_pay_amount'] ? true : false;
	}

	//2014-07-17 api start
	public function _addCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无款项记录数据');
		return $this->api->_return(__class__ . ' _addCheck succ', $data, 'succ');
	}

	public function _updateCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无款项记录数据');
		if (!isset($data['salespay_number']))
			return $this->api->_msg('无款项编号');
		$res = $this->db->select('salespay_id')->get_where('tc_salespay', array('salespay_number' => $data['salespay_number']))->row_array();
		if (!$res)
			return $this->api->_msg('款项编号不存在');
		return $this->api->_return(__class__ . ' _updateCheck succ', $data, 'succ');
	}
	//2014-07-17 api end
	
	/**
	* @例:数据查询
	* @params $data 待查询的数据条件
	* @params return array 返回的数据集结果
	*/
	public function listGetInfoIn( $data ) {
		return $this->db->from("tc_salespay")->where_in( "salespay_order_id", $data )->get()->result_array();
	}
	
	/**
	* @例:数据删除
	* @params $salespay_order_id 待查询的数据条件
	* @retyrb 无
	*/
	public function _deleteInfo( $salespay_order_id ) {
		$this->db->where_in( "salespay_order_id", $salespay_order_id )->delete("tc_salespay");
	}

}

?>
