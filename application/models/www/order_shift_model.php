<?php

class Order_shift_model extends CI_Model {

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'order_shift_order_id' => '订单id',
			'order_shift_old_data' => '旧数据',
			'order_shift_new_data' => '新数据（更新数据）',
			'order_shift_update_time' => '修改时间',
		);
	}

	/**
	 * 列表查询
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'order_shift_id' => $id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data, $id) {
		$this->db->where('order_shift_id', $id)->update('tc_order_shift', $data);
	}

	//单个删除
	public function del($id) {
		$this->db->where('order_shift_id', $id)->delete('tc_order_shift');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_order_shift', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_order_shift');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_order_shift');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容
		foreach ($data as $k => $v) {
			//所有属性循环
			if ($v['order_shift_order_id'] != "" and $v['order_shift_order_id'] != 0) {
				$this->load->model('www/order_model', 'order');
				$data[$k]['order_shift_order_id_arr'] = $this->order->id_aGetInfo($v['order_shift_order_id']);
			}
		}
		return $data;
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('order_shift');

		//生成创建人id
		if (isset($this->session->userdata)) {
			$create_user_id = $this->session->userdata['user_id'];
			unset($data['user']);
		} else {
			if (isset($data['user'])) {
				$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
				if ($user)
					$create_user_id = $user['user_id'];
				unset($data['user']);
			}
		}

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
//					$json = $this->arrJson($v);
//					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}
		$res = $this->db->insert('tc_order_shift', $save_data);

		if ($res) {
			return array('res' => 'succ', 'msg' => '');
		} else
			return array('res' => 'fail', 'msg' => 'error order_shift save');
	}

}

?>
