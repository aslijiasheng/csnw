
class Account_model extends CI_Model{
	public function listData(){
		return $this->db->get('tc_account')->result_array();
	}
	
	//这里代表那些字段在列表上显示出来
	public function listAttr(){
		return array(
		
			'account_name',
		
			'account_path',
		
			'account_leader',
		
			'account_create_time',
		
			'account_update_time',
		
		);
	}

	//属性对应的中文标签
	public function attributeLabels()
	{
		return array(
		
			'account_name' => '名称',
		
			'account_path' => '级别',
		
			'account_leader' => '负责人',
		
			'account_create_time' => '创建时间',
		
			'account_update_time' => '更新时间',
		
		);
	}
}
?>
