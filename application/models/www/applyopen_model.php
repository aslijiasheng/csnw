<?php
class Applyopen_model extends CI_Model{

	//属性对应的中文标签
	public function attributeLabels(){
		return array(
			'salespay_create_time' => '创建时间',
			'salespay_order_id' => '订单ID',
			'salespay_create_user' => '创建人',
			'salespay_pay_amount' => '入账金额',
			'salespay_pay_date' => '入账日期',

		);
	}

	public function info($data){
		foreach ($data as $k=>$v){
			//p($v);die;
			//所有属性循环
			if($v['salespay_order_id']!="" and $v['salespay_order_id']!=0){
				$this->load->model('www/order_model','order');
				$data[$k]['salespay_order_id_arr'] = $this->order->id_aGetInfo($v['salespay_order_id']);
			}
			if($v['salespay_create_user']!="" and $v['salespay_create_user']!=0){
				$this->load->model('www/user_model','user');
				$data[$k]['salespay_create_user_arr'] = $this->user->id_aGetInfo($v['salespay_create_user']);
			}
				 //当属性类型为下拉单选时
			if($v['salespay_pay_method']!="" and $v['salespay_pay_method']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['salespay_pay_method_arr']=$this->enum->getlist(144,$data[$k]['salespay_pay_method']);
			}
				 //当属性类型为单选时
			if($v['salespay_status']!="" and $v['salespay_status']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['salespay_status_arr']=$this->enum->getlist(146,$data[$k]['salespay_status']);
			}
			if($v['salespay_finance']!="" and $v['salespay_finance']!=0){
				$this->load->model('www/user_model','user');
				$data[$k]['salespay_finance_arr'] = $this->user->id_aGetInfo($v['salespay_finance']);
			}
				 //当属性类型为下拉单选时
			if($v['salespay_remit_bank']!="" and $v['salespay_remit_bank']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['salespay_remit_bank_arr']=$this->enum->getlist(363,$data[$k]['salespay_remit_bank']);
			}
				 //当属性类型为下拉单选时
			if($v['salespay_alipay_bank']!="" and $v['salespay_alipay_bank']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['salespay_alipay_bank_arr']=$this->enum->getlist(364,$data[$k]['salespay_alipay_bank']);
			}
			if($v['salespay_book_id']!="" and $v['salespay_book_id']!=0){
				$this->load->model('www/books_model','books');
				$data[$k]['salespay_book_id_arr'] = $this->books->id_aGetInfo($v['salespay_book_id']);
			}
		}
		return $data;
	}

	//单个新增
	public function add($data){
		$this->db->insert('tc_applyopen',$data);
		$insert_id=$this->db->insert_id();
		return $insert_id;
	}

	//单个修改
	public function update($order_d_id,$data){
		$where['applyopen_order_d_id']=$order_d_id;
		$this->db->where($where)->update('tc_applyopen',$data);
	}
	
	//通过ID查询是否有这个ID的数据
	public function id_isThere($order_d_id){
		$where['applyopen_order_d_id']=$order_d_id;
		$this->db->from('tc_applyopen');
		$this->db->where($where);
		$count = $this->db->count_all_results();
		if($count==0){
			return false;
		}else{
			return true;
		}
	}

	//用ID单个查询
	public function id_aGetInfo($id){
		$where=array(
			'applyopen_order_d_id'=>$id
		);
		$data = $this->GetInfo($where);
		if(isset($data[0])){
			$data = $data[0];
		}else{
			$data = false;
		}
		return $data;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where="",$limit="",$like=""){
		//首先查询出本身所有的内容
		$this->db->from('tc_applyopen');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($limit!=""){
			$this->db->limit($limit[0],$limit[1]);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$data=$this->db->get()->result_array();
		//然后循环查询出所有引用的内容
		foreach ($data as $k=>$v){
			//p($v);die;
			//所有属性循环

		}
		return $data;
	}


	//********************** api接口部分 start ****************************
	//save保存接口 如果已存在则保存 如果不存在则新增
	public function api_save($data){
		//转换语句
		$data = $this->data_field($data);
		//p($data);
		foreach($data['applyopen_order_d_id'] as $v){
			//先查询这个明细ID是否有参数
			$res_applyopen_list = $this->db->select('applyopen_return_info')->where('applyopen_order_d_id',$v)->get('tc_applyopen')->result_array();
			//p($res_applyopen_list);exit;
			if(isset($res_applyopen_list[0])){
				//查询者个订单的返回参数是否存在
				if($res_applyopen_list[0]["applyopen_return_info"]=="" or $res_applyopen_list[0]["applyopen_return_info"]==null){
					//这里修改
					return array('res' => 'fail', 'msg' => '已存在，是否修改？');
					$update_data = array();
					$update_data['applyopen_return_info'] = json_encode(array('actual_startdate'=>$data["applyopen_open_startdate"],"actual_enddate"=>$data["applyopen_open_enddate"]));
					unset($update_data['applyopen_open_startdate']);
					unset($update_data['applyopen_open_enddate']);
					$res =$this->db->where(array("applyopen_order_d_id"=>$v))->update('tc_applyopen',$update_data);
					if (!$res) {
						return array('res' => 'fail', 'msg' => $v.' error applyopen update');
					}
					//将明细的状态改成已开通，开始时间 结束时间反写上
					$res = $this->db->where(array("order_d_id"=>$v))->update('tc_order_d',array("order_d_product_basic_style"=>1003,"order_d_open_startdate"=>$data["applyopen_open_startdate"],"order_d_open_enddate"=>$data["applyopen_open_enddate"]));
					if (!$res) {
						return array('res' => 'fail', 'msg' => $v.' error order_d update');
					}
				}else{
					return array('res' => 'fail', 'msg' => '该明细存在返回参数，请检查是否已开通');
				}
				
			}else{
				//echo "不存在";
				//不存在则新建
				$add_data = $data;
				$add_data['applyopen_order_d_id']=$v;
				$add_data['applyopen_return_info'] = json_encode(array('actual_startdate'=>$data["applyopen_open_startdate"],"actual_enddate"=>$data["applyopen_open_enddate"]));
				unset($add_data['applyopen_open_startdate']);
				unset($add_data['applyopen_open_enddate']);
				if(!isset($add_data['applyopen_create_time'])){
					$add_data['applyopen_create_time'] = date("Y-m-d H:i:s");
				}
				$res = $this->db->insert('tc_applyopen', $add_data);
				if (!$res) {
					return array('res' => 'fail', 'msg' => $v.' error applyopen insert');
				}
				//将明细的状态改成已开通，开始时间 结束时间反写上
				$res = $this->db->where(array("order_d_id"=>$v))->update('tc_order_d',array("order_d_product_basic_style"=>1003,"order_d_open_startdate"=>$data["applyopen_open_startdate"],"order_d_open_enddate"=>$data["applyopen_open_enddate"]));
				if (!$res) {
					return array('res' => 'fail', 'msg' => $v.' error order_d update');
				}
				//p($this->db->last_query());
				//exit;
			}
		}
		return array('res' => 'succ', 'msg' => 'add success');
		//return $data;
	}

	//接口通用方法
	//转换格式 内容转化成字段
	public function data_field($data){
		$save_data = array();
		$objName = 'applyopen';
		//p($data);
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user){
				$data['create_user'] = $user['user_id'];
			}
			unset($data['user']);
		}
		//将属性都转换成对应的数据字段
		foreach ($data as $k => $v) {
			if (empty($v)) {
				unset($data[$k]);
				continue;
			}

			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k); 
				$this_fields = $objName.'_'.$fields;
				//获取这个属性的信息
				$field_arr = $this->field_arr($this_fields,$objName);
				//p($field_arr);//exit;
				//根据不同的数据类型查询不同的值
				switch ($field_arr['attr_type']) {
					case 'enum':
						echo "枚举";exit;
						break;
					case '19': //引用
						//echo "引用";
						//获取相关数据
						$res_quote = $this->select_ids($v,$attr,$field_arr['quote_obj_name']);
						if ($res_quote){
							$save_data[$this_fields] = $res_quote;
						}else{
							echo "失败";exit;
						}
						break;
					default: //引用
						echo "未知属性类型，请排查问题";exit;
						break;
				}
			} else {
				$this_fields = $objName. '_' . $k;
				$save_data[$this_fields] = $v;
			}
		}
		return $save_data;
	}

	//查询对应属性信息
	public function field_arr($field_name,$obj_name){
		//根据对象名查询出这个对象的ID
		$res_obj_list = $this->db->select('obj_id')->where('obj_name',$obj_name)->get('dd_object')->result_array();
		if(isset($res_obj_list[0]['obj_id'])){
			$obj_id = $res_obj_list[0]['obj_id'];
		}
		//根据对象ID查询出这个字段的相关内容
		$res_obj_list = $this->db->select('attr_id, attr_name, attr_type, attr_field_name,attr_quote_id')->where(array('attr_obj_id'=>$obj_id,'attr_field_name'=>$field_name))->get('dd_attribute')->result_array();
		if(isset($res_obj_list[0])){
			$field_arr = $res_obj_list[0];
		}
		//查询出引用对象的ID
		if(isset($field_arr['attr_quote_id'])){
			$res_obj_list = $this->db->select('obj_name')->where('obj_id',$field_arr['attr_quote_id'])->get('dd_object')->result_array();
			if(isset($res_obj_list[0]['obj_name'])){
				$field_arr['quote_obj_name'] = $res_obj_list[0]['obj_name'];
			}
		}
		return $field_arr;
	}

	//引用类型查询出多个ID
	public function select_ids($v,$attr,$obj_name){
		$obj_id_attr = $obj_name.'_id';
		$fields = $obj_name.'_'.$attr;
		$table_name = 'tc_'.$obj_name;
		$res_id_list = $this->db->select($obj_id_attr)->where($fields,$v)->get($table_name)->result_array();
		if(isset($res_id_list[0])){
			//循环出一个结果集
			$res_ids_arr = array();
			foreach($res_id_list as $v){
				$res_ids_arr[] = $v[$obj_id_attr];
			}
			return $res_ids_arr;
			//用,号分割成字符串 用于in语句
			//$res_ids =  implode(",",$res_ids_arr);
			//return $res_ids;
		}else{
			echo "没有数据";exit;
		}
	}

	//********************** api接口部分 end ****************************
}
?>
