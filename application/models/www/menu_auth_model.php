<?php 
  class Menu_auth_model extends CI_Model{
  	public function add($data){
      $this->db->insert("dd_auth_menu",$data);
  	} 
  	public function check($role_id){
  		return $this->db->get_where('tc_role',array('role_id'=>$role_id))->result_array();
  	}
  	public function update($role_id,$data){
       $this->db->where('role_id',$role_id)->update('tc_role',$data);
  	}
  	public function delete($role_id){
       $this->db->where('role_id',$role_id)->delete('dd_auth_menu');
  	}
  }
 ?>