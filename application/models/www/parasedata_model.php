<?php
class parasedata_model extends CI_Model
{

	/***
	* @params string $id 关联ID
	* @params array $value 传入的数组
	* @return array 传出拼接好的数据
	*/
	public function paraseData( $id, $value )
	{
		$pre_data = array();
		$ren_data = array();
		//定义数组存储分析结果后的计算值
		$yummy = array();
		if(!empty($value['attr_formula']))
		{
			//公式说明
			$data = $value['attr_formula'];
			//首先取出要调用的函数
			//利用正则解析公式
			$parseData = $this->reg_match( '/(\w+\([^()]+\))/x', $data );
			/*得到公式数组后循环公式集合，计算出集合值后重新累加
			//sum('pkey:say_order_id', invoe.account, type=1001 and type_name=入款项 and i_status=1002)+sum('pkey:say_order_id', invoe.account, type=1001 and type_name=入款项 and i_status=1002) -- $data
			//array(sum('pkey:say_order_id', invoe.account, type=1001 and type_name=入款项 and i_status=1002),sum('pkey:say_order_id', invoe.account, type=1001 and type_name=入款项 and i_status=1002)) -- $parseData
			//array(11, 1); -- $yummy
			*/
			foreach($parseData as $k => $v)
			{
				/***
				* 说明:先通过正则'/([^(),]+)/x' 解析出对应的 函数名,实参值;
				* 利用reg_match方法解析出数组:
				* 第一个元素为执行的函数名;
				* 其后元素为传入执行函数的实参
				* 再利用回调函数call_user_func_array传入对应的方法和实参
				*/
				$match = $this->reg_match( '/([^(),]+)/x', $v );
				$func = "row_".$match[0];
				$match['id'] = $id;
				//判断要执行的方法是否存在
				if( method_exists( __CLASS__, $func ) ) 
				{
					$res = call_user_func_array( array( __CLASS__, $func ), $match );//利用回调函数取出数据执行结果
					$yummy[$k] = $res;
				}
			}
			//将得出的三个值做替换
			//$newphrase = str_replace($parseData, $yummy, $data);再使用EVAL进行动态计算
			//$str = '$resful = '.$newphrase.';'; eval($str); $resful为计算后的值
			$newphrase = str_replace($parseData, $yummy, $data);
			if (empty($newphrase)) {
				$resful = 0;
			} else {
				$str = '$resful = '.$newphrase.';';
				//开始计算运算
				eval($str);
			}
			//计算后的值赋值给新数组
			$ren_data[$value['attr_field_name']] = $resful;
		}
		
		return $ren_data;
		
	}
	
	/***
	* 说明:正则匹配 匹配出所有规则
	* @params string 正则表达式
	* @params string 公式说明
	* @return array 匹配出的结果
	*/
	private function reg_match( $pattern, $subject )
	{
		preg_match_all( $pattern, $subject, $result, PREG_PATTERN_ORDER);
		return $result[0];
	}
	
	
	/***例:'pkey:say_order_id', invoe.account, type=1001 and type_name=入款项 and i_status=1002
	* select sum(account) from invoe where say_order_id = $id and type=1001 and type_name='入款项' and i_status=1002
	* 说明:	返回值以当前方法名为键值方便管理及查询
	* @params $func 执行的函数名
	* @params $id_attr 关联ID属性
	* @params $obj_attr 对象.属性
	* @params $where 条件
	* @params $id 关联ID
	* @return string 返回执行出的结果
	*/
	private function row_sum( $func, $id_attr, $obj_attr, $where, $id )
	{
		$sql = "";
		//替换多余字符
		$id_attr = $this->ex_replace( $id_attr );
		//取出关联的ID
		$sid = $this->ex_array( $id_attr, ":" );
		//取出关联的对象和属性
		$obj_arr = $this->ex_array( $obj_attr, "." );
		//拼接SQL
		$sql = "select $func($obj_arr[1]) as ".__FUNCTION__." from $obj_arr[0] where $sid[1]=$id and $where";
		$resful = (array)$this->db->query($sql)->row();
		return $resful[__FUNCTION__];
		
	}
	
	/***例:'pkey:say_order_id', invoe.account, type=1001 and type_name=入款项 and i_status=1002
	* select count(account) from invoe where say_order_id = $id and type=1001 and type_name='入款项' and i_status=1002
	* 说明:	返回值以当前方法名为键值方便管理及查询
	* @params $func 执行的函数名
	* @params $id_attr 关联ID属性
	* @params $obj_attr 对象.属性
	* @params $where 条件
	* @params $id 关联ID
	* @return string 返回执行出的结果
	*/
	private function row_count( $func, $id_attr, $obj_attr, $where, $id )
	{
		$sql = "";
		//取出关联的ID
		$sid = ex_array( $id_attr, ":" );
		//取出关联的对象和属性
		$obj_arr = ex_array( $id, "." );
		//拼接SQL
		$sql = "select $func($obj_arr[1]) as __FUNCTION__ from $obj_arr[0] where $sid[1]=$id and $where";
		$resful = $this->db->query($sql)->row();
		return $resful[__FUNCTION__];
	}
	
	/***
	* 说明：替换不规则字符
	* @params string 要替换的字符串
	* @return string 替换后的字符串
	*/
	private function ex_replace( $str )
	{
		$str = str_replace("'","",$str);
		return $str;
	}
	
	/**
	* @params string 要分隔的字符串
	* @params string 分隔的字符
	* @return array 返回的数组
	*/
	private function ex_array( $arr, $separator )
	{
		return explode( $separator, $arr );
	}
	
}