<?php

class Product_tech_model extends CI_Model {

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'product_tech_name',
			'product_tech_code',
			'product_tech_desc',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'product_tech_code',
			'product_tech_name',
			'product_tech_desc',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
			'product_tech_code',
			'product_tech_name',
			'product_tech_desc',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'product_tech_code' => '产品编号',
			'product_tech_name' => '产品名称',
			'product_tech_desc' => '产品描述',
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'product_tech_id' => $id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data, $id) {
		$this->db->where('product_tech_id', $id)->update('tc_product_tech', $data);
	}

	//单个删除
	public function del($id) {
		$this->db->where('product_tech_id', $id)->delete('tc_product_tech');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_product_tech', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_product_tech');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_product_tech');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容

		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环
			$this->db->select('tc_goods.goods_id, tc_goods.goods_name');
			$this->db->where('tc_gprelation.gprelation_product_tech_id', $v['product_tech_id']);
			$this->db->from('tc_goods');
			$this->db->join('tc_gprelation', 'tc_goods.goods_id = tc_gprelation.gprelation_goods_id');
			$this->db->group_by('tc_goods.goods_id');
			$data[$k]['goods'] = $this->db->get()->result_array();
		}
		return $data;
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('product_tech');
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $user['user_id'];
			unset($data['user']);
		}

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
//					$json = $this->arrJson($v);
//					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}

		if ($tId = $this->db->select('product_tech_id')->get_where('tc_product_tech', array('product_tech_code' => $save_data['product_tech_code']))->row_array()) {
//			return array('res' => 'succ', 'msg' => '技术产品已存在', 'info' => array('product_tech_id' => $tId['product_tech_id']));
			$this->db->update('tc_product_tech', $save_data, array('product_tech_id' => $tId['product_tech_id']));
			return array('res' => 'succ', 'msg' => '技术产品更新', 'info' => array('product_tech_id' => $tId['product_tech_id']));
		}

		$res = $this->db->insert('tc_product_tech', $save_data);
		if ($res) {
			$res_id = $this->db->insert_id();
			return array('res' => 'succ', 'msg' => 'add success', 'info' => array('product_tech_id' => $res_id));
		} else
			return array('res' => 'fail', 'msg' => 'error product_tech save');
	}

}

?>
