<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/*
 * @Author:sky
 * @Crate Time:2015-01-20 19:28
 * @Last modified:2015-01-20 19:28
 * @Description:指定模型
 */
class Instruction_model extends CI_Model {

    function __construct() {
        parent::__construct();
		$this->db->query('set names utf8');
		date_default_timezone_set('PRC');
		$this->db->trans_strict(FALSE);
    }

	/**
	 * 接口 _addCheck 检查
	 * @param type $data 数据
	 * @param type $user 创建者id
	 * @return type array
	 */
	public function _addCheck($data = '', $user = '') { 
		$this->load->model('www/api_model', 'api');
		//基础数据验证S
		$res_data = $this->_basicCheck($data);
		if (!isset($res_data['res']) || $res_data['res'] != 'succ')
			return $res_data;
		$data = $res_data['info'];
		if (!isset($data['instruction_create_time']))
			$data['instruction_create_time'] = date("Y-m-d H:i:s", time());
		if (!isset($data['create_user_id'])) {
			if (isset($user)) {
				$res = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $user))->row_array();
				$data['instruction_create_user_id'] = $res['user_id'];
			} else
				return $this->api->_msg('指令单不能没有创建人');
		}
		//基础数据验证E
		//针对不同订单进行验证S
        $data['instruction_code'] = 'io' . $data['instruction_code'];
        return $this->_parseData($data, $user);
	}
    /*
     * @Func:_parseData
     * @Author:sky
     * @Crate Time:2015-01-21 11:02
     * @Last modified:2015-01-21 11:02
     * @Description:数据校验
     * @Return:json
     */
	private function _parseData($data, $user) {
		// if ($this->checkCode($data['instruction_code']))
			// return $this->api->_return('指令编号已存在');
		return $this->api->_return('parseData succ', $data, 'succ');
	}
	//检查订单编号
	private function checkCode($instruction_code = '') {
		$r_id = $this->db->select('instruction_id')->get_where('tc_instruction', array('instruction_code' => $instruction_code))->row_array();
		if ($r_id)
			return true;
		else
			return false;
	}
	/**
	 * 指令基本数据过滤
	 * @param array $data
	 * @return array
	 */
	private function _basicCheck($data = '') {
		if (empty($data))
			return $this->api->_return('无效指令数据');
		return $this->api->_return(__class__ . '_basicCheck succ', $data, 'succ');
	}

    /**
     * id_aGetInfo 
     * 通用查询方法
     * @param mixed $id 主键ID
     * @access public
     * @return void array
     */
	public function id_aGetInfo($id) {
		$where = array(
			'instruction_id' => $id
		);
		$data = $this->GetInfo($where);
		if (isset($data[0])) {
			$data = $data[0];
		}
		return $data;
	}

    //所有的查询都写一个通用方法 参数$where为判断条件的数组
    /**
     * GetInfo 
     * 通用查询
     * @param string $where 条件
     * @param string $limit 条数
     * @param string $like 暂时无
     * @access public
     * @return void array
     */
    public function GetInfo($where = "", $limit = "", $like = "") {
        //首先查询出本身所有的内容
        $this->db->from('tc_instruction');
        if ($where != "") {
            $this->db->where($where);
        }
        if ($limit != "") {
            $this->db->limit($limit[0], $limit[1]);
        }
        if ($like != "") {
            $this->db->like($like);
        }
        $data = $this->db->get()->result_array();
        //然后循环查询出所有引用的内容
        $data = $this->info($data); //关联查询其他的数据，下面统一用一个方法
        return $data;
    }

    /**
     * info 
     * 查询明细信息
     * @param mixed $data 主数据
     * @access public
     * @return void array
     */
    public function info($data) {
        $this->load->model('admin/enum_model', 'enum');
        $this->load->model('www/user_model', 'user');
        $this->load->model('www/department_model', 'department');
        $this->load->model('www/account_model', 'account');
        $this->load->model('www/partner_model', 'partner');
        $this->load->model('www/instruction_item_model', 'instruc_item');
        /**
         * 处理相应的数据包括明细，用户，客户等等
         */
        foreach ($data as $k => &$v) {
            //处理客户
            $account_datas_attr = $this->account->id_aGetInfo($v['instruction_account']);
            $v['instruction_account_attrs'] = $account_datas_attr;
            //处理拥有者
            $owner_datas_attr = $this->user->id_aGetInfo($v['instruction_owner']);
            $v['instruction_owner_attrs'] = $owner_datas_attr;
            //处理创建者
            $create_datas_attr = $this->user->id_aGetInfo($v['instruction_create_user_id']);
            $v['instruction_create_user_id_attrs'] = $create_datas_attr;
            //处理指令明细
            $instruction_datas_attr = $this->instruc_item->get_item_instruc($v['instruction_id']);
            $v['instruction_id_attrs'] = $instruction_datas_attr;
        }
        return $data;
    }


}

?>
