<?php

class Books_model extends CI_Model {

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'books_company',
			'books_account_id',
			'books_adate',
			'books_bdate',
			'books_trade_no',
			'books_attn_id',
			'books_department_id',
			'books_memo',
			'books_customer_info',
			'books_sale_no',
			'books_order_no',
			'books_invoice_track',
			'books_rate',
			'books_rmb',
			'books_state',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'books_company',
			'books_account_id',
			'books_adate',
			'books_bdate',
			'books_trade_no',
			'books_attn_id',
			'books_department_id',
			'books_memo',
			'books_customer_info',
			'books_sale_no',
			'books_order_no',
			'books_invoice_track',
			'books_rate',
			'books_rmb',
			'books_state',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
			'books_company',
			'books_account_id',
			'books_adate',
			'books_bdate',
			'books_trade_no',
			'books_attn_id',
			'books_department_id',
			'books_memo',
			'books_customer_info',
			'books_sale_no',
			'books_order_no',
			'books_invoice_track',
			'books_rate',
			'books_rmb',
			'books_state',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'books_company' => '公司',
			'books_subsidiary' => '所属子公司',
			'books_bank_account' => '账户',
			'books_adate' => '实际到账/出账日期',
			'books_bdate' => '确认到账/出账日期',
			'books_trade_no' => '交易号',
			'books_attn_id' => '经办人',
			'books_department_id' => '部门',
			'books_memo' => '备注',
			'books_customer_info' => '客户信息',
			'books_sale_no' => '销售记录号',
			'books_order_no' => '订单号',
			'books_invoice_track' => '发票追踪',
			'books_rate' => '汇率',
			'books_rmb' => '折合RMB',
			'books_state' => '状态',
			'books_createtime' => '添加时间',
			'books_updatetime' => '更新时间',
			'books_Note' => '摘要',
			'books_debit_amount' => '借方发生额',
			'books_Credit_amount' => '贷方发生额',
			'books_cashier' => '出纳',
			'books_accounting' => '会计',
			'books_sales_no' => '销售号',
			'books_other_memo' => '其他备注',
			'books_number' => '日记账编号',
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'books_id' => $id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data, $id) {
		return $this->db->where('books_id', $id)->update('tc_books', $data);
	}

	//单个删除
	public function del($id) {
		$this->db->where('books_id', $id)->delete('tc_books');
	}

	//单个新增
	public function add($data) {
		if (!isset($data['books_number'])) {
			$res = $this->db->select('nextid')->get_where('dd_nextid', array('id' => 1))->row_array();
			$nextid = $res['nextid'] = $res['nextid'] + 1;
			$this->db->where('id', 1);
			$this->db->update('dd_nextid', $res);
			$data['books_number'] = "b-" . $nextid;
		}
		$this->db->insert('tc_books', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_books');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_books');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容
		foreach ($data as $k => $v) {

			//所有属性循环
			//当属性类型为单选时
			if ($v['books_company'] != "" and $v['books_company'] != 0) {
				$this->load->model('admin/enum_model', 'enum');
				$this->books->db->query('SET NAMES UTF8');
				$data[$k]['books_company_arr'] = $this->enum->getlist(114, $data[$k]['books_company']);
			}
			
			//所属子公司循环 2014/8/25
			if ($v['books_subsidiary'] != "" and $v['books_subsidiary'] != 0) {
				$this->load->model('admin/enum_model', 'enum');
				$this->books->db->query('SET NAMES UTF8');
				$data[$k]['books_subsidiary_arr'] = $this->enum->getlist(628, $data[$k]['books_subsidiary']);
			}
			//所属子公司循环 2014/8/25
			
			//p($data[$k]);die;
			//当属性类型为下拉单选时
			if ($v['books_bank_account'] != "" and $v['books_bank_account'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['books_bank_account_arr'] = $this->enum->getlist(115, $data[$k]['books_bank_account']);
			}

			if ($v['books_attn_id'] != "" and $v['books_attn_id'] != 0) {
				$this->load->model('www/user_model', 'user');
				$data[$k]['books_attn_id_arr'] = $this->user->id_aGetInfo($v['books_attn_id']);
			}


			if ($v['books_department_id'] != "" and $v['books_department_id'] != 0) {
				$this->load->model('www/department_model', 'department');
				$data[$k]['books_department_id_arr'] = $this->department->id_aGetInfo($v['books_department_id']);
			}
			
			//当属性类型为单选时
			if ($v['books_invoice_track'] != "" and $v['books_invoice_track'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['books_invoice_track_arr'] = $this->enum->getlist(133, $data[$k]['books_invoice_track']);
			}

			//当属性类型为单选时
			if ($v['books_state'] != "" and $v['books_state'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['books_state_arr'] = $this->enum->getlist(136, $data[$k]['books_state']);
			}


			if ($v['books_cashier'] != "" and $v['books_cashier'] != 0) {
				$this->load->model('www/user_model', 'user');
				$data[$k]['books_cashier_arr'] = $this->user->id_aGetInfo($v['books_cashier']);
			}



			if ($v['books_accounting'] != "" and $v['books_accounting'] != 0) {
				$this->load->model('www/user_model', 'user');
				$data[$k]['books_accounting_arr'] = $this->user->id_aGetInfo($v['books_accounting']);
			}
		}
		return $data;
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('books');
		//生成创建人id
		//p($this->session->userdata);exit;
		if (isset($this->session->userdata)) {
			$create_user_id = $this->session->userdata['user_id'];
			unset($data['user']);
		} else {
			if (isset($data['user'])) {
				$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
				if ($user)
					$create_user_id = $user['user_id'];
				unset($data['user']);
			}
		}


		foreach ($data as $k => $v) {
			$v = trim($v, ' ');
			if($v==''){
				unset($data[$k]);
				continue;
			}

			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
						{
							$save_data[$this_fields] = $res_enum;
						}
						else
						{
							
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						}
							
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
						{
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						}
							
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
//					$json = $this->arrJson($v);
//					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}
		
		//p("dd");exit;
		//cus lee 2014-04-24 如果没有编号，添加1个编号 start
		
		if (!isset($save_data['number'])) {
			$res = $this->db->select('nextid')->get_where('dd_nextid', array('id' => 1))->row_array();
			$nextid = $res['nextid'] = $res['nextid'] + 1;
			$this->db->where('id', 1);
			$this->db->update('dd_nextid', $res);
			$save_data['books_number'] = "b-" . $nextid;
		}
		//cus lee 2014-04-24 如果没有编号，添加1个编号 end
		$res = $this->db->insert('tc_books', $save_data);
		// error_log(print_r(array('Date'=>date('Y-m-d H:i:s'), $this->db->last_query()),true)."\n",3,'./logs/yuancheng'.date("Ymd_h")."log.log");

		//echo $this->db->last_query();exit;
		if ($res) {
			return array('res' => 'succ', 'msg' => '');
		} else
		{
			return array('res' => 'fail', 'msg' => 'error books save');
		}
			
	}

	public function getAmount($post) {
		if ($post['amount'] == 'debit') {
			$amount = 'books_debit_amount'; //借方
		} else {
			$amount = 'books_Credit_amount'; //贷方
		}
		$between = " AND " . $post['date_type'] . " BETWEEN \"" . $post['date_start'] . "\" AND \"" . $post['date_end'] . "\"";

		$sql = 'SELECT sum(' . $amount . ') as amount FROM tc_books WHERE 1';
		$sql .= $between;

		if (isset($post['books_bank_account']))
			$sql .= ' AND books_bank_account = ' . $post['books_bank_account'];
		if (isset($post['books_company']))
			$sql .= ' AND books_subsidiary = ' . $post['books_company'];

		$res = $this->db->query($sql)->row_array();
		if ($res['amount'] >= 0) {
			$result = sprintf("%0.2f", $res['amount']);
			return '￥ ' . $result;
//			return '￥ ' . $res['amount'];
		} else
			return '￥ 0.00';
	}

	public function checkOrder($books_id) {
		$books = $this->db->select('books_bdate,books_department_id')->from('tc_books')->where('books_id', $books_id)->get()->row_array();
		if (empty($books['books_bdate']) && empty($books['books_department_id'])) {
			$res = $this->db->delete('tc_books', array('books_id' => $books_id));
			if ($res) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * 日记账编号，是否存在关联
	 * @param type $books_number
	 * @return boolean
	 */
	public function checkBooksNumber($books_number = '') {
		if (empty($books_number)) {
			return false;
		} else {
			$res_b = $this->GetInfo($where = array('books_number' => $books_number));
			if (!$res_b) {
				return false;
			} elseif (count($res_b) > 1) { //重复日记账编号
				return false;
			} elseif ($res_b[0]['books_bdate'] or $res_b[0]['books_department_id'] or $res_b[0]['books_order_no']) { //日记账已经关联，确认到账时间、部门id、订单编号
			//暂时
				return false;
			} else
				return true;
		}
	}

	//2014-07-17 api end
	public function _addCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无日记账数据');
		return $this->api->_return(__class__ . ' _addCheck succ', $data, 'succ');
	}

	public function _updateCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无日记账数据');
		return $this->api->_return(__class__ . ' _updateCheck succ', $data, 'succ');
	}
	//2014-07-17 api end
	
	/**
	* @例:数据查询
	* @params $data 待查询的数据条件
	* @params return array 返回的数据集结果
	*/
	public function listGetInfoIn( $data ) {
		return $this->db->from("tc_books")->where_in( "books_order_no", $data )->get()->result_array();
	}
	
	/**
	* @例:更新特定的数据
	* @params $data 待查询的数据条件
	* @params return 无
	*/
	public function _upInfo( $data ) {
		//待更新的数据
		$updata = array( 
						 "books_attn_id"       => "",
						 "books_department_id" => "",
						 "books_order_no"      => "",
						);
		$this->db->where_in( "books_order_no", $data )->update( "tc_books", $updata );
	}
}

?>
