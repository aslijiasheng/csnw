<?php
class Order_adjunct_model extends CI_Model{

	//这里代表那些字段在列表上显示出来
	public function listLayout(){
		return array(
			
			'order_adjunct_name',
			
			'order_adjunct_url',
			
			'order_adjunct_atime',
			
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout(){
		return array(
			
			'order_adjunct_name',
			
			'order_adjunct_url',
			
			'order_adjunct_atime',
			
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout(){
		return array(
			
			'order_adjunct_name',
			
			'order_adjunct_atime',
			
		);
	}

	//属性对应的中文标签
	public function attributeLabels(){
		return array(
			
			'order_adjunct_name' => '附件名称',
			
			'order_adjunct_url' => '下载地址',
			
			'order_adjunct_atime' => '创建时间',
			
			'order_adjunct_order_id' => '订单ID',
			
		);
	}

	//列表查询
	/*
		$page //获得当前的页面值
		$perNumber //每页显示的记录数
	*/
	public function listGetInfo($where,$page=1,$perNumber=10,$like=""){
		$limitStart = ($page-1)*$perNumber+1-1; //这里因为数据库是从0开始算的！所以要减1
		$limit=array($perNumber,$limitStart);
		//p($limit);
		$data = $this->GetInfo($where,$limit,$like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id){
		$where=array(
			'order_adjunct_id'=>$id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data,$id){
		$this->db->where('order_adjunct_id',$id)->update('tc_order_adjunct',$data);
	}

	//单个删除
	public function del($id){
		$this->db->where('order_adjunct_id',$id)->delete('tc_order_adjunct');
	}

	//单个新增
	public function add($data){
		$this->db->insert('tc_order_adjunct',$data);
		$insert_id=$this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where="",$like=""){
		$this->db->from('tc_order_adjunct');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}


	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where="",$limit="",$like=""){
		//首先查询出本身所有的内容
		$this->db->from('tc_order_adjunct');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($limit!=""){
			$this->db->limit($limit[0],$limit[1]);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$data=$this->db->get()->result_array();
		//然后循环查询出所有引用的内容
		
		foreach ($data as $k=>$v){
			//p($v);die;
			//所有属性循环
			
				
			
				
			
				
			
			
				
			
				
			
				
			
			
				
			
				
			
				
			
			
				 
			if($v['order_adjunct_order_id']!="" and $v['order_adjunct_order_id']!=0){
				$this->load->model('www/order_model','order');
				$data[$k]['order_adjunct_order_id_arr'] = $this->order->id_aGetInfo($v['order_adjunct_order_id']);
			}
				
			
				
			
				
			
			
		}
		return $data;
	}
}
?>
