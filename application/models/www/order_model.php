<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Order_model extends CI_Model {

	/***
	* @params $data 传入的订单ID
	* @return array 返回的值
	*/
	public function parseData($id)
	{
		if( isset( $id ) )
		{
			//首先查出当前表中所有计算型字段
			$this->load->model('admin/object_model', 'obj');
			$this->load->model('www/parasedata_model', 'parase');
			//取出表名
			$obj_name = strtolower( substr( __CLASS__, 0, strpos( __CLASS__, "_" ) ) );
			//取出计算型字段所有属性
			$obj_attr = $this->obj->nameGetInfo($obj_name);
			
			//循环开始,不断解析计算型字段数据
			foreach( $obj_attr['attr_arr'] as $key=>$value )
			{
				//解析数据并处理
				$parse_data[$key] = $this->parase->paraseData( $id, $value );
			}
			//定义新数组将二维数组转化为一维数组方便数据更新,减少SQL压力
			$parse_up = array();
			//循环开始,将二维数组转化为一维数组
			foreach( $parse_data as $k=>$v )
			{
				foreach( $v as $kk=>$vv )
				{
					//将字段名定义为键值,将计算后的数据定义为元素
					$parse_up[$kk] = $vv;
				}
			}
			//更新计算结果数据
			$this->update($parse_up, $id);
			return array("status"=>"succ","description"=>"data is succ");
		}
		else
		{
			return array("status"=>"failed","description"=>"data is not exits");
		}
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('order');
		// 创建者id，根据接口账号走
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $save_data['order_create_user_id'] = $user['user_id'];
			unset($data['user']);
		}
		foreach ($data as $k => $v) {
			if (empty($v)) {
				unset($data[$k]);
				continue;
			}
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;
				if ($fields == 'type') {
					$where = array('type_' . $attr => $v);
					$this->db->query("set names utf8");
					$res = $this->db->select('type_id')->get_where('dd_type', $where)->row_array();
					if ($res)
						$save_data['type_id'] = $res['type_id'];
					else
						return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
				} else {
					switch ($objDetail['obj_attr'][$fields]) {
						case 'enum':
							$attr_id = $objDetail['obj_attr_id'][$fields];
							$res_enum = $this->api->parseEnum($v, $attr_id);
							if ($res_enum)
								$save_data[$this_fields] = $res_enum;
							else
								return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
							break;
						default: //引用
							$q_obj = $objDetail['obj_attr'][$fields];
							$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
							if ($res_quote)
								$save_data[$this_fields] = $res_quote;
							else {
								$res_qv = $this->api->apiSync($q_obj, $v);
								if ($res_qv['res'] == 'fail')
									return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . '] apiSync none');
								else
									$save_data[$this_fields] = $res_qv['info'][$q_obj . '_id'];
//								return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
							}
							break;
					}
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				$save_data[$this_fields] = $v;
			}
		}
		
		if (isset($create_user_id) && isset($save_data['order_create_user_id']))
			$save_data['order_create_user_id'] = $create_user_id;
		if (!isset($save_data['type_id']) || empty($save_data['type_id']))
			return array('res' => 'fail', 'msg' => 'error none order type');
		else {
			$number = isset($save_data['order_number']) ? $save_data['order_number'] : $this->getNumber($save_data['type_id']);
			//获取销售相应部门（合同只打销售工号）
			if (isset($save_data['order_owner']) && !isset($save_data['order_department'])) {
				$res_department = $this->db->select('user_department')->get_where('tc_user', array('user_id' => $save_data['order_owner']))->row_array();
				if ($res_department)
					$save_data['order_department'] = $res_department['user_department'];
			}
			
			switch ($save_data['type_id']) {
				case '6': //销售订单
				
					$save_data['order_number'] = 'so' . $number;
					if ($this->checkNumber($save_data['order_number'])) {
						return array('res' => 'fail', 'msg' => '订单编号已存在');
					}
					if (isset($save_data['order_agreement_no']) && $this->checkAgreement($save_data['order_agreement_no'])) {
						return array('res' => 'fail', 'msg' => '合同编号已存在');
					}
					if (!isset($save_data['order_account']) or empty($save_data['order_account'])) {
						return array('res' => 'fail', 'msg' => '客户不能为空');
					}
					$result = $this->saleOrder($save_data);
					break;
				case '7': //线上订单
					$save_data['order_number'] = 'oo' . $number;
					if ($this->checkNumber($save_data['order_number']))
						return array('res' => 'fail', 'msg' => '订单编号已存在');
					$result = $this->onlinesOrder($save_data);
					break;
				case '8': //返点订单
					$save_data['order_number'] = 'ro' . $number;
					if ($this->checkNumber($save_data['order_number']))
						return array('res' => 'fail', 'msg' => '订单编号已存在');
					$result = $this->rebatesOrder($save_data);
					break;
				case '9': //内部划款订单
					$save_data['order_number'] = 'po' . $number;
					if ($this->checkNumber($save_data['order_number']))
						return array('res' => 'fail', 'msg' => '订单编号已存在');

					//检查必填项
					$fields = array('order_transfer_state', 'order_transfer_name', 'order_change_out', 'order_change_into', 'order_out_examine', 'order_relation_order_id', 'order_change_into_money', 'order_transfer_reason');
					$diff = array_diff($fields, array_keys($save_data));
					if ($diff)
						return array('res' => 'fail', 'msg' => '缺少必填项', 'info' => $diff);

					$result = $this->pennyOrder($save_data);
					break;
				case '10': //预收款订单
					$save_data['order_number'] = 'do' . $number;
					if ($this->checkNumber($save_data['order_number']))
						return array('res' => 'fail', 'msg' => '订单编号已存在');
					$result = $this->depositOrder($save_data);
					break;
				case '13': //提货销售订单
					$save_data['order_number'] = 'lo' . $number;
					
					//if ($this->checkNumber($save_data['order_number'])) {
					//	return array('res' => 'fail', 'msg' => '订单编号已存在');
					//}
					$result = $this->deliveryOrder($save_data);
					
					break;
				default:
					return array('res' => 'fail', 'msg' => 'error order type');
					break;
			}
			if (isset($result['fail']) && $result['res'] == 'fail') {
				return array('res' => 'fail', 'msg' => $result['msg']);
			}
		}
		$res_id = $this->db->select('order_id')->get_where('tc_order', array('order_number' => $result['order_number']))->result_array();
		if ($res_id)
			return array('res' => 'fail', 'msg' => 'error order save [重复订单]');
		$save_data = $result;
		
		$res = $this->db->insert('tc_order', $save_data);
		
		if ($res) {
			$info['order_id'] = $this->db->insert_id();
			$info['order_number'] = $save_data['order_number'];
			$info['order_agreement_no'] = isset($save_data['order_agreement_no']) ? $save_data['order_agreement_no'] : '';
			return array('res' => 'succ', 'msg' => 'add success', 'info' => $info);
		} else
			return array('res' => 'fail', 'msg' => 'error order save');
	}

	//销售订单
	private function saleOrder($save_data) {
		//获取销售相应部门（合同只打销售工号）
		if (!isset($save_data['order_nreview']) && $save_data['order_create_user_id']) {
			// 根据创建人筛选订单
			$res_cid = $this->db->select('order_nreview')->from('tc_order')->where(array('order_create_user_id' => $save_data['order_create_user_id'], 'type_id' => 6))->order_by('order_id', 'desc')->get()->row_array();
			if (isset($res_cid['order_nreview']) && $res_cid['order_nreview'] == '2109') {
				$save_data['order_nreview'] = '2108';
			} else {
				$save_data['order_nreview'] = '2109';
			}
		}
		return $save_data;
	}

	//线上订单
	private function onlinesOrder($save_data) {
		//获取销售相应部门（合同只打销售工号）
		if (!isset($save_data['order_nreview']) && $save_data['order_create_user_id']) {
			// 根据创建人筛选订单
			$res_cid = $this->db->select('order_nreview')->from('tc_order')->where(array('order_create_user_id' => $save_data['order_create_user_id'], 'type_id' => 7))->order_by('order_id', 'desc')->get()->row_array();
			if (isset($res_cid['order_nreview']) && $res_cid['order_nreview'] == '2109') {
				$save_data['order_nreview'] = '2108';
			} else {
				$save_data['order_nreview'] = '2109';
			}
		}
		// 1000017 短信同步账号
		if ($save_data['order_create_user_id'] == '1000017') {
			$save_data['order_typein'] = $save_data['order_owner'];
		}
		return $save_data;
	}

	//返点订单
	private function rebatesOrder($save_data) {
		return $save_data;
	}

	//内部划款订单
	private function pennyOrder($save_data) {
		if ($save_data['order_change_out']) {
			$this->load->model('www/user_mode', 'user');
			$res_u = $this->user->id_aGetInfo($save_data['order_change_out']);
			if ($res_u)
				$save_data['order_department'] = $res_u['user_department'];
		}
		return $save_data;
	}

	//预收款订单
	private function depositOrder($save_data) {
		//销售人员：s0194  业务主管：s0737  财务人员：s1861  部门：跟销售人员的部门
		//$userArr = array('s0194', 's0737', 's1861');
		$userArr = array('s0194', 's0737', 's0674'); //财务戴汉君转岗换成司文欣
		$users = $this->db->from('tc_user')->where_in('user_gonghao', $userArr)->get()->result_array();
		foreach ($users as $user) {
			$resUser[$user['user_gonghao']]['id'] = $user['user_id'];
			$resUser[$user['user_gonghao']]['department'] = $user['user_department'];
		}

		if (!isset($save_data['order_owner']))
			$save_data['order_owner'] = $resUser['s0194']['id']; //销售
		if (!isset($save_data['order_review']))
			$save_data['order_review'] = $resUser['s0737']['id']; //业务主管
		if (!isset($save_data['order_finance']))
			//$save_data['order_finance'] = $resUser['s1861']['id']; //财务人员
			$save_data['order_finance'] = $resUser['s0674']['id']; //财务人员 //财务戴汉君转岗换成司文欣
		if (!isset($save_data['order_department']))
			$save_data['order_department'] = $resUser['s0194']['department']; //部门

		return $save_data;
	}

	//提货销售订单
	private function deliveryOrder($save_data) {
		//获取销售相应部门（合同只打销售工号）
		if (!isset($save_data['order_nreview'])) {
			// 根据创建人筛选订单
			$res_cid = $this->db->select('order_nreview')->from('tc_order')->where(array('type_id' => 13))->order_by('order_id', 'desc')->get()->row_array();
			if (isset($res_cid['order_nreview']) && $res_cid['order_nreview'] == '2109') {
				$save_data['order_nreview'] = '2108';
			} else {
				$save_data['order_nreview'] = '2109';
			}
		}
		return $save_data;
	}

	/**
	 * 订单编号
	 * @param  string $type_id 订单类型
	 * @return string order_number
	 */
	private function getNumber($type_id = '') {
		date_default_timezone_set('PRC');
		$res = $this->db->select('order_number')->get_where('dd_number', array('order_type' => $type_id))->row_array();
		if (empty($res['order_number'])) {
			$this->db->where('order_type', $type_id);
			$res['order_number'] = date('Ymd', time()) . '0001';
			$this->db->update('dd_number', $res);
		}
		$now = date('Ymd', time());
		$tail = substr($res['order_number'], 8, 4);
		if ($now != substr($res['order_number'], 0, 8)) {
			$tail = '0001';
		} else
			$tail = str_pad($tail + 1, 4, "0", STR_PAD_LEFT);
		$data = date('Ymd', time()) . $tail;
		$this->db->where('order_type', $type_id);
		$this->db->update('dd_number', array('order_number' => $data));
		return $res['order_number'];
	}

	//检查订单编号
	private function checkNumber($order_number = '') {
		$r_id = $this->db->select('order_id')->get_where('tc_order', array('order_number' => $order_number))->row_array();
		if ($r_id)
			return true;
		else
			return false;
	}

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'order_number',
			'order_create_time',
			'order_department',
			'order_owner',
			'order_create_user_id',
			'order_account',
			'order_agent',
			'order_htbh',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'order_number',
			'order_create_user_id',
			'order_create_time',
			'order_department',
			'order_owner',
			'order_account',
			'order_agent',
			'order_htbh',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
			'order_number',
			'order_create_time',
			'order_department',
			'order_owner',
			'order_create_user_id',
			'order_account',
			'order_agent',
			'order_htbh',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
//            'order_number' => '订单编号',
//            'order_create_user_id' => '订单创建者id',
//            'order_create_time' => '订单创建时间',
//            'order_department' => '所属部门',
//            'order_owner' => '所属销售',
//            'order_account' => '所属客户',
//            'order_agent' => '所属代理商',
//            'order_agreement_no' => '合同编号',
//            'order_amount' => '订单金额',
//            'order_finance' => '财务人员',
//            'order_agreement_name' => '合同名称',
//            'order_review' => '业务主管',
//            'order_nreview' => '内审人员',
//            'order_if_renew' => '是否续费订单',
//            'order_rebate_amount' => '可返点总金额',
//			'transfer_name'=>'款项名称',
//
			'order_number' => '订单编号',
			'order_create_time' => '订单创建时间',
			'order_create_user_id' => '订单创建者id',
			'order_department' => '所属部门',
			'order_subsidiary' => '所属子公司',
			'order_owner' => '所属销售',
			'order_account' => '所属客户',
			'order_agent' => '所属代理商',
			'order_agreement_no' => '合同编号',
			'order_amount' => '订单金额',
			'order_finance' => '财务人员',
			'order_agreement_name' => '合同名称',
			'order_review' => '业务主管',
			'order_nreview' => '内审人员',
			'order_if_renew' => '是否续费订单',
			'order_rebate_amount' => '可返点总金额',
			'order_is_cancel' => '订单是否作废',
			'order_relation_order_id' => '关联销售订单',
			'order_transfer_name' => '款项名称',
			'order_change_out' => '转出方',
			'order_change_into' => '转入方',
			'order_out_examine' => '转出方主管审核',
			'order_change_into_money' => '转入金额',
			'order_transfer_state' => '内划状态',
			'order_transfer_date' => '确认划款日期',
			'order_PassID' => 'PassID',
			'order_customer_type' => '客户类型',
			'order_rmb_type' => '返款类型',
			'order_rmb_amount' => '申请支出金额',
			'order_rmb_handlingcharge' => '手续费承担方',
			'order_rmb_pay_method' => '打款方式',
			'order_rmb_bank' => '汇款银行',
			'order_state' => '订单状态',
			'order_account_type' => '账户类型',
			'order_account_name' => '账户名称',
			'order_account_number' => '银行账号',
			'order_bank_name' => '开户银行名称',
			'order_bank_country' => '开户银行所在城市',
			'order_payment_number' => '支付宝账号',
			'order_rmb_reason' => '返款原因',
			'order_rmb_person' => '选择审核人员',
			'order_rmb_user' => '经手人',
			'order_rmb_finance_id' => '选择审核财务',
			'order_prepay_company' => '公司名称',
			'order_sale_source' => '销售项目',
			'order_transfer_reason' => '划款原因',
			'order_rebates_type' => '返点类型',
			'order_rebates_state' => '返点订单状态',
			'order_rebates_remark' => '返点备注',
			'order_typein' => '录入人'
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		
		$where = array(
			'order_id' => $id
		);
		$data = $this->GetInfo($where);
		if (isset($data[0])) {
			$data = $data[0];
		}
		
		return $data;
	}

	//单个修改
	public function update($data, $id) {
		$affer_row = $this->db->where('order_id', $id)->update('tc_order', $data);
		return $affer_row;
	}

	//单个删除
	public function del($id) {
		$this->db->where('order_id', $id)->delete('tc_order');
	}

	//单个新增
	public function add($data) {
		//循环里面的属性！
		foreach ($data as $k => $v) {
			//通过.来判断关联的属性
			if (strstr($k, '.')) {
				$kk = explode(".", $k);
				//暂时只支持两层结构
				//首先查询出这个属性是什么类型
				$this->load->model('admin/attribute_model', 'attribute');
				$this->attribute->db->query('SET NAMES UTF8');
				$where = array(
					'attr_name' => $kk[0],
					'attr_obj_id' => 20
				);
				$attr_arr = $this->attribute->whereGetInfo($where);
				$attr_arr = $attr_arr[0];
				if ($attr_arr['attr_type'] == 19) { //当属性类型为引用的时候
					//去引用的那个对象上查询数据
					$obj_name = $attr_arr['attr_quote_id_arr']['obj_name'];
					$this->load->model('www/' . $obj_name . '_model', $obj_name);
					$this->$obj_name->db->query('SET NAMES UTF8');
					$where = array(
						$obj_name . "_" . $kk[1] => $v,
					);
					//p($where);exit;
					$data2 = $this->$obj_name->GetInfo($where);
					//p($data2);
					if (!isset($data2[0])) {
						//echo $k."参数没有早到";
						//最后转化成有用的参数
						$data[$kk[0]] = "";
						//删除前面没用的参数
						unset($data[$k]);
					} else {
						$data2 = $data2[0];
						$id2 = $data2[$obj_name . "_id"];
						//p($id2);
						//最后转化成有用的参数
						$data[$kk[0]] = $id2;
						//删除前面没用的参数
						unset($data[$k]);
					}
				}
			} else {
				//p($k."不包含");
			}
		}
		//p($data);
		//把所有数据前面加上对项目，转换成字段
		foreach ($data as $k => $v) {
			//其中的type需要单独转换下
			if ($k == "type") {
				$data['type_id'] = $v;
				unset($data[$k]);
			} else {
				//最后转化成有用的参数
				$data["order_" . $k] = $v;
				//删除前面没用的参数
				unset($data[$k]);
			}
		}
		//最后写入数据库
		//p($data);
		$this->db->insert('tc_order', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_order');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_order');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容
		$data = $this->info($data); //关联查询其他的数据，下面统一用一个方法
		return $data;
	}
	
	//根据order_number查询order_id
	public function getOrderId( $where ) {
		$res = $this->db->select('order_id')->from('tc_order')->where($where)->get()->result_array();
		return $res['0'];
	}
	
	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function apiGetInfo( $sql ) {
		//首先查询出本身所有的内容
		$data = $this->db->query($sql)->result_array();
		//然后循环查询出所有引用的内容
		$data = $this->info($data); //关联查询其他的数据，下面统一用一个方法
		return $data;
	}

	// api 创建对象接口
	public function add_new($api_data, $object, $action, $d_table = 'order_d') {

		$this->load->model('www/api_model', 'api');
		$this->api->add($api_data, $object);

		exit;

		$this_class = strtolower(rtrim(__CLASS__, '_model'));
		$this_fun = strtolower(trim(__FUNCTION__));
		$save_data = array();
		$error_data = array();

		$this->db->query('SET NAMES UTF8');
		$this->load->model('admin/enum_model', 'enum'); //枚举对象
		$this->load->model('admin/object_model', 'obj'); //对象
		$this->load->model('admin/type_model', 'type'); //API日志
		$this->load->model('admin/api_error_model', 'err'); //API日志
		$this->load->model('admin/attribute_model', 'attr'); //属性对象

		$this->db->trans_start(); //事物开始
		// 获取所有对象
		$obj_list = $this->obj->objlist();
		foreach ($obj_list as $v) {
			$obj_arr[$v['obj_id']] = $v['obj_name'];
		}
		// 获取当前对象所有引用字段
		$attr_list = $this->attr->check(array_search($this_class, $obj_arr));
		foreach ($attr_list as $k => $v) {
			if (isset($v['attr_quote_id_arr'])) {
				$attr_arr[$v['attr_name']] = $v['attr_quote_id_arr']['obj_name'];
			}
		}
		// 循环数据，拆分处理
		foreach ($api_data as $k => $v) { //$k字段名称.对象 $v数据
			if (strpos($k, '.')) { // 获取带.的key
				$key_arr = explode('.', $k); // 拆分
				list($obj, $attr) = $key_arr;
				// 根据引用获取对象
				if (in_array($obj, array_keys($attr_arr))) {
					$s_obj = $attr_arr[$obj]; //需要搜索内容的model对象
					if ($this->db->field_exists($attr, 'tc_' . $s_obj)) { // 判断数据库字段是否存在
						$this->load->model('www/' . $s_obj . '_model', $s_obj); // 载入相应 model
						$where = array($attr => $v);
						$res = $this->$s_obj->GetInfo($where);
						$save_data[$this_class . '_' . $obj] = $res[0][$s_obj . '_id']; // ? 是否 account_id
					} else {
						// $error_data[] = '存在对象不存在字段'.$obj.' => '.$v;
						return json_encode(array("res" => "fail", "msg" => "error fields.object.fields", "info" => "excess fields [" . $k . "]=>" . $v));
					}
				} elseif ($attr == 'enum_name' || $attr == 'enum_key') { // ? 优化 关联查询
					$obj_id = array_search($this_class, $obj_arr);
					$where = array('attr_obj_id' => $obj_id, 'attr_name' => $obj); // 获取attr_id
					$res_attr = $this->attr->whereGetInfo($where);
					$where = array('attr_id' => $res_attr['attr_id'], $attr => $v);
					$res_enum = $this->enum->whereGetInfo($where);
					$save_data[$this_class . '_' . $obj] = $res_enum['enum_key'];
				} elseif ($attr == 'type_id' || $attr == 'type_name') { // 类型
					$res_type = $this->type->whereGetInfo(array($attr => $v));
					$save_data['type_id'] = $res_type['type_id'];
				} else { //不存在的引用字段
					// $error_data[] = '不存在对象'.$obj.' => '.$v;
					return json_encode(array("res" => "fail", "msg" => "error fields.object", "info" => "excess fields [" . $k . "]=>" . $v));
				}
			} elseif ($k !== 'detailed') { // 普通数据
				if ($this->db->field_exists($this_class . '_' . $k, 'tc_' . $this_class)) { // 判断数据库字段是否存在
					$save_data[$this_class . '_' . $k] = $v;
				} else
					return json_encode(array("res" => "fail", "msg" => "error fields", "info" => "excess fields [" . $k . "]=>" . $v));
			}
		}
		$this->db->insert('tc_' . $this_class, $save_data);
		$res_id = $this->db->insert_id();
		if (isset($api_data['detailed'])) {// 处理明细内容
			$detail_arr = json_decode($api_data['detailed'][$d_table], true);
			foreach ($detail_arr as $dk => $dv) {
				$res_d = $this->dealDetailData($dv);
				if ($res_d) {
					$this->saveOrderDetail($res_d, $res_id); // 保存商品明细
				} else
					return $res_d;
			}
		}
//		$this->db->query("update from  tc_order_d set order_d_goods_name='1111' where order_d_id=10000");
//事物处理
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return json_encode(array("res" => "fail", "msg" => "error " . $this_class . " -> " . $this_fun, "info" => "sql insert error"));
		} else {
			$this->db->trans_commit();
			$this->db->close();
			return json_encode(array("res" => "succ", "msg" => "succ", "info" => ""));
		}
// $this->db->trans_complete(); //结束事物
	}

	private function dealDetailData($detail_data) {
		foreach ($detail_data as $dk => $dv) {
			if (strpos($dk, '.')) { // 拆分. 处理
				$detail_arr = explode('.', $dk);
				list($d_obj, $d_attr) = $detail_arr;
				if ($d_obj == 'goods') { // 商品信息
					$this->load->model('www/' . $d_obj . '_model', $d_obj); // 载入相应的model
					$where = array($d_obj . '_' . $d_attr => $dv);
					$res = $this->$d_obj->GetInfo($where); // 查询
					$result['goods_code'] = $res[0]['goods_code'];
					$result['goods_name'] = $res[0]['goods_name'];
					$result['goods_visable'] = $res[0]['goods_visable'];
					$result['product_arr'] = $this->dealProductData($res[0]); // 商品信息
				} elseif ($d_obj == 'goods_price') { // 价格信息
					$this->load->model('www/' . $d_obj . '_model', $d_obj); // 载入相应的model
					$where = array($d_obj . '_' . $d_attr => $dv);
					$res = $this->$d_obj->GetInfo($where); // 查询
					$result['goods_primecost'] = $res[0]['goods_price_cycle_price'];
					$result['goods_price_effective_days'] = $res[0]['goods_price_effective_days'];
				}
			} else {
				$result['goods_disc'] = $detail_data['goods_amount'];
			}
		}
		return $result;
	}

	private function dealProductData($data) {
		$this->load->model('www/bgrelation_model', 'bgr');
		$this->load->model('www/gprelation_model', 'gpr');
		$this->load->model('www/product_basic_model', 'pb');
		$this->load->model('www/goods_price_model', 'gp');
		$this->load->model('www/goods_model', 'goods');

		$res_data = array();
		if (isset($data['goods_type']) && $data['goods_type'] == '1001') {
			// 处理捆绑商品
			$where = array('bgrelation_bundle_id' => $data['goods_id']);
			$res_bgr = $this->bgr->GetInfo($where); // 返回多个商品绑定信息
			if ($res_bgr) { //捆绑商品处理多个标准商品
				foreach ($res_bgr as $k => $v) {
					$where = array('gprelation_goods_id' => $v['bgrelation_goods_id']);
					$res_gpr = $this->gpr->GetInfo($where); //返回商品内所有产品
					// p($where);
					if ($res_gpr) { // 包含多个
						if (count($res_gpr) > 1) { //多个基础产品
							foreach ($res_gpr as $kk => $vv) {
								// 获取基本产品信息
								$where = array('product_basic_id' => $res_gpr[$kk]['gprelation_product_basic_id']);
								$res_pb = $this->pb->GetInfo($where);
								$res_data[$k][$kk]['product_basic_name'] = $res_pb[$kk]['product_basic_name']; // 基础产品名称
								$res_data[$k][$kk]['product_basic_code'] = $res_pb[$kk]['product_basic_code']; // 基础产品编码
								$res_data[$k][$kk]['product_rate'] = $res_gpr[$kk]['gprelation_rate']; // 比例
								$res_data[$k][$kk]['group_price'] = $res_bgr[0]['bgrelation_group_price'];

								//p($where);
								exit;
								echo'<hr>';
							}
						} else { // 单一基础产品
							// 获取基本产品信息
							$where = array('product_basic_id' => $res_gpr[0]['gprelation_product_basic_id']);
							$res_pb = $this->pb->GetInfo($where);
							$res_data[$k]['product_basic_name'] = $res_pb[0]['product_basic_name']; // 基础产品名称
							$res_data[$k]['product_basic_code'] = $res_pb[0]['product_basic_code']; // 基础产品编码
							$res_data[$k]['product_rate'] = $res_gpr[0]['gprelation_rate']; // 比例
							$res_data[$k]['group_price'] = $res_bgr[0]['bgrelation_group_price'];
						}
					}
				}
			}
		} elseif (isset($data['goods_type']) && $data['goods_type'] == '1002') {
			// 处理标准商品
			$where = array('gprelation_goods_id' => $data['goods_id']);
			$res_gpr = $this->gpr->GetInfo($where); //返回商品内所有产品
			if ($res_gpr) {
				if (count($res_gpr) > 1) { //多个基础产品
					foreach ($res_gpr as $kk => $vv) {
						// 获取基本产品信息
						$where = array('product_basic_id' => $vv['gprelation_product_basic_id']);
						$res_pb = $this->pb->GetInfo($where);
						$res_data[$kk]['product_basic_name'] = $res_pb[0]['product_basic_name']; // 基础产品名称
						$res_data[$kk]['product_basic_code'] = $res_pb[0]['product_basic_code']; // 基础产品编码
						$res_data[$kk]['product_rate'] = $res_gpr[$kk]['gprelation_rate']; // 比例
					}
				} else { // 单一基础产品
					// 获取基本产品信息
					$where = array('product_basic_id' => $res_gpr[0]['gprelation_product_basic_id']);
					$res_pb = $this->pb->GetInfo($where);
					$res_data[0]['product_basic_name'] = $res_pb[0]['product_basic_name']; // 基础产品名称
					$res_data[0]['product_basic_code'] = $res_pb[0]['product_basic_code']; // 基础产品编码
					$res_data[0]['product_rate'] = $res_gpr[0]['gprelation_rate']; // 比例
				}
			}
		}
		return $res_data;
	}

	private function saveOrderDetail($data, $res_order_id) {
//        p($data);
//        echo '<hr>';
		$arr = $data['product_arr'];
		foreach ($arr as $k => $v) {
			$pd['order_d_goods_code'] = $data['goods_code'];
			$pd['order_d_goods_name'] = $data['goods_name'];
			$pd['order_d_product_basic_name'] = $v['product_basic_name'];
			$pd['order_d_product_basic_code'] = $v['product_basic_code'];
			$pd['order_d_order_id'] = $res_order_id;
			$pd['order_d_goods_primecost'] = $data['goods_primecost']; // 商品原价
			$pd['order_d_goods_disc'] = $data['goods_disc']; // 商品折后价
			$pd['order_d_product_basic_primecost'] = $data['goods_primecost'] / ($v['product_rate'] / 100); // 产品原价
			$pd['order_d_product_basic_disc'] = $data['goods_disc'] / ($v['product_rate'] / 100); // 产品折后价
			$pd['order_d_product_rate '] = $v['product_rate']; // 比例
			$this->db->insert('tc_order_d', $pd);
		}
	}

	public function recordSelect($where) { // 不做字段类型处理，直接获取数据内容
		$res = $this->db->get_where('tc_order', $where)->row_array();
		if ($res)
			return $res;
		else
			return false;
	}

	public function recordInsert($data) { // 不做字段类型处理，直接插入数据内容
		$res = $this->db->insert('tc_order', $data);
		if ($res)
			return $res;
		else
			return false;
	}

	public function whereGetInfo($where) {
		if (!empty($where)) {
			$res = $this->db->get_where('tc_order', $where)->row_array();
			if ($res) {
				return $res;
			} else
				return false;
		}return false;
	}
	
	private function selectOne($id) {
		$where = array(
				"order_id"	=>	$id
		);
		return $this->db->from("tc_order")->where($where)->get()->result_array();
	}

	//根据规范条件查询
	public function NewWhereListGetInfo($where) {
		//$res = $this->db->get_where('tc_order', $new_where)->row_array();
		//p($where);
		echo $sql = $this->QueryConditions($where['where_all'], $where['rel_all'], $where['obj']);
		//echo $sql_where;exit;
		//$sql = "select * from tc_order where ".$sql_where;
		exit;
		return false;
	}

	//用于组合查询条件
	public function QueryConditions($arr, $rel, $obj) {
		//$arr = json_decode(str_replace("#",".",json_encode($arr)));//将这个数组转化成JSON替换掉所有的#号改为.再转换回数组
		//p($arr);exit;
		foreach ($arr as $key => $value) {
			if ($value['attr'] == "type_id") {
				$rel = str_replace($key, "`" . $value['attr'] . "`" . $value['action'] . "'" . $value['value'] . "'", $rel);
			} else {
				//这里需要根据#来做判断条件
				$rel = str_replace($key, "`" . $obj . "_" . $value['attr'] . "`" . $value['action'] . "'" . $value['value'] . "'", $rel);
			}
		}
		return "select * from tc_" . $obj . " where " . $rel;
	}

	public function info($data) {
		error_reporting(E_ALL & ~E_NOTICE);
		$this->load->model('admin/enum_model', 'enum');
		$this->load->model('www/user_model', 'user');
		$this->load->model('www/department_model', 'department');
		$this->load->model('www/account_model', 'account');
		$this->load->model('www/partner_model', 'partner');
		//PC::debug($data);
		foreach ($data as $k => $v) {
			//p($v);die;
			
			//所有属性循环
			if ($v['order_create_user_id'] != "" and $v['order_create_user_id'] != 0) {
				$data[$k]['order_create_user_id_arr'] = $this->user->id_aGetInfo($v['order_create_user_id']);
			}
			
			if ($v['order_department'] != "" and $v['order_department'] != 0) {
				$data[$k]['order_department_arr'] = $this->department->id_aGetInfo($v['order_department']);
			}
			if ($v['order_owner'] != "" and $v['order_owner'] != 0) {
				$data[$k]['order_owner_arr'] = $this->user->id_aGetInfo($v['order_owner']);
			}
			if ($v['order_account'] != "" and $v['order_account'] != 0) {
				$data[$k]['order_account_arr'] = $this->account->id_aGetInfo($v['order_account']);
			}
			if ($v['order_agent'] != "" and $v['order_agent'] != 0) {
				$data[$k]['order_agent_arr'] = $this->partner->id_aGetInfo($v['order_agent']);
			}
			if ($v['order_finance'] != "" and $v['order_finance'] != 0) {
				$data[$k]['order_finance_arr'] = $this->user->id_aGetInfo($v['order_finance']);
			}
			if ($v['order_review'] != "" and $v['order_review'] != 0) {
				$data[$k]['order_review_arr'] = $this->user->id_aGetInfo($v['order_review']);
			}
			if ($v['order_nreview'] != "" and $v['order_nreview'] != 0) {
				$data[$k]['order_nreview_arr'] = $this->user->id_aGetInfo($v['order_nreview']);
			}
			
			//当属性类型为单选时
			if ($v['order_if_renew'] != "" and $v['order_if_renew'] != 0) {
				$data[$k]['order_if_renew_arr'] = $this->enum->getlist(227, $data[$k]['order_if_renew']);
			}
			
			//所属子公司 2014-08-25
			if ($v['order_subsidiary'] != "" and $v['order_subsidiary'] != 0) {
				$data[$k]['order_subsidiary_arr'] = $this->enum->getlist(627, $data[$k]['order_subsidiary']);
			}
			//所属子公司 2014-08-25
			
			if ($v['order_is_cancel'] != "" and $v['order_is_cancel'] != 0) {
				$data[$k]['order_is_cancel_arr'] = $this->enum->getlist(466, $data[$k]['order_is_cancel']);
			}
			
			//查询反关联作废订单数据 2014-09-02
			if ($v['order_id']!="" and $v['order_id']!=0) {
				$data[$k]['order_cancel_order_arr'] = $this->order_cancel_orderId($v['order_id']);
			}
			//查询反关联作废订单数据 2014-09-02
			
			//内划状态
			if ($v['order_transfer_state'] != "" and $v['order_transfer_state'] != 0) {
				$data[$k]['order_transfer_state_arr'] = $this->enum->getlist(473, $data[$k]['order_transfer_state']);
			}
			if ($v['order_change_into'] != "" and $v['order_change_into'] != 0) {
				$this->load->model('www/user_model', 'user');
				$data[$k]['order_change_into_arr'] = $this->user->id_aGetInfo($v['order_change_into']);
			}
			if ($v['order_change_out'] != "" and $v['order_change_out'] != 0) {
				$this->load->model('www/user_model', 'user');
				$data[$k]['order_change_out_arr'] = $this->user->id_aGetInfo($v['order_change_out']);
			}
			if ($v['order_out_examine'] != "" and $v['order_out_examine'] != 0) {
				$this->load->model('www/user_model', 'user');
				$data[$k]['order_out_examine_arr'] = $this->user->id_aGetInfo($v['order_out_examine']);
			}
			if ($v['order_relation_order_all'] != "" and $v['order_relation_order_all'] != 0) {
				if (strpos($v['order_relation_order_all'], ', ')) {
					$v['order_relation_order_all'] = explode(', ', $v['order_relation_order_all']);
					foreach ($v['order_relation_order_all'] as $vv) {
						$data[$k]['order_relation_order_all_arr'][] = $this->id_aGetInfo($vv['order_relation_order_all']);
					}
				} else {
					$data[$k]['order_relation_order_all_arr'][] = $this->id_aGetInfo($v['order_relation_order_all']);
				}
			}

			if ($v['order_rmb_user'] != "" and $v['order_rmb_user'] != 0) {
				$data[$k]['order_rmb_user_arr'] = $this->user->id_aGetInfo($v['order_rmb_user']);
			}
			if ($v['order_rebates_type'] != "" and $v['order_rebates_type'] != 0) {
				$data[$k]['order_rebates_type'] = $this->enum->getlist(510, $data[$k]['order_rebates_type']);
			}
			if ($v['order_rebates_state'] != "" and $v['order_rebates_state'] != 0) {
				$data[$k]['order_rebates_state'] = $this->enum->getlist(512, $data[$k]['order_rebates_state']);
			}
			if ($v['order_deposit_paytype'] != "" and $v['order_deposit_paytype'] != 0) {
				$data[$k]['order_deposit_paytype_arr'] = $this->enum->getlist(516, $data[$k]['order_deposit_paytype']);
			}

			if ($v['order_source'] != "" and $v['order_source'] != 0) {
				$data[$k]['order_source_arr'] = $this->enum->getlist(517, $data[$k]['order_source']);
			}

			if ($v['order_typein'] != "" and $v['order_typein'] != 0) {
				$data[$k]['order_typein_arr'] = $this->user->id_aGetInfo($v['order_typein']);
			}
			
			

			//关联销售订单（这里只能用简单查询，防止无线循环）
			if ($v['order_relation_order_id'] != "" and $v['order_relation_order_id'] != 0) {
				$data[$k]['order_relation_order_id_arr'] = $this->selectOne($v['order_relation_order_id']);
			}
			
			//变更订单查询
			if ($v['order_cancel_order_id'] != "" and $v['order_cancel_order_id'] != 0) {
				$data[$k]['order_cancel_order_id_arr'] = $this->selectOne($v['order_cancel_order_id']);
			}
 
			//查询出这个订单相关的明细
			if ($v['type_id'] == 6 or $v['type_id'] == 7 or $v['type_id'] == 13 or $v['type_id'] == 10 or $v['type_id'] == 14) { //如果是销售订单
				$order_id = $v['order_id'];
				$this->load->model('www/order_d_model', 'order_d');
				$where = array(
					'order_d_order_id' => $order_id
				);
				$data[$k]['detailed']['order_d_arr'] = $this->order_d->listGetInfo($where);
				//将里面的产品根据商品归类
				foreach ($data[$k]['detailed']['order_d_arr'] as $oda_k => $oda_v) {
					//汇总折后价
					if (!isset($sp_zhj[$oda_v['order_d_list']])) {
						$sp_zhj[$oda_v['order_d_list']] = 0;
					}
					$sp_zhj[$oda_v['order_d_list']] = $sp_zhj[$oda_v['order_d_list']] + $oda_v['order_d_product_basic_disc'];
					$data[$k]['detailed']['order_d_goods_arr'][$oda_v['order_d_list']]['order_d_product_basic_disc'] = $sp_zhj[$oda_v['order_d_list']];

					//汇总原价
					if (!isset($sp_yj[$oda_v['order_d_list']])) {
						$sp_yj[$oda_v['order_d_list']] = 0;
					}
					$sp_yj[$oda_v['order_d_list']] = $sp_yj[$oda_v['order_d_list']] + $oda_v['order_d_product_basic_primecost'];
					$data[$k]['detailed']['order_d_goods_arr'][$oda_v['order_d_list']]['order_d_product_basic_primecost'] = $sp_yj[$oda_v['order_d_list']];

					//商品名称
					$data[$k]['detailed']['order_d_goods_arr'][$oda_v['order_d_list']]['order_d_goods_name'] = $oda_v['order_d_goods_name'];

					//商品编号
					$data[$k]['detailed']['order_d_goods_arr'][$oda_v['order_d_list']]['order_d_goods_code'] = $oda_v['order_d_goods_code'];
				}
				//汇总一下已到帐金额
				$this->load->model('www/salespay_model', 'salespay');
				$this->salespay->db->query('SET NAMES UTF8');
				$where = array(
					'salespay_order_id' => $order_id,
					'salespay_status' => 1002,
				);
				$data[$k]['ydz_amount'] = $this->salespay->SumGetInfo('salespay_pay_amount', $where); //已到帐
			}


			//查询出这个订单关联的首次入款项
			if ($v['type_id'] == 10) { //如果是预收款订单才处理
				$order_id = $v['order_id'];
				//p($order_id);
				//exit;
				$this->load->model('www/salespay_model', 'salespay');
				$where = array(
					'salespay_order_id' => $order_id,
				);
				//p($where);exit;
				$data[$k]['detailed']['salespay_arr'] = $this->salespay->GetInfo($where);
			}
			
		}
		return $data;
	}
	
	/**
	* 查询反关联作废订单项
	* @params 	$cancel_orderId 	关联订单ID
	* @return 	array 			返回数组
	*/
	private function order_cancel_orderId($cancel_orderId) {
		$where = array(
			'order_cancel_order_id'  => $cancel_orderId,
		);
		return $this->db->select('order_id,order_number')->where($where)->get('tc_order')->row_array();
	}

	public function orderRecon($orders_id, $sp_date = '') {
		$result = array();
		if (strpos($orders_id, ',')) {
			$orders_id = explode(',', $orders_id);
			$result['num'] = count($orders_id);
		} else
			$result['num'] = '1';

		$res_salespay = $this->db->select('salespay_id, salespay_order_id, salespay_alipay_order')->where_in('salespay_order_id', $orders_id)->get('tc_salespay')->result_array();
		if (!$res_salespay) {
			$result['error'] = '没有关联关系，请检查订单';
			return $result;
		}
		foreach ($res_salespay as $k => $v) {
			if (!empty($v['salespay_order_id']) && !empty($v['salespay_alipay_order'])) {
				$order = $this->id_aGetInfo($v['salespay_order_id']);
				//sky 2014-10-29 增加日记账关联 原因：曾经关联过的订单还可以再关联一次 start
				$where = array(
						'books_trade_no' => $v['salespay_alipay_order'],
						'books_order_no' => NULL,
				);
				//sky 2014-10-29 增加日记账关联 原因：曾经关联过的订单还可以再关联一次 start
				$res_books = $this->db->select('books_id')->where($where)->get('tc_books')->row_array();
				if ($res_books) {
					$where = array('order_id' => $v['salespay_order_id']);
					$this->db->where($where);
					$res_u = $this->db->update('tc_order', array('type_id' => 6));
					
					//确认到账时间
					$sp_date = empty($sp_date) ? date("Y-m-d H:i:s", time()) : $sp_date;
					$post['salespay'] = array(
						'salespay_id' => $v['salespay_id'],
						'salespay_order_id' => $v['salespay_order_id'],
						'salespay_book_id' => $res_books['books_id'],
						'salespay_sp_date' => $sp_date,
						'salespay_sp_real_date' => $sp_date,
						'salespay_status' => 1002,
					);
					$res_s = $this->ajax_update_post($post);
					
					//修复漏洞 批量对账无法更新财务人员
					$books_data = array(
						'books_department_id' => $order['order_department'],
						'books_order_no' => $order['order_number'],
						'books_updatetime' => date("Y-m-d H:i:s", time()),
						'books_bdate' => $sp_date,
						'books_accounting' => $order['order_finance'],
					);
					//修复漏洞 批量对账无法更新财务人员
					$this->books->update($books_data, $res_books['books_id']);
					if ($res_u && $res_s) {
						$result['sure'][] = $order['order_number']; //关联成功
					} else
						$result['error']['rel'][] = $order['order_number']; //关联失败
				} else {
					$result['error']['books'][] = $order['order_number']; //没有关联日记账
				}
				
			} else
				$result['error']['def'][] = $v['salespay_id']; //出入账没有关联订单和交易号
		}
		return $result;
	}

	//用ID单个查询(简单版-为了防止无限循环)
	public function id_aGetInfo_simple($id) {
		$this->db->from('tc_order');
		$where = array(
			'order_id' => $id
		);
		$this->db->where($where);
		$data = $this->db->get()->result_array();
		//虽然是简单查询，还是需要循环查询点东西的
		$data = $this->info_simple($data);
		if (isset($data[0])) {
			$data = $data[0];
		}
		return $data;
	}

	public function info_simple($data) {
		$this->load->model('www/account_model', 'account');
		foreach ($data as $k => $v) {
			//订单的所属客户信息
			if ($v['order_account'] != "" and $v['order_account'] != 0) {
				$data[$k]['order_account_arr'] = $this->account->id_aGetInfo_simple($v['order_account']);
			}
		}
		return $data;
	}

	public function api_update($data, $where) {
		$update_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('order');
		$object = $objDetail['obj'];

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $object . '_' . $fields;
				if (!$this->db->field_exists($this_fields, 'tc_' . $object)) {
					return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
				}

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$update_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$update_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $object . '_' . $k;
				if (!$this->db->field_exists($this_fields, 'tc_' . $object)) {
					return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
				}

				//内容是数组，将内容组成 json
				if (is_array($v)) {
//					$json = '';
//					$update_data[$this_fields] = json_encode($json);
				} else
					$update_data[$this_fields] = $v;
			}
		}
		$res = $this->db->update('tc_' . $object, $update_data, $where);
		if ($res) {
			return array('res' => 'succ', 'msg' => 'update success');
		} else
			return array('res' => 'fail', 'msg' => 'update failure');
	}

	//ajax调用的编辑
	private function ajax_update_post($post) {
		$this->load->model('www/salespay_model', 'salespay');
		$this->load->model('www/message_model', 'message');
		$this->load->model('www/system_log_model', 'system_log');
		
		if ($post) {
			if (!$this->session->userdata('user_id'))
				die(', 请重新登陆'); //2014-6-17 add
			$salespay_data = $post['salespay'];
			if (isset($post['payinfo'])) {
				$salespay_data['salespay_pay_info'] = json_encode($post['payinfo']);
			}
			if (!isset($salespay_data['salespay_status'])) {
				$salespay_data['salespay_status'] = 1001;
			}
			//这里添加个回传渠道接口状态接口
			//p($salespay_data);exit;
//cus lee 2014-04-23 同步渠道 start
			//首先要判断出是不是渠道打过来的订单
			$salespay_sql_data = $this->salespay->id_aGetInfo($salespay_data['salespay_id']);
			if ($salespay_sql_data['salespay_order_id_arr']['order_source'] == 1003) { //当来源为【渠道】的订单
				//调用打渠道的接口
				$params['action'] = 'ForeDepositApply'; //预存款和销售订单的审核
				//p($salespay_sql_data['salespay_order_id_arr']['type_id']);//exit;
				if ($salespay_sql_data['salespay_order_id_arr']['type_id'] == 8) {//返点订单打这个
					$params['action'] = 'SalesReturnMoneyOrderApply';
				}
				$params['id'] = $salespay_sql_data['salespay_number']; //传给营收的工单编码

				if ($salespay_data['salespay_status'] == 1002) { //当选择确认款项的时候
					$salespay_data['salespay_create_time'] = date("Y-m-d H:i:s");
					$params['result'] = 'ok'; //审核成功
					$params['remark'] = 'XXXXXXX'; //审核备注
					$params['time'] = date("Y-m-d H:i:s"); //审核时间 (用当前时间)
					$res = $this->qudao_api($params);
					if ($res['res'] == 'fail') {
						//echo $res['msg'];exit;
					} else {
						//p($res);
					}
				} else if ($salespay_data['salespay_status'] == 1003) { //当选择款项不成功的时候
					$params['result'] = 'failure'; //审核失败
					$params['remark'] = 'XXXXXXX'; //审核备注
					$params['time'] = date("Y-m-d H:i:s"); //审核时间 (用当前时间)
					$res = $this->qudao_api($params);
					if ($res['res'] == 'fail') {
						//echo $res['msg'];exit;
					} else {
						//p($res);
						//p($params);
					}
				}
			}
//cus lee 2014-04-23 同步渠道 end
//			p($salespay_data);die;

			$this->salespay->update($salespay_data, $salespay_data['salespay_id']);
			$order_data = $this->id_aGetInfo($salespay_data['salespay_order_id']);
//			exit;
			if ($salespay_data['salespay_status'] == 1002) {
				$log_data = array(
					'system_log_addtime' => date('Y-m-d H:i:s', time()),
					'system_log_user_id' => $this->session->userdata('user_id'),
					'system_log_operation' => '确认到账',
					'system_log_module' => 'Order',
					'system_log_module_id' => $salespay_data['salespay_order_id']
				);
				$message_data = array(
					'message_status' => 1002
				);
				$where1 = array('message_module_id' => $salespay_data['salespay_id']);
				$this->message->update($message_data, $where1);
				$this->load->model('www/books_model', 'books');
				$this->books->update(array('books_state' => 1002), $salespay_data['salespay_book_id']);
			} else if ($salespay_data['salespay_status'] == 1001) {
				$log_data = array(
					'system_log_addtime' => date('Y-m-d H:i:s', time()),
					'system_log_user_id' => $this->session->userdata('user_id'),
					'system_log_operation' => '编辑入款项',
					'system_log_module' => 'Order',
					'system_log_module_id' => $salespay_data['salespay_order_id'],
					'system_log_note' => $salespay_data['salespay_pay_note']
				);
				$message1_data = array(
					'message_status' => 1002
				);

				$where1 = array('message_module_id' => $salespay_data['salespay_id'], 'message_type' => 1010);
				$this->message->update($message1_data, $where1);
				$message_data = array(
					'message_owner' => $order_data['order_finance'],
					'message_module' => 'salespay',
					'message_module_id' => $salespay_data['salespay_id'],
					'message_type' => 1001,
					'message_department' => $order_data['order_finance_arr']['user_department'],
					'message_url' => 'www/salespay/ajax_confirm_salespay'
				);
				$this->message->add($message_data);
			} else {
				$log_data = array(
					'system_log_addtime' => date('Y-m-d H:i:s', time()),
					'system_log_user_id' => $this->session->userdata('user_id'),
					'system_log_operation' => '到账不成功',
					'system_log_module' => 'Order',
					'system_log_module_id' => $salespay_data['salespay_order_id']
				);
				$message1_data = array(
					'message_status' => 1002
				);
				$where1 = array('message_module_id' => $salespay_data['salespay_id'], 'message_type' => 1001);
				$this->message->update($message1_data, $where1);
				$message_data = array(
					'message_owner' => $order_data['order_owner'],
					'message_module' => 'salespay',
					'message_module_id' => $salespay_data['salespay_id'],
					'message_type' => 1010,
					'message_department' => $order_data['order_owner_arr']['user_department'],
					'message_url' => 'www/salespay/ajax_edit'
				);
				$this->message->add($message_data);
			}

			$this->system_log->add($log_data);
			return true;
		} else {
			return false;
		}
	}

	private function checkAgreement($agreement_no = '') {
		return $this->db->get_where('tc_order', array('order_agreement_no' => $agreement_no))->row_array();
	}

	// 获取附件
	public function getAdjunct($data) {
		$this->db->get_where('tc_adjunct', array('adjunct_order_id' => $data['order_id']))->result_array();
	}

	//2014-07-17 api start
	/**
	 * 接口 _addCheck 检查
	 * @param type $data 数据
	 * @param type $user 创建者id
	 * @return type array
	 */
	public function _addCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		//基础数据验证S
		$res_data = $this->_basicCheck($data);
		if (!isset($res_data['res']) || $res_data['res'] != 'succ')
			return $res_data;
		$data = $res_data['info'];
		if (!isset($data['create_time']))
			$data['order_create_time'] = date("Y-m-d H:i:s", time());

		if (!isset($data['create_user_id'])) {
			if (isset($user)) {
				$res = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $user))->row_array();
				$data['order_create_user_id'] = $res['user_id'];
			} else
				return $this->api->_msg('订单不能没有创建人');
		}
		//获取销售相应部门（合同只打销售工号）
		if (isset($data['order_owner']) && !isset($data['order_department'])) {
			$res_department = $this->db->select('user_department')->get_where('tc_user', array('user_id' => $data['order_owner']))->row_array();
			if ($res_department)
				$data['order_department'] = $res_department['user_department'];
		}
		//基础数据验证E
		//针对不同订单进行验证S
		$number = isset($data['order_number']) ? $data['order_number'] : $this->getNumber($data['type_id']);
		switch ($data['type_id']) {
			case '6': //销售订单
				$data['order_number'] = 'so' . $number;
				return $this->_saleOrder($data, $user);
				break;
			case '7': //线上订单
				$data['order_number'] = 'oo' . $number;
				return $this->_onlinesOrder($data);
				break;
			case '8': //返点订单
				$data['order_number'] = 'ro' . $number;
				return $this->_rebatesOrder($data);
				break;
			case '9': //内部划款订单
				$data['order_number'] = 'po' . $number;
				return $this->_pennyOrder($data);
				break;
			case '10': //预收款订单
				$data['order_number'] = 'do' . $number;
				return $this->_depositOrder($data);
				break;
			case '13': //提货销售订单
				$data['order_number'] = 'lo' . $number;
				return $this->_deliveryOrder($data);
				break;
			case '14': //续费订单
				$data['order_number'] = 're' . $number;
				return $this->_renewOrder($data);
				break;
			default:
				return $this->api->_msg('订单异常');
				break;
		}
		//针对不同订单进行验证E
		return $this->api->_msg('addCheck error');
	}

	public function _updateCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		$res_data = $this->_basicCheck($data);
		if (!isset($res_data['res']) || $res_data['res'] != 'succ')
			return $res_data;
		$data = $res_data['info'];
		//检查订单编号，根据前2个字符匹配类型，如果没有增加类型字符
		if (isset($data['order_number'])) {
			$type_start = substr($data['order_number'], 0, 2);
			$order_type = array('6' => 'so', '7' => 'oo', '8' => 'ro', '9' => 'po', '10' => 'do', '13' => 'lo');
			foreach ($order_type as $v) {
				if ($v != $type_start) {
					$data['order_number'] = $order_type[$data['type_id']] . $data['order_number'];
					break;
				}
			}
		}
		return $this->api->_return('_updateCheck succ', $data, 'succ');
	}

	/**
	 * 订单基本数据过滤
	 * @param array $data
	 * @return array
	 */
	private function _basicCheck($data = '') {
		if (empty($data))
			return $this->api->_return('无效订单数据');
		if (!isset($data['type.name']) and ! isset($data['type_id'])) {
			return $this->api->_return('没有订单类型');
		}
		if (isset($data['type.name'])) {
			$res = $this->db->select('type_id')->get_where('dd_type', array('type_name' => $data['type.name']))->row_array();
			if (!$res)
				return $this->api->_return('没有该类型订单');
			else {
				$data['type_id'] = $res['type_id'];
				unset($data['type.name']);
			}
		}
		return $this->api->_return(__class__ . '_basicCheck succ', $data, 'succ');
	}

	//销售订单
	private function _saleOrder($data, $user) {
		if ($this->checkNumber($data['order_number']))
			return $this->api->_return('订单编号已存在');
		if($user == 'htSync'){ //从合同平台过来的订单必须有客户和合同编号
			if (isset($data['order_agreement_no']) && $this->checkAgreement($data['order_agreement_no']))
				return $this->api->_return('合同编号已存在');
			if (!isset($data['order_account']) or empty($data['order_account']))
				return $this->api->_return('客户不能为空');
		}

		//获取销售相应部门（合同只打销售工号）
		if (!isset($data['order_nreview']) && $data['order_create_user_id']) {
			// 根据创建人筛选订单
			$res_cid = $this->db->select('order_nreview')->from('tc_order')->where(array('order_create_user_id' => $data['order_create_user_id'], 'type_id' => 6))->order_by('order_id', 'desc')->get()->row_array();
			$data['order_nreview'] = (isset($res_cid['order_nreview']) && $res_cid['order_nreview'] == '2109') ? '2108' : '2109';
		}	
		return $this->api->_return('saleOrder succ', $data, 'succ');
	}

	//线上订单
	private function _onlinesOrder($data) {
		if ($this->checkNumber($data['order_number']))
			return $this->api->_return('订单编号已存在');
		//up liiasheng 2014-07-24 17:26 更新客户不能为空 start
		if (!isset($data['order_account']) or empty($data['order_account']))
			return $this->api->_return('客户不能为空');
		//up liiasheng end
		//获取销售相应部门（合同只打销售工号）
		if (!isset($data['order_nreview']) && $data['order_create_user_id']) {
			// 根据创建人筛选订单
			$res_cid = $this->db->select('order_nreview')->from('tc_order')->where(array('order_create_user_id' => $data['order_create_user_id'], 'type_id' => 7))->order_by('order_id', 'desc')->get()->row_array();
			if (isset($res_cid['order_nreview']) && $res_cid['order_nreview'] == '2109') {
				$data['order_nreview'] = '2108';
			} else {
				$data['order_nreview'] = '2109';
			}
		}
		// 1000017 短信同步账号
		if ($data['order_create_user_id'] == '1000017') {
			$data['order_typein'] = $data['order_owner'];
		}
		return $this->api->_return('onlinesOrder succ', $data, 'succ');
	}

	//返点订单
	private function _rebatesOrder($data) {
		if ($this->checkNumber($data['order_number']))
			return $this->api->_return('订单编号已存在');
		return $this->api->_return('rebatesOrder succ', $data, 'succ');
	}

	//内部划款订单
	private function _pennyOrder($data) {
		if ($this->checkNumber($data['order_number']))
			return $this->api->_return('订单编号已存在');
		if ($data['order_change_out']) {
			$this->load->model('www/user_mode', 'user');
			$res_u = $this->user->id_aGetInfo($data['order_change_out']);
			if ($res_u)
				$data['order_department'] = $res_u['user_department'];
		}
		return $this->api->_return('pennyOrder succ', $data, 'succ');
	}

	//预收款订单
	private function _depositOrder($data) {
		if ($this->checkNumber($data['order_number']))
			return $this->api->_return('订单编号已存在');
		//销售人员：s0194  业务主管：s0737  财务人员：s1861  部门：跟销售人员的部门
		//$userArr = array('s0194', 's0737', 's1861');
		$userArr = array('s0194', 's0737', 's0674'); //财务戴汉君转岗换成司文欣
		$users = $this->db->from('tc_user')->where_in('user_gonghao', $userArr)->get()->result_array();
		foreach ($users as $user) {
			$resUser[$user['user_gonghao']]['id'] = $user['user_id'];
			$resUser[$user['user_gonghao']]['department'] = $user['user_department'];
		}

		if (!isset($data['order_owner']))
			$data['order_owner'] = $resUser['s0194']['id']; //销售
		if (!isset($data['order_review']))
			$data['order_review'] = $resUser['s0737']['id']; //业务主管
		if (!isset($data['order_finance']))
			//$data['order_finance'] = $resUser['s1861']['id']; //财务人员
			$data['order_finance'] = $resUser['s0674']['id']; //财务人员 //财务戴汉君转岗换成司文欣
		if (!isset($data['order_department']))
			$data['order_department'] = $resUser['s0194']['department']; //部门
		//新增所属子公司判断如果不为或不存在则变更为商派网络1001
		if ( !isset($data['order_subsidiary']) || empty($data['order_subsidiary']) ) {
			$data['order_subsidiary'] = "1001";
		}
		//新增所属子公司判断如果不为或不存在则变更为商派网络1001	
		return $this->api->_return('depositOrder succ', $data, 'succ');
	}

	//提货销售订单
	private function _deliveryOrder($data) {
		if ($this->checkNumber($data['order_number']))
			return $this->api->_return('订单编号已存在');
		//获取销售相应部门（合同只打销售工号）
		if (!isset($data['order_nreview'])) {
			// 根据创建人筛选订单
			$res_cid = $this->db->select('order_nreview')->from('tc_order')->where(array('type_id' => 13))->order_by('order_id', 'desc')->get()->row_array();
			if (isset($res_cid['order_nreview']) && $res_cid['order_nreview'] == '2109') {
				$data['order_nreview'] = '2108';
			} else {
				$data['order_nreview'] = '2109';
			}
		}
		return $this->api->_return('deliveryOrder succ', $data, 'succ');
	}

	//2014-07-17 api end
	
	//续费订单
	private function _renewOrder($data) {
		if ($this->checkNumber($data['order_number']))
			return $this->api->_return('订单编号已存在');
		if (!isset($data['order_account']) or empty($data['order_account']))
			return $this->api->_return('客户不能为空');
		return $this->api->_return('deliveryOrder succ', $data, 'succ');
	}
	
	
	/**
	* 例:
	* SELECT * FROM tc_order WHERE order_number='oo201404300212044' 订单表
	* SELECT * FROM tc_order_d WHERE order_d_id='650' 				订单详细表
	* SELECT * FROM tc_salespay WHERE salespay_order_id = '650'		入款项表
	* SELECT * FROM tc_invoice WHERE invoice_order_id='650'			开票表
	* SELECT * FROM tc_books  #attn_id department_id order_no		日记项数据还原
	* @外部接口调用DEL命令
	* @params $data 传入的要处理的数据
	* @return bool true|false
	*/
	public function api_del( $data ) {
		$this->db->query('SET NAMES UTF8');
		//加载要用到的MODEL层
		foreach ( $this->getArray() as $key => $value) {
			$this->load->model( $value, $key );
		}
		//加载完毕
		
		$data = $this->paraseData( $data );//格式化后的数据用于IN
		$order_id = array();//初始化order_id数据
		//开始做数据删除
		$order_json_data = $this->listGetInfoIn( $data );
		//判断ORDER主表是否有数据,如果没有退出
		if (empty($order_json_data)) {
			return false;
		}
		//循环存储入ORDER_ID
		foreach ( $order_json_data as $k => $v ) {
			$order_id[$k] = $v["order_id"];
		}
		
		/**********************START数据存储开始***************************/
		/********************ORDER*****************************/
		//将ORDER数据转化成JSON数据存储到文件中
		$order_json_data = json_encode($order_json_data);
		//将ORDER数据压入文件中
		$this->file_put($order_json_data);
		/********************ORDER_D*****************************/
		//将ORDER_d数据转化成JSON数据存储到文件中
		$order_d_json_data = json_encode($this->order_d->listGetInfoIn( $order_id ));
		//将ORDER_d数据压入文件中
		$this->file_put($order_d_json_data);
		/*********************SALESPAY****************************/
		//将tc_salespay数据转化成JSON数据存储到文件中
		$salespay_json_data = json_encode($this->salespay->listGetInfoIn( $order_id ));
		//将tc_salespay数据压入文件中
		$this->file_put($salespay_json_data);
		/*********************INVOICE****************************/
		//将tc_invoice数据转化成JSON数据存储到文件中
		$invoice_json_data = json_encode($this->invoice->listGetInfoIn( $order_id ));
		//将tc_invoice数据压入文件中
		$this->file_put($invoice_json_data);
		/*********************BOOKS****************************/
		//将tc_books数据转化成JOSN数据存储到文件中
		$books_json_data = json_encode($this->books->listGetInfoIn( $data ));
		//将tc_books数据压入文件中
		$this->file_put($books_json_data);
		/**********************END数据存储完成***************************/
		/**
		* 开始执行删除方法
		* 对每个MODEL做一个删除方法以便后期对其的修改
		* 目前用到的MODEL为order order_d salespay invoice books
		*/
		$this->_deleteInfo( $data );
		$this->order_d->_deleteInfo( $order_id );
		$this->salespay->_deleteInfo( $order_id );
		$this->invoice->_deleteInfo( $order_id );
		$this->books->_upInfo( $data );
		
		//再次判断数据是否删除成功
		$order_json_data = $this->listGetInfoIn( $data );
		
		if (count($order_json_data)>0) {
			return false;
		} else {
			return true;
		}
		
	}
	
	/**
	* @例:数据删除
	* @params $data 待查询的数据条件
	* @retyrb 无
	*/
	private function _deleteInfo( $data ) {
		$this->db->where_in( "order_number", $data )->delete("tc_order");
	}
	
	/**
	* @例:数据查询
	* @params $data 待查询的数据条件
	* @params return array 返回的数据集结果
	*/
	private function listGetInfoIn( $data ) {
		return $this->db->from("tc_order")->where_in( "order_number", $data )->get()->result_array();
	}
	
	/**
	* @例：用IN进行数据查询
	* @params $data 进行查询的条件
	* @return array 返回查询的查果集
	*/
	public function SelectInWhere ($data) {
		extract($data,EXTR_OVERWRITE);
		return $this->db->select($fields)->from("tc_order")->where_in("order_id", $where)->get()->result_array();
	}
	
	/**
	* @初始化加载的MODEL
	*/
	private function getArray() {
		return array( "order" => "www/order_model", 
					  "order_d" => "www/order_d_model",
					  "salespay" => "www/salespay_model",
					  "invoice" => "www/invoice_model",
					  "books" => "www/books_model",);
	}
	
	/***
	* @例:生成日志文件
	* @params $json_data 输出文件的数据
	*/
	private function file_put( $json_data ) {
		$file_path = "syslog\\".date("YmdHis");
		$file_path .= "log.log";
		
		error_log($json_data."\r\n", 3, $file_path);
		
	}
	
	/***
	* @例:
	* oo201404300212044,oo201404300212044,oo201404300212044处理数据变为IN可用模式
	* @params $data 要处理的数据
	* @return string 要返回的IN模式数据
	*/
	private function paraseData( $data, $bool=FALSE ) {
		//格式化数据转化成IN模式
		$data = trim($data);
		$data = explode( ",", $data );
		if (!$bool) {
			return $data;
		}
		$str = "";//初始化STR用于存储返回的IN数据
		foreach ( $data as $key => $value ) {
			if (!empty($value)) {
				$str.="'".$value."',";
			}
		}
		return $this->sub_str($str);		
	}
	
	/**
	* @例:
	* 将数据最末的字符去掉
	* @params $str 要处理的数据
	* @params string 要返回的数据
	*/
	private function sub_str( $str ) {
		return substr($str,0,strrpos($str,","));
	}
}

?>
