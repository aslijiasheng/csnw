<?php

class Bgrelation_model extends CI_Model {

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'bgrelation_group_price',
			'bgrelation_goods_price_id',
			'bgrelation_goods_id',
			'bgrelation_bundle_id',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'bgrelation_group_price',
			'bgrelation_goods_price_id',
			'bgrelation_goods_id',
			'bgrelation_bundle_id',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
			'bgrelation_group_price',
			'bgrelation_goods_price_id',
			'bgrelation_goods_id',
			'bgrelation_bundle_id',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'bgrelation_bundle_id' => '组合ID（父商品ID）',
			'bgrelation_goods_id' => '被捆绑商品的ID（子ID）',
			'bgrelation_goods_price_id' => '商品价格ID',
			'bgrelation_group_price' => '组合价格',
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'bgrelation_id' => $id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data, $id) {
		$this->db->where('bgrelation_id', $id)->update('tc_bgrelation', $data);
	}

	//单个删除
	public function del($id) {
		$this->db->where('bgrelation_id', $id)->delete('tc_bgrelation');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_bgrelation', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_bgrelation');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_bgrelation');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容

		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环


			if ($v['bgrelation_bundle_id'] != "" and $v['bgrelation_bundle_id'] != 0) {
				$this->load->model('www/goods_model', 'goods');
				$data[$k]['bgrelation_bundle_id_arr'] = $this->goods->id_aGetInfo($v['bgrelation_bundle_id']);
			}








			if ($v['bgrelation_goods_id'] != "" and $v['bgrelation_goods_id'] != 0) {
				$this->load->model('www/goods_model', 'goods');
				$data[$k]['bgrelation_goods_id_arr'] = $this->goods->id_aGetInfo($v['bgrelation_goods_id']);
			}








			if ($v['bgrelation_goods_price_id'] != "" and $v['bgrelation_goods_price_id'] != 0) {
				$this->load->model('www/goods_price_model', 'goods_price');
				$data[$k]['bgrelation_goods_price_id_arr'] = $this->goods_price->id_aGetInfo($v['bgrelation_goods_price_id']);
			}
		}
		return $data;
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('bgrelation');
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $user['user_id'];
			unset($data['user']);
		}

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
					$json = $this->arrJson($v);
					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}

		if ($bgId = $this->db->select('bgrelation_id')->get_where('tc_bgrelation', array('bgrelation_bundle_id' => $save_data['bgrelation_bundle_id'], 'bgrelation_goods_id' => $save_data['bgrelation_goods_id']))->row_array()) {
//			return array('res' => 'succ', 'msg' => '组合商品', 'info' => array('goods_price_id' => $gpId['goods_price_id']));
			$this->db->update('tc_bgrelation', $save_data, array('bgrelation_id' => $bgId['bgrelation_id']));
			return array('res' => 'succ', 'msg' => '商品更新', 'info' => array('bgrelation_id' => $bgId['bgrelation_id']));
		}

		$res = $this->db->insert('tc_bgrelation', $save_data);
		if ($res) {
			return array('res' => 'succ', 'msg' => '');
		} else
			return array('res' => 'fail', 'msg' => 'error bgrelation save');
	}

	private function arrJson($v) {
		$json = '';
//		foreach ($v as $kk => $vv) {
//			if (strpos($kk, '.')) {
//				list($fields, $attr) = explode('.', $kk);
//				if ($fields == 'bank_type') {
//					$res_enum = $this->db->select('enum_key')->get_where('dd_enum', array('enum_name' => $vv, 'attr_id' => '560'))->row_array();
//					if (!$res_enum)
//						return array('res' => 'fail', 'msg' => 'error [' . $kk . ' : ' . $vv . ']');
//					$json[$fields] = $res_enum['enum_key'];
//				}
//			} else {
//				$json[$kk] = $vv;
//			}
//		}
		return $json;
	}

}

?>
