<?php

class Goods_model extends CI_Model {

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'goods_name',
			'goods_utime',
			'goods_type',
			'goods_check_pay',
			'goods_trail_days',
			'goods_code',
			'goods_is_new',
			'goods_visable',
			'goods_sale_type',
			'goods_is_trail',
			'goods_desc',
			'goods_is_sale',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'goods_code',
			'goods_name',
			'goods_type',
			'goods_visable',
			'goods_is_sale',
			'goods_is_trail',
			'goods_trail_days',
			'goods_sale_type',
			'goods_check_pay',
			'goods_utime',
			'goods_desc',
			'goods_is_new',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
			'goods_utime',
			'goods_is_new',
			'goods_check_pay',
			'goods_sale_type',
			'goods_trail_days',
			'goods_is_trail',
			'goods_is_sale',
			'goods_desc',
			'goods_visable',
			'goods_type',
			'goods_code',
			'goods_name',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'goods_code' => '商品编码',
			'goods_name' => '商品名称',
			'goods_type' => '商品类型',
			'goods_desc' => '商品简介',
			'goods_visable' => '是否启用',
			'goods_is_trail' => '是否可试用',
			'goods_is_sale' => '是否允许单独售卖',
			'goods_sale_type' => '售卖方分类',
			'goods_trail_days' => '试用天数（天）',
			'goods_check_pay' => '计费方式',
			'goods_is_new' => '新商品',
			'goods_utime' => '商品信息更新时间',
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'goods_id' => $id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data, $id) {
		$this->db->where('goods_id', $id)->update('tc_goods', $data);
	}

	//单个删除
	public function del($id) {
		$this->db->where('goods_id', $id)->delete('tc_goods');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_goods', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_goods');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_goods');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容

		$this->load->model('admin/enum_model', 'enum');
		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环
			//当属性类型为单选时
			if ($v['goods_type'] != "" and $v['goods_type'] != 0) {
				$data[$k]['goods_type_arr'] = $this->enum->getlist(402, $data[$k]['goods_type']);
			}
			if ($v['goods_visable'] != "" and $v['goods_visable'] != 0) {
				$data[$k]['goods_visable_arr'] = $this->enum->getlist(404, $data[$k]['goods_visable']);
			}
			if ($v['goods_is_trail'] != "" and $v['goods_is_trail'] != 0) {
				$data[$k]['goods_is_trail_arr'] = $this->enum->getlist(405, $data[$k]['goods_is_trail']);
			}
			if ($v['goods_is_sale'] != "" and $v['goods_is_sale'] != 0) {
				$data[$k]['goods_is_sale_arr'] = $this->enum->getlist(406, $data[$k]['goods_is_sale']);
			}
			if ($v['goods_sale_type'] != "" and $v['goods_sale_type'] != 0) {
				$data[$k]['goods_sale_type_arr'] = $this->enum->getLists(407, $data[$k]['goods_sale_type']);
			}
			if ($v['goods_check_pay'] != "" and $v['goods_check_pay'] != 0) {
				$data[$k]['goods_check_pay_arr'] = $this->enum->getlist(409, $data[$k]['goods_check_pay']);
			}
		}
		return $data;
	}

	// 抓取产品中心商品
	public function sync($starttime, $endtime) {
		$this->load->library('snoopy');
		$goods_arr = $this->getGoods($starttime, $endtime);
		p($goods_arr);
		exit;

		error_log(print_r(array('starttime' => $starttime, 'endtime' => $endtime, 'goods_arr' => $goods_arr), true) . "\n", 3, 'goods' . date("Ymd_h") . "log.log");
		if ($goods_arr) {
			foreach ($goods_arr as $gk => $gv) {
				$goods_data['goods_code'] = $gv['goods_code'];
				$goods_data['goods_name'] = $gv['goods_name'];
				$goods_data['goods_desc'] = $gv['goods_desc'];
				$goods_data['goods_is_new'] = $gv['goods_is_new'];
				$goods_data['goods_utime'] = date('Y-m-d H:i:s', $gv['goods_utime']);
				if (isset($gv['goods_trail_days']))
					$goods_data['goods_trail_days'] = $gv['goods_trail_days'];
				// 枚举类型 默认 1001:是  1002:否
				$goods_data['goods_visable'] = 1002 - $gv['goods_visable'];
				$goods_data['goods_is_sale'] = 1002 - $gv['goods_is_sale'];
				$goods_data['goods_type'] = 1002 - $gv['goods_type'];
				if (isset($gv['goods_is_trail']))
					$goods_data['goods_is_trail'] = 1002 - $gv['goods_is_trail'];
				if (isset($gv['goods_check_pay']))
					$goods_data['goods_check_pay'] = 1002 - $gv['goods_check_pay'];
				// 处理 售卖方分类
				$enum_sale_type = array('1901' => '1001', '1902' => '1002', '1903' => '1003', '1904' => '1004', '1905' => '1005', '1906' => '1006', '1907' => '1007', '1908' => '1008', '1909' => '1009');
				if (isset($gv['goods_sale_type'])) {
					if (strpos($gv['goods_sale_type'], ',')) {
						$gv['goods_sale_type'] = explode(',', $gv['goods_sale_type']);
						$goods_data['goods_sale_type'] = '';
						foreach ($gv['goods_sale_type'] as $v) {
							$goods_data['goods_sale_type'] .= ',' . $enum_sale_type[$v];
						}$goods_data['goods_sale_type'] = ltrim($goods_data['goods_sale_type'], ',');
					} else {
						$goods_data['goods_sale_type'] = $enum_sale_type[$gv['goods_sale_type']];
					}
				}
				// 添加/保存商品基本信息 start
				$res_goods = $this->db->get_where('tc_goods', array(
							'goods_code' => $goods_data['goods_code'],
							'goods_type' => $goods_data['goods_type']
						))->row_array();
				if ($res_goods) {
					$this->db->update('tc_goods', $goods_data, array('goods_id' => $res_goods['goods_id']));
					$res_goods_id = $res_goods['goods_id'];
				} else {
					$res_goods_id = $this->db->insert('tc_goods', $goods_data);
				}
				if (!$res_goods_id) {
					$res['line'] = __LINE__;
					return $res;
				}// 添加/保存商品基本信息 end
				// 标准商品 start
				if ($gv['goods_type'] == '0') {
					foreach ($gv['prosuite'] as $k => $v) {
						$gpr_data = array();
						// 检查 技术产品
						$res_tech = $this->db->get_where('tc_product_tech', array('product_tech_code' => $v['product_code']))->row_array();
						if (!$res_tech) {
							break;
							$res['line'] = __LINE__;
							return $res;
						}
						// 检查 基础产品
						$res_basic = $this->db->get_where('tc_product_basic', array('product_basic_code' => $v['prosuite_code']))->row_array();
						if (!$res_basic) {
							break;
							$res['line'] = __LINE__;
							return $res;
						}

						$gpr_data['gprelation_goods_id'] = $res_goods_id; // 当前商品id
						$gpr_data['gprelation_product_tech_id'] = $res_tech['product_tech_id']; // 技术产品id
						$gpr_data['gprelation_product_basic_id'] = $res_basic['product_basic_id']; // 基础产品id
						$gpr_data['gprelation_rate'] = $v['rate']; // 比例

						$res_gpr = $this->db->get_where('tc_gprelation', array(
									'gprelation_goods_id' => $gpr_data['gprelation_goods_id'],
									'gprelation_product_tech_id' => $gpr_data['gprelation_product_tech_id'],
									'gprelation_product_basic_id' => $gpr_data['gprelation_product_basic_id']
								))->row_array();
						if ($res_gpr) {
							$this->db->update('tc_gprelation', $gpr_data, array('gprelation_id' => $res_gpr['gprelation_id']));
							$res_gpr_id = $res_gpr['gprelation_id'];
						} else {
							$res_gpr_id = $this->db->insert('tc_gprelation', $gpr_data);
						}
						if (!$res_gpr_id) {
							$res['line'] = __LINE__;
							return $res;
						}
					}// 商品与产品关系 end
					// 保存价格信息 start
					foreach ($gv['gprice'] as $k => $v) {
						$gprice_data = array();
						$gprice_data['goods_price_goods_id '] = $res_goods_id;
						$gprice_data['goods_price_code'] = $v['gp_code'];
						$gprice_data['goods_price_visable'] = $v['gp_visable'];
						$gprice_data['goods_price_start_price'] = $v['gp_start_price'];
						$gprice_data['goods_price_cycle_days'] = $v['gp_cycle_days'];
						$enum_cycle_unit = array('1401' => '1001', '1402' => '1002', '1403' => '1003');
						$gprice_data['goods_price_cycle_unit'] = $enum_cycle_unit[$v['gp_cycle_unit']];
						$gprice_data['goods_price_cycle_price'] = $v['gp_cycle_price'];
						$gprice_data['goods_price_effective_days'] = $v['gp_effective_days'];

						$res_gprice = $this->db->get_where('tc_goods_price', array(
									'goods_price_code' => $gprice_data['goods_price_code']
								))->row_array();
						if ($res_gprice) {
							$this->db->update('tc_goods_price', $gprice_data, array('goods_price_id' => $res_gprice['goods_price_id']));
							$res_gprice_id = $res_gprice['goods_price_id'];
						} else {
							$res_gprice_id = $this->db->insert('tc_goods_price', $gprice_data);
						}
						if (!$res_gprice_id) {
							$res['line'] = __LINE__;
							return $res;
						}
					}// 标准商品 end
				} else {
					// return true;
					// 捆绑商品 start
					foreach ($gv['rel_goods'] as $k => $v) {
						$bgr_data = array();
						// 检查商品
						$res_goods = $this->db->get_where('tc_goods', array('goods_code' => $v['goods_code']))->row_array();
						if (!$res_goods) {
							$res['line'] = __LINE__;
							return $res;
						}
						// 检查商品价格
						$res_gprice = $this->db->get_where('tc_goods_price', array('goods_price_code' => $v['gp_code']))->row_array();
						if (!$res_gprice) {
							$res['line'] = __LINE__;
							return $res;
						}
						$bgr_data['bgrelation_bundle_id'] = $res_goods_id;
						$bgr_data['bgrelation_goods_id'] = $res_goods['goods_id'];
						$bgr_data['bgrelation_goods_price_id'] = $res_gprice['goods_price_id'];
						$bgr_data['bgrelation_group_price'] = $v['group_price'];

						$res_bgr = $this->db->get_where('tc_bgrelation', array(
									'bgrelation_bundle_id' => $bgr_data['bgrelation_bundle_id'],
									'bgrelation_goods_id' => $bgr_data['bgrelation_goods_id']
								))->row_array();
						if ($res_bgr) {
							$this->db->update($bgr_data, $bgr_data, array('bgrelation_id' => $res_bgr['bgrelation_id']));
							$res_bgr_id = $res_bgr['bgrelation_id'];
						} else {
							$res_bgr_id = $this->db->insert('tc_bgrelation', $bgr_data);
						}
						if (!$res_bgr_id) {
							$res['line'] = __LINE__;
							return $res;
						}
					}
				}// 捆绑商品 end
			}
		} return false;
	}

	// 抓取商品信息
	private function getGoods($starttime, $endtime) {
		$api_url = 'http://pro.shopex.cn/index.php/openapi/api/post';
		$params['method'] = 'product.getGoods';
		$params['app_type'] = 'post';
		$params['format'] = 'json';
		$params['version'] = '1.0';
		$params['call_times'] = 'time';
		$params['return'] = 'has';
		$get_goods_params = array(
			'starttime' => $starttime,
			'endtime' => $endtime,
		);
		// $params = array_merge($params, $get_goods_params);
		$params['sign'] = $this->make_shopex_ac($params, 'procpbi');
		$this->snoopy->submit($api_url, $params);
		$res = $this->snoopy->results;
		p($res);
		exit;
		$json_arr = json_decode($res, true);

		$goods_arr = $json_arr['info']['goods'];
		return $goods_arr;
	}

	private function make_shopex_ac($post_params, $token) {
		ksort($post_params);
		$str = '';
		foreach ($post_params as $key => $value) {
			if ($key != 'sign') {
				$str.=$value;
			}
		}
		return md5($str . $token);
	}

	public function getGoodsProduct($goods_id) {
		$this->db->select('tc_product_basic.product_basic_code, tc_product_basic.product_basic_name, tc_gprelation.gprelation_rate');
		$this->db->from('tc_product_basic');
//        $this->db->where('tc_gprelation.gprelation_goods_id ', '495');
		$this->db->where('tc_gprelation.gprelation_goods_id ', $goods_id);
		$this->db->join('tc_gprelation', 'tc_product_basic.product_basic_id = tc_gprelation.gprelation_product_basic_id');
//$a = $this->db->get()->result_array();
//p($a);exit;
		return $this->db->get()->result_array();
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('goods');
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $user['user_id'];
			unset($data['user']);
		}

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
					$json = $this->arrJson($v);
					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}

		if ($gId = $this->db->select('goods_id')->get_where('tc_goods', array('goods_code' => $save_data['goods_code']))->row_array()) {
//			return array('res' => 'succ', 'msg' => '商品已存在', 'info' => array('goods_id' => $gId['goods_id']));
			// 商品存在更新商品信息
			$this->db->update('tc_goods', $save_data, array('goods_id' => $gId['goods_id']));
			return array('res' => 'succ', 'msg' => '商品更新', 'info' => array('goods_id' => $gId['goods_id']));
		}

		$res = $this->db->insert('tc_goods', $save_data);
		if ($res) {
			$res_id = $this->db->insert_id();
			return array('res' => 'succ', 'msg' => 'add success', 'info' => array('goods_id' => $res_id));
		} else
			return array('res' => 'fail', 'msg' => 'error goods save');
	}

	private function arrJson($v) {
		$json = '';
//		foreach ($v as $kk => $vv) {
//			if (strpos($kk, '.')) {
//				list($fields, $attr) = explode('.', $kk);
//				if ($fields == 'bank_type') {
//					$res_enum = $this->db->select('enum_key')->get_where('dd_enum', array('enum_name' => $vv, 'attr_id' => '560'))->row_array();
//					if (!$res_enum)
//						return array('res' => 'fail', 'msg' => 'error [' . $kk . ' : ' . $vv . ']');
//					$json[$fields] = $res_enum['enum_key'];
//				}
//			} else {
//				$json[$kk] = $vv;
//			}
//		}
		return $json;
	}

	/**
	 * [checkGoodsCode description] 检查商品是否存在
	 * @param  string $goods_code 商品编码
	 * @return [type]             goods_id or false
	 */
	public function checkGoodsCode($goods_code = '') {
		return $this->db->select('goods_id')->get_where('tc_goods', array('goods_code' => $goods_code))->row_array();
	}

}

?>