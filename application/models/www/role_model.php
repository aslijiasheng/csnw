<?php
class Role_model extends CI_Model{

	//这里代表那些字段在列表上显示出来
	public function listLayout(){
		return array(
			
			'role_name',
			
			'role_desc',
			
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout(){
		return array(
			
			'role_name',
			
			'role_desc',
			
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout(){
		return array(
			
			'role_name',
			
			'role_desc',
			
		);
	}

	//属性对应的中文标签
	public function attributeLabels(){
		return array(
			
			'role_name' => '角色名称',
			
			'role_desc' => '角色描述',
			
		);
	}

	//列表查询
	/*
		$page //获得当前的页面值
		$perNumber //每页显示的记录数
	*/
	public function listGetInfo($where,$page=1,$perNumber=10,$like=""){
		$limitStart = ($page-1)*$perNumber+1-1; //这里因为数据库是从0开始算的！所以要减1
		$limit=array($perNumber,$limitStart);
		//p($limit);
		$data = $this->GetInfo($where,$limit,$like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id){
		$where=array(
			'role_id'=>$id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data,$id){
		$this->db->where('role_id',$id)->update('tc_role',$data);
	}

	//单个删除
	public function del($id){
		$this->db->where('role_id',$id)->delete('tc_role');
	}

	//单个新增
	public function add($data){
		$this->db->insert('tc_role',$data);
		$insert_id=$this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where="",$like=""){
		$this->db->from('tc_role');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}


	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where="",$limit="",$like=""){
		//首先查询出本身所有的内容
		$this->db->from('tc_role');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($limit!=""){
			$this->db->limit($limit[0],$limit[1]);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$data=$this->db->get()->result_array();
		//然后循环查询出所有引用的内容
		
		foreach ($data as $k=>$v){
			//p($v);die;
			//所有属性循环
			
				
			
				
			
				
			
			
				
			
				
			
				
			
			
		}
		return $data;
	}
}
?>
