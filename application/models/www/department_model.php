<?php
class Department_model extends CI_Model{

	//这里代表那些字段在列表上显示出来
	public function listLayout(){
		return array(

			'department_name',

			'department_uid',

			'department_code',

			'department_treepath',

			'department_treelevel',

		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout(){
		return array(

			'department_name',

			'department_uid',

			'department_code',

		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout(){
		return array(

			'department_name',

			'department_uid',

			'department_code',

			'department_treepath',

			'department_treelevel',

		);
	}

	//属性对应的中文标签
	public function attributeLabels(){
		return array(

			'department_name' => '部门名称',

			'department_uid' => '上级部门',

			'department_code' => '部门编码',

			'department_treepath' => '树路径',

			'department_treelevel' => '级别',

		);
	}

	//列表查询
	/*
		$page //获得当前的页面值
		$perNumber //每页显示的记录数
	*/
    //自定义排序查询方法

	public function listGetInfo($where,$page=1,$perNumber=10,$like=""){
		$limitStart = ($page-1)*$perNumber+1-1; //这里因为数据库是从0开始算的！所以要减1
		$limit=array($perNumber,$limitStart);
		//p($limit);
		$data = $this->GetInfo($where,$limit,$like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id){
		$where=array(
			'department_id'=>$id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单条查询
	public function validation($data) {
		extract($data, EXTR_OVERWRITE);
		$fields = empty($fields)?"*":$fields;
		$this->db->select($fields);
		$this->db->from('tc_department');
		$this->db->where($where);
		if ($where!="") {
			$this->db->where($where);
		}
		$data = $this->db->get()->result_array();
		return $data[0];
	}

	//单个修改
	public function update($data,$id){
		$this->db->where('department_id',$id)->update('tc_department',$data);
	}

	//单个删除
	public function del($id){
		$this->db->where('department_id',$id)->delete('tc_department');
	}

	//单个新增
	public function add($data){
		$this->db->insert('tc_department',$data);
		$insert_id=$this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where="",$like=""){
		$this->db->from('tc_department');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}


	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where="",$limit="",$like=""){
		//首先查询出本身所有的内容
		$this->db->from('tc_department');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($limit!=""){
			$this->db->limit($limit[0],$limit[1]);
		}
		if ($like!=""){
			$this->db->like($like);
		}

		//$data=$this->db->query('select * from tc_department');
		$data=$this->db->order_by('department_treepath','asc')->get()->result_array();
		//然后循环查询出所有引用的内容

		foreach ($data as $k=>$v){
			//p($v);die;
			//所有属性循环

			if($v['department_uid']!="" and $v['department_uid']!=0){
				$this->load->model('www/department_model','department');
				$data[$k]['department_uid_arr'] = $this->department->id_aGetInfo($v['department_uid']);
			}

		}
		return $data;
	}
}
?>
