<?php

class Order_d_model extends CI_Model {

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'order_d_order_product',
			'order_d_order_productcode',
			'order_d_order_basicproduct',
			'order_d_order_basiccode',
			'order_d_order_servicecycle',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'order_d_order_id',
			'order_d_order_product',
			'order_d_order_productcode',
			'order_d_order_basicproduct',
			'order_d_order_basiccode',
			'order_d_order_servicecycle',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
			'order_d_order_product',
			'order_d_order_productcode',
			'order_d_order_basicproduct',
			'order_d_order_basiccode',
			'order_d_order_servicecycle',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'order_d_order_product' => '商品名称',
			'order_d_order_productcode' => '商品编码',
			'order_d_order_basicproduct' => '基础产品名称',
			'order_d_order_basiccode' => '基础产品编码',
			'order_d_order_servicecycle' => '服务周期',
			'order_d_order_primecost' => '产品原价',
			'order_d_order_disc' => '折后价',
			'order_d_order_style' => '开通状态',
			'order_d_order_id' => '所属订单',
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $like = "") {
		$data = $this->GetInfo($where, '', $like);
		return $data;
	}
	
	/**
	* @例:数据查询
	* @params $data 待查询的数据条件
	* @params return array 返回的数据集结果
	*/
	public function listGetInfoIn( $data ) {
		return $this->db->from("tc_order_d")->where_in( "order_d_id", $data )->get()->result_array();
	}
	
	/**
	* @例:数据删除
	* @params $orderid 待查询的数据条件
	* @retyrb 无
	*/
	public function _deleteInfo( $orderid ) {
		$this->db->where_in( "order_d_id", $orderid )->delete("tc_order_d");
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'order_d_id' => $id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data, $id) {
		$res = $this->db->where('order_d_id', $id)->update('tc_order_d', $data);
		if ($res) {
			return array('res' => 'succ', 'msg' => 'add success');
		}
		return array('res' => 'fail', 'msg' => 'error order_d save');
	}
	
	public function update_pro($data, $pro_data) {
		$res = $this->db->where($pro_data)->update('tc_order_d', $data);
		if ($res) {
			return array('res' => 'succ', 'msg' => 'add success');
		}
		return array('res' => 'fail', 'msg' => 'error order_d save');
	}

	//单个删除
	public function del($id) {
		$this->db->where('order_d_id', $id)->delete('tc_order_d');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_order_d', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_order_d');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_order_d');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容
		$this->load->model('admin/enum_model', 'enum');
		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环
			if ($v['order_d_product_basic_style'] != "" and $v['order_d_product_basic_style'] != 0) {
				$data[$k]['order_d_product_basic_style_arr'] = $this->enum->getlist(126, $data[$k]['order_d_product_basic_style']);
			}
			//这里需要知道这个明细对应的商品的开通、返回参数属性
			$data[$k]['params_arr'] = $this->order_d_id_params_info($v['order_d_id']);
			
			//查询技术产品结果
			if (isset($v['order_d_product_basic_code'])) {
				$data[$k]['product_tech_arr'] = $this->order_d_product_tech($v['order_d_product_basic_code']);
			}
			//查询开通参数和返回参数
			$data[$k]['applyopen_arr'] = $this->applyopen_info($v['order_d_id']);
			if ( empty($data[$k]['applyopen_arr']) ) {
				unset($data[$k]['applyopen_arr']);
			}

		}
		return $data;
	}
	
	/**
	* @params $id 订单明细ID
	* @return array 返回查询出的结果集,如果没有或失败返回空
	*/
	private function applyopen_info($id) {
		$this->db->query("set names utf8");
		$sql = "SELECT * FROM tc_applyopen WHERE applyopen_order_d_id = '".$id."'";
		if ($this->db->query($sql)) {
			$res = $this->db->query($sql)->result_array();
			return $res;
		} else {
			return null;
		}

	}
	
	/**
	* @params $basic_code 技术产品编号
	* @return array 技术产品结果集
	*/
	private function order_d_product_tech($basic_code) {
		$sql = "SELECT product_tech_code,product_tech_name FROM tc_product_tech WHERE product_tech_id = (SELECT ptrelation_product_tech_id FROM tc_ptrelation WHERE ptrelation_product_basic_id = (SELECT product_basic_id FROM tc_product_basic WHERE product_basic_code = '".$basic_code."'))";
		$res = $this->db->query($sql)->result_array();
		return $res;
	}

	public function order_d_id_params_info($id) {
		//$sql = "select * from tc_product_oparam where product_oparam_product_basic_id = (select product_basic_id from tc_product_basic where product_basic_code = (select order_d_product_basic_code from tc_order_d where order_d_id = '".$id."'))";
		$sql = "
select
po.product_oparam_id,
po.product_oparam_product_basic_id,
po.product_oparam_name,
po.product_oparam_content,
po.product_oparam_type,
po.product_oparam_atime,
po.product_oparam_utime
from tc_product_oparam po
left join tc_product_basic pb on po.product_oparam_product_basic_id = pb.product_basic_id
left join tc_order_d od on pb.product_basic_code = od.order_d_product_basic_code
where order_d_id = '" . $id . "'
		";
		//p($sql);
		$res = $this->db->query($sql)->result_array();
		foreach ($res as $k => $v) {
			if ($v['product_oparam_type'] == 1001) { //表示开通参数
				$params_o = json_decode($v['product_oparam_content'], true);
				$res['open'][$params_o['params_code']] = $params_o;
			}
			if ($v['product_oparam_type'] == 1002) { //表示返回参数
				$params_r = json_decode($v['product_oparam_content'], true);
				$res['return'][$params_r['params_code']] = $params_r;
			}
		}
		return $res;
	}

	public function GetOrderList($order_id) {
		$where = array(
			'order_d_order_id' => $order_id,
		);
		$this->db->select()->from('tc_order_d')->where($where)->order_by('order_d_id')->group_by('order_d_goods_code');
		$res = $this->db->get()->result_array();
		foreach ($res as $k => $v) {
			$data['listData'][$k] = $v;
			$data['listData'][$k]['order_d_order_primecost'] = 0;
			$data['listData'][$k]['order_d_order_disc'] = 0;
			$where = array(
				'order_d_order_id' => $order_id,
				'order_d_goods_code' => $v['order_d_goods_code']
			);
			$res_p = $this->order_d->GetInfo($where);
			$data['listData'][$k]['product_arr'] = $res_p;
			foreach ($res_p as $vv) {
				$data['listData'][$k]['order_d_order_primecost'] += $vv['order_d_product_basic_primecost']; // 原价
				$data['listData'][$k]['order_d_order_disc'] += $vv['order_d_product_basic_disc']; // 折后价
			}
		}
		return $data['listData'];
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('order_d');
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $user['user_id'];
			unset($data['user']);
		}

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
					$json = $this->arrJson($v);
					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}

		$res = $this->db->insert('tc_order_d', $save_data);
		if ($res) {
			return array('res' => 'succ', 'msg' => 'add success');
		} else
			return array('res' => 'fail', 'msg' => 'error order_d save');
	}

	public function getCycle($data) {
		$r_goods = $this->db->select('goods_id,goods_type')->get_where('tc_goods', array('goods_code' => $data['order_d_goods_code']))->row_array();
		if ($r_goods['goods_type'] == '1001') {
			$sql = 'select * from tc_goods_price where goods_price_goods_id=' . $r_goods['goods_id'] . ' and (goods_price_cycle_price=' . $data['order_d_product_basic_primecost'] . ' or goods_price_start_price=' . $data['order_d_product_basic_primecost'] . ')';
			return $this->db->query($sql)->row_array();
		} else {
			$sql = 'select bgrelation_goods_price_id from tc_bgrelation where bgrelation_bundle_id=' . $r_goods['goods_id'] . ' and bgrelation_group_price=' . $data['order_d_product_basic_primecost'];
			$r_bg = $this->db->query($sql)->row_array();
			return $this->db->get_where('tc_goods_price', array('goods_price_id' => $r_bg['bgrelation_goods_price_id']))->row_array();
		}
	}

	//2014-07-17 api start
	public function _addCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无订单明细数据');
		return $this->api->_return(__class__ . ' _addCheck succ', $data, 'succ');
	}

	public function _updateCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无订单明细数据');
		return $this->api->_return(__class__ . ' _updateCheck succ', $data, 'succ');
	}
	//2014-07-17 api end

}

?>
