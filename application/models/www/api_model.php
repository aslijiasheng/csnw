<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Api_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->db->query('set names utf8');
		date_default_timezone_set('PRC');
		$this->db->trans_strict(FALSE);
	}

	/**
	 * 通用查询
	 * @param  string $data 条件组合
	 * @return array
	 */
	public function select($data = '', $object = '') {
		$data = is_array($data) ? $data : json_decode($data, true);
		$diff = array_diff(array('where_all', 'rel_all', 'page', 'perNumber', 'obj'), array_keys($data));
		if ($diff)
			return $this->apiLog(false, __FUNCTION__ . '缺少内容' . $diff); //检查数据完整性
		$res_data['sel_data'] = $data;
		$objDetail = $this->objDetail($data['obj']);
		
		if (!$data['where_all'])
			$data['where_all'] = array(); // 避免空数组

		foreach ($data['where_all'] as $rel => $cond) {
			$cond['value'] = (isset($cond['value']) || (!empty($cond['value']))) ? trim($cond['value']) : ''; // 过滤空格
			if (strpos($cond['attr'], '.')) {
				list($fields, $attr) = explode('.', $cond['attr']);
				$this_fields = $data['obj'] . '_' . $fields;
				$q_obj = $objDetail['obj_attr'][$fields]; //引用对象
				
				$q_obj_fields = $q_obj . '_' . $attr; //引用对象字段
				//p($q_obj);
				switch ($q_obj) {
					case 'enum': //枚举
						$cond['action'] = "inc";
						if (strpos($cond['value'], ',')) {
							$where = array('attr_id' => $objDetail['obj_attr_id'][$fields]);
							$res_enum = $this->db->select($q_obj . '_id')->where($where)->where_in($q_obj_fields, explode(',', $cond['value']))->get('tc_' . $q_obj)->result_array();
						} else {
							$where = array('attr_id' => $objDetail['obj_attr_id'][$fields]);
							$res_enum = $this->db->select('enum_key')->like($q_obj_fields, $cond['value'], 'both')->where($where)->get('dd_enum')->result_array();
						}
						if ($res_enum) {
							$cond['attr'] = $this_fields;
							if (count($res_enum) > 1) {
								$cond['value'] = '';
								foreach ($res_enum as $ev) {
									$cond['value'] .= ',' . $ev['enum_key'];
								}
								$cond['value'] = ltrim($cond['value'], ',');
							} else {
								$cond['value'] = $res_enum[0]['enum_key'];								
							}
						} else
							return $this->apiLog(false, __FUNCTION__ . ' error enum ' . $fields);
						break;
					default: //引用
						if (strpos($cond['value'], ',')) {
							$res_q_id = $this->db->select($q_obj . '_id')->where_in($q_obj_fields, explode(',', $cond['value']))->get('tc_' . $q_obj)->result_array();
						} else {
//							$where = array($q_obj_fields => $cond['value']);
//							$res_q_id = $this->db->select($q_obj . '_id')->where($where)->get('tc_' . $q_obj)->result_array(); //精确查询
							
							$res_q_id = $this->db->select($q_obj . '_id')->like($q_obj_fields, $cond['value'], 'both')->get('tc_' . $q_obj)->result_array(); //模糊查询
						}

						if ($res_q_id) {
							$cond['attr'] = $this_fields;
							if (count($res_q_id) > 1) {
								$cond['value'] = '';
								foreach ($res_q_id as $qv) {
									$cond['value'] .= ',' . $qv[$q_obj . '_id'];
								}
								$cond['value'] = ltrim($cond['value'], ',');
								$cond['action'] = 'INC';
							} else {
								$cond['value'] = $res_q_id[0][$q_obj . '_id'];
//								$cond['action'] = '=';
							}
						} else
							return $this->apiLog(false, __FUNCTION__ . ' error quote');
						break;
				}
			} else
				$cond['attr'] = ($cond['attr'] == 'type_id') ? 'type_id' : $data['obj'] . '_' . $cond['attr'];
			switch (strtoupper($cond['action'])) {
				case 'NULL'; //为空 同is null
					$$rel = $cond['attr'] . ' IS NULL OR ' . $cond['attr'] . ' = ""';
					break;
				case 'NOT_NULL': //不为空 同is not null
					$$rel = $cond['attr'] . ' IS NOT NULL';
					break;
				case 'NOT_EQUAL': //不等于 同<>
					$$rel = $cond['attr'] . ' != \'' . $cond['value'] . '\'';
					break;
				case 'GT': //大于 同>
					$$rel = $cond['attr'] . ' > \'' . $cond['value'] . '\'';
					break;
				case 'GT_EQUAL': //大于等于 同>=
					$$rel = $condition['attr'] . ' >= \'' . $cond['value'] . '\'';
					break;
				case 'LE': //小于 同<
					$$rel = $cond['attr'] . ' < \'' . $cond['value'] . '\'';
					break;
				case 'LE_EQUAL': //小于等于 同<=
					$$rel = $cond['attr'] . ' <= \'' . $cond['value'] . '\'';
					break;
				case 'INC': //涵盖 同in
					$cond['value'] = str_replace(',', '\',\'', $cond['value']);
					$$rel = $cond['attr'] . ' IN(\'' . $cond['value'] . '\')';
					break;
				case 'NOT_INC': //不涵盖 同not in
					$cond['value'] = str_replace(',', '\',\'', $cond['value']);
					$$rel = $cond['attr'] . ' NOT IN(\'' . $cond['value'] . '\')';
					break;
				case 'RANGE': //区间 同>= and <=
					if(preg_match("/[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}/",$cond['value2'])){
						//是日期时间类型，给成 23:59:59
						if(strlen($cond['value2'])<=10 and strlen($cond['value2'])>=8){
							$cond['value2'] = $cond['value2']." 23:59:59";
						}
					}
					$$rel = $cond['attr'] . ' >= \'' . $cond['value1'] . '\' AND ' . $cond['attr'] . ' <= \'' . $cond['value2'] . '\'';
					break;
				case 'LIKE':
					$$rel = $cond['attr'] . ' LIKE "%' . $cond['value'] . '%"';
					break;
				case 'NOT_LIKE': //不包含 同not like
					$$rel = $cond['attr'] . ' NOT LIKE "%' . $cond['value'] . '%"';
					break;
				case 'BEFORE': //早于，用于日期时间类型
					$$rel = $cond['attr'] . ' < "' . $cond['value'] . '"';
					break;
				case 'AFTER': //晚于，用于日期时间类型
					$$rel = $cond['attr'] . ' > "' . $cond['value'] . '"';
					break;
				case 'RECENT': //最近，用于日期时间类型
					$$rel = $cond['attr'] . ' >= "' . date("Y-m-d H:i:s", strtotime("-$cond[value1] $cond[value2]")) . '"';
					break;
				case 'FUTURE': //未来，用于日期时间类型
					$$rel = $cond['attr'] . ' >= "' . date("Y-m-d H:i:s", strtotime("+$cond[value1] $cond[value2]")) . '"';
					break;
				case 'BELONGTO':
					if ($q_obj == 'department') {
						$cond['value'] = str_pad($cond['value'], 4, "0", STR_PAD_LEFT);
						$department_id = $this->db->select($q_obj . '_id')->like('department_treepath', $cond['value'], 'both')->get('tc_' . $q_obj)->result_array();
						if ($department_id) {
							$d_arr = array();
							foreach ($department_id as $dv) {
								$d_arr[] = $dv['department_id'];
							}
							$cond['value'] = implode(',', $d_arr);
						} else
							return $this->apiLog(false, __FUNCTION__ . ' error ' . $department_id);
						$$rel = $cond['attr'] . ' IN(' . $cond['value'] . ')';
					}
					break;
				default:
					$$rel = $cond['attr'] . ' = \'' . $cond['value'] . '\'';
					break;
			}
			$data['rel_all'] = str_replace($rel, $$rel, $data['rel_all']);
		}

		$limitStart = ($data['page'] - 1) * $data['perNumber'] + 1 - 1;
		if ($data['obj'] == 'order' || $data['obj'] == 'message' || $data['obj'] == 'books' || $data['obj'] == 'salespay' || $data['obj'] == 'invoice') {
			$orderBy = ' order by ' . $data['obj'] . '_id desc ';
		} else
			$orderBy = '';


		if ($data['rel_all'] != '') {
			$sql = 'SELECT * FROM ' . 'tc_' . $data['obj'] . ' WHERE 1 AND ' . $data['rel_all'] . $orderBy . ' LIMIT ' . $limitStart . ', ' . $data['perNumber'];
		} else
			$sql = 'SELECT * FROM ' . 'tc_' . $data['obj'] . $orderBy . ' LIMIT ' . $limitStart . ', ' . $data['perNumber'];
			//p($sql);die();
		$result = $this->db->query($sql)->result_array();
		//echo $sql;
		// 返回结果处理
		$res_data['info'] = $this->parse($result, $data['obj'], $objDetail);

		if ($data['rel_all'] != '') {
			$sql_n = 'SELECT count(*) num FROM ' . 'tc_' . $data['obj'] . ' WHERE 1 AND ' . $data['rel_all'];
		} else
			$sql_n = 'SELECT count(*) num FROM ' . 'tc_' . $data['obj'];
		//p($sql_n);
		$res_n = $this->db->query($sql_n)->row_array();
		$res_data['count'] = $res_n['num'];

		if ($res_data) {
			if ($object)
				return $res_data['info'];
			return $res_data;
		} else
			return false;
	}
	
	/**
	 * api接口特殊数据解析
	 * @param array $data 要解析的数组
	 * @return array 返回解析后的数组
	 */
	private function apiParseData( $data ) {
		
		foreach ($data['where_all'] as $rel=>$cond) {
			if (strpos($cond['attr'], '.')) {
				list($fields, $attr) = explode('.', $cond['attr']);
				if ($fields=="type") {
					$type_sql = "SELECT type_id FROM dd_type WHERE type_".$attr."='".$cond['value']."'";
					
					//判断SQL执行是否成功
					if ( $query = $this->db->query( $type_sql ) ) {
						$res = $query->result_array();
					} else {
						return $this->_msg($sql.'--执行失败'); //检查数据完整性
					}
					
					if(isset($res[0])){
						$data['where_all'][$rel]['attr'] = 'type_id';
						$data['where_all'][$rel]['value'] = $res[0]['type_id'];
					}else{
						return $this->_msg(__FUNCTION__.'无结果'); //检查数据完整性
					}
				}
			}
		}
		return $data;
	}
	/**
	 * 接口通用查询 2014-08-21
	 * @param  string $data 条件组合
	 * @return array
	 */
	public function api_select($data = '', $object = '') {
		$data = is_array($data) ? $data : json_decode($data, true);
		//$data = $this->parseData(array("type.name"=>"销售订单"), $object);
		//die(json_encode($data));
		$diff = array_diff(array('where_all', 'rel_all', 'page', 'perNumber', 'obj'), array_keys($data));
		if ($diff)
			return $this->apiLog(false, __FUNCTION__ . '缺少内容' . $diff); //检查数据完整性
		$res_data['sel_data'] = $data;
		$objDetail = $this->objDetail($data['obj']);
		
		if (!$data['where_all'])
			$data['where_all'] = array(); // 避免空数组
			
		$data = $this->apiParseData($data); //处理特殊数据

		foreach ($data['where_all'] as $rel => $cond) {
			$cond['value'] = (isset($cond['value']) || (!empty($cond['value']))) ? trim($cond['value']) : ''; // 过滤空格
			if (strpos($cond['attr'], '.')) {
				list($fields, $attr) = explode('.', $cond['attr']);
				$this_fields = $data['obj'] . '_' . $fields;
				$q_obj = $objDetail['obj_attr'][$fields]; //引用对象
				$q_obj_fields = $q_obj . '_' . $attr; //引用对象字段
				switch ($q_obj) {
					case 'enum': //枚举
						if (strpos($cond['value'], ',')) {
							$where = array('attr_id' => $objDetail['obj_attr_id'][$fields]);
							$res_enum = $this->db->select($q_obj . '_id')->where($where)->where_in($q_obj_fields, explode(',', $cond['value']))->get('tc_' . $q_obj)->result_array();
						} else {
							$where = array('attr_id' => $objDetail['obj_attr_id'][$fields]);
							$res_enum = $this->db->select('enum_key')->like($q_obj_fields, $cond['value'], 'both')->where($where)->get('dd_enum')->result_array();
						}

						if ($res_enum) {
							$cond['attr'] = $this_fields;
							if (count($res_enum) > 1) {
								$cond['value'] = '';
								foreach ($res_enum as $ev) {
									$cond['value'] .= ',' . $ev['enum_key'];
								}
								$cond['value'] = ltrim($cond['value'], ',');
							} else
								$cond['value'] = $res_enum[0]['enum_key'];
						} else
							return $this->apiLog(false, __FUNCTION__ . ' error enum ' . $fields);
						break;
					default: //引用
						if (strpos($cond['value'], ',')) {
							$res_q_id = $this->db->select($q_obj . '_id')->where_in($q_obj_fields, explode(',', $cond['value']))->get('tc_' . $q_obj)->result_array();
						} else {
							$res_q_id = $this->db->select($q_obj . '_id')->like($q_obj_fields, $cond['value'], 'both')->get('tc_' . $q_obj)->result_array(); //模糊查询
						}

						if ($res_q_id) {
							$cond['attr'] = $this_fields;
							if (count($res_q_id) > 1) {
								$cond['value'] = '';
								foreach ($res_q_id as $qv) {
									$cond['value'] .= ',' . $qv[$q_obj . '_id'];
								}
								$cond['value'] = ltrim($cond['value'], ',');
								$cond['action'] = 'INC';
							} else {
								$cond['value'] = $res_q_id[0][$q_obj . '_id'];
//								$cond['action'] = '=';
							}
						} else
							return $this->apiLog(false, __FUNCTION__ . ' error quote');
						break;
				}
			} else
				$cond['attr'] = ($cond['attr'] == 'type_id') ? 'type_id' : $data['obj'] . '_' . $cond['attr'];
			switch (strtoupper($cond['action'])) {
				case 'NULL'; //为空 同is null
					$$rel = $cond['attr'] . ' IS NULL OR ' . $cond['attr'] . ' = ""';
					break;
				case 'NOT_NULL': //不为空 同is not null
					$$rel = $cond['attr'] . ' IS NOT NULL';
					break;
				case 'NOT_EQUAL': //不等于 同<>
					$$rel = $cond['attr'] . ' != \'' . $cond['value'] . '\'';
					break;
				case 'GT': //大于 同>
					$$rel = $cond['attr'] . ' > \'' . $cond['value'] . '\'';
					break;
				case 'GT_EQUAL': //大于等于 同>=
					$$rel = $condition['attr'] . ' >= \'' . $cond['value'] . '\'';
					break;
				case 'LE': //小于 同<
					$$rel = $cond['attr'] . ' < \'' . $cond['value'] . '\'';
					break;
				case 'LE_EQUAL': //小于等于 同<=
					$$rel = $cond['attr'] . ' <= \'' . $cond['value'] . '\'';
					break;
				case 'INC': //涵盖 同in
					$cond['value'] = str_replace(',', '\',\'', $cond['value']);
					$$rel = $cond['attr'] . ' IN(\'' . $cond['value'] . '\')';
					break;
				case 'NOT_INC': //不涵盖 同not in
					$cond['value'] = str_replace(',', '\',\'', $cond['value']);
					$$rel = $cond['attr'] . ' NOT IN(\'' . $cond['value'] . '\')';
					break;
				case 'RANGE': //区间 同>= and <=
					if(preg_match("/[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}/",$cond['value2'])){
						//是日期时间类型，给成 23:59:59
						if(strlen($cond['value2'])<=10 and strlen($cond['value2'])>=8){
							$cond['value2'] = $cond['value2']." 23:59:59";
						}
					}
					$$rel = $cond['attr'] . ' >= \'' . $cond['value1'] . '\' AND ' . $cond['attr'] . ' <= \'' . $cond['value2'] . '\'';
					break;
				case 'LIKE':
					$$rel = $cond['attr'] . ' LIKE "%' . $cond['value'] . '%"';
					break;
				case 'NOT_LIKE': //不包含 同not like
					$$rel = $cond['attr'] . ' NOT LIKE "%' . $cond['value'] . '%"';
					break;
				case 'BEFORE': //早于，用于日期时间类型
					$$rel = $cond['attr'] . ' < "' . $cond['value'] . '"';
					break;
				case 'AFTER': //晚于，用于日期时间类型
					$$rel = $cond['attr'] . ' > "' . $cond['value'] . '"';
					break;
				case 'RECENT': //最近，用于日期时间类型
					$$rel = $cond['attr'] . ' >= "' . date("Y-m-d H:i:s", strtotime("-$cond[value1] $cond[value2]")) . '"';
					break;
				case 'FUTURE': //未来，用于日期时间类型
					$$rel = $cond['attr'] . ' >= "' . date("Y-m-d H:i:s", strtotime("+$cond[value1] $cond[value2]")) . '"';
					break;
				case 'BELONGTO':
					if ($q_obj == 'department') {
						$cond['value'] = str_pad($cond['value'], 4, "0", STR_PAD_LEFT);
						$department_id = $this->db->select($q_obj . '_id')->like('department_treepath', $cond['value'], 'both')->get('tc_' . $q_obj)->result_array();
						if ($department_id) {
							$d_arr = array();
							foreach ($department_id as $dv) {
								$d_arr[] = $dv['department_id'];
							}
							$cond['value'] = implode(',', $d_arr);
						} else
							return $this->apiLog(false, __FUNCTION__ . ' error ' . $department_id);
						$$rel = $cond['attr'] . ' IN(' . $cond['value'] . ')';
					}
					break;
				default:
					$$rel = $cond['attr'] . ' = \'' . $cond['value'] . '\'';
					break;
			}
			$data['rel_all'] = str_replace($rel, $$rel, $data['rel_all']);
		}

		$limitStart = ($data['page'] - 1) * $data['perNumber'] + 1 - 1;
		if ($data['obj'] == 'order' || $data['obj'] == 'message' || $data['obj'] == 'books' || $data['obj'] == 'salespay' || $data['obj'] == 'invoice') {
			$orderBy = ' order by ' . $data['obj'] . '_id desc ';
		} else
			$orderBy = '';


		if ($data['rel_all'] != '') {
			$sql = 'SELECT * FROM ' . 'tc_' . $data['obj'] . ' WHERE 1 AND ' . $data['rel_all'] . $orderBy . ' LIMIT ' . $limitStart . ', ' . $data['perNumber'];
		} else {
			$sql = 'SELECT * FROM ' . 'tc_' . $data['obj'] . $orderBy . ' LIMIT ' . $limitStart . ', ' . $data['perNumber'];
		}

		//判断SQL执行是否成功
		if ( $query = $this->db->query( $sql ) ) {
			$result = $query->result_array();
		} else {
			return $this->_msg('SQL查询执行失败'); //检查数据完整性
		}

		
		//2014-08-21 更新查询接口 start
		$api_model = "www/".$data['obj']."_model";
		$this->load->model($api_model, $data['obj']);
		$result = $this->$data['obj']->info( $result );
		
		//2014-08-21 更新查询接口 end
		// 返回结果处理
		//$res_data['info'] = $this->parse($result, $data['obj'], $objDetail);
		$res_data['info'] = $result;
		if ($data['rel_all'] != '') {
			$sql_n = 'SELECT count(*) num FROM ' . 'tc_' . $data['obj'] . ' WHERE 1 AND ' . $data['rel_all'];
		} else
			$sql_n = 'SELECT count(*) num FROM ' . 'tc_' . $data['obj'];
		
		//判断SQL执行是否成功
		if ( $query = $this->db->query( $sql_n ) ) {
			$res_n = $query->row_array();
		} else {
			return $this->_msg('SQL统计查询执行失败'); //检查数据完整性
		}

		$res_data['count'] = $res_n['num'];
		if ($res_data) {
			if ($object)
				return $res_data['info'];
			return $res_data;
		} else
			return false;
	}

	public function api_add($data = '', $object = '', $user = '') {
		$data = is_array($data) ? $data : json_decode($data, true);
		if (empty($data))
			return $this->apiLog('fail', 'error data 数据有问题');

		$this->db->trans_begin(); //事务 Start
		if (isset($data['detailed'])) { //明细
			$detailed = $data['detailed'];
			unset($data['detailed']);
		} else
			$detailed = array();

		// 调用相应 model app_add
		$this->load->model('www/' . $object . '_model', $object);
		$data['user'] = $user;
		$result = $this->$object->api_add($data);
		p($result);
		if (!isset($result)) {
			$this->db->trans_rollback();
			$this->db->close();
			return $this->apiLog('fail', 'error none return');
		} elseif ($result['res'] == 'fail') {
			$this->db->trans_rollback();
			$this->db->close();
			return $this->apiLog('fail', $result['msg']);
		}

		// 返回内容
		if (isset($result['info']))
			$info = $result['info'];
			
		// 明细处理
		foreach ($detailed as $d_object => $d_data) {
			$this->load->model('www/' . $d_object . '_model', $d_object);
			foreach ($d_data as $k => $v) {
				// 返回内容处理
				switch ($d_object) {
					case 'order_d':
						$res_d = $this->dealDetailData($v);
						if ($res_d) {
							$this->saveOrderDetail($res_d, $info['order_id'], $k); // 保存商品明细
						} else {
							$this->db->trans_rollback();
							$this->db->close();
							return $this->apiLog('fail', 'goods price error');
						}
						$result = array('res' => 'succ', 'msg' => '');
						break;
					case 'rel_order_order':
						if (isset($info['order_id'])) {
							$v['my_order'] = $info['order_id'];
						} else {
							$this->db->trans_rollback();
							$this->db->close();
							return $this->apiLog('fail', 'error rel_order_order my_order is none');
						}
						$result = $this->$d_object->api_add($v);
						break;
					case 'salespay':
						if (isset($info['order_id'])) {
							$v['order_id'] = $info['order_id'];
						} else {
							$this->db->trans_rollback();
							$this->db->close();
							return $this->apiLog('fail', 'error salespay order_id is none');
						}
						$result = $this->$d_object->api_add($v);
						break;
				}

				if (!isset($result)) {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog('fail', 'error none return');
				} elseif ($result['res'] == 'fail') {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog('fail', $result['msg']);
				}
			}
		}

		// 事务 End

		if ($this->db->trans_status() === false) {
			//p($this->db->last_query());
			$this->db->trans_rollback();
			$this->db->close();
			return $this->apiLog('fail', 'add data error sql rollback');
		} else {
			$this->db->trans_commit();
			$this->db->close();
			if (isset($info) && !empty($info))
				return $this->apiLog('succ', $result['msg'], $info);
			else
				return $this->apiLog('succ', 'add success');
		}
	}

	/**
	 * 处理枚举对象
	 * @param type $data
	 * @param type $attr_id
	 */
	public function parseEnum($data = '', $attr_id = '') {
		if (strpos($data, ',')) {
			$where = array('attr_id' => $attr_id);

			$arr = explode(',', $data);
			foreach ($arr as $k => $v) {
				$arr[$k] = trim($v);
			}
			$this->db->query("set names utf8");
			$res_enum = $this->db->select('enum_key')->where($where)->where_in('enum_name', $arr)->get('dd_enum')->result_array();
		} else {
			$where = array('enum_name' => $data, 'attr_id' => $attr_id);
			$this->db->query("set names utf8");
			$res_enum = $this->db->select('enum_key')->where($where)->get('dd_enum')->row_array();
		}

		if ($res_enum) {
			if (count($res_enum) > 1) {
				$result = '';
				foreach ($res_enum as $ev) {
					$result .= ',' . $ev['enum_key'];
				}
				return ltrim($result, ',');
			} else
				return $res_enum['enum_key'];
		} else
			return false;
	}

	/**
	 * 处理引用对象
	 * @param type $data
	 * @param type $object
	 */
	public function parseQuote($data = '', $attr = '', $object = '') {
		$fields = $object . '_' . $attr;
		if (!$this->db->field_exists($fields, 'tc_' . $object)) {
			return false;
		}
		if (strpos($data, ',')) {
			$res_q_id = $this->db->select($object . '_id')->where_in($q_obj_fields, explode(',', $v))->get('tc_' . $q_obj)->result_array();
		} else {
			$where = array($fields => $data);
			$res_q_id = $this->db->select($object . '_id')->where($where)->get('tc_' . $object)->row_array();
		}

		if ($res_q_id) {
			if (count($res_q_id) > 1) {
				$result = '';
				foreach ($res_q_id as $qv) {
					$result .= ',' . $qv[$object . '_id'];
				}
				return ltrim($result, ',');
			} else
				return $res_q_id[$object . '_id'];
		} else {
			return false;
//			数据不存在，抓取数据
//			$res_sync = array();
//			$res_sync = $this->apiSync($q_obj, $v);
//			if ($res_sync['res'] == 'fail') {
//				$this->db->trans_rollback();
//				return $this->apiLog('fail', 'api sync ' . $k . ' ' . $v);
//			} else
//				$save_data[$this_fields] = $res_sync['msg'];
		}
	}

	/**
	 * api 日志
	 * @return boolean
	 */
	public function apiLog($res = 'fail', $msg = '', $info = '') {
		$data['res'] = $res;
		$data['msg'] = $msg;
		$data['atime'] = date('Y-m-d H:i:s', time());
		$this->db->insert('tc_api_old_log', $data);
		unset($data['atime']);

		if (!empty($info))
			$data['info'] = $info;
		return json_encode($data);
	}

	/**
	 * 对象数据
	 * @param  string $obj_name 对象名称
	 * @return array
	 */
	public function objDetail($obj_name = '') {
		$res_obj_list = $this->db->select('obj_id, obj_name')->get('dd_object')->result_array();
		foreach ($res_obj_list as $v) {
			$obj_arr[$v['obj_id']] = $v['obj_name'];
		}

		$this_obj_id = array_search($obj_name, $obj_arr);

		if ($this_obj_id) {
			$res_obj_list = $this->db->select('attr_id, attr_name, attr_type, attr_field_name, attr_quote_id')->where('attr_obj_id', $this_obj_id)->get('dd_attribute')->result_array(); //获取当前对象属性
		} else {
			return $this->apiLog('fail', 'error objDetail ' . $obj_name);
		}

		$this_obj_attr = $this_obj_attr_id = array();
		foreach ($res_obj_list as $k => $v) { //筛选出数字类型、引用、枚举
			switch ($v['attr_type']) {
				case '5': //5 数字类型
					$this_obj_attr[$v['attr_name']] = 'integer';
					$this_obj_attr_id[$v['attr_name']] = $v['attr_id'];
					break;
				case '14': //14,15 枚举类型
				case '15':
					$this_obj_attr[$v['attr_name']] = 'enum';
					$this_obj_attr_id[$v['attr_name']] = $v['attr_id'];
					break;
				case '19': //19,23 引用类型
				case '23':
					$this_obj_attr[$v['attr_name']] = $obj_arr[$v['attr_quote_id']];
					$this_obj_attr_id[$v['attr_name']] = $v['attr_id'];
					break;
				default:
					break;
			}
		}
		return array('obj' => $obj_name, 'obj_id' => $this_obj_id, 'obj_attr' => $this_obj_attr, 'obj_attr_id' => $this_obj_attr_id);
	}

	/**
	 * 解析object
	 * @param  array  $data 数据
	 * @param  string $obj 对象
	 * @return array
	 */
	public function parse($data, $obj, $objDetail) {
		foreach ($data as $key => $value) {
			foreach ($value as $kk => $vv) {
				$k = str_replace($obj . '_', '', $kk);
				if (isset($objDetail['obj_attr'][$k]) && $objDetail['obj_attr'][$k] != 'integer' && $objDetail['obj_attr'][$k] != 'enum') {
					if (!empty($vv)) {
						$q_obj = $objDetail['obj_attr'][$k];
						$res_q = $this->db->get_where('tc_' . $q_obj, array($q_obj . '_id' => $vv))->row_array();
						$data[$key][$kk . '_arr'] = $res_q ? $res_q : '';
					}
				} elseif (isset($objDetail['obj_attr'][$k]) && $objDetail['obj_attr'][$k] == 'enum') {
					if (!empty($vv)) {
						$q_obj = $objDetail['obj_attr'][$k];
						$res_q = $this->db->get_where('dd_enum', array('enum_key' => $vv, 'attr_id' => $objDetail['obj_attr_id'][$k]))->row_array();
						$data[$key][$kk . '_arr'] = $res_q ? $res_q : '';
					}
				}
			}

			//已到账金额
			if ($obj == 'order' && ($value['type_id'] == 6 || $value['type_id'] == 10 || $value['type_id'] == 7 || $value['type_id'] == 13)) {
				$this->load->model('www/salespay_model', 'salespay');
				$where = array(
					'salespay_order_id' => $data[$key]['order_id'],
					'salespay_status' => 1002,
					'salespay_money_type' => 1001
				);
				$data[$key]['ydz_amount'] = $this->salespay->SumGetInfo('salespay_pay_amount', $where);
			}
		}
		return $data;
	}

	/**
	 * 订单编号
	 * @param  string $type_id 订单类型
	 * @return string order_number
	 */
	private function getNumber($type_id = '') {
		$res = $this->db->select('order_number')->get_where('dd_number', array('order_type' => $type_id))->row_array();
		if (empty($res['order_number'])) {
			$this->db->where('order_type', $type_id);
			$res['order_number'] = date('Ymd', time()) . '0001';
			$this->db->update('dd_number', $res);
		}
		$now = date('Ymd', time());
		$tail = substr($res['order_number'], 8, 4);
		if ($now != substr($res['order_number'], 0, 8)) {
			$tail = '0001';
		} else
			$tail = str_pad($tail + 1, 4, "0", STR_PAD_LEFT);
		$data = date('Ymd', time()) . $tail;
		$this->db->where('order_type', $type_id);
		$this->db->update('dd_number', array('order_number' => $data));
		return $res['order_number'];
	}

	//检查订单编号
	private function checkNumber($order_number = '') {
		$r_id = $this->db->select('order_id')->get_where('tc_order', array('order_number' => $order_number))->row_array();
		if ($r_id)
			return true;
		else
			return false;
	}

	/**
	 * 调用接口
	 * @param  string $object 对象
	 * @param  string $data 数据
	 * @return array
	 */
	public function apiSync($object = '', $data = '') {
		switch ($object) {
			case 'account':
				$this->load->model('www/account_model', 'account');
				//CRM抓取客户信息
				//$lee_url = '192.168.0.18:8088';
				$lee_url = '192.168.35.14'; //CRM升级负载均衡换IP
				$lee_orgcode = 'ShopEx'; //单位名称
				$wsURL = "http://" . $lee_url . "/webservice/service.php?orgcode=" . $lee_orgcode . "&class=WS_System&wsdl";
				$params = array("admin", "hql444h3l");
				$header = null;
				$client = new SoapClient($wsURL);
				$session = $client->__soapCall("login", $params, array(), $header);
				$wsURL = "http://" . $lee_url . "/webservice/service.php?orgcode=" . $lee_orgcode . "&class=WS_EAI&wsdl";
//				$queryXML = '<Interface type="query" model="object" value ="Account"><Condition><Express field="Account.AccountNumber" operator="=" value="' . $data . '"/></Condition></Interface>';
				$queryXML = '<Interface type="query" model="object" value ="Account"><Condition><Express field="Account.customerID" operator="=" value="' . $data . '"/></Condition></Interface>';
				$params = array($queryXML);
				$header = new SoapHeader("http://" . $_SERVER['HTTP_HOST'], "PHPSESSID", $session);
				$client = new SoapClient($wsURL);
				$resultXML = $client->__soapCall("process", $params, array(), $header);
				$resultObj = simplexml_load_string($resultXML);
				$resultArr = get_object_vars($resultObj->Row); //转换成数组

				if ($resultArr) {
					$add_data = array(
						'name' => $resultArr['Account.Name'],
						'update_time' => $resultArr['Account.ModifiedTime'],
						'account_shopexid' => $resultArr['Account.customerID'],
						'create_time' => $resultArr['Account.CreatedTime'],
						'telephone' => $resultArr['Account.Mobile'],
						'email' => $resultArr['Account.Email']
					);
					if ($resultArr['Account.Type'] == '个人客户')
						$add_data['type_id'] = 1;
					else
						$add_data['type_id'] = 2;

					return $this->account->api_add($add_data);
//					if ($result['res'] == 'fail') {
//						die(json_encode(array('res' => 'fail', 'msg' => 'apiSync error', 'info' => $result['msg'])));
//					} else
//						return $result;
				} else { //openapi抓取客户信息
					require_once('application/libraries/snoopy.php');
					require_once('application/libraries/oauth.php');
					$config = array(
						'prism' => array(
							'key' => 'AQAFY1',
							'secret' => '3JD5ZICVHKMAT5EOAY6A',
							'site' => 'https://openapi.ishopex.cn',
							'oauth' => 'https://oauth.ishopex.cn',
						),
					);
					//关于新用户中心调用查看的接口方法
					//测试环境可能有时间差！所以通过网络获取
					$snoopy = new Snoopy();
					$snoopy->submit("http://openapi.ishopex.cn/api/platform/timestamp", "");
					$time = $snoopy->results;
					$url = 'api/usercenter/ents/getinfo';
					$d["eid"] = $data;
					$prism = new oauth2($config['prism']);
					$r = $prism->request()->post($url, $d, $time);
					$res = $r->parsed();

					if ($res['status'] == 'success') {
						$account = $res['data'];
						$add_data = array(
							'name' => trim($account['ent_name']),
							'leader' => trim($account['contact']),
							'update_time' => trim($account['updated_at']),
							'account_shopexid' => trim($data),
							'create_time' => trim($account['created_at']),
							'telephone' => trim($account['mobile']),
						);
						if ($account['ent_type'] == 'company')
							$add_data['type_id'] = 2;
						else
							$add_data['type_id'] = 1;
						return $this->account->api_add($add_data);
					} else {
						$url = 'api/usercenter/passport/getinfo'; // 个人信息
						$d["uid"] = $data;
						$prism = new oauth2($config['prism']);
						$r = $prism->request()->post($url, $d, $time);
						$res = $r->parsed();

						if ($res['status'] == 'success') {
							$account = $res['data'];
							$name = empty($account['name']) ? $account['email'] : $account['name'];
							$add_data = array(
								'name' => $name,
								'update_time' => trim($account['updated_at']),
								'account_shopexid' => trim($account['uid']),
								'create_time' => trim($account['created_at']),
								'telephone' => trim($account['mobile']),
								'type_id' => 1 // 个人客户
							);
							return $this->account->api_add($add_data);
						} else
							return array('res' => 'fail', 'msg' => 'error apiSync');
					}
				}
				break;
			default:
				break;
		}
		return array('res' => 'fail', 'msg' => 'error apiSync');
	}

	/**
	 * openapi 接口
	 * @param string $url 接口地址
	 * @param array  $params 参数
	 * @return string
	 */
	public function getOpenApi($url, $params) {
		require_once('application/libraries/snoopy.php');
		require_once('application/libraries/oauth.php');
		$config = array(
			'prism' => array(
				'key' => 'AQAFY1',
				'secret' => '3JD5ZICVHKMAT5EOAY6A',
				'site' => 'https://openapi.ishopex.cn',
				'oauth' => 'https://oauth.ishopex.cn',
			),
		);
		$snoopy = new Snoopy();
		$snoopy->submit("http://openapi.ishopex.cn/api/platform/timestamp", "");
		$time = $snoopy->results;
		$prism = new oauth2($config['prism']);
		$r = $prism->request()->post($url, $params, $time);
		$res = $r->parsed();
		return $res;
	}

	//捆绑商品/商品
	public function dealDetailData($detail_data) {
		$result = array();
		foreach ($detail_data as $k => $v) {
			$v = trim($v);
			if ($k == 'goods_code' || $k == 'goods.code') {
				$res_goods = $this->db->get_where('tc_goods', array('goods_code' => $v))->row_array(); //商品信息
				if (empty($res_goods))
					return false; //检查商品是否存在
				$result['res_goods'] = $res_goods;
				if ($res_goods['goods_type'] == '1002') { //捆绑商品
					$res_bg = $this->db->get_where('tc_bgrelation', array('bgrelation_bundle_id' => $res_goods['goods_id']))->result_array();
					$result['res_bg'] = $res_bg;
					foreach ($res_bg as $bgk => $bgv) {
						$res_g = $this->db->get_where('tc_goods', array('goods_id' => $bgv['bgrelation_goods_id']))->row_array();
//						$result['res_bg'][$bgk]['goods_arr'] = $res_g;
						$res_gp = $this->db->get_where('tc_gprelation', array('gprelation_goods_id' => $res_g['goods_id']))->result_array();
						foreach ($res_gp as $gpk => $gkv) {
							$res_pb = $this->db->get_where('tc_product_basic', array('product_basic_id' => $gkv['gprelation_product_basic_id']))->row_array();
						}
						$result['res_bg'][$bgk] = $res_pb;
						$result['res_bg'][$bgk]['bg_arr'] = $bgv;
					}
				} else {
					$res_gpr = $this->db->get_where('tc_gprelation', array('gprelation_goods_id' => $res_goods['goods_id']))->result_array();
					foreach ($res_gpr as $gpk => $gkv) {
						$res_pb = $this->db->get_where('tc_product_basic', array('product_basic_id' => $gkv['gprelation_product_basic_id']))->row_array();
						$result['res_bg'][$gpk] = $res_pb;
						$result['res_bg'][$gpk]['res_gpr'] = $gkv;
					}
					//模板堂订单 原价就是 折后价
					$result['res_gp']['goods_price_cycle_price'] = $detail_data['goods_amount'];
					$result['res_gp']['goods_price_start_price'] = $detail_data['goods_amount'];
				}
				$result['res_goods']['goods_disc'] = $detail_data['goods_amount'];
			} elseif ($k == 'goods_price_code' || $k == 'goods_price.code') {
				if (empty($v))
					break;
				$res_gp = $this->db->get_where('tc_goods_price', array('goods_price_code' => $v))->row_array();
				if (!$res_gp)
					return false;
				$result['res_gp'] = $res_gp;
			} else {
				if (!empty($v))
					$result[$k] = $v;
			}
		}
		return $result;
	}

	/**
	 * 保存订单明细
	 * @param type $data 商品信息
	 * @param type $res_order_id 对应订单id
	 */
	public function saveOrderDetail($data, $res_order_id, $list = '', $return = false) {
		$save_data['goods_name'] = $data['res_goods']['goods_name']; //商品名称
		$save_data['goods_code'] = $data['res_goods']['goods_code']; //商品编号
		// 增加字段 编号 开通/结束时间
		if (isset($data['number']))  //明细编号
			$save_data['number'] = $data['number'];
		if (isset($data['open_startdate'])) // 开通时间
			$save_data['open_startdate'] = $data['open_startdate'];
		if (isset($data['open_enddate']))  // 结束时间
			$save_data['open_enddate'] = $data['open_enddate'];

		if ($data['res_goods']['goods_type'] == '1002') //捆绑商品
			$save_data['goods_primecost'] = $data['res_goods']['goods_price']; //商品原价
		else {
			$save_data['goods_primecost'] = $data['res_gp']['goods_price_start_price'] > 0 ? $data['res_gp']['goods_price_start_price'] : $data['res_gp']['goods_price_cycle_price'];
		}
		$save_data['goods_disc'] = $data['res_goods']['goods_disc']; //商品折后价
		if (!empty($data['note'])) {
			$save_data['note'] = $data['note'];
		}
		$this->load->model('www/order_d_model', 'order_d');
		$count = count($data['res_bg'])-1;//标记最后一个游标
		$sum   = 0;//记录累记总金额用于计算最后商品金额
		foreach ($data['res_bg'] as $k => $v) {
			$save_data['product_basic_name'] = $v['product_basic_name']; //基础产品名称
			$save_data['product_basic_code'] = $v['product_basic_code']; //基础产品编号
			$save_data['domain_cost'] = $v['product_basic_domain_cost']; //域名／备案成本
			$save_data['sms_cost'] = $v['product_basic_sms_cost']; //短信成本
			$save_data['idc_cost'] = $v['product_basic_idc_cost']; //IDC成本
			$save_data['order_id'] = $res_order_id; //订单id
			//代表已进行到最后一件商品
			if ($count == $k) {
				if ($data['res_goods']['goods_type'] == '1002') { //捆绑商品
					$rate = ($v['bg_arr']['bgrelation_group_price'] / $data['res_goods']['goods_price']); //比例
					$rate = round($rate, 2);
					$save_data['product_basic_primecost'] = $v['bg_arr']['bgrelation_group_price']; //基础产品原价
					$save_data['product_basic_disc'] = $save_data['goods_disc'] - $sum;//基础产品折后价
					$save_data['product_rate'] = $rate * 100; // 比例
				} else {
					$save_data['product_rate'] = $v['res_gpr']['gprelation_rate']; // 比例
					$save_data['product_basic_primecost'] = $save_data['goods_primecost'] * ($save_data['product_rate'] / 100); //基础产品原价
					$save_data['product_basic_disc'] = $save_data['goods_disc'] - $sum; //基础产品折后价
				}
			} else {
				if ($data['res_goods']['goods_type'] == '1002') { //捆绑商品
					$rate = ($v['bg_arr']['bgrelation_group_price'] / $data['res_goods']['goods_price']); //比例
					$rate = round($rate, 2);
					$save_data['product_basic_primecost'] = $v['bg_arr']['bgrelation_group_price']; //基础产品原价
					$save_data['product_basic_disc'] = $data['res_goods']['goods_disc'] * $rate; //基础产品折后价
					$save_data['product_rate'] = $rate * 100; // 比例
				} else {
					$save_data['product_rate'] = $v['res_gpr']['gprelation_rate']; // 比例
					$save_data['product_basic_primecost'] = $save_data['goods_primecost'] * ($save_data['product_rate'] / 100); //基础产品原价
					$save_data['product_basic_disc'] = $save_data['goods_disc'] * ($save_data['product_rate'] / 100); //基础产品折后价
				}
			}
			$sum += $save_data['product_basic_disc'];//统计每次商品总金额
			$save_data['list'] = $list;
			$res = $this->order_d->api_add($save_data, 'order_d');
			if($res['res'] != 'succ')
				return $res;
		}
		return $this->_return('saveOrderDetail succ', '', 'succ');
	}
    /**
     * saveInstrucItem (未完待完成商品与产品户号生成)
     * 指令明细数据处理
     * @param mixed $data 商品明细数据
     * @param mixed $res_instruction_id 指令主键ID
     * @param string $list 商品列号
     * @param mixed $return 是否返回查询数据
     * @access public
     * @return json or array
     */
	public function saveInstrucItem($data, $res_instruction_id, $list = '', $return = false) {
		$this->load->model('www/instruction_item_model', 'instru_item');
        $sequence_data = $data['sequence_data'];
		$save_data['goods_name'] = $data['res_goods']['goods_name']; //商品名称
		$save_data['goods_code'] = $data['res_goods']['goods_code']; //商品编号
        $goods_num = $sequence_data['goods']['object_sequence_sequence_code'] + 1;
		$save_data['goods_acct_code'] = 'goods' . $goods_num;//商品户号
		$save_data['type.name'] = $data['type.name']; //商品名称
		// 增加字段 编号 开通/结束时间
		if (isset($data['number']))  //明细编号
			$save_data['number'] = $data['number'];
		$count = count($data['res_bg'])-1;//标记最后一个游标
		$pro_num   = 0;//记录产品户号累计
		foreach ($data['res_bg'] as $k => $v) {
			$save_data['product_basic_name'] = $v['product_basic_name']; //基础产品名称
			$save_data['product_basic_code'] = $v['product_basic_code']; //基础产品编号
            $pro_num =  $sequence_data['products']['object_sequence_sequence_code'] + 1;
            $save_data['product_basic_acct_code'] = 'pro' . $pro_num;
            $save_data['instruction_id'] = $res_instruction_id; //指令单id
            $save_data['list'] = $list;
			$res = $this->instru_item->api_add($save_data, 'instru_item');
			if($res['res'] != 'succ')
				return $res;
		}
		return $this->_return('saveInstrucItem succ', '', 'succ');
	}

    /**
     * acctOpen 
     * 开户信息表与代办信息表处理
     * @param mixed $res_instruction_id 指令ID
     * @access public 
     * @return void json
     */
    public function acctOpen($res_instruction_id) {
        /**
         * 开始处理开户数据
         * 条件指令与指令明细插入成功
         * 1 开始生成开户数据
         * 2 生成开开户代办数据 
         */
        $this->load->model('www/acct_open_model', 'acct_open');
        return $this->acct_open->open_account($res_instruction_id);
    }
    
	//通过shopex_id获取用户中心的信息
	public function lee_getCustomerEmail($shopex_id) {
		require_once('application/libraries/snoopy.php');
		require_once('application/libraries/oauth.php');
		$config = array(
			'prism' => array(
				'key' => 'AQAFY1',
				'secret' => '3JD5ZICVHKMAT5EOAY6A',
				'site' => 'https://openapi.ishopex.cn',
				'oauth' => 'https://oauth.ishopex.cn',
			),
		);
		//关于新用户中心调用查看的接口方法
		//测试环境可能有时间差！所以通过网络获取
		$snoopy = new Snoopy();
		$snoopy->submit("http://openapi.ishopex.cn/api/platform/timestamp", "");
		$time = $snoopy->results;
		//$time = time();
		$url = 'api/usercenter/passport/getinfo';
		//echo date("Y-m-d H:i:s",$time);
		//$data["uid"] = '441111082500';
		$data["uid"] = $shopex_id;
		//$data["login_name"] = "564277291@qq.com22";
		$prism = new oauth2($config['prism']);
		$r = $prism->request()->post($url, $data, $time);
		$res = $r->parsed();
		//echo 111;
		//print_r($res);
		if ($res["status"] == "success") {
			//成功
			return $res["data"]['email'];
		} elseif ($res["status"] == "error") {
			//echo "<pre>";print_r($res);exit;
			switch ($res["code"]) {
				case "0011":
					return "没有匹配到账号";
					break;
				case "0001":
					return "参数不能为空";
					break;
				default:
					return $res["code"];
			}
		}
		return $res;
	}

	//保存数据至营收平台
	public function saveSaasOrder($data) {
		p($data);
		$params['action'] = 'add';
		$params['object'] = 'order';
		// $data['contract_no'] = isset($data['contract_no']) ? $data['contract_no'] : '';
		// //product_basic_servicecycle 服务周期

		$sql = 'select `goods_price_code` from `tc_goods_price` where `goods_price_goods_id` = (select `goods_id` from `tc_goods` where `goods_code` = "' . $data['goods_id'] . '")';
		$goods_price_code = $this->db->query($sql)->row_array();
		$save_data = array(
			'type.name' => '提货销售订单',
			'source.name' => '云生意',
			'create_time' => $data['created_at'],
			'amount' => $data['total_pay_fee'],
			'number' => $data['order_id'],
			//'finance.gonghao' => 's1861', //财务,戴汉君
			'finance.gonghao' => 's0674', //财务,戴汉君转岗换成司文欣
			'owner.gonghao' => 's0194', //销售,徐知月
			'review.gonghao' => 's0194', //主管,徐知月
			'account.account_shopexid' => $data['eid'],
			'agreement_no' => $data['contract_no'],
			'PassID' => $data['eid'],
			'detailed' => array(
				'order_d' => array(
					array(
						'goods.code' => $data['goods_id'],
						'goods_amount' => $data['total_pay_fee'],
						'goods_price_code' => $goods_price_code['goods_price_code'],
						'open_startdate' => $data['service_start_time'],
						'open_enddate' => $data['service_end_time'])
				)
			)
		);
		
		//cus lee 2014-06-13 根据不同情况设置销售人员以及业务主管 start
		//如果agent_id不为空，销售人员固定给王凯，主管给王文明
		if ($data['agent_id'] != "") {
			$save_data['owner.gonghao'] = "s1791"; //销售,王凯
			$save_data['review.gonghao'] = "s0031"; //销售,王文明
		}
		//cus lee 2014-06-13 根据不同情况设置销售人员以及业务主管 end
		$save_data = json_encode($save_data);
		return $this->api_add($save_data, $params['object']);
	}

	//保存预收款订单数据
	public function saveAdvanceOrder($data) {
		$params['action'] = 'add';
		$params['object'] = 'order';

		// p($data);exit;
		$save_data = array(
			'type.name' => '预收款订单',
			'create_time' => $data['create_time'],
			'amount' => $data['total_pay_fee'],
			'number' => $data['order_id'],
			'finance.gonghao' => 's0134', //财务,阎静
			'owner.gonghao' => 's0194', //销售,徐知月
			'review.gonghao' => 's0194', //主管,徐知月
			'account.account_shopexid' => $data['ent_id'],
			'agreement_no' => $data['days'],
			'PassID' => $data['ent_id']
				// 'detailed' => array(
				// 	'order_d' => array(
				// 		array('goods.code' => $data['goods_id'], 'goods_amount' => $data['total_pay_fee'])
				// 	)
				// )
		);
		$save_data = json_encode($save_data);
		return $this->add($save_data, $params['object']);
		// $res_order = $this->db->get_where('tc_order', array('order_number' => $data['order_id']))->row_array();
		// if (!$res_order)
		// $res = $this->add($save_data, $params['object']);
	}

	//账号验证
	public function userVerify($user, $pwd) {
		$user = trim($user);
		$pwd = trim($pwd);
		return $this->db->get_where('tc_user', array('user_login_name' => $user, 'user_password' => $pwd))->row_array();
	}

	/**
	 * 从产品中心抓取数据
	 * @param  [timestamp] $starttime 开始时间
	 * @param  [timestamp] $endtime   结束时间
	 * @return [array]                产品数据
	 */
	public function getGoods($starttime, $endtime) {
		$this->load->library('snoopy');
		$api_url = 'http://pro.shopex.cn/index.php/openapi/api/post';
		$params['method'] = 'product.getGoods';
		$params['app_type'] = 'post';
		$params['format'] = 'json';
		$params['version'] = '1.0';
		$params['call_times'] = 'time';
		$params['return'] = 'has';
		$get_goods_params = array(
			'starttime' => $starttime,
			'endtime' => $endtime,
		);
		$params = array_merge($params, $get_goods_params);
		
		$params['sign'] = $this->make_shopex_ac($params, 'procpbi');
		$this->snoopy->submit($api_url, $params);
		$res = $this->snoopy->results;
		$json_arr = json_decode($res, true);
		$goods_arr = $json_arr['info']['goods'];
		return $goods_arr;
	}

	// 抓取技术产品信息
	public function getTechProduct($tech_code) {
		$api_url = 'http://pro.shopex.cn/index.php/openapi/api/post';
		$params['method'] = 'product.getProduct';
		$params['app_type'] = 'post';
		$params['format'] = 'json';
		$params['version'] = '1.0';
		$params['call_times'] = 'time';
		$params['return'] = 'has';
		$get_product_params = array(
			'product_code' => $tech_code,
		);
		$params = array_merge($params, $get_product_params);
		$params['sign'] = $this->make_shopex_ac($params, 'procpbi');
		$this->snoopy->submit($api_url, $params);
		$res = $this->snoopy->results;
		$json_arr = json_decode($res, true);
		$tech_arr = $json_arr['info']['product'];
		return $tech_arr;
	}

	// 抓取基础产品信息
	public function getBasicProduct($suite_code) {
		$api_url = 'http://pro.shopex.cn/index.php/openapi/api/post';
		$params['method'] = 'product.getSuite';
		$params['app_type'] = 'post';
		$params['format'] = 'json';
		$params['version'] = '1.0';
		$params['call_times'] = 'time';
		$params['return'] = 'has';
		$get_product_params = array(
			'suite_code' => $suite_code,
		);
		$params = array_merge($params, $get_product_params);
		$params['sign'] = $this->make_shopex_ac($params, 'procpbi');
		$this->snoopy->submit($api_url, $params);
		$res = $this->snoopy->results;
		$json_arr = json_decode($res, true);
		$tech_arr = $json_arr['info']['suite'][0];
		return $tech_arr;
	}

	/**
	 * shopexid 验证
	 * @param type $post_params
	 * @param type $token
	 * @return type string
	 */
	private function make_shopex_ac($post_params, $token) {
		ksort($post_params);
		$str = '';
		foreach ($post_params as $key => $value) {
			if ($key != 'sign') {
				$str.=$value;
			}
		}
		return md5($str . $token);
	}

	/**
	 *
	 * @param type $data
	 * @param type $object
	 * @return msg
	 */
	public function update($data = '', $object = '') {
		$data = is_array($data) ? $data : json_decode($data, true);
		if (empty($data))
			return $this->apiLog('fail', 'update error data 数据有问题');

		$this->db->trans_begin(); //事务 Start
		if (!empty($object))
			$this->load->model('www/' . $object . '_model', $object); //载入相应对象 model

		switch ($object) {
			case 'order':
				if (!isset($data['number']) && empty($data['number'])) {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog('fail', 'error number 没有订单编号');
				}

				if ($data['type.name'] == '返点订单') {
					$data['number'] = 'ro' . $data['number'];
					unset($data['type.name']);
				}
				$where = array('order_number' => $data['number']);
				$res = $this->db->select('order_id')->get_where('tc_order', $where)->row_array();
				if (!$res) {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog('fail', 'error number 订单编号错误');
				}
				unset($data['number']);

				if (isset($data['detailed'])) { //明细
					$detailed = $data['detailed'];
					unset($data['detailed']);
				} else
					$detailed = array();

				$result = $this->$object->api_update($data, $where);
				if ($result['res'] == 'fail') {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog($result['res'], $result['msg']);
				} else {
					$res_id = $this->db->select('order_id')->get_where('tc_order', $where)->row_array();
					foreach ($detailed as $d_object => $d_data) {
						$this->load->model('www/' . $d_object . '_model', $d_object);
						foreach ($d_data as $k => $v) {
							if ($d_object == 'salespay') {
								$where = array('salespay_number' => $v['number']);
							} elseif ($d_object == 'rel_order_order') {
								$where = array('rel_order_order_my_order' => $res_id['order_id']);
							}
							$res_d = $this->$d_object->api_update($v, $where);
							if (isset($res_d['res']) && $res_d['res'] == 'fail') {
								return array('res' => 'fail', 'msg' => 'update failure detail ' . $d_object);
							}
						}
					}
				}
				break;
			case 'salespay':
				if (!isset($data['number']) && empty($data['number'])) {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog('fail', 'error number 没有入款项编号');
				}

				$where = array('salespay_number' => $data['number']);
				$res = $this->db->select('salespay_id')->get_where('tc_salespay', $where)->row_array();
				if (!$res) {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog('fail', 'error number 入款项编号错误');
				}
				unset($data['number']);

				$result = $this->$object->api_update($data, $where);
				if ($result['res'] == 'fail') {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog($result['res'], $result['msg']);
				}
				break;
			case 'refund':
				if (!isset($data['number']) && empty($data['number'])) {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog('fail', 'error number 没有退款编号');
				}

				$where = array('refund_number' => $data['number']);
				$res = $this->db->select('refund_id')->get_where('tc_refund', $where)->row_array();
				if (!$res) {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog('fail', 'error number 退款编号错误');
				}
				unset($data['number']);

				$result = $this->$object->api_update($data, $where);
				if ($result['res'] == 'fail') {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog($result['res'], $result['msg']);
				}
				break;
			case 'invoice':
				if (!isset($data['number']) && empty($data['number'])) {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog('fail', 'error number 没有收据编号');
				}

				$where = array('invoice_number' => $data['number']);
				$res = $this->db->select('invoice_id')->get_where('tc_invoice', $where)->row_array();
				if (!$res) {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog('fail', 'error number 收据编号错误');
				}
				unset($data['number']);

				$result = $this->$object->api_update($data, $where);
				if ($result['res'] == 'fail') {
					$this->db->trans_rollback();
					$this->db->close();
					return $this->apiLog($result['res'], $result['msg']);
				}
				break;
			default:
				$this->db->trans_rollback();
				$this->db->close();
				return $this->apiLog('fail', 'error object');
				break;
		}

		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$this->db->close();
			return $this->apiLog('fail', 'update error sql rollback');
		} else {
			$this->db->trans_commit();
			$this->db->close();
			return $this->apiLog('succ', 'update success');
		} //事务 End
	}

	public function getSaasOrder($data = '', $user = '') {
		$data = is_array($data) ? $data : json_decode($data, true);
		$diff = array_diff(array('start_time', 'end_time', 'perNumber', 'page'), array_keys($data));
		if ($diff)
			return $this->apiLog('fail', '缺少必填项'); //检查数据完整性

		if ($data['perNumber'] > 100)
			$data['perNumber'] = 100;
		$return = array();
		//总数
		$where = "`type_id` = 13 AND `order_create_time` BETWEEN '" . $data['start_time'] . "' AND '" . $data['end_time'] . "' AND order_source = 1002";
		$this->db->select('count(order_id) as number');
		$this->db->from('tc_order')->where($where);
		$res_num = $this->db->get()->row_array();
//		die($this->db->last_query());
//		p($res_num);
//		exit;

		$where = "`type_id` = 13 AND `order_create_time` BETWEEN '" . $data['start_time'] . "' AND '" . $data['end_time'] . "' AND order_source = 1002";
		$this->db->select('order_id,order_number,order_amount,order_create_time,order_agreement_no,order_owner,order_finance,order_review,order_agreement_name');
		$this->db->from('tc_order')->where($where)->limit($data['perNumber'], $data['perNumber'] * ($data['page'] - 1))->order_by('order_create_time', 'desc');
		$res_order = $this->db->get()->result_array();

//		p($res_order);exit;
//		die($this->db->last_query());
		foreach ($res_order as $k => $v) {
			unset($res_order[$k]['order_id']);
			if (!empty($v['order_owner'])) {
				$owner = $this->db->select('user_gonghao')->get_where('tc_user', array('user_id' => $v['order_owner']))->row_array();
				$res_order[$k]['order_owner'] = $owner['user_gonghao'];
			}
			if (!empty($v['order_finance'])) {
				$owner = $this->db->select('user_gonghao')->get_where('tc_user', array('user_id' => $v['order_finance']))->row_array();
				$res_order[$k]['order_finance'] = $owner['user_gonghao'];
			}
			if (!empty($v['order_review'])) {
				$owner = $this->db->select('user_gonghao')->get_where('tc_user', array('user_id' => $v['order_review']))->row_array();
				$res_order[$k]['order_review'] = $owner['user_gonghao'];
			}
			$res_order_d = $this->db->select('order_d_goods_code,order_d_product_basic_disc')->get_where('tc_order_d', array('order_d_order_id' => $v['order_id']))->result_array();
			$res_order[$k]['order_d'] = $res_order_d;
		}
//		$res_order['rel_all'] = $res_num['number'];
//		$res_order['perNumber'] = $data['perNumber'];
//		$res_order['page'] = $data['page'];
//		p($res_order);
//		exit;
//		$return = $res_order;
		return json_encode(array('res' => 'succ', 'order' => $res_order, 'rel_all' => $res_num['number'], 'perNumber' => $data['perNumber'], 'page' => $data['page']));
	}

	public function saveDepositOrder($data = '') {
		$params['action'] = 'add';
		$params['object'] = 'order';

//		$sql = 'select `goods_price_code` from `tc_goods_price` where `goods_price_goods_id` = (select `goods_id` from `tc_goods` where `goods_code` = "' . $data['goods_id'] . '")';
//		$goods_price_code = $this->db->query($sql)->row_array();

		if ($data['method'] == 'alipay') {
			$save_data = array(
				'type.name' => '预收款订单',
				'number' => $data['recharge_no'],
				'account.account_shopexid' => $data['ent_id'],
				'source.name' => '云生意',
				'deposit_paytype.name' => '预收款',
				'amount' => $data['amount'], // 金额
				'create_time' => $data['create_time'],
				'detailed' => array(
					'salespay' => array(
						array(
							'pay_date' => $data['finish_time'], //时间
							// 'sp_date' => $data['finish_time'], //确认到账时间
							'create_time' => $data['create_time'],
							'pay_amount' => $data['amount'], //金额
							'pay_note' => $data['remark'], //备注
							'status.name' => '未确认到帐', //到账状态
							"money_type.name" => '入账记录', //款项类型 (入账记录/出账记录)
							'my_alipay_account.name' => 'alipay_f@shopex.cn', //支付宝账号（固定）
							'pay_method' => 1002, //支付宝
							'alipay_order' => $data['trade_no'] //交易号
						)
					)
				)
			);
		} else {
			$save_data = array(
				'type.name' => '预收款订单',
				'number' => $data['recharge_no'],
				'account.account_shopexid' => $data['ent_id'],
				'source.name' => '云生意',
				'deposit_paytype.name' => '预收款',
				'amount' => $data['amount'], // 金额
				'detailed' => array(
					'salespay' => array(
						array(
							'pay_date' => $data['finish_time'], //时间
							'sp_date' => $data['finish_time'], //确认到账时间
							'create_time' => $data['create_time'], //确认到账时间
							'pay_amount' => $data['amount'],
							'pay_note' => $data['remark'],
							'pay_method' => 1001, //汇款
							'alipay_order' => $data['trade_no']
						)
					)
				)
			);
		}
		$save_data = json_encode($save_data);
		return $this->api_add($save_data, $params['object']);
	}

//	public function delOrder($order) {
//		$res = $this->db->select('order_id')->get_where('tc_order', array('order_number' => $order))->row_array();
//		if ($res) {
//			$this->db->delete('tc_salespay', array('salespay_order_id' => $res['order_id']));
//			$this->db->delete('tc_rebate', array('rebate_order_id' => $res['order_id']));
//			$this->db->delete('tc_invoice', array('invoice_order_id' => $res['order_id']));
//			$this->db->delete('tc_order_d', array('order_d_order_id' => $res['order_id']));
//			$this->db->delete('tc_refund', array('refund_order_id' => $res['order_id']));
//			$this->db->delete('tc_cancel', array('cancel_order_id' => $res['order_id']));
//			$this->db->delete('tc_rel_order_order', array('rel_order_order_my_order' => $res['order_id']));
//			$this->db->delete('tc_order', array('order_id' => $res['order_id']));
//			return true;
//		} else
//			return false;
//	}
	//cus lee 根据订单ID同步CRM订单接口 2014-05-25
	public function order_crm_add($order_id = "") {
		$this->load->model('www/system_log_model', 'system_log');
		$this->load->model('www/order_model', 'order');
		$this->load->model('www/applyopen_model', 'applyopen');
		$this->db->query('SET NAMES UTF8');
		if ($order_id == "") {
			if (isset($_GET['order_id'])) {
				$order_id = $_GET['order_id'];
			} else {
				//写日志
				$log_data = array(
					'system_log_addtime' => date('Y-m-d H:i:s'),
					'system_log_user_id' => $this->session->userdata('user_id'),
					'system_log_operation' => '订单同步CRM',
					'system_log_note' => "失败：不能没有参数。",
					'system_log_module' => 'order',
					'system_log_module_id' => ""
				);
				$this->system_log->add($log_data);
				return "失败：不能没有参数。";
			}
		}
		//echo "导入 CRM";
		//查询出这个订单的内容
		$order_data = $this->order->id_aGetInfo($order_id);
		//这里要判断下订单的创建人，只有合同同步账号过来的订单才做同步
		if ($order_data['order_create_user_id'] != 1000019) {
			//echo "该订单不是从合同过来的订单，不能同步到CRM";
			//写日志
			$log_data = array(
				'system_log_addtime' => date('Y-m-d H:i:s'),
				'system_log_user_id' => $this->session->userdata('user_id'),
				'system_log_operation' => '订单同步CRM',
				'system_log_note' => "该订单不是从合同过来的订单，不能同步到CRM",
				'system_log_module' => 'order',
				'system_log_module_id' => $order_id
			);
			$this->system_log->add($log_data);
			return "该订单不是从合同过来的订单，不能同步到CRM";
		}
		//判断这个订单是否同步过CRM
		if ($order_data['order_is_crm'] == 1002) {
			//echo "该订单已经同步过CRM,不能再同步了";
			//写日志
			$log_data = array(
				'system_log_addtime' => date('Y-m-d H:i:s'),
				'system_log_user_id' => $this->session->userdata('user_id'),
				'system_log_operation' => '订单同步CRM',
				'system_log_note' => "该订单已经同步过CRM,不能再同步了",
				'system_log_module' => 'order',
				'system_log_module_id' => $order_id
			);
			$this->system_log->add($log_data);
			return "该订单已经同步过CRM,不能再同步了";
		}
		//查询这个订单的部门是否有对应CRM部门编号
		if (isset($order_data['order_department_arr']['department_crm_code'])) {
			if ($order_data['order_department_arr']['department_crm_code'] == "") {
				//echo $order_data['order_department_arr']['department_name']."该部门没有对应CRM部门编号";
				//写日志
				$log_data = array(
					'system_log_addtime' => date('Y-m-d H:i:s'),
					'system_log_user_id' => $this->session->userdata('user_id'),
					'system_log_operation' => '订单同步CRM',
					'system_log_note' => $order_data['order_department_arr']['department_name'] . "该部门没有对应CRM部门编号",
					'system_log_module' => 'order',
					'system_log_module_id' => $order_id
				);
				$this->system_log->add($log_data);
				return $order_data['order_department_arr']['department_name'] . "该部门没有对应CRM部门编号";
			}
		}
		//exit;
		//这里分析下订单是否有明细没有开通过
		foreach ($order_data['detailed']['order_d_arr'] as $k => $v) {
			if ($v['order_d_product_basic_style'] != 1003) {
				//echo "改订单存在未开通信息，只有全部开通后才能同步给CRM";
				//写日志
				$log_data = array(
					'system_log_addtime' => date('Y-m-d H:i:s'),
					'system_log_user_id' => $this->session->userdata('user_id'),
					'system_log_operation' => '订单同步CRM',
					'system_log_note' => "改订单存在未开通信息，只有全部开通后才能同步给CRM",
					'system_log_module' => 'order',
					'system_log_module_id' => $order_id
				);
				$this->system_log->add($log_data);
				return "改订单存在未开通信息，只有全部开通后才能同步给CRM";
			}
			//处理下返回参数，改造成文本类容
			//p($v);
			$applyopen_data = $this->applyopen->id_aGetInfo($v['order_d_id']);
			$applyopen['open'] = json_decode($applyopen_data['applyopen_open_info'], true);
			$applyopen['return'] = json_decode($applyopen_data['applyopen_return_info'], true);
			//p($applyopen);

			if (isset($applyopen['return'])) {
				$order_data['detailed']['order_d_arr'][$k]['params_return'] = "";
				foreach ($applyopen['return'] as $kk => $vv) {
					$order_data['detailed']['order_d_arr'][$k]['params_return'] .= $v['params_arr']['return'][$kk]['params_name'] . ':' . $vv . '  ';
				}
			} else {
				$order_data['detailed']['order_d_arr'][$k]['params_return'] = '该产品没有返回参数';
			}
			//echo $params_return.'<br/>';
		}
		//p($order_data);
		//exit;
		//同步给crm订单信息
		//$lee_url = '192.168.0.18:8088'; //正式环境
		$lee_url = '192.168.35.14'; //CRM升级负载均衡换IP
		//$lee_url = '192.168.35.218:8088'; //测试环境
		$lee_orgcode = 'ShopEx'; //单位名称
		$wsURL = "http://" . $lee_url . "/webservice/service.php?orgcode=" . $lee_orgcode . "&class=WS_System&wsdl";
		//$params = array("admin", "hql444h3l");
		$params = array("npbisycn", "lee123456");
		$header = null;
		$client = new SoapClient($wsURL);
		$session = $client->__soapCall("login", $params, array(), $header);
		$wsURL = "http://" . $lee_url . "/webservice/service.php?orgcode=" . $lee_orgcode . "&class=WS_EAI&wsdl";
		$queryXML = '
		<Interface type="ADD" model="OBJECT" value="Contract">
			<Row IdentifyCode="">
		';
		$queryXML .= '<Contract.Name>' . $order_data['order_number'] . '</Contract.Name>'; //主题
		$queryXML .= '<Contract.StartDate>' . date("Y-m-d", strtotime($order_data['order_create_time'])) . '</Contract.StartDate>'; //订单日期
		$queryXML .= '<Contract.Amount>' . $order_data['order_amount'] . '</Contract.Amount>'; //订单金额
		$queryXML .= '<Contract.Owner.JobNumber>' . $order_data['order_owner_arr']['user_gonghao'] . '</Contract.Owner.JobNumber>'; //业务员.名称
		$queryXML .= '<Contract.Account.customerID>' . $order_data['order_account_arr']['account_account_shopexid'] . '</Contract.Account.customerID>'; //客户.商业ID
		//$queryXML .= '<Contract.Account.customerID>88140401177570</Contract.Account.customerID>';//客户.商业ID
		$queryXML .= '<Contract.BookID>1</Contract.BookID>'; //价格表
		if ($order_data['order_if_renew'] == 1001) {
			$queryXML .= '<Contract.sfxfdd>0</Contract.sfxfdd>'; //是否续费订单 0否1是
		} else {
			$queryXML .= '<Contract.sfxfdd>1</Contract.sfxfdd>'; //是否续费订单 0否1是
		}
		$queryXML .= '<Contract.Type.Name>订单</Contract.Type.Name>'; //订单类型
		$queryXML .= '<Contract.Department.Code>' . $order_data['order_department_arr']['department_crm_code'] . '</Contract.Department.Code>'; //部门名称
		//添加明细
		$queryXML .= '<detail>';
		//循环产品明细
		foreach ($order_data['detailed']['order_d_arr'] as $v) {
			//$queryXML .= '<row identifycode="">';
			$queryXML .= '<row identifycode="">';
			$queryXML .= '<Contractitem.ProductID>5447</Contractitem.ProductID>'; //产品 这里默认给一个【新营收同步订单】
			$queryXML .= '<Contractitem.Quantity>1</Contractitem.Quantity>'; //产品数量
			$queryXML .= '<Contractitem.Amount>' . $v['order_d_product_basic_disc'] . '</Contractitem.Amount>'; //成交价
			$queryXML .= '<Contractitem.PName>' . $v['order_d_product_basic_name'] . '</Contractitem.PName>'; //产品名称
			$queryXML .= '<Contractitem.Value>' . $v['order_d_product_basic_disc'] . '</Contractitem.Value>'; //单价
			$queryXML .= '<Contractitem.Number>' . $v['order_d_product_basic_code'] . '</Contractitem.Number>'; //产品编号
			$queryXML .= '<Contractitem.Open>1002</Contractitem.Open>'; //是否已开通 默认全部是已开通
			$queryXML .= '<Contractitem.Notes>' . $v['params_return'] . '</Contractitem.Notes>'; //开通信息
			$queryXML .= '</row>';
		}
		$queryXML .= '</detail>';
		$queryXML .= '
			</Row>
		</Interface>
		';
		$params = array($queryXML);
		//p($params);//exit;
		$header = new SoapHeader("http://" . $_SERVER['HTTP_HOST'], "PHPSESSID", $session);
		$client = new SoapClient($wsURL);
		$resultXML = $client->__soapCall("process", $params, array(), $header);
		//echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
		//echo $resultXML;
		//echo "<br>";
		$result_arr = (array) simplexml_load_string($resultXML);
		$row_arr = (array) $result_arr['Row'];
		if ($row_arr['@attributes']['success'] == 'false') {
			//echo "同步失败：".$row_arr['@attributes']['message'];
			//写日志
			$log_data = array(
				'system_log_addtime' => date('Y-m-d H:i:s'),
				'system_log_user_id' => $this->session->userdata('user_id'),
				'system_log_operation' => '订单同步CRM',
				'system_log_note' => "同步失败：" . $row_arr['@attributes']['message'],
				'system_log_module' => 'order',
				'system_log_module_id' => $order_id
			);
			$this->system_log->add($log_data);
			return "同步失败：" . $row_arr['@attributes']['message'];
		} else
		if ($row_arr['@attributes']['success'] == 'true') {
			//echo "同步成功";
			//写日志
			$log_data = array(
				'system_log_addtime' => date('Y-m-d H:i:s'),
				'system_log_user_id' => $this->session->userdata('user_id'),
				'system_log_operation' => '订单同步CRM',
				'system_log_note' => "同步成功",
				'system_log_module' => 'order',
				'system_log_module_id' => $order_id
			);
			$this->system_log->add($log_data);
			//当同步成功的时候，给这个订单的是否同步过CRM改成是
			$where = array(
				'order_is_crm' => '1002',
			);
			$this->order->update($where, $order_id);
			return "同步成功";
		} else {
			//echo "返回结果解析不出来，未知问题！";
			//写日志
			$log_data = array(
				'system_log_addtime' => date('Y-m-d H:i:s'),
				'system_log_user_id' => $this->session->userdata('user_id'),
				'system_log_operation' => '订单同步CRM',
				'system_log_note' => "返回结果解析不出来，未知问题！",
				'system_log_module' => 'order',
				'system_log_module_id' => $order_id
			);
			$this->system_log->add($log_data);
			return "返回结果解析不出来，未知问题！";
		}
	}

	/**
	 * 解析 where 条件
	 * @param type $data json/array
	 * * @param type $obj string
	 * @return array
	 */
	public function whereSql($data = '', $obj = '') {
		if (empty($data) || empty($obj))
			return false;

		$data['where_all'] = is_array($data) ? $data : json_decode($data, true);
		$objDetail = $this->objDetail($data['obj']);
		foreach ($data['where_all'] as $rel => $cond) {
			$cond['value'] = (isset($cond['value']) || (!empty($cond['value']))) ? trim($cond['value']) : ''; // 过滤空格
			if (strpos($cond['attr'], '.')) {
				list($fields, $attr) = explode('.', $cond['attr']);
				$this_fields = $data['obj'] . '_' . $fields;
				$q_obj = $objDetail['obj_attr'][$fields]; //引用对象
				$q_obj_fields = $q_obj . '_' . $attr; //引用对象字段

				switch ($q_obj) {
					case 'enum': //枚举
						if (strpos($cond['value'], ',')) {
							$where = array('attr_id' => $objDetail['obj_attr_id'][$fields]);
							$res_enum = $this->db->select($q_obj . '_id')->where($where)->where_in($q_obj_fields, explode(',', $cond['value']))->get('tc_' . $q_obj)->result_array();
						} else {
							$where = array('attr_id' => $objDetail['obj_attr_id'][$fields]);
							$res_enum = $this->db->select('enum_key')->like($q_obj_fields, $cond['value'], 'both')->where($where)->get('dd_enum')->result_array();
						}

						if ($res_enum) {
							$cond['attr'] = $this_fields;
							if (count($res_enum) > 1) {
								$cond['value'] = '';
								foreach ($res_enum as $ev) {
									$cond['value'] .= ',' . $ev['enum_key'];
								}
								$cond['value'] = ltrim($cond['value'], ',');
							} else
								$cond['value'] = $res_enum[0]['enum_key'];
						} else
							return $this->apiLog(false, __FUNCTION__ . ' error enum ' . $fields);
						break;
					default: //引用
						if (strpos($cond['value'], ',')) {
							$res_q_id = $this->db->select($q_obj . '_id')->where_in($q_obj_fields, explode(',', $cond['value']))->get('tc_' . $q_obj)->result_array();
						} else {
//							$where = array($q_obj_fields => $cond['value']);
//							$res_q_id = $this->db->select($q_obj . '_id')->where($where)->get('tc_' . $q_obj)->result_array(); //精确查询
							$res_q_id = $this->db->select($q_obj . '_id')->like($q_obj_fields, $cond['value'], 'both')->get('tc_' . $q_obj)->result_array(); //模糊查询
						}

						if ($res_q_id) {
							$cond['attr'] = $this_fields;
							if (count($res_q_id) > 1) {
								$cond['value'] = '';
								foreach ($res_q_id as $qv) {
									$cond['value'] .= ',' . $qv[$q_obj . '_id'];
								}
								$cond['value'] = ltrim($cond['value'], ',');
								$cond['action'] = 'INC';
							} else {
								$cond['value'] = $res_q_id[0][$q_obj . '_id'];
//								$cond['action'] = '=';
							}
						} else
							return $this->apiLog(false, __FUNCTION__ . ' error quote');
						break;
				}
			} else
				$cond['attr'] = ($cond['attr'] == 'type_id') ? 'type_id' : $data['obj'] . '_' . $cond['attr'];

			switch (strtoupper($cond['action'])) {
				case 'NULL'; //为空 同is null
					$$rel = $cond['attr'] . ' IS NULL OR ' . $cond['attr'] . ' = ""';
					break;
				case 'NOT_NULL': //不为空 同is not null
					$$rel = $cond['attr'] . ' IS NOT NULL';
					break;
				case 'NOT_EQUAL': //不等于 同<>
					$$rel = $cond['attr'] . ' != \'' . $cond['value'] . '\'';
					break;
				case 'GT': //大于 同>
					$$rel = $cond['attr'] . ' > \'' . $cond['value'] . '\'';
					break;
				case 'GT_EQUAL': //大于等于 同>=
					$$rel = $condition['attr'] . ' >= \'' . $cond['value'] . '\'';
					break;
				case 'LE': //小于 同<
					$$rel = $cond['attr'] . ' < \'' . $cond['value'] . '\'';
					break;
				case 'LE_EQUAL': //小于等于 同<=
					$$rel = $cond['attr'] . ' <= \'' . $cond['value'] . '\'';
					break;
				case 'INC': //涵盖 同in
					$cond['value'] = str_replace(',', '\',\'', $cond['value']);
					$$rel = $cond['attr'] . ' IN(\'' . $cond['value'] . '\')';
					break;
				case 'NOT_INC': //不涵盖 同not in
					$cond['value'] = str_replace(',', '\',\'', $cond['value']);
					$$rel = $cond['attr'] . ' NOT IN(\'' . $cond['value'] . '\')';
					break;
				case 'RANGE': //区间 同>= and <=
					$$rel = $cond['attr'] . ' >= \'' . $cond['value1'] . '\' AND ' . $cond['attr'] . ' <= \'' . $cond['value2'] . '\'';
					break;
				case 'LIKE':
					$$rel = $cond['attr'] . ' LIKE "%' . $cond['value'] . '%"';
					break;
				case 'NOT_LIKE': //不包含 同not like
					$$rel = $cond['attr'] . ' NOT LIKE "%' . $cond['value'] . '%"';
					break;
				case 'BEFORE': //早于，用于日期时间类型
					$$rel = $cond['attr'] . ' < "' . $cond['value'] . '"';
					break;
				case 'AFTER': //晚于，用于日期时间类型
					$$rel = $cond['attr'] . ' > "' . $cond['value'] . '"';
					break;
				case 'RECENT': //最近，用于日期时间类型
					$$rel = $cond['attr'] . ' >= "' . date("Y-m-d H:i:s", strtotime("-$cond[value1] $cond[value2]")) . '"';
					break;
				case 'FUTURE': //未来，用于日期时间类型
					$$rel = $cond['attr'] . ' >= "' . date("Y-m-d H:i:s", strtotime("+$cond[value1] $cond[value2]")) . '"';
					break;
				case 'BELONGTO':
					if ($q_obj == 'department') {
						$cond['value'] = str_pad($cond['value'], 4, "0", STR_PAD_LEFT);
						$department_id = $this->db->select($q_obj . '_id')->like('department_treepath', $cond['value'], 'both')->get('tc_' . $q_obj)->result_array();
						if ($department_id) {
							$d_arr = array();
							foreach ($department_id as $dv) {
								$d_arr[] = $dv['department_id'];
							}
							$cond['value'] = implode(',', $d_arr);
						} else
							return $this->apiLog(false, __FUNCTION__ . ' error ' . $department_id);
						$$rel = $cond['attr'] . ' IN(' . $cond['value'] . ')';
					}
					break;
				default:
					$$rel = $cond['attr'] . ' = \'' . $cond['value'] . '\'';
					break;
			}
			$data['rel_all'] = str_replace($rel, $$rel, $data['rel_all']);
		}
		return $data['rel_all'];
	}

	public function api_save($data = '', $object = '', $user = '') {
		$data = is_array($data) ? $data : json_decode($data, true);
		if (empty($data)) {
			return $this->apiLog('fail', 'error data 数据有问题');
		}
		//p($user);
		//exit;
		// 调用相应 model app_add
		$this->db->trans_begin(); //事务 Start
		$this->load->model('www/' . $object . '_model', $object);
		$data['user'] = $user;
		$result = $this->$object->api_save($data);
		if (!isset($result)) {
			$this->db->trans_rollback();
			$this->db->close();
			return $this->apiLog('fail', 'error none return');
		} elseif ($result['res'] == 'fail') {
			$this->db->trans_rollback();
			$this->db->close();
			return $this->apiLog('fail', $result['msg']);
		}
		//p($result);
		// 事务 End
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$this->db->close();
			return $this->apiLog('fail', 'add data error sql rollback');
		} else {
			$this->db->trans_commit();
			$this->db->close();
			if (isset($info) && !empty($info))
				return $this->apiLog('succ', $result['msg'], $info);
			else
				return $this->apiLog('succ', 'add success');
		}
	}

	// 2014-07-17 api start
	/**
	 * 订单删除（删除关联信息）
	 * @param int $order_id
	 */
	public function delOrder($order_id) {
		$insert['order_id'] = $order_id;
		//款项
		$res = $this->db->get_where('tc_salespay', array('salespay_order_id' => $order_id))->result_array();
		$insert['salespay'] = isset($res) ? json_encode($res) : '';
		$this->db->delete('tc_salespay', array('salespay_order_id' => $order_id));
		//返点
		$res = $this->db->get_where('tc_rebate', array('rebate_order_id' => $order_id))->result_array();
		$insert['rebate'] = isset($res) ? json_encode($res) : '';
		$this->db->delete('tc_rebate', array('rebate_order_id' => $order_id));
		//票据
		$res = $this->db->get_where('tc_invoice', array('invoice_order_id' => $order_id))->result_array();
		$insert['invoice'] = isset($res) ? json_encode($res) : '';
		$this->db->delete('tc_invoice', array('invoice_order_id' => $order_id));
		//明细
		$res = $this->db->get_where('tc_order_d', array('order_d_order_id' => $order_id))->result_array();
		$insert['order_d'] = isset($res) ? json_encode($res) : '';
		$this->db->delete('tc_order_d', array('order_d_order_id' => $order_id));
		//退款
		$res = $this->db->get_where('tc_refund', array('refund_order_id' => $order_id))->result_array();
		$insert['refund'] = isset($res) ? json_encode($res) : '';
		$this->db->delete('tc_refund', array('refund_order_id' => $order_id));
		//关联
		$res = $this->db->get_where('tc_rel_order_order', array('rel_order_order_my_order' => $order_id))->result_array();
		$insert['rel_order_order'] = isset($res) ? json_encode($res) : '';
		$this->db->delete('tc_rel_order_order', array('rel_order_order_my_order' => $order_id));
		//订单
		$res = $this->db->get_where('tc_order', array('order_id' => $order_id))->result_array();
		$insert['order'] = isset($res) ? json_encode($res) : '';

		$this->db->delete('tc_order', array('order_id' => $order_id));
		$this->db->insert('tc_api_del_bak', $insert);
	}

	/**
	 * 解析对象
	 * @param string $object
	 * @return array
	 */
	private function parseObj($object) {
//		$sql = 'SELECT attr_id,attr_name,attr_type, attr_field_name, attr_quote_id FROM dd_attribute WHERE attr_obj_id = (SELECT obj_id FROM dd_object WHERE obj_name = \'' . $object . '\')';
		$sql = "
SELECT ab.attr_id, ab.attr_name,
CASE ab.attr_type
WHEN 5 THEN 'integer'
WHEN 6 THEN 'integer'
WHEN 10 THEN 'integer'
WHEN 7 THEN 'float'
WHEN 8 THEN 'string'
WHEN 9 THEN 'string'
WHEN 11 THEN 'string'
WHEN 12 THEN 'string'
WHEN 13 THEN 'string'
WHEN 17 THEN 'string'
WHEN 18 THEN 'string'
WHEN 20 THEN 'string'
WHEN 21 THEN 'string'
WHEN 22 THEN 'string'
WHEN 14 THEN 'enum'
WHEN 15 THEN 'enum'
WHEN 16 THEN 'enum'
WHEN 19 THEN 'quote'
WHEN 23 THEN 'quote'
END AS attr_type,
ab.attr_field_name, ab.attr_quote_id
FROM dd_attribute AS ab LEFT JOIN dd_object AS o ON o.obj_id = ab.attr_obj_id
WHERE o.obj_name = '" . $object . "'";
		$res_obj = $this->db->query($sql)->result_array();

		//整理数据类型
		$obj = array();
		foreach ($res_obj as $k => $v) {
			switch ($v['attr_type']) {
				case 'integer': //数值类型
					$obj[$v['attr_name']]['type'] = 'integer';
					$obj[$v['attr_name']]['attr_id'] = $v['attr_id'];
					break;
				case 'float': //浮点类型
					$obj[$v['attr_name']]['type'] = 'float';
					$obj[$v['attr_name']]['attr_id'] = $v['attr_id'];
					break;
				case 'string': //字符串类型
					$obj[$v['attr_name']]['type'] = 'string';
					$obj[$v['attr_name']]['attr_id'] = $v['attr_id'];
					break;
				case 'enum': //枚举类型
					$obj[$v['attr_name']]['type'] = 'enum';
					$obj[$v['attr_name']]['attr_id'] = $v['attr_id'];
					break;
				case 'quote': //引用类型
					$obj[$v['attr_name']]['type'] = 'quote';
					$obj[$v['attr_name']]['attr_id'] = $v['attr_id'];
					$obj[$v['attr_name']]['attr_quote_id'] = $v['attr_quote_id'];
					break;
			}
		}
		return $obj;
	}

	/**
	 * 接口数据解析
	 * @param array $data 数据
	 * @param string $object 对象
	 * @return array
	 */
	public function parseData($data, $object) {
		$obj = $this->parseObj($object);
		//过滤数据
		foreach ($data as $k => $v) {
			$v = trim($v, ' ');
			if ($v == '' or $v == null) { //删除空元素
				unset($data[$k]);
				continue;
			}
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				if (isset($obj[$fields]['type'])) {
					switch ($obj[$fields]['type']) {
						case 'enum':
							//fields[attr_id] => enum_name
							$enum_data[$fields][$obj[$fields]['attr_id']] = $v;
							break;
						case 'quote':
							//quote_obj_id[quote_fields] => value
							$quote_data[$obj[$fields]['attr_quote_id']][$attr][] = $v;
							break;
					}
				}
			} else {
				if (isset($obj[$k]['type'])) {
					switch ($obj[$k]['type']) {
						case 'integer':
							if (!settype($v, 'integer'))
								return $this->_return('数据类型有误', $k . '=>' . $v);
							break;
						case 'float':
							if (!settype($v, 'float'))
								return $this->_return('数据类型有误', $k . '=>' . $v);
							break;
						case 'string':
							if (!settype($v, 'string'))
								return $this->_return('数据类型有误', $k . '=>' . $v);
							break;
					}
					$data[$k] = $v;
				}
			}
		}
		//枚举类型数据
		if (isset($enum_data)) {
			$res_enum = array();
			foreach ($enum_data as $k => $v) {
				$this->db->select('enum_key')->from('dd_enum');
				foreach ($v as $kk => $vv) {
					$this->db->where('attr_id', $kk);
					$this->db->where('enum_name', $vv);
					$res = $this->db->get()->row_array();
					if (!$res)
						return $this->_return('枚举数据错误', $k . '=>' . $vv);
					else
						$res_enum[$k] = $res['enum_key'];
				}
			}
		}

		//引用类型数据
		if (isset($quote_data)) {
			$res_quote = array();
			foreach ($quote_data as $k => $v) {
				$q_obj = $this->db->select('obj_id, obj_name')->get_where('dd_object', array('obj_id' => $k))->row_array();
				$fields = array_keys($v);
				$this->db->select()->from('tc_' . $q_obj['obj_name']);
				foreach ($v as $kk => $vv) {
					$this->db->or_where_in($q_obj['obj_name'] . '_' . $kk, $vv);
				}
				$res = $this->db->get()->result_array();

				foreach ($fields as $fv) { //array('gonghao','login_name')
					$res_quote[$fv]['object'] = $q_obj['obj_name'];
					foreach ($res as $rk => $rv) { //array(array('user_id'=>'xxx'……),array(……))
						$res_quote[$fv][$rv[$q_obj['obj_name'] . '_' . $fv]] = $rv[$q_obj['obj_name'] . '_id'];
					}
				}
			}
		}

		//数据整理
		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				if (isset($obj[$fields]['type'])) {
					switch ($obj[$fields]['type']) {
						case 'enum':
							if (isset($res_enum[$fields])) {
								$data[$object . '_' . $fields] = $res_enum[$fields];
							}
							unset($data[$k]);
							break;
						case 'quote':
							if($attr == 'gonghao') $v = strtolower($v); //修正工号s大小写问题
							//如果引用内容不存在
							if (!isset($res_quote[$attr][$v])) {
								//抓取数据
								$res_sync = $this->apiSync($res_quote[$attr]['object'], $v);
								if (!isset($res_sync['res']) or $res_sync['res'] != 'succ')
									return $this->_return('引用类型错误', $k . '=>' . $v);
								$data[$object . '_' . $fields] = $res_sync['info'][$res_quote[$attr]['object'] . '_id'];
							} else
								$data[$object . '_' . $fields] = $res_quote[$attr][$v];
							unset($data[$k]);
							break;
					}
				}
			}else {
				$data[$object . '_' . $k] = $v;
				unset($data[$k]);
			}
		}
		return $this->_return('parseData succ', $data, 'succ');
	}

	/**
	 * 直接输出json信息，用于输出错误信息
	 * @param string $msg
	 */
	public function _msg($msg = 'error') {
		die(json_encode(array('res' => 'fail', 'msg' => $msg)));
	}

	/**
	 * 接口返回信息
	 * @param string $msg 提示信息
	 * @param array $info 返回内容
	 * @param string $res 成功失败
	 * @return array
	 */
	public function _return($msg = '', $info = '', $res = 'fail') {
		$data['res'] = $res;
		$data['msg'] = $msg;
		if (!empty($info))
			$data['info'] = $info;
		return $data;
	}

	/**
	 * 接口数据添加
	 * @param array $data 数据
	 * @param string $object 对象
	 * @return array
	 */
	public function _insert($data, $object) {
		$res = $this->db->insert('tc_' . $object, $data);
		return $res ? array('res' => 'succ', $object . '_id' => $this->db->insert_id()) : array('res' => 'fail');
	}

	/**
	 * 接口数据更新
	 * @param array $data
	 * @param string $object
	 * @return array
	 */
	public function _update($data, $object) {
		$where = array($object . '_number' => $data[$object . '_number']);
		$res = $this->db->update('tc_' . $object, $data, $where);
		return $res ? array('res' => 'succ', $object . '_id' => $this->db->affected_rows()) : array('res' => 'fail');
	}

	// 2014-07-17 api end
}

?>
