<?php
class Rebate_model extends CI_Model{

	//这里代表那些字段在列表上显示出来
	public function listLayout(){
		return array(

			'rebate_create_time',

			'rebate_order_id',

			'rebate_create_user',

			'rebate_amount',

			'rebate_account_id',

			'rebate_pay_method',

			'rebate_pay_info',

			'rebate_status',

			'rebate_note',

			'rebate_person',

		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout(){
		return array(

			'rebate_create_time',

			'rebate_order_id',

			'rebate_create_user',

			'rebate_amount',

			'rebate_account_id',

			'rebate_pay_method',

			'rebate_pay_info',

			'rebate_status',

			'rebate_note',

			'rebate_person',

		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout(){
		return array(

			'rebate_create_time',

			'rebate_order_id',

			'rebate_create_user',

			'rebate_amount',

			'rebate_account_id',

			'rebate_pay_method',

			'rebate_pay_info',

			'rebate_status',

			'rebate_note',

			'rebate_person',

		);
	}

	//属性对应的中文标签
	public function attributeLabels(){
		return array(

			'rebate_order_id' => '所属订单',

			'rebate_create_user' => '创建人',

			'rebate_create_time' => '创建时间',

			'rebate_amount' => '返点金额',

			'rebate_account_id' => '选择返点的客户',

			'rebate_pay_method' => '付款方式',

			'rebate_pay_info' => '付款内容',

			'rebate_status' => '返点状态',

			'rebate_note' => '返点备注',

			'rebate_person' => '返点经手人姓名',

			'rebate_book_id' => '对应日记账',

			'rebate_pay_account ' => '支付账户',

			'rebbate_reviewer' => '业务负责人'

		);
	}

	//列表查询
	/*
		$page //获得当前的页面值
		$perNumber //每页显示的记录数
	*/
	public function listGetInfo($where,$page=1,$perNumber=10,$like=""){
		$limitStart = ($page-1)*$perNumber+1-1; //这里因为数据库是从0开始算的！所以要减1
		$limit=array($perNumber,$limitStart);
		//p($limit);
		$data = $this->GetInfo($where,$limit,$like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id){
		$where=array(
			'rebate_id'=>$id
		);
		$data = $this->GetInfo($where);
		if(!empty($data)){
			$data = $data[0];
		}

		return $data;
	}

	//单个修改
	public function update($data,$id){
		$this->db->where('rebate_id',$id)->update('tc_rebate',$data);
	}

	//单个删除
	public function del($id){
		$this->db->where('rebate_id',$id)->delete('tc_rebate');
	}

	//单个新增
	public function add($data){
		$this->db->insert('tc_rebate',$data);
		$insert_id=$this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where="",$like=""){
		$this->db->from('tc_rebate');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}


	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where="",$limit="",$like=""){
		//首先查询出本身所有的内容
		$this->db->from('tc_rebate');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($limit!=""){
			$this->db->limit($limit[0],$limit[1]);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$data=$this->db->get()->result_array();
		//然后循环查询出所有引用的内容
		//p($data);
		foreach ($data as $k=>$v){
			//p($v);die;
			//所有属性循环
			if($v['rebate_reviewer']!="" and $v['rebate_reviewer']!=0){
				$this->load->model('www/user_model','user');
				$data[$k]['rebate_reviewer_arr'] = $this->user->id_aGetInfo($v['rebate_reviewer']);
			}
			if($v['rebate_order_id']!="" and $v['rebate_order_id']!=0){
				$this->load->model('www/order_model','order');
				$data[$k]['rebate_order_id_arr'] = $this->order->id_aGetInfo($v['rebate_order_id']);
			}
			if($v['rebate_create_user']!="" and $v['rebate_create_user']!=0){
				$this->load->model('www/user_model','user');
				$data[$k]['rebate_create_user_arr'] = $this->user->id_aGetInfo($v['rebate_create_user']);
			}
			if($v['rebate_account_id']!="" and $v['rebate_account_id']!=0){
				$this->load->model('www/account_model','account');
				$data[$k]['rebate_account_id_arr'] = $this->account->id_aGetInfo($v['rebate_account_id']);
			}
			//当属性类型为下拉单选时
			if($v['rebate_pay_method']!="" and $v['rebate_pay_method']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['rebate_pay_method_arr']=$this->enum->getlist(435,$data[$k]['rebate_pay_method']);
			}
			//当属性类型为下拉单选时
			if($v['rebate_status']!="" and $v['rebate_status']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['rebate_status_arr']=$this->enum->getlist(437,$data[$k]['rebate_status']);
			}

			if($v['rebate_book_id']!="" and $v['rebate_book_id']!=0){
				$this->load->model('www/books_model','books');
				$data[$k]['rebate_book_id_arr'] = $this->books->id_aGetInfo($v['rebate_book_id']);
			}

			if($v['rebate_pay_account']!="" and $v['rebate_pay_account']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['rebate_pay_account_arr']=$this->enum->getlist(540,$data[$k]['rebate_pay_account']);
			}
		}
		return $data;
	}

	//rebate需要专门汇总金额，写个获取相关订单的未确认返点、已确认返点
	public function SumGetInfo($field_name,$where=""){
		//echo $field_name;
		//p($where);
		//exit;
		$this->db->from('tc_rebate');
		$this->db->select_sum($field_name,"sum");
		if ($where!=""){
			$this->db->where($where);
		}
		//p($this->db->Produces);
		$data=$this->db->get()->result_array();
		if ($data[0]['sum']==""){
			$data[0]['sum']=0;
		}
		return $data[0]['sum'];
	}

}
?>
