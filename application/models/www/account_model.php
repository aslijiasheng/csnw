<?php

class Account_model extends CI_Model {

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'account_name',
			'account_leader',
			'account_account_shopexid',
			'account_telephone',
			'account_email',
			'account_department',
			'account_{#attr_name#}',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'account_leader',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
			'account_name',
			'account_leader',
			'account_account_shopexid',
			'account_telephone',
			'account_email',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'account_name' => '名称',
			'account_leader' => '负责人',
			'account_update_time' => '更新时间',
			'account_account_shopexid' => '商业id',
			'account_create_time' => '创建时间',
			'account_telephone' => '电话',
			'account_type_id' => '客户类型',
			'account_email' => '客户邮箱',
			'account_department' => '所属部门',
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'account_id' => $id
		);
		$data = $this->GetInfo($where);
		if (!empty($data)) {
			$data = $data[0];
		}

		return $data;
	}

	//单条查询
	public function validation($data) {
		extract($data, EXTR_OVERWRITE);
		$fields = empty($fields)?"*":$fields;
		$this->db->select($fields);
		$this->db->from('tc_account');
		$this->db->where($where);
		if ($where!="") {
			$this->db->where($where);
		}
		$get_obj  = $this->db->get();
		$data     = $get_obj->result_array();
		return $data[0];
	}

	//用ID单个查询(简单版-为了防止无限循环)
	public function id_aGetInfo_simple($id) {
		$this->db->from('tc_account');
		$where = array(
			'account_id' => $id
		);
		$this->db->where($where);
		$data = $this->db->get()->result_array();
		if (isset($data[0])) {
			$data = $data[0];
		}
		return $data;
	}

	//单个修改
	public function update($data, $id) {
		$this->db->where('account_id', $id)->update('tc_account', $data);
	}

	//单个删除
	public function del($id) {
		$this->db->where('account_id', $id)->delete('tc_account');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_account', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_account');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_account');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容

		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环


























































			if ($v['account_department'] != "" and $v['account_department'] != 0) {
				$this->load->model('www/department_model', 'department');
				$data[$k]['account_department_arr'] = $this->department->id_aGetInfo($v['account_department']);
			}
		}
		return $data;
	}

	public function __call($name, $arguments) {
		return 'none';
	}

	public function api_add($data, $nowReturn = false) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('account');
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $user['user_id'];
			unset($data['user']);
		}

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				if ($k == 'type_id') {
					$save_data[$k] = $v;
				} else {
					$this_fields = $objDetail['obj'] . '_' . $k;
					if (is_array($v)) {
						$json = $this->arrJson($v);
						$save_data[$this_fields] = json_encode($json);
					} else
						$save_data[$this_fields] = $v;
				}
			}
		}

//		p($save_data);exit;
		if ($aId = $this->db->select('account_id')->get_where('tc_account', array('account_account_shopexid' => $save_data['account_account_shopexid']))->row_array()) {
			return array('res' => 'succ', 'msg' => '客户已存在', 'info' => array('account_id' => $aId['account_id']));
		}

		$res = $this->db->insert('tc_account', $save_data);
		if ($res) {
			$res_id = $this->db->insert_id();
			return array('res' => 'succ', 'msg' => '', 'info' => array('account_id' => $res_id));
		} else
			return array('res' => 'fail', 'msg' => 'error account save');
	}

	private function arrJson($v) {
		$json = '';
//		foreach ($v as $kk => $vv) {
//			if (strpos($kk, '.')) {
//				list($fields, $attr) = explode('.', $kk);
//				if ($fields == 'bank_type') {
//					$res_enum = $this->db->select('enum_key')->get_where('dd_enum', array('enum_name' => $vv, 'attr_id' => '560'))->row_array();
//					if (!$res_enum)
//						return array('res' => 'fail', 'msg' => 'error [' . $kk . ' : ' . $vv . ']');
//					$json[$fields] = $res_enum['enum_key'];
//				}
//			} else {
//				$json[$kk] = $vv;
//			}
//		}
		return $json;
	}

	public function getRelOrder($account_id) {
		return $this->db->select()->from('tc_order')->where('order_account', $account_id)->get()->result_array();
	}

}

?>
