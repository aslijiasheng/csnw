<?php 
  class Activity_auth_model extends CI_Model{
  	 public function add($data,$role_id){
       $this->db->where('role_id',$role_id)->update('tc_role',$data);
  	 }
  	 public function check($role_id){
       return $this->db->get_where('tc_role',array('role_id'=>$role_id))->result_array();
  	 }
  }
 ?>