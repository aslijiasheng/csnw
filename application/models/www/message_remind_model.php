<?php
class Message_remind_model extends CI_Model{

	//属性对应的中文标签
	public function attributeLabels(){
		return array(

			'message_remind_role_id' => '角色id',

			'message_remind_status' => '状态',

		);
	}

	//列表查询
	/*
		$page //获得当前的页面值
		$perNumber //每页显示的记录数
	*/
	public function listGetInfo($where,$page=1,$perNumber=10,$like=""){
		$limitStart = ($page-1)*$perNumber+1-1; //这里因为数据库是从0开始算的！所以要减1
		$limit=array($perNumber,$limitStart);
		//p($limit);
		$data = $this->GetInfo($where,$limit,$like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id){
		$where=array(
			'message_remind_role_id'=>$id
		);
		$data = $this->GetInfo($where);
		if(!empty($data)){
			$data = $data[0];
		}

		return $data;
	}

	//单个修改
	public function update($data,$id){
		$re = $this->id_aGetInfo($id);
		if(!empty($re)){
			$this->db->where('message_remind_role_id',$id)->update('tc_message_remind',$data);
		}else{
			$this->add($data);
		}

	}

	//单个删除
	public function del($id){
		$this->db->where('message_remind_id',$id)->delete('tc_message_remind');
	}

	//单个新增
	public function add($data){
		$this->db->insert('tc_message_remind',$data);
		$insert_id=$this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where="",$like=""){
		$this->db->from('tc_message_remind');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}


	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where="",$limit="",$like=""){
		//首先查询出本身所有的内容
		$this->db->from('tc_message_remind');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($limit!=""){
			$this->db->limit($limit[0],$limit[1]);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$data=$this->db->get()->result_array();
		//然后循环查询出所有引用的内容

		foreach ($data as $k=>$v){
			//p($v);die;
			//所有属性循环










				 //当属性类型为单选时
			if($v['message_remind_status']!="" and $v['message_remind_status']!=0){
				$this->load->model('admin/enum_model','enum');

				$data[$k]['message_remind_status_arr']=$this->enum->getlist(537,$data[$k]['message_remind_status']);
			}





		}
		return $data;
	}
}
?>
