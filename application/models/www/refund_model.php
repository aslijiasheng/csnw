<?php

class Refund_model extends CI_Model {

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'refund_manage',
			'refund_goods_info',
			'refund_pay_method',
			'refund_reason',
			'refund_status',
			'refund_create_user',
			'refund_create_time',
			'refund_pay_info',
			'refund_order_id',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
			'refund_manage',
			'refund_goods_info',
			'refund_pay_method',
			'refund_reason',
			'refund_status',
			'refund_create_user',
			'refund_create_time',
			'refund_pay_info',
			'refund_order_id',
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
			'refund_manage',
			'refund_goods_info',
			'refund_pay_method',
			'refund_reason',
			'refund_status',
			'refund_create_user',
			'refund_create_time',
			'refund_pay_info',
			'refund_order_id',
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'refund_manage' => '业务总监',
			'refund_goods_info' => '退款商品信息',
			'refund_pay_method' => '退款方式',
			'refund_reason' => '退款理由',
			'refund_status' => '退款状态',
			'refund_create_user' => '创建人',
			'refund_create_time' => '创建时间',
			'refund_pay_info' => '付款信息',
			'refund_order_id' => '关联订单',
			'refund_book_id' => '对应日记账',
			'refund_pay_account ' => '支付账户',
			'refund_reviewer' => '业务负责人'
		);
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'refund_id' => $id
		);
		$data = $this->GetInfo($where);
		if (!empty($data)) {
			$data = $data[0];
		}

		return $data;
	}

	//单个修改
	public function update($data, $id) {
		$this->db->where('refund_id', $id)->update('tc_refund', $data);
	}

	//单个删除
	public function del($id) {
		$this->db->where('refund_id', $id)->delete('tc_refund');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_refund', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_refund');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_refund');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容

		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环


			if ($v['refund_manage'] != "" and $v['refund_manage'] != 0) {
				$this->load->model('www/user_model', 'user');
				$data[$k]['refund_manage_arr'] = $this->user->id_aGetInfo($v['refund_manage']);
			}
			//当属性类型为下拉单选时
			if ($v['refund_pay_method'] != "" and $v['refund_pay_method'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['refund_pay_method_arr'] = $this->enum->getlist(453, $data[$k]['refund_pay_method']);
			}

			//当属性类型为下拉单选时
			if ($v['refund_status'] != "" and $v['refund_status'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['refund_status_arr'] = $this->enum->getlist(455, $data[$k]['refund_status']);
			}
			if ($v['refund_create_user'] != "" and $v['refund_create_user'] != 0) {
				$this->load->model('www/user_model', 'user');
				$data[$k]['refund_create_user_arr'] = $this->user->id_aGetInfo($v['refund_create_user']);
			}
			if ($v['refund_order_id'] != "" and $v['refund_order_id'] != 0) {
				$this->load->model('www/order_model', 'order');
				$data[$k]['refund_order_id_arr'] = $this->order->id_aGetInfo($v['refund_order_id']);
			}

			if ($v['refund_book_id'] != "" and $v['refund_book_id'] != 0) {
				$this->load->model('www/books_model', 'books');
				$data[$k]['refund_book_id_arr'] = $this->books->id_aGetInfo($v['refund_book_id']);
			}

			if ($v['refund_pay_account'] != "" and $v['refund_pay_account'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['refund_pay_account_arr'] = $this->enum->getlist(540, $data[$k]['refund_pay_account']);
			}
		}
		return $data;
	}

	//refund需要专门汇总金额，写个获取相关订单的未确认返点、已确认返点
	public function SumGetInfo($field_name, $where = "") {
		//echo $field_name;
		//p($where);
		//exit;
		$this->db->from('tc_refund');
		$this->db->select_sum($field_name, "sum");
		if ($where != "") {
			$this->db->where($where);
		}
		//p($this->db->Produces);
		$data = $this->db->get()->result_array();
		if ($data[0]['sum'] == "") {
			$data[0]['sum'] = 0;
		}
		return $data[0]['sum'];
	}

	public function api_update($data, $where) {
		$update_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('refund');
		$object = $objDetail['obj'];

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $object . '_' . $fields;
				if (!$this->db->field_exists($this_fields, 'tc_' . $object)) {
					return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
				}

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$update_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$update_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $object . '_' . $k;
				if (!$this->db->field_exists($this_fields, 'tc_' . $object)) {
					return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
				}

				//内容是数组，将内容组成 json
				if (is_array($v)) {
					$json = '';
					foreach ($v as $kk => $vv) {
						if (strpos($kk, '.')) {
							list($fields, $attr) = explode('.', $kk);
							if ($fields == 'bank_type') {
								$res_enum = $this->db->select('enum_key')->get_where('dd_enum', array('enum_name' => $vv, 'attr_id' => '560'))->row_array();
								if (!$res_enum)
									return array('res' => 'fail', 'msg' => 'error [' . $kk . ' : ' . $vv . ']');
								$json[$fields] = $res_enum['enum_key'];
							}
						} else {
							$json[$kk] = $vv;
						}
					}
					$update_data[$this_fields] = json_encode($json);
				} else
					$update_data[$this_fields] = $v;
			}
		}
		$res = $this->db->update('tc_' . $object, $update_data, $where);
		if ($res)
			return array('res' => 'succ', 'msg' => 'update success');
		else
			return array('res' => 'fail', 'msg' => 'update failure');
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('refund');
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $user['user_id'];
			unset($data['user']);
		}

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
					$json = $this->arrJson($v);
					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}

		$res = $this->db->insert('tc_refund', $save_data);
		if ($res) {
			return array('res' => 'succ', 'msg' => '');
		} else
			return array('res' => 'fail', 'msg' => 'error refund save');
	}

	private function arrJson($v) {
		$json = '';
		foreach ($v as $kk => $vv) {
			if (strpos($kk, '.')) {
				list($fields, $attr) = explode('.', $kk);
				if ($fields == 'bank_type') {
					$res_enum = $this->db->select('enum_key')->get_where('dd_enum', array('enum_name' => $vv, 'attr_id' => '560'))->row_array();
					if (!$res_enum)
						return array('res' => 'fail', 'msg' => 'error [' . $kk . ' : ' . $vv . ']');
					$json[$fields] = $res_enum['enum_key'];
				}
			} else {
				$json[$kk] = $vv;
			}
		}
		return $json;
	}

	public function _addCheck($data = '', $user = '') {
		if (empty($data))
			return $this->api->_msg('');
		return $this->api->_return(__class__ . ' _addCheck succ', $data, 'succ');
	}

}

?>
