<?php
class System_log_model extends CI_Model{

	//这里代表那些字段在列表上显示出来
	public function listLayout(){
		return array(

			'system_log_user_id',

			'system_log_user_name',

			'system_log_ip',

			'system_log_login_time',

			'system_log_addtime',

			'system_log_operation',

		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout(){
		return array(

			'system_log_user_id',

			'system_log_ip',

			'system_log_user_action',

			'system_log_param',

			'system_log_addtime',

		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout(){
		return array(

			'system_log_user_id',

			'system_log_ip',

			'system_log_user_action',

			'system_log_param',

			'system_log_addtime',

		);
	}

	//属性对应的中文标签
	public function attributeLabels(){
		return array(

			'system_log_user_id' => '操作人ID',

			'system_log_ip' => 'IP地址',

			'system_log_operation' => '登录人操作',

			'system_log_param' => '详细参数',

			'system_log_addtime' => '日志添加时间',

			'system_log_login_time' => '登录时间',

			'system_log_user_name' => '操作人姓名',

			'system_log_module' => '操作模块',

			'system_log_module_id' => '模块id',

			'system_log_note' => '备注信息',

		);
	}

	//列表查询
	/*
		$page //获得当前的页面值
		$perNumber //每页显示的记录数
	*/
	public function listGetInfo($where,$page=1,$perNumber=10,$like=""){
		$limitStart = ($page-1)*$perNumber+1-1; //这里因为数据库是从0开始算的！所以要减1
		$limit=array($perNumber,$limitStart);
		//p($limit);
		$data = $this->GetInfo($where,$limit,$like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id){
		$where=array(
			'system_log_id'=>$id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data,$id){
		$this->db->where('system_log_id',$id)->update('tc_system_log',$data);
	}

	//单个删除
	public function del($id){
		$this->db->where('system_log_id',$id)->delete('tc_system_log');
	}

	//单个新增
	public function add($data){
		$this->db->insert('tc_system_log',$data);
		echo "<br/>".$this->db->_error_message();
		echo "<br/>".$this->db->_error_number();
		$insert_id=$this->db->insert_id();
		if($data['system_log_user_id']==0 and $data['system_log_operation']=="确认到账" and $data['system_log_module']=="Order"){
			p("本操作出现故障，请不要继续操作，请速与IT部 李成 联系。");
			exit;
		}
		return $insert_id;
	}

	//根据条件查询出数据总数
	public function countGetInfo($where="",$like=""){
		$this->db->from('tc_system_log');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}


	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where="",$limit="",$like=""){
		//首先查询出本身所有的内容
		$this->db->from('tc_system_log');
		if ($where!=""){
			$this->db->where($where);
		}
		if ($limit!=""){
			$this->db->limit($limit[0],$limit[1]);
		}
		if ($like!=""){
			$this->db->like($like);
		}
		$data=$this->db->order_by('system_log_addtime','desc')->get()->result_array();
		//然后循环查询出所有引用的内容

		foreach ($data as $k=>$v){
			//p($v);die;
			//所有属性循环


			if($v['system_log_user_id']!="" and $v['system_log_user_id']!=0){
				$this->load->model('www/user_model','user');
				$data[$k]['system_log_user_id_arr'] = $this->user->id_aGetInfo($v['system_log_user_id']);
			}
		}
		return $data;
	}

}
?>
