<?php

class Rel_order_order_model extends CI_Model {

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_rel_order_order');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->get()->result_array();
		//然后循环查询出所有引用的内容
		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环
			if ($v['rel_order_order_rel_order'] != "" and $v['rel_order_order_rel_order'] != 0) {
				$this->load->model('www/order_model', 'order');
				$data[$k]['rel_order_order_rel_order_arr'] = $this->order->id_aGetInfo($v['rel_order_order_rel_order']);
			}
		}
		return $data;
	}

	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('rel_order_order');
		if (isset($data['user'])) {
			$create_user_id = $data['user'];
			unset($data['user']);
		}

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				if ($fields == 'type') {
					$where = array('type_' . $attr => $v);
					$res = $this->db->select('type_id')->get_where('dd_type', $where)->row_array();
					if ($res)
						$save_data['type_id'] = $res['type_id'];
					else
						return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
				} else {
					switch ($objDetail['obj_attr'][$fields]) {
						case 'enum':
							$attr_id = $objDetail['obj_attr_id'][$fields];
							$res_enum = $this->api->parseEnum($v, $attr_id);
							if ($res_enum)
								$save_data[$this_fields] = $res_enum;
							else
								return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
							break;
						default: //引用
							$q_obj = $objDetail['obj_attr'][$fields];
							$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
							if ($res_quote)
								$save_data[$this_fields] = $res_quote;
							else
								return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
							break;
					}
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
//					$json = $this->arrJson($v);
					$json = '';
					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}

		$res = $this->db->insert('tc_rel_order_order', $save_data);
		if ($res) {
			return array('res' => 'succ', 'msg' => '');
		} else
			return array('res' => 'fail', 'msg' => 'error rel_order_order save');
	}

	public function api_update($data, $where) {
		$update_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('rel_order_order');
		$object = $objDetail['obj'];

		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $object . '_' . $fields;
				if (!$this->db->field_exists($this_fields, 'tc_' . $object)) {
					return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
				}

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$update_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$update_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $object . '_' . $k;
				if (!$this->db->field_exists($this_fields, 'tc_' . $object)) {
					return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
				}

				//内容是数组，将内容组成 json
				if (is_array($v)) {
//					$json = '';
//					$update_data[$this_fields] = json_encode($json);
				} else
					$update_data[$this_fields] = $v;
			}
		}
		$res = $this->db->update('tc_' . $object, $update_data, $where);
		if ($res) {
			return array('res' => 'succ', 'msg' => 'update success');
		} else
			return array('res' => 'fail', 'msg' => 'update failure');
	}

	//2014-07-17 api start
	public function _addCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无关联订单数据');
		return $this->api->_return(__class__ . ' _addCheck succ', $data, 'succ');
	}

	public function _updateCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无关联订单数据');
		return $this->api->_return(__class__ . ' _updateCheck succ', $data, 'succ');
	}
	//2014-07-17 api end

}

?>
