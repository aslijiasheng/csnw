
<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/*
 * @Author:sky
 * @Crate Time:2015-01-20 19:28
 * @Last modified:2015-01-20 19:28
 * @Description:指令明细模型
 */
class Instruction_item_model extends CI_Model {

    function __construct() {
        parent::__construct();
		$this->db->query('set names utf8');
		date_default_timezone_set('PRC');
		$this->db->trans_strict(FALSE);
    }

	public function _addCheck($data = '', $user = '') {
		$this->load->model('www/api_model', 'api');
		if (empty($data))
			return $this->api->_msg('无订单明细数据');
		return $this->api->_return(__class__ . ' _addCheck succ', $data, 'succ');
	}

    /**
     * api_add 
     * 指令明细新增方法
     * @param mixed $data 新增数据
     * @access public
     * @return void
     */
	public function api_add($data) {
		$save_data = array();
		$this->load->model('www/api_model', 'api');
		$objDetail = $this->api->objDetail('instruction_item');
		//生成创建人id
		if (isset($data['user'])) {
			$user = $this->db->select('user_id')->get_where('tc_user', array('user_login_name' => $data['user']))->row_array();
			if ($user)
				$create_user_id = $user['user_id'];
			unset($data['user']);
		}
		foreach ($data as $k => $v) {
			if (strpos($k, '.')) {
				list($fields, $attr) = explode('.', $k);
				$this_fields = $objDetail['obj'] . '_' . $fields;

				switch ($objDetail['obj_attr'][$fields]) {
					case 'enum':
						$attr_id = $objDetail['obj_attr_id'][$fields];
						$res_enum = $this->api->parseEnum($v, $attr_id);
						if ($res_enum)
							$save_data[$this_fields] = $res_enum;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
					default: //引用
						$q_obj = $objDetail['obj_attr'][$fields];
						$res_quote = $this->api->parseQuote($v, $attr, $q_obj);
						if ($res_quote)
							$save_data[$this_fields] = $res_quote;
						else
							return array('res' => 'fail', 'msg' => 'error [' . $k . ' : ' . $v . ']');
						break;
				}
			} else {
				$this_fields = $objDetail['obj'] . '_' . $k;
				if (is_array($v)) {
					$json = $this->arrJson($v);
					$save_data[$this_fields] = json_encode($json);
				} else
					$save_data[$this_fields] = $v;
			}
		}
		$res = $this->db->insert('tc_instruction_item', $save_data);
		if ($res) {
			return array('res' => 'succ', 'msg' => 'add success');
		} else
			return array('res' => 'fail', 'msg' => 'error order_d save');
	}

    /**
     * get_item_instruc 
     * 处理指令明细数据查询
     * @param mixed $instruc_id 指令明细主键ID
     * @access public
     * @return array
     */
    public function get_item_instruc($instruc_id) {
        $where = array(
            "instruction_item_instruction_id" => $instruc_id,
        );
        //首先查询出本身所有的内容
        $this->db->from('tc_instruction_item');
        if ($where != "") {
            $this->db->where($where);
        }
        $data = $this->db->get()->result_array();
        return $data;
    }

}
?>
