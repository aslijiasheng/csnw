<?php

class Message_model extends CI_Model {

	//这里代表那些字段在列表上显示出来
	public function listLayout() {
		return array(
			'message_owner',
			'message_module',
			'message_module_id',
			'message_type',
			'message_status',
		);
	}

	//这里代表那些字段在编辑页面上显示出来
	public function editLayout() {
		return array(
		);
	}

	//这里代表那些字段在查看页面上显示出来
	public function viewLayout() {
		return array(
		);
	}

	//属性对应的中文标签
	public function attributeLabels() {
		return array(
			'message_owner' => '负责人',
			'message_module' => '所属模块',
			'message_module_id' => '模块id',
			'message_type' => '代办类型',
			'message_url' => '链接地址',
			'message_status' => '待办状态',
			'message_department' => '部门',
		);
	}

	public function action_name($val) {
		//echo $val;
		switch ($val) {
			case '1001':
				return 'confirm_salespay';
				break;
			case '1002':
				return 'confirm_invoice';
				break;
			case '1003':
				return 'invalidate_invoice';
				break;
			case '1004':
				return 'verify_invoice';
				break;
			case '1005':
				return 'examine_rebate';
				break;
			case '1006':
				return 'confirm_rebate';
				break;
			case '1007':
				return 'examine_refund';
				break;
			case '1008':
				return 'confirm_refund';
				break;
			case '1010':
				return 'edit_salespay';
				break;
			case '1011':
				return 'edit_invoice';
				break;
			case '1012':
				return 'edit_rebate';
				break;
			case '1013':
				return 'edit_refund';
				break;
			case '1014':
			case '1018':
				return 'cancel_check';
				break;
			case '1015':
			case '1019':
				return 'cancel_affirm';
				break;
			case '1016':
			case '1017':
				return 'order_cancel';
				break;
			case '1020':
				return 'check_salespay';
				break;
			case '1021':
				return 'examine_penny';
				break;

			default:
				# code...
				break;
		}
	}

	//列表查询
	/*
	  $page //获得当前的页面值
	  $perNumber //每页显示的记录数
	 */
	public function listGetInfo($where, $page = 1, $perNumber = 10, $like = "") {
		$limitStart = ($page - 1) * $perNumber + 1 - 1; //这里因为数据库是从0开始算的！所以要减1
		$limit = array($perNumber, $limitStart);
		//p($limit);
		$data = $this->GetInfo($where, $limit, $like);
		return $data;
	}

	//用ID单个查询
	public function id_aGetInfo($id) {
		$where = array(
			'message_id' => $id
		);
		$data = $this->GetInfo($where);
		$data = $data[0];
		return $data;
	}

	//单个修改
	public function update($data, $where) {
		//p($where);
		$this->db->where($where)->update('tc_message', $data);
	}

	//单个删除
	public function del($id) {
		$this->db->where('message_id', $id)->delete('tc_message');
	}

	//单个新增
	public function add($data) {
		$this->db->insert('tc_message', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//通过api打数据时使用的新增
	public function api_add($param) {

	}

	//根据条件查询出数据总数
	public function countGetInfo($where = "", $like = "") {
		$this->db->from('tc_message');
		if ($where != "") {
			$this->db->where($where);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$count = $this->db->count_all_results();
		return $count;
	}
	
	//2014-08-15 更新计算统计
	public function getCount( $where )
	{
		if( isset( $where ) )
		{
			$this->db->from('tc_message');
			$this->db->where_in("message_type",$where["message_type"]);
			unset($where['message_type']);
			$this->db->where($where);
			$count = $this->db->count_all_results();			
		}
		return $count;
	}

	//所有的查询都写一个通用方法 参数$where为判断条件的数组
	public function GetInfo($where = "", $limit = "", $like = "") {
		//首先查询出本身所有的内容
		$this->db->from('tc_message');
		if(isset($where['message_type']))
		{
			$this->db->where_in("message_type",$where['message_type']);
			unset($where['message_type']);
		}
		if ($where != "") {
			$this->db->where($where);
		}
		if ($limit != "") {
			$this->db->limit($limit[0], $limit[1]);
		}
		if ($like != "") {
			$this->db->like($like);
		}
		$data = $this->db->order_by('message_id', 'desc')->get()->result_array();
		//然后循环查询出所有引用的内容
		$data = $this->info( $data );
		return $data;
	}
	
	public function info( $data )
	{
		
		foreach ($data as $k => $v) {
			//p($v);die;
			//所有属性循环
			$message_module = $v['message_module']."_model";

			$this->load->model( 'www/'.$message_module, $message_module );
//			PC::data("ddd");
			$data[$k]['info'] = $this->$message_module->id_aGetInfo( $v['message_module_id'] );
			//array_push($data['info'][$k],$data[$k]);
			//unset($data[$k]);
			
			if ($v['message_owner'] != "" and $v['message_owner'] != 0) {
				$this->load->model('www/user_model', 'user');
				$data[$k]['message_owner_arr'] = $this->user->id_aGetInfo($v['message_owner']);
			}


			//当属性类型为单选时
			if ($v['message_type'] != "" and $v['message_type'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['message_type_arr'] = $this->enum->getlist(526, $data[$k]['message_type']);
			}

			if ($v['message_department'] != "" and $v['message_department'] != 0) {
				$this->load->model('www/department_model', 'department');
				$data[$k]['message_department_arr'] = $this->department->id_aGetInfo($v['message_department']);
			}

			//当属性类型为单选时
			if ($v['message_status'] != "" and $v['message_status'] != 0) {
				$this->load->model('admin/enum_model', 'enum');

				$data[$k]['message_status_arr'] = $this->enum->getlist(528, $data[$k]['message_status']);
			}

			$this->load->model('www/' . $v['message_module'] . '_model', $v['message_module']);
			$module_data = $this->$v['message_module']->id_aGetInfo($v['message_module_id']);
			//p($module_data);
			$data[$k]['order_number'] = $module_data[$v['message_module'] . '_order_id_arr']['order_number'];
			
		}
		return $data;
	}

}

?>
