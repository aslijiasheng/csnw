/************dialog************/
//cus lee 首先是初始化所有添加dialog的页面 start
dialog_btn_id = $("[leetype='dialog']").attr("id");
dialog_div_id = dialog_btn_id+"_dialog_div";
dialog_html_id = dialog_btn_id+"_dialog_html";
$("[leetype='dialog']").closest("div").append("<div id=\""+dialog_div_id+"\" style=\"display:none;\"><div id=\""+dialog_html_id+"\"></div></div>");
//cus lee 首先是初始化所有添加dialog的页面 end
$("[leetype='dialog']").click(function(){
	url = $(this).attr('url');
	btn_id = $(this).attr("id");
	title = $(this).attr("title");
	div_id = btn_id+"_dialog_div";
	html_id = btn_id+"_dialog_html";
	$.ajax({
		'type':'get',
		'data':{
			'div_id':div_id,
			'html_id':html_id
		},
		'success':function(data){
			$('#'+html_id).html(data);
		},
		'url':url,
		'cache':false
	});
	$("#"+div_id).dialog({
		title:title,
		width:800,
		height:450
	});
	$('#'+div_id).dialog('open');
});

/************icondialog************/
//cus lee 首先是初始化所有添加dialog的页面 start
$("[leetype='icondialog']").each(function(){
	input_id = $(this).attr("id");
	url = $(this).attr("url");
	div_id = input_id+"_dialog_div";
	html_id = input_id+"_dialog_html";
	span_id = input_id+"_span";
	btn_id = input_id+"_btn";
	//在上面加上2个显示图标页面和输入栏
	$(this).before('<span id="'+span_id+'" class="add-on"><span data-icon=""></span></span>');
	//在下面加上dialog的2个DIV
	$(this).after("<div id=\""+div_id+"\" style=\"display:none;\"><div id=\""+html_id+"\"></div></div>");
	$(this).after("<span id='"+btn_id+"' url='"+url+"' class='add-on btn' leetype='icondialog_btn'><span data-icon='&#xe0a9'></span></span>");
	//将他们框起来
	$("#"+span_id+",#"+input_id+",#"+btn_id).wrapAll("<div class='input-prepend input-append'></div>");
});

//cus lee 首先是初始化所有添加dialog的页面 end
/*
$("[leetype='icondialog']").click(function(){
	url = $(this).attr('url');
	input_id = $(this).attr("id");
	title = $(this).attr("title");
	div_id = input_id+"_dialog_div";
	html_id = input_id+"_dialog_html";
	span_id = input_id+"_span";
	btn_id = input_id+"_btn";
	$.ajax({
		'type':'get',
		'data':{
			'div_id':div_id,
			'html_id':html_id,
			'span_id':span_id,
			'input_id':input_id
		},
		'success':function(data){
			$('#'+html_id).html(data);
		},
		'url':url,
		'cache':false
	});
	$("#"+div_id).dialog({
		title:title,
		width:800,
		height:450
	});
	$('#'+div_id).dialog('open');
});
*/
$("[leetype='icondialog_btn']").click(function(){
	url = $(this).attr('url');
	btn_id = $(this).attr("id");
	input_id = btn_id.replace("_btn", "");
	title = $("#"+input_id).attr("title");
	div_id = input_id+"_dialog_div";
	html_id = input_id+"_dialog_html";
	span_id = input_id+"_span";
	btn_id = input_id+"_btn";
	$.ajax({
		'type':'get',
		'data':{
			'div_id':div_id,
			'html_id':html_id,
			'span_id':span_id,
			'input_id':input_id
		},
		'success':function(data){
			$('#'+html_id).html(data);
		},
		'url':url,
		'cache':false
	});
	$("#"+div_id).dialog({
		title:title,
		width:800,
		height:450
	});
	$('#'+div_id).dialog('open');
});